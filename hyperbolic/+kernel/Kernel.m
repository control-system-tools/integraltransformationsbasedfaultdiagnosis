classdef Kernel < kernel.Characteristic
	%
	properties
		% Successive approximations parameter
		tolerance (1, 1) double;
		iterationMargin (1, :) {mustBeInteger, mustBePositive};
		% finite settling time achievable with backstepping and/or fredholm state feedback
		settlingTime (1, 1);
	end
	
	properties (Constant)
		% path for saving and loading computed kernels
		savedKernelPath = fullfile("build", "kernel");
	end
	
	properties (Abstract = true, SetAccess = protected)
		signOfIntegralTerm (1, 1);
		signOfIntegralTermInverse (1, 1);
		integralBounds cell;
		valueInverse (:, :) quantity.Discrete;
	end
	
	methods
		function obj = Kernel(varargin)
			obj@kernel.Characteristic(varargin{:});
			%% Constructor
			if nargin > 0 % this if-clause is needed to support
				% Characteristic-Array. Whenever a new obj(it) is
				% initialized, the constructor is called without
				% input-arguments. This would lead to errors, that are
				% avoided by this condition.
				%% Input parser
				NameValue = kernel.Kernel.constructorInputParser(varargin{2:end});
				[obj.tolerance] = deal(NameValue.tolerance);
				[obj.iterationMargin] = deal(NameValue.iterationMargin);
								
				if ~obj(1).dummy		
					% calculate settling time
					phi1 = obj(1).Lambda.abs.inv.diag2vec.int("z", 0, 1);
					[obj.settlingTime] = deal(sum(phi1(1:(obj(1).rankPositiveLambda+1))));
				end % if ~obj(1).dummy
			end % if nargin > 0
		end % Kernel() constructor
		
		function [h, hashData] = hash(obj)
			% HASH compute a hash value for this object
			% h = hash(obj) computes a hash value for this object based on
			% the parameters:		
			% * tolerance
			% * iterationMargin
			% * signOfIntegralTerm
			% * signOfIntegralTermInverse
			% * integralBounds
			
			[~, charaData] = hash@kernel.Characteristic(obj);
			
			hashData = [charaData(:)', {obj(1).tolerance, ...
										obj(1).iterationMargin, ...
										obj(1).signOfIntegralTerm, ...
										obj(1).signOfIntegralTermInverse, ...
										obj(1).integralBounds}];
			
			h = misc.hash(hashData);					
		end
		
		function progress = successiveApproximation(obj, varargin)
			%% This method performs the method of successive approximations
			% to calculate the kernel. 			

			progress = misc.ProgressBar("name", ...
				"### Successive Approximations of kernel " + obj(1).name + ": ", ...				
				"steps", obj(1).iterationMargin(end), ...
				"terminalValue", obj(1).iterationMargin(end), ...
				"progress", 1, "silent", obj(1).silent, ...
				"printAbsolutProgress", true);
			
			lastValue = zeros([numel(obj(1).referenceGrid), numel(obj(1).referenceGrid), size(obj)]);
			currentValue = lastValue;
			progress.start("value", currentValue, "addMessage", "steps"); 
			
			obj.updateBoundaryCondition();
			while progress.progress < obj(1).iterationMargin(1) ...
					|| (~(progress.progress > obj(1).iterationMargin(2)) ...
					&& (max(abs(currentValue(:) - lastValue(:))) > obj(1).tolerance))
				% iterate until limit of iterations is reached or until the
				% the difference between the latest result and the result of
				% the iteration before is smaller than the.
				
				%% calculate rhs of characteristic ODE
				currentRhsOfOde = obj.rhsOfOde();

				for it = 1:numel(obj)
					for jt = 1:numel(obj(it).lines)
						thisLinesRhs = diag(currentRhsOfOde(it).on(...
							{obj(it).lines(jt).zDiscrete, obj(it).lines(jt).zetaDiscrete}, ...
							{'z', 'zeta'}));
						obj(it).lines(jt).value(:) = ...
							obj(it).lines(jt).value(1) ...
							+ numeric.cumtrapz_fast(obj(it).lines(jt).sDiscrete, thisLinesRhs.');
					end
				end
			
				%% update boundary conditions
				obj.setValue();
				obj.updateBoundaryCondition();
			
				%% Prepare next iteration
				lastValue = currentValue;
				currentValue = obj.getValue.on({obj(1).referenceGrid, obj(1).referenceGrid});
				progress.raise([], 'value', currentValue, 'addMessage', ...
					['steps | changed: ', num2str(max(abs(currentValue(:) - lastValue(:)))), ...
					' (tolerance=', num2str(obj(1).tolerance), ')\n']);
			end
			
			obj.setValueDiagonalDominant();
			progress.stop();
		end % successiveApproximation()
		
		function xOut = backsteppingTransformation(obj, xIn)
		%FREDHOLM_TRANSFORMATION calculates the backstepping-transform of x_in
		%	x_out(z) = x_in(z) + signOfIntegralTerm ...
		%						* int_0^z backsteppingKernel(z, zeta) x_in(zeta) dzeta
		% backsteppingKernel is sampled to spatial resolution of xIn.
		%
		% INPUT PARAMETERS:
		% 	QUANTITY / DOUBLE-ARRAY	  x_in : value to be transformed of format 
		%									(spatialGrid, index, optional index)
		%
		% OUTPUT PARAMETERS:
		% 	QUANTITY			 x_out : transformed value of format 
		%									(spatialGrid, index, optional index)

			%% Init and input check
			if isnumeric(xIn)
				xSize = size(xIn);
				z = quantity.Domain("z", linspace(0, 1, xSize(1)));
				n = xSize(2);
				columns = size(xIn, 3); % can not be obtained from xSize if ndims(xSize) == 2
				kernelSampled = obj.getValue.on({z.grid, z.grid});

				integral = zeros(xSize);
				for zIdx = 2:z.n
					integrand = zeros(z.n, n, columns);
					for cIdx = 1:columns
						for it = 1:n 
							integrand(:, it, cIdx) = ...
								sum(reshape(kernelSampled(zIdx, :, it, :), [z.n, n]) ...
																.* xIn(:, :, cIdx), 2);
						end
					end
					integral(zIdx, :, :) = reshape(numeric.trapz_fast_nDim(...
						z.grid(1:zIdx), integrand(1:zIdx, :, :), 1), [n, cIdx]);
				end
				xValue = xIn + obj(1).signOfIntegralTerm * integral;
				xOut = quantity.Discrete(xValue, z);
			elseif isa(xIn, "quantity.Discrete")
				xOut = xIn + obj(1).signOfIntegralTerm * ...
					int(obj.getValue() * xIn.subs("z", "zeta"), ...
						"zeta", obj(1).integralBounds{:});
			end
		end % backsteppingTransformation()
		
		function xOut = inverseBacksteppingTransformation(obj, xIn, optArg)
		%inverseBacksteppingTransformation calculates the inverse backstepping-transform of x_in
		%	x_out(z) = x_in(z) + signOfIntegralTerm ...
		%						* int_0^z inverseBacksteppingKernel(z, zeta) x_in(zeta) dzeta
		% backsteppingKernel is sampled to spatial resolution of xIn.
		arguments
			obj
			xIn
			optArg.z (1,1) double;
		end

			%% Init and input check
			if isnumeric(xIn)
				
				if isfield(optArg, "z")
					error("Not yet implemented.")
				end
				
				xSize = size(xIn);
				z = quantity.Domain("z", linspace(0, 1, xSize(1)));
				n = xSize(2);
				columns = size(xIn, 3); % can not be obtained from xSize if ndims(xSize) == 2
				kernelSampled = obj.getValueInverse.on({z.grid, z.grid});

				integral = zeros(xSize);
				for zIdx = 2:z.n
					integrand = zeros(z.n, n, columns);
					for cIdx = 1:columns
						for it = 1:n 
							integrand(:, it, cIdx) = ...
								sum(reshape(kernelSampled(zIdx, :, it, :), [z.n, n]) ...
																.* xIn(:, :, cIdx), 2);
						end
					end
					integral(zIdx, :, :) = reshape(numeric.trapz_fast_nDim(...
						z.grid(1:zIdx), integrand(1:zIdx, :, :), 1), [n, cIdx]);
				end
				xValue = xIn + obj(1).signOfIntegralTermInverse * integral;
				xOut = quantity.Discrete(xValue, z);
			elseif isa(xIn, "quantity.Discrete")
				
				if isfield(optArg, "z")
					% compute the inverse transfromation for a specific
					% point z^*
					% 	x(z*, t) = tilde{x}(z*, t) 
					%             + int_0^z* K_I(z*, z) * tilde{x}(z,t) dz
					
					bounds = obj(1).integralBounds;
					boundLog = strcmp( obj(1).integralBounds, "z" );
					bounds{boundLog} = optArg.z;
					
					xOut = subs(xIn, "z", optArg.z) ...
						+ obj(1).signOfIntegralTermInverse * ...
						int(subs( obj.getValueInverse(), ...
							["z", "zeta"], optArg.z, "z") * xIn, "z", bounds{:});
				else
					% compute the inverse transformation on the spatial
					% domain
					xOut = xIn + obj(1).signOfIntegralTermInverse * ...
						int(obj.getValueInverse() * xIn.subs("z", "zeta"), ...
							"zeta", obj(1).integralBounds{:});
				end
			end
		end % inverseBacksteppingTransformation()
		
		function plotProgress(obj, progress)
			% plots the progress saved during successive approximation
			% using slideSurf.
			sizeOfvalue = size(progress.dataStore{1}.value);
			for it = 1:numel(progress.dataStore)
				progress.dataStore{it}.value = progress.dataStore{it}.value .* ...
					repmat((ones(sizeOfvalue(1)) + ...
						triu(ones(sizeOfvalue(1))+NaN, +1)), ...
						[1, 1, sizeOfvalue(3:end)]);
			end
			misc.slideSurf(progress.dataStore, "xLabel", 'z', "yLabel", '\zeta', ...
				"myTitle", obj(1).name);
		end % plotProgress()
		
		function setValueInverse(obj)
			assert(isequal(size(obj), size(obj(1).Lambda)), "all elements of the kernel are needed to calculate the inverse");
			valueInverseMat = misc.invertBacksteppingKernel(...
				obj.on(), obj(1).signOfIntegralTermInverse);
			valueInverseTemp = ...
				quantity.Discrete(valueInverseMat, obj(1).value(1).domain, "name", obj(1).name + "_I");
			for it = 1 : numel(obj)
				obj(it).valueInverse = valueInverseTemp(it);
			end
		end % setValueInverse()
		
		function kernelInverse = getValueInverse(obj)
			kernelInverse = reshape([obj.valueInverse], size(obj));			
		end % getValueInverse()
		
	end % methods
	
	methods (Access = protected, Hidden = true, Static = true)
		
		function NameValue = constructorInputParserHelper(NameValue)
			arguments
				NameValue.tolerance (1, 1) double = 1e-3;
				NameValue.iterationMargin = [3, 30];
			end
		end % constructorInputParser()
		
		function NameValue = constructorInputParser(varargin)
			validInputs = false(numel(varargin), 1);
			for it = 1 : 2 : numel(varargin)
				if any(strcmp(["tolerance", "iterationMargin"], varargin{it}))
					validInputs([it, (it+1)], 1) = true;
				end
			end %for it = 1 : 2 : numel(varargin)
			NameValue = kernel.Kernel.constructorInputParserHelper(varargin{validInputs});
		end % constructorInputParser()
		
	end % methods (Access = protected, Hidden = true, Static = true)
	
	methods (Abstract = true)
		result = updateBoundaryCondition(obj);
		result = rhsOfOde(obj);
		targetSystem = getTargetSystem(obj, system);
	end % methods
end % classdef
