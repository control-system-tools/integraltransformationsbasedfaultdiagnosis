classdef Characteristic < handle & matlab.mixin.Copyable
	% Characteristic is a class to solve hyperbolic PDEs on the domain (z, zeta) of
	% the kind
	%		Lambda(z) K_z(z, zeta) + K_zeta(z, zeta) Lambda(zeta) = f(z, zeta, K)
	% by using the method of characteristics. It assumed, that boundary conditions 
	% are defined such that the problem ist well-posed and the solution unique.
	% The method of characteristics is based on rewriting the equation with a
	% characteristic variable s = f1(z) = f2(zeta) such that
	%		K_s(z(s), zeta(s)) = Lambda(z) K_z(z, zeta) + K_zeta(z, zeta) Lambda(zeta).
	% By doing so, the PDE on (z, zeta) is reformulated as ODE on (s) along of the
	% characteristic lines (z(s), zeta(s)). This class initializes those lines.
	% The class kernel.Kernel allows to perform successive approximations
	% along these characteristic lines. The class kernel.kernel.Regular
	% implements the calculation of the backstepping kernel for nxn hyperbolic
	% systems with spatially varying coefficients and boundary actuation at z=1.
	
	properties (SetAccess = immutable)
		characteristic (1, 2) quantity.Discrete;
		Lambda (:, :) quantity.Discrete;
		rankPositiveLambda (1, 1) {mustBeNonnegative, mustBeInteger};
		rankNegativeLambda (1, 1) {mustBeNonnegative, mustBeInteger};
		rankLambda (1, 1) {mustBeNonnegative, mustBeInteger};
		sameLambda (:, 1);
		numberOfLines (1, 1) {mustBeNonnegative, mustBeInteger};
		maxStepSize (1, 1) double {mustBeNonnegative};
		stepSizeCharacteristic (1, 1) double {mustBeNonnegative};
	end % properties (SetAccess = immutable)
	
	properties (SetAccess = protected)
		referenceGrid (1, :) double;
	end % properties (SetAccess = protected)
	
	properties (SetAccess = private)
		lines (1,:) kernel.Line;
		lineOfSeparation kernel.LineOfSeparation;
		lineIcValues (2, :) double {mustBeNonnegative}; % contains the z and zeta values
													% where the characteristic start
		valueInterpolants (3, 1) numeric.ScatteredInterpolant; %1: z=zeta, 2: z=1, 3: zeta=0
		value (1, 1) quantity.Discrete;
		valueDiagonalDominant (1, 1) quantity.Discrete;
		identifyLinesOriginZ1 (1, :) logical;
		identifyLinesOriginZZeta (1, :) logical;
		identifyLinesOriginZeta0 (1, :) logical;
		
		isBcAtZeqZeta (1, 1) logical;
		isBcAtZetaEq0 (1, 1) logical;
		isBcAtZetaEq0Artificial (1, 1) logical;
		isBcAtZEq1 (1, 1) logical;
		isBcAtZEq1Artificial (1, 1) logical;
	end % properties (SetAccess = private)
	%
	properties (SetAccess = public)
		name (1, :) string;
		silent (1, 1) logical;
	end % properties (SetAccess = public)
	
	properties (Constant = true)
		bcIdentify = ["z = zeta"; "z = 1"; "zeta = 0"];
	end
	
	properties ( Access = protected, Hidden )
		dummy (1,1) logical = false;
	end
	
	methods
		function obj = Characteristic(Lambda, varargin)
			%% Characteristic constructur returns an array of 
			% kernel.Characteristic objects of the size of Lambda.
			
			if nargin > 0 % this if-clause is needed to support obj arrays
				NameValue = kernel.Characteristic.constructorInputParser(Lambda, varargin{:});
				
				%% Checks on Lambda
				assert(isdiag(Lambda) && issorted(diag2vec(Lambda), "descend"), ...
					"Lambda must be diagonal and its diagonal elements must be sorted descendingly.");
				sameLambda = kernel.Characteristic.findSameLambda(Lambda);
				[~, uniqueLambdaIdx, ~] = unique(sameLambda);
				uniqueLambda = Lambda(uniqueLambdaIdx, uniqueLambdaIdx);
				assert(issorted(diag2vec(uniqueLambda), "strictdescend"), ...
					"The diagonal elements of Lambda must be either identical on the whole " ...
					+ "domain or they must be distinct on the whole domain.");
				
				%% init-obj-Array and dimensions and set some parameters
				sizeLambda = size(Lambda);
				Lambda0 = reshape(Lambda.on(0), sizeLambda);
				rankPositiveLambdaTmp = sum(diag(Lambda0) > 0);
				obj(sizeLambda(1), sizeLambda(2)).rankPositiveLambda = rankPositiveLambdaTmp;
				[obj.rankPositiveLambda] = deal(rankPositiveLambdaTmp);
				[obj.rankLambda] = deal(sizeLambda(1));
				[obj.rankNegativeLambda] = deal(obj(1).rankLambda - obj(1).rankPositiveLambda);
				[obj.numberOfLines] = deal(NameValue.numberOfLines);
				[obj.maxStepSize] = deal(NameValue.maxStepSize);
				[obj.name] = deal(NameValue.name);
				[obj.sameLambda] = deal(sameLambda);
				[obj.silent] = deal(NameValue.silent);
				[obj.referenceGrid] = deal(NameValue.referenceGrid);
				[obj.dummy] = deal(NameValue.dummy);
				obj(1).Lambda = Lambda;
				
				if NameValue.dummy
					return
				end
				
				%% Set logical boundary flags isBcAtZeqZeta, isBcAtZetaEq0, ...
				% which indicate for each Characterstic array element if there is a
				% boundary condition at z=zeta or z=0 or zeta = 0 defined for this
				% element.
				obj.setLocationOfBoundaryCondition();
				
				%% set characteristic line s(z,zeta)
				% estimate bounds of domain of s
				%	s = 0 at initial / boundary condition 
				%	s_max <= | domainLength / min(|Lambda|) |
				LambdaAbsMin = -MAX(-abs(diag2vec(Lambda)));
				assert(LambdaAbsMin>0, "zero convection Lambda is not permitted");
				sGrid = linspace(-1, 1, 5 * numel(obj(1).referenceGrid)-1) / LambdaAbsMin;
				try
					if silent
						warning("off", "all");
					end
					solveThisOde = Lambda.diag2vec.solveDVariableEqualQuantity(...
									"variableGrid", sGrid);
					[~, id] = lastwarn(); 
					if contains(id, "symbolic:dsolve:")
						error("jump 2 catch");
					end
					warning("on", "all");
				catch
					LambdaDiscrete = quantity.Discrete(Lambda);
					solveThisOde = LambdaDiscrete.diag2vec.solveDVariableEqualQuantity(...
							"variableGrid", sGrid);
				end
				
				% set stepSizeCharacteristic and init all elements of obj-array
				for it = 1:size(obj, 1)
					for jt = 1:size(obj, 2)
						obj(it, jt).stepSizeCharacteristic = ...
							obj(it, jt).maxStepSize / sqrt(max(abs(Lambda(it, it).valueDiscrete))^2 ...
								+ max(abs(Lambda(jt, jt).valueDiscrete))^2);
						obj(it, jt).characteristic = solveThisOde([it, jt]);
					end
				end
				
				%% set lines
				obj.calculateLineOfSeparation();
				setLineIcValues(obj, obj(1).numberOfLines);
				progress = misc.ProgressBar('name', ...
					"### Set lines of characteristics of " + obj(1).name + ": ", ...				
					"steps", numel(obj), ...
					"terminalValue", numel(obj), ...
					"progress", 1, "silent", obj(1).silent, ...
					"printAbsolutProgress", false);
					
				progress.start(); if ~obj(1).silent; progress.show(); end
				
				useParallel = ~isempty(ver("parallel"));
				if useParallel
					dataQueue = parallel.pool.DataQueue;
					afterEach(dataQueue, @progress.raise);
				end
				
				for it = 1:numel(obj)
					obj(it).lines(obj(it).identifyLinesOriginZ1) = ...
						kernel.Line(obj(it).characteristic, ...
						"z = 1", obj(it).lineIcValues(2, obj(it).identifyLinesOriginZ1), ...
						obj(it).stepSizeCharacteristic);

					obj(it).lines(obj(it).identifyLinesOriginZZeta) = ...
						kernel.Line(obj(it).characteristic, ...
						"z = zeta", obj(it).lineIcValues(1, obj(it).identifyLinesOriginZZeta), ...
						obj(it).stepSizeCharacteristic);

					obj(it).lines(obj(it).identifyLinesOriginZeta0) = ...
						kernel.Line(obj(it).characteristic, ...
						"zeta = 0", obj(it).lineIcValues(1, obj(it).identifyLinesOriginZeta0), ...
						obj(it).stepSizeCharacteristic);
					
					if useParallel
						send(dataQueue,[]);
					else
						progress.raise();
					end
				end
				progress.stop();
				
				%% set interpolants
				obj.setValue();
			end
		end % Characteristic() constructor
		
		function plot2D(obj)
			% This method plots the lines of the characteristic in
			% subplots.
			figure();
			for it = 1 : numel(obj)
				[rowIdx, columnIdx] = ind2sub(size(obj), it);
				subplot(size(obj, 1), size(obj, 2), sub2ind(size(obj), columnIdx, rowIdx));
				hold on;
				%% draw boundary
				obj(1).plotTriangularDomain(0);

				%% draw lines
				for lineIdx = 1:numel(obj(it).lines)
					plot(obj(it).lines(lineIdx).z.valueDiscrete, ...
						obj(it).lines(lineIdx).zeta.valueDiscrete, "-+");
				end
				%% draw external
				if false
					for lineIdx = 1:numel(obj(it).lines)
						plot(obj(it).lines(lineIdx).zExternal, ...
							obj(it).lines(lineIdx).zetaExternal, "ko");
					end
				end

				%% draw line of separation
				if ~any(isempty(obj(it).lineOfSeparation))
					plot(obj(it).lineOfSeparation.z.valueDiscrete, ...
						obj(it).lineOfSeparation.zeta.valueDiscrete, "r", "linewidth", 2);
				end
				xlim([-0.05, 1.05]); ylim([-0.05, 1.05]);
				xlabel("$$z$$", "Interpreter","latex"); ylabel("$$\zeta$$", "Interpreter","latex");
				a = gca();
				a.TickLabelInterpreter = "latex";
 				title(obj(it).name + "$$_{" + num2str(rowIdx) + "," + num2str(columnIdx) + "}$$", ...
					"Interpreter", "latex");
			end
		end % plot()
		
		function plot(obj, varargin)
			% PLOT creates a 3d plot of characteristics.
			myParser = misc.Parser();
			myParser.addParameter("zLim", [-inf, inf]);
			myParser.addParameter("plotLines", true);
			myParser.addParameter("plotSurf", true);
			myParser.addParameter("plotExternal", false);
			myParser.addParameter("LineStyle", "none");
			myParser.addParameter("inverse", false);
			myParser.parse(varargin{:});
			misc.struct2ws(myParser.Results);
			% This method plots the lines of the characteristic in
			% subplots.
			if plotSurf
				gridSurf = obj(1).referenceGrid;
				if inverse
					KI = obj.getValueInverse();
					valuesInterpolated = KI.on({gridSurf, gridSurf}, {'z', 'zeta'});
					plotLines = false;
				else
					valuesInterpolated = obj.on({gridSurf, gridSurf});
				end
				% remove upper triangle which is outside of domain:
				valuesInterpolated = valuesInterpolated .* ...
					repmat((ones(numel(gridSurf)) ...	
					+ triu(ones(numel(gridSurf))+NaN, +1)), ...
						[1, 1, size(obj)]);
				[z1Grid, z2Grid] = ndgrid(gridSurf, gridSurf);
			end
			figure();
			for it = 1 : numel(obj)
				[rowIdx, columnIdx] = ind2sub(size(obj), it);
				subplot(size(obj, 1), size(obj, 2), sub2ind(size(obj), columnIdx, rowIdx));
				hold on;

				%% draw lines
				if plotLines
					for lineIdx = 1:numel(obj(it).lines)
						plot3(obj(it).lines(lineIdx).zDiscrete, ...
							obj(it).lines(lineIdx).zetaDiscrete, ...
							obj(it).lines(lineIdx).value, ...%, "Marker", "+", ...
							"-", "MarkerSize",2);%
					end
				end
				%% draw surf
				if plotSurf
					surf(z1Grid, z2Grid, valuesInterpolated(:, :, it), ...
						"LineStyle", LineStyle, "FaceAlpha", 1 / (plotSurf+plotLines));%
				end
				
				%% draw external
				if plotExternal
					for lineIdx = 1:numel(obj(it).lines)
						plot3(obj(it).lines(lineIdx).zExternal, ...
							obj(it).lines(lineIdx).zetaExternal, ...
							obj(it).lines(lineIdx).valueExternal, ...
							"ko");
					end
				end
				view(3);
				
				zlim(zLim);
				xlabel("$$z$$", "Interpreter", "latex"); ylabel("$$\zeta$$", "Interpreter","latex");
				a = gca();
				a.TickLabelInterpreter = "latex";
				if inverse
					title("$$" + obj(it).name + "_{I, " + num2str(rowIdx) + "," + num2str(columnIdx) + "}" + "$$", "Interpreter","latex");
				else
					title("$$" + obj(it).name + "_{" + num2str(rowIdx) + "," + num2str(columnIdx) + "}" + "$$", "Interpreter","latex");
				end
				
				%% plot domain
				obj(1).plotTriangularDomain(...
					[min([obj(it).lines(:).value]), max([obj(it).lines(:).value])]);
			end
		end % plot()
		
		function calculateLineOfSeparation(obj)
			%% Lines of separation are the characteristics that go through
			% the corner between two boundary conditions. Since these lines
			% separate two different domains, they are of major interest.
			for it = 1 : numel(obj)
				if obj(it).isBcAtZeqZeta
					if obj(it).isBcAtZetaEq0 || obj(it).isBcAtZetaEq0Artificial
					%% lines going through (z, zeta) = (0, 0)
						obj(it).lineOfSeparation = ...
							kernel.LineOfSeparation(obj(it).characteristic, ...
							"zeta = 0", 0, obj(it).stepSizeCharacteristic);

					elseif obj(it).isBcAtZEq1 || obj(it).isBcAtZEq1Artificial
					%% lines going through (z, zeta) = (1, 1)
						obj(it).lineOfSeparation = ...
							kernel.LineOfSeparation(obj(it).characteristic, ...
							"z = 1", 1, obj(it).stepSizeCharacteristic);
					end
				end
			end
		end % calculateLineOfSeparation()
		
		function setLineIcValues(obj, numberOfLines)
			% Characteristic lines start at some boundary where a boundary condition is defined.
			% Which ones, depends on the slope of the characteristic and is rather complicated to
			% explain here briefly, instead see:
			% Hu, L., Meglio, F. D., Vazquez, R., & Krstic, M. (2016).
			%		Control of homodirectional and general heterodirectional linear coupled
			%		hyperbolic PDEs.
			%		IEEE Transactions on Automatic Control, 61, 3301�3314
			[obj.lineIcValues] = deal(zeros(2, numberOfLines));
			[obj.identifyLinesOriginZZeta] = deal(zeros(1, numberOfLines));
			[obj.identifyLinesOriginZeta0] = deal(zeros(1, numberOfLines));
			[obj.identifyLinesOriginZ1] = deal(zeros(1, numberOfLines));
			% ic is a vector of flags that indicate which boundary
			% condition must be used.
			ic = reshape([[obj.isBcAtZeqZeta], ...
				[obj.isBcAtZetaEq0] + [obj.isBcAtZetaEq0Artificial], ...
				[obj.isBcAtZEq1] + [obj.isBcAtZEq1Artificial]], ...
				[numel(obj), 3]);
			for it = 1:numel(obj) % why does parfor not work here?
				if sum(ic(it, :)) == 1
					% if there is only one boundary condition, all points are
					% set at that boundary condition
					if ic(it, 1) %z=zeta
						obj(it).identifyLinesOriginZZeta(:) = true;
						obj(it).lineIcValues = [linspace(0, 1, numberOfLines); linspace(0, 1, numberOfLines)];
					elseif ic(it, 2) %zeta=0
						obj(it).identifyLinesOriginZeta0(:) = true;
						obj(it).lineIcValues = [linspace(0, 1, numberOfLines); zeros(1, numberOfLines)];
					elseif ic(it, 3) %z=1
						obj(it).identifyLinesOriginZ1(:) = true;
						obj(it).lineIcValues = [ones(1, numberOfLines); linspace(0, 1, numberOfLines)];
					end
				elseif sum(ic(it, :)) == 2
					% if there are two boundary conditions, then the points are
					% distributed over that two boundaries.
					halfNumberOfLines = floor(numberOfLines/2);
					otherHalfNumberOfLines = numberOfLines-halfNumberOfLines;
					if ic(it, 1) %z=zeta
						obj(it).identifyLinesOriginZZeta(1 : otherHalfNumberOfLines) = true;
						lineIcValues1 = [linspace(0, 1, otherHalfNumberOfLines); linspace(0, 1, otherHalfNumberOfLines)];
					end
					if ic(it, 2) %zeta=0
						obj(it).identifyLinesOriginZeta0(otherHalfNumberOfLines+1 : end) = true;
						lineIcValues2 = [linspace(0, 1, halfNumberOfLines); zeros(1, halfNumberOfLines)];
					elseif ic(it, 3) %z=1
						obj(it).identifyLinesOriginZ1(otherHalfNumberOfLines+1 : end) = true;
						lineIcValues2 = [ones(1, halfNumberOfLines); linspace(0, 1, halfNumberOfLines)];
					end
					obj(it).lineIcValues = [lineIcValues1, lineIcValues2];
				else
					error("setting the initial condition failed of a Characteristic line");
				end
			end
		end % setLineIcValues(obj, numberOfLines)
		
		function domain = pointBelongsToDomain(obj, z, zeta, dominantBc, outside2zero)
			%% domain will be set to 1, 2, or 3 to indicate
			% if the point (z, zeta) belongs to the domain that is spanned
			% by the characteristic lines that origin in
			%	"z = zeta"	<=> domain = 1
			%	"z = 1"		<=> domain = 2
			%	"zeta = 0"	<=> domain = 3
			% Afterwards, this can be evaluated easily by comparing to
			% bcIdentify = ["z = zeta"; "z = 1"; "zeta = 0"].
			arguments
				obj;
				z;
				zeta;
				dominantBc (1, 1) string = "zeta = 0";
				outside2zero (1, 1) = true;
			end % arguments
			
			if ~isequal(size(z), size(zeta))
				error("z and zeta must be of same size")
			end
			domain = zeros([numel(z), numel(obj)]);
			for it = 1:numel(obj)
				if obj(it).isBcAtZeqZeta
					domain(:, it) = 1;
					if obj(it).isBcAtZEq1 || obj(it).isBcAtZEq1Artificial
						zLineOfSeparation = ...
							obj(it).lineOfSeparation.zOfZeta.on({zeta(:)}, {'zeta'});
						
						if strcmp(dominantBc, "z = zeta")
							domain(z(:) > zLineOfSeparation, it) = 2;
							
						else
							domain(z(:) >= zLineOfSeparation, it) = 2;
						end
						
					elseif obj(it).isBcAtZetaEq0 || obj(it).isBcAtZetaEq0Artificial
						zetaLineOfSeparation = ...
							obj(it).lineOfSeparation.zetaOfZ.on({z(:)}, {'z'});
						
						if strcmp(dominantBc, "z = zeta")
							domain(zeta(:) < zetaLineOfSeparation, it) = 3;
						else
							domain(zeta(:) <= zetaLineOfSeparation, it) = 3;
						end
						
					end

				elseif obj(it).isBcAtZEq1 || obj(it).isBcAtZEq1Artificial
					domain(:, it) = 2;
					
				elseif obj(it).isBcAtZetaEq0 || obj(it).isBcAtZetaEq0Artificial
					domain(:, it) = 3;
				end
			end % for it = 1:numel(obj)
			if outside2zero
				domain((zeta(:)>z(:)) | (z(:)<0) | (zeta(:)<0) | (z(:)>1), :) = 0;
			end
			domain = reshape(domain, [size(z), size(obj)]);
		end % domain = pointBelongsToDomain(obj, z, zeta)
		
		function value = on(obj, myGrid, NameValue)
			% on() returns the value of the lines on the grid myGrid by 
			% evaluating the interpolantArray.
			arguments
				obj;
				myGrid cell = {obj(1).referenceGrid, obj(1).referenceGrid};
				NameValue.dominantBc (1, 1) string = "zeta = 0";
				NameValue.outside2zero (1, 1) = true;
			end
			
			value = zeros([numel(myGrid{1}), numel(myGrid{2}), size(obj)]);
			if ~isempty(obj)
				[zGrid, zetaGrid] = ndgrid(myGrid{:});
				interpolantArray = reshape(permute(...
					[obj.valueInterpolants], [2, 1]), [size(obj), 3]);
				boundaryNumber = obj.pointBelongsToDomain(zGrid, zetaGrid, ...
					NameValue.dominantBc, NameValue.outside2zero);
				for bcIdx = 1 : 3
					value = value + interpolantArray(:, :, bcIdx).evaluate(zGrid, zetaGrid) ...
						.* (boundaryNumber == bcIdx);
 				end
			end
		end % on()
		
		function setValue(obj, myGrid)
			% setValue reads the values of the lines and writes them to the
			% the valueInterpolants and to the value.
			arguments
				obj;
				myGrid = obj(1).referenceGrid;
			end % arguments
			
			obj = setValueInterpolants(obj);
			myDomain = [quantity.Domain("z", myGrid), quantity.Domain("zeta", myGrid)];
			tempValue = obj.on({myGrid, myGrid}, "outside2zero", false);
			tempValue = quantity.Discrete(tempValue, myDomain);
			for it = 1 : numel(obj)
				obj(it).value = tempValue(it);
			end
		end % setValue()
		
		function setValueDiagonalDominant(obj, myGrid)
			% setValue reads the values of the lines and writes them to the
			% the valueInterpolants and to the value.
			arguments
				obj;
				myGrid = obj(1).referenceGrid;
			end % arguments
			
			obj = setValueInterpolants(obj);
			myDomain = [quantity.Domain("z", myGrid), quantity.Domain("zeta", myGrid)];
			tempValue = obj.on({myGrid, myGrid}, "dominantBc", 'z = zeta', "outside2zero", false);
			tempValue = quantity.Discrete(tempValue, myDomain);
			for it = 1 : numel(obj)
				obj(it).valueDiagonalDominant = tempValue(it);
			end
		end % setValue()
		
		function value = getValue(obj, NameValue)
			% get the value of the kernel on the spatial domain.
			arguments
				obj;
				NameValue.dominantBc (1, 1) string = "zeta = 0";
				NameValue.name (1, 1) string = obj(1).name
			end % arguments
			if strcmp(NameValue.dominantBc, "z = zeta")
				value = reshape([obj.valueDiagonalDominant], size(obj));
			else
				value = reshape([obj.value], size(obj));
			end
			value = value.setName(NameValue.name);
		end % getValue()
		
		function [obj_dZ, obj_dZeta] = getDerivatives(obj, myGrid, NameValue)
			% getDerivatives calculates the derivatives of the kernel value
			% numerically. As each sub-domain (w.r.t. boundaries) is
			% derived separatly, the result is better, than the
			% derivative obtained from diff(obj(1).value).
			% on() returns the value of the lines on the grid myGrid by 
			% evaluating the interpolantArray.
			arguments
				obj;
				myGrid cell = {obj(1).referenceGrid, obj(1).referenceGrid};
				NameValue.dominantBc (1, 1) string = "zeta = 0";
				NameValue.outside2zero (1, 1) = true;
			end
			
			valueDZ = zeros([cellfun(@numel, myGrid), size(obj)]);
			valueDZeta = zeros([cellfun(@numel, myGrid), size(obj)]);
			if ~isempty(obj)
				interpolantArray = reshape(permute(...
					[obj.valueInterpolants], [2, 1]), [size(obj), 3]);
				[zGrid, zetaGrid] = ndgrid(myGrid{1}, myGrid{2});
			
				boundaryNumber = obj.pointBelongsToDomain(zGrid, zetaGrid, ...
					NameValue.dominantBc, NameValue.outside2zero);
				for bcIdx = 1 : 3
					temp.value = interpolantArray(:, :, bcIdx).evaluate(zGrid, zetaGrid);
					if numel(obj) > 1
						[temp.dzeta, temp.dz] = gradient(temp.value, ...
							myGrid{1}, myGrid{2}, 1:size(obj, 1), 1:size(obj, 2));
					else
						[temp.dzeta, temp.dz] = gradient(temp.value, ...
							myGrid{1}, myGrid{2});
					end
					valueDZ = valueDZ + temp.dz	.* (boundaryNumber == bcIdx);
					valueDZeta = valueDZeta + temp.dzeta .* (boundaryNumber == bcIdx);
				end
			end
			myDomain = [quantity.Domain("z", myGrid{1}), quantity.Domain("zeta", myGrid{2})];
 			obj_dZ = quantity.Discrete(valueDZ, myDomain, "name", obj(1).name + "_{z}");
 			obj_dZeta = quantity.Discrete(valueDZeta, myDomain, "name", obj(1).name + "_{zeta}");
		end % getDerivatives()
	end % methods
			
	methods (Access = public)
		function [h, hashData] = hash(obj)
			% HASH compute a hash value for this object
			% h = hash(obj) computes a hash value for this object based on
			% the parameters:
			% * number of lines
			% * maxStepSize
			% * referenceGrid
			hashData = {obj(1).numberOfLines, ...
					   obj(1).maxStepSize, ...
					   obj(1).referenceGrid};
			h = misc.hash(hashData);
			
		end
	end
	
	methods (Access = private)
		function obj = setValueInterpolants(obj)
			%% in this function the property valueInterpolants is set
			% by using the values in the lines
			for it = 1 : numel(obj) % don't use parfor, it causes empty interpolants!
				if obj(it).isBcAtZeqZeta
					obj(it).valueInterpolants(1) = numeric.ScatteredInterpolant(...
						vertcat(obj(it).lines(obj(it).identifyLinesOriginZZeta).zDiscrete), ...
						vertcat(obj(it).lines(obj(it).identifyLinesOriginZZeta).zetaDiscrete), ...
						horzcat(obj(it).lines(obj(it).identifyLinesOriginZZeta).value), ...
						'ExtrapolationMethod', 'nearest', 'Method', 'linear'); 
					% Caution:	('Method', 'natural') or 
					%			('ExtrapolationMethod', 'linear') 
					%			might cause NaN-results.
				end

				if obj(it).isBcAtZEq1 || obj(it).isBcAtZEq1Artificial
					obj(it).valueInterpolants(2) = numeric.ScatteredInterpolant(...
						vertcat(obj(it).lines(obj(it).identifyLinesOriginZ1).zDiscrete), ...
						vertcat(obj(it).lines(obj(it).identifyLinesOriginZ1).zetaDiscrete), ...
						horzcat(obj(it).lines(obj(it).identifyLinesOriginZ1).value), ...
						'ExtrapolationMethod', 'nearest', 'Method', 'linear');
				end

				if obj(it).isBcAtZetaEq0 || obj(it).isBcAtZetaEq0Artificial
					obj(it).valueInterpolants(3) = numeric.ScatteredInterpolant(...
						vertcat(obj(it).lines(obj(it).identifyLinesOriginZeta0).zDiscrete), ...
						vertcat(obj(it).lines(obj(it).identifyLinesOriginZeta0).zetaDiscrete), ...
						horzcat(obj(it).lines(obj(it).identifyLinesOriginZeta0).value), ...
						'ExtrapolationMethod', 'nearest', 'Method', 'linear');
				end
			end % for it = 1 : numel(obj)
		end % setValueInterpolants()
		
	end %methods (Access = private)
	
	methods (Access = protected, Hidden = true)
		function setLocationOfBoundaryCondition(obj)
			% setLocationOfBoundaryCondition sets the logical indices
			%	isBcAtZeqZeta, isBcAtZetaEq0, isBcAtZetaEq0Artificial, isBcAtZEq1, 
			%	isBcAtZEq1Artificial
			% which define for each element of a Characteristic array if a boundary
			% condition has to be defined on this element (true), or if no boundary
			% condition must be defined on this element (false). This follows
			% directly by investigating the direction of the characteristic lines
			% through the domain (z, zeta), which is set by the value of Lambda.
			
			for rowIdx = 1:obj(1).rankLambda
				for columnIdx = 1:obj(1).rankLambda
					obj(rowIdx, columnIdx).isBcAtZeqZeta = ...
						obj(1).sameLambda(rowIdx) ~= obj(1).sameLambda(columnIdx);
					
					obj(rowIdx, columnIdx).isBcAtZetaEq0 = ...
						(rowIdx<=columnIdx) && (columnIdx<=obj(1).rankPositiveLambda);
					
					obj(rowIdx, columnIdx).isBcAtZetaEq0Artificial = ...
						(columnIdx>=obj(1).rankPositiveLambda+1) && (columnIdx<=rowIdx);
					
					obj(rowIdx, columnIdx).isBcAtZEq1 = false;
					
					obj(rowIdx, columnIdx).isBcAtZEq1Artificial = ...
						((rowIdx<=obj(1).rankPositiveLambda) && (columnIdx<rowIdx)) ...
							|| ((rowIdx>=obj(1).rankPositiveLambda+1) && (rowIdx<columnIdx));
				end
			end
		end % function setLocationOfBoundaryCondition(obj)
	end % methods (Access = private, Hidden = true)
	
	methods (Static = true)
		function sameLambda = findSameLambda(Lambda)
			% findSameLambda determine which diagonal values of Lambda ar identical.
			% For instance, if 
			%	Lambda = diag([3, 2, 2, -1, -1, -1.5]),
			% then 
			%	isotachic = [1; 2; 2; 3; 3; 4].
			
			n = size(Lambda, 1);
			sameLambda = ones(n, 1);
			runner = 1;
			for it = 2:n
				if MAX(abs(Lambda(it-1, it-1) - Lambda(it, it))) >= 1e-3
					runner = runner + 1;
				end
				sameLambda(it) = runner;
			end
		end % findSameLambda()
	end % methods (Static = true)
	
	methods (Access = protected, Hidden = true, Static = true)
		function plotTriangularDomain(z)
			z = unique(z);
			% used in plot2D() and plot()
			for it = 1:numel(z)
				plot3([0, 1], [0, 1], [z(it), z(it)], "k", "linewidth", 0.5);
				plot3([0, 1], [0, 0], [z(it), z(it)], "k", "linewidth", 0.5);
				plot3([1, 1], [0, 1], [z(it), z(it)], "k", "linewidth", 0.5);
			end
			if numel(z) > 1
				plot3([1, 1], [1, 1], [min(z), max(z)], "k", "linewidth", 0.5, "LineStyle", "--");
				plot3([1, 1], [0, 0], [min(z), max(z)], "k", "linewidth", 0.5, "LineStyle", "--");
				plot3([0, 0], [0, 0], [min(z), max(z)], "k", "linewidth", 0.5, "LineStyle", "--");
			end
		end %function plotTriangularDomain(z)
		
		function NameValue = constructorInputParserHelper(Lambda, referenceGrid, NameValue)
			arguments
				Lambda quantity.Discrete;
				referenceGrid (1, :) double;
				NameValue.numberOfLines (1, 1) double = ceil(numel(referenceGrid));
				NameValue.maxStepSize (1, 1) double = diff(referenceGrid([1, 2]))/2;
				NameValue.name (1, 1) string = "";
				NameValue.silent (1, 1) logical = false;
				NameValue.dummy (1, 1) logical = false;
			end
			NameValue.referenceGrid = referenceGrid;
		end % constructorInputParser()
		
		function NameValue = constructorInputParser(Lambda, varargin)
			validInputs = false(numel(varargin), 1);
			referenceGrid = Lambda(1).domain(1).grid;
			for it = 1 : 2 : numel(varargin)
				if any(strcmp("referenceGrid", varargin{it}))
					referenceGrid = varargin{it+1};
				elseif any(strcmp(["numberOfLines", "maxStepSize", "name", "silent", "dummy"], varargin{it}))
					validInputs([it, (it+1)], 1) = true;
				end
			end %for it = 1 : 2 : numel(varargin)
			NameValue = kernel.Characteristic.constructorInputParserHelper(...
				Lambda, referenceGrid, varargin{validInputs});
		end % constructorInputParser()
		
	end % methods (Access = protected, Hidden = true, Static = true)
	
end % classdef kernel.Characteristic
