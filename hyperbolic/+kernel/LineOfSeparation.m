classdef LineOfSeparation < kernel.Line
	% LineOfSeparation is a child of kernel.Line with the additional
	% properties zOfZeta and zetaOfZ that describe the relation of z and zeta along
	% this line.
	
	properties (SetAccess = immutable)
		zOfZeta quantity.Discrete;
		zetaOfZ quantity.Discrete;
	end % properties
	
	methods
		
		function obj = LineOfSeparation(varargin)
			obj@kernel.Line(varargin{:})
			%% Constructor
			if nargin > 0 % this if-clause is needed to support
				% LineOfSeparation-Array. Whenever a new obj(it) is
				% initialized, the constructor is called without
				% input-arguments. This would lead to errors, that are
				% avoided by this condition.
				obj.zOfZeta = quantity.Discrete(obj(1).zDiscrete, ...
					quantity.Domain("zeta", obj(1).zetaDiscrete), "name", "z(zeta)");
				obj.zetaOfZ = quantity.Discrete(obj(1).zetaDiscrete, ...
					quantity.Domain("z", obj(1).zDiscrete), "name", "zeta(z)");
			end
		end % Line() constructor
		
	end % methods
	
end % classdef
