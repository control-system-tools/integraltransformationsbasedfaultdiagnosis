classdef Feedback < kernel.Kernel
%kernel.Feedback is written to calculate the solution 
% K(z, zeta) of the kernel equations:
% PDE:	Lambda(z) K_z(z, zeta) + (K(z, zeta) Lambda(zeta))_zeta ...
%			= K(z, zeta) A(zeta) - F(z, zeta) ...
%				+ int_zeta^z K(z, eta) F(eta, zeta) deta
% BC:	K(z, z) Lambda(z) - Lambda(z) K(z, z) = A(z)
% BC:	K(z, 0) Lambda(0)(E1 + E2 Q0) = A0Tilde(z) -backsteppingTransformation[A0](z)
% Therin, A, F, A0, Q0 und Lambda are parameter of the original system. A0Tilde is a
% resulting parameter of the target system.
% It is assumed that Lambda is diagonal and sorted descending and that the diagonal 
% elements of A are zero.
%
% See also model.Transport

	properties (SetAccess = private)
		A quantity.Discrete;
		F quantity.Discrete;
		A0 quantity.Discrete;
		Q0 (:, :) double;
		E1 (:, :) double;
		E2 (:, :) double;
		Lambda_z (:, :);
	end % properties (SetAccess = private)
	
	properties (SetAccess = protected)
		signOfIntegralTerm = -1;
		signOfIntegralTermInverse = +1;
		integralBounds = {0, "z"};
		valueInverse;
		optionBcZeta0 string {mustBeMember(optionBcZeta0, ["constant", "2x2"])} = "constant";
		sampledQuantities (1, 1) struct;
	end % properties (SetAccess = protected)
	
	properties (Hidden = true)
		preparedBcZZeta;	% is used in  boundaryConditionZZeta
	end % properties (Hidden = true)
	
	
	methods
		function obj = Feedback(varargin)
			obj@kernel.Kernel(varargin{:});
			%% Constructor for a backstepping kernel
			% Object for the computation of a backstepping kernel for a
			% system of the form
			% PDE		x_t(z,t) = Lambda(z) x_z(z,t) + A(z) x(z,t) + A0(z) x_1(0,t)
			%					  + int_a^b F(z,zeta) x(zeta,t) dzeta + B(z) u(t)
			%					  + ode2pde.B(z) w(t)
			% ODE		  w_t(t) = ode.A w(t) + ode.B pde2ode[x] + odeInput u(t) 
			% BC z0		x_2(0,t) = Q0 x_1(0,t) + B0 u(t) + ode2pde.B0 w(t) 
			% BC z1		x_1(1,t) = Q1 x_2(1,t) + B1 u(t) + ode2pde.B1 w(t) 
			%
			% obj = kernel.Kernel(Lambda, name-value-pairs) creates the object
			% for the solution of the kernel equations
			%
			% PDE:	Lambda(z) K_z(z, zeta) + (K(z, zeta) Lambda(zeta))_zeta ...
			%			= K(z, zeta) A(zeta) - F(z, zeta) ...
			%				+ int_zeta^z K(z, eta) F(eta, zeta) deta
			% BC:	K(z, z) Lambda(z) - Lambda(z) K(z, z) = A(z)
			% BC:	K(z, 0) Lambda(0)(E1 + E2 Q0) = A0Tilde(z) -backsteppingTransformation[A0](z)
			%
			% with the name-value-pairs the system parameters "A", "F",
			% "A0", "Q0" can be set. For the backstepping transformation 
			% the further parameters ... #todo@Jakob explain the missing
			% parameters
			%
			%
			if nargin > 0 % this if-clause is needed to support Characteristic-Arrays.
				% Whenever a new obj(it) is initialized, the constructor is called without
				% input-arguments. This would lead to errors, that are avoided by this condition.
				if numel(varargin) > 1
					obj = obj.constructorParser(varargin{:});
					
					if ~obj(1).dummy
						
						[obj.E1] = deal(eye(obj(1).rankLambda, obj(1).rankPositiveLambda));
						[obj.E2] = deal([zeros(obj(1).rankPositiveLambda, obj(1).rankNegativeLambda); ...
							eye(obj(1).rankNegativeLambda)]);
						
						assert(isequal(...
							abs([obj(1).signOfIntegralTerm, obj(1).signOfIntegralTermInverse]), ...
							[1, 1]), "signOfIntegralTerm and signOfIntegralTermInverse must be +-1");
						assert(isequal(size(obj(1).A), [obj(1).rankLambda, obj(1).rankLambda]), ...
							"A has wrong size");
						sameLambdaSelector = obj(1).sameLambda == obj(1).sameLambda.';
						assert(MAX(abs(obj(1).A(sameLambdaSelector))) <= 1e-12, ...
							"The elements of A = [a_ij] must be zero for i, j such that " ...
							+ "lambda_i = lambda_j with Lambda = diag(lambda_1, ... lambda_n).");
						assert(isequal(size(obj(1).F), [obj(1).rankLambda, obj(1).rankLambda]), ...
							"F has wrong size");
						assert(isequal(size(obj(1).A0), [obj(1).rankLambda, obj(1).rankPositiveLambda]), ...
							"A0 has wrong size");
						assert(isequal(size(obj(1).Q0), [obj(1).rankNegativeLambda, obj(1).rankPositiveLambda]), ...
							"Q0 has wrong size");
						
						obj = obj.setPreparedBcZZeta();
						obj.setSampledQuantities();
						obj.successiveApproximation("silent", obj(1).silent);
						obj.setValueInverse();
					end % if ~obj(1).dummy
					
				end % if numel(varargin) > 1
			end % if nargin > 0
		end % kernel.Feedback constructor
		
	end % methods
	
	methods (Access = public)
		
		function [h, hashData] = hash(obj)
			% HASH compute a hash value for this object
			% h = hash(obj) computes a hash value for this object based on
			% the parameters:		
			% * Lambda, A, F, A0, Q0
			% * optionBcZeta0
			
			[~, kernelData] = hash@kernel.Kernel(obj);
			
			hashData = [kernelData, ...
						   obj(1).A.hash, ...
						   obj(1).Lambda.hash, ...
						   obj(1).F.hash, ...
						   obj(1).A0.hash, ...
						   obj(1).Q0];
			
			h = misc.hash(hashData);
								
		end
		
		function [result] = rhsOfOde(obj)
			% rhsOfOde calculates the right-hand side of the PDE
			% Lambda(z) K_z(z, zeta) + (K(z, zeta) Lambda(zeta))_zeta ...
			%			= K(z, zeta) A(zeta) - F(z, zeta) ...
			%				+ int_zeta^z K(z, eta) F(eta, zeta) deta
			% but reformulated as
			% Lambda(z) K_z(z, zeta) + K_zeta(z, zeta) Lambda(zeta) ...
			%			= K(z, zeta) (A(zeta) Lambda_zeta(zeta)) - F(z, zeta) ...
			%				+ int_zeta^z K(z, eta) F(eta, zeta) deta
			% This is used in the successive approximations. To speed up the calculations the
			% terms that do not depend on K are sampled and calculated in advance in 
			% setSampledQuantities
			% See also kernel.Kernel.successiveApproximation, kernel.Feedback.setSampledQuantities
			
			K = obj.getValue();
			integrand = K.subs("zeta", "eta") * obj(1).sampledQuantities.FofEta;
			result = K * obj(1).sampledQuantities.AminusLambda_zOfZeta ...
				- (obj(1).sampledQuantities.F ...
				+ obj(1).signOfIntegralTerm * int(integrand, "eta", "zeta", "z"));
		end % rhsOfOde
		
		function updateBoundaryCondition(obj)
			% calculate values of kernel at boundaries
			zeta0Boundary = boundaryConditionZeta0(obj);
			zZetaBoundary = boundaryConditionZZeta(obj);
			
			% set boundary values at all lines of characteristics as initial
			% condition
			for k = 1:numel(obj)
				if obj(k).isBcAtZEq1Artificial
					% continous to non-artifical bc
						obj(k).lines(obj(k).identifyLinesOriginZ1).setIcValue(...
							zZetaBoundary(k).at(1));
				end

				if obj(k).isBcAtZetaEq0Artificial
					% continous to non-artifical bc or equal zero
					if strcmp(obj(k).optionBcZeta0, "constant")
						if obj(k).isBcAtZeqZeta
							obj(k).lines(obj(k).identifyLinesOriginZeta0).setIcValue(...
								zZetaBoundary(k).at(0));
						else
							obj(k).lines(obj(k).identifyLinesOriginZeta0).setIcValue(0);
						end
					elseif strcmp(obj(k).optionBcZeta0, "2x2")
						[it, jt] = ind2sub(size(obj(1).Lambda), k);
						obj(k).lines(obj(k).identifyLinesOriginZeta0).setIcValue(...
							zeta0Boundary(it, jt));
					end
				end

				if obj(k).isBcAtZeqZeta
					obj(k).lines(obj(k).identifyLinesOriginZZeta).setIcValue(...
						zZetaBoundary(k));
				end
				
				if obj(k).isBcAtZetaEq0
					[it, jt] = ind2sub(size(obj(1).Lambda), k);
					obj(k).lines(obj(k).identifyLinesOriginZeta0).setIcValue(...
						zeta0Boundary(it, jt));
				end
			end % for it = 1:numel(obj)
			obj.setValue();
		end % updateBoundaryCondition()
		
		function result = boundaryConditionZeta0(obj)
			% Are the values at the boundary needed for setting the boundary
			% conditions, i.e. result = blkdiag(K_11, K_22) in which 
			%	K(z, 0) = [K_11, K_12; K_21, K_22](z, 0),
			%	K_11(z, 0) = E1^T * K(z, 0) * E1, 
			%	K_12(z, 0) = E1^T * K(z, 0) * E2, 
			%	...
			% Those values are calculated via
			%	K_11 = (R_1(z) - K_12(z, 0) * Lambda_2(0) * Q0 ) / Lambda_1(0),
			%	K_22 = (R_2(z) - K_21(z, 0) * Lambda_1(0) ) / Lambda_2(0) * pinv(Q0)
			% wherein
			%	R_1(z) = E1^T * R(z),
			%	R_2(z) = E2^T * R(z),
			%	R(z) = - Tc[A0](z),
			%	Lambda_1(0) = E1^T * Lambda(0),
			%	Lambda_2(0) = E2^T * Lambda(0).
			
			% result = (- K(z, 0) Lambda(0) E2 Q0 - Tc[A0](z)
			%				/ Lambda(0)
			assert(numel(obj) == obj(1).rankLambda^2, "The BC must be set for the complete kernel at once.");
			
			if obj(1).rankNegativeLambda == 0
				result = - obj.backsteppingTransformation(obj(1).A0);
			elseif obj(1).rankPositiveLambda == 0
				result = [];
			else
				K = subs(obj.getValue(), "zeta", 0);
				K_12 = K(1:obj(1).rankPositiveLambda, (obj(1).rankPositiveLambda+1) : end);
				K_21 = K((obj(1).rankPositiveLambda+1) : end, 1:obj(1).rankPositiveLambda);
				Lambda0 = obj(1).Lambda.at(0);
				Lambda0_1 = Lambda0(1:obj(1).rankPositiveLambda, 1:obj(1).rankPositiveLambda);
				Lambda0_2 = Lambda0(...
					(obj(1).rankPositiveLambda+1) : end, (obj(1).rankPositiveLambda+1) : end);
				R = - obj.backsteppingTransformation(obj(1).A0);
				R_1 = R(1:obj(1).rankPositiveLambda, :);
				R_2 = R((obj(1).rankPositiveLambda+1) : end, :);
				
				K_11 = (R_1 - K_12 * Lambda0_2 * obj(1).Q0 ) / Lambda0_1;
				K_22 = ((R_2 - K_21 * Lambda0_1 ) / Lambda0_2) * pinv(obj(1).Q0);
				
				result = blkdiag(K_11, K_22);
			end
		end % boundaryConditionZeta0()
				
		function result = boundaryConditionZZeta(obj)
			result = obj(1).preparedBcZZeta;
		end % boundaryConditionZZeta()
		
		function A0Target = getA0Target(obj)
			% Calculate local coupling term of target system A0Target, byg
			% using the boundary condition of the kernel equations at (z,0)
			K = obj.getValue();
			KZeta0 = K.subs("zeta", 0);
			Lambda0 = obj(1).Lambda.at(0);
			if obj(1).rankPositiveLambda == 0
				A0Target = zeros(obj(1).rankLambda, obj(1).rankPositiveLambda);
			elseif obj(1).rankNegativeLambda == 0
				A0Target = (KZeta0 * Lambda0 + obj.backsteppingTransformation(obj(1).A0));				
			else
				A0Target = KZeta0 * Lambda0 * (obj(1).E1 + obj(1).E2 * obj(1).Q0) ...
					+ obj.backsteppingTransformation(obj(1).A0);
			end
			A0Target.setName("\tilde{A}_{0}");
		end % getA0Target()
		
		function targetSystem = getTargetSystem(obj, originalSystem, Q1)
			% getTargetSystem computes the backstepping target system.
			%
			%	targetSystem = getTargetSystem(obj, originalSystem) get the backstepping target
			%		system targetSystem based on the original system originalSystem, by exchanging 
			%		the parameters:
			%			- A, F, Q1 are set zero.
			%			- A0 is set to obj.getA0Target().
			%			- The inverse backstepping transformation is applied to all outputs
			%				originalSystem.output.
			%			- The backstepping transformation is considered in all input gains.
			%
			%	targetSystem = getTargetSystem(obj, originalSystem, Q1) allows to specify Q1 values
			%		in the target system that are different to zero.
			%		By default, Q1 is zero, hence it is fully compensated. In case of problems 
			%		w.r.t. large contol input signals or delay robustness, try not to fully
			%		compensate Q1, i.e. use c*originalSystem.Q1 as input parameter, with 
			%		c \in (0, 1).
			arguments
				obj;
				originalSystem model.Transport;
				Q1 (:, :) double = zeros(size(obj(1).Q0.'));
			end
			K = obj.getValue();
			targetData.prefix = originalSystem.prefix;
			targetData.A0 = obj.getA0Target();
			targetData.Q0 = originalSystem.Q0;
			targetData.Q1 = Q1;
			if isprop(originalSystem, "ode")
				targetData.ode = originalSystem.ode;
				targetData.odeInput = originalSystem.odeInput;
			end
			targetData.output = originalSystem.output.backstepping(obj, "inverse", true);
			targetData.input = originalSystem.input.copy();
			
			for it = 1 : targetData.input.numTypes
				if ~isempty(targetData.input.input(it).B) ...
						&& ~isempty(targetData.input.input(it).B0)
					targetData.input.input(it).B = ...
						obj.backsteppingTransformation(targetData.input.input(it).B)...
						+ K.subs("zeta", 0) * originalSystem.Lambda.at(0)...
							* obj(1).E2 * targetData.input.input(it).B0;

				elseif isempty(targetData.input.input(it).B) ...
						&& ~isempty(targetData.input.input(it).B0)
					targetData.input.input(it).B = K.subs("zeta", 0) * originalSystem.Lambda.at(0)...
						* obj(1).E2 * targetData.input.input(it).B0;

				elseif ~isempty(targetData.input.input(it).B) ...
						&& isempty(targetData.input.input(it).B0)
					targetData.input.input(it).B = ...
						obj.backsteppingTransformation(targetData.input.input(it).B);
				end
			end
			targetDataNameValue = misc.struct2namevaluepair(targetData);
			
			% call constructor
			targetSystem = misc.constructorOf(...
				originalSystem, originalSystem.Lambda, targetDataNameValue{:});
		end % getTargetSystem()
		
		function feedback = getFeedback(obj, Q1, NameValue)
			% getFeedback calculates the state feedback that ensures that the original system has
			% the same behaviour as the backstepping target system.
			%
			%	feedback = getFeedback(obj, Q1) creates a model.Output object feedback with the type
			%		"control", that implements the backstepping feedback. Q1 is the boundary
			%		coupling (sometimes called proximal reflection)
			%			x_1(1,t) = Q1 x_2(1,t)
			%		present in the original system which is fully compensated.
			%
			%	feedback = getFeedback(obj, Q1, Q1target) allows to leave some boundary coupling
			%		also in the target system of the kind
			%			x_1(1,t) = Q1target x_2(1,t).
			%		Note that the parameter Q1target should also be considered in the method
			%		getTargetSystem().
			%
			%	feedback = getFeedback(obj, Q1, Q1target, type) allows the specify the type of the
			%		feedback as a string input. Note, for simulations, the feedback type should fit
			%		to the plants control input type, for instance to one of
			%		systemAntidiagonal.input.type.
			arguments
				obj;
				Q1 (:, :) double;
				NameValue.Q1target (:, :) double = Q1*0;
				NameValue.type (1, 1) string = "control";
			end
			feedback = model.Output(NameValue.type, ...
				"C1", (NameValue.Q1target - Q1) * obj(1).E2.', ...
				"C", subs((obj(1).E1.' - NameValue.Q1target * obj(1).E2.') * obj.getValue(), ...
								["z", "zeta"], 1, "z"), ...
				"input", misc.Gain("input", eye(size(Q1, 1)), "outputType", NameValue.type));
		end % getFeedback()
		
	end % methods
	
	methods (Access = private, Hidden = true)
		
		function obj = setSampledQuantities(obj)
			% setSampledQuantities samples the quantities used in the successive approximations to
			% the reference grid and stores them in sampledQuantities in order to speed up
			% rhsOfOde(obj)
			
			z = quantity.Domain("z", obj(1).referenceGrid);
			zeta = z.rename("zeta");
			sampledQuantities.F = obj(1).F.changeDomain([z, zeta]);
			sampledQuantities.FofEta = sampledQuantities.F.subs("z", "eta");
			sampledQuantities.AminusLambda_zOfZeta = ...
				subs(obj(1).A - obj(1).Lambda_z, "z", "zeta").changeDomain(zeta);
			
			obj(1).sampledQuantities = sampledQuantities;
			
		end % setSampledQuantities
		
		function obj = setPreparedBcZZeta(obj)
			% setPreparedBcZZeta calculates the hidden property preparedBcZZeta, which is used in 
			% boundaryConditionZZeta (which is used in updateBoundaryCondition.
			
			preparedBcZZeta = quantity.Discrete.zeros(size(obj), obj(1).Lambda(1).domain);
			for it = 1 : obj(1).rankLambda
				for jt = 1 : obj(1).rankLambda
					if obj(1).sameLambda(it) ~= obj(1).sameLambda(jt)
						% this boundary condition is not defined for lambda_it = lambda_jt,
						% hence the resulting elements of preparedBcZZeta remain zero.
						preparedBcZZeta(it, jt) = obj(1).A(it, jt) / ...
							(obj(1).Lambda(jt, jt) - obj(1).Lambda(it, it));
					end
				end
			end
			[obj.preparedBcZZeta] = deal(preparedBcZZeta);
		end % setPreparedBcZZeta()
		
		function obj = constructorParser(obj, varargin)
			
			validArgs = ["A", "F", "A0", "Q0", "signOfIntegralTerm", ...
				"signOfIntegralTermInverse", "Lambda_z", "optionBcZeta0"];
			validInputs = false(numel(varargin), 1);
			for it = 2 : 2 : numel(varargin)
				if any(strcmp(validArgs, varargin{it}))
					validInputs([it, (it+1)], 1) = true;
				end
			end %for it = 2 : 2 : numel(varargin)
			
			z = quantity.Domain("z", obj(1).referenceGrid);
			zeta = z.rename("zeta");
			NameValue = kernel.Feedback.constructorParserHelper(...
				obj(1).rankLambda, obj(1).rankPositiveLambda, obj(1).rankNegativeLambda, ...
				z, zeta, ...
				obj(1).Lambda, varargin{validInputs});
			
			[obj.A] = deal(NameValue.A);
			[obj.F] = deal(NameValue.F);
			[obj.A0] = deal(NameValue.A0);
			[obj.Q0] = deal(NameValue.Q0);
			[obj.signOfIntegralTerm] = deal(NameValue.signOfIntegralTerm);
			[obj.signOfIntegralTermInverse] = deal(NameValue.signOfIntegralTermInverse);
			[obj.Lambda_z] = deal(NameValue.Lambda_z);
			[obj.optionBcZeta0] = deal(NameValue.optionBcZeta0);

		end % constructorParser()
		
	end % methods (Access = private, Hidden = true)
	
	methods (Access = protected, Hidden = true, Static = true)
		
		function NameValue = constructorParserHelper(n, p, m, z, zeta, Lambda, NameValue)
			arguments
				n;
				p;
				m;
				z;
				zeta;
				Lambda;
				NameValue.A = quantity.Discrete.zeros([n, n], z, "name", "A");
				NameValue.F = quantity.Discrete.zeros([n, n], [z, zeta], "name", "F");
				NameValue.A0 = quantity.Discrete.zeros([n, p], z, "name", "A0");
				NameValue.Q0 = zeros([m, p]);
				NameValue.signOfIntegralTerm (1, 1) double = -1;
				NameValue.signOfIntegralTermInverse (1, 1) double = +1;
				NameValue.Lambda_z = Lambda.diff("z", 1);
				NameValue.optionBcZeta0 = "constant";
			end
		end % constructorParserHelper()
		
	end % methods (Access = protected, Hidden = true, Static = true)
	
	methods (Static)
		function [K, loaded] = tryToLoadKernel(varargin)
			
			if nargin == 1 && isa( varargin{1}, "kernel.Feedback")
				% get the hash-value for a kernel object
				kernelHash = varargin{1}.hash();
			else
				% generate a dummy-kernel-object:
				dummyKernel = kernel.Feedback(varargin{:}, "dummy", true);
				kernelHash = dummyKernel.hash();
			end

			% check if the directoy exist or create it.
			% the return parameter must be set like this, otherwise the
			% function will throw a warning if the path already exists.
			[~,~,~] = mkdir(kernel.Kernel.savedKernelPath);

			% build the filename for the saved kernel
			savedKernelFileName = fullfile(kernel.Kernel.savedKernelPath, ...
				kernelHash + ".mat");
			
			if exist(savedKernelFileName, 'file')
				% check if there is a saved backstepping kernel
				fprintf("## Load the kernel %s from disk...\n", ...
					savedKernelFileName);
				loadedKernel = load(savedKernelFileName);
				K = loadedKernel.K;
				loaded = true;
				return
			else
				fprintf("## compute a new backstepping kernel ... \n")
				K = kernel.Feedback(varargin{:});
				% save the current object
				save(savedKernelFileName, "K");
				loaded = false;
			end
		end % tryToLoadKernel()
		
	end % methods (Static)
	
end % classdef
