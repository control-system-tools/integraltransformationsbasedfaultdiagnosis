classdef Line < handle & matlab.mixin.Copyable
	%
	properties (SetAccess = immutable)
		icType string {mustBeMember(icType, [""; "z = zeta"; "z = 1"; "zeta = 0"])};
	end % properties (SetAccess = immutable)
	
	properties (SetAccess = private)
		z (1, 1) quantity.Discrete;		% spatial z(s)-value
		zeta (1, 1) quantity.Discrete;	% spatial zeta(s)-value
		margin (1, 2) double = [0, 0]; % min and max value of s
	end % properties (SetAccess = private)
	
	properties (Access = public)
		value (1, :) double;
	end % properties (Access = public)
	
	properties (Dependent = true)
		% zDiscrete and zetaDiscrete are the properties
		% obj.z.valueDiscrete and obj.zeta.valueDiscrete. But to access
		% them for multiple Line objects at the same time, it is easier if
		% they are a (Dependent) property.
		zDiscrete;		% = obj.z.valueDiscrete
		zetaDiscrete;	% = obj.zeta.valueDiscrete
		sDiscrete;		% = obj.z.grid{:}
		initialPosition % variable spatial coordinate at boundary, see below
		%valueDz;
		%valueDzeta;
	end % properties (Dependent = true)
	
	methods
		function obj = Line(characteristic, icType, icValue, stepSizeCharacteristic)
			%% Constructor
			if nargin > 0 % this if-clause is needed to support
				% Characteristic-Array. Whenever a new obj(it) is
				% initialized, the constructor is called without
				% input-arguments. This would lead to errors, that are
				% avoided by this condition.
				if isempty(icValue)
					return;
				end
				% init obj.array
				obj(numel(icValue)).icType = icType;
				[obj.icType] = deal(icType);
				
				%% 
				for it = 1 : numel(obj)
					if strcmp(obj(it).icType, "z = zeta")
						obj(it).z = characteristic(1).subs("ic", icValue(it));
 						obj(it).zeta = characteristic(2).subs("ic", icValue(it));
 						sEndCandidate1 = obj(it).z.solveAlgebraic(1);
 						if (obj(it).zeta.on(sEndCandidate1) <= 1 && obj(it).zeta.on(sEndCandidate1) >= 0) ...
								&& (sEndCandidate1 ~= 0)
 							obj(it).margin(2) = sEndCandidate1;
						else
							sEndCandidate2 = obj(it).zeta.solveAlgebraic(0);
							if (obj(it).z.on(sEndCandidate2) <= 1 && obj(it).z.on(sEndCandidate2) >= 0)
								obj(it).margin(2) = sEndCandidate2;
							else
								obj(it).margin(2) = 0;
							end
 						end

 					elseif strcmp(obj(it).icType, "z = 1")
 						obj(it).z = characteristic(1).subs("ic", 1);
 						obj(it).zeta = characteristic(2).subs("ic", icValue(it));
 						% Calcualte sEnd = s(zeta=0)
 						obj(it).margin(1) = obj(it).z.solveAlgebraic(1);
 						obj(it).margin(2) = obj(it).zeta.solveAlgebraic(0);
 
 					elseif strcmp(obj(it).icType, "zeta = 0")
 						obj(it).z = characteristic(1).subs("ic", icValue(it));
 						obj(it).zeta = characteristic(2).subs("ic", 0);
 						% Calcualte sEnd = s(z=1)
 						obj(it).margin(1) = obj(it).zeta.solveAlgebraic(0);
 						obj(it).margin(2) = obj(it).z.solveAlgebraic(1);

					end
					% Update s-Grid of z(s) and zeta(s)
					if abs(diff(obj(it).margin)) < 10*eps
						% to avoid redundant = almost identical points on characteristic
						sGrid = obj(it).margin(1);
					else
						sGrid = linspace(obj(it).margin(1), obj(it).margin(2), ...
							ceil(abs(diff(obj(it).margin)) / stepSizeCharacteristic)+1);
					end
 					obj(it).z = obj(it).z.changeDomain(quantity.Domain("s", sGrid));
 					obj(it).zeta = obj(it).zeta.changeDomain(quantity.Domain("s", sGrid));
					obj(it).value = sGrid*0;
				end
				
			end
		end % Line() constructor
		
		function initialPosition = get.initialPosition(obj)
			if strcmp(obj.icType, "z = 1")
				initialPosition = obj.zeta.on(obj.margin(1));
			else
				initialPosition = obj.z.on(obj.margin(1));
			end
		end % get.initialPosition()
		
		function setIcValue(obj, value)
			% set value(1) of all objects to value(initialPosition)	
			if isnumeric(value)
				if isscalar(value)
					for it = 1 : numel(obj)
						obj(it).value(1) = value;
					end
				elseif numel(value) == numel(obj)
					for it = 1 : numel(obj)
						obj(it).value(1) = value(it);
					end
				else
					error("values do not fit to obj");
				end
				
			else
				tempValues = value.on([obj.initialPosition]);
				for it = 1 : numel(obj)
					obj(it).value(1) = tempValues(it);
				end % for it = 1 : numel(obj)
			end
		end % setIcValue()
		
		function plot(obj)
			% This method plots the lines of the characteristic in
			% subplots.
			figure();
			for it = 1:size(obj, 1)
				for jt = 1:size(obj, 2)
					% draw boundary
					plot([0, 1], [0, 1], "b", "linewidth", 2); hold on
					plot([0, 1], [0, 0], "b", "linewidth", 2);
					plot([1, 1], [0, 1], "b", "linewidth", 2);
					plot(obj(it, jt).z.valueDiscrete, obj(it, jt).zeta.valueDiscrete, "+");
				end
			end
			xlim([-0.1, 1.1]); ylim([-0.05, 1.05]);
		end % plot()
		
		function zDiscrete = get.zDiscrete(obj)
			zDiscrete = obj.z.on();
		end
		function zetaDiscrete = get.zetaDiscrete(obj)
			zetaDiscrete = obj.zeta.on();
		end
		function sDiscrete = get.sDiscrete(obj)
			sDiscrete = obj.z.domain(1).grid;
		end
		
		function [value, z, zeta] = on(obj, s)
			% this method returns the value of the line and the coordinates
			% (z, zeta) for a given s. If no input s is defined, then
			% s = obj.sDiscrete is assumed.
			arguments
				obj (1, 1);
				s;
			end
			if nargin == 1
				value = obj.value;
				z = obj.zDiscrete;
				zeta = obj.zetaDiscrete;
			else
				if numel(obj.sDiscrete) == 1
					value = repmat(obj.value, numel(s));
				else
					value = interp1(obj.sDiscrete, obj.value, s);
				end
				z = obj.z.on(s);
				zeta = obj.z.on(s);
			end
		end % on()
		
% 		function valueDz = get.valueDz(obj)
% 			% VALUEDZ() returns the spatial derivative of value w.r.t. the spatial
% 			% coordinate z.
% 			valueDz = gradient(obj.value(:), obj.sDiscrete(:)) .* gradient(obj.sDiscrete(:), obj.zDiscrete(:));
% 		end % diffDz()
% 		
% 		function valueDzeta = get.valueDzeta(obj)
% 			% VALUEDZETA() returns the spatial derivative of value w.r.t. the spatial
% 			% coordinate zeta.
% 			valueDzeta = gradient(obj.value(:), obj.sDiscrete(:)) .* gradient(obj.sDiscrete(:), obj.zetaDiscrete(:));
% 		end % diffDzeta()
		
	end % methods
	
end % classdef
