function myIc = combineInitialCondition(myStateSpace, varargin)
%tool.combineInitialCondition goes through all input arguments and searches
%for objects for which "getInitialCondition" methods are defined. Those methods are
%called, with the remaining input parameters to obtain initial conditions as double arrays.
%At the end, it is verified that the resulting IC-array myIc fits to myStateSpace.A and an error is
%thrown if not. This assertion deactivated if a name-value pair "checkSize", false is passed.

% iterate through varargin to extract all objects for which getInitialConditions are
% defined in simulationObject cell-array and in a parsedInput-struct
checkSize = true;
simulationObject = {};
parsedInput = struct();
for it = 1 : 2 : numel(varargin)
	if ismethod(varargin{it+1}, "getInitialCondition")
		simulationObject{end+1} = varargin{it+1};
	elseif strcmp(varargin{it}, "checkSize")
		checkSize = varargin{it+1};
	else
 		parsedInput = misc.setfield(parsedInput, varargin{it}, varargin{it+1});
	end
end

% get initial condition of every simulationObject
icCell = {};
stateNameArray = {};
for it = 1 : numel(simulationObject)
	[icCellTemp, stateNameCellTemp] = simulationObject{it}.getInitialCondition(parsedInput);
	icCell = cat(1, icCell, icCellTemp);
	stateNameArray = cat(1, stateNameArray, stateNameCellTemp);
end

% sort initial conditions values according to myStateSpace.OutputName
outputName = stateSpace.removeEnumeration(myStateSpace.OutputName, true);
[~, ~, indexIcCell] = intersect(outputName, stateNameArray, "stable");

% create resulting double array of initial conditions
myIc = cat(1, icCell{indexIcCell});

% check if resulting ic is correct
if checkSize
	assert(numel(myIc) == size(myStateSpace.A, 1), ...
		"The resulting IC must fit to system dynamics, but numel(xIc) = " ...
		+ string(numel(myIc)) + " and size(A) = " + mat2str(size(myStateSpace.A)));
end
assert(misc.isunique(stateNameArray), ...
	"There is a initial condition defined multiple times.");
end % combineInitialCondition()