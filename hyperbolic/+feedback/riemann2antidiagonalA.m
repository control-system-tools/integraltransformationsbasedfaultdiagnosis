function [systemRiemannAntidiagonal, T, TI, myFeedback] = riemann2antidiagonalA(systemRiemann)
% riemann2antidiagonalA applies a Hopf-Cole transformation to a model.Transport system, such that
% the diagonal elements of the distributed matrix A(z) = [a_ij(z)] are zero, i.e., a_ii(z) = 0, 
% i = 1, ..., n.
% For this, the Hopf-Cole transformation
%	x_ad(z) = T(z) x(z)
% is used, in which x(z) are the distributed states of the original system, and x_ad are the 
% distributed states of the target system.
%
%	systemRiemannAntidiagonal = riemann2antidiagonalA(systemRiemann) the target system of this
%		transformation is systemRiemannAntidiagonal, with systemRiemannAntidiagonal.A(i, i) == 0, 
%		i = 1, ..., n.
%
%	[systemRiemannAntidiagonal, T, TI] = riemann2antidiagonalA(systemRiemann) addtionally returns
%		the transformation matrixes T and its inverse TI, such that 
%			x_ad(z) = T(z) x(z) and x(z) = TI(z) x_ad(z)
%		in which x(z) are the distributed states of the original system, and x_ad are the 
%		distributed states of the target system.
%
%	[systemRiemannAntidiagonal, T, TI, myFeedback] = riemann2antidiagonalA(systemRiemann) as this
%		transformation T also scales the control input-matrix, a state feedback designed for the 
%		target system can not be applied to the original system. Hence, the model.Output object
%		myFeedback is contains in its myFeedback.input property, the gain needed to compensate the 
%		scaling of the transformation.

% See also model.Transport.findHopfColeForAntidiagonalA, feedback.energy2Riemann, ...
% feedback.backstepping and tool.stateFeedbackDesign.

%% get transformation matrix
[T, TI, ~, TI_z] = systemRiemann.findHopfColeForAntidiagonalA();
T1 = systemRiemann.e1.' * T * systemRiemann.e1;
T2 = systemRiemann.e2.' * T * systemRiemann.e2;
TI1 = systemRiemann.e1.' * TI * systemRiemann.e1;
% TI2 = systemRiemann.e2.' * TI * systemRiemann.e2;

%% transform parameter
parameter.A = T * (systemRiemann.A * TI + systemRiemann.Lambda * TI_z);
for it = 1 : max(systemRiemann.isotachic)
	selector = (systemRiemann.isotachic == it);
	if MAX(abs(parameter.A(selector, selector))) > 1e-6
		warning("Hopf-Cole transformation failed: max diagonal value of A = " ...
			+ string(MAX(abs(parameter.A(selector, selector)))));
	end
	parameter.A(selector, selector) = 0*parameter.A(selector, selector);
end
	
parameter.F = T * systemRiemann.F * TI.subs("z", "zeta");
parameter.A0 = T * systemRiemann.A0 * TI1.at(0);
parameter.bc0 = mtimes(systemRiemann.bc0.hopfCole(TI, TI_z), T2.at(0), "bc0");
parameter.bc1 = mtimes(systemRiemann.bc1.hopfCole(TI, TI_z), T1.at(1), "bc1");
% rounding ensures, that bc1 has the structure 0 = -1 x_1(1) + Q1 x_2(1), since the
% coefficient -1 might be slightly faulty
C0rounded = parameter.bc0.C0;
C0rounded(:, (systemRiemann.p+1):end) = round(C0rounded(:, (systemRiemann.p+1):end), 15);
C1rounded = parameter.bc1.C1;
C1rounded(:, 1:systemRiemann.p) = round(C1rounded(:, 1:systemRiemann.p), 15);
parameter.bc0 = parameter.bc0.copyAndReplace("C0", C0rounded);
parameter.bc1 = parameter.bc1.copyAndReplace("C1", C1rounded);

parameter.name = systemRiemann.name;
% the transformation of the control input is already considered in the resulting
% feedback gain. Hence it remains unchanged. This is implemented by saving it as a
% backup, and then exchange the transformed Input of the type "control" with the
% stored one.
controlInputBackup = copy(systemRiemann.input.get(systemRiemann.prefix + ".control"));
assert(all(controlInputBackup.isMatrixZeroOrEmpty("B", "B0")));

% transform input:
transformedInput = copy(systemRiemann.input);
if transformedInput.rowsB > 0
	transformedInput.B = T * systemRiemann.input.B;
end
if transformedInput.rowsB0 > 0
	transformedInput.B0 = T2.at(0) * systemRiemann.input.B0;
end
if transformedInput.rowsB1 > 0
	transformedInput.B1 = T1.at(1) * systemRiemann.input.B1;
end
if isempty( controlInputBackup )
	parameter.input = transformedInput;
else
	parameter.input = transformedInput.exchange(controlInputBackup);
end
% transform output:
parameter.output = systemRiemann.output.hopfCole(TI, TI_z);

%% construct hyperbolic system+ODE
parameterNameValuePairs = misc.struct2namevaluepair(parameter);
systemRiemannAntidiagonal = systemRiemann.copyAndReplace(parameterNameValuePairs{:});

if ~isempty( controlInputBackup ) && nargout >= 4
	myFeedback = model.Output(systemRiemann.prefix + ".control", ...	
		"C0", zeros(controlInputBackup.length, systemRiemannAntidiagonal.n), ...
		"C1", zeros(controlInputBackup.length, systemRiemannAntidiagonal.n), ...
		"input", misc.Gain("input", TI1.at(1), "outputType", systemRiemann.prefix + ".control"));
end
end