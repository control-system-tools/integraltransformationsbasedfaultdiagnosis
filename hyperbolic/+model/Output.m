classdef Output  < handle & matlab.mixin.Copyable
%model.Output implements output operators for distributed-parameter systems, i.e.,
%     y = C[x] + D u
%		= C0 x(0) + Cz0 x_z(0) + Ct0 x_t(0)
%			+ C1 x(1) + Cz1 x_z(1) + Ct1 x_t(1)
%			+ sum_k^l Ck x(zk) + Czk x_z(zk) + Ctk x_t(zk)
%			+ int_0^1 C(z) x(z) + Cz(z) x_z(z) + Ct(z) x_t(z) dz
%			+ D u(t)
% with z, zk \in [0, 1] and the distributed variable x(z) defined on the spatial domain z. The
% indices _z and _t indicate spatial and temporal derivatives. Moreover, the output y can be
% identified by the name stored in the type-property. The length of the state vector x and of the
% output vector y are stored in the lengthState and lengthOutput properties.
%
% Thus, y results from x(z) being evaluated at the boundaries z=0, 1, distributed and pointwise
% in-domain and from feedthoughs D u(t) of signals u(t). Hence, model.Output can be used to
% implement formal output operators, boundary operators or feedback for distributed-parameter
% systems.
%
% This class features methods for
%	- the evaluation of distributed states (see model.Output.out),
%	- for spatial discretizations of the operator (see model.Output.setApproximation and the 
%		properties Cop and grid)
%	- for usage as boundary operator or state feedback operator (see
%		model.Output.useAsBoundaryOperator, model.Output.useAsFeedback)
%	- for the combination of operators (see model.Outputs, model.Output.plus, model.Output.parallel,
%		model.Output.series, model.Output.blkdiag)
%	- for mathematical operators (see model.Output.mtimes, model.Output.kron, model.Output.parallel,
%		model.Output.series)
%	- state transformations (see model.Output.backstepping, model.Output.hopfCole,
%		model.Output.integralTransformation)
%
% See also model.Output.Output (constructor), model.Outputs, model.Input, model.Inputs, misc.Gain,
% misc.Gains, model.Dps, model.Transport, model.Wave, model.TransportOde, model.WaveOde.

    properties (SetAccess = protected)
		% output matrix of dirichlet term C0 x(0) at left boundary (z=0)
		C0 (:,:) double;
		% output matrix of neumann term Cz0 x_z(0) at left boundary (z=0)
		Cz0 (:,:) double;
		% output matrix of velocity term Ct0 x_t(0) at left boundary (z=0)
		Ct0 (:,:) double;
		% output matrix of dirichlet term C1 x(1) at right boundary (z=1)
		C1 (:,:) double;
		% output matrix of neumann term Cz1 x_z(1) at right boundary (z=1)
		Cz1 (:,:) double;
		% output matrix of velocity term Ct1 x_t(1) at right boundary (z=1)
		Ct1 (:,:) double;
		% vector if spatial points of pointwise outputs, length = k
		zk (:,1) double;
		% output matrix of dirichlet terms sum_k^l Ck(:, :, k) x(zk(k)), size(Ck, 3) = k
		Ck (:,:,:) double;
		% output matrix of neumann terms sum_k^l Czk(:, :, k) x_z(zk(k)), size(Czk, 3) = k
		Czk (:,:,:) double;
		% output matrix of velocity terms sum_k^l Ctk(:, :, k) x_t(zk(k)), size(Ctk, 3) = k
		Ctk (:,:,:) double;
		% output matrix of distributed outputs int_0^1 C(z) x(z) dz
		C (:,:) quantity.Discrete;
		% output matrix of distributed outputs int_0^1 Cz(z) x_z(z) dz
		Cz (:,:) quantity.Discrete;
		% output matrix of distributed outputs int_0^1 Ct(z) x_t(z) dz
		Ct (:,:) quantity.Discrete;
		
		% feedthough gains for terms D u(t) of inputs. See misc.Gains for details.
		input (1,1) misc.Gains;
		
		% name of the output, for instance "control", "measurement", "ode" 
		type string;
		% length of the output vector y
		lengthOutput (1, 1) double;
		% length of the distributed state vector x
		lengthState (1, 1) double;
		
		% Cop is a matrix discretized approximation of this operator. Multiplying this array with
		% the discretized state vector x = [x_1(0), x_1(dz), x_1(2 dz), ... x_n(1-dz), x_n(1)]^T, 
		% n = lengthState, yields the output, i.e.:
		%	y = Cop * x.
		% By default, time-derivatives x_t are not considered in Cop. See the "withTimeDerivative"
		% option in model.Output.setApproximation for details.
		Cop (:,:) double;
		% grid contains the grid used to discretize the output operator and the distributed
		% state. As every state is allowed to be discretized on a different spatial grid,
		% grid is a cell-array containing the grid as double-array for every state x_1, x_2,
		% ..., x_n, n = lengthState.
		grid cell;				
	end
	properties (Dependent = true)
		% number of grid points of every grid in grid
		gridLength;
		% number of spatial points zk of pointwise outputs
		k (1,1) double;
		
		% feedthrough gain stored in input
		D (:,:);
		% name of input signals in u for which a feedthrough is defined by input
		inputTypes string {mustBe.unique};
		% length of input signals in u for which a feedthrough is defined by input
		lengthInput (1, 1) double;
		
		% if false, then Ct, Ctk, Ct1, Ct0 are empty, otherwise true
		withTimeDerivative (1, 1) logical;
	end
	
    methods
		% constructor
        function obj = Output(type, varargin)
			% model.Output.Output constructs a model.Output object, which describes a formal output
			% operator for distributed-parameter systems.
			%
			%	obj = model.Output(type) the string type specifies the name of the output operator
			%		and the output signal. For instance, "controlOutput" or "y" can be used to
			%		define the output to be controlled. This is the only required input argument.
			%
			%	model.Transport([...], "C0", C0) sets the Dirichlet output gain C0. C0 must be a
			%		double array of the size lengthOutput x lengthState. Likewise, the output 
			%		matrices Cz0, Ct0, C1, Cz1, Ct1 can be defined. The default values are
			%		zeros(lengthOutput, lengthState)
			%
			%	model.Transport([...], "C", C) sets the distributed output matrix C. C must be a
			%		quantity.Discrete array of the size lengthOutput x lengthState and defined on a
			%		spatial domain z defined as quantity.Domain with [z.lower, z.upper] = [0, 1].
			%		Likewise, the distributed output matrices Cz, Ct can be defined.
			%
			%	model.Transport([...], "zk", zk, "Ck", "Ck") sets the pointwise output points zk and
			%		the related Dirichlet output gain Ck. zk must be a double array and Ck must be a
			%		double array of the size lengthOutput x lengthState x numel(zk). Likewise, the 
			%		output matrices Czk, Ctk can be defined.
			%
			% See also model.Output (class description and equations), model.Outputs, model.Input,
			%	model.Inputs, misc.Gain, misc.Gains, model.Dps, model.Transport, model.Wave,
			%	model.TransportOde, model.WaveOde.
			
			if nargin > 0 % needed to initialise empty model.Output objects
				
				obj.type = type;
				
				myParser1 = misc.Parser();
				addParameter(myParser1, "C0", []);
				addParameter(myParser1, "Cz0", []);
				addParameter(myParser1, "Ct0", []);
				addParameter(myParser1, "C1", []);
				addParameter(myParser1, "Cz1", []);
				addParameter(myParser1, "Ct1", []);
				addParameter(myParser1, "Ck", []);
				addParameter(myParser1, "Czk", []);
				addParameter(myParser1, "Ctk", []);
				addParameter(myParser1, "C", quantity.Discrete.empty(), ...
														@(C) isa(C, "quantity.Discrete"));
				addParameter(myParser1, "Cz", quantity.Discrete.empty(), ...
														@(Cz) isa(Cz, "quantity.Discrete"));
				addParameter(myParser1, "Ct", quantity.Discrete.empty(), ....
														@(Ct) isa(Ct, "quantity.Discrete"));
				myParser1.parse(varargin{:});
				parameterList1 = fieldnames(myParser1.Results);
				
				% set lengthOutput and lengthState and check matrix sizes
				obj.lengthOutput = 0;
				obj.lengthState = 0;
				for myParameter = parameterList1.'
					obj.(myParameter{:}) = myParser1.Results.(myParameter{:});
					if (obj.lengthOutput == 0) && ~isempty(myParser1.Results.(myParameter{:}))
						obj.lengthOutput = size(myParser1.Results.(myParameter{:}), 1);
					elseif ~isempty(myParser1.Results.(myParameter{:}))
						assert(isequal(obj.lengthOutput, size(myParser1.Results.(myParameter{:}), 1)), ...
							"number of rows of " + myParameter{:} ...
							+ " does not fit to other parameters");
					end
					if obj.lengthState == 0
						obj.lengthState = size(myParser1.Results.(myParameter{:}), 2);
					elseif ~isempty(myParser1.Results.(myParameter{:}))
						assert(isequal(obj.lengthState, size(myParser1.Results.(myParameter{:}), 2)), ...
							"number of columns of " + myParameter{:} ...
							+ " does not fit to other parameters");
					end
				end
				
				myParser2 = misc.Parser();
				addParameter(myParser2, "zk", []);
				addParameter(myParser2, "input", misc.Gains());
				myParser2.parse(varargin{:});
				obj.zk = myParser2.Results.zk;
				if ~isempty(myParser2.Results.input) && ~isempty(myParser2.Results.input.outputType)
					assert(all(strcmp(myParser2.Results.input.outputType, type)), ...
						"input must have the same outputType as the type of the model.Output object");
					obj.input = misc.Gains(myParser2.Results.input);
				end
				
				if ~isempty(obj.input) && obj.lengthOutput == 0
					obj.lengthOutput = obj.input.lengthOutput;
				end
				
				% correct faulty default values
				for defaultParameter = myParser1.UsingDefaults
					thisParameter = defaultParameter{1};
					if any(strcmp(thisParameter, ["Ck", "Czk", "Ctk"]))
						obj.(thisParameter) = zeros(obj.lengthOutput, obj.lengthState, obj.k);
					elseif any(strcmp(thisParameter, ["C", "Cz", "Ct"]))
						if (obj.lengthState == 0) || (obj.lengthOutput == 0)
							obj.(thisParameter) = quantity.Discrete.empty(...
								obj.lengthOutput, obj.lengthState);
						else
							obj.(thisParameter) = quantity.Discrete.zeros(...
								[obj.lengthOutput, obj.lengthState], ...
								quantity.Domain("z", [0, 1]), "name", thisParameter);
						end
					else
						obj.(thisParameter) = zeros(obj.lengthOutput, obj.lengthState);
					end
				end
				verifySizes(obj);
			end % if nargin > 0
		end % Output() Constructor
		
		function y = out(obj, x, NameValue)
			% out Evaluate the output operator for a given state distributed state x, a time
			% derivative xt and the feedthrough input u.For this,
			%     y = C[x] + D u
			%		= C0 x(0) + Cz0 x_z(0) + Ct0 x_t(0)
			%			+ C1 x(1) + Cz1 x_z(1) + Ct1 x_t(1)
			%			+ sum_k^l Ck x(zk) + Czk x_z(zk) + Ctk x_t(zk)
			%			+ int_0^1 C(z) x(z) + Cz(z) x_z(z) + Ct(z) x_t(z) dz
			%			+ D u(t)
			% is calculated.
			%
			%	y = out(obj, x) evaluates the output operator using the matrix-valued function x
			%		which must be a quantity.Discrete defined on a domain named "z" and must have
			%		obj.lengthState columns. In this case, u = 0 is assumed.
			%
			%	y = out(obj, x, "u", u) moreover consideres the feedthroug term D u. u must be a
			%		double or quantity.Discrete array with obj.lengthInput rows.
			%		Default: u = zeros(obj.lengthInput, size(x, 2))
			%
			%	y = out(obj, x, "xt", xt) allows to specify the time-derivative of x explicitly.
			%		Default: xt = x.diff("t, 1).
			
			arguments
				obj;
				x quantity.Discrete;
				NameValue.u = zeros([obj.lengthInput, size(x, 2)]);
				NameValue.xt quantity.Discrete = x.diff("t", 1);
			end
			assert(size(x, 1) == obj.lengthState, "x must have obj.lenghtState rows");
			assert(all(size(NameValue.xt) == size(x)), ...
				"xt must have obj.lenghtState rows and the same number of columns as x");
			assert(size(NameValue.u, 1) == obj.lengthInput, ...
				"u must have sum(obj.lengthInput) rows");
			
			xz = x.diff("z", 1);
			% distributed
			y = int(obj.C*x + obj.Cz*xz, "z");

			% boundaries
			y = y + obj.C0 * x.subs("z", 0) + obj.C1 * x.subs("z", 1) + ...
				+ obj.Cz0 * xz.subs("z", 0) + obj.Cz1 * xz.subs("z", 1);

			% pointwise
			for it = 1 : obj.k
				y = y + obj.Ck(:,:,it) * x.subs("z", obj.zk(it)) ...
					+ obj.Czk(:,:,it) * xz.subs("z", obj.zk(it));
			end
			
			% consider feedthrough
			if obj.lengthInput > 0
				 y = y + obj.D * NameValue.u;
			end
			
			% consider time derivative
			if obj.withTimeDerivative
				y = y + obj.Ct0 * NameValue.xt.subs("z", 0) ...
					 + obj.Ct1 * NameValue.xt.subs("z", 1);
				yIntDt = int(obj.Ct * NameValue.xt, "z");
				y = y + yIntDt;
				for it = 1 : obj.k
					y = y + obj.Ctk(:,:,it) * NameValue.xt.subs("z", obj.zk(it));
				end
			end
		end % out()
		
		function obj = setApproximation(obj, grid, NameValue)
			% setApproximation calculates a discrete approximation of the formal output operator and
			% stores it in obj.Cop. obj.Cop is a matrix representing a discretized approximation of
			% this operator. Multiplying this array with the discretized state vector x = [x_1(0),
			% x_1(dz), x_1(2 dz), ... x_n(1-dz), x_n(1)]^T, n = lengthState, yields the output
			%	y = Cop * x
			%	  = C0 x(0) + Cz0 x_z(0) + Ct0 x_t(0)
%			%		+ C1 x(1) + Cz1 x_z(1) + Ct1 x_t(1)
%			%		+ sum_k^l Ck x(zk) + Czk x_z(zk) + Ctk x_t(zk)
%			%		+ int_0^1 C(z) x(z) + Cz(z) x_z(z) + Ct(z) x_t(z) dz
			%
			%	obj = setApproximation(obj, grid) creates a finiteDifferences approximation using a
			%		3 point centered scheme for the indomain differentiation and a 2 point stencil
			%		for the boundary conditions using the spatial grid defined by grid. grid can be
			%		a double-array if all states are discretized with the same grid, or it can be a
			%		cell-array with numel obj.lengthState in order to specify the grid for each
			%		state element individually. The approximation is stored in obj.Cop, also the
			%		grid is stored in obj.grid.
			%
			%	setApproximation([...], "withTimeDerivative", withTimeDerivative) if 
			%		withTimeDerivative = true, additionally the terms 
			%			Ct0 x_t(0) + Ct1 x_t(1) + Ctk x_t(zk) + int_0^1 Ct x_t(z) dz 
			%		are considered in the approximation. Then,
			%			y = Cop * col(x, x_t)
			%		yields the output y.
			%		Default: withTimeDerivative = false 
			%
			%	setApproximation([...], "method", method) the string method allows to choose 
			%		wheather finite differences (method = "finiteDifferences") or chebfun (mehtod =
			%		"cheb") is used to approximate the output operator. See chebfun.org for details.
			%		Default: method = "finiteDifferences"
			%
			%	setApproximation([...], "stencilLength", stencilLength) the integer stencilLength
			%		specifies in the case method = "finiteDifferences" the number of points used for
			%		the finite difference stencil at the boundaries are supposed to be used.
			%		Default: stencilLength = 2
			
			arguments
				obj;
				grid;
				NameValue.withTimeDerivative (1, 1) logical = false;
				NameValue.method (1, 1) string ...
					{mustBeMember(NameValue.method, ["cheb", "finiteDifferences"])} ...
					= "finiteDifferences";
				NameValue.stencilLength (1, 1) double = 2;
			end
			
			% get grid in cell format
			if ~iscell(grid)
				grid = {grid};
			end
			if numel(grid) ~= obj.lengthState
				assert(numel(grid) == 1, ...
					"grid does not fit. It must be either a double array vector given " ...
					+ "blank or as a cell. If different grids for every state element " ...
					+ "are supposed, then numel(grid) must be equal obj.lengthState");
				grid(1:obj.lengthState, 1) = grid;
			end
			obj.grid = grid;
			gridLength = obj.gridLength;
			
			% calculate Cop
			obj.Cop = zeros(obj.lengthOutput, sum(gridLength));
			
			% integral-approximation of obj.C and obj.Cz
			for it = 1 : obj.lengthOutput
				for jt = 1 : obj.lengthState
					if strcmp(NameValue.method, "finiteDifferences")
 						D1 = three_point_centered_D1(grid{jt});
						% D1 = numeric.fd.matrix('3pointCentral', grid{jt}, 1);
 					elseif strcmp(NameValue.method, "cheb")
 						D1 = diffmat(numel(grid{jt}), 1, grid{jt}([1, end]).');
					end
					columns = sum(gridLength(1:jt-1)) + (1:gridLength(jt));
					if strcmp(NameValue.method, "finiteDifferences")
						massMatrix_jt = numeric.massMatrix(quantity.Domain("z", grid{jt})).';
						obj.Cop(it, columns) = ...
							(massMatrix_jt * obj.C(it, jt).on(grid{jt})).' ...
							+ (massMatrix_jt * obj.Cz(it, jt).on(grid{jt})).' * D1;
					elseif strcmp(NameValue.method, "cheb")
						massMatrix_jt = cumsummat(numel(grid{jt}), grid{jt}([1, end]).');
						obj.Cop(it, columns) = ...
							massMatrix_jt(end, :) .* obj.C(it, jt).on(grid{jt}).' ...
							+ (massMatrix_jt(end, :) .* obj.Cz(it, jt).on(grid{jt}).') * D1;
					end
				end
			end

			% discrete output-points
			for kt = 1 : obj.k
				zkTemp = obj.zk(kt);
				for jt = 1 : obj.lengthState
					zIdx = interp1(grid{jt}, ...
						linspace(1, gridLength(jt), gridLength(jt)), zkTemp);
					zIdxMin = floor(zIdx);
					zIdxMax = min(ceil(zIdx), gridLength(jt));
					z_min = grid{jt}(zIdxMin);
					z_max = grid{jt}(zIdxMax);
					dz = z_max - z_min;

					% Ck
					if zIdxMax ~= zIdxMin
						obj.Cop(:, zIdxMin + sum(gridLength(1:jt-1))) = ...
							obj.Cop(:, zIdxMin + sum(gridLength(1:jt-1))) ...
							+ (1-(zkTemp-z_min)/dz) * obj.Ck(:, jt, kt);

						obj.Cop(:, zIdxMax + sum(gridLength(1:jt-1))) = ...
							obj.Cop(:, zIdxMax + sum(gridLength(1:jt-1))) ...
							+ ((zkTemp-z_min)/dz) * obj.Ck(:, jt, kt);
					else
						obj.Cop(:, zIdxMin + sum(gridLength(1:jt-1))) = ...
							obj.Cop(:, zIdxMin + sum(gridLength(1:jt-1))) ...
							+ obj.Ck(:, jt, kt);
					end
					
					% Czk
					if zIdxMax == zIdxMin
						zIdxMax = min(zIdxMax+1, gridLength(jt));
						zIdxMin = max(zIdxMin-1, 1);
					end
					CzTemp = obj.Czk(:, jt, kt) * [-1, 1] ...
						/ (grid{jt}(zIdxMax) - grid{jt}(zIdxMin));
					obj.Cop(:, zIdxMin + sum(gridLength(1:jt-1))) = ...
						obj.Cop(:, zIdxMin + sum(gridLength(1:jt-1))) + CzTemp(:, 1);
					obj.Cop(:, zIdxMax + sum(gridLength(1:jt-1))) = ...
						obj.Cop(:, zIdxMax + sum(gridLength(1:jt-1))) + CzTemp(:, 2);
				end % for jt = 1 : obj.lengthState
			end % for kt = 1 : obj.k
			
			% boundary
			for jt = 1 : obj.lengthState
				% left boundary
				% C0
				obj.Cop(:, 1 + sum(gridLength(1:jt-1))) = ...
					obj.Cop(:, 1 + sum(gridLength(1:jt-1))) ...
					+ obj.C0(:, jt);
				% Cz0
				if strcmp(NameValue.method, "finiteDifferences")
					fdStencil = numeric.fd.stencilBoundary(...
						NameValue.stencilLength, grid{jt}, 0);
				elseif strcmp(NameValue.method, "cheb")
					D1 = diffmat(numel(grid{jt}), 1, grid{jt}([1, end]).');
					fdStencil = D1(1, :);
				end
				CzTemp = obj.Cz0(:, jt) * fdStencil;
				obj.Cop(:, (1:numel(fdStencil)) + sum(gridLength(1:jt-1))) = ...
					obj.Cop(:, (1:numel(fdStencil)) + sum(gridLength(1:jt-1))) + CzTemp;
				
				% C1
				% right boundary
				obj.Cop(:, sum(gridLength(1:jt))) = ...
					obj.Cop(:, sum(gridLength(1:jt))) ...
					+ obj.C1(:, jt);
				% Cz1
				if strcmp(NameValue.method, "finiteDifferences")
					fdStencil = numeric.fd.stencilBoundary(...
						NameValue.stencilLength, grid{jt}, 1);
				elseif strcmp(NameValue.method, "cheb")
					% D1 already calculated above at Cz0
					fdStencil = D1(end, :);
				end
				CzTemp = obj.Cz1(:, jt) * fdStencil;
				obj.Cop(:, (1-numel(fdStencil) : 0) + sum(gridLength(1:jt))) = ...
					obj.Cop(:, ((1-numel(fdStencil)) : 0) + sum(gridLength(1:jt))) + CzTemp;
			end % for jt = 1 : obj.lengthState
			
			%% gain for time derivative Ct, Ctk, Ct0, Ct1
			if obj.withTimeDerivative || NameValue.withTimeDerivative
				% init discrete output matrix attached to obj.Cop later via horzcat
				CopDt = zeros(obj.lengthOutput, sum(gridLength));
				
				% integral-approximation
				for it = 1 : obj.lengthOutput
					for jt = 1 : obj.lengthState
						C_j = obj.Ct.on(grid{jt});
						columns = sum(gridLength(1:jt-1)) + (1:gridLength(jt));
						if strcmp(NameValue.method, "finiteDifferences")
							massMatrix_jt = numeric.massMatrix(quantity.Domain("z", grid{jt})).';
							CopDt(it, columns) = massMatrix_jt * C_j(:,it,jt);
						elseif strcmp(NameValue.method, "cheb")
							massMatrix_jt = cumsummat(numel(grid{jt}), grid{jt}([1, end]).');
							CopDt(it, columns) = massMatrix_jt(end, :) .* C_j(:,it,jt).';
						end
					end
				end
				
				% discrete output-points in-domain
				for kt = 1 : obj.k
					zkTemp = obj.zk(kt);
					for jt = 1 : obj.lengthState
						zIdx = interp1(grid{jt}, ...
							linspace(1, gridLength(jt), gridLength(jt)), zkTemp);
						zIdxMin = floor(zIdx);
						zIdxMax = min(ceil(zIdx), gridLength(jt));
						z_min = grid{jt}(zIdxMin);
						z_max = grid{jt}(zIdxMax);
						dz = z_max - z_min;

						% Ctk
						if zIdxMax ~= zIdxMin
							CopDt(:, zIdxMin + sum(gridLength(1:jt-1))) = ...
								CopDt(:, zIdxMin + sum(gridLength(1:jt-1))) ...
								+ (1-(zkTemp-z_min)/dz) * obj.Ctk(:, jt, kt);

							CopDt(:, zIdxMax + sum(gridLength(1:jt-1))) = ...
								CopDt(:, zIdxMax + sum(gridLength(1:jt-1))) ...
								+ ((zkTemp-z_min)/dz) * obj.Ctk(:, jt, kt);
						else
							CopDt(:, zIdxMin + sum(gridLength(1:jt-1))) = ...
								CopDt(:, zIdxMin + sum(gridLength(1:jt-1))) ...
								+ obj.Ctk(:, jt, kt);
						end
					end % for jt = 1 : obj.lengthState
				end % for kt = 1 : obj.k
			
				% boundary
				for jt = 1 : obj.lengthState
					% C0 : left boundary
					CopDt(:, 1 + sum(gridLength(1:jt-1))) = ...
						CopDt(:, 1 + sum(gridLength(1:jt-1))) ...
						+ obj.Ct0(1:obj.lengthOutput, jt);
					% C1 : right boundary
					CopDt(:, sum(gridLength(1:jt))) = ...
						CopDt(:, sum(gridLength(1:jt))) ...
						+ obj.Ct1(1:obj.lengthOutput, jt);
				end % for jt = 1 : obj.lengthState
				
				obj.Cop = horzcat(obj.Cop, CopDt);
			end % if obj.withTimeDerivative
		end % setApproximation()
		
		function [bcOperatorAllDomain, weights, inputMatrix] = useAsBoundaryOperator(obj, type, z, varargin)
			% See model.Outputs.useAsBoundaryOperator
			[bcOperatorAllDomain, weights, inputMatrix] ...
				= useAsBoundaryOperator(model.Outputs(obj), type, z, varargin{:});
		end % useAsBoundaryOperator()
		
		function myStateSpace = addOutput2stateSpace(obj, myStateSpace, stateName, ...
				grid, withTimeDerivative, varargin)
			% input checks
			assert(isstring(stateName), "state name must be string");
			assert(iscell(grid), "grid must be a cell array");
			assert(islogical(withTimeDerivative), "withTimeDerivative must be logical");
			
			myParser = misc.Parser();
			myParser.addParameter("finiteDifferencesStencilLength", 2, @(v) isnumeric(v) & isscalar(v));
			myParser.addParameter("method", "finiteDifferences");
			myParser.parse(varargin{:});
			
			% combine output matrices
			obj = obj.setApproximation(grid, ...
				"withTimeDerivative", withTimeDerivative, ...
				"stencilLength", myParser.Results.finiteDifferencesStencilLength, ...
				"method", myParser.Results.method);
			gainC = horzcat(obj.Cop, obj.input.gain.value);
			gainInputName = vertcat(stateName, obj.input.gain.inputType);
			
			allInputLength = sum(obj.gridLength(:));
			if withTimeDerivative
				allInputLength = [allInputLength; allInputLength(1)];
			end
			allInputLength = [allInputLength; obj.input.lengthInput(:)];
			outputSs = ss([], [], [], gainC);
			outputSs = stateSpace.setSignalName(outputSs, "input", gainInputName, allInputLength);
			outputSs = stateSpace.setSignalName(outputSs, "output", obj.type, obj.lengthOutput);
			
			% combine input names
			allInputNames = unique([myStateSpace.InputName; obj.input.InputName], "stable");
			allOutputNames = unique([myStateSpace.OutputName; obj.type], "stable");
			
			% connect outputs with stateSpace
 			optConnect = connectOptions("Simplify", false);
			myStateSpace = stateSpace.connect(allInputNames, allOutputNames, optConnect, ...
				myStateSpace, outputSs);
		end % addOutput2stateSpace
		
		function myStateSpace = useAsFeedback(obj, myStateSpace, controlInputName, ...
				stateName, grid, withTimeDerivative, varargin)
			% USEASFEEDBACK connects the feedback with the state-space myStateSpace.
			% The feedback is approximated as state-space block and its input- and 
			% output-names are  set according to the input parameter. 
			% Then, via stateSpace.connect, the state spaces are combined.
			%
			% INPUT PARAMETER
			%	obj					feedback implemented as model.Output
			%	myStateSpace		system to be controlled as a state space
			%	controlInputName	name of the InputName of myStateSpace that is
			%						used as control input
			%	stateName			name of the OutputName of myStateSpace that is
			%						used as states for evaluating the feedback
			%	grid			spatial grid that suits the discretization of
			%						myStateSpace and which is also used for
			%						approximating the feedback (obj)
			%	withTimeDerivative	logical that indicates if time derivatives of the
			%						states are used for the state feedback (this is
			%						the case for wave equations)
			% OUTPUT PARAMETER
			%	myStateSpace		closed-loop of feedback (obj) and plant
			
			% input checks
			assert(isstring(controlInputName) && (numel(controlInputName) == 1));
			assert(isstring(stateName));
			assert(iscell(grid));
			assert(islogical(withTimeDerivative));
			
			myParser = misc.Parser();
			myParser.addParameter("finiteDifferencesStencilLength", 2, @(v) isnumeric(v) & isscalar(v));
			myParser.addParameter("method", "finiteDifferences", @(v) ismember(v, ["cheb", "finiteDifferences"]));
			myParser.parse(varargin{:});
			
			% approximate state-feedback
			obj = obj.setApproximation(grid, ...
				"withTimeDerivative", withTimeDerivative, ...
				"stencilLength", myParser.Results.finiteDifferencesStencilLength, ...
				"method", myParser.Results.method);
			allGain = obj.Cop;
			allNames = stateName;
			
			% add the gains for further signals specified by obj.input
			% (this could be, for instance, an ode-state)
			allNames = [allNames; obj.input.inputType];
			allGain = horzcat(allGain, obj.input.gain.value);
			% for later specification of the signal names, the sizes of all signals
			% have to be known. The size of the plant state signals is equal to the
			% number of grid points.
			allInputLength = sum(obj.gridLength(:));
			if withTimeDerivative
				% if time derivatives of the state are used for the feedback, they
				% have the same size as the non-derivative state signals.
				allInputLength = [allInputLength; allInputLength(1)];
			end
			% now add the length of the other gains
			if ~isempty( obj.input.lengthInput(:)) && any(obj.input.lengthInput > 0)
				allInputLength = [allInputLength; obj.input.lengthInput(:)];
			end
			
			% create feedback state-space and set the input names
			feedbackSs = ss([], [], [], allGain);
			
			feedbackSs = stateSpace.setSignalName(feedbackSs, "input", allNames, allInputLength);
			
			% the output names of the feedback are set according to the number of
			% inputs of the plant myStateSpace
			unenumerateSsInputNames = stateSpace.removeEnumeration(myStateSpace.InputName);
			controlInputLength = sum(strcmp(unenumerateSsInputNames, controlInputName));
			feedbackSs = stateSpace.setSignalName(feedbackSs, "output", controlInputName, controlInputLength);
			
			% combine input names:
			% remove Inputs of obj, that are outputs of myStateSpace
			allInputNames = [...				
				setdiff(myStateSpace.InputName, feedbackSs.OutputName, "stable"); ...
				 setdiff(feedbackSs.InputName, myStateSpace.OutputName, "stable")];
			allOutputNames = union(myStateSpace.OutputName, feedbackSs.OutputName, "stable");
			
			% connect outputs with stateSpace
			myStateSpace = stateSpace.connect(...
				allInputNames, allOutputNames, myStateSpace, feedbackSs);
		end % useAsFeedback()
		
		function quan = approximation2quantity(obj, domain, varargin)
			% approximation2quantity returns a quantity.Discrete object quan such
			% that y = obj.out(x) = int_0^1 quan * x dx.
			obj.setApproximation(domain.grid);
			myMatrix = zeros(domain.n, obj.lengthOutput, obj.lengthState);
			for it = 1 : obj.lengthState
				myMatrix(:, :, it) = obj.Cop(:, (1:domain.n)+(it-1)*domain.n).';
			end % for it = 1 : obj.lengthState
			
			% consider spacing
			assert(domain.isequidistant(), "only implemented for equidistant grids");
			myMatrix(1, :, :) = 2 * myMatrix(1, :, :);
			myMatrix(end, :, :) = 2 * myMatrix(end, :, :);
			myMatrix = myMatrix * (domain.n-1) / diff(domain.grid([1, end]));
			
			quan = quantity.Discrete(myMatrix, domain);
		end % approximation2quantity()
		
		function outputSeries = series(obj, inputType, varargin)
			% Two feedbacks, i.e. 
			%	u1 = c1(x, u_2) and u_2 = c2(x, u3)
			% with u1 = C1[x] + D1 u_2 and u2 = C2[x] + D2 u_3
			% can be connected in series in the form
			%	u	=	u1(x, u2) 
			%		=	C1[x] + D1 u2
			%		=	C1[x] + D1 C2[x] + D1 D2 u3
			%		=	C3[x] + D3 u3
			% with C3 = C1 + D1 C2 and D3 = D1 D2.
			% Therein, the input of the obj specified by the input parameter is used.
			assert(isstring(inputType), "inputType input parameter must be a string specifying an input of obj");
			assert(numel(obj.inputTypes) >= 1, ...
				"there must be at least one input for a series connection");
			outputSeries = copy(obj);
			
			for it = 1 : numel(varargin)
				tempD = outputSeries.input.valueOf(inputType);
				outputNext = varargin{it};
				assert(size(tempD, 2) == outputNext.lengthOutput);
				if outputNext.lengthState > 0
					if outputSeries.lengthState ~= 0
						assert(outputSeries.lengthState == outputNext.lengthState);
					else
						% if outputSeries was empty before, this else case
						% writes zeros in all output matrices concerning the
						% state. Otherwise, sizes of output matrices would be
						% messed up.
						outputSeries.lengthState = outputNext.lengthState;
						zeroFit = zeros(outputSeries.lengthOutput, outputSeries.lengthState);
						for myParameter = ["C0", "Cz0", "Ct0", "C1", "Cz1", "Ct1"]
							outputSeries.(myParameter{1}) = zeroFit;
						end
						outputSeries.C = outputNext.C * zeros(outputSeries.lengthState);
						zeroFit = zeros(outputSeries.lengthOutput, outputSeries.lengthState, 0);
						for myParameter = ["Ck", "Czk", "Ctk"]
							outputSeries.(myParameter{1}) = zeroFit;
						end
					end
					for myParameter = ["C0", "Cz0", "Ct0", "C1", "Cz1", "Ct1", "C", "Cz", "Ct"]
						if isempty(outputSeries.(myParameter{1}))
							outputSeries.(myParameter{1}) = ...
								tempD * outputNext.(myParameter{1});
						else
							outputSeries.(myParameter{1}) = outputSeries.(myParameter{1}) ...
								+ tempD * outputNext.(myParameter{1});
						end
					end
					if outputNext.k > 0
						outputSeries.zk = [outputSeries.zk; outputNext.zk];
						for kt = 1 : outputNext.k
							outputSeries.Ck = cat(3, outputSeries.Ck, ...
								tempD * outputNext.Ck(:, :, kt));
							outputSeries.Czk = cat(3, outputSeries.Czk, ...
								tempD * outputNext.Czk(:, :, kt));
							outputSeries.Ctk = cat(3, outputSeries.Ctk, ...
								tempD * outputNext.Ctk(:, :, kt));
						end
					end
				end
				outputSeries.input = series(outputSeries.input, outputNext.input, ...
					inputType, outputNext.input.outputType);
			end
			outputSeries.verifySizes();
		end % series()
		
		
		function outputParallel = parallel(obj, varargin)
			% Two feedbacks, i.e. 
			%	u1 = c1(x, u) and u = u_2 = c2(x, u3)
			% with u1 = C1[x] + D1 u and u2 = C2[x] + D2 u
			% can be connected in parallel in the form
			%	u	=	u1(x, u3)  + u2(x, u3)
			%		=	C1[x] + D1 u3 + C2[x] + D2 u3
			%		=	C3[x] + D3 u3
			% with C3 = C1 + C2 and D3 = D1 + D2
			outputParallel = copy(obj);
			outputParallel.input = misc.Gains();
			for it = 1 : numel(varargin)
				outputNext = varargin{it};
				% check sizes
				assert(obj.lengthState == outputNext.lengthState, ...
					"Parallel outputs must be defined for same state length");
				assert(obj.lengthOutput == outputNext.lengthOutput, ...
					"Parallel outputs must be defined for same output length");
				if obj.lengthState > 0
					for myParameter = ["C0", "Cz0", "Ct0", "C1", "Cz1", "Ct1", "C", "Cz", "Ct"]
						outputParallel.(myParameter{1}) = ...
							outputParallel.(myParameter{1}) + outputNext.(myParameter{1});
					end
					if outputNext.k > 0
						outputParallel.zk = [outputParallel.zk; outputNext.zk];
						for kt = 1 : outputNext.k
							outputParallel.Ck = cat(3, outputParallel.Ck, ...
								outputNext.Ck(:, :, kt));
							outputParallel.Czk = cat(3, outputParallel.Czk, ...
								outputNext.Czk(:, :, kt));
							outputParallel.Ctk = cat(3, outputParallel.Ctk, ...
								outputNext.Ctk(:, :, kt));
						end
					end
				end
				outputParallel.type = outputParallel.type + "||" + outputNext.type;
			end
			% set input later, because final outputParallel.type must be known
			if obj.lengthInput > 0
				inputNext = copy(obj.input);
				inputNext.outputType = [outputParallel.type];
				outputParallel.input = outputParallel.input.add(inputNext);
			end
			for it = 1 : numel(varargin)
				outputNext = varargin{it};
				if outputNext.lengthInput > 0
					inputNext = copy(outputNext.input);
					inputNext.outputType = [outputParallel.type];
					outputParallel.input = outputParallel.input.add(inputNext);
				end
			end
			outputParallel.verifySizes();
		end % parallel()
		
		function newOutput = blkdiag(a, b, varargin)
			% blkdiag combines two model.Output objects by combining them block-diagonally.
			% if outputA = CA * xA + DA * uA and outputB = CB * xB + DB * uB
			% then newOutput = blkdiag(CA, CB) [xA; xB] + blkdiag(DA, DB) * [uA; uB].
			if nargin == 1
				newOutput = a.copy();
				return;
			end
			assert(isequal(a.type, b.type), "model.Outputs to be combined via blkdiag need to have same type");
			assert(all(a.isMatrixZeroOrEmpty("zk", "Ck", "Czk", "Ctk")) ...
				&& all(b.isMatrixZeroOrEmpty("zk", "Ck", "Czk", "Ctk")), ...
				"blkdiag for pointwise indomain outputs are not implemented yet");
			newParameter = struct();
			for myParameter = ["C0", "Cz0", "Ct0", "C1", "Cz1", "Ct1", "C", "Cz", "Ct", "input"]
				newParameter.(myParameter) = blkdiag(a.(myParameter), b.(myParameter));
			end % for newParameter
			newParameterNameValue = misc.struct2namevaluepair(newParameter);
			newOutput = model.Output(a.type, newParameterNameValue{:});
			if nargin > 2
				newOutput = blkdiag(newOutput, varargin{:});
			end % if nargin > 2
		end % blkdiag()
		
		function newOutput = vertcat(a, b, varargin)
			% vertcat combines two model.Output objects by combining them vertically.
			% if outputA = CA * x + DA * u and outputB = CB * x + DB * u
			% then newOutput = vertcat(CA, CB) xA + vertcat(DA, DB) * u.
			if nargin == 1
				newOutput = a.copy();
				return;
			end
			assert(isequal(a.type, b.type), ...
				"model.Output to be combined via vertcat need to have same type");
			assert(all(a.isMatrixZeroOrEmpty("zk", "Ck", "Czk", "Ctk")) ...
				&& all(b.isMatrixZeroOrEmpty("zk", "Ck", "Czk", "Ctk")), ...
				"vertcat for pointwise indomain outputs are not implemented yet");
			newParameter = struct();
			for myParameter = ["C0", "Cz0", "Ct0", "C1", "Cz1", "Ct1", "C", "Cz", "Ct"]
				newParameter.(myParameter) = vertcat(a.(myParameter), b.(myParameter));
			end % for newParameter
			
			if isempty(a.input) && isempty(b.input)
				% do nothing
			elseif ~isempty(a.input) && ~isempty(b.input)
				assert(a.input.numTypes == b.input.numTypes, ...
					"only implemented for obj.input with same numTypes")
				newParameter.input = misc.Gains();
				for it = 1 : a.input.numTypes
					assert(strcmp(a.input(it).inputType, b.input(it).inputType) ...
						&& strcmp(a.input(it).outputType, b.input(it).outputType), ...
						"only implements for obj.input(:) with same inputType and outputType");
					newParameter.input.add(misc.Gain(a.input(it).inputType, ...
						[a.input(it).value; b.input(it).value], ...
						"outputType", a.type));
				end % for it = 1 : a.input.numTypes
				
			elseif isempty(a.input) && ~isempty(b.input)
				newParameter.input = misc.Gains();
				for it = 1 : b.input.numTypes
					newParameter.input.add( ...
						misc.Gain(b.input(it).inputType, ...
							[zeros(a.lengthOutput, b.input(it).lengthInput); b.input(it).value], ...
							"outputType", a.type));
				end
				
			elseif ~isempty(a.input) && isempty(b.input)
				newParameter.input = misc.Gains();
				for it = 1 : a.input.numTypes
					newParameter.input.add( ...
						misc.Gain(a.input(it).inputType, ...
							[a.input(it).value; zeros(b.lengthOutput, a.input(it).lengthInput)], ...
							"outputType", a.type));
				end
			end % if isempty(a.input) && isempty(b.input)
			newParameterNameValue = misc.struct2namevaluepair(newParameter);
			newOutput = model.Output(a.type, newParameterNameValue{:});
			if nargin > 2
				newOutput = blkdiag(newOutput, varargin{:});
			end % if nargin > 2
		end % vertcat()
		
		function myOutputs = plus(a, b)
			% combining model.Output and model.Outputs
			if isa(b, "model.Outputs")
				myOutputs = b + a;
			elseif isa(b, "model.Output")
				if strcmp(a.type, b.type)
					myOutputs = parallel(a, b);
					myOutputs = myOutputs.strrepType(myOutputs.type, a.type);
				else
					myOutputs = model.Outputs(a, b);
				end
			else
				error("model.Output can only be added with other Output or model.Outputs");
			end
		end % plus()
		
		function newObj = mtimes(obj, a, newType)
			% mtimes is the method for left-side multiplication of the constant 
			% double-array a to all model.Output matrices of obj.
			arguments
				obj model.Output;
				a (:, :) double;
				newType string = obj.type;
			end
			
			assert(isscalar(a) || (size(a, 2) == obj.lengthOutput), "size(a, 2) must be equal obj.lengthOutput");
			
			% perform multiplication for all matrices
			for myParameter = ["C0", "Cz0", "Ct0", "C1", "Cz1", "Ct1", "C", "Cz", "Ct"]
				newParameter.(myParameter{1}) = a * obj.(myParameter{1});
			end
			if obj.k > 0 % pointwise-indomain matrices
				newParameter.zk = obj.zk;
				if isscalar(a)
					newParameter.Ck = a * obj.Ck;
					newParameter.Ctk = a * obj.Ctk;
					newParameter.Czk = a * obj.Czk;
				else
					newParameter.Ck = misc.multArray(a, obj.Ck);
					newParameter.Ctk = misc.multArray(a, obj.Ctk);
					newParameter.Czk = misc.multArray(a, obj.Czk);
				end
			end

			% multiply inputs
			newParameter.input = mtimes(obj.input, a, obj.input.inputType, newType);

			% create new output newOut
			newParameterNameValue = misc.struct2namevaluepair(newParameter);
			newObj = model.Output(newType, newParameterNameValue{:});
		end % mtimes()
		
		function obj = setProperty(obj, varargin)
			for it = 1 : 2 : numel(varargin)
				if isprop(obj, varargin{it})
					if isempty(obj.(varargin{it})) ...
						|| isequal(size(obj.(varargin{it})), size(varargin{it+1}))
						obj.(varargin{it}) = varargin{it+1};
					else
						error("The parameter " + varargin{it} + " has different size than old one");
					end
				end
			end % for it = 1 : 2 : numel(varargin)
		end % setProperty()
		
		function newOutput = invert(obj, inputType)
			% The output signal of the inverted output is the input ux, 
			% specified by inputType.
			% Instead of 
			%	y = Cx + Dx ux + D u
			% the newOutput is
			%	ux = - Dx^-1 * Cx + Dx^-1 y - Dx^-1 D u.
			% Obviously, Dx must be invertible.
			if (nargin == 1)
				if (obj.input.numTypes == 1)
					inputType = obj.input.inputType{1};
				else
					error("The input which is the new output of the inverted operator has to be specified.");
				end
			end
			tempD = obj.input.valueOf(inputType);
			assert(size(tempD, 1) == size(tempD, 2), "D must be quadratic");
			assert(abs(det(tempD)) > 1e-4, "D must be invertible");
			
			
			% new input
			newInput = misc.Gain('oldOutput', inv(tempD), "outputType", inputType);
			for tempType = obj.input.inputType
				if ~isequal(tempType{1}, inputType)
					newInput = newInput + ...
						misc.Gain(tempType{1}, - Dtemp \ obj.input.valueOf(tempType{1}), ...
						"outputType", inputType);
				end
			end
			newParameterList = {"input", newInput};
			
			% new output gains
			for myParameter = ["C0" "Cz0", "Ct0", "C1", "Cz1", "Ct1", "C", "Cz", "Ct"]
				if ~isempty(obj.(myParameter))
					newParameterList = [newParameterList, ...
						{myParameter, - tempD \ obj.(myParameter)}];
				end
			end
			if ~isempty(obj.zk)
				newParameterList = [newParameterList, {"zk", obj.zk}];
				for myParameter = ["Ck", "Czk", "Ctk"]
					if ~isempty(obj.(myParameter))
						newValueTemp = obj.(myParameter{1});
						for it = 1 : obj.k
							newValueTemp(:, :, it) = - tempD \ newValueTemp(:, :, it);
						end
						newParameterList = [newParameterList, {myParameter, newValueTemp}];
					end
				end
			end
			
			% constructor
			newOutput = model.Output(inputType, newParameterList{:});
		end % invert()
        
         
        function newOutput = kron(A, obj)
            % kron calculates the Kronecker of a matrix A and and the output operator obj.
            % For example, when obj implements the output equation
            %   y = C[x] = int_0^1 C(z) x(z) dz \in R^{p}
            % for the distributed quantity x(z) \in R^{n}, then the newOutput is 
            %   y_new = kron(A, C)[v] = int_0^1 kron(A, C(z) v(z) dz \in R^{p*rows(A)}
			% with v(z) \in R^{n*columns(A)}.
			%
            % newOutput will have no input property, as the input property is not considered in the
            % current version of this method.
			%
			%	newOutput = kron(A, obj) calculates the Kronecker product of the model.Output obj
			%		and the double matrix A to obtain the model.Output newOutput.
			%
			% See also kron.
			
			arguments
				A (:, :) double;
				obj model.Output;
			end
			
			assert(isempty(obj.input), "not implemented yet for outputs with input property");
            
            newC0 = kron(A, obj.C0);
            newCz0 = kron(A, obj.Cz0);
            newCt0 = kron(A, obj.Ct0);
            newC1 = kron(A, obj.C1);
            newCz1 = kron(A, obj.Cz1);
            newCt1 = kron(A, obj.Ct1);
            newCk = zeros(size(newC0, 1), size(newC0, 2), 0);
            newCzk = zeros(size(newC0, 1), size(newC0, 2), 0);
            newCtk = zeros(size(newC0, 1), size(newC0, 2), 0);
            for dim = 1:size(obj.Ck, 3)
                newCk = cat(3, newCk, kron(A, obj.Ck(:, :, dim)));
                newCzk = cat(3, newCzk, kron(A, obj.Czk(:, :, dim)));
                newCtk = cat(3, newCtk, kron(A, obj.Ctk(:, :, dim)));
            end
            newC = kron(A, obj.C);
            newCz = kron(A, obj.Cz);
            newCt = kron(A, obj.Ct);
			newParameterList = {"C0", newC0, "Cz0", newCz0, "Ct0", newCt0, ...
				"C1", newC1, "Cz1", newCz1, "Ct1", newCt1, "C", newC, "Cz", newCz, ...
				"Ct", newCt, "zk", obj.zk};
			
            if ~isempty(obj.Ck)
                newParameterList = [newParameterList, {"Ck", newCk}];
            end
            if ~isempty(obj.Czk)
                newParameterList = [newParameterList, {"Czk", newCzk}];
            end
            if ~isempty(obj.Ctk)
                newParameterList = [newParameterList, {"Ctk", newCtk}];
            end
            
            newOutput = model.Output(obj.type, newParameterList{:});
            
        end %kron()
		
		
		%% Considering transformation of the state
		function newOutput = hopfCole(obj, T, T_z)
			% Create the output that consideres a Hopf-Cole Transformation
			%	newOutput.out(x, u) = obj.out(T*x, u)
			% The transformation matrix T must be a quantity.Discrete-object.
			%
			%	newOutput = hopfCole(obj, T) creates the output operator that is equivalent to the
			%		output operator obj evaluated for T*x, with the transformation matrix T(z)
			%		defined as a quantity.Discrete.
			%
			%	newOutput = hopfCole(obj, T, T_z) uses the spatial derivative T_z, which are needed
			%		in the case that Cz, Cz1, Cz0, Czk are non zero. By default, 
			%		T_z = T.diff("z", 1) is used
			
			arguments
				obj;
				T quantity.Discrete;
				T_z = [];
			end
			if isempty(T_z)
				if isa(T, "quantity.Symbolic")
					T_z = T.diff("z", 1);
				else
					T_z = T.diffFd("z", 1);
				end
			end
			assert(isequal(size(T), [obj.lengthState, obj.lengthState]), ...
				"T must fit the state used in the output");
			
			newParameterList = {"input", copy(obj.input), ...
					"C", obj.C * T + obj.Cz * T_z, ...
					"Cz", obj.Cz * T, ...
					"Ct", obj.Ct * T, ...
					"C0", obj.C0 * T.at(0) + obj.Cz0 * T_z.at(0), ...
				... % the second term results from the spatial derivative in
				... % Cz0 dz (T(z) x(z))_{z=0} = Cz0 (T'(0) x(0) + T(0) x'(0))
					 "Cz0", obj.Cz0 * T.at(0), ...
					 "Ct0", obj.Ct0 * T.at(0), ...
					 "C1", obj.C1 * T.at(1) + obj.Cz1 * T_z.at(1), ...
					 "Cz1", obj.Cz1 * T.at(1), ...
					 "Ct1", obj.Ct1 * T.at(1)};
						
			if ~isempty(obj.zk)
				CkTemp = 0*obj.Ck;
				CzkTemp = 0*obj.Czk;
				CtkTemp = 0*obj.Ctk;
				for kt = 1 : obj.k
					TTemp = T.at(obj.zk(kt));
					CkTemp(:,:,kt) = obj.Ck(:,:,kt) * TTemp + obj.Czk(:,:,kt) * T_z.at(obj.zk(kt));
					CzkTemp(:,:,kt) = obj.Czk(:,:,kt) * TTemp;
					CtkTemp(:,:,kt) = obj.Ctk(:,:,kt) * TTemp;
				end
				
				newParameterList = [newParameterList, ...
					{"zk", obj.zk, ...
					"Ck", CkTemp, ...
					"Czk", CzkTemp, ...
					"Ctk", CtkTemp}];
			end
			newOutput = model.Output(obj.type, newParameterList{:}, "input", copy(obj.input));
		end % hopfCole()
		
		function newOutput = integralTransformation(obj, K, integralBounds)
			% Create the output that consideres a Integral Transformation
			%	newOutput.out(x, u) ...
			%			   = obj.out(int_a^b K(z, zeta) * x(zeta) dzeta, u)
			% with a = integralBounds{1} and b = integralBounds{2}.
			% The transformation matrix K must be a quantity-object.
			
			%% input checks
			assert(isequal(size(K), [obj.lengthState, obj.lengthState]), ...
				"K must fit the state used in the output");
			assert(isa(K, "quantity.Discrete"));
			assert(any(strcmp([K(1).domain.name], "z")) && any(strcmp([K(1).domain.name], "zeta")));
			assert(iscell(integralBounds), "integralBounds must be specified as cell-array");
			assert(numel(integralBounds) == 2, ["Please specify both integral bounds", ...
				", for instance integralBounds = {0, \'z\'}"]);
			
			% assert that no output matrices are neglected, that are not implemented 
			% in this method.
			assert(all(obj.isMatrixZeroOrEmpty("Czk", "Ctk")), ...
				"not implemented for Czk, Ctk yet");
			if ~obj.isMatrixZeroOrEmpty("Cz")
				% As this method does not consider obj.Cz, this term is removed via
				% integration by parts and the transformation is applied to the
				% resulting output without Cz.
				objPiCz = obj.integrationByPartsCz();
				newOutput = objPiCz.integralTransformation(K, integralBounds);
				return
			end
			%% init integral domain
			% integralDomain(z) = 1		if z \in [integralBounds{1}, integralBounds{2}]
			%					= 0		else.
			myGrid = obj.getFinestGrid(K.gridOf("z"), K.gridOf("zeta"), ...
					obj.grid{:}, obj.C.gridOf("z"), obj.Ct.gridOf("z"));
			zDomain = quantity.Domain("z", myGrid);
			zetaDomain = quantity.Domain("zeta", myGrid);
			integralDomain = model.Dps.integralDomain(integralBounds, [zDomain, zetaDomain]);
			
			%% init new C and Ct
			newC = quantity.Symbolic.zeros([obj.lengthOutput, obj.lengthState], zDomain);
			newCt = newC;
			
			%% consider boundary output
			% matrices C0, C1, Ct0, Ct1, Cz0 and Cz1
			if ~strcmp(integralBounds{1}, "z") % C1, Ct1 and int-part of Cz1
				newC = newC + obj.C1 ...					% C1
					* subs(K * integralDomain, ["z", "zeta"], 1, "z");
				newC = newC + ...							% Cz1
					obj.Cz1 * subs(K.diff("z", 1) * integralDomain, ["z", "zeta"], 1, "z");
				newCt = newCt + ...							% Ct1
					obj.Ct1 * subs(K * integralDomain, ["z", "zeta"], 1, "z");
			% else strcmp(integralBounds{1}, 'z')
				% int_z^b -> C1 has no influence, since int_1^b == 0 for
				% all b \in [0, 1]
			end
			if ~strcmp(integralBounds{2}, "z") % C0, Ct0 and int-part of Cz0
				newC = newC + obj.C0 ...					% C0
					* subs(K * integralDomain, ["z", "zeta"], 0, "z");
				newC = newC + ...							% Cz0
					obj.Cz0 * subs(K.diff("z", 1) * integralDomain, ["z", "zeta"], 0, "z");
				newCt = newCt + ...							% Ct0
					obj.Ct0 * subs(K * integralDomain, ["z", "zeta"], 0, "z");
			% else strcmp(integralBounds{2}, 'z')
				% int_a^z -> C0 has no influence, since int_a^0 == 0 for
				% all a \in [0, 1]
			end
			
			% partial integral of Cz0 and Cz1 yields integral term (see 
			% above) and the following dirichlet terms for the boundary:
			newC0 = zeros(obj.lengthOutput, obj.lengthState);
			newC1 = zeros(obj.lengthOutput, obj.lengthState);
			if strcmp(integralBounds{1}, "z")
				newC0 = - obj.Cz0 * reshape(K.on({0, 0}), size(K));
				newC1 = - obj.Cz1 * reshape(K.on({1, 1}), size(K));
			elseif strcmp(integralBounds{2}, "z")
				newC0 = obj.Cz0 * reshape(K.on({0, 0}), size(K));
				newC1 = obj.Cz1 * reshape(K.on({1, 1}), size(K));
			end
			
			%% transform pointwise indomain outputs
			% Ck x(z) -> Ck [int_a^b K(zk, z) x(z) dz]
			% Ctk x_t(z) -> Ctk [int_a^b K(zk, z) x_t(z) dz]
			Cpointwise = 0 * newC;
			Ctpointwise = 0 * newCt;
			for it = 1 : obj.k
				Cpointwise = Cpointwise + obj.Ck(:,:,it) * ...
					subs(K * integralDomain, ["z", "zeta"], obj.zk(it), "z"); 
				Ctpointwise = Ctpointwise + obj.Ctk(:,:,it) * ...
					subs(K * integralDomain,["z", "zeta"], obj.zk(it), "z"); 
			end
			
			%% consider distributed output obj.C and Ct
			% change of integration order neccessary:
			%	int_0^1 int_a^b C(z) K(z, zeta), x(zeta) dzeta dz = ...
			%		= int_0^1 CTilde(z) x(z) dz
			% with
			%	if [a, b] = [0, 1]: CTilde(z) = int_0^1 C(zeta) K(zeta, z) dzeta
			%	if [a, b] = [z, 1]: CTilde = int_0^z C(zeta) K(zeta, z) dzeta
			%	if [a, b] = [0, z]: CTilde = int_z^1 C(zeta) K(zeta, z) dzeta
			changeOfIntegrationBounds = {0, 1};
			if strcmp(integralBounds{1}, "z")
				changeOfIntegrationBounds{2} = "z";
			end
			if strcmp(integralBounds{2}, "z")
				changeOfIntegrationBounds{1} = "z";
			end
			CTilde = int(subs(obj.C * K, ["z", "zeta"], "zeta", "z"), ...
				"zeta", changeOfIntegrationBounds{:});
			CtTilde = int(subs(obj.Ct * K, ["z", "zeta"], "zeta", "z"), ...
				"zeta", changeOfIntegrationBounds{:});
			if ~strcmp(integralBounds{1}, "z") && ~strcmp(integralBounds{2}, "z")
				CTilde = integralDomain.subs("zeta", "z") * CTilde;
				CtTilde = integralDomain.subs("zeta", "z") * CtTilde;
			end
			
			newC = newC + CTilde + Cpointwise;
			newCt = newCt + CtTilde + Ctpointwise;
			newC = newC.setName("C");
			newCt = newCt.setName("C_t");
			
			%% Create model.Output result
			newOutput = model.Output(obj.type, ...
				"input", obj.input, "C", newC.setName("C"), "Ct", newCt.setName("C_{t}"), ...
				"C0", newC0, "C1", newC1);
		end % integralTransformation()
		
		function newObj = integrationByPartsCz(obj)
			% integrationByPartsCz converts the integral term
			%	int_0^1 Cz(z) x_z(z) dz 
			% by performing integration by parts into
			%	int_0^1 Cz(z) x_z(z) dz
			%			= Cz(1) x(1) - Cz(0) x(0) - int_0^1 Cz_z(z) x(z) dz.
			CzDz = obj.Cz.diff("z", 1);
			if CzDz.abs.MAX() > 10 * CzDz.abs.mean("all")
				warning("Cz might be non-smooth -> check numerics!")
			end
			newObj = model.Output(obj.type, ...
				"C1", obj.Cz.at(1),  ...
				"C0", -obj.Cz.at(0),  ...
				"C", -CzDz);
			
			newObj = parallel(newObj, obj.copyAndReplace("Cz", setName(obj.Cz*0, "")));
		end % integrationByPartsCz()
		
		function outEnergy = transform2energy(obj)
		% This method transforms the output object that is defined for
		% the state v such that it suites the energy state x, which are given by
		%	x = [x_1; x_2] = [v_z; v_t].
		% It assumed that C0, C1, Ck, C == 0, hence the resulting output outEnergy is
		% obtained by 
		%		C0 = [old Cz0, old Ct0]
		%		C1 = [old Cz1, old Ct1]
		%		Ck = [old Czk, old Ctk]
		%		 C = [old Cz, old Ct1]
		% and copying the input-property.
			assert(all(obj.isMatrixZeroOrEmpty("C", "C1", "C0", "Ck")), ...
				"Transformation to energy coordinates is only possible, if the ", ...
				"output operator only uses time and/or space deriviatves of the state");
		
			outEnergy = model.Output(obj.type, ...
				"input", copy(obj.input), ...
				"C0", [obj.Cz0, obj.Ct0], ...
				"C1", [obj.Cz1, obj.Ct1], ...
				"Ck", cat(2, obj.Czk, obj.Ctk), ...
				"zk", obj.zk, ...
				"C", [obj.Cz, obj.Ct]);
			
		end % transform2energy()

		function newOutput = backstepping(obj, K, varargin)
			% backstepping can be used to transform an output from original
			% coordinates (x) into target systems coordinates (xTilde). When doing
			% this, usually the inverse backstepping transformation must be used.
			% y = obj.out(x, u) = newOutput.out(xTilde, u) ...
			%	= obj.out( xTilde +/- int KI xTilde dzeta, u)
			% Similarily, this method can be applied to transform an Output from
			% backstepping coordinates (xTilde) into original coordinates - which is
			% often used for state feedbacks. Hereby, the non-inverse backstepping
			% kernel must be used
			% y = obj.out(xTilde, u) = newOutput.out(x, u) ...
			%	= obj.out( x +/- int K x dzeta, u)
			% For calculation of the transformed outputs parameters the methods
			% integralTransformation and parallel are used.
			myParser = misc.Parser();
			myParser.addParameter("integralSign", []);
			myParser.addParameter("integralBounds", []);
			myParser.addParameter("inverse", false);
			myParser.parse(varargin{:});
			if isa(K, "kernel.Kernel")
				if myParser.Results.inverse
					integralSign = K.signOfIntegralTermInverse;
					Kquan = K.getValueInverse();
				else
					integralSign = K.signOfIntegralTerm;
					Kquan = K.getValue();
				end
				integralBounds = K.integralBounds;
			else
				assert(isa(K, "quantity.Discrete"), "K must be a quantity.Discrete");
				Kquan = K;
				integralSign = myParser.Results.integralSign;
				integralBounds = myParser.Results.integralBounds;
			end
			assert(~isempty(integralSign) && ~isempty(integralBounds), ...
				"integralSign and integralBounds must be specified");
			
			%
			newOutput = parallel(obj, obj.integralTransformation(...
				integralSign*Kquan, integralBounds));
			newOutput.input = copy(obj.input);	% due to parallel() D would have been 
			%									% considered twice without this line
			newOutput.type = obj.type;
		end % backstepping()
		
		function newOutput = decouple(obj, Pi, varargin)
			% y = obj.out(e, u) = pdeOutput.out(x, u) + odeOutput(w)
			% with the decoupling state
			%	e(z, t) = x(z, t) + Pi(z) w(t)
			% Pi is expected to be numeric or quantity.Discrete
			myParser = misc.Parser();
			myParser.addParameter("decoupledStateName", "decoupledOde", @(v) isstring(v));
			myParser.addRequired("Pi", @(v) (size(v, 1) == obj.lengthState) && ...
				(isnumeric(v) || isa(v, "quantity.Discrete")));
			myParser.parse(Pi, varargin{:});
			
			newOutput = copy(obj);
			newOutput.input.add(...
				misc.Gain(myParser.Results.decoupledStateName, ...
				obj.out(Pi), "outputType", obj.type));
		end % decouple()
		
		%% Verification
		function verifySizes(obj)
			assert(isequal([obj.lengthOutput, obj.lengthState], size(obj.C0)));
			assert(isequal([obj.lengthOutput, obj.lengthState], size(obj.Cz0)));
			assert(isequal([obj.lengthOutput, obj.lengthState], size(obj.Ct0)));
			assert(isequal([obj.lengthOutput, obj.lengthState], size(obj.C1)));
			assert(isequal([obj.lengthOutput, obj.lengthState], size(obj.Cz1)));
			assert(isequal([obj.lengthOutput, obj.lengthState], size(obj.Ct1)));
			assert(isequal([obj.lengthOutput, obj.lengthState], size(obj.C)));
			assert(isequal([obj.lengthOutput, obj.lengthState], size(obj.Cz)));
			assert(isequal([obj.lengthOutput, obj.lengthState], size(obj.Ct)));
			
			assert(isequal([obj.lengthOutput, obj.lengthInput], size(obj.D)));
			
			assert(isequal(size(obj.zk), [obj.k, 1]));
			if obj.k == 1
				assert(isequal([obj.lengthOutput, obj.lengthState], size(obj.Ck)));
				assert(isequal([obj.lengthOutput, obj.lengthState], size(obj.Czk)));
				assert(isequal([obj.lengthOutput, obj.lengthState], size(obj.Ctk)));
			else
				assert(isequal([obj.lengthOutput, obj.lengthState, obj.k], size(obj.Ck)));
				assert(isequal([obj.lengthOutput, obj.lengthState, obj.k], size(obj.Czk)));
				assert(isequal([obj.lengthOutput, obj.lengthState, obj.k], size(obj.Ctk)));
			end
		end % verifySizes
		
		function flag = isMatrixZeroOrEmpty(obj, varargin)
			flag = false(numel(varargin), 1);
			for it = 1 : numel(varargin)
				testProperty = varargin{it};
				if isempty(obj.(testProperty))
					flag(it) = true;
				else
					if isa(obj.(testProperty), "quantity.Discrete")
						value = obj.(testProperty).on();
					elseif isa(obj.(testProperty), "misc.Gains") || isa(obj.(testProperty), "misc.Gain")
						value = obj.(testProperty).value;
					else
						value = obj.(testProperty);
					end
					flag(it) = isempty(value) || (mean(abs(value(:))) == 0);
				end
			end
		end % isMatrixZeroOrEmpty()
		
		function obj = extendType(obj, position, newText)
			% extendType adds a pre- or a postfix to obj.type
			arguments
				obj;
				position (1, 1) string;
				newText (1, 1) string;
			end
			assert(any(strcmp(position, ["front", "back"])));
			
			if strcmp(position, "front")
				obj.type = newText + obj.type;
			elseif strcmp(position, "back")
				obj.type = obj.type + newText;
			end
		end % extendType()
		
		function obj = strrepType(obj, oldText, newText, NameValue)
			% replace strings in type, input.inputType and input.outputType
			arguments
				obj;
				oldText (1, 1) string;
				newText (1, 1) string;
				NameValue.startOnly (1, 1) logical = false;
			end
			
			for it = 1 : numel(obj)
				if NameValue.startOnly && startsWith(obj.type, oldText)
					obj(it).type = newText + eraseBetween(obj(it).type, 1, strlength(oldText));
				elseif ~NameValue.startOnly
					obj(it).type = strrep(obj(it).type, oldText, newText);
				end
				
				obj(it).input = obj(it).input.strrepInputType(oldText, newText, ...
					"startOnly", NameValue.startOnly);
				obj(it).input = obj(it).input.strrepOutputType(oldText, newText, ...
					"startOnly", NameValue.startOnly);
			end
		end % strrepType()
		
		function obj = strrepOutputType(obj, oldText, newText, NameValue)
			% replace strings in type and input.outputType
			arguments
				obj;
				oldText (1, 1) string;
				newText (1, 1) string;
				NameValue.startOnly (1, 1) logical = false;
			end
			
			for it = 1 : numel(obj)
				if NameValue.startOnly && startsWith(obj.type, oldText)
					obj(it).type = newText + eraseBetween(obj(it).type, 1, strlength(oldText));
				elseif ~NameValue.startOnly
					obj(it).type = strrep(obj(it).type, oldText, newText);
				end
				obj(it).input = obj(it).input.strrepOutputType(oldText, newText, ...
					"startOnly", NameValue.startOnly);
			end
		end % strrepType()
		
		%% get methods
		function gridLength = get.gridLength(obj)
			if isempty(obj.grid)
				gridLength = 0;
			else
				gridLength = cellfun(@(v) numel(v), obj.grid);
			end
		end % get.gridLength()
		
		function k = get.k(obj)
			k = numel(obj.zk);
		end % get.k()
		
		function D = get.D(obj)
			if isempty(obj.input)
				D = zeros(obj.lengthOutput, 0);
			else
				D = obj.input.value;
			end
		end % get.D()
		
		function D = inputOf(obj, type)
			D = obj.input.get(type);
		end % get.inputGainOf()
		
		function inputTypes = get.inputTypes(obj)
			inputTypes = obj.input.inputType;
		end % get.inputTypes()
		
		function lengthInput = get.lengthInput(obj)
			if isempty(obj.input)
				lengthInput = 0;
			else
				lengthInput = sum(obj.input.lengthInput);
			end
		end % get.lengthInput()
		
		function withTimeDerivative = get.withTimeDerivative(obj)
			withTimeDerivative = ~all(obj.isMatrixZeroOrEmpty("Ct", "Ct0", "Ct1", "Ctk"));
		end % get.withTimeDerivative()
		
		function [texString, nameValuePairs] = printParameter(obj)
			% Create TeX-code for all non-zero parameter. In this method, the
			% implementation of model.Outputs is used.
			[texString, nameValuePairs] = printParameter(model.Outputs(obj));
		end % printParameter()
		
		function texString = print(obj, leftHandSide, stateNamePde)
			% print Print equation to command window as latex compatible string-array
			
			arguments
				obj;
				leftHandSide = obj.type.extractBefore(2) + "_{" + obj.type.extractAfter(1) + "}(t)";
				stateNamePde = "x";
			end % arguments
			
			suffix.C0 = stateNamePde + "(0, t)";
			suffix.C1 = stateNamePde + "(1, t)";
			suffix.Cz0 = stateNamePde + "^{\prime}(0, t)";
			suffix.Cz1 = stateNamePde + "^{\prime}(1, t)";
			suffix.Ct0 = "\dot{" + stateNamePde + "}(0, t)";
			suffix.Ct1 = "\dot{" + stateNamePde + "}(1, t)";
			suffix.C = stateNamePde + "(z, t)";
			suffix.Cz = stateNamePde + "^{\prime}(z, t)";
			suffix.Ct = "\dot{" + stateNamePde + "}(z, t)";
			suffix.Ck = cell(size(obj.zk));
			suffix.Czk = cell(size(obj.zk));
			suffix.Ctk = cell(size(obj.zk));
			option.rational = true;
			for kt = 1 : numel(obj.zk)
				suffix.Ck{kt} = stateNamePde + "(" + misc.latexChar(obj.zk(kt), option) + ", t)";
				suffix.Czk{kt} = stateNamePde + "^{\prime}(" + misc.latexChar(obj.zk(kt), option) + ", t)";
				suffix.Ctk{kt} = "\dot{" + stateNamePde + "}(" + misc.latexChar(obj.zk(kt), option) + ", t)";
			end
			
			% put string together
			if isnumeric(leftHandSide)
				if isscalar(leftHandSide) && obj.lengthOutput > 1
					leftHandSide = leftHandSide * ones(obj.lengthOutput, 1);
				end
				leftHandSide = misc.latexChar(leftHandSide);
			end
			if strcmp(leftHandSide, "none")
				myString = string();
			else
				myString = leftHandSide + " &=";
			end
			plusNeeded = false;
			
			% boundary output
			for myPar = ["C0", "Cz0", "Ct0", "C1", "Cz1", "Ct1"]
				if ~obj.isMatrixZeroOrEmpty(myPar)
					if ~plusNeeded
						% plus is only not needed after equal sign, hence true for
						% later
						plusNeeded = true;
					else
						myString = myString + " +";
					end
					if misc.iseye(obj.(myPar))
						myString = myString + " ";
					else
						myString = myString + " " + misc.latexChar(obj.(myPar));
					end
					if isfield(suffix, myPar)
						myString = myString + " " + suffix.(myPar);
					end
				end
			end % for myPar = ["C0", "Cz0", "Ct0", "C1", "Cz1", "Ct1"]
			
			% distributed output
			if any(~obj.isMatrixZeroOrEmpty("C", "Cz", "Ct"))
				if plusNeeded
					% plus is only not needed after equal sign, and also not after
					% int
					myString = myString + " +";
				end
				% integral
				myString = myString + "\int\limits_{0}^{1} ";
				for myPar = ["C", "Cz", "Ct"]
					if ~obj.isMatrixZeroOrEmpty(myPar)
						if ~plusNeeded
							% plus is only not needed after equal sign, hence true for
							% later
							plusNeeded = true;
						else
							myString = myString + " +";
						end
						if iseye(obj.(myPar))
							myString = myString + " ";
						else
							myString = myString + " " + misc.latexChar(obj.(myPar));
						end
						if isfield(suffix, myPar)
							myString = myString + " " + suffix.(myPar);
						end
					end
				end  % for myPar = ["C", "Cz", "Ct"]
				myString = myString + " \mathrm{d}z";
			end
			
			% pointwise indomain output
			for kt = 1 : numel(obj.zk)
				for myPar = ["Ck", "Czk", "Ctk"]
					if ~obj.isMatrixZeroOrEmpty(myPar)
						if ~plusNeeded
							% plus is only not needed after equal sign, hence true for
							% later
							plusNeeded = true;
						else
							myString = myString + " +";
						end
						if misc.iseye(obj.(myPar))
							myString = myString + " ";
						else
							myString = myString + " " + misc.latexChar(obj.(myPar));
						end
						if isfield(suffix, myPar)
							myString = myString + " " + suffix.(myPar){kt};
						end
					end
				end % for myPar = ["C0", "Cz0", "Ct0", "C1", "Cz1", "Ct1"]
			end % for kt = 1 : numel(obj.zk)
			
			if ~obj.isMatrixZeroOrEmpty("D")
				if plusNeeded
					myString = myString + " + " + obj.input.print(obj.type);
				else
					myString = myString + " " + obj.input.print(obj.type);
				end
			end
			
			if nargout > 0
				texString = myString;
			else
				misc.printTex(myString);
			end
		end % print()
		
		function obj = discretizeQuantity(obj)
			% discretizeQuantity converts all properties containg quantity-objects to
			% quantity.Discrete objects. This is often speeds up calculations.
			for prop = ["C", "Cz", "Ct"]
				obj.(prop) = quantity.Discrete(obj.(prop));
			end
		end % discretizeQuantity()
		
		function newObj = copyAndReplace(oldObj, varargin)
			% copyButReplace creates a copy of the object.
			% However, properties specified by a name-value-pair in varargin replace
			% the old objects properties.
			
			myArgs = ["C0", "Cz0", "Ct0", "C1", "Cz1", "Ct1", "zk", ...
				"Ck", "Czk", "Ctk", "C", "Cz", "Ct", "input"];
			myParser = misc.Parser;
			myParser.KeepUnmatched = false;
			for it = 1:numel(myArgs)
				thisArg = myArgs(it);
				if isa(oldObj.(thisArg), "matlab.mixin.Copyable")
					myParser.addParameter(thisArg, copy(oldObj.(thisArg)));
				else
					myParser.addParameter(thisArg, oldObj.(thisArg));
				end
			end
			myParser.parse(varargin{:});
			newObj = misc.constructorOf(oldObj, oldObj.type, myParser.ResultsNameValuePair{:});
		end % copyAndReplace()
		
	end % methods
	
	methods (Static = true)
		%% helper function
		function myGrid = getFinestGrid(varargin)
			myGrid = varargin{1};
			for myRunner = 2 : nargin
				if numel(myGrid) < numel(varargin{myRunner})
					myGrid = varargin{myRunner};
				end
			end
		end
	end
	
	methods (Access = protected)
		% Override copyElement method:
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@matlab.mixin.Copyable(obj);
			cpObj.C = copy(obj.C);
			cpObj.Cz = copy(obj.Cz);
			cpObj.Ct = copy(obj.Ct);
			cpObj.input = copy(obj.input);
		end
	end % methods (Access = protected)
end
