classdef Inputs < handle & matlab.mixin.Copyable
%Inputs is class to enease implementation of multiple different input 
%signals of dps, for instance control, disturbance, fault inputs or
%couplings with other systems. Inputs at both boundaries, distributed
%inputs and inputs acting on the output are implemented. It is assumed,
%that the input signal is multiplied with the input matrices from right
%side, i.e. B * u(t).
%For every input there is an Input-object (singular!), which contains the
%corresponding matrices and information for that input. This class Inputs
%(plural!) contains all inputs of a dps and acts as the interface to the
%classes of dps.
	properties (SetAccess = protected)
		input (1, :) model.Input;		% this array contains the data of
		%							% each input
	end

	properties (Dependent = true)
		numTypes;					% number of different input-object that
		%							% this Inputs-object contains
		
		% data resulting from the input-property
		% input matrices
		B (:,:) quantity.Discrete;	% distributed input
		B0 (:,:) double;			% left boundary (z=0) input
		B1 (:,:) double;			% right boundary (z=1) input
		D (1,1) misc.Gains;			% feedthrough from input to outputs specified by 
		%							% D.outputType
		
		% data about signal names
		type (:, 1) string;					% type of input signal, for instance 
		%							% 'control', 'disturbance', 'fault' etc
		%							% Input types must be unique, this is
		%							% verified in the constructor and in
		%							% add().
		length (:, 1) double;		% length of input vectors
	end
	
	methods
		function obj = Inputs(varargin)
			%% Constructor
			if nargin > 0
				% input arguments can be of type model.Input, model.Inputs, misc.Gain and
				% misc.Gains
				obj.add(varargin{:});
			end % if nargin > 0
		end % Input() Constructor
		
		%% Combining objects:
		% add further input(s) to Inputs obj
		function obj = add(obj, varargin)
			for it = 1 : numel(varargin)
				if ~isempty(varargin{it})
					if isa(varargin{it}, "model.Input")
						if any(strcmp(varargin{it}.type, obj.type))
							oldInput = obj.get(varargin{it}.type);
							obj.exchange(oldInput + varargin{it});
						else
							obj.input(end+1) = varargin{it};
						end
					elseif isa(varargin{it}, "model.Inputs")
						tempInputs = varargin{it};
						for jt = 1 : tempInputs.numTypes
							obj.add(tempInputs.input(jt));
						end
					elseif isa(varargin{it}, "misc.Gain")
						tempGain = varargin{it};
						myIdx = obj.index(tempGain.inputType);
						if isempty(myIdx)
							obj.input(end+1) = model.Input(tempGain.inputType, "D", tempGain);
						else
							obj.input(myIdx).D = obj.input(myIdx).D.add(tempGain);
						end
					elseif isa(varargin{it}, "misc.Gains")
						tempGains = varargin{it};
						for jt = 1 : tempGains.numTypes
							obj.add(tempGains.gain(it));
						end
					else
						error("input must be model.Input or model.Inputs");
					end
				end
			end % for it
			assert(misc.isunique(obj.type), "input types must be unique");
			obj.verifySizes();
		end % add()
		
		% exchange input(s) of Inputs obj
		function obj = exchange(obj, newInput)
			obj.input(obj.index(newInput.type)) = newInput;
		end % exchange()
		
		% add input or combine inputs with simple + notation
		function c = plus(a, b)
			if isempty(a)
				c = b.copy();
				if isa(c, "model.Input")
					c = model.Inputs(c);
				end
			elseif isempty(b)
				c = a.copy();
			else
				c = a.copy();
				if isa(c, "model.Inputs") && isa(b, "model.Input")
					c = c.add(b);
				elseif isa(c, "model.Inputs") && isa(b, "model.Inputs")
					for it = 1 : b.numTypes
						c = c.add(b.input(it));
					end
				else
					error("input must be model.Input or model.Inputs");
				end
			end
		end % plus()
		
		function newInput = blkdiag(a, b, varargin)
			% blkdiag combines two model.Inputs objects by combining them block-diagonally.
			
			assert(isequal(a.type, b.type), "blkdiag is only defined for model.Inputs with same type");
			
			newInput = model.Inputs();
			for it = 1 : a.numTypes
				newInput = newInput.add(blkdiag(a.input(it), b.input(it)));
			end % for it()
			
			if nargin > 2
				newInput = blkdiag(newInput, varargin{:});
			end
		end % blkdiag()
		
		function obj = remove(obj, type)
			% remove delete the element of obj.input with the type(s) specified by the type input
			% argument (string-array).
			arguments
				obj;
				type (:, 1) string;
			end
			for it = 1 : numel(type)
				obj.input = obj.input((1:1:obj.numTypes) ~= obj.index(type(it)));
			end
		end % remove()
		
		function input2inputs = input2inputs(obj, thisType)
			% this method returns a matrix that can be used to connect a input of the
			% type thisType (single input!) to the complete input matrices.
			input2inputs = zeros(sum(obj.length(:)), obj.lengthOf(thisType));
			thisIndex = obj.index(thisType);
			assert(~isempty(thisIndex), 'Type is not defined');
			input2inputs(sum(obj.length(1:(thisIndex-1))) + (1 : obj.length(thisIndex)), :) ...
				= eye(obj.length(thisIndex));
		end % input2inputs()
		
		function myStateSpace = addD2ss(obj, myStateSpace)
			% addD2Ss adds the feedthrough D to a given state space model
			if ~isempty(obj.D)
				myDSs = gain2ss(obj.D);
				myStateSpace = stateSpace.parallel(myStateSpace, myDSs);
			end
		end % addD2ss()
		
		function obj = extendType(obj, position, newText)
			% extendType adds a pre- or a postfix to all obj.type
			arguments
				obj;
				position (1, 1) string;
				newText (1, 1) string;
			end
			for it = 1 : obj.numTypes
				obj.output(it).extendType(position, newText);
			end
		end % extendType()
		
		function obj = strrepType(obj, oldText, newText, NameValue)
			% replace strings in type
			arguments
				obj;
				oldText (1, 1) string;
				newText (1, 1) string;
				NameValue.startOnly (1, 1) logical = false;
			end
			for it = 1 : obj.numTypes
				obj.input(it).strrepType(oldText, newText, "startOnly", NameValue.startOnly);
			end
		end % strrepType()
		
		function obj = setPrefix(obj, prefix)
			% add a prefix to the type
			arguments
				obj;
				prefix char;
			end
			for it = 1 : obj.numTypes
				obj.input(it).setPrefix(prefix);
			end
		end % setPrefix()
				
		%% Methods for getting input-properties:
		function idx = index(obj, type)
			arguments
				obj;
				type (1, 1) string;
			end
			% find input by naming its type
			idx = [];
			for it = 1 : numel(obj.type)
				if strcmp(obj.type(it), type)
					idx = it;
				end
			end
		end % index()
		
		% get input of the type specified by the strings contained in varargin
		function thisInput = get(obj, varargin)
			if nargin == 2
				thisInput = obj.input(obj.index(varargin{1}));
			else
				myType = varargin;
				thisInput = cell(numel(myType), 1);
				for it = 1 : numel(myType)
					thisInput{it} = obj.input(obj.index(myType{it}));
				end
			end
		end % get()
		
		% create name-value pair of inputs
		function nameValuePairs = nameValuePairs(obj)
			nameValuePairs = cell(1, 2*obj.numTypes);
			for it = 1 : obj.numTypes
				nameValuePairs{2*it-1} = obj.input(it).type + "Input";
				nameValuePairs{2*it} = obj.input(it);
			end
		end % nameValuePairs()
		
		% get distributed input with state-individual discretization
		function BMat = getBDiscrete(obj, myGrid)
			if ~iscell(myGrid)
				assert(isvector(myGrid));
				myGrid = {myGrid};
			end
			if numel(myGrid) == 1
				myGrid = repmat(myGrid, size(obj.B));
			end
			BCell= cell(size(obj.B));
			for it = 1 : size(obj.B, 1)
				for jt = 1 : size(obj.B, 2)
					BCell{it, jt} = obj.B(it, jt).on(myGrid{it}, 'z');
				end
			end
			BMat = cell2mat(BCell); 
		end % getBDiscrete()
		
		%% get meta data about inputs
		function numTypes = get.numTypes(obj)
			numTypes = numel(obj.input);
		end %get.numTypes()
		
		function type = get.type(obj)
			type = vertcat(obj.input.type);
		end % get.type()
			
		function length = get.length(obj)
			length = [obj.input(:).length].';
		end % length()
		
		function length = lengthOf(obj, type)
			length = obj.length(obj.index(type));
			if isempty(length)
				length = 0;
			end
		end % lengthOf()
		
		%% rows*-getter
		function rowsB = rowsB(obj)
			rowsB = 0;
			for it = 1 : obj.numTypes
				tempSizeB = size(obj.input(it).B, 1);
				if tempSizeB > 0
					if rowsB == 0
						rowsB = tempSizeB;
					else
						assert(isequal(rowsB, tempSizeB), ...
							"number of input rows must coincide!");
					end
				end
			end
		end % rowsB()
		
		function rowsB0 = rowsB0(obj)
			rowsB0 = 0;
			for it = 1 : obj.numTypes
				tempSizeB0 = size(obj.input(it).B0, 1);
				if tempSizeB0 > 0
					if rowsB0 == 0
						rowsB0 = tempSizeB0;
					else
						assert(isequal(rowsB0, tempSizeB0), ...
							"number of input rows must coincide!");
					end
				end
			end
		end % rowsB0()
		
		function rowsB1 = rowsB1(obj)
			rowsB1 = 0;
			for it = 1 : obj.numTypes
				tempSizeB1 = size(obj.input(it).B1, 1);
				if tempSizeB1 > 0
					if rowsB1 == 0
						rowsB1 = tempSizeB1;
					else
						assert(isequal(rowsB1, tempSizeB1), ...
							"number of input rows must coincide!");
					end
				end
			end
		end % rowsB1()
		
		%% input matrices getter
		function B = get.B(obj)
			% B = [input(1).B, input(2).B, ... ]
			if obj.rowsB > 0
				Bcell = cell(obj.numTypes, 1);
				[bDomain, bElement] = getBdomainHelper(obj);
				for it = 1 : obj.numTypes
					if isempty(obj.input(it).B) && obj.input(it).length > 0
						Bcell{it} = bElement.zeros(...
							[obj.rowsB, obj.input(it).length], bDomain);
					else
						Bcell{it} = obj.input(it).B;
					end
				end
				B = horzcat(Bcell{:});
			else
				B = reshape(obj.input(1).B, [0, sum(obj.length)]);
			end
		
			function [bDomain, bElement] = getBdomainHelper(obj)
				bDomain = quantity.Domain.empty();
				bElement = [];
				for jt = 1 : obj.numTypes
					if ~isempty(obj.input(jt).B) && ~isNumber(obj.input(jt).B)
						bElement = obj.input(jt).B(1);
						bDomain = join(bDomain, bElement.domain);
					end
				end % for it
			end % getBdomainHelper()
			
		end % get.B()
		
		function B0 = get.B0(obj)
			% B0 = [input(1).B0, input(2).B0, ... ]
			if obj.rowsB0 > 0
				B0cell = cell(obj.numTypes, 1);
				for it = 1 : obj.numTypes
					if ~isempty(obj.input(it).B0)
						B0cell{it} = obj.input(it).B0;
					else
						B0cell{it} = zeros([obj.rowsB0, obj.input(it).length]);
					end
				end
				B0 = horzcat(B0cell{:});
			else
				B0 = zeros([0, sum(obj.length)]);
			end
		end % get.B0()
		
		function B1 = get.B1(obj)
			% B1 = [input(1).B1, input(2).B1, ... ]
			if obj.rowsB1 > 0
				B1cell = cell(obj.numTypes, 1);
				for it = 1 : obj.numTypes
					if ~isempty(obj.input(it).B1)
						B1cell{it} = obj.input(it).B1;
					else
						B1cell{it} = zeros([obj.rowsB1, obj.input(it).length]);
					end
				end
				B1 = horzcat(B1cell{:});
			else
				B1 = zeros([0, sum(obj.length)]);
			end
		end % get.B1()
		
		function D = get.D(obj)
			% D	 =  [input(1).D.of("typeA"), input(2).D.of("typeA"), ...	;]
			%		[input(1).D.of("typeB"), input(2).D.of("typeB"), ...	;]
			%		[						...								 ]
			D = misc.Gains();
			for it = 1 : numel(obj.input)
				if ~isempty(obj.input(it).D)
					D = D + obj.input(it).D;
				end
			end
		end % get.D()
		
		%% set input matrix
		function set.B(obj, B)
			for it = 1 : obj.numTypes
				obj.input(it).B = B(:, sum(obj.length(1:it-1))+(1:obj.length(it)));
			end
		end % set.B()
		
		function set.B0(obj, B0)
			for it = 1 : obj.numTypes
				obj.input(it).B0 = B0(:, sum(obj.length(1:it-1))+(1:obj.length(it)));
			end
		end % set.B0()
		
		function set.B1(obj, B1)
			for it = 1 : obj.numTypes
				obj.input(it).B1 = B1(:, sum(obj.length(1:it-1))+(1:obj.length(it)));
			end
		end % set.B1()
		
		%% test dimensions of all inputs
		function verifySizes(obj)
			lengthAllInputs = sum(obj.length);
			assert(isequal(size(obj.B), [obj.rowsB, lengthAllInputs]));
			assert(isequal(size(obj.B0), [obj.rowsB0, lengthAllInputs]));
			assert(isequal(size(obj.B1), [obj.rowsB1, lengthAllInputs]));
		end % verifySizes
		
		function result = isempty(obj)
			result = obj.numTypes == 0;
		end %isempty()
		
		function flag = isMatrixZeroOrEmpty(obj, varargin)
			flag = false(numel(varargin), 1);
			for it = 1 : numel(varargin)
				testProperty = varargin{it};
				if isempty(obj.(testProperty))
					flag(it) = true;
				else
					if isa(obj.(testProperty), "quantity.Discrete")
						value = obj.(testProperty).on();
					elseif isa(obj.(testProperty), "misc.Gain") ...
							|| isa(obj.(testProperty), "misc.Gains")
						value = obj.(testProperty).value;
					else
						value = obj.(testProperty);
					end
					flag(it) = isempty(value) || (mean(abs(value(:))) == 0);
				end
			end
		end % isMatrixZeroOrEmpty()
		
		function [texString, nameValuePairs] = printParameter(obj)
			% Create TeX-code for all non-zero parameter.
			propList = {'B', 'B0', 'B1'};
			nameList = {'B', 'B_{0}', 'B_{1}'};
			nameValuePairs = {};
			for it = 1 : numel(propList)
				if ~obj.isMatrixZeroOrEmpty(propList{it})
					nameValuePairs{end+1} = nameList{it};
					nameValuePairs{end+1} = obj.(propList{it});
				end
			end % for it = 1 : numel(propList)
			[~, nameValuePairsD] = print(obj.D);
			nameValuePairs = [nameValuePairs, nameValuePairsD];
			texString = misc.variables2tex([], nameValuePairs);
		end % printParameter()
		
		function texString = print(obj, varargin)
			% print Print equation to command window as latex compatible string-array
			% uses implementation of model.Input
			myString = "";
			for it = 1 : obj.numTypes
				myString = myString + obj.input(it).print(varargin{:});
			end % for it = 1 : obj.numTypes
			
			if nargout > 0
				texString = myString;
			else
				misc.printTex(myString);
			end
		end % print()
		
		function obj = discretizeQuantity(obj)
			% discretizeQuantity converts all properties containg quantity-objects to
			% quantity.Discrete objects. This is often speeds up calculations.
			for it = 1 : obj.numTypes
				obj.input(it) = obj.input(it).discretizeQuantity();
			end
		end % discretizeQuantity()
		
		function result = mustBe0(obj, prop)
			% mustBe0 throws an error if not all properties defined by prop are zero
			% or empty.
			result = all(obj.isMatrixZeroOrEmpty(prop));
			assert(result, prop + " must be zero");
		end % mustBe0()
		
	end % methods
	
	methods (Access = protected)
		% Override copyElement method:
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@matlab.mixin.Copyable(obj);
			cpObj.input = copy(obj.input);
		end
	end % methods (Access = protected)
end
