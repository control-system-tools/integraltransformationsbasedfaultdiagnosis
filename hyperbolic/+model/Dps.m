classdef Dps < handle & matlab.mixin.Copyable
	% dps.Dps is an abstract parent class for standard interfaces of different kind of distributed
	% parameter systems (dps). To ensure similar interfaces for initialisation, simulation,
	% visualisation and controller design for dps.
	%
	% See also model.Transport, model.TransportOde, model.Wave, model.WaveOde.
	
	properties (Abstract = true, SetAccess = protected) 
		bc0 (1,1) model.Output;		% left boundary operator at z=0, see model.Output
		bc1 (1,1) model.Output;		% right boundary operator at z=1, see model.Output
		
		% Input gains for control signal, disturbances, faults, etc. are stored in this model.Inputs
		% object. Inputs are defined at the left (B0) and right (B1) boundary and distributed (B).
		% Feedthroughs (D) can also be implemented via the output property. See model.Inputs
		input (1,1) model.Inputs;
		
		% Output operator for control, measurement, PDE -> ODE coupling, etc... Implements boundary, 
		% distributed, pointwise, dirichlet, neumann, and time-derivative outputs.
		output (1,1) model.Outputs;
		
		name;			% name of object
		arglist cell;	% list of input parameters used in the constructor
		prefix string;	% is added to the type of output, the state name and simulation signals
	end
	
	properties (Abstract = true, Hidden = true)
		% name of the matrix defining the dominant PDE dynamics i.e. "Gamma" for the wave velocity
		% of model.Wave or "Lambda" for the transport velocity of model.Transport
		nameCharacteristicMatrix string;
	end % properties (Abstract = true, Hidden = true)
	
	properties (Dependent = true)
		% value of the matrix defining the dominant PDE dynamics i.e. "Gamma" for the wave velocity
		% of model.Wave or "Lambda" for the transport velocity of model.Transport
		characteristicMatrix quantity.Discrete;
	end % properties (Dependent = true)
	
	properties (Constant = true)
		birthday = datestr(now,'yyyy-mm-dd_HH-MM-SS');	% time of creation
	end % properties (Constant = true)
	
	properties (Dependent = true, Abstract = true)
		settlingTime struct;	% struct of the nominal achievable settling time by state feedback
	end % properties (Dependent = true, Abstract = true)
	
	%% Methods
	% Constructor: not needed
	
	methods (Abstract = true) 
		obj = setApproximation(obj, varargin)
		myStateSpace = getClosedLoopSimulationModel(obj, varargin)
		[ic, stateName] = getInitialCondition(obj, varargin)
		[texString, nameValuePairs] = printParameter(obj)
		texString = print(obj, stateNamePde, stateNameOde, doPrintOutput)
		myStateName = stateName(obj)	% returns a struct naming the states, 
		%								% for instance obj.stateName.pde(1) 
		%								%	= 'prefix.x', ...
	end % methods (Abstract = true)
	
	methods (Access = protected, Abstract = true)
		cpObj = copyElement(obj)
	end % methods (Access = protected)
		
	methods
		function [simData, myStateSpace] = simulate(obj, varargin)
			% SIMULATE Perform a simulation. The matlab lsim solver is used which
			% converts the system to a time discrete system and hence is exact in 
			% every discretization point in time.
			%
			% simData = simulate(OBJ) returns a struct containing quantity.Discrete
			% arrays for every signal. For instance, the distributed states are
			% stored in simData.(obj.stateName.pde(1)).
			%
			% [simData, myStateSpace] = simulate(OBJ) also returns the state space 
			% model used for simulation.
			%
			% simulate(OBJ, 't', time) The double array t specifies the time
			% discretization (default: t = linspace(0, 5, 1001)).
			%
			% simulate(OBJ, 'feedback', feedback) add a state feedback, feedback must
			% be a dps.Ouput and its type must coincide with the type of an
			% obj.input, which will be used as control input. Similarily an observer
			% or a signal model can be added.
			% (See model.Wave.getClosedLoopSimulationModel)
			%
			% simulate(OBJ, OBJ.input(it).type, signal) Applies the signal, which is
			% a quantity.Discrete defined over the spatial domain t and is of suiting
			% dimensions to the input OBJ.input(it).
			% (See stateSpace.combineInputSignals)
			%
			% simulate(OBJ, obj.(stateName.pde(1)), x0) Sets the initial condition x0
			% for the distributed state x(z, 0). x0 is a quantity.Discrete.
			% Default: x0 = 0. (See tool.combineInitialCondition)
			%
			%% further options for model.Transport < model.Dps:
			% the spatial discretization results from the step size of t, such that
			% the cfl-number is close to 1 everywhere
			%
			%% further options for model.Wave < model.Dps:
			% simulate(OBJ, 'method', method) with method = "finiteDifferences" or
			% "cheb" specifies the approximation method used for the spatial
			% coordinate. default: "cheb" (See model.Wave.setApproximation)
			%
			% simulate(OBJ, 'zPointSimulation', n) Number n of spatial discretization
			% points used in simulation, default: numel(obj.Gamma(1).domain.grid)
			% (See model.Wave.setApproximation)
			%
			% simulate(OBJ, 'domainResult', domain) domain is the domain to which the
			% distributed states in simData will be interpolated to. It must be a
			% quantity.Domain array that may contain a domain t and / or a domain z.
			% This is usallay used to convert non equidistant grids used for
			% simulation method "cheb" to equidistant grids.
			%
			% simulate(OBJ, obj.(stateName.pde(2)), xt0) Sets the initial condition 
			% xt0 for the distributed state x_t(z, 0). xt0 is a quantity.Discrete.
			% Default: xt0 = 0. (See tool.combineInitialCondition)
			
			% read input and set time discretization
			vararginNoDots = misc.nameValuePairRemoveDots(varargin{:});
			myParserTime = misc.Parser();
			myParserTime.addParameter("t", linspace(0, 4, 201));
			myParserTime.parse(vararginNoDots{:});
			t = myParserTime.Results.t;
			if myParserTime.isDefault("t")
				varargin = [{"t", t}, varargin];
				vararginNoDots = [{"t", t}, vararginNoDots];
			end
						
			% set approximation the spatial discretization
			myStateSpace = obj.getClosedLoopSimulationModel(vararginNoDots{:});
			myStateSpace = stateSpace.addInput2Output(myStateSpace);
			
			% init simulation data
			myIc = tool.combineInitialCondition(myStateSpace, "obj", obj, varargin{:});
			u = stateSpace.combineInputSignals(myStateSpace, t, varargin{:});
			
			% run simulation
			[simOutput, ~] = stateSpace.simulate(myStateSpace, u.on(t), t(:), myIc, "lsim");
			
			% split output of simulation
			simData = stateSpace.simulationOutput2Quantity(...
				simOutput, t, myStateSpace.OutputName, ...
				"z", obj.getSpatialGridsOfSimulation(vararginNoDots{:}), ...
				"predefinedInput", u);
			
			% for model.Wave the pde state is sampled to domainResult
			myParserDomain = misc.Parser();
			x = misc.getfield(simData, obj.stateName.pde(1)); 
			defaultDomain = x(1).domain;
			myParserDomain.addParameter("domainResult", defaultDomain, ...
				@(d) isequal([d.name], ["t", "z"]));
			myParserDomain.parse(vararginNoDots{:});
			simData.domain = myParserDomain.Results.domainResult;
			
			% some objects need some post production of simData, for instance there
			% needs to be a copy of the ode states of an observer
			simData = stateSpace.simulatePostproduction(simData, obj, varargin{:});
		end % simulate()
		
		function plotState(obj, x, varargin)
			% plotState creates 3d plots of the distributed state.
			%
			%	plotState(obj, x) creates a 3d plot for every element of the distributed state x.
			%
			%	plotState(obj, x, "compareWith", xComparison) additional plots xComparision as a
			%		mesh (if animate=false) or in a different color (if animate=true).
			%
			%	plotState(obj, x, "animate", true) creates a animatedline instead, see 
			%		misc.subanimate.
			%
			%	plotState(obj, x, "animate", true, "frames", frames) frames defines the number of
			%		frames shall be used.
			%
			% See also misc.subsurf, misc.subanimate, model.Dps.plotNorm.
			myParser = misc.Parser();
			myParser.addParameter('animate', false);
			myParser.addParameter('frames', x.getDomain.find("t").n);
			myParser.addParameter('compareWith', []);
			myParser.parse(varargin{:});
			
			assert(nargin(x(1)) == 2, "plot state is only defined for 2-dimensional quantities");
			if myParser.Results.animate
				misc.subanimate(x, "frames", myParser.Results.frames, ...
					"compareWith", myParser.Results.compareWith);
			else
				doCompare = ~isempty(myParser.Results.compareWith);
				if doCompare
					compareWith = myParser.Results.compareWith.on(...
						{x.gridOf('t'), x.gridOf('z')}, {'t', 'z'});
				else
					compareWith = [];
				end
				remainingInput = misc.struct2namevaluepair(myParser.Unmatched);
				misc.subsurf(x.on({x.gridOf('t'), x.gridOf('z')}, {'t', 'z'}), ...
					'myTitle', {'$$x$$', '$$(z, t)$$'}, ...
					'grid1', x.gridOf('t'), 'xLabel', 't', ...
					'grid2', x.gridOf('z'), 'yLabel', 'z', ...
					'compareWith', compareWith, remainingInput{:});
			end 
		end % plotState()
		
		function plotNorm(obj, normVector, varargin)
			% plotNorm plots the norm of quantity.Discrete input signals, further options can be
			% given as name-value pairs.
			myParser = misc.Parser();
			myParser.addParameter('maxRealEigenvalue', []);
			myParser.addParameter('compareWith', []);
			myParser.addParameter('tGrid', normVector(1).domain(1).grid);
			myParser.addParameter('decorate', true);
			myParser.parse(varargin{:});
			maxRealEigenvalue = myParser.Results.maxRealEigenvalue;
			referenceNorm = myParser.Results.compareWith;
			
			tc = obj.settlingTime.finite;
			
			figure();
			for it = 1 : numel(normVector)
				subplot(numel(normVector), 1, it);
				t = myParser.Results.tGrid;
				plot(t, normVector(it).on(t), 'LineWidth', 2);
				hold on; grid off;
				if ~isempty(referenceNorm)
					plot(t, referenceNorm(it).on(t), 'LineWidth', 2, 'LineStyle', '--', 'Color', '[0.4660 0.6740 0.1880]');
				end
				myYLim = ylim;
				if myParser.Results.decorate && tc < t(end)
					plot([tc, tc], [myYLim(1), myYLim(2)], 'k--');
					if ~isempty(maxRealEigenvalue)
						expDecay = quantity.Discrete(exp(maxRealEigenvalue * (t - tc)), ...
							quantity.Domain('t', t));
						scale = max(on(normVector(it) * inv(expDecay), t(t>=tc & t<=3*tc)));
						expDecayData = expDecay.on() * scale;
						gain = abs(max(normVector(it).on(t(t>=tc))));
						plot(t(t>=tc), gain*exp(maxRealEigenvalue*(t(t>=tc)-tc)), ...
							'LineWidth', 2, 'LineStyle', '--', 'Color', 'r');
						plot(t(t>=tc), expDecayData(t>=tc), ...
							'LineWidth', 2, 'LineStyle', '--', 'Color', 'm');
					end
				end
				ylim(myYLim);
				xlim([t(1), t(end)]);
				xlabel("$$t$$", 'Interpreter','latex');
				title("$$" + normVector(it).name + "$$", 'Interpreter','latex');
			end
		end % plotNorm()
				
		function obj = setOutputName(obj)
			% setOutputName ensures that obj.output and obj odeInput types start with the string 
			% obj.prefix.
			%
			% See also model.Dps.resetOutputName, model.Dps.resetInputName.
			for it = 1 : obj.output.numTypes
				if ~contains(obj.output.type(it), obj.prefix + ".")
					obj.output.output(it).extendType('front', obj.prefix + ".");
				end
			end
			if isprop(obj, "odeInput") && ~isempty(obj.odeInput)
				for it = 1 : numel(obj.odeInput.outputType)
					if ~contains(obj.odeInput.outputType(it), obj.prefix + ".")
						obj.odeInput.strrepOutputType(obj.odeInput.outputType(it), ...
							obj.prefix + "." + obj.odeInput.outputType(it));
					end
				end
			end % isprop(obj, "odeInput") && ~isempty(obj.odeInput)
		end % setOutputName()
		
		function obj = resetOutputName(obj, oldPrefix, newPrefix)
			% resetOutputName replaces oldPrefix with newPrefix in all obj.output.
			%
			% See also model.Dps.setOutputName, model.Dps.resetInputName.
			for it = 1 : obj.output.numTypes
				if ~contains(obj.output.type(it), oldPrefix + ".")
					obj.output.output(it).extendType("front", newPrefix + ".");
				else
					obj.output.output(it).strrepType(oldPrefix+".", newPrefix+".");
				end
			end
		end % resetOutputName()
		
		function obj = resetInputName(obj, oldPrefix, newPrefix)
			% resetInputName replaces oldPrefix with newPrefix in all obj.input.
			%
			% See also model.Dps.setOutputName, model.Dps.resetOutputName.
			for it = 1 : obj.input.numTypes
				if contains(obj.input.type(it), oldPrefix+".")
					obj.input.input(it).strrepType(oldPrefix+".", newPrefix+".");
				end
			end
		end % resetInputName()
				
		function newObj = copyAndReplace(oldObj, varargin)
			% copyButReplace creates a copy of the object based on obj.arglist.
			% However, properties specified by a name-value-pair in varargin replace
			% the old objects properties.
			
			myParser = misc.Parser();
			myParser.KeepUnmatched = false;
			for it = 1:numel(oldObj.arglist)
				thisArg = oldObj.arglist{it};
				if any(strcmp(["pde2ode", "ode2pde"], thisArg))
					% do nothing, since these are dependend parameters stored in input and output.
					% an error is thrown below, if pde2ode and ode2pde are specified via varargin
				elseif isa(oldObj.(thisArg), 'matlab.mixin.Copyable')
					myParser.addParameter(thisArg, copy(oldObj.(thisArg)));
				else
					myParser.addParameter(thisArg, oldObj.(thisArg));
				end
			end
			
			% get characteristic matrix
			vararginNames = string(varargin(1:2:end));
			idx = find(strcmp(oldObj.nameCharacteristicMatrix, vararginNames));
			if ~isempty(idx)
				characteristicMatrix = varargin{2*idx}.copy();
				varargin = varargin(setdiff(1:1:numel(varargin), 2*idx + [-1, 0]));
			else
				characteristicMatrix = oldObj.characteristicMatrix.copy();
			end
			
			myParser.parse(varargin{:});
			assert(isempty(fieldnames(myParser.Unmatched)), ...
				"there are unmatched name-value inputs: " + fieldnames(myParser.Unmatched));
			if ~strcmp(oldObj.prefix, myParser.Results.prefix)
				% update prefix
				myParser.Results.output.strrepType(...
					oldObj.prefix+".", myParser.Results.prefix+".", "startOnly", true);
				myParser.Results.input.strrepType(...
					oldObj.prefix+".", myParser.Results.prefix+".", "startOnly", true);
				if isfield(myParser.Results, "odeInput")
					myParser.Results.odeInput.strrepInputType(...
						oldObj.prefix+".", myParser.Results.prefix+".", "startOnly", true);
					myParser.Results.odeInput.strrepOutputType(...
						oldObj.prefix+".", myParser.Results.prefix+".", "startOnly", true);
				end
			end % ~strcmp
			newObj = misc.constructorOf(oldObj, characteristicMatrix, ...
				myParser.ResultsNameValuePair{:});
		end % copyAndReplace()
		
		function characteristicMatrix = get.characteristicMatrix(obj)
			% get.characteristicMatrix getter for dependent property characteristicMatrix.
			characteristicMatrix = obj.(obj.nameCharacteristicMatrix);
		end % characteristicMatrix
		
	end % methods 
	
	methods (Static = true)
		
		function domainSelector = integralDomain(bounds, domain)
			% domainSelector creates a quantity.Discrete on the domain, that is 1 for all points in
			% between bounds.
			arguments
				bounds cell;
				domain quantity.Domain;
			end % arguments
			
			assert(isequal([domain.name], ["z", "zeta"]) || isequal([domain.name], ["zeta", "z"]), ...
				"domain must be defined for z and zeta");
			domain = domain.find("z", "zeta"); % sort domain
			
			if isequal(bounds, {0, 1})
				domainSelector = quantity.Discrete.ones([1, 1], domain);
			elseif isequal(bounds, {0, "z"}) || isequal(bounds, {0, 'z'})
				ndGrids = domain.ndgrid;
				domainSelector = quantity.Discrete(double(ndGrids{1} >= ndGrids{2}), domain);
			elseif isequal(bounds, {"z", 1}) || isequal(bounds, {'z', 1})
				ndGrids = domain.ndgrid;
				domainSelector = quantity.Discrete(double(ndGrids{1} <= ndGrids{2}), domain);
			elseif isnumeric(bounds{1}) && isnumeric(bounds{2})
				ndGrids = domain.ndgrid;
				domainSelector = quantity.Discrete(double(...
					bounds{1} <= ndGrids{2} & bounds{2} >= ndGrids{2}), domain);
			else
				error("unknown domain-bounds: " + string(bounds{1}) + ", " + string(bounds{2}));
			end
			
		end % integralDomain()
		
	end % methods (Static = true)
end % classdef: model.Dps
