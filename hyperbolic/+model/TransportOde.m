classdef TransportOde < model.Transport & handle & matlab.mixin.Copyable
% model.TransportOde implements n coupled first order hyperbolic equations that are coupled with an 
% ODE.
% This implements hyperbolic systems of the form
%
%	x_t(z,t) = Lambda(z) x_z(z,t) + A(z) x(z,t) + A0(z) x_1(0,t) ...
%				+ int_a^b F(z,zeta) x(zeta,t) dzeta + input.B(z) u(t)
%				+ ode2pde.B(z) w(t)
%          0 = bc0(x) + input.B0 u(t) + ode2pde.B0 w(t)
%          0 = bc1(x) + input.B1 u(t) + ode2pde.B1 w(t)
%	  w_t(t) = ode.A w(t) + ode.B pde2ode[x] + odeInput u(t) 
%		y(t) = output[x(z,t), w(t)] + input.D u(t)
%
% and the initial conditions x(z,0) = x0(z), w(0) = w0.
% The distributed state is x(z, t) \in R^n, with the spatial variable z in [0, 1] and the 
% temporal variable t. The state vector can be splitted into x = col(x_1, x_2), with
%
%	x_1(z, t) = e1.' x(z, t) \in R^p describing the states related to transport from z=1 to z=0,
%	x_2(z, t) = e2.' x(z, t) \in R^m describing the states related to transport from z=0 to z=1.
%
% The lumped ODE-state is w.
% Usually, the boundary conditions are described by the operators bc0 and bc1, that implement
%
%	x_2(0,t) = Q0 x_1(0,t) + input.B0 u(t)
%	x_1(1,t) = Q1 x_2(1,t) + input.B1 u(t).
%
% However, the operatores bc0 and bc1 allow to define additional integral terms or other
% couplings, see model.Output. The vector u(t) contains all input signals, for instance control
% input,  disturbances or faults. y(t) is a vector of output signals, for instance the control 
% output or measurements. Outputs can define general outputs
%
%	output[h, w] = sum_{i=1}^{l} [F_i * h(z_i)] + int_0^1 [C(z) h(z)] dz + D w
%
% see model.Outputs.
% The bounds of the integral in the PDE is defined by Fbounds = {a, b} wherein at a, b can be
% spatial values in [0, 1] or a string "z".
% The coupling from the PDE to the ODE is implemented with the pde2ode model.Output-object, which is
% part of the output property. Hence, it can define boundary and indomain coupling, see 
% model.Output.
% The coupling from the ODE to the PDE is implemented with the ode2pde model.Input-object, which is
% part of the input property. Hence, it can define coupling at the boundaries and indomain,
% see model.Input.
% Additional inputs at the ODE, for instance control input, disturbances, faults, ... are defined
% via the odeInput-property, which is a misc.Gains-object, see misc.Gain.
%
% See also model.TransportOde.TransportOde (constructor), model.Dps, model.Transport, model.Wave,
%	model.WaveOde.
	
	properties (SetAccess = protected)
		ode ss;					% ODE-subsystem defined as a state-space model
		odeInput misc.Gains;	% input of ODE-subsystem.
	end % properties (SetAccess = protected)
	
	properties (Dependent = true)
		% Coupling from the PDE to the ODE as model.Output-object, which is part of the output 
		% property. Hence, it can define boundary and indomain coupling, see model.Output.
		pde2ode (1,1) model.Output;
		% Coupling from the ODE to the PDE as a ode2pde model.Input-object, which is part of the
		% input property. Hence, it can define coupling at the boundaries and indomain,
		% see model.Input.
		ode2pde (1,1) model.Input;
	end % properties (Dependent = true)
	
	methods
		function obj = TransportOde(Lambda, varargin)
			% model.TransportOde.TransportOde constructs a model.TransportOde object, which 
			% describes a nxn coupled hyperbolic system, coupled with an ODE.
			%
			%	See the parent class constructor model.Transport.Transport on how to define 
			%		PDE-parameter, boundary conditions, name, prefix, input and output.
			%
			%	obj = model.TransportOde(Lambda, [...], "ode", ode) creates a model.TransportOde
			%		object, with the ODE-subsystem described by the state space model ode.
			%
			%	model.TransportOde([...], "odeInput", odeInput) sets input matrices for external
			%		signals as disturbances as an misc.Gains object, see misc.Gain and misc.Gains.
			%
			%	model.TransportOde([...], "pde2ode", pde2ode) sets the output operator for coupling
			%		of the PDE to the ODE as a model.Output-object. Alternatively, pde2ode can be
			%		included in the output input-argument. The type of the pde2ode model.Output
			%		object must be "pde2ode" or prefix + ".pde2de".
			%
			%	model.TransportOde([...], "ode2pde", ode2pde) sets the input matrices for coupling
			%		of the ODE to the PDE as a model.Input-object. Alternatively, ode2pde can be
			%		included in the input input-argument. The type of the ode2pde model.Output
			%		object must be "ode2pde" or prefix + ".ode2pde".
			%
			% See also model.TransportOde (class description and equations), model.Dps, 
			% model.Transport.
			
			obj@model.Transport(Lambda, varargin{:});
			if nargin > 0
				% input parser
				myParser = misc.Parser();
				myParser.addParameter("ode", ss(), @(v) isa(v, "ss"));
				myParser.addParameter("odeInput", []);
				myParser.addParameter("pde2ode", model.Output(obj.prefix + ".pde2ode"), ...
					@(v) isa(v, "model.Output"));
				myParser.addParameter("ode2pde", model.Input(obj.prefix + ".ode2pde"), ...
					@(v) isa(v, "model.Input"));
				myParser.parse(varargin{:});
				obj.arglist = vertcat(obj.arglist, fieldnames(myParser.Results));
				
				% write from parser to object
				obj.ode = myParser.Results.ode;
				if isempty(myParser.Results.odeInput)
					obj.odeInput = misc.Gains();
				else
					obj.odeInput = myParser.Results.odeInput.copy();
				end
				
				if ~isempty(obj.pde2ode)
					% pde2ode is already part of obj.output
					if ~any(strcmp(myParser.UsingDefaults, "pde2ode"))
						error("pde2ode was already specified in output")
					end
				else
					obj.output = obj.output.add(myParser.Results.pde2ode.copy());
				end
				
				if any(contains(obj.input.type, "ode2pde"))
					% ode2pde is already part of obj.input, hence assure that it is
					% not defined multiple times. Furthermore, it has the wrong type,
					% so that is corrected.
					if ~any(strcmp(myParser.UsingDefaults, "ode2pde"))
						error("ode2pde was already specified in input")
					end
					ode2pdeIdx = contains(obj.input.type, "ode2pde");
					assert(sum(ode2pdeIdx)==1, "there must be only one ode2pde input");
					if strcmp(obj.input.input(ode2pdeIdx).type, "ode2pde")
						obj.input.input(ode2pdeIdx).setPrefix(obj.prefix);
					else
						assert(strcmp(obj.input.input(ode2pdeIdx).type, obj.prefix + ".ode2pde"), ...
							"ode2pde must have type 'ode2pde' or '[prefix].ode2pde'");
					end
					
				else
					assert(contains(myParser.Results.ode2pde.type, "ode2pde"), ...
						"ode2pde has wrong type property");
					if strcmp(myParser.Results.ode2pde.type, "ode2pde")
						myParser.Results.ode2pde.setPrefix(obj.prefix);
					else
						assert(strcmp(myParser.Results.ode2pde.type, obj.prefix + ".ode2pde"), ...
							"ode2pde must have type 'ode2pde' or '[prefix].ode2pde'");
					end
					obj.input = obj.input.add(myParser.Results.ode2pde.copy());
				end % if any(contains(obj.input.type, "ode2pde"))
				
				%% add prefix to output
				obj = obj.setOutputName();
				
				%% Check parameter
				% Check ode
				assert(misc.iseye(obj.ode.C), "Coupling between ode and wave is considered via" ...
					+ " pde2ode output and ode2pde input, hence the output matrix of " ...
					+ "the ode must be an identity matrix.");
				assert(misc.iseye(obj.ode.B), "Coupling between ode and wave is considered via " ...
					+ "pde2ode output and ode2pde input, hence the output matrix of " ...
					+ "the ode must be an identity matrix.");
				
				% Check pde2ode
				assert(~any(strcmp(obj.output.type, "pde2ode")), ...
					"Property type of pde2pde must be '[obj.prefix].pde2ode'");
				assert(isempty(obj.pde2ode.input), "input property of pde2ode must be empty");
				if (obj.pde2ode.lengthOutput ~= 0) || (obj.pde2ode.lengthState ~= 0)
					assert(obj.pde2ode.lengthOutput == size(obj.ode.B, 2), ...
						"lengthOutput of pde2ode must be equal to size(ode.B, 2)");
					assert(obj.pde2ode.lengthState == size(obj.(obj.nameCharacteristicMatrix), 1), ...
						"lengthState of pde2ode must be equal to number of states of pde");
				end % if ~isempty(obj.pde2ode)
				
				% Check ode2pde
				assert(isempty(obj.ode2pde.D), "D property of ode2pde must be empty");
				assert((obj.ode2pde.length == size(obj.ode.C, 1)) || (obj.ode2pde.length == 0), ...
					"length of ode2pde must be equal to size(ode.C, 1)");
				
				% Check odeInput
				if ~isempty(obj.odeInput)
					assert((numel(obj.odeInput.outputType) == 1) ...
						&& strcmp(obj.odeInput.outputType, obj.prefix + ".odeInput"), ...
						"Property outputType of odeInput must be '[obj.prefix].odeInput'");
					assert(size(obj.ode, 2) == obj.odeInput.lengthOutput, ...
						"odeInput must fit to ode-input-size");
				end
				
			end % nargin > 0
		end % TransportOde() Constructor
		
		function dzSys = faultdiagnosis.hyperbolic.heteroDirectional.System(obj)
			% convert the coupled pde-ode system which is written with
			% respect to dt into the form with respect to dz.
			% To be particular, the system in the form
			%
			% 	x_t(z,t) = Lambda(z) x_z(z,t) + A(z) x(z,t) + 
			%					  + int_a^b F(z,zeta) x(zeta,t) dzeta 
			%					  + input('f').B f(t)
			%					  + ode2pde.B(z) w(t)
			% 	  w_t(t) = ode.A w(t) + eye(.) pde2ode.C0 x(0) + odeInput('f') f(t)
			% 	x_2(0,t) = Q0 x_1(0,t) + ode2pde.B0 w(t) + input('f').B0 f(t)
			% 	x_1(1,t) = Q1 x_2(1,t) + input('u').B1 u(t) + input('f').B1 f(t)
			%
			% will be converted into the form
			%	
			%	x_z = Lambda^-1 x_t - Lambda^-1 A x 
			%			- int_0^z Lambda^-1 F(z,zeta) x(zeta) d_zeta
			%			- Lambda^-1 input('f').B w(t)
			%			- Lambda^-1 ode2pde.B(z) f(t)
			%	  w_t(t) = F w(t) + pde2ode.C0 * J_p' * x_p(0) + E_4 f(t)
			%  x_\n(0,t) = K_0 x_\p(0,t) + H_2 w(t) + E_2 f(t)
			%  x_\p(1,t) = K_1 x_\n(1,t) + E_3 f(t) + u(t)			
			
			% dimensions of the original system:
			n_w = size( obj.ode.A, 1);
			n   = originalSystem.n; % number of states
			
			J_p = originalSystem.e1.';
			J_n = originalSystem.e2.';	
			
			invLambda = inv(obj.Lambda);
			
			% verify the L_2 value:
			%	obj.pde2ode.C0 * x(0) == L_2 x_p(0)
			%	L_2 == C0 * J_p' with the requirement C - *J_n' == 0
			if ~all( size( obj.pde2ode.C0 ) == [n_w, n] )
				obj.pde2ode.C0 = zeros(n_w, n);
			end
			assert( all( obj.pde2ode.C0 * J_n.' == 0) );
			L_2 = obj.pde2ode.C0 * J_p.';
			
 			dzSys =	faultdiagnosis.hyperbolic.heteroDirectional.System( ...
 				invLambda, obj.ode.A, 'A', -invLambda*obj.A, ...
				'D', -invLambda * obj.F, ...
				'H_1', -invLambda * obj.input.get('f').B, ...
				'E_1', -invLambda * obj.ode2pde.B, ...
				'L_2', L_2, ...
				'E_4', obj.odeInput.get('f'), ...
				'K_0', obj.Q0, 'K_1', obj.Q1, ...
				'H_2', obj.ode2pde.B0, ...
				'E_2', obj.input.get('f').B0, ...
				'E_3', obj.input.get('f').B1);
			
		end % faultdiagnosis.hyperbolic.heteroDirectional.System(obj)
		
		function numerator = numeratorOfTransferFunction(obj, s, outputType)
			% numeratorOfTransferFunction calculates the numerator of the
			% transferfunction of the hyperbolic system obj for all complex
			% frequencies defined by s from the input defined by  inputType to the
			% output defined by outputType.
			% It is assumed that the system obj describes systems of the kind
			% PDE          x_t(z,t) = Lambda(z) x_z(z,t) + A0(z) x_1(0,t)
			% BC z=0	   x_2(0,t) = Q0 x_1(0,t)
			% BC z=1	   x_1(1,t) = u(t)
			% ODE			 w_t(t) = A w(t) + B x_1(0,t)
			% Output		   y(t) = C_op[x(z,t)] + Cw w(t).
			% Then the numerator can be calculated using the following calculations.
			arguments
				obj model.Transport;
				s;
				outputType = obj.prefix + ".controlOutput";
			end
			% verify that the system has the structure specified above:
			assert(all(obj.ode2pde.isMatrixZeroOrEmpty('B', 'B0', 'B1', 'D')), ...
				"PDE must be decoupled from ODE");
			assert(all(obj.pde2ode.isMatrixZeroOrEmpty(...
				'Cz0', 'Ct0', 'C1', 'Ct1', 'Cz1', 'Cz', 'Ct', 'Czk', 'Ctk', 'Ck')), ...
				"only implemented for pde2ode.C0 = [B 0]");
			assert(all(obj.pde2ode.C0 * obj.e2 == 0, "all"), ...
				"only implemented for pde2ode.C0 = [B 0]");
			
			% first, calculate contribution of distributed output in superclass
			numerator = numeratorOfTransferFunction@model.Transport(...
				obj, s, outputType);
			
			% get s from later result (it got sorted and verified in superclass)
			s = 1i * numerator(1).domain.grid;
			sQuantity = quantity.Discrete(s, numerator(1).domain);
			
			% calculate numerator
			A = obj.ode.A;
			B = obj.pde2ode.C0 * obj.e1;
			if any(strcmp(obj.output.get(outputType).input.inputType, obj.stateName.ode))
				C = obj.output.get(outputType).input.get(obj.stateName.ode).value;
			else
				C = zeros([size(B, 2), size(A, 1)]);
			end
			I = eye(size(A));
			
			numerator = numerator + C * inv(sQuantity * I - A) * B;
			numerator.setName("numerator");
		end % numeratorOfTransferFunction()
		
		function simData = simulatePostproduction(obj, simData)
			% simulatePostproduction ensures, that the simulation output struct simData contains a
			% signal that is named as the obj.ode2pde input, i.e. it copies
			% simData.(obj.stateName.ode) to simData.(obj.ode2pde.type).
			simData = misc.setfield(simData, obj.ode2pde.type, ...
						misc.getfield(simData, obj.stateName.ode));
		end % simulatePostproduction()
		
		function [K, Acl] = place(obj, desiredEigenValues)
			% place performes an eigenvalue placement of the ODE-subsystem.
			if any(real(desiredEigenValues)>=0)
				warning("do you really want to assign eigenvalues with real() >= 0?");
			end
			myB = obj.ode.B * obj.pde2ode.C0 * obj.e1;
			try
				warning("off")
				K = misc.placeLQR(obj.ode.A, myB, desiredEigenValues);
				warning("on")
			catch
				warning("misc.placeLQR failed")
			end
			[msgstr, ~] = lastwarn();
			if contains(msgstr, "Poles are not placed accurately!") ...
					|| contains(msgstr, "misc.placeLQR failed")
				fprintf("> try placing again with matlab built-in place...\n");
				K = misc.place(obj.ode.A, myB, desiredEigenValues);
			end
			Acl = obj.ode.A - myB * K;
		end % place()
		
		function myStateSpace = addConvection2SimulationModel(obj, ...
				stepSize, myContinuousStateSpace, convectionShift, varargin)
			% addConvection2SimulationModel converts the time continuous state space
			% myContinuousStateSpace to a time discrete model and adds the convection
			% terms, which are implemented by shift-matrix convectionShift.
			% Additionally, the ode is added to the simulation model.
			%
			% See also setApproximation, getClosedLoopSimulationModel, simulate, combineSsPdeOde.
			myStateSpacePde = addConvection2SimulationModel@model.Transport(obj, ...
				stepSize, myContinuousStateSpace, convectionShift, varargin{:});
			
			% add ode of system
			myStateSpace = combineSsPdeOde(obj, myStateSpacePde);
			
			% add ode of observer
			myParser = misc.Parser();
			myParser.addParameter("observer", []);
			myParser.parse(varargin{:});
			if ~isempty(myParser.Results.observer)
				myStateSpace = combineSsPdeOde(...
					myParser.Results.observer.dynamics, myStateSpace);
			end
				
		end % addConvection2SimulationModel()
				
		function myStateSpace = combineSsPdeOde(obj, pdeSs)
			% combineSsPdeOde connect the state-space model of the approximated PDE-dynamics and the 
			% ODE-subsystem.
			pdeSs = stateSpace.changeSignalName(pdeSs, obj.ode2pde.type, obj.stateName.ode);
			odeSs = c2d(obj.ode, pdeSs.Ts);
			odeSs.OutputName = obj.stateName.ode;
			odeSs = stateSpace.setSignalName(odeSs, "input",  ...
				obj.prefix + ".odeInputInternal", size(odeSs, 2));
			if (numel(obj.odeInput)>0) && (obj.odeInput.numTypes > 0)
				odeSum = sumblk(char(...
					obj.prefix + ".odeInputInternal = " + obj.prefix + ".pde2ode + " ...
					+ string(obj.odeInput.outputType2sumblk())), size(odeSs, 2));
				odeInputGainSs = obj.odeInput.gain2ss();
			else
				odeSum = sumblk(char(...
					obj.prefix + ".odeInputInternal = " + obj.prefix + ".pde2ode"), size(odeSs, 2));
				odeInputGainSs = ss();
			end
			
			approximationOutput = [pdeSs.OutputName; odeSs.OutputName];
			
			approximationInput = [pdeSs.InputName(...
				~strcmp(stateSpace.removeEnumeration(pdeSs.InputName), obj.stateName.ode)); ...
				odeInputGainSs.InputName];
			approximationInput = setdiff(approximationInput, approximationOutput);
			
			myStateSpace = stateSpace.connect(...
				approximationInput, approximationOutput, pdeSs, odeSs, odeInputGainSs, odeSum);
		end % combineSsPdeOde()
		
		function [ic, stateName] = getInitialCondition(obj, varargin)
			% getInitialCondition reads varargin if there are initial conditions
			% defined as name-value-pair with the name according to obj.stateName and
			% returns 2 cell arrays: 
			%	1. ic is a cell array of the values of the states initial condition
			%	2. stateName is a cell array of the names of those states.
			
			icStruct = misc.struct(varargin{:});
			
			% Create initial condition for all states specified by obj.stateName
			% If a value is specified in icStruct, than this value is used.
			% Otherwise, a default value is considered.
			[ic, stateName] = getInitialCondition@model.Transport(obj, icStruct);
			if misc.isfield(icStruct, obj.stateName.ode)
				w0 = misc.getfield(icStruct, obj.stateName.ode);
			else
				w0 = zeros(size(obj.ode.A, 1), 1);
			end
			assert(numel(w0) == size(obj.ode.A, 1), ...
				"vector of odes initial condition has wrong length");
			
			% Create output parameter
			ic = [ic(:); w0];
			stateName = [stateName(:); obj.stateName.ode];
		end % getInitialCondition()
		
		function obj = setOutputName(obj)
			% setOutputName ensures that obj.output and obj odeInput types start with the string 
			% obj.prefix.
			%
			% See also model.Dps.setOutputName.
			setOutputName@model.Transport(obj);
			if ~isempty(obj.odeInput)
				for it = 1 : numel(obj.odeInput.outputType)
					if ~contains(obj.odeInput.outputType(it), obj.prefix + ".")
						obj.odeInput.strrepOutputType(obj.odeInput.outputType(it), ...
							obj.prefix + "." + obj.odeInput.outputType(it));
					end
				end
			end
		end % setOutputName()
		
		function obj = resetOutputName(obj, oldPrefix, newPrefix)
			% resetOutputName replaces oldPrefix with newPrefix in all obj.output.
			%
			% See also model.Dps.resetOutputName.
			resetOutputName@model.Transport(obj, oldPrefix, newPrefix);
			if ~isempty(obj.odeInput)
				obj.odeInput.strrepOutputType(oldPrefix+".", newPrefix+".");
				assert(all(contains(obj.odeInput.outputType, [newPrefix, '.'])), ...
					'outputType of odeInput is faulty');
			end
		end % resetOutputName()
		
		function [texString, nameValuePairs] = printParameter(obj, varargin)
			% printParameter displays a list of all parameters in the command window.
			%
			% See also model.Transport.printParameter, misc.variables2tex, misc.variables2tex, 
			% misc.latexChar, print.
			[~, nameValuePairsWave] = printParameter@model.Transport(obj);
			nameValuePairs = {
				'F', obj.ode.A, ...
				'B', obj.pde2ode.C0 * obj.e1, ...
				'C', obj.ode2pde.B0, ...
				};
			texString = misc.variables2tex([], nameValuePairsWave{:}, nameValuePairs{:}, varargin{:});
		end % printParameter()
		
		function texString = print(obj, stateNamePde, stateNameOde, doPrintOutput)
			% print Print equation to command window as latex compatible string-array.
			%
			% See also misc.variables2tex, misc.variables2tex, misc.latexChar, printParameter.
			
			arguments
				obj;
				stateNamePde = strrep(obj.stateName.pde(1), obj.prefix + ".", "");
				stateNameOde = strrep(obj.stateName.ode, obj.prefix + ".", "");
				doPrintOutput = true;
			end % arguments
			
			% call parent class to get PDE + BCs, but not outputs.
			myStringArray = print@model.Transport(obj, stateNamePde, stateNameOde, false);
			
			% add ODE
			myOdeString = "\dot{" + stateNameOde + "}(t) &= " ...
				+ misc.latexChar(obj.ode.A) + stateNameOde + "(t)";
			pde2odeString = obj.pde2ode.print("none", stateNamePde);
			if ~isempty(pde2odeString) && ~strcmp(pde2odeString, "")
				myOdeString = myOdeString + " + " + pde2odeString;
			end
			if ~isempty(obj.odeInput)
				odeInputString = obj.odeInput.print(obj.odeInput.outputType);
				if ~isempty(odeInputString) && ~strcmp(odeInputString, "")
					myOdeString = myOdeString + " + " + odeInputString;
				end
			end % if ~isempty(obj.odeInput)
			myStringArray = [myStringArray; myOdeString];
			
			% add output
			if doPrintOutput
				myInput = obj.input.copy.remove(obj.ode2pde.type);
				outputWithoutPde2ode = obj.output.copy.remove(obj.pde2ode.type);
				outputWithoutPde2ode = outputWithoutPde2ode.add(myInput.D);
				myStringArray = [myStringArray; outputWithoutPde2ode.print()];
			end
			
			% set output parameter or print
			if nargout > 0
				texString = myStringArray;
			else
				misc.printTex(myStringArray);
			end
		end % print()
		
		%% get methods
		function pde2ode = get.pde2ode(obj)
			% get.pde2ode reads pde2ode from obj.output parameter.
			pde2ode = obj.output.output(obj.output.index(obj.prefix + ".pde2ode"));
		end % get.pde2ode()
		
		function ode2pde = get.ode2pde(obj)
			% get.ode2pde reads ode2pde from obj.input parameter.
			ode2pde = obj.input.input(obj.input.index(obj.prefix + ".ode2pde"));
		end % get.ode2pde()
		
		function myStateName = stateName(obj)
			% stateName getter of stateName struct.
			if isscalar(obj)
				myStateName = stateName@model.Transport(obj);
				myStateName.ode = obj.prefix + ".w";
			else
				myStateNameCell = cell(size(obj));
				for it = 1 : numel(obj)
					myStateNameCell{it} = obj(it).stateName;
				end
				myStateName = cat(1, myStateNameCell{:});
			end
		end % stateName()
	end % methods
		
	methods (Access = public)
		function [x, w, u, x1_0] = setPointChange(obj, y_f, tau, NameValue )
			% SETPOINTCHANGE flatness based trajectory planning
			%	[X, W, U, X1_0] = setPointChange(obj, Y_F, TAU ) will use a
			%	flatness based approach to plan a trajectory for the
			%	system. The system must be in the form
			%		dt x = Lambda x + A_0 x_1(0)
			%	  x_2(0) = Q_0 x_1(0)
			%	  x_1(1) = u
			%	 dt w(t) = A w(t) + B x_1(0)
			%
			%	By default, the virtual flat output is choosen in the frequency domain as
			%		x_1(0,s) = det(sI-A) y_f(s).
			%	Alternatively, by using the "odeOutputGain" and "flatOutputChoice" options, see
			%	below, a flat output can be defined only for the ODE subsystem via
			%		y_f(s) = odeOutputGain * w(s).
			%	This yields a trajectory for the ODE-input x_1(0, t). Then, a trajectory for the
			%	PDE-subsystem is planned, which implements this trajectory for x_1(0, t).
			% 
			%	[x, w, u, x1_0] = setPointChange(obj, y_f, tau) calculates the trajectory of the 
			%		pde states X, the ode state W,  the control input U, and the boundary value 
			%		x_1(0,t) = x1_0, such that the trajectory Y_F is realized. All those
			%		quantites must be quantity.Discrete objects. Additionally, the quantity.Domain
			%		object TAU defines the time domain, which must be sufficiently larger then the
			%		domain of y_f, due to the predictions and delay operators
			%
			%	simData = setPointChange([...], "struct", true) returns X and U as fields of the
			%		struct simData. Additionally, all outputs defined by obj.output will be fields
			%		of simData.
			%
			%	simData = setPointChange([...], "struct", true, "output2struct", false) deactivates,
			%		that further output signals are calculated and stored in simData.
			%
			%	setPointChange([...], "controlInputName", controlInputName) the string
			%		controlInputName defines which obj.input will be used as control input. It will
			%		be used for verifying, that the control input is defined as x_1(1) = u.
			%
			%	setPointChange([...], "flatOutputChoice", "ODE-output", "odeOutputGain", Cw) plans
			%		the trajectory directly for the ODE-output 
			%			y_f = Cw * odeState.
			%		In contrast to the other cases, the the ODE-output tracks the trajectory of the
			%		flat output exaclty, and not only in the steady state. However, no PDE-outputs
			%		can be considered in this case. If rank(Cw) < obj.p, the additional degrees of
			%		freedoms can be used, see "dof" input argument.
			%
			%	setPointChange([...], "flatOutputChoice", "ODE-output", "odeOutputGain", Cw, ...
			%			"dof", dof) not only implements exact tracking for y_f = Cw * odeState, but
			%		also uses further degrees of freedoms ("dof"), to ensure
			%			dof * x1_0 = 0.
			%		This allows, to specify additional outputs, for instance y = C[x, w] to be zero
			%		in the set points. In this case, x and w can be expressed via the
			%		delay/predictor parametrization of the flat PDE system as [x, w] = f(x1_0). By
			%		inserting the parametrization of the states into the output operator and
			%		considering time t -> infinity, the input parameter dof
			%			0 = C[f(x1_0(t->inf)] = dof * x1_0(t -> inf)
			%		results as a matrix.
			%
			% See also model.Transport.setPointChange, model.TransportOde.setPointChange4output and
			% stateSpace.setPointChange.
			arguments
				obj
				y_f ;
				tau quantity.Domain;
				NameValue.struct (1, 1) logical = false;
				NameValue.output2struct (1, 1) logical = true;
				NameValue.controlInputName (1, 1) string = obj.prefix + ".control";
				NameValue.flatOutputChoice (1, 1) string = "default";
				NameValue.dof (:, :) double = [];
				NameValue.odeOutputGain (:, :) double;
			end % arguments
			
			% input verifications	
			assert( isempty(obj.ode2pde) || (obj.ode2pde.length == 0) || ...
				all(obj.ode2pde.isMatrixZeroOrEmpty("B", "B0", "B1", "D")), ...
				"ode2pde coupling must not exist");
 			assert( all(obj.pde2ode.isMatrixZeroOrEmpty(...
				"C", "Cz", "Ct", "Cz0", "Ct0", "C1", "Cz1", "Ct1", "D", "Czk", "Ck", "Ctk")),...
				"all but obj.pde2ode.C0 must be zero");
			assert( all(obj.pde2ode.C0 * obj.e2 == 0, "all"), ...
				"only x_1(0, t) is allowed to couple into the ode, hence obj.pde2ode.C0 * e2 == 0");
			
			t = y_f.domain;	
			
			% compute the ode part
			if strcmp(NameValue.flatOutputChoice, "default")
				% initialize the polynomial operator corresponding to the
				% state-space system
				%	P = sI - A
				P = signals.PolynomialOperator({ -obj.ode.A, eye(size(obj.ode.A)) }, "name", "P");
				% compute the x_1(0,t) signal
				%	x_1(0,t) = det(P)[ y_f(t) ];
				x1_0 = det(P).applyTo(y_f, "domain", t);
				% compute the state of the ode sub-system
				%	w(t) = adj(sI - A) * B * y_f(s)
				B = obj.ode.B * obj.pde2ode.C0 * obj.e1;
				w = P.adj.applyTo( B * y_f );
				
			elseif strcmp(NameValue.flatOutputChoice, "ODE-output")
				pde2odeX1_0 = obj.ode.B * obj.pde2ode.C0 * obj.e1;
				sysFlat = stateSpace.FlatSystem(obj.ode.A, pde2odeX1_0, NameValue.odeOutputGain);
				uHelper = sysFlat.parametrizeInput(y_f, "mode", "minimum");
				x1_0 = pinv([pde2odeX1_0; NameValue.dof]) ...
					* [sysFlat.B * uHelper; uHelper(1)*zeros(size(NameValue.dof, 1), 1)];
				residuumTmp = MAX(abs(sysFlat.B * uHelper - pde2odeX1_0 * x1_0));
				assert(residuumTmp < 1e-9, ...
					"The trajectory design is faulty (error: " + string(residuumTmp) ...
					+ "), as the trajectory of x_1(0, t) will not " ...
					+ "ensure that the ODE-states track the desired trajectory.");
				if ~isempty(NameValue.dof)
					residuumTmp = MAX(abs(NameValue.dof * x1_0));
					assert(residuumTmp < 1e-9, ...
						"The trajectory design is faulty (error: " + string(residuumTmp) ...
						+ "), as the trajectory of x_1(0, t) will not " ...
						+ "ensure that: NameValue.dof * x1_0 = 0.");
				end
				w = sysFlat.parametrizeState(y_f);
				
			else
				error("flatOutputChoice " + flatOutputChoice + ...
					" is unknown. Use 'default' or 'ODE-output'");
			end
			w = w.changeDomain(tau);
			
			% compute the pde part
			[x, u] = setPointChange@model.Transport(obj, x1_0, tau, ...
				"controlInputName", NameValue.controlInputName);
			
			if NameValue.struct
				myStruct = misc.struct(...
					obj.stateName.pde(1), x.setName(obj.stateName.pde(1)), ...
					obj.stateName.ode(1), w.setName(obj.stateName.ode(1)), ...
					NameValue.controlInputName, u.setName(NameValue.controlInputName), ...
					"flatOutput", quantity.Discrete(y_f).changeDomain(t).setName("flatOutput"));
				if NameValue.output2struct
					for it = 1 : obj.output.numTypes
						outputValue = obj.output.output(it).out(x);
						if any(strcmp(obj.output.output(it).inputTypes, obj.stateName.ode))
							outputValue = outputValue + obj.output.output(it).input.valueOf(...
								obj.stateName.ode) * w;
						end
						
						myStruct = misc.setfield(myStruct, obj.output.type(it), ...
								outputValue.setName(obj.output.type(it)));
					end % for it = 1 : obj.output.numTypes
				end % if NameValue.output2struct
				x = myStruct;
			end % if NameValue.struct
			
		end % setPointChange()
		
		function trajectory = setPointChange4output(obj, t, tA, tB, yA, yB, output, NameValue)
			% setPointChange4output plans a trajectory for a set point change of an output.
			%
			%	trajectory = setPointChange4output(obj, t, yA, yB, tA, tB, output) calculates the
			%		setPoint change, the inputs and outputs are defined as:
			%	Output parameter:
			%		- trajectory (struct) data including state, input and output trajectories)
			%	Input parameter:
			%		- obj (model.TransportOde) is the transport-PDE-ODE model.
			%		- t (quantity.Domain) is the time horizont on which the trajectory is planned
			%		- tA ([1, n]-double) is the start time of the trajectory for the flat output. If
			%			multiple consecutive trajectories shall be planned, then each column stands
			%			for one trajectory.
			%		- tB ([1, n]-double) is the time when the trajectory of the flat output reaches
			%			the steady state. If multiple consecutive trajectories shall be planned, 
			%			then each column stands for one trajectory.
			%		- yA ([p, n]-double) is the set point of the output at the beginning of the 
			%			trajectory.
			%			In the MIMO-case: if yA has 1 row, then for all output this value is taken.
			%			If yA is an array of size [obj.p, n], then for the i-th output the values 
			%			yA(i, :)  are considered. If multiple consecutive trajectories shall be 
			%			planned, then each column stand for one trajectory.
			%		- yB ([p, n]-double) is the set point of the output at the end of the trajectory.
			%			In the MIMO-case: same as for yA. If multiple consecutive trajectories shall
			%			be planned, then each column stand for one trajectory.
			%		- output (model.Output) the control output for which the set point change
			%			y(t0) = yA -> y(T) = yB should be calculated.
			%
			%	setPointChange4output([...], "trajectoryForm", trajectoryForm) the  trajectoryForm 
			%		input sets if the trajectory for the flat output shall be a polyom or defined via 
			%		cosine. In details:
			%			"polynom" (default): uses stateSpace.setPointChange with algorithm = "sum"
			%			"cos": yfA + (yfB-yfA) * 0.5 * (1 - cos(pi*(t-tA) / (tB-tA))), t \in [tA,tB]
			%
			%	setPointChange4output([...], "trajectoryForm", "polynom", "nSteadyBc", n) the name-
			%		value pair "nSteadyBc" with the value n, defines the n-th derivative for which
			%		the boudary condition is steady, i.e., 
			%			d^k / dt^k f(t)|_{t=t0,t1} = 0,		k = 1, 2, ..., n
			%		Default value is the order of the boundary ode, i.e, size(obj.ode.A, 2). This is
			%		only valid if also "trajectoryForm", "polynom" is used.
			%
			%	setPointChange4output(obj, t, yA, yB, tA, tB, "flatOutputChoice", "ODE-output", ...
			%		"odeOutputGain", Cw) plans the trajectory directly for the ODE-output 
			%			y_f = Cw * odeState.
			%		In contrast to the other cases, the the ODE-output tracks the trajectory of the
			%		flat output exaclty, and not only in the steady state. However, no PDE-outputs
			%		can be considered in this case. If rank(Cw) < obj.p, the additional degrees of
			%		freedoms are used, such that all elements of the x_1(0, t) are similar.
			%		See stateSpace.FlatSystem.Bminimization for details on this case.
			%
			%	setPointChange4output([...], "flatOutputChoice", "ODE-output", "odeOutputGain", Cw)
			%		plans the trajectory directly for the ODE-output 
			%			y_f = Cw * odeState.
			%		In contrast to the other cases, the the ODE-output tracks the trajectory of the
			%		flat output exaclty, and not only in the steady state. However, no PDE-outputs
			%		can be considered in this case. If rank(Cw) < obj.p, the additional degrees of
			%		freedoms can be used, see "dof" input argument.
			%
			%	setPointChange4output([...], "flatOutputChoice", "ODE-output", ...
			%		"odeOutputGain", Cw, "dof", dof) not only implements exact tracking for
			%			y_f = Cw * odeState, but
			%		also uses further degrees of freedoms ("dof"), to ensure
			%			dof * x1_0 = 0.
			%		This allows, to specify additional outputs, for instance y = C[x, w] to be zero
			%		in the set points. In this case, x and w can be expressed via the
			%		delay/predictor parametrization of the flat PDE system as [x, w] = f(x1_0). By
			%		inserting the parametrization of the states into the output operator and
			%		considering time t -> infinity, the input parameter dof
			%			0 = C[f(x1_0(t->inf)] = dof * x1_0(t -> inf)
			%		results as a matrix.
			%
			%	setPointChange4output([...], "trajectoryModifier", trajectoryModifier) the
			%		trajectoryModifier allows to modify selected elements of the output trajectory.
			%		trajectoryModifier must be a cell-array containing [] or function_handles with
			%		nargin == 1. If trajectoryModifier{i, j} is non-empty, then the function_handle
			%		will be applied to the i-th output vector element of the j-th consecutive
			%		trajectory. By doing this, more general trajectories can be implemented. For
			%		instance, if the control output y=[y1; y2] to be controlled is a horizontal
			%		position y1 and a vertical position y2, specifying 
			%			trajectoryModifier = {[]; @(y) y^2} allows to created quadratic curves in
			%		space, i.e. y2 = y1^2.
			%
			% See also model.Transport.setPointChange, model.TransportOde.setPointChange and
			% stateSpace.setPointChange.
			arguments
				obj;
				t (1, 1) quantity.Domain;
				tA (1, :) double = t.lower + t.grid(find(t.grid>max(abs(obj.Phi.at(1))), 1, "first"));
				tB (1, :) double = t.upper - t.grid(find(t.grid>max(abs(obj.Phi.at(1))), 1, "first"));
				yA (:, :) double = 0;
				yB (:, :) double = 1;
				output (:, :) model.Output = model.Output(obj.prefix + ".controlOutput", ...
					"C0", zeros([obj.p, obj.n]), "C1", zeros([obj.p, obj.n]), ...
					"input", misc.Gain(obj.stateName.ode, eye([obj.p, size(obj.ode.A, 1)]), ...
						"outputType", obj.prefix + ".controlOutput"));
				NameValue.trajectoryForm (:, 1) string = "polynom";
				NameValue.nSteadyBc (1, 1) double = size(obj.ode.A, 2);
				NameValue.flatOutputChoice (1, 1) string = "default";
				NameValue.odeOutputGain (:, :) double;
				NameValue.trajectoryModifier (:, :) cell = cell(0);
			end % arguments 
			
			assert(numel(tA) == numel(tB), "tA and tB must have same number of elements");
			
			if obj.p > 1 && ~strcmp(NameValue.flatOutputChoice, "ODE-output")
				if isscalar(yA)
					yA = ones(obj.p, 1) * yA;
				end
				if isscalar(yB)
					yB = ones(obj.p, 1) * yB;
				end
			elseif strcmp(NameValue.flatOutputChoice, "ODE-output")
				assert(isfield(NameValue, "odeOutputGain"), "For the option " ...
					+ "NameValue.flatOutputChoice = ODE-output, also NameValue.odeOutputGain " ...
					+ "must be defined.");
				if isscalar(yA)
					yA = ones(size(NameValue.odeOutputGain, 1), 1) * yA;
				end
				if isscalar(yB)
					yB = ones(size(NameValue.odeOutputGain, 1), 1) * yB;
				end
			end
			
			if numel(tA) > 1 || numel(tB) > 1
				trajectory = obj.setPointChange4outputPiecewiseHelper(...
					t, tA, tB, yA, yB, output, NameValue);
				return;
			else
				assert((size(yA, 2) == 1) & (size(yB, 2) == 1), ...
					"yA and yB have as many columns as tA and tB.");
			end
			
			% round tA and tB to values on the grid of t
 			tA = round(t.grid(find(t.grid>tA, 1, "first") - 1), 15);
 			tB = round(t.grid(min(find(t.grid<tB, 1, "last"), t.n-1) + 1), 15);
			assert(tA > t.lower + max(abs(obj.Phi.at(1))), ...
				"The trajectory must start after a delay of the longest transport time in the " ...
				+ "system, i.e. " + num2str(max(abs(obj.Phi.at(1)))) + ". Consider increasing " ...
				+ "tA or decreasing t.lower.");
			dt = diff(t.grid([1, 2]));
			tTrajectory = quantity.EquidistantDomain("t", ...
				tA - ceil(max(abs(obj.Phi.at(1))) / dt)*dt, ...
				tB + ceil(max(abs(obj.Phi.at(1))) / dt)*dt, ...
				"stepSize", dt);
			
			tAbExtendedYf = quantity.EquidistantDomain("t", ...
				tA - 2*ceil(max(abs(obj.Phi.at(1))) / dt)*dt, ...
				tB + 2*ceil(max(abs(obj.Phi.at(1))) / dt)*dt, ...
				"stepSize", dt);
			Iyf = tAbExtendedYf.copy.split( [ tA, tB ] );

			% define flat output y_f
			%	y_f(t <= tA) = yA
			%	y_f(t > tA & t < tB) = [trajectory defined according to trajectoryForm]
			%	y_f(t >= tB) = yB
			% y_f for different intervals I1, I2, I3
			y_f_I1 = yA * quantity.Discrete.ones(1, Iyf(1));
			if strcmp(NameValue.trajectoryForm, "polynom")
				y_f_I2 = quantity.Symbolic( ...
					stateSpace.setPointChange( yA, yB, Iyf(2).lower, Iyf(2).upper, ...
						NameValue.nSteadyBc, "var", Iyf(2).name, "algorithm", "sum"), ...
					Iyf(2));
			elseif strcmp(NameValue.trajectoryForm, "cos")
				y_f_I2 = quantity.Symbolic(...
					yA + (yB-yA) * 0.5 *(1 - cos(pi*(sym("t")-tA) / (tB-tA))), ...
					Iyf(2));
			else
				error("trajectoryForm is not recognized");
			end
			if ~isempty(NameValue.trajectoryModifier)
				if isscalar(NameValue.trajectoryModifier)
					NameValue.trajectoryModifier = repmat(NameValue.trajectoryModifier, size(y_f_I2));
				else
					NameValue.trajectoryModifier = NameValue.trajectoryModifier;
				end
				for it = 1 : numel(NameValue.trajectoryModifier)
					if isa(NameValue.trajectoryModifier{it}, "function_handle") ...
							&& (nargin(NameValue.trajectoryModifier{it}) == 1)
						y_f_I2(it) = NameValue.trajectoryModifier{it}(y_f_I2(it));
					elseif ~isempty(NameValue.trajectoryModifier{it})
						error("Elements of the cell-array trajectoryModifier must be a " ...
							+ "function_handle with nargin=1, or empty in order to have no effect");
					end
				end
			end % if ~isempty(NameValue.trajectoryModifier)
			y_f_I3 = yB * quantity.Discrete.ones(1, Iyf(3));
			
 			% stich y_f together
			y_fCell = cell(size(obj.ode.A, 2)+1, 1);
			y_fCell{1} = quantity.Piecewise({ y_f_I1, y_f_I2, y_f_I3 }, ...
				'upperBoundaryIncluded', [true, false]);
			for k = 2 : size(obj.ode.A, 2)+1
				y_fCell{k} = quantity.Piecewise({ y_f_I1*0, y_f_I2.diff("t", k-1), y_f_I3*0 }, ...
					'upperBoundaryIncluded', [true, false]);
			end
			y_fUnscaled = signals.BasicVariable(y_fCell{1}, y_fCell(2:end));
			if strcmp(NameValue.flatOutputChoice, "default")
				% scale y_f such that y(t < tA) -> yA and y(t > tB) -> yB
				gainYtoYf = output.out(...
					obj.e1 + obj.e2 * obj.Q0 - int(inv(obj.Lambda) * obj.A0, "z", 0, "z")) ...
					* det(-obj.ode.A);
				if any(strcmp(output.input.inputType, obj.stateName.ode))
					gainYtoYf = gainYtoYf + output.input.valueOf(obj.stateName.ode) ...
									* double(misc.adj(-obj.ode.A)) * obj.pde2ode.C0 * obj.e1;
				end
				if abs(det(gainYtoYf)) < 1e-9
					warning("scaling set point of y into a set point for y_f is badly scaled. " ...
						+ "Using pseudo inverse pinv instead of inv");
					y_f = pinv(gainYtoYf) * y_fUnscaled;
				else
					y_f = inv(gainYtoYf) * y_fUnscaled;
				end

				% plan trajectory for transport-ODE system
				trajectory = obj.setPointChange(y_f, tTrajectory, "struct", true);
				
			elseif strcmp(NameValue.flatOutputChoice, "ODE-output")
				% plan trajectory for transport-ODE system
				if size(NameValue.odeOutputGain, 1) == obj.p
					% all degrees of freedoms are needed for the ODE tracking
					trajectory = obj.setPointChange(y_fUnscaled, tTrajectory, "struct", true, ...
						"flatOutputChoice", "ODE-output", "odeOutputGain", NameValue.odeOutputGain);
				else
				% scale y_f such that y(t < tA) -> yA and y(t > tB) -> yB
					gainYtoYf = output.out(...
						obj.e1 + obj.e2 * obj.Q0 - int(inv(obj.Lambda) * obj.A0, "z", 0, "z"));
					if any(strcmp(output.input.inputType, obj.stateName.ode))
						gainYtoYf = gainYtoYf + (output.input.valueOf(obj.stateName.ode) ...
										/ (-obj.ode.A)) * obj.pde2ode.C0 * obj.e1;
					end
					if size(NameValue.odeOutputGain, 1) + size(gainYtoYf, 1) <= obj.p
						% use additional degress of freedoms to ensure output is zero in set point
						trajectory = obj.setPointChange(y_fUnscaled, tTrajectory, "struct", true, ...
							"flatOutputChoice", "ODE-output", "odeOutputGain", NameValue.odeOutputGain, ...
							"dof", gainYtoYf);
					else
						error("It is unclear, how remaining degrees of freedom for the set point " ...
							+ "change should be used. Please specify the 'output' input parameter " ...
							+ "such that its number of outputs fullfills " ...
							+ "size(odeOutputGain, 1) + size(output, 1) <= obj.p. The trajectory " ...
							+ "will be planned such that the output is zero in the set points.");
					end
				end
				
			else 
				error("NameValue.flatOutputChoice must be either default or ODE-output. " ...
					+ "But it is " + NameValue.flatOutputChoice);
			end % if strcmp(NameValue.flatOutputChoice, "default")
			
			% extrapolation: for t<tA = value at t=tA and t>tB = value at t=tB
			tBefore = quantity.Domain("t", linspace(t.lower, tTrajectory.lower, 11));
			tAfter = quantity.Domain("t", linspace(tTrajectory.upper, t.upper, 11));
			for thisSignalName = misc.fieldnames(trajectory.(obj.prefix)).'
				thisSignal = misc.getfield(trajectory.(obj.prefix), thisSignalName).changeDomain(...
					tTrajectory);
				
				domainBeforeTra = thisSignal(1).domain.replace(tBefore);
				domainAfterTra = thisSignal(1).domain.copy.replace(tAfter);
				
				if (tTrajectory.upper >= t.upper) && (t.lower <= tTrajectory.lower)
					thisSignalNew = thisSignal;
					
				elseif (t.lower >= tTrajectory.lower)
					thisSignalNew = quantity.Discrete(quantity.Piecewise({...
						thisSignal, ...
						thisSignal.subs("t", tTrajectory.upper) ...
							+ quantity.Discrete.zeros(size(thisSignal), domainAfterTra)}, ...
						"upperBoundaryIncluded", false, "domain2join", t));
					
				elseif (t.upper <= tTrajectory.upper)
					thisSignalNew = quantity.Discrete(quantity.Piecewise({...
						thisSignal.subs("t", tTrajectory.lower) ...
							+ quantity.Discrete.zeros(size(thisSignal), domainBeforeTra), ...
						thisSignal}, ...
						"upperBoundaryIncluded", true, "domain2join", t));
					
				else
					thisSignalNew = quantity.Discrete(quantity.Piecewise({...
						thisSignal.subs("t", tTrajectory.lower) ...
							+ quantity.Discrete.zeros(size(thisSignal), domainBeforeTra), ...
						thisSignal, ...
						thisSignal.subs("t", tTrajectory.upper) ...
							+ quantity.Discrete.zeros(size(thisSignal), domainAfterTra)}, ...
						"upperBoundaryIncluded", [true, false], "domain2join", t));
				end
				thisSignalNew = thisSignalNew.changeDomain(t).setName(obj.prefix + "." + thisSignalName);
				trajectory = misc.setfield(trajectory, obj.prefix + "." + thisSignalName, thisSignalNew);
			end % for thisSignal = misc.fieldnames(trajectory)
		end % setPointChange4output
		
	end % methods (Access = public)
	
	methods (Access = protected, Hidden = true)
		function trajectory = setPointChange4outputPiecewiseHelper(...
				obj, t, tA, tB, yA, yB, output, NameValue)
			% setPointChange4outputPiecewiseHelper is a helper method for setPointChange4output to
			% implement the case, where multiple consecutive setPointChanges are defined via
			% multiple columns of tA, tB, yA, yB. This method calls setPointChange4output for every
			% setPointChange separately and stiches all results back together in the end.
			% 
			% See also model.TransportOde.setPointChange4output.
			
			trajectoryCell = cell(size(tA));
			splittingPoints = zeros([numel(tA)-1, 1]);
			
			% split time-domain
			for it = 1 : numel(splittingPoints)
				splittingPoints(it) = tA(it+1) - 0.5*(tA(it+1) - tB(it));
				% round to value on the grid of t
				splittingPoints(it) = round(t.grid(find(t.grid>splittingPoints(it), 1, "first") - 1), 15);
				assert(tA(it+1) - tB(it) >= 0, ...
					"piecewise trajectories must start after each other, hence " ...
					+ "tA(it+1) - tB(it) > 0 has to hold, but tA(it+1) - tB(it) = " ...
					+ string(tA(it+1) - tB(it)) + " for it = " + string(it) + ".");
			end
			tSplitted = t.split(splittingPoints);
			trajectoryModifier = NameValue.trajectoryModifier;
			if ~isempty(trajectoryModifier)
				if size(trajectoryModifier, 2) == 1
					trajectoryModifier = repmat(trajectoryModifier, [1, numel(tA)]);
				else
					assert(size(trajectoryModifier, 2) == numel(tA), ...
						"trajectoryModifier must can be defined (I) as a cell-column vector " ...
						+ "or (II) as a cell-matrix. In case (I), the modification is applied " ...
						+ "to all segments. In case (II), the matrix must have one column for " ...
						+ " every trajectory segment, and the ith-column is applied to the " ...
						+ "ith segment.")
				end
			else
				trajectoryModifier = repmat({[]}, [1, numel(tA)]);
			end
				
			NameValuePairs = misc.struct2namevaluepair(rmfield(NameValue, "trajectoryModifier"));
			for it = 1 : numel(trajectoryCell)
				trajectoryCell{it} = obj.setPointChange4output(...
					tSplitted(it), tA(it), tB(it), yA(:, it), yB(:, it), output, ...
					NameValuePairs{:}, "trajectoryModifier", trajectoryModifier(:, it));
			end
			
			% put pieces together
			trajectory = struct();
			allSignals = setdiff(misc.fieldnames(trajectoryCell{1}), "flatOutput").';
			for thisSignalName = allSignals
				for it = 1 : numel(trajectoryCell)
					thisSignalCell{it} = misc.getfield(trajectoryCell{it}, thisSignalName);
				end % for thisSignal = misc.fieldnames(trajectory)
				thisSignalNew = setName(quantity.Discrete(...
						quantity.Piecewise(thisSignalCell, "domain2join", t)), ...
					thisSignalName);
				thisSignalNew = thisSignalNew.changeDomain(t);
				trajectory = misc.setfield(trajectory, thisSignalName, thisSignalNew);
			end % for thisSignalName = allSignals
			
		end % setPointChange4outputPiecewiseHelper()
		
	end % methods
	
	methods (Access = protected)
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@model.Transport(obj);
			cpObj.odeInput = copy(obj.odeInput);
		end % copyElement()
	end % methods (Access = protected)
end % classdef: model.TransportOde