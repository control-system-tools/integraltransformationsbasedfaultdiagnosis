classdef Input < handle & matlab.mixin.Copyable
	%Input is class to enease implementation of multiple different input
	%signals of dps, for instance control, disturbance, fault inputs or
	%couplings with other systems. Inputs at both boundaries, distributed
	%inputs and inputs acting on the output are implemented. It is assumed,
	%that the input signal is multiplied with the input matrices from right
	%side, i.e. B * u(t).
	%Input is only one input, for use of multiple input-signals, use Inputs!
	
	properties
		% input matrices
		B (:,:) quantity.Discrete;	% distributed input
		B0 (:,:) double;			% left boundary (z=0) input
		B1 (:,:) double;			% right boundary (z=1) input
		D (1,1) misc.Gain;			% feedthrough from input to output specified by 
		%							% D.type
		
		% data about input
		type (1, 1) string;					% type of input signal, for instance
		%							% 'control', 'disturbance', 'fault' etc
		length (1, 1) double;		% length of input vector
		
		lengthOutput (:, 1) double;	% length of output vectors of D
		outputType (:, 1) string;	% type of output signals of D, for instance 
		%							% 'control', 'measurement', 'ode', etc.
	end
	
	methods
		function obj = Input(type, varargin)
			% INPUT object for representation of system inputs
			% obj = Input(TYPE, <name-value-paris>) initializes an input for a pde
			% system. The TPYE (as char) describes the name of this input variable.
			% Use name-value-pairs to specify the input parameters
			%	B(z) as a quantity.Discrete for an in-domain input
			%	B0 as double for a boundary input at z = 0
			%	B1 as double for a boundary input at z = 1
			%	D as misc.Gain as feedthrough to the output
			%% Constructor
			if nargin > 0
				% read input
				obj.type = type;
				
				myParser = misc.Parser();
				addParameter(myParser, "B", quantity.Discrete.empty());
				addParameter(myParser, "B0", []);
				addParameter(myParser, "B1", []);
				addParameter(myParser, "D", misc.Gain(), @(v) isa(v, 'misc.Gain'));
				myParser.parse(varargin{:});
				
				% set length and check input matrix sizes
				obj.length = 0;
				for myParameter = fieldnames(myParser.Results).'
					obj.(myParameter{:}) = myParser.Results.(myParameter{:});
					if ~isempty(myParser.Results.(myParameter{:}))
						if strcmp(myParameter{:}, 'D')
							tempLength = sum(myParser.Results.D.lengthInput);
						else
							tempLength = size(myParser.Results.(myParameter{:}), 2);
						end
						if obj.length == 0
							obj.length = tempLength;
						else
							assert(isequal(obj.length, tempLength), ...
								'number of input columns must coincide!');
						end
					end
				end
				if ~isempty(obj.D)
					assert(isequal(obj.D.inputType, obj.type), ...
						['Input type of D ', obj.D.inputType, ...
						' must be equal to Input object type', obj.type]);
					assert(isequal(obj.D.lengthInput, obj.length), ...
						['Input length of D=', num2str(obj.D.lengthInput), ...
						' must be equal to length of input object=', num2str(obj.length)]);
				end
				for defaultParameter = setdiff(myParser.UsingDefaults, 'D')
					obj.(defaultParameter{:}) = reshape(obj.(defaultParameter{:}), [0, obj.length]);
				end
			end
		end % Input() Constructor
		
		function myInputs = plus(a, b)
			% combining model.Input and model.Inputs
			if isa(b, 'model.Inputs')
				myInputs = b.copy() + a;
			elseif isa(b, 'model.Input')
				if strcmp(b.type, a.type)
					tempParameter = struct();
					for myParameter = ["B", "B0", "B1", "D"]
						if a.isMatrixZeroOrEmpty(myParameter)
							if ~b.isMatrixZeroOrEmpty(myParameter)
								tempParameter.(myParameter) = b.(myParameter);
							end
						else
							if b.isMatrixZeroOrEmpty(myParameter)
								tempParameter.(myParameter) = a.(myParameter);
							else
								tempParameter.(myParameter) = ...
									a.(myParameter) + b.(myParameter);
							end
						end
					end % for myParameter = ...
					myParameterNameValuePairs = misc.struct2namevaluepair(tempParameter);
					myInputs = model.Input(a.type, myParameterNameValuePairs{:});
				else
					myInputs = model.Inputs(a, b);
				end
			elseif isempty(b)
				myInputs = model.Inputs(a);
			else
				error('model.Input can only be added with other Input or model.Inputs');
			end
		end % plus()
		
		function newInput = blkdiag(a, b, varargin)
			% blkdiag combines two model.Input objects by combining them block-diagonally.
			assert(isequal(a.type, b.type), "model.Inputs to be combined via blkdiag need to have same type");
			newParameter = struct();
			for myParameter = ["B", "B0", "B1", "D"]
				newParameter.(myParameter) = blkdiag(a.(myParameter), b.(myParameter));
			end % for newParameter
			newParameterNameValue = misc.struct2namevaluepair(newParameter);
			newInput = model.Input(a.type, newParameterNameValue{:});
		end % blkdiag()
		
		function flag = isMatrixZeroOrEmpty(obj, varargin)
			
			if isempty(obj)
				flag = true(numel(varargin), 1);
				return;
			else
				flag = false(numel(varargin), 1);
			end
				
			for it = 1 : numel(varargin)
				testProperty = varargin{it};
				if isempty(obj.(testProperty))
					flag(it) = true;
				else
					if isa(obj.(testProperty), 'quantity.Discrete')
						value = obj.(testProperty).on();
					elseif isa(obj.(testProperty), 'misc.Gain')
						value = obj.(testProperty).value;
					else
						value = obj.(testProperty);
					end
					flag(it) = isempty(value) || (mean(abs(value(:))) == 0);
				end
			end
		end % isMatrixZeroOrEmpty()
		
		function myStateSpace = addD2ss(obj, myStateSpace)
			% addD2Ss adds the feedthrough D to a given state space model
			objInputs = model.Inputs(obj);
			myStateSpace = objInputs.addD2ss(myStateSpace);
		end % addD2ss()
		
		function obj = extendType(obj, position, newText)
			% extendType adds a pre- or a postfix to obj.type
			arguments
				obj;
				position (1, 1) string;
				newText (1, 1) string;
			end
			assert(any(strcmp(position, ["front", "back"])));
			
			if strcmp(position, "front")
				obj.type = newText + obj.type;
			elseif strcmp(position, "back")
				obj.type = obj.type + newText;
			end
			obj.D.inputType = obj.type;
		end % extendType()
		
		function obj = strrepType(obj, oldText, newText, NameValue)
			% replace strings in type
			arguments
				obj;
				oldText (1, 1) string;
				newText (1, 1) string;
				NameValue.startOnly (1, 1) logical = false;
			end
			if NameValue.startOnly && startsWith(obj.type, oldText)
				obj.type = newText + eraseBetween(obj.type, 1, strlength(oldText));
			elseif ~NameValue.startOnly
				obj.type = strrep(obj.type, oldText, newText);
			end
			obj.D = obj.D.strrepInputType(oldText, newText, "startOnly", NameValue.startOnly);
			obj.D = obj.D.strrepOutputType(oldText, newText, "startOnly", NameValue.startOnly);
		end % strrepType()
		
		function obj = setPrefix(obj, prefix)
			% add an additional prefix to the type
			arguments
				obj;
				prefix char; %	cast to char to check last char-element in the following.
			end
			if prefix(end) ~= '.'
				prefix = prefix + ".";
			end
			obj = obj.extendType("front", prefix);
		end % setPrefix()
		
		function BMat = getBDiscrete(obj, myGrid)
			% getBDiscrete get distributed input with state-individual discretization
			if ~iscell(myGrid)
				assert(isvector(myGrid));
				myGrid = {myGrid};
			end
			if numel(myGrid) == 1
				myGrid = repmat(myGrid, size(obj.B));
			end
			BCell= cell(size(obj.B));
			for it = 1 : numel(obj.B)
				BCell{it} = obj.B(it).on(myGrid{it}, 'z');
			end
			BMat = cell2mat(BCell);
		end % getBDiscrete()
		
		function outputType = get.outputType(obj)
			outputType = obj.D.outputType;
		end % get.outputType(obj)
		
		function lengthOutput = get.lengthOutput(obj)
			lengthOutput = obj.D.lengthOutput;
		end % get.lengthOutput(obj)
		
		function [texString, nameValuePairs] = printParameter(obj)
			% Create TeX-code for all non-zero parameter. In this method, the
			% implementation of model.Inputs is used.
			[texString, nameValuePairs] = printParameter(model.Inputs(obj));
		end % printParameter()
		
		function texString = print(obj, matrixName, inputName)
			% print Print equation to command window as latex compatible string-array
			arguments
				obj;
				matrixName;
				inputName = obj.type.extractBefore(2) + "_{" + obj.type.extractAfter(1) + "}(t)";
			end % arguments
			
			myString = "";
			if any(strcmp(matrixName, ["B", "B0", "B1"]))
				if ~obj.isMatrixZeroOrEmpty(matrixName)
					if (~strcmp(matrixName, "B") && misc.iseye(obj.(matrixName))) ...
							|| (strcmp(matrixName, "B") && iseye(obj.(matrixName)))
						myString = " + " + inputName;
					else
						myString = " +" + misc.latexChar(obj.(matrixName)) + " " + inputName;
					end
				end
			elseif contains(obj.D.outputType, matrixName)
				if misc.iseye(obj.D.valueOfOutput(matrixName))
					myString = " + " + inputName;
				else
					myString = " +" + misc.latexChar(obj.D.valueOfOutput(matrixName)) + " " + inputName;
				end
			else
				error("matrixName does not specify B, B0, B1 or any output in D");
			end
			
			if nargout > 0
				texString = myString;
			else
				misc.printTex(myString);
			end
		end % print()
		
		function obj = discretizeQuantity(obj)
			% discretizeQuantity converts all properties containg quantity-objects to
			% quantity.Discrete objects. This is often speeds up calculations.
			for prop = "B"
				obj.(prop) = quantity.Discrete(obj.(prop));
			end
		end % discretizeQuantity()
		
		function newObj = copyAndReplace(oldObj, varargin)
			% copyButReplace creates a copy of the object.
			% However, properties specified by a name-value-pair in varargin replace
			% the old objects properties.
			
			myArgs = ["B", "B0", "B1", "D"];
			myParser = misc.Parser;
			myParser.KeepUnmatched = false;
			for it = 1:numel(myArgs)
				thisArg = myArgs(it);
				if isa(oldObj.(thisArg), 'matlab.mixin.Copyable')
					myParser.addParameter(thisArg, copy(oldObj.(thisArg)));
				else
					myParser.addParameter(thisArg, oldObj.(thisArg));
				end
			end
			myParser.parse(varargin{:});
			newObj = misc.constructorOf(oldObj, oldObj.type, myParser.ResultsNameValuePair{:});
		end % copyAndReplace()
		
	end % methods
	
	methods (Access = protected)
		% Override copyElement method:
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@matlab.mixin.Copyable(obj);
			cpObj.B = copy(obj.B);
			cpObj.D = copy(obj.D);
		end
	end % methods (Access = protected)
	
end
