classdef Transport < model.Dps & handle & matlab.mixin.Copyable
	% model.Transport represents n coupled first order hyperbolic equations.
	% This implements hyperbolic systems of the form
	%
	%	x_t(z,t) = Lambda(z) x_z(z,t) + A(z) x(z,t) + A0(z) x_1(0,t) + A1(z) x_2(1,t) ...
	%				+ int_a^b F(z,zeta) x(zeta,t) dzeta + input.B(z) u(t)
	%          0 = bc0(x) + input.B0 u(t)
	%          0 = bc1(x) + input.B1 u(t)
	%		y(t) = output[x(z,t)] + input.D u(t)
	%
	% and the initial condition x(z,0) = x0(z).
	% The distributed state is x(z, t) \in R^n, with the spatial variable z in [0, 1] and the 
	% temporal variable t. The state vector can be splitted into x = col(x_1, x_2), with
	%
	%	x_1(z, t) = e1.' x(z, t) \in R^p describing the states related to transport from z=1 to z=0,
	%	x_2(z, t) = e2.' x(z, t) \in R^m describing the states related to transport from z=0 to z=1.
	%
	%  Usually, the boundary conditions are described by the operators bc0 and bc1, that implement
	%
	%	x_2(0,t) = Q0 x_1(0,t) + input.B0 u(t)
	%	x_1(1,t) = Q1 x_2(1,t) + input.B1 u(t).
	%
	% However, the operators bc0 and bc1 allow to define additional integral terms or other
	% couplings, see model.Output. The vector u(t) contains all input signals, for instance control
	% input,  disturbances or faults. y(t) is a vector of output signals, for instance the control 
	% output or measurements. Outputs can define general outputs
	%
	%	output[h] = sum_{i=1}^{l} [F_i * h(z_i)] + int_0^1 [C(z) h(z)] dz
	%
	% see model.Outputs.
	% The bounds of the integral in the PDE is defined by Fbounds = {a, b} wherein at a, b can be
	% spatial values in [0, 1] or a string "z".
	%
	% See also model.Transport.Transport (constructor), model.Dps, model.TransportOde, model.Wave,
	%	model.WaveOde.
	
	properties (SetAccess = protected)
		Lambda quantity.Discrete {mustBe.quadraticMatrix};	% transport / convection matrix
		A quantity.Discrete {mustBe.quadraticMatrix};		% reaction matrix
		F quantity.Discrete  {mustBe.quadraticMatrix};		% integral coefficient
		FBounds (1, 2) cell = {0, 0};						% integral bounds a, b in a cell
		A0 quantity.Discrete;								% local term x(0)->PDE
		A1 quantity.Discrete;								% local term x(1)->PDE
		
		% left boundary operator at z=0, see model.Output. Usually implements
		%	x_2(0) = Q0 x_1(0), bc0.C0=[-I Q0].
		bc0;
		% right boundary operator at z=1, see model.Output. Usually implements
		%	x_1(1) = Q1 x_2(1), bc1.C1=[Q1 -I].
		bc1;
		
		% Input gains for control signal, disturbances, faults, etc. are stored in this model.Inputs
		% object. Inputs are defined at the left (B0) and right (B1) boundary and distributed (B).
		% Feedthroughs (D) can also be implemented via the output property. See model.Inputs
		input;
		
		% Output operator for control, measurement, PDE -> ODE coupling, etc... Implements boundary, 
		% distributed, pointwise, dirichlet, neumann, and time derivative outputs.
		output;
		
		n (1,1) {mustBeInteger, mustBeNonnegative};	% number of PDEs, i.e. size(obj.Lambda, 1)
		p (1,1) {mustBeInteger, mustBeNonnegative};	% number of PDEs with positive lambda_i
		m (1,1) {mustBeInteger, mustBeNonnegative};	% number of PDEs with negative lambda_i
		e1 double;									% matrix selector for x_1(z,t) = e1.' x(z,t)
		e2 double;									% matrix selector for x_2(z,t) = e2.' x(z,t)
		
		% finite approximation of the dynamics, without transport, see
		% model.Transport.setApproximation.
		approximationNoConvection;
		% shift matrix to implement the transport dynamics, see model.Transport.setApproximation,
		% model.Transport.addConvection2SimulationModel
		approximationConvectionShift;
		
		% cell of spatial grids for every state which are used for the discrete approximation. The
		% grids might be different for every state, as the simulation implements velocity dependent
		% discretization.
		grid cell;
		% matrix used to extract the only the indomain entries of distretized state vector
		% without the boundary values
        domain2indomain double {mustBeInteger, mustBeNonnegative};
        
		% step size in the temporal domain used for the lumped dynamics, see 
		% model.Transport.suggestStepSize, model.Transport.setApproximation.
		stepSize = 0;
		% cflNumber resulting by the spatial and temporal discretization, see 
		% model.Transport.suggestStepSize, model.Transport.setApproximation.
		cflNumber;
		
		name;			% name of object
		arglist cell;	% list of input parameters used in the constructor
		prefix string;	% is added to the type of output, the state name and simulation signals
		
		% isotachic states have the same velocity, i.e., identical values 
		%	Lambda(i, i) - Lambda(j, j) = 0.
		% This isotachic-property is an array that indexes identical Lambda-values. For instance, if
		% Lambda = diag(3, 2, 2, -1, -1, -1.5), then isotachic = [0; 1; 1; 2; 2; 0].
		isotachic (:, 1) double;
	end % properties (SetAccess = protected)
	
	properties (Dependent = true)
		Q0 double;	% z0 boundary coupling-matrix, see bc0.
		Q1 double;	% z1 boundary coupling-matrix, see bc1.
		gridLength (:, 1) double;	% length of the elements of the obj.grid-cell array
		interpMatrix (:, :) cell;	% interpolation matrix used to interpolate between grids.
		Phi (:, 1) quantity.Discrete;	% transport times
		settlingTime	% struct of the nominal achievable settling time by state feedback
	end % properties (Dependent = true)
	
	properties (Hidden = true)
		% name of the matrix defining the dominant PDE dynamics i.e. "Gamma" for the wave velocity
		% of model.Wave or "Lambda" for the transport velocity of model.Transport
		nameCharacteristicMatrix = "Lambda";
	end % properties (Hidden = true)
	
	%% Methods and Constructor
	methods
		function obj = Transport(Lambda, varargin)
			% model.Transport.Transport constructs a model.Transport object, which describes a nxn
			% coupled hyperbolic system.
			%
			%	obj = model.Transport(Lambda) obj is a hyperbolic system with transport velocities 
			%		defined by Lambda. Usually, Lambda is diagonal, and positive diagonal-elements 
			%		describe from z=1 to z=0, while negative diagonal elements describe transport
			%		from z=0 to z=1.
			%
			%	model.Transport([...], "A", A) sets the distributed matrix A. A must be a
			%		quantity.Discrete with size and domain fitting to Lambda.
			%
			%	model.Transport([...], "A0", A0) sets the local-PDE-coupling A0. A0 must be a
			%		quantity.Discrete with size and domain fitting to Lambda.
			%
			%	model.Transport([...], "A1", A1) sets the local-PDE-coupling A0. A0 must be a
			%		quantity.Discrete with size and domain fitting to Lambda.
			%
			%	model.Transport([...], "F", F) sets the coefficient of the integral. F must be a
			%		quantity.Discrete with size fitting to Lambda and domains z and zeta.
			%
			%	model.Transport([...], "FBounds", FBounds) sets integral bounds of the PDE to 
			%		FBounds. FBounds{1} is the lower bound, FBounds{2} is the upper bound. Elements
			%		of FBounds may be scalars \in [0, 1] or a string/char "z".
			%
			%	model.Transport([...], "bc0", bc0) sets the boundary operator for z=0 to the 
			%		model.Output object bc0, to implement: 0 = bc0(x) + input.B0 u(t)
			%		input must be defined separate, see below. When there is a bc0 input, there must
			%		not be a Q0 input.
			%
			%	model.Transport([...], "bc1", bc1) sets the boundary operator for z=1 to the
			%		model.Output object bc1, to implement: 0 = bc1(v, v_t) + input.B1 u(t)
			%		input must be defined separate, see below. When there is a bc1 input, there must
			%		not be a Q1 input.
			%
			%	model.Transport([...], "Q0", Q0) sets the boundary operator for z=0 to
			%		the model.Output object bc0, to implement: x_2(0,t)=Q0 x_1(0,t) + input.B0 u(t)
			%		input must be defined separate, see below. When there is a Q0 input, there must
			%		not be a bc0 input.
			%
			%	model.Transport([...], "Q1", Q1) sets the boundary operator for z=1 to the 
			%		model.Output object bc1, to implement: x_1(1,t) = Q1 x_2(1,t) + input.B1 u(t).
			%		input must be defined separate, see below. When there is a Q1 input, there must
			%		not be a bc1 input.
			%
			%	model.Transport([...], "input", input) sets obj.input. Input gains for control
			%		signal, disturbances, faults, etc. are stored in this model.Inputs object.
			%		Inputs are  defined at the left (B0) and right (B1) boundary and distributed (B).
			%		Feedthroughs (D) can also be implemented via the output property. 
			%		See model.Inputs
			%
			%	model.Transport([...], "output", output) sets obj.output. Output operator for
			%		control output, measurement, PDE -> ODE coupling, etc... Implements boundary,
			%		distributed, pointwise, dirichlet, neumann, and time-derivative outputs.
			%
			%	model.Transport([...], "name", name) sets obj.name.
			%
			%	model.Transport([...], "prefix", prefix) sets obj.prefix, which is a string, which
			%		is added to the type of output, the state name and simulation signals to be able
			%		to differ between different systems.
			%
			% See also model.Transport (class description and equations), model.Dps, 
			%	model.TransportOde.
			if nargin > 0
				%% Velocity-Matrix and system-order
				obj.Lambda = Lambda;
				obj.n = size(obj.Lambda, 1);
				obj.p = sum(eig(obj.Lambda.at(0)) > 0);
				obj.m = obj.n - obj.p;
				
				% Matrices to select states with positive or negative lambda_i:
				I = eye(size(Lambda));
				if isdiag(Lambda.at(0))
					positiveLambda = diag(Lambda.at(0)) > 0;
					obj.e1 = I(:, positiveLambda);
					obj.e2 = I(:, ~positiveLambda);
				elseif (obj.p == obj.m) && misc.iseye(Lambda(1:obj.p, (obj.p+1):end).at(0))
					% energy coordinates
					obj.e1 = eye(obj.n, obj.p);
					obj.e2 = [zeros(obj.m, obj.m); eye(obj.m, obj.m)];
				else
					warning("unrecognized structure of Lambda");
					obj.e1 = [];
					obj.e2 = [];
				end
				obj = obj.setIsotachic();
				
				%% Input parser
				myParser = misc.Parser();
				myParser.addParameter("name", num2str(obj.p) + "+" + num2str(obj.m) + "_transport");
				myParser.addParameter("prefix", "plant", @(v) ischar(v) | isstring(v));
				
				% PDE
				zDomain = obj(1).Lambda(1).domain;
				myParser.addParameter("A", ...
					quantity.Discrete.zeros([obj.n, obj.n], zDomain, "name", "A"));
				myParser.addParameter("F", quantity.Discrete.zeros([obj.n, obj.n], ...
											[zDomain, zDomain.rename("zeta")], "name", "F"));
				myParser.addParameter("FBounds", {0, 0});
				myParser.addParameter("A0", ...
					quantity.Discrete.zeros([obj.n, obj.p], zDomain, "name", "A_0"));
				myParser.addParameter("A1", ...
					quantity.Discrete.zeros([obj.n, obj.m], zDomain, "name", "A_1"));
				
				% Input & Output
				myParser.addParameter("input", model.Inputs());
				myParser.addParameter("output", model.Outputs());
				
				% Boundary conditions
				% can be specified by in two ways:
				%	1. name-value-pair inputs of "bc0" "bc1" as model.Output-objects
				%	2. name-value-pair inputs of "Q0", "Q1" as numeric arrays.
				% In case 2, the resulting properties "bc0" and "bc1" are set in the
				% following. If an input "bc0" or "bc1" is defined, the values of "Q0", "Q1" are
				% ignored.
				myParserQ0Q1 = misc.Parser();
				myParserQ0Q1.addParameter("Q0", zeros(obj.m, obj.p));
				myParserQ0Q1.addParameter("Q1", zeros(obj.p, obj.m));
				myParserQ0Q1.parse(varargin{:});
				assert(isequal(size(myParserQ0Q1.Results.Q0), [obj.m, obj.p]), ...
					"Missmatching size of Q0 = " + mat2str(size(myParserQ0Q1.Results.Q0)) + ...
					", but it is expected to be " + mat2str([obj.m, obj.p]));
				assert(isequal(size(myParserQ0Q1.Results.Q1), [obj.p, obj.m]), ...
					"Missmatching size of Q1 = " + mat2str(size(myParserQ0Q1.Results.Q1)) + ...
					", but it is expected to be " + mat2str([obj.p, obj.m]));
				
				% BC at z = 0
				myParser.addParameter("bc0", ...
					model.Output("bc0", "C0", [myParserQ0Q1.Results.Q0, -eye(obj.m)]));
				% BC at z = 1
				myParser.addParameter("bc1", ...
					model.Output("bc1", "C1", [-eye(obj.p), myParserQ0Q1.Results.Q1]));
				
				%% write from parser to obj
				myParser.parse(varargin{:});
				obj.arglist = fieldnames(myParser.Results);
				for it = 1 : numel(obj.arglist)
					myProp = myParser.Results.(obj.arglist{it});
					% check grid
					if any(strcmp(obj.arglist{it}, ["A", "F", "A0", "A1"]))
						assert(isequal(myProp(1).domain(1).grid([1, end]), [0; 1]), ...
							"spatial grids must be defined from 0 to 1");
						if strcmp(obj.arglist{it}, "F")
							assert(isequal(myProp(2).domain(2).grid([1, end]), [0; 1]), ...
								"spatial grids must be defined from 0 to 1");
						end
					elseif strcmp(obj.arglist{it}, "input")
						if isa(myProp, "model.Input")
							% cast Input 2 Inputs
							myProp = model.Inputs(myProp.copy());
						else
							myProp = myProp.copy();
						end
					elseif strcmp(obj.arglist{it}, "output")
						if isa(myProp, "model.Output")
							% cast Output 2 Outputs
							myProp = model.Outputs(myProp.copy());
						else
							myProp = myProp.copy();
						end
					end
					obj.(obj.arglist{it}) = myProp;
				end
				
				%% Input-Check: Boundary-Coupling
				assert(isequal(size(obj.Q0), [obj.m, obj.p]), ...
					"Q0 must be a mxp-Matrix. obj.m=" + num2str(obj.m) ...
					+ "; obj.p=" + num2str(obj.p));
				assert(isequal(size(obj.Q1), [obj.p, obj.m]), ...
					"Q1 must be a pxm-Matrix. obj.p=" + num2str(obj.p) ...
					+ "; obj.m=" + num2str(obj.m));
				
				%% Input-Check: Input-Matrices
				assert(any(obj.input.rowsB == [0, obj.n]), "B of wrong size");
				assert(any(obj.input.rowsB0 == [0, obj.m]), "B0 of wrong size");
				assert(any(obj.input.rowsB1 == [0, obj.p]), "B1 of wrong size");
				
				%% Check if intergral bounds of F are defined
				if any(strcmp(myParser.UsingDefaults, "F"))
					assert(any(strcmp(myParser.UsingDefaults, "FBounds")) ...
						&& iscell(obj.FBounds) && (numel(obj.FBounds) == 2) ...
						&& (isnumeric(obj.FBounds{1}) || strcmp(obj.FBounds{1}, "z")) ...
						&& (isnumeric(obj.FBounds{2}) || strcmp(obj.FBounds{2}, "z")), ...
						"FBounds must be defined if F is input of constructor. " ...
						+ "FBounds must be a cell array with two elements which are either" ...
						+ " numeric values or a char z");
				end
				
				%% Input-Check: Output & Input
				% todo
				
				%% add prefix to output
				obj = obj.setOutputName();
				
			end % nargin > 0
		end % System() Constructor
		
		function obj = setApproximation(obj, varargin)
			% setApproximation creates a lumped approximation of the dynamics by setting the
			% obj-properties approximationConvectionShift, approximationNoConvection, stepSize and
			% grid.
			% For model.Transport objects with diagonal Lambda-matrices, the approximation is based
			% on a 2-point finite differences scheme while choosing a spatial discretization for a 
			% given temporal discretization, such that the cfl-number is close to 1. For spatial-
			% dependent Lambda, this leads to a non-homogeneous spatial grid. If setApproximation 
			% was performed before, new values are only calculated, if the step size w.r.t. time 
			% changed.
			% For systems with non diagonal Lambda matrix, the approximation is set using a 5-point
			% central finite differences scheme.
			%
			%	obj = setApproximation(obj) calculates the approximation for a temporal step size of
			%		0.02.
			%
			%	obj = setApproximation(obj, "t", t) the step size obtained from the temporal grid t
			%		is used to set the approximation.
			%
			%	setApproximation([...] "UseIteration", UseIteration) if UseIteration=true
			%		(default), then the spatial grid is calculated using a iterative method of 
			%		adding grid points. If UseIteration=false, the step size is calculated in an 
			%		analytic way, based on the transport velocity. See computeCflGrid
			%
			% See also getClosedLoopSimulationModel, addConvection2SimulationModel, simulate,
			%	computeCflGrid.
			myParser = misc.Parser();
			myParser.addParameter("t", linspace(0, 4, 201));
			myParser.parse(varargin{:});
			obj.stepSize = myParser.Results.t(2) - myParser.Results.t(1);
			
			% Check if Lambda is diagonal
			diagonalConvection = all(diag(min(abs(obj.Lambda)))>0);
			if diagonalConvection
				obj = setCflGrid(obj, obj.stepSize, varargin{:});
			else
				obj.grid = cell(obj.n, 1);
				[obj.grid{:}] = deal(linspace(0, 1, floor(1/obj.stepSize)-1));
			end
			myGridLength = obj.gridLength;
			obj.output = obj.output.setApproximation(obj.grid);
			
			%% matrices for combining in-domain and boundary states
			% to a full state-vector. As the boundary conditions are
			% implemented as algebraic, dirichlet boundary conditions (even
			% if they are Neumann or Robin), they have to be combined with
			% the in-domain states to obtain a full domain state vector x.
			% Hence, the matrices bc2domain and indomain2domain have to be
			% calculated such that
			%	   x = indomain2domain * xPde + bc2domain xBc
			% holds. Notice, that also
			%	xPde = indomain2domain.' * x = domain2indomain * x and
			%	 xBc = bc2domain.' * x = domain2bc * x
			% holds.
			indomain2domainCell = cell(obj.n, 1);
			bc2domain = cell(obj.n, 1);
			boundaryNoBc2domain = cell(obj.n, 1);
			convectionShiftDomain = cell(obj.n, 1);
			for it = 1 : obj.p
				indomain2domainCell{it} = ...
					[eye(myGridLength(it)-1); ...
					zeros(1, myGridLength(it)-1)];
				bc2domain{it} = ...
					[zeros(myGridLength(it)-1, 1);
					1];
				boundaryNoBc2domain{it} = ...
					[1;
					zeros(myGridLength(it)-1, 1)];
				convectionShiftDomain{it} = diag(ones(1, myGridLength(it)-1), 1);
			end % for it = 1 : obj.p
			
			for it = (obj.p+1) : obj.n
				indomain2domainCell{it} = ...
					[zeros(1, myGridLength(it)-1); ...
					eye(myGridLength(it)-1);];
				bc2domain{it} = ...
					[1;
					zeros(myGridLength(it)-1, 1)];
				boundaryNoBc2domain{it} = ...
					[zeros(myGridLength(it)-1, 1);
					1];
				convectionShiftDomain{it} = diag(ones(1, myGridLength(it)-1), -1);
			end % for it = (obj.p+1) : obj.n
			
			indomain2domain = blkdiag(indomain2domainCell{:});
			bc2domain = blkdiag(bc2domain{:});
			domain2bc = bc2domain.';
			boundaryNoBc2domain = blkdiag(boundaryNoBc2domain{:});
			domain2indomain = indomain2domain.';
			obj.domain2indomain = domain2indomain;
			domain2boundaryNoBc = boundaryNoBc2domain.';
			if diagonalConvection
				convectionShiftDomain = blkdiag(convectionShiftDomain{:});
			else
				convectionShiftDomain = eye(sum(myGridLength));
			end
			
			% check result
			assert(isequal(size(bc2domain), [sum(myGridLength(:)), obj.n]));
			assert(isequal(size(boundaryNoBc2domain), [sum(myGridLength(:)), obj.n]));
			assert(isequal(size(indomain2domain), sum(myGridLength(:))+[0, -obj.n]));
			assert(sum(bc2domain(:)) == obj.n);
			assert(sum(bc2domain(:))+sum(indomain2domain(:)) == sum(myGridLength));
			assert(sum(domain2boundaryNoBc(:)) == obj.n);
			assert(sum(domain2boundaryNoBc(:))+sum(indomain2domain(:)) == sum(myGridLength));
			xTest = rand(sum(myGridLength), 1);
			assert(isequal(xTest, ...
				indomain2domain * domain2indomain * xTest + bc2domain * bc2domain.' * xTest));

			%% Calculate Convection-Matrix for FDM-Approximation
			AAdomainCell = cell(obj.n, obj.n);
			ALambdaDomainCell = cell(obj.n, obj.n);
			for it=1:obj.n
				for jt=1:obj.n
					AAdomainCell{it,jt} = obj.interpMatrix{it,jt} * diag(obj.A(it, jt).on(obj.grid{jt}));
					D1 = three_point_centered_D1(obj.grid{jt});
%					D1 = five_point_centered_D1(obj.grid{jt});
					%D1 = numeric.fd.matrix('5pointCentral', obj.grid{jt}, 1);
% 					D1(end, :) = 0; D1(end, end+[-1, 0]) = [-1, 1]/(obj.grid{jt}(end)-obj.grid{jt}(end-1));
% 					D1(1, :) = 0; D1(1, 1+[0, 1]) = [-1, 1]/(obj.grid{jt}(2)-obj.grid{jt}(1));
					ALambdaDomainCell{it, jt} = obj.interpMatrix{it,jt} ...
						* diag(obj.Lambda(it, jt).on(obj.grid{jt})) ...
						* D1;
				end
				% if the diagonal elements of the convection are already considered
				% with convectionShift, they are erased in the following line:
				if diagonalConvection
					ALambdaDomainCell{it, it} = 0 * ALambdaDomainCell{it, it};
				end
			end
			AAdomain = cell2mat(AAdomainCell);
			ALambdaDomain = cell2mat(ALambdaDomainCell);

			%% Consider local coupling A_0 * E_1^T x(0,t) in pde
			if obj.A0.iszero(true)
				AA0x1z02domain = 0 * ALambdaDomain;
			else
				assert(isequal(size(obj.A0), [obj.n, obj.p]), 'A0 must have size obj.n x obj.p');
				AA0domainCell = cell(obj.n, obj.p);
				for it=1:obj.n
					for jt=1:obj.p
						AA0domainCell{it,jt} = obj.A0(it, jt).on(obj.grid{it});
					end
				end
				AA0x1z02domain = cell2mat(AA0domainCell) * obj.e1.' * domain2boundaryNoBc;
			end

			%% Consider local coupling A_1 * E_2^T x(1,t) in pde
			if obj.A1.iszero(true)
				AA1x2z12domain = 0 * ALambdaDomain;
			else
				assert(isequal(size(obj.A1), [obj.n, obj.m]), 'A1 must have size obj.n x obj.m');
				AA1domainCell = cell(obj.n, obj.m);
				for it=1:obj.n
					for jt=1:obj.m
						AA1domainCell{it,jt} = obj.A1(it, jt).on(obj.grid{it});
					end
				end
				AA1x2z12domain = cell2mat(AA1domainCell) * obj.e2.' * domain2boundaryNoBc;
			end
			
			%% Consider integralTerm int_a^b F(z, zeta) x(zeta) dzeta
			% with integral-bounds specified by a=obj.FBounds{1}, b=obj.FBounds{1}
			AFdomainCell = cell(obj.n, obj.n);
			integralDomain = obj.integralDomain(obj.FBounds, obj.F(1).domain);
			
			FIntDomain = obj.F * integralDomain;
 			for jt = 1 : obj.n
				myMassMatrix = numeric.massMatrix(quantity.Domain("z", obj.grid{jt}));
				for it = 1 : obj.n
					AFdomainItJt = zeros(myGridLength(it), myGridLength(jt));
					FIntDomainItJt = FIntDomain(it, jt).on({obj.grid{it}, obj.grid{jt}}, ...
						{'z', 'zeta'});
					for zIdx = 1 : myGridLength(it)
						AFdomainItJt(zIdx, :) = myMassMatrix * FIntDomainItJt(zIdx, :).';
					end
					AFdomainCell{it, jt} = AFdomainItJt;
				end
			end
			AFdomain = cell2mat(AFdomainCell);
			
			%% Consider boundary condition
% 			Qall = [zeros(obj.p, obj.m), obj.Q1; obj.Q0, zeros(obj.m, obj.p)];
% 			dBcDomain = bc2domain * Qall * domain2boundaryNoBc;
			[bc.operator, bc.weights, bc.input] = useAsBoundaryOperator(...
				obj.bc0 + obj.bc1, ["transport", "transport"], [0, 1], ...
				"grid", obj.grid, "withTimeDerivative", false, "input", obj.input, ...
				"lambdaPositive", sum(obj.e1, 2));
			
			%% Consider inputs
			if isempty(obj.input.B)
				Bindomain = zeros(sum(obj.gridLength)-obj.n, sum(obj.input.length));
			else
				Bindomain = domain2indomain * obj.input.getBDiscrete(obj.grid);
			end
% 			if isempty(obj.input.B1) && isempty(obj.input.B0)
% 				Bbc = zeros(obj.n, sum(obj.input.length));
% 			elseif isempty(obj.input.B1)
% 				Bbc = [zeros(obj.p, sum(obj.input.length)); obj.input.B0];
% 			elseif isempty(obj.input.B0)
% 				Bbc = [obj.input.B1; zeros(obj.m, sum(obj.input.length))];
% 			else
% 				Bbc = [obj.input.B1; obj.input.B0];
% 			end
			
			%% Set state space model
			% non-convection parts
			AallDomain = AAdomain + AA0x1z02domain + AA1x2z12domain + AFdomain + ALambdaDomain;
			ssA = domain2indomain * AallDomain * (indomain2domain + bc2domain * bc.operator * indomain2domain);
			ssB = Bindomain + domain2indomain * AallDomain * bc2domain * bc.input;
			ssC = indomain2domain + bc2domain * bc.operator * indomain2domain;
			ssD = bc2domain * bc.input;
			obj.approximationNoConvection = ss(ssA, ssB, ssC, ssD);
			
			% set input and output names
			obj.approximationNoConvection = stateSpace.setSignalName(...
				obj.approximationNoConvection, 'input', ...
				obj.input.type, obj.input.length);
			obj.approximationNoConvection = stateSpace.setSignalName(...
				obj.approximationNoConvection, 'output', obj.stateName.pde, sum(myGridLength));
			
			%% add Outputs to simulation model
			obj.approximationNoConvection = obj.output.addOutput2stateSpace(...
				obj.approximationNoConvection, ...
				obj.stateName.pde, obj.grid, false);
			obj.approximationNoConvection = obj.input.addD2ss(obj.approximationNoConvection);
			
			% convection
			obj.approximationConvectionShift = domain2indomain * convectionShiftDomain;
		end % setApproximation()
		
		function myStateSpace = addConvection2SimulationModel(obj, ...
				stepSize, myContinuousStateSpace, convectionShift, varargin)
			% addConvection2SimulationModel converts the time continuous state space
			% myContinuousStateSpace to a time discrete model and adds the convection
			% terms, which are implemented by shift-matrix convectionShift.
			%
			% See also setApproximation, getClosedLoopSimulationModel, simulate.
			
			myParser = misc.Parser();
			myParser.addParameter("observer", []);
			myParser.parse(varargin{:});
			observer = myParser.Results.observer;
			
			% continuous -> discrete model
			myDiscreteStateSpace = c2d(myContinuousStateSpace, stepSize);
			
			% get matrices to extract the states of the plant obj and the observer as
			% well as the remaining outputs. Hereby, the OutputName property of the
			% ss-Model is compared to obj.stateName or observer.dynamics.stateName.
			allOutputNames = stateSpace.removeEnumeration(myDiscreteStateSpace.OutputName);
			logicIndexingPlantStateRows = strcmp(allOutputNames, obj.stateName.pde(1));
			if ~isempty(observer)
				logicIndexingObserverPdeStateRows = strcmp(allOutputNames, observer.dynamics.stateName.pde(1));
				convectionShift = blkdiag(convectionShift, ...
					observer.dynamics.approximationConvectionShift);
				logicIndexingObserverOdeStateRows = false(numel(allOutputNames), 1);
				for it = 1 : observer.ode.numTypes
					logicIndexingObserverOdeStateRows = ...
						logicIndexingObserverOdeStateRows ...
							| strcmp(allOutputNames, observer.ode.type{it});
					assert(find(logicIndexingObserverOdeStateRows, 1) > size(convectionShift, 2), ...
						['it is assumed, that first the pde states appear, and the ode dynamics ', ...
						'is placed below right in the approximations system matrix. Aparently, ', ...
						'this is not the case.'])
					convectionShift = blkdiag(convectionShift, eye(observer.ode.n(it)));
				end
			else
				logicIndexingObserverPdeStateRows = false(numel(allOutputNames), 1);
				logicIndexingObserverOdeStateRows = false(numel(allOutputNames), 1);
			end
			logicIndexingAllStates = ...
				logicIndexingPlantStateRows | logicIndexingObserverPdeStateRows ...
				| logicIndexingObserverOdeStateRows;
% 			selectAll = eye(size(myDiscreteStateSpace.C, 1));
% 			selectStates = selectAll(logicIndexingAllStates, :);
% 			selectOutputs = selectAll(~logicIndexingAllStates, :);
			
			Asim = myDiscreteStateSpace.A * convectionShift * myDiscreteStateSpace.C(logicIndexingAllStates, :);
			Bsim = myDiscreteStateSpace.B + ...
				myDiscreteStateSpace.A * convectionShift * myDiscreteStateSpace.D(logicIndexingAllStates, :);
			
% 			% get output again
% 			C = zeros(numel(allOutputNames), size(selectStates, 1));
% 			C(logicIndexingAllStates, :) = eye(size(selectStates, 1));
% 			C(~logicIndexingAllStates, :) = selectOutputs*Asim;
% 			D = zeros(numel(allOutputNames), size(Bsim, 2));
% 			D(~logicIndexingAllStates, :) = selectOutputs*Bsim;
			
			myStateSpace = ss(...
				Asim, ...
				Bsim, ...
				myDiscreteStateSpace.C, ... %[eye(size(selectStates, 1)); selectOutputs*Asim], ... % this is the fault!
				myDiscreteStateSpace.D, ... %[zeros(size(selectStates, 1), size(Bsim, 2)); selectOutputs*Bsim], ...
				stepSize, ...
				"OutputName", myContinuousStateSpace.OutputName, ...
				"InputName", myContinuousStateSpace.InputName);
		end % addConvection2SimulationModel()
		
		function myStateSpace = getClosedLoopSimulationModel(obj, varargin)
			% getClosedLoopSimulationModel creates a closed-loop simulation model containing the
			% lumped plant approximation, static and dynamic feedback and observer.
			% See also simulate, setApproximation, addConvection2SimulationModel.
			myParser = misc.Parser();
			myParser.addParameter("feedback", []);	% model.Output
			myParser.addParameter("observer", []);	% model.Observer
			myParser.addParameter("signalModel", []); % model.SignalModel
			myParser.addParameter("Odes", []);		% stateSpace.Odes
			myParser.addParameter("sumblk", []);	% sumblk
			myParser.parse(varargin{:});
			myFeedback = myParser.Results.feedback;
			myObserver = myParser.Results.observer;
			mySignalModel = myParser.Results.signalModel;
			myOdes = myParser.Results.Odes;
			mySumblk = myParser.Results.sumblk;
			
			% combine feedback and model:
			if isempty(myFeedback) && isempty(myObserver)
				% open loop
				% set approximation of system dynamics
				obj = obj.setApproximation(myParser.UnmatchedNameValuePair{:});
				myStateSpace = obj.approximationNoConvection;
			
				% add convection
				myStateSpace = obj.addConvection2SimulationModel( ...
					obj.stepSize, myStateSpace, obj.approximationConvectionShift, ...
					"observer", myObserver);
				
			elseif ~isempty(myFeedback) && isempty(myObserver) 
				% closed loop state feedback
				assert(~isempty(obj.input.get(obj.prefix + ".control")) && ...
					misc.iseye(obj.input.get(obj.prefix + ".control").B1), ...
					"the control input matrix must be an identity and defined at the right boundary");
				
				% get closed-loop dynamics by including feedback to boundary condition bc1. This
				% allows a better implementation of the closed-loop boundary dynamics.
				% 1. split pde state feedback and other gains in feedback.input
				fb = myFeedback.copyAndReplace("input", misc.Gains());
				otherGain = myFeedback.input.copy();
				% rename ode-feedback into ode2pde-type to consider it correctly when adding
				% otherGain to input
				if isfield(obj.stateName, "ode")
					otherGain.get(obj.stateName.ode).strrepInputType(obj.stateName.ode, obj.ode2pde.type);
				end

				% 2. set closed-loop boundary condition
				bc1ClosedLoop = parallel(obj.bc1, fb); 
				bc1ClosedLoop = bc1ClosedLoop.strrepType(bc1ClosedLoop.type, obj.bc1.type);

				% 3. add otherGain to input.
				inputClosedLoop = copy(obj.input);
				for it = 1 : otherGain.numTypes
					inputClosedLoop = inputClosedLoop.add(model.Input(...
						otherGain.gain(it).inputType, "B1", otherGain.gain(it).value));
				end
				inputClosedLoop.remove(obj.prefix + ".control");

				% 4. add feedback to output to obtain the control input in the
				% simulation output simData
				outputClosedLoop = copy(obj.output).add(...
					myFeedback.copy.strrepType(myFeedback.type, obj.prefix + ".control"));

				% 5. get closed-loop system.
				closedLoopSystem = obj.copyAndReplace(...
					"bc1", bc1ClosedLoop, ...
					"input", inputClosedLoop, ...
					"output", outputClosedLoop);

				% 6. get closed-loop approximation
				closedLoopSystem = closedLoopSystem.setApproximation(varargin{:});
				myStateSpace = closedLoopSystem.approximationNoConvection;
				obj.approximationNoConvection = closedLoopSystem.approximationNoConvection;
				obj.approximationConvectionShift = closedLoopSystem.approximationConvectionShift;
				obj.grid = closedLoopSystem.grid;
				obj.stepSize = closedLoopSystem.stepSize;
				obj.cflNumber = closedLoopSystem.cflNumber;
			
				% finally, add convection (earlier adding is bad for simulation result)
				myStateSpace = obj.addConvection2SimulationModel( ...
					obj.stepSize, myStateSpace, obj.approximationConvectionShift, ...
					"observer", myObserver);
				
			elseif ~isempty(myObserver)
				% observer parallel to plant
				% set approximation of system dynamics
				obj = obj.setApproximation(myParser.UnmatchedNameValuePair{:});
				myObserver = myObserver.setApproximation(myParser.UnmatchedNameValuePair{:});
				
				% Determine input and output signals of (observer+plant)-system
				myObserver.approximation = stateSpace.removeInputOfOutput(myObserver.approximation);
				myOutputName = [obj.approximationNoConvection.OutputName; myObserver.approximation.OutputName];
				myInputName = union(obj.approximationNoConvection.InputName, myObserver.approximation.InputName, 'stable');
				myInputName = setdiff(myInputName, myOutputName);	% for instance, measurement of 
				%													% plant is no input signal of 
				%													% closed loop system
				
				myStateSpace = stateSpace.connect( ...
					myInputName, myOutputName, ...
					obj.approximationNoConvection, myObserver.approximation);
			
				% finally, add convection (earlier adding is bad for simulation result)
				myStateSpace = obj.addConvection2SimulationModel( ...
					obj.stepSize, myStateSpace, obj.approximationConvectionShift, ...
					"observer", myObserver);
				
				if ~isempty(myFeedback)
					% observerFeedback is the original myFeedback, but with the input names changed
					% such that the observer signals are used. For instance, consider that
					% obj.prefix = "plant" and myObserver.prefix = "observer, then instead of
					% plant.disturbancesState the new observerFeedback uses 
					% observer.disturbanceState.
					observerFeedback = myFeedback.copy();
					if any(contains(observerFeedback.input.inputType, ".network."))
						assert(all(contains(observerFeedback.input.inputType, ".network.")), ...
							"only the case that all inputTypes are used for networked control " ...
							+ "is implemented yet")
						% this should be easy to resolve with a for loop though.
						% The prefix of cooperative feedback must not be altered!
					else
						observerFeedback = observerFeedback.copyAndReplace("input", ...
							observerFeedback.input.strrepInputType(...
								obj.prefix+".", myObserver.prefix+".", "startOnly", true));
					end
					
					% add observer based feedback to plant input
					myStateSpace = observerFeedback.useAsFeedback(myStateSpace, ...
						obj.prefix + ".control", myObserver.dynamics.stateName.pde, ...
						obj.grid, false);
					
					% add observer based feedback to observer input
					myStateSpace = observerFeedback.useAsFeedback(myStateSpace, ...
						myObserver.prefix + ".control", myObserver.dynamics.stateName.pde, ...
						obj.grid, false);
				end
				
			end
			
			if ~isempty(mySignalModel)
				myStateSpace = mySignalModel.connect2ss(myStateSpace, ...
					"stepSize", obj.stepSize);
			end % if ~isempty(signalModel)
			
			% additional sumblk (sum blocks)
			if ~isempty(mySumblk)
				if ~iscell(mySumblk)
					mySumblk = {mySumblk};
				end
				for it = 1 : numel(mySumblk)
					myStateSpace = stateSpace.connect(...
						[setdiff(myStateSpace.InputName, mySumblk{it}.OutputName); ...
							setdiff(mySumblk{it}.InputName, myStateSpace.OutputName)], ...
						[myStateSpace.OutputName; mySumblk{it}.OutputName], ...
						myStateSpace, mySumblk{it});
				end
			end % if ~isempty(mySumblk)
			
			% additional state-spaces defined as stateSpace.Odes object
			if ~isempty(myOdes)
				myStateSpace = stateSpace.connect(...
					[setdiff(myStateSpace.InputName, myOdes.OutputName); ...
						setdiff(myOdes.InputName, myStateSpace.OutputName)], ...
					[myStateSpace.OutputName; myOdes.OutputName], ...
					myStateSpace, c2d(myOdes.odes, obj.stepSize));
			end % if ~isempty(mySs)
			
		end % getClosedLoopSimulationModel()
				
		function [ic, stateName] = getInitialCondition(obj, varargin)
			% getInitialCondition reads varargin if there are initial conditions
			% defined as name-value-pair with the name according to obj.stateName and
			% returns 2 cell arrays: 
			%	1. ic is a cell array of the values of the states initial condition
			%	2. stateName is a cell array of the names of those states.
			
			icStruct = misc.struct(varargin{:});
			
			% Create initial condition for all states specified by obj.stateName
			% If a value is specified in icStruct, than this value is used.
			% Otherwise, a default value is considered.
			if misc.isfield(icStruct, obj.stateName.pde(1))
				x0quantity = misc.getfield(icStruct, obj.stateName.pde(1));
			else
				x0quantity = obj.setX0zero();
			end
			assert(numel(x0quantity) == obj.n, ...
				"vector of pdes initial condition has wrong length");
			x0 = [];
			for it = 1 : obj.n
				if it <= obj.p
					x0 = [x0; x0quantity(it).on(obj.grid{it}(1:end-1))];
				else
					x0 = [x0; x0quantity(it).on(obj.grid{it}(2:end))];
				end
			end
			
			% create output parameter
			ic = {x0};
			stateName = obj.stateName.pde;
		end % getInitialCondition()
		
		function spatialGrids = getSpatialGridsOfSimulation(obj, varargin)
			% getSpatialGridsOfSimulation returns a cell array containing 2 
			% cell-arrays. The first contains the names of signals that are 
			% distributed in space, the second one contains the spatial grids of 
			% those signals. The resulting cell-array spatial grids is later used in
			% stateSpace.simulationOutput2Quantity to split the simulations result
			% into signals.
			myParser = misc.Parser();
			myParser.addParameter("observer", []);
			myParser.parse(varargin{:});
			
			spatialGrids = {obj.stateName.pde(:), {obj.grid}};
			if ~isempty(myParser.Results.observer)
				spatialGrids{1} = [spatialGrids{1}(:); ...
					myParser.Results.observer.dynamics.stateName.pde(:)];
				spatialGrids{2} = [spatialGrids{2}(:); {myParser.Results.observer.grid}];
			end
		end % getSpatialGridsOfSimulation
		
		function x0 = setX01minusCos2PiZQuad(obj)
			% setX01minusCos2PiZQuad returns a vector x0 that can be used as initial condition fo
			% the PDE-state. Every element of x0 is 0.5*(1-cos(2*pi*z))^2.
			% See also setX0zero, setX0one.
			x0 = quantity.Symbolic(repmat(0.5*(1-cos(2*pi*sym("z")))^2, [obj.n, 1]), ...
				obj.Lambda(1).domain, "name", "x_{0}");
		end % setX01minusCos2PiZQuad()
		
		function x0 = setX0zero(obj)
			% setX0zero returns a vector x0 = 0 that can be used as initial condition for the
			% PDE-state.
			% See also setX01minusCos2PiZQuad, setX0one.
			x0 = quantity.Discrete.zeros([obj.n, 1], obj.Lambda(1).domain, "name", "x_{0}");
		end % setX0zero()
		
		function x0 = setX0one(obj)
			% setX0one returns a vector x0 = 1 that can be used as initial condition for the
			% PDE-state.
			% See also setX0zero, setX01minusCos2PiZQuad.
			x0 = quantity.Discrete.ones([obj.n, 1], obj.Lambda(1).domain, "name", "x_{0}");
		end % setX0one()
		
		function result = isDiagonalOfAZero(obj)
			% isDiagonalOfAZero returns true, if all diagonal elements of obj.A are zero,
			% otherwise, false is returned.
			% See also findHopfColeForAntidiagonalA
			result = numeric.near(obj.A.diag2vec.on(), 0*obj.A.diag2vec.on());
		end % isDiagonalOfAZero()
		
		function result = isA4isotachicZero(obj)
			% isA4isotachicZero returns true, if there is no coupling between transport PDEs with
			% same velocity Lambda
			% See also findHopfColeForAntidiagonalA
			result = true;
			for it = 1 : max(obj.isotachic)
				Atemp = obj.A(obj.isotachic==it, obj.isotachic==it);
				if MAX(abs(Atemp)) > 1e-12
					result = false;
					return
				end
			end
		end % isA4isotachicZero()
		
		function [T, TI, T_z, TI_z] = findHopfColeForAntidiagonalA(obj)
			% findHopfColeForAntidiagonalA calculates a Hopf-Cole type transformation, that ensures
			% that the diagonal elements of the PDE-parameter A of the transformed systems are zero.
			% Moreover, if there are transport PDEs with same velocities, that those are decoupled.
			% This can be ensured by applying the Hopf-Cole-Transformation
			%	x_ad(z) = T(z) x(z).
			% For this, it is assumed, that the velocity matrix is diagonal and sorted descendingly.
			% Then, T results as block-diagonal matrix
			%	T = diag(T_1, T_2, ..., T_k)
			% with k being the number of distinct Lambda-diagonal elements (= max(obj.isotachic) and
			%	T_i(z) = exp( int_0^z A_ii(zeta) / Lambda_ii(zeta) dzeta.
			%
			%	[T, TI] = findHopfColeForAntidiagonalA(obj) returns the transformation matrix T and
			%		its inverse TI.
			%
			%	[T, TI, T_z, TI_z] = findHopfColeForAntidiagonalA(obj) additionally returns the
			%		analytically determined spatial derivatives T_z = diff(T, "z", 1) and 
			%		spatial derivatives TI_z = diff(TI, "z", 1).
			%
			% See also isDiagonalOfAZero, feedback.riemann2antidiagonalA.
			
			assert(isdiag(obj.Lambda) && issorted(diag(obj.Lambda.at(0)), "descend"), ...
				"Lambda must be diagonal with descending elements")
			if obj.isA4isotachicZero()
				% if diagonal of A is already zero, then the transformation
				% matrix is the identity 1:
				sizeT = size(obj(1).A);
				T = quantity.Discrete.eye(sizeT, obj.A(1).domain, "name", "T");
				TI = quantity.Discrete.eye(sizeT, obj.A(1).domain, "name", "T_{I}");
				T_z = quantity.Discrete.zeros(sizeT, obj.A(1).domain, "name", "T_{z}");
				TI_z = quantity.Discrete.zeros(sizeT, obj.A(1).domain, "name", "T_{I,z}");
			else
				% Calculate T = diag(T_1, T_2, ..., T_k) with
				%	T_i(z) = exp( int_0^z A_ii(zeta) / Lambda_ii(zeta) dzeta.
				
				Tcell = cell(max(obj.isotachic), 1);
				T_zcell = cell(max(obj.isotachic), 1);
				TI_zcell = cell(max(obj.isotachic), 1);
				for it = 1 : numel(Tcell)
					selector = (obj.isotachic == it);
					Aii = obj.A(selector, selector);
					Lambdaii = obj.Lambda(selector, selector);

					Tcell{it} = expm(int(Aii / Lambdaii(1), "z", 0, "z"));
					T_zcell{it} = Aii * Tcell{it} / Lambdaii(1);
					TI_zcell{it} = -(Aii / Tcell{it}) / Lambdaii(1);
				end
				T = setName(blkdiag(Tcell{:}), "T");
				TI = inv(T);
				T_z = setName(blkdiag(T_zcell{:}), "T_{z}");
				TI_z = setName(blkdiag(TI_zcell{:}), "T_{I,z}");
			end
		end % findHopfColeForAntidiagonalA()
		
		function numerator = numeratorOfTransferFunction(obj, s, outputType)
			% numeratorOfTransferFunction calculates the numerator of the
			% transfer-function of the hyperbolic system obj for all complex
			% frequencies defined by s from the input defined by  inputType to the
			% output defined by outputType.
			% It is assumed that the system obj describes systems of the kind
			% PDE          x_t(z,t) = Lambda(z) x_z(z,t) + A0(z) x_1(0,t)
			% BC z=0	   x_2(0,t) = Q0 x_1(0,t)
			% BC z=1	   x_1(1,t) = u(t)
			% Output		   y(t) = C_op[x(z,t)].
			% Then the numerator can be calculated using the following calculations.
			arguments
				obj model.Transport;
				s (:, 1) =  1i * linspace(-100, 100, 201);
				outputType = obj.prefix + ".controlOutput";
			end
			% verify that the system has the structure specified above:
			assert(MAX(abs(obj.A)) == 0, "only implemented for A = 0");
			assert(MAX(abs(obj.F)) == 0, "only implemented for F = 0");
			assert(all(obj.bc0.isMatrixZeroOrEmpty(...
				"Cz0", "Ct0", "C1", "Ct1", "Cz1", "Cz", "Ct", "Czk", "Ctk", "Ck")), ...
				"only implemented for bc0: x_2(0) = Q0 x_1(0)");
			assert(all(obj.bc1.isMatrixZeroOrEmpty(...
				"Cz0", "Ct0", "C0", "Ct1", "Cz1", "Cz", "Ct", "Czk", "Ctk", "Ck")), ...
				"only implemented for bc1: x_1(1) = u");
			assert(~isempty(obj.input.get(obj.prefix + ".control")) ...
				&& misc.iseye(obj.input.get(obj.prefix + ".control").B1), ...
				"control input has to be defined at z=1 and its gain must be the identity matrix");
			
			% check frequencies and sort them, since quantity might be buggy for non
			% sorted frequencies.
			assert(max(real(s), [], "all") < 10*eps, ...
				"only implemented for imaginary frequencies without real part");
				% in fact, the calculation should work also with general complex s,
				% but sorting would be complicated and usually this is evaluated for 
				% only imaginary s.
			s = 1i * unique(imag(s), "sorted");
			sQuantity = quantity.Discrete(s, quantity.Domain("imag(s)", imag(s)));
			
			% calculate numerator
			LambdaInv = obj.Lambda.inv();
			fundamentalMatrix = vec2diag(exp(sQuantity * ...
				int(LambdaInv.diag2vec.subs("z", "eta"), "eta", "zeta", "z")));
			M = fundamentalMatrix.subs("zeta", 0) * (obj.e1 + obj.e2 * obj.Q0) ...
				- int(fundamentalMatrix * subs(LambdaInv * obj.A0, "z", "zeta"), ...
					"zeta", 0, "z");
				
			% get numerator N by inserting M it in the controlOutput operator
			controlOutput = obj.output.get(outputType);
			numerator = controlOutput.out(M).setName("numerator");
		end % numeratorOfTransferFunction()
				
		function gridLength = get.gridLength(obj)
			% gridLength is the number of discrete points used for the numerical
			% approximation. Since the number of grid points may be individual for
			% every state, gridLength is a vector of the lengths of each state
			gridLength = cellfun(@(v) numel(v), obj.grid);
		end % get.gridLength()
		
		function myStateName = stateName(obj)
			% stateName getter of stateName struct.
			if isscalar(obj)
				myStateName.pde = obj.prefix + ".x";
			else
				myStateNameCell = cell(size(obj));
				for it = 1 : numel(obj)
					myStateNameCell{it} = obj(it).stateName;
				end
				myStateName = cat(1, myStateNameCell{:});
			end
		end % stateName()
		
		function [texString, nameValuePairs] = printParameter(obj)
			% printParameter displays a list of all parameters in the command window.
			%
			% See also misc.variables2tex, misc.variables2tex, misc.latexChar, print.
			nameValuePairs = {
				"\Lambda", obj.Lambda, ...
				"A", obj.A, ...
				"F", obj.F, ...
				"A0", obj.A0, ...
				"Q0", obj.Q0, ...
				"Q1", obj.Q1, ...
				};
			texString = misc.variables2tex([], nameValuePairs);
		end % printParameter
		
		function texString = print(obj, stateNamePde, stateNameOde, doPrintOutput)
			% print Print equation to command window as latex compatible string-array.
			%
			% See also misc.variables2tex, misc.variables2tex, misc.latexChar, printParameter.
			
			arguments
				obj;
				stateNamePde = strrep(obj.stateName.pde(1), obj.prefix + ".", "");
				stateNameOde = "";
				doPrintOutput = true;
			end % arguments
			
			% prepare pde
			suffix.A = stateNamePde+"(z, t)";
			suffix.A0 = stateNamePde+"(0, t)";
			suffix.F = stateNamePde+"(\zeta, t) \mathrm{d}\zeta";
			option.rational = true;
			pre.F = "\int\limits_{" + misc.latexChar(obj.FBounds{1}, option) ...
				+ "}^{" + misc.latexChar(obj.FBounds{2}, option) + "}";
			
			% put pde together
			pdeString = "\dot{"+stateNamePde+"}(z, t) &= " ...
				+ misc.latexChar(obj.Lambda) + " " + stateNamePde+"^{\prime}(z, t)";
			for myPar = ["A", "A0", "F"]
				if MAX(abs(obj.(myPar))) ~= 0
					pdeString = pdeString + " +";
					if isfield(pre, myPar)
						pdeString = pdeString + " " + pre.(myPar);
					end
					if iseye(obj.(myPar))
						pdeString = pdeString + " ";
					else
						pdeString = pdeString + " " + misc.latexChar(obj.(myPar));
					end
					if isfield(suffix, myPar)
						pdeString = pdeString + " " + suffix.(myPar);
					end
				end
			end
			
			myInput = copy(obj.input);
			if isa(obj, "model.TransportOde")
				myInput = myInput.remove(obj.ode2pde.type);
				ode2pdeString = [...
					obj.ode2pde.print("B", stateNameOde + "(t)"); ...
					obj.ode2pde.print("B0", stateNameOde + "(t)"); ...
					obj.ode2pde.print("B1", stateNameOde + "(t)")];
				if ~obj.ode2pde.isMatrixZeroOrEmpty("D")
					warning("D in ode2pde is nonzero but not printed");
				end
			else
				ode2pdeString = [""; ""; ""];
			end
			
			% add inputs and boundary conditions
			myStringArray = [...
					pdeString + myInput.print("B"); ...
					obj.bc0.print(0, stateNamePde) + myInput.print("B0"); ...
					obj.bc1.print(0, stateNamePde) + myInput.print("B1")]...
				+ ode2pdeString;
			
			% add output
			if doPrintOutput
				tempOutput = obj.output.copy.add(myInput.D);
				myStringArray = [myStringArray; tempOutput.print()];
			end
			
			% set output parameter or print
			if nargout > 0
				texString = myStringArray;
			else
				misc.printTex(myStringArray);
			end
		end % print()
		
		function interpMatrix = get.interpMatrix(obj)
			% get.interpMatrix returns interpolation matrix to interpolate between the spatial grids
			% of obj.grid.
			% Since every transport equation has its own grid, it
			% might be useful to translate between those grids by interpolation.
			% Therfore, interpolation matrices are calculated in the following. For
			% instance, if you have a distributed function a(z) as a vector a_i
			% which consists of a(z) evaluated at every point of the i-th grid
			% .zdisc{i}, then you can "interpolate" a_j - which is a(z) evaluated
			% at every point of the j-th grid .zdisc{j} by calculating
			%	a_j = interpMatrix{j, i} * a_i
			interpMatrix = cell(obj.n, obj.n);
			for it = 1 : obj.n
				for jt = 1 : obj.n
					interpMatrix{it, jt} = misc.interp_mat(obj.grid{jt}, obj.grid{it});
				end
			end
		end % get.interpMatrix()
		
		function Psi = Psi(obj, s)
			% Psi = diag(exp(s * (phi_1(z) - phi_1(zeta), ...
			%				s * (phi_2(z) - phi_2(zeta), ...
			%							...	
			%				s * (phi_n(z) - phi_n(zeta)))
			phi = obj.Phi;
			Psi = exp(s * phi);
			Psi = vec2diag(Psi ./ Psi.subs("z", "zeta"));
			Psi = Psi.setName("Psi");
		end % Psi(obj)
		
		function Phi = get.Phi(obj)
			% phi_i = int_0^z (1 / lambda_i(zeta)) dzeta
			% Phi = [phi_1; phi_2; ... ; phi_n]
			Phi = int(rdivide(1, diag2vec(obj.Lambda)), "z", 0, "z");
			Phi = Phi.setName("Phi");
		end % get.Phi(obj)
		
		function settlingTime = get.settlingTime(obj)
			% based on the state feedback control of hyperbolic transport systems,
			% this settling time is the nominal settling time via backstepping (finite) or
			% backstepping-Fredholm (minimum) boundary control.
			phi1 = int(1 ./ diag2vec(obj.Lambda), "z", 0, 1);
			settlingTime.minimum = phi1(obj.p) - phi1(obj.p+1);
			settlingTime.finite = sum(abs(phi1(1:(obj.p+1))));
			settlingTime.finiteAlt = sum(abs(phi1(obj.p:end)));
		end % get.settlingTime()
		
		function obj = setIsotachic(obj)
			% isotachic states have the same velocity, i.e., identical values 
			%	Lambda(i, i) - Lambda(j, j) = 0.
			% This isotachic-property is an array that indexes identical Lambda-values.
			% For instance, if 
			%	Lambda = diag([3, 2, 2, -1, -1, -1.5]),
			% then 
			%	isotachic = [1; 2; 2; 3; 3; 4].
			if isdiag(obj.Lambda)
				Lambda0 = round(diag(obj.Lambda.at(0)), 3);
				if obj.n == numel(unique(Lambda0))
					obj.isotachic = (1:1:obj.n).';
					return
				else
					[~, idx] = sort(Lambda0, "descend");
					isotachicGroupIdx = 1;
					obj.isotachic = ones(obj.n, 1);
					for it = 2:obj.n
						lastIdx = idx(it-1);
						currentIdx = idx(it);
						if MAX(abs(...
							obj.Lambda(lastIdx, lastIdx) - obj.Lambda(currentIdx, currentIdx)))...
								>= 1e-3
							isotachicGroupIdx = isotachicGroupIdx + 1;
						end
						obj.isotachic(currentIdx) = isotachicGroupIdx;
					end
				end
			else
				obj.isotachic = [];
			end
		end % setIsotachic()
		
		function Q0 = get.Q0(obj)
			% Q0 is the matrix of the boundary condition
			%	x_2(0, t) = Q0 * x_1(0, t) + f(input)
			Q0 = obj.bc0.C0(:, 1:obj.p);
		end % get.Q0(obj)
		
		function Q1 = get.Q1(obj)
			% Q1 is the matrix of the boundary condition
			%	x_1(1, t) = Q1 x_2(1, t) + f(input)
			Q1 = obj.bc1.C1(:, (obj.p+1):end);
		end % get.Q1(obj)
				
		function [dt] = suggestStepSize(obj, desiredStepSize, display)
			% SUGGESTSTEPSIZE Suggest a time good discretization 
			%	dt = suggestStepSize(obj, desiredStepSize, display)
			%	will compute a time discretization, so that a spatial
			%	discretization is possible in order to achieve a good
			%	cfl-number. For this, the transition time
			%		T_i = | int_0^1 1 / lambda_i(z) dz |
			%	must be a integer multiple, i.e.,
			%		T_i = dt * n_i
			%	with dt as step size and n_i as integer.
			%	In general it will be not possible to find a suitable dt
			%	for all lambda_i.
			%	With the display input parameter (default: "off") the display option
			%	of the fminsearch optimizer can be set, see doc fminsearch.
			
			arguments
				obj;
				desiredStepSize = 1e-2;
				display = "off";
			end		
				
			% check if the desired step size satisfies the required
			% tolerance:	
			% J = int_0^1 ( 1 - cfl ).^2
			J = @(dt) obj.costFunctional(dt);
			
			if J(desiredStepSize) < 1e-6
				dt = desiredStepSize;
			else
				options = optimset("TolX", 1e-6, "display", display);
				dt = fminsearch( J, desiredStepSize, options);
			end
			
			[cfl, myGrid] = computeCflGrid(obj, dt);
			
			misc.warning( all( cellfun( @(c) numel(c) > 1, myGrid ) ), ...
				"Step size is too small. Spatial grid must have more than one point!");
			
			misc.warning( all([cfl{:}] <= 1.05), ...
				"cfl number = " + num2str(max(abs([cfl{:}]))) + " > 1");
			
		end % suggestStepSize()
		
		function J = costFunctional(obj, dt)
			% COSTFUNCTIONAL for the selection of a time discretization.
			% See also suggestStepSize.
			
			tol = 1e-3;
			
			cfl = obj.computeCflGrid(dt, "UseIteration", true);
			
			CFL = cell2mat(cfl');
			
			% ensure that the cfl number is never bigger than one:
			CFL( CFL > 1 + tol ) = inf;
			
			% quadratic cost functional
			J = sum( (1 - CFL(:)).^2 );
			
		end % costFunctional()
	
		function [cfl, myGrid] = computeCflGrid(obj, dt, varargin)
			% computeCflGrid caluclates a discretization of the spatial grid.
			%
			%	[cfl, myGrid] = computeCflGrid(obj, dt) the spatial discretization myGrid is set
			%		such that the cfl-number is close to 1 for a given step-size dt.
			%
			%	computeCflGrid(obj, dt, "UseIteration", UseIteration) if UseIteration=true
			%		(default), then the grid is calculated using a iterative method of adding grid
			%		points. If UseIteration=false, the step size is calculated in an analytic way,
			%		based on the transport velocity.
			
			myParser = misc.Parser();
			myParser.addParameter("UseIteration", true);
			myParser.parse(varargin{:});
			useIteration = myParser.Results.UseIteration;
			
			assert(isequal(obj.Lambda(1).domain.grid([1; end]), [0; 1]), ...
				"This method only supports spatial grids defined as z \in [0, 1].")
				
			%% Init Parameters
			myGrid = cell(obj.n, 1);
			cfl = cell(obj.n, 1);% data store of grid for every transport equation
			%myLambda = sign(obj.riemannAntidiagonal.Lambda.at(0)) * max(abs(obj.Lambda));
			% The step size of the spatial grid is choosen such that the
			% resulting cfl-number is closest possible to 1, but not greater than 1.
			% The cfl-number is known by the equation
			%	cfl = lambda * dt / dz
			% in which lambda is (negative) transport velocity of the transport
			% equation x_dt = lambda(z) x_dz [+ ... some not relevant terms ...].
			% Since lambda (system parameter), the cfl-number (1) and dt
			% (simulation parameter) are already set, dz has to be determined such
			% that the equation above is fullfilled. Hence
			%	dz = lambda(z) * dt / cfl			(I)
			% has to hold. Since lambda is space-dependent, dz is space-dependent
			% and the equation can not be solved explicitly, but solving it iterative 
			% is quite easy. Just start at z=0, evaluate the equation (I), then you
			% get dz(0). The next discrete point of the grid results from
			% z=0+dz(0), hence (I) can be solved for that point. And so on until 
			% the boundary of the domain is reached. This results in grid of the 
			% spatial variable such that cfl=1 everywhere. Sadly, its very common,
			% that the last point of this calculated grid doesn't coincide with the
			% value of z at the boundary, since the grid is discrete. The solution
			% is to stop just before the boundary, and strech the grid afterwards,
			% which reduces the cfl-number to somewhat less than 1, but this is the
			% best solution possible for that choice of dt. This calculations are
			% done in the following loop for every of the i=1...n transport
			% equations.
			for it=1:obj.n

				LambdaIt = obj.Lambda(it, it).abs();
				
				assert(min(LambdaIt) > 1e-6, ...
					"if there is no convection (i.e. |Lambda(z)| too small), then"...
					+ " a very fine spatial discretization results.");
		
				if useIteration
					% Use an iterative way to compute the spatial
					% discretiation:
					%	for a time step size the space discretization must
					%	be computed separatley for each step. 
					z_k = 0;		% start iterating at left boundary z=z0
					zdiscArray = [];% temporary. 
					k=1;			% index variable of the following while-loop
					
					% since the while-loop might need numerous iterations for the calculation of
					% dz_k a function handle is used instead of  LambdaIt.at() * dt to improve
					% calculation speed
					if isa(LambdaIt, "quantity.Function")
						dz_k = @(z_k) LambdaIt.valueContinuous(z_k) * dt;
					else
						LambdaItInterpolant = LambdaIt.getInterpolant();
						dz_k = @(z_k) LambdaItInterpolant.evaluate(z_k) * dt;
					end

					% iterate through spatial domain until right boundary
					% is almost reached
					while round(z_k, 14) <= 1 
						zdiscArray(k, 1) = z_k; %#ok<AGROW>
						z_k = z_k + dz_k(z_k);
						k=k+1;
					end
					
					myGrid{it} = zdiscArray;
				else
					% use the invert function to compute the discretization
					% Due to
					%	T = int_0^1 1 / lambda_i(z) dz = F(z)
					% and an equidistant time discretization, the inverse
					% function z = F^-1(t) can be used to compute the
					% spatial varying grid.
					X = int( 1 ./ LambdaIt, "z", 0, "z");
					X_inv = X.invert();
					T = X.at(1);
					timeGrid = 0:dt:T;
					myGrid{it} = X_inv.on(timeGrid);					
				end
				
				% in general the length of the spatial domain is not reached exactly. Thus, the
				% domain will be streched by a factor to ensure a length of 1.
				st = myGrid{it}(end) - myGrid{it}(1);
				myGrid{it} = myGrid{it} / st;
				% set the final value explicitly to 1 in order to avoid numerical errors.
				myGrid{it}(1) = 0; 
				myGrid{it}(end) = 1; 
				
				% compute cfl-number
				LambdaTemp = LambdaIt.on(myGrid{it});
				if it <= obj.p
					cfl{it} = ( LambdaTemp(1:end-1) * dt ...
						./ abs(myGrid{it}(2:end)-myGrid{it}(1:end-1)) )';
				else
					cfl{it} = ( LambdaTemp(2:end) * dt ...
						./ abs(myGrid{it}(2:end)-myGrid{it}(1:end-1)) )';
				end
			end % for it = 1 : obj.n
			obj.cflNumber = cfl;
		end % computeCflGrid()
		
		function obj = discretizeQuantity(obj)
			% discretizeQuantity converts all properties containg quantity-objects to
			% quantity.Discrete objects. This is often speeds up calculations.
			for it = 1 : numel(obj)
				for prop = ["Lambda", "A", "A0", "F"]
					obj(it).(prop) = quantity.Discrete(obj(it).(prop));
				end
				obj(it).input = obj(it).input.discretizeQuantity();
				obj(it).output = obj(it).output.discretizeQuantity();
				obj(it).bc0 = obj(it).bc0.discretizeQuantity();
				obj(it).bc1 = obj(it).bc1.discretizeQuantity();
			end
		end % discretizeQuantity()
		
	end % methods 
		
	methods (Access = public)
		function [x, u] = setPointChange(obj, y_f, tau, NameValue )
			% SETPOINTCHANGE flatness based trajectory planning
			%	[X, W, U, X1_0] = setPointChange(obj, Y_F, TAU ) uses flatness to plan a trajectory
			%	for the transport system. The system must be in the form
			%		dt x = Lambda x + A_0 x_1(0)
			%	  x_2(0) = Q_0 x_1(0)
			%	  x_1(1) = Q_1 x_2(1) + u.
			%	Then, the virtual flat output is 
			%		x_1(0,s) = y_f(s).
			%
			% The solution will be computed by the differential parameterization, given in the 
			% frequency domain:
			%	x(z,s) = M(z,s) * x_1(0,s) = M(z,s) * y_f(s)
			%
			% The spatial dependent transfer matrix M(z,s) contains time-delay/prediction operators
			% of the form exp(s f(z,zeta)). These operators are computed on the basis of the general
			% solution for the ODE 
			%	dz x(z,s) = s Lambda^1- x(z,s) - Lambda^-1 A_0 x_1(0).
			%
			% The solution using the boundary condition is
			%	x(z,s) = Phi(z,0,s) (e_1 + e_2 * Q_0 ) x_1(0) + ...
			%			 - int_0^z Psi(z,zeta,s) x_1(0) dzeta
			%	and
			%	  u(s) = x_1(1, s) - Q_1 x_2(1, s)
			% using
			%	Phi(z,zeta,s) = diag( exp( s * phi_i(z, zeta ) ) ),
			%	phi(z,zeta) = int_zeta^z 1 / lambda_1( xi ) d x, and,
			%	Psi(z,zeta,s) = Phi(z,zeta,s) Lambda^-1(zeta) A_0(zeta).
			%
			%	For a simpler implementation M(z,s) is separated in to
			%		M(z,s) = M_1(z,s) + M_2(z,s)
			%	with
			%		M_1(z,s) = Phi(z,0,s) * (E_1 + E_2 Q_0)
			%		M_2(z,s) = - int_0^z Psi(z,zeta,s) dzeta.
			% 
			%	[x, u] = setPointChange(obj, y_f, tau) calculates the trajectory of the pde states X
			%		and the control input U, such that the trajectory Y_F is realized. All those
			%		quantites must be quantity.Discrete objects. Additionally, the quantity.Domain
			%		object TAU defines the time domain, which must be sufficiently larger then the
			%		domain of y_f, due to the predictions and delay operators
			%
			%	simData = setPointChange([...], "struct", true) returns X and U as fields of the
			%		struct simData. Additionally, all outputs defined by obj.output will be fields
			%		of simData.
			%
			%	simData = setPointChange([...], "struct", true, "output2struct", false) deactivates,
			%		that further output signals are calculated and stored in simData.
			%
			%	setPointChange([...], "controlInputName", controlInputName) the string
			%		controlInputName defines which obj.input will be used as control input. It will
			%		be used for verifying, that the control input is defined as x_1(1) = u.
			
			% See also model.TransportOde.setPointChange, stateSpace.setPointChange and
			% model.TransportOde.setPointChange4output.
			arguments
				obj;
				y_f;
				tau quantity.Domain;
				NameValue.struct (1, 1) logical = false;
				NameValue.output2struct (1, 1) logical = true;
				NameValue.controlInputName (1, 1) string = obj.prefix + ".control";
			end % arguments
			
			% input verifications
			assert( all( obj.A.on == 0, "all"), "A matrix must be zero" )
			assert( all( obj.F.on == 0, "all"), "F matrix must be zero" )
			assert( any(strcmp(obj.input.type, NameValue.controlInputName)), ...
				"There must be an obj.input.type that suites NameValue.controlInputName");
			assert( misc.iseye(obj.input.get(NameValue.controlInputName).B1),  ...
				"B1 of obj.input.get(NameValue.controlInputName) must be an Identity matrix I");
			assert( all(obj.input.get(NameValue.controlInputName).isMatrixZeroOrEmpty("B", "B0", "D")), ...
				"The control input, i.e., obj.input.get(NameValue.controlInputName), must be " ...
				+ "defined only at the right boundary");
			
			% prepare local parameter
			t = y_f.domain;
			try
				lambda = diag2vec( obj.Lambda );
				phi = int( 1 ./ lambda, "z", "zeta", "z");
				phi0 = phi.subs("zeta", 0);

				Phi = diag( signals.TimeDelayOperator(phi, tau) );
				Phi0 = diag( signals.TimeDelayOperator(phi0, tau) );

				% x_homogeneous(z,t) = Phi(z,0,t) [ (e1 + e2 Q0) x_1(0, t) ]
				xHom = Phi0.applyTo( (obj.e1 + obj.e2 * obj.Q0) * y_f );

				% computation of the particular part
				% At first compute the part the operator will be applied to:
				%	F(z,t) = Lambda(z)^-1 * A0(z) * x_1(0,t)
				F = obj.Lambda \ obj.A0 * y_f;
				% Then, apply the time-delay operator to the state and space
				% dependent function:
				%	Psi(z,zeta,t)[x_1(0,t)] = Phi(z,zeta,t) [ M1(z,s) ]
				PsiX_1_0 = Phi.applyTo( F.subs("z", "zeta"), "domain", t );
			catch ME
				if strcmp(ME.identifier, "MATLAB:array:SizeLimitExceeded")
					zOriginal = obj.Lambda(1).domain.find("z")
					if zOriginal.n > 200
						nLowRes = 101;
					else
						nLowRes = ceil(zOriginal.n / 2);
					end
					warning("Trajectory planning with "+string(zOriginal.n) + " grid points in " ...
						+ "space failed because of memory limits. I will try it again with " ...
						+ string(nLowRes) + " grid points. If this failes again, try it with " ...
						+ "even coarser discretization.");
					zLowRes = quantity.Domain("z", linspace(zOriginal.lower, zOriginal.upper, nLowRes));
					lambda = diag2vec( obj.Lambda );
					lambda.changeDomain(zLowRes);
					phi = int( 1 ./ lambda, "z", "zeta", "z");
					phi0 = phi.subs("zeta", 0);

					Phi = diag( signals.TimeDelayOperator(phi, tau) );
					Phi0 = diag( signals.TimeDelayOperator(phi0, tau) );

					% x_homogeneous(z,t) = Phi(z,0,t) [ (e1 + e2 Q0) x_1(0, t) ]
					xHom = Phi0.applyTo( (obj.e1 + obj.e2 * obj.Q0) * y_f );

					% computation of the particular part
					% At first compute the part the operator will be applied to:
					%	F(z,t) = Lambda(z)^-1 * A0(z) * x_1(0,t)
					F = changeDomain(obj.Lambda \ obj.A0 * y_f, zLowRes);
					% Then, apply the time-delay operator to the state and space
					% dependent function:
					%	Psi(z,zeta,t)[x_1(0,t)] = Phi(z,zeta,t) [ M1(z,s) ]
					PsiX_1_0 = Phi.applyTo( F.subs("z", "zeta"), "domain", t );
				else
					rethrow(ME);
				end
			end
			% At least, do the integration:
			%	x_particular = - int_0^z Psi(z,zeta,t)[x_1(0,t)] d zeta
			xPar = - int( PsiX_1_0, "zeta", 0, "z" );
			
			% x = x_homogeneous + x_particular
			x = xHom + xPar;
			
			% compute the input
			u = -obj.bc1.out(x);
			
			% if the struct = true input option is chosen, the output x will be replaced by a
			% struct containing all the trajectories.
			if NameValue.struct
				myStruct = misc.struct(...
					obj.stateName.pde(1), x.setName(obj.stateName.pde(1)), ...
					obj.stateName.ode(1), w.setName(obj.stateName.ode(1)), ...
					NameValue.controlInputName, u.setName(NameValue.controlInputName), ...
					"flatOutput", quantity.Discrete(y_f).changeDomain(t).setName("flatOutput"));
				if NameValue.output2struct
					% if the output2struct = true input is chosen, additionally, all trajectories of
					% outputs given by obj.output are calculated.
					for it = 1 : obj.output.numTypes
						outputValue = obj.output.output(it).out(x);
						if any(strcmp(obj.output.output(it).inputTypes, obj.stateName.ode))
							outputValue = outputValue + obj.output.output(it).input.valueOf(...
								obj.stateName.ode) * w;
						end
						
						myStruct = misc.setfield(myStruct, obj.output.type(it), ...
								outputValue.setName(obj.output.type(it)));
					end % for it = 1 : obj.output.numTypes
				end % if NameValue.output2struct
				x = myStruct;
			end % if NameValue.struct
		end % function setPointChange()
		
	end % methods (Access = public)
	
	methods% (Access = private)
		
		function obj = setCflGrid(obj, dt, varargin)
			% setCflGrid sets the cfl grid suiting to the temporal step size dt.
			% Additional to computeCflGrid, some verifications are performed.
			% See also computeCflGrid, setApproximation.
			[cfl, myGrid] = obj.computeCflGrid(dt, varargin{:});

			assert(   all( cellfun( @(c) numel(c) > 1, myGrid ) ), ...
				"Step size is too small. Spatial grid must have more than one point!");
			
			assert(all([cfl{:}] <= 1.05), "cfl number = " + num2str(max(abs([cfl{:}]))) + " > 1");	
			
			obj.cflNumber = cfl;
			obj.grid = myGrid;
		end % setCflGrid()
		
	end % methods (Access = Private)
	
	
	methods (Access = protected)
		function cpObj = copyElement(obj)
			% Override copyElement method:
			% Make a shallow copy of all properties
			cpObj = copyElement@matlab.mixin.Copyable(obj);
			cpObj.Lambda = copy(obj.Lambda);
			cpObj.A = copy(obj.A);
			cpObj.F = copy(obj.F);
			cpObj.A0 = copy(obj.A0);
			cpObj.input = copy(obj.input);
			cpObj.output = copy(obj.output);
		end % function cpObj = copyElement(obj)
	end % methods (Access = protected)
end % classdef: model.Transport
