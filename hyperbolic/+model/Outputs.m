classdef Outputs < handle & matlab.mixin.Copyable
%Outputs is class to enease implementation of multiple different output 
%signals of dps, for instance control, measurement or
%couplings with other systems. Outputs at both boundaries, distributed
%outputs and pointwise are implemented.
%For every output there is an Output-object (singular!), which 
%contains the corresponding matrices and information for that output.
%This class Outputs (plural!) contains all outputs of a dps and acts as the
%interface to the classes of dps.

	properties (SetAccess = protected)
		output (1, :) model.Output; % this array contains the data of
		%							% each output
	end

	properties (Dependent = true)
		% data resulting from the input-property:
		% left boundary
		C0 (:,:);					% dirichlet left boundary (z=0)
		Cz0 (:,:);					% neumann left boundary (z=0)
		Ct0 (:,:);					% time-diff left boundary (z=0)
		% right boundary
		C1 (:,:);					% dirichlet right boundary (z=1)
		Cz1 (:,:);					% neumann right boundary (z=1)
		Ct1 (:,:);					% time-diff right boundary (z=1)
		% pointwise in-domain
		zk (:,1) double;			% points of pointwise outputs
		k (:,1) double;				% number of points used for pointwise outputs
		Ck (:,:,:);					% dirichlet pointwise in-domain (z_k
		Czk (:,:,:);				% neumann right boundary (z=1)
		Ctk (:,:,:) ;				% time-diff right boundary (z=1)
		% distributed
		C (:,:) quantity.Discrete;	% distributed characteristic of int
		Cz (:,:) quantity.Discrete;	% distributed characteristic of int
		Ct (:,:) quantity.Discrete;	% distributed characteristic of int
		% pass-through
		input (1,1) misc.Gains;		% inputs resulting in D
		D (:,:);					% gain of input (disturbances, ...)
		inputTypes string {mustBe.unique}	% type of inputs
		
		% data about output
		type string {mustBe.unique};	% type of output signal, for instance 
		%							% 'control', 'measurement'.
		%							% Output types must be unique, this is
		%							% verified in the constructor and in
		%							% add().
		lengthOutput (1, 1) double;	% length of output vectors
		lengthState (1, 1) double;	% length of state vector
		lengthInput (1, 1)			% length of input vector
		numTypes (1, 1) double;		% number of different output-object that
		%							% this Outputs-object contains
		withTimeDerivative (1, 1) logical	% flag that indicates if 
		%							% Ct, Ctk, Ct1, Ct0 are empty (then its false),
		%							% or not (true).
		
		% approximation
		Cop (:,:) double;			% Cop is a matrix for representing the
		%							% this output numerically. Multiplying
		%							% this array with discrete states
		%							% yields the output, i.e.:
		%							%	y = Cop * x
		%							% x is a state vector in the form
		%							% [x1(0:1), x2(0:1), ... xn(0:1)]^T
		grid cell;				% grid contains the grid for 
		%							% every state x1, x2, ..., xn
		gridLength;				% length of the grids in grid
	end
	
	methods
		function obj = Outputs(varargin)
			if nargin > 0
				%% Constructor
				% all outputs of constructor in varargin are of output_operator
				% class
				obj.add(varargin{:});
			end % if nargin > 0
		end % Output() Constructor
		
		function y = out(obj, x, NameValue)
			% out Evaluate the output operator for a given state distributed state x, a time
			% derivative xt and the feedthrough input u.
			arguments
				obj;
				x quantity.Discrete;
				NameValue.u = zeros([obj.lengthInput, size(x, 2)]);
				NameValue.xt quantity.Discrete = x.diff("t", 1);
			end
			assert(size(x, 1) == obj.lengthState, "x must have obj.lenghtState rows");
			assert(all(size(NameValue.xt) == size(x)), ...
				"xt must have obj.lenghtState rows and the same number of columns as x");
			assert(size(NameValue.u, 1) == obj.lengthInput, ...
				"u must have sum(obj.lengthInput) rows");
			
			y = [];
			for it = 1 : obj.numTypes
				y = vertcat(y, obj.output(it).out(x, "xt", NameValue.xt));
			end
			
			% consider input u
			if obj.lengthInput > 0
				 y = y + obj.D * NameValue.u;
			end
		end % out()
		
		function obj = setApproximation(obj, grid, NameValue)
			arguments
				obj;
				grid;
				NameValue.withTimeDerivative (1, 1) logical = false;
				NameValue.stencilLength (1, 1) double = 2;
				NameValue.method (1, 1) string ...
					{mustBeMember(NameValue.method, ["cheb", "finiteDifferences"])} ...
					= "finiteDifferences";
			end
			NameValuePairs = misc.struct2namevaluepair(NameValue);
			for it = 1 : obj.numTypes
				obj.output(it) = obj.output(it).setApproximation(grid, ...
					NameValuePairs{:});
			end
		end % setApproximation()
		
		function myStateSpace = addOutput2stateSpace(obj, myStateSpace, stateName, ...
				grid, withTimeDerivative, varargin)
			for it = 1 : obj.numTypes
				myStateSpace = addOutput2stateSpace(obj.output(it), myStateSpace, stateName, ...
					grid, withTimeDerivative, varargin{:});
			end
		end % addOutput2stateSpace()
		
		function myStateSpace = useAsFeedback(obj, myStateSpace, controlInputName, ...
				stateName, grid, withTimeDerivative, varargin)
			% USEASFEEDBACK connects the feedback with the state-space myStateSpace.
			% The feedback is approximated as state-space block and its input- and 
			% output-names are  set according to the input parameter. 
			% For details and implementation see model.Output.useAsFeedback.
			for it = 1 : obj.numTypes
				myStateSpace = useAsFeedback(obj.output(it), myStateSpace, controlInputName, ...
					stateName, grid, withTimeDerivative, varargin{:});
			end
		end % useAsFeedback()
		
		function [bcOperatorAllDomain, weights, inputMatrix] = ...
										useAsBoundaryOperator(obj, type, z, varargin)
			% useAsBoundaryOperator approximates the output operator obj in order to implement
			% boundary conditions as algeraic equations.
			%
			% To give a simple example, in order to implement the Riemann boundary condition
			%	0 = x(1, t) + x_z(1, t) + B u(t)
			% with the input u(t), the state x(z, t), z in [0, 1] is discretized in space and the
			% Neumann term is approximated using finiteDifferences or chebfun. Then, the boundary
			% equation can be solved such that an algebraic equation
			%	x(1, t) = BC[x(t)] + B2 u(t)
			% results with x(z, t) discretized in space. This method returns the lumped
			% approximation of the operator BC (bcOperatorAllDomain) and the inputMatrix B2 for a
			% given boundary operator obj.
			% In general, model.Output-objects can be used to represent general boundary conditions
			% that are described by
			% 0 = BC[x] + B u
			%	= C0 x(0) + Cz0 x_z(0) + Ct0 x_t(0)
			%		+ C1 x(1) + Cz1 x_z(1) + Ct1 x_t(1)
			%		+ sum_k^l C0k x(zk) + Czk x_z(zk) + Ctk x_t(zk)
			%		+ int_0^1 C(z) x(z) + Cz(z) x_z(z) + Ct(z) x_t(z) dz + B u
			% In the end, the boundary condition is approximated with the algeraic
			% equation
			%	x(z=bc) = bcOperatorAllDomain * x + weights * B * u(t)
			% where u are inputs and inputMatrix = weights * B.
			%
			%	[bcOperatorAllDomain, weights, inputMatrix] = useAsBoundaryOperator(obj, type, z)
			%		returns a lumped algebraic equation to obtain the boundary values x(z=bc, t)
			%		based on a lumped approximation of the distributed state x(z, t) such that
			%			x(z=bc) = bcOperatorAllDomain * x + inputMatrix * u(t).
			%		- weights: is a matrix that was used to solve for x(z=bc).
			%		- obj: model.Outputs object that specifies boundary conditions for z=0 and z=1
			%			in seperate obj.output elements.
			%		- type: specifies if the boundary condition should be solved for a position term
			%			x(bc,t), a velocity term x_t(bc, t) or if an approximation for a transport
			%			PDE is requested.
			%		- z: double array with elements 0 or 1. The i-th element of z indicates if
			%			obj.output(i) is defined at z=0 or z=1.
			%
			%	Further name-value pair options:
			%		- grid: specify the spatial grid which is used for the spatial discretization
			%			(default: obj.grid)
			%		- withTimeDerivative: true/false to specify if velocity dynamics should be
			%			considered or not (model.Transport: false, model.Wave: true)
			%			(default: obj.withTimeDerivative)
			%		- input: allows to specify further inputs as model.Input(s)
			%		- method: use finiteDifferences ("finiteDifferences ") or chebfun ("cheb") for
			%			the approximation of integrals and derivatives.
			%			(default: "finiteDifferences")
			%		- finiteDifferencesStencilLength: if finite differences are used, this specifies
			%			the size of the stencil for the approximation.
			%		- lambdaPositive: if method = "transport" is used (which is usually the case for
			%			model.Transport), then lambdaPositive is used to indicate for which states
			%			x_i(z, t), i = 1, ..., n, of x(z, t) = [x_1(z, t), ..., x_n(z, t)] the
			%			boundary condition has to be implemented at the left or right boundary.
			%			For lambdaPositive(i) = 1, there is a boundary condition for x_i(1, t) and
			%			for lambdaPositive(i) = 0, the boundary condition is x_i(0, t).
			%			lambdaPositive is supposed to be a double array with elements 0 and 1.
			%
			% See also model.Outputs.setApproximation, model.Outputs.useAsFeedback.
			
			% read inputs
			myParser = misc.Parser();
			myParser.addRequired('type', @(v) isstring(v) & (numel(v) == obj.numTypes) ...
				& all(arrayfun(@(vI) any(strcmp(["position", "velocity", "transport"], vI)), v)));
			myParser.addRequired('z', @(v) (numel(v) == obj.numTypes) ...
				& all(arrayfun(@(vI) (vI==0) | (vI==1), v)));
			myParser.addParameter('grid', obj.grid);
			myParser.addParameter('withTimeDerivative', obj.withTimeDerivative);
			myParser.addParameter('input', [], @(v) isa(v, 'model.Input') | isa(v, 'model.Inputs'));
			myParser.addParameter('method', "finiteDifferences");
			myParser.addParameter('finiteDifferencesStencilLength', 2);
			myParser.addParameter('lambdaPositive', []);
			myParser.parse(type, z, varargin{:});
			
			if any(strcmp(myParser.Results.type, "transport"))
				assert(~isempty(myParser.Results.lambdaPositive), ...
					"If type = 'transport', then lambdaPositive must be specified");
			end
			
			% split into left boundary conditions bc0 and right boundary conditions bc1
			bc0 = obj.output(z==0);
			bc1 = obj.output(z==1);
			
			% set approximation of model.Outputs
			obj = obj.setApproximation(myParser.Results.grid, ...
				'withTimeDerivative', myParser.Results.withTimeDerivative, ...
				'stencilLength', myParser.Results.finiteDifferencesStencilLength, ...
				'method', myParser.Results.method);
			bcOperatorAllDomain = obj.Cop;
			
			% boundarySelector selects the values of the double array
			% bcOperatorAllDomain that represent the values at the boundary of the
			% boundary conditions
			boundarySelector = false(size(bcOperatorAllDomain));
			for kt = 1 : obj.numTypes
				for it = 1 : obj.output(kt).lengthState
					if myParser.Results.z(kt) == 0
						if strcmp(myParser.Results.type(kt), "position")
							boundarySelector(:, 1+sum(obj.gridLength(1:(it-1)))) = true;
						elseif strcmp(myParser.Results.type(kt), 'velocity')
							boundarySelector(:, ...
								sum(obj.gridLength) + 1 + sum(obj.gridLength(1:(it-1)))) = true;
						elseif strcmp(myParser.Results.type(kt), "transport")
							if myParser.Results.lambdaPositive(it) == 0
								boundarySelector(:, 1+sum(obj.gridLength(1:(it-1)))) = true;
							end
						end
					elseif myParser.Results.z(kt) == 1
						if strcmp(myParser.Results.type(kt), "position")
							boundarySelector(:, sum(obj.gridLength(1:it))) = true;
						elseif strcmp(myParser.Results.type(kt), "velocity")
							boundarySelector(:, ...
								sum(obj.gridLength) + sum(obj.gridLength(1:it))) = true;
						elseif strcmp(myParser.Results.type(kt), "transport")
							if myParser.Results.lambdaPositive(it) == 1
								boundarySelector(:, sum(obj.gridLength(1:it))) = true;
							end
						end
					end
				end
			end
			
			invWeights = -reshape(bcOperatorAllDomain(boundarySelector), ...
				[obj.lengthOutput, obj.lengthOutput]);
			assert(abs(det(invWeights)) > 1e-14, "solving for boundary value is not possible")
			weights = inv(invWeights);
			assert(abs(det(weights)) > 1e-14, "solving for boundary value is not possible: "...
				+ num2str(abs(det(weights))))
			
			bcOperatorAllDomain = (invWeights \ bcOperatorAllDomain);
			assert(misc.iseye(round(-reshape(bcOperatorAllDomain(boundarySelector), ...
				[size(boundarySelector, 1), size(boundarySelector, 1)]), 8)), ...
				"There is some mistake in the implementation of the boundary conditions")
			bcOperatorAllDomain(boundarySelector) = 0;
			
			% Consider boundary inputs
			myInput = myParser.Results.input;
			if ~isempty(myInput)
				if ~isempty(myInput.B0)
					B0 = myInput.B0;
				else
					B0 = zeros(bc0.lengthOutput, sum(myInput.length));
				end
				if ~isempty(myInput.B1)
					B1 = myInput.B1;
				else
					B1 = zeros(bc1.lengthOutput, sum(myInput.length));
				end
				inputMatrix = invWeights \ [B0; B1];
			else
				inputMatrix = zeros(size(bcOperatorAllDomain, 1), 0);
			end % if ~isempty(myInput)
		end % useAsBoundaryOperator()
		
		%% Combining objects:
		function obj = add(obj, varargin)
		% add further output(s) to Outputs obj
			for it = 1 : numel(varargin)
				if ~isempty(varargin{it})
					if isa(varargin{it}, 'model.Output')
						assert(~any(strcmp(varargin{it}.type, obj.type)), ...
							"can not add output b if same type already exists in output a");
						obj.output(end+1) = varargin{it};
					elseif isa(varargin{it}, 'model.Outputs')
						additionalOutputs = varargin{it};
						for jt = 1 : additionalOutputs.numTypes
							assert(~any(strcmp(additionalOutputs.type(jt), obj.type)), ...
								"can not add output b if same type already exists in output a");
							obj.output(end+1) = additionalOutputs.output(jt);
						end % jt = 1 : additionalOutputs.numTypes
					elseif isa(varargin{it}, 'misc.Gain')
						for jt = 1 : numel(varargin{it}.outputType)
							tempGain = varargin{it}.gainOfOutput(varargin{it}.outputType(jt));
							currentIdx = obj.index(tempGain.outputType{it});
							if isempty(currentIdx)
								obj = obj.add(model.Output(tempGain.outputType, ...
									"C0", zeros([tempGain.lengthOutput, obj.lengthState]), ...
									"C1", zeros([tempGain.lengthOutput, obj.lengthState]), ...
									"D", tempGain));
							else
								obj.output(currentIdx).input.add(tempGain);
							end
						end
					elseif isa(varargin{it}, 'misc.Gains')
						for jt = 1 : varargin{1}.numTypes
							obj = obj.add(varargin{1}.gain(jt));
						end
					else
						error("unknown data type to be added");
					end
				end
			end
			assert(misc.isunique(obj.type), "output types must be unique");
			obj.verifySizes();
		end
		
		function c = plus(a, b)
		% add output or combine outputs with simple + notation
			c = a.add(b);
		end %plus()
		
		function obj = exchange(obj, newOutput)
		% exchange output of Outputs obj
			obj.output(obj.index(newOutput.type)) = newOutput;
		end % exchange()
		
		function obj = extendType(obj, position, newText)
			% extendType adds a pre- or a postfix to all obj.type
			arguments
				obj;
				position (1, 1) string;
				newText (1, 1) string;
			end
			
			for it = 1 : obj.numTypes
				obj.output(it).extendType(position, newText);
			end
		end % extendType()
		
		function obj = strrepType(obj, oldText, newText, NameValue)
			% replace strings in type
			arguments
				obj;
				oldText (1, 1) string;
				newText (1, 1) string;
				NameValue.startOnly (1, 1) logical = false;
			end
			
			for it = 1 : obj.numTypes
				obj.output(it).strrepType(oldText, newText, "startOnly", NameValue.startOnly);
			end
		end % strrepType()
		
		function obj = remove(obj, type)
			% remove output(s) of Outputs obj
			for it = 1 : numel(type)
				obj.output = obj.output((1:1:obj.numTypes) ~= obj.index(type(it)));
			end
		end % remove()
		
		function newOutput = blkdiag(a, b, varargin)
			% blkdiag combines two model.Outputs objects by combining them block-diagonally.
			% if outputA = CA * xA + DA * uA and outputB = CB * xB + DB * uB
			% then newOutput = blkdiag(CA, CB) [xA; xB] + blkdiag(DA, DB) * [uA; uB].
			
			assert(isequal(a.type, b.type), "blkdiag is only defined for model.Outputs with same type");
			
			newOutput = model.Outputs();
			for it = 1 : a.numTypes
				newOutput = newOutput.add(blkdiag(a.output(it), b.output(it)));
			end % for it()
			
			if nargin > 2
				newOutput = blkdiag(newOutput, varargin{:});
			end
		end % blkdiag()
		
		%% Transformations:
		function newOutputs = hopfCole(obj, T, T_z)
			% for description see model.Output.hopfCole()
			arguments
				obj;
				T quantity.Discrete;
				T_z = [];
			end
			if isempty(T_z)
				if isa(T, "quantity.Symbolic")
					T_z = T.diff("z", 1);
				else
					T_z = T.diffFd("z", 1);
				end
			end
			newOutputs = model.Outputs();
			for it = 1 : obj.numTypes
				newOutputs = newOutputs + hopfCole(obj.output(it), T, T_z);
			end
		end % hopfCole()
		
		function newOutputs = integralTransformation(obj, varargin)
			% for description see model.Output.integralTransformation()
			newOutputs = model.Outputs();
			for it = 1 : obj.numTypes
				newOutputs = newOutputs + integralTransformation(obj.output(it), varargin{:});
			end
		end % integralTransformation()
		
		function newOutputs = integrationByPartsCz(obj, varargin)
			% for description see model.Output.integrationByPartsCz()
			newOutputs = model.Outputs();
			for it = 1 : obj.numTypes
				newOutputs = newOutputs + integrationByPartsCz(obj.output(it), varargin{:});
			end
		end % integrationByPartsCz()
		
		function outputEnergy = transform2energyCoordinates(outputOriginal)
		% This method transforms the output object that is defined for
		% the state v such that it suites the energy state x, which are given by
		%	x = [x_1; x_2] = [v_z; v_t].
		% Note that, v(z) = v(0) + int_0^z x_1(zeta) dzeta and v_t = x_2. So far, it
		% is assumed that v(0) = 0.
		% Hence, this can be implemented by three steps:
		%	1. The influence of x_1 on the output can be implemented by an integral
		%		transformation, that is already implemented as a method of the model.Output
		%		class. Note that the gains for states time-derivatives (i.e. x_1_t)
		%		of the transformed output-object need to be deleted, since these will be
		%		considered in the next step.
		%	2. The influence of x_2 on the output is described by the gaines for the time
		%		derivatives in the original output for v_t, i.e. C0t, C1t, Ckt, Ct.
		%	3. Finally, the gains resulting from steps 1. and 2. are horizontally
		%		concatenated to obtain an output-object for x = [x_1; x_2].

			outputOld = copy(outputOriginal);
			% Step 1: integral transformation
			syms z zeta
			I1Const = eye(outputOld.lengthState);
			myGrid = outputOld.C(1).domain(1).grid;
			I1 = quantity.Symbolic(I1Const + z*zeta*0*I1Const, ...
				[quantity.Domain('z', myGrid), quantity.Domain('zeta', myGrid)]);
			outputX1 = outputOld.integralTransformation(I1, {0, 'z'});

			% Step 2 and 3: consider x_2 and vertcat
			outputEnergy = model.Outputs();
			for it = 1 : outputOld.numTypes
				tempParameter.input = copy(outputOld.output(it).input);
				if outputOld.output(it).k > 0 && ~outputOld.isMatrixZeroOrEmpty('Ctk')
					tempParameter.zk = outputOld.output(it).zk;
					tempParameter.Ck = horzcat(...
							0*outputOld.output(it).Ctk, outputOld.output(it).Ctk);
					% note that there are none pointwise indomain outputs of outputX1,
					% because they are eliminated in the integralTransformation.
				end
				tempParameter.C0 = horzcat(outputX1.output(it).C0, ...
					outputOld.output(it).Ct0);
				tempParameter.C1 = horzcat(outputX1.output(it).C1, ...
					outputOld.output(it).Ct1);
				tempParameter.C = horzcat(outputX1.output(it).C, ...
					outputOld.output(it).Ct);
				tempParameter.Cz0 = horzcat(outputX1.output(it).Cz0, ...
					0 * outputX1.output(it).Cz0);
				tempParameter.Cz1 = horzcat(outputX1.output(it).Cz1, ...
					0 * outputX1.output(it).Cz1);
				tempParameter.Cz = horzcat(outputX1.output(it).Cz, ...
					0 * outputX1.output(it).Cz);

				tempParameterNameValues = misc.struct2namevaluepair(tempParameter);
				outputEnergy = outputEnergy + model.Output(...
					outputOld.output(it).type, ...
					tempParameterNameValues{:});
			end % for it = 1 : outputOld.numTypes
		end % transform2energyCoordinates()
		
		function outEnergy = transform2energy(obj)
		% See description in model.Output.transform2energy.
			outEnergy = model.Outputs();
			for it = 1 : obj.numTypes
				outEnergy = outEnergy.add(obj.output(it).transform2energy());
			end % it = 1 : obj.numTypes
		end % transform2energy()
		
		%% backstepping() for every output in Outputs
		function newOutputs = backstepping(obj, varargin)
			% for description see model.Output -> backstepping()
			newOutputs = model.Outputs();
			for it = 1 : obj.numTypes
				newOutputs = newOutputs + backstepping(obj.output(it), varargin{:});
			end
		end % backstepping()
		
		function newOutputs = decouple(obj, varargin)
			% for description see model.Output -> decouple()
			newOutputs = model.Outputs();
			for it = 1 : obj.numTypes
				newOutputs = newOutputs.add(decouple(obj.output(it), varargin{:}));
			end
		end % decouple()
		
		%% Methods for getting output-properties:
		function idx = index(obj, type, ignorePrefix)
		% find output by naming its type
			
			arguments
				obj;
				type string;
				ignorePrefix = false;
			end
			
			idx = [];
			allTypes = obj.type;
			if ~ignorePrefix
				for it = 1 : numel(allTypes)
					if strcmp(allTypes(it), type)
						idx = it;
						return;
					end
				end
			elseif ignorePrefix
				for it = 1 : numel(allTypes)
					thisType = split(allTypes(it), ".");
					if strcmp(thisType(end), type)
						idx = it;
						return;
					end
				end
			end
		end % index()
		
		function thisOutput = get(obj, type, ignorePrefix)
		% get finds the output that matches obj.type
		%
		%	thisOutput = get(obj, type) returns the output that corresponds to strcmp(obj.type,type)
		%
		%	thisOutput = get(obj, type, ignorePrefix) ignores if obj.type multiple levels separated
		%		by dots, for instance if ignorePrefix, then obj.type = "levelA.levelB.myOutput"
		%		would be a match for the type input parameter = "myOutput.
			arguments
				obj model.Outputs;
				type (1, 1) string;
				ignorePrefix  (1, 1) logical = false;
			end
			idxMax = numel(obj);
			% prealocation
			idxTmp = obj(idxMax).index(type, ignorePrefix);
			if isempty(idxTmp)
				thisOutput(idxMax) = model.Output();
			else
				thisOutput(idxMax) = obj(idxMax).output(idxTmp);
			end
			for it = 1 : (idxMax-1)
				idxTmp = obj(it).index(type, ignorePrefix);
				if isempty(idxTmp)
					thisOutput(it) = model.Output();
				else
					thisOutput(it) = obj(it).output(idxTmp);
				end
			end
			thisOutput = reshape(thisOutput, size(obj));
		end % get()
		
		% create name-value pair of outputs
		function nameValuePairs = nameValuePairs(obj)
			nameValuePairs = cell(1, 2*obj.numTypes);
			for it = 1 : obj.numTypes
				nameValuePairs{2*it-1} = obj.output(it).type + "Output";
				nameValuePairs{2*it} = obj.output(it);
			end
		end % nameValuePairs()
		
		%% get meta data about outputs
		function numTypes = get.numTypes(obj)
			numTypes = numel(obj.output);
		end % get.numTypes()
		
		function type = get.type(obj)
			type = vertcat(obj.output(:).type);
		end % get.type()
			
		function lengthOutput = get.lengthOutput(obj)
			lengthOutput = sum([obj.output(:).lengthOutput]);
		end % get.lengthOutput()
		
		function lengthOutput = lengthOutputOf(obj, type)
			lengthOutput = obj.lengthOutput(obj.index(type));
			if isempty(lengthOutput)
				lengthOutput = 0;
			end
		end % lengthOutputOf()
		
		function lengthState = get.lengthState(obj)
			lengthState = 0;
			for it = 1 : obj.numTypes
				if it == 1
					lengthState = obj.output(it).lengthState;
				elseif (obj.output(it).lengthState > 0) && lengthState ~= obj.output(it).lengthState
					error("outputs have different lengthState values");
				end
			end
		end % get.lengthState()
		
		function D = get.D(obj)
			D = obj.input.value;
			D = zeros(obj.lengthOutput, size(D, 2));
			for inputIdx = 1 : obj.input.numTypes
				tempInput = obj.input.gain(inputIdx);
				if inputIdx > 1
					inputColumns = sum(obj.input.gain(1 : inputIdx-1).lengthInput);
				else
					inputColumns = 0;
				end 
				inputColumns = inputColumns + (1 : tempInput.lengthInput);
				for outputIdx = 1 : obj.numTypes
					outputLines = sum([obj.output(1:(outputIdx-1)).lengthOutput]) ...
						+ (1 : obj.output(outputIdx).lengthOutput);
					if any(contains(tempInput.outputType, obj.type(outputIdx)))
						D(outputLines, inputColumns) = tempInput.valueOfOutput(obj.type(outputIdx));
					end
				end
			end
		end % get.D()
		
		function input = get.input(obj)
			% get input for all outputs combined. The resulting input is
			% sorted according to obj.inputTypes
			input = misc.Gains();
% 			for it = 1 : numel(obj.inputTypes)
% 				% ensures that D is defined for all outputs.
% 				input = input + misc.Gain(obj.inputTypes{it}, ...
% 					zeros(obj.lengthOutput, obj.lengthInputOf(obj.inputTypes{it})), ...
% 					"outputType", obj.type, "lengthOutput", [obj.output.lengthOutput]);
% 			end
			for it =  1 : numel(obj.inputTypes)
				input = input + obj.inputOf(obj.inputTypes{it});
			end
		end % get.input()
		
		function thisInput = inputOf(obj, inputType)
			% get the input suiting for all the outputs resulting from
			% outputs that is specified by the string inputType
			thisInput = misc.Gains();
			for it = 1 : obj.numTypes
				thisGain = obj.output(it).inputOf(inputType);
				if ~isempty(thisGain)
					thisInput.add(thisGain);
				end
			end
		end % inputOf()
		
		function thisLengthInput = lengthInputOf(obj, inputType)
			% get the length of the inputs of the type specified by the
			% string inputType
			thisLengthInput = 0;
			for it = 1 : obj.numTypes
				if thisLengthInput == 0
					if obj.output(it).input.lengthOf(inputType) > 0
						thisLengthInput = obj.output(it).input.lengthOf(inputType);
					end
				elseif obj.output(it).input.lengthOf(inputType) > 0
					assert(isequal(thisLengthInput, obj.output(it).input.lengthOf(inputType)))
				end
			end
		end % lengthInputOf()
		
		function lengthInput = get.lengthInput(obj)
			lengthInput = size(obj.D, 2);
		end % get.lengthInput()
		
		function inputTypes = get.inputTypes(obj)
			inputTypes = unique(vertcat(obj.output.inputTypes), 'stable');
		end % get.inputTypes
		
		function k = get.k(obj)
			k = 0*(1:obj.numTypes).';
			for it = 1 : obj.numTypes
				k(it) = obj.output(it).k;
			end
		end % get.k()
		
		
		%% getter
		% get matrices
		function C0 = get.C0(obj)
			C0 = getOutputMatrixHelper(obj, 'C0');
		end
		function Cz0 = get.Cz0(obj)
			Cz0 = getOutputMatrixHelper(obj, 'Cz0');
		end
		function Ct0 = get.Ct0(obj)
			Ct0 = getOutputMatrixHelper(obj, 'Ct0');
		end
		function C1 = get.C1(obj)
			C1 = getOutputMatrixHelper(obj, 'C1');
		end
		function Cz1 = get.Cz1(obj)
			Cz1 = getOutputMatrixHelper(obj, 'Cz1');
		end
		function Ct1 = get.Ct1(obj)
			Ct1 = getOutputMatrixHelper(obj, 'Ct1');
		end
		function C = get.C(obj)
			C = getOutputMatrixHelper(obj, 'C');
		end
		function Cz = get.Cz(obj)
			Cz = getOutputMatrixHelper(obj, 'Cz');
		end
		function Ct = get.Ct(obj)
			Ct = getOutputMatrixHelper(obj, 'Ct');
		end
		function zk = get.zk(obj)
			zk = [];
			for it = 1 : obj.numTypes
				zk = [zk; obj.output(it).zk];
			end
		end
		function Ck = get.Ck(obj)
			Ck = getOutputMatrixHelperZk(obj, 'Ck');
		end
		function Czk = get.Czk(obj)
			Czk = getOutputMatrixHelperZk(obj, 'Czk');
		end
		function Ctk = get.Ctk(obj)
			Ctk = getOutputMatrixHelperZk(obj, 'Ctk');
		end
		function Cop = get.Cop(obj)
			Cop = vertcat(obj.output.Cop);
		end
		function grid = get.grid(obj)
			grid = obj.output(1).grid;
		end
		function gridLength = get.gridLength(obj)
			if isempty(obj.grid)
				gridLength = 0;
			else
				gridLength = cellfun(@(v) numel(v), obj.grid);
			end
		end
		
		function withTimeDerivative = get.withTimeDerivative(obj)
			withTimeDerivative = ~all(obj.isMatrixZeroOrEmpty('Ct', 'Ct0', 'Ct1', 'Ctk'));
		end % get.withTimeDerivative()
		
		%% test dimensions of all outputs
		function verifySizes(obj)
			for it = 1 : obj.numTypes
				obj.output(it).verifySizes();
				assert(obj.lengthState == obj.output(it).lengthState, ...
					"all model.Output-objects must suite the same state, but " ...
					+ obj.output(it).type + " does not fit to the others.");
				assert(obj.k(it) == obj.output(it).k, ...
					"all model.Output-objects must have the same number of pointwise-indomain " ...
					+ "evaluation points k, but " + obj.output(it).type ...
					+ " does not fit to the others.");
			end
		end % verifySizes()
		
		function flag = isMatrixZeroOrEmpty(obj, varargin)
			flag = false(numel(varargin), 1);
			for it = 1 : numel(varargin)
				testProperty = varargin{it};
				if isa(obj.(testProperty), 'quantity.Discrete')
					value = obj.(testProperty).on();
				else
					value = obj.(testProperty);
				end
				flag(it) = isempty(value) || (mean(abs(value(:))) == 0);
			end
		end % isMatrixZeroOrEmpty()
		
		function [texString, nameValuePairs] = printParameter(obj)
			% Create TeX-code for all non-zero parameter.
			propList = {'C0', 'Cz0', 'Ct0', 'C1', 'Cz1', 'Ct1', ...
				'C', 'Cz', 'Ct', 'D', 'Ck', 'Czk', 'Ctk'};
			nameList = {'C_0', 'C_{z,0}', 'C_{t,0}', 'C_1', 'C_{z,1}', 'C_{t,1}', ...
				'C', 'C_z', 'C_t', 'D', 'C_k', 'C_{z,k}', 'C_{t,k}'};
			nameValuePairs = {};
			for it = 1 : numel(propList)
				if ~obj.isMatrixZeroOrEmpty(propList{it})
					nameValuePairs{end+1} = nameList{it};
					nameValuePairs{end+1} = obj.(propList{it});
				end
			end % for it = 1 : numel(propList)
			if numel(obj.zk) > 0
				nameValuePairs{end+1} = 'z_k';
				nameValuePairs{end+1} = obj.zk;
			end
			texString = misc.variables2tex([], nameValuePairs);
		end % printParameter()
		
		function texString = print(obj, varargin)
			% print Print equation to command window as latex compatible string-array
			% uses implementation of model.Output
			myString = strings(obj.numTypes, 1);
			for it = 1 : obj.numTypes
				myString(it) = obj.output(it).print(varargin{:});
			end
			
			if nargout > 0
				texString = myString;
			else
				misc.printTex(myString);
			end
		end % print()
		
		function obj = discretizeQuantity(obj)
			% discretizeQuantity converts all properties containg quantity-objects to
			% quantity.Discrete objects. This is often speeds up calculations.
			for it = 1 : obj.numTypes
				obj.output(it) = obj.output(it).discretizeQuantity();
			end
		end % discretizeQuantity()
		
	end % methods
	
	methods (Hidden = true)
		function outputMatrix = getOutputMatrixHelper(obj, propertyName)
			% D = [output(1).D; output(2).D; ... ]
			thisColumns = getColumnsHelper(obj, propertyName);
			if thisColumns > 0
				thisMatrixCell = cell(obj.numTypes, 1);
				for it = 1 : obj.numTypes
					if ~isempty(obj.output(it).(propertyName))
						thisMatrixCell{it} = obj.output(it).(propertyName);
					else
						thisMatrixCell{it} = zeros([obj.output(it).lengthOutput, thisColumns]);
					end
				end
				outputMatrix = vertcat(thisMatrixCell{:});
			else
				outputMatrix = [];
			end
		end % getOutputMatrixHelper()
		
		function outputMatrix = getOutputMatrixHelperZk(obj, propertyName)
			% D = [output(1).D; output(2).D; ... ]
			thisColumns = getColumnsHelper(obj, propertyName);
			if thisColumns > 0
				thisMatrixCell = cell(obj.numTypes, 1);
				numPreviousZk = 0;
				for it = 1 : obj.numTypes
					thisMatrixCell{it} = zeros([obj.output(it).lengthOutput, thisColumns, numel(obj.zk)]);
					if ~isempty(obj.output(it).(propertyName))
						thisMatrixCell{it}(:, :, numPreviousZk+(1:numel(obj.output(it).zk)))...
							= obj.output(it).(propertyName);
					end
					numPreviousZk = numPreviousZk + numel(obj.output(it).zk);
				end
				outputMatrix = vertcat(thisMatrixCell{:});
			else
				outputMatrix = [];
			end
		end % getOutputMatrixHelper()
		
		function columnsC = getColumnsHelper(obj, propertyName)
			columnsC = 0;
			for it = 1 : obj.numTypes
				tempSizeC = size(obj.output(it).(propertyName), 2);
				if tempSizeC > 0
					if columnsC == 0
						columnsC = tempSizeC;
					else
						assert(isequal(columnsC, tempSizeC), ...
							'number of output matrix columns must coincide!');
					end
				end
			end
		end % getColumnsHelper()
	end % methods
	
	methods (Access = protected)
		% Override copyElement method:
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@matlab.mixin.Copyable(obj);
			cpObj.output = copy(obj.output);
		end
	end % methods (Access = protected)
	
end
