classdef WaveOde < model.Wave & handle & matlab.mixin.Copyable
	% model.WaveOde represents n coupled linear wave equations that are additionally coupled with an
	% ODE.
	% This implements wave systems of the form
	%
	%	v_tt(z,t) = Gamma(z) v_zz(z,t) + D(z) v_t(z,t) + G(z) v_z(z,t) - G0(z) v_z(0,t) ...
	%				 S(z) v(z,t) + int_at^bt Ft(z,zeta) v_t(zeta,t) dzeta ...
	%				 + int_az^bz Fz(z,zeta) v_z(zeta,t) dzeta + int_a^b F(z,zeta) v(zeta,t) dzeta
	%				 + input.B(z) u(t) + ode2pde.B(z) w(t)
	%			0 = bc0(v, v_t) + input.B0 u(t) + ode2pde.B0 w(t)
	%			0 = bc1(v, v_t) + input.B1 u(t) + ode2pde.B1 w(t)
	%	   w_t(t) = ode.A w(t) + ode.B pde2ode[v, v_t] + odeInput u(t) 
	%		 y(t) = output[v(z,t), v_t(z,t), w(t)] + input.D u(t)
	%
	% and the initial conditions v(z,0) = v_0(z), v_t(z,0) = v_t,0(z), w(0) = w0.
	% The distributed states are v(z, t), v_t(z, t) \in R^n, with the spatial variable z in [0, 1] 
	% and the temporal variable t.
	%
	% The lumped ODE-state is w.
	% The boundary operators bc0 and bc1 allow to define general Dirichlet, Neumann, velocity and
	% integral terms, see model.Output. The vector u(t) contains all input signals, for instance 
	% control input,  disturbances or faults. y(t) is a vector of output signals, for instance the
	% control output or measurements. Outputs can define general outputs
	%
	%	output[h, h_t] = sum_{i=1}^{l} [F_i * h(z_i)] + int_0^1 [C(z) h(z) + Cz(z) h_z(z)] dz ...
	%					 + sum_{i=1}^{lt} [Ft_i * h_t(z_i)] + int_0^1 [Ct(z) h(z)] dz + D w
	%
	% see model.Outputs.
	% The bounds of the integral in the PDE are defined by FBounds = {a, b}, FtBounds = {at, bt}, 
	% FzBounds = {az, bz} wherein at a, b, at, bt, az, bz can be spatial values in [0, 1] or a 
	% string "z".
	%
	% The coupling from the ODE to the PDE is implemented with the ode2pde model.Input-object, which is
	% part of the input property. Hence, it can define coupling at the boundaries and indomain,
	% see model.Input.
	% Additional inputs at the ODE, for instance control input, disturbances, faults, ... are defined
	% via the odeInput-property, which is a misc.Gains-object, see misc.Gain.
	%
	% See also model.WaveOde.WaveOde (constructor), model.Dps, model.Wave, model.Transport and
	% model.TransportOde.
	
	properties (SetAccess = protected)
		ode ss;					% ODE-subsystem defined as a state-space model
		odeInput misc.Gains;	% input of ODE-subsystem.
	end % properties (SetAccess = protected)
	
	properties (Dependent = true)
		% Coupling from the PDE to the ODE as model.Output-object, which is part of the output 
		% property. Hence, it can define boundary and indomain coupling, see model.Output.
		pde2ode (1,1) model.Output;
		% Coupling from the ODE to the PDE as a ode2pde model.Input-object, which is part of the
		% input property. Hence, it can define coupling at the boundaries and indomain,
		% see model.Input.
		ode2pde (1,1) model.Input;
	end % properties (Dependent = true)
	
	methods
		function obj = WaveOde(Gamma, varargin)
			% model.WaveOde.WaveOde constructs a model.WaveOde object, which represents n coupled 
			% linear wave equations that are additionally coupled with an ODE.
			%
			%	See the parent class constructor model.Wave.Wave on how to define 
			%		PDE-parameter, boundary conditions, name, prefix, input and output.
			%
			%	obj = model.WaveOde(Lambda, [...], "ode", ode) creates a model.WaveOde
			%		object, with the ODE-subsystem described by the state space model ode.
			%
			%	model.WaveOde([...], "odeInput", odeInput) sets input matrices for external
			%		signals as disturbances as an misc.Gains object, see misc.Gain and misc.Gains.
			%
			%	model.WaveOde([...], "pde2ode", pde2ode) sets the output operator for coupling
			%		of the PDE to the ODE as a model.Output-object. Alternatively, pde2ode can be
			%		included in the output input-argument. The type of the pde2ode model.Output
			%		object must be "pde2ode" or prefix + ".pde2de".
			%
			%	model.WaveOde([...], "ode2pde", ode2pde) sets the input matrices for coupling
			%		of the ODE to the PDE as a model.Input-object. Alternatively, ode2pde can be
			%		included in the input input-argument. The type of the ode2pde model.Output
			%		object must be "ode2pde" or prefix + ".ode2pde".
			%
			% See also model.WaveOde (class description and equations), model.Dps, model.Wave.
			obj@model.Wave(Gamma, varargin{:});
			if nargin > 0
				% input parser
				myParser = misc.Parser();
				myParser.addParameter("ode", ss(), @(v) isa(v, "ss"));
				myParser.addParameter("odeInput", []);
				myParser.addParameter("pde2ode", model.Output(obj.prefix + ".pde2ode"), ...
					@(v) isa(v, "model.Output"));
				myParser.addParameter("ode2pde", model.Input(obj.prefix + ".ode2pde"), ...
					@(v) isa(v, "model.Input"));
				myParser.parse(varargin{:});
				obj.arglist = vertcat(obj.arglist, fieldnames(myParser.Results));
				
				% write from parser to object
				obj.ode = myParser.Results.ode;
				if isempty(myParser.Results.odeInput)
					obj.odeInput = misc.Gains();
				else
					obj.odeInput = myParser.Results.odeInput.copy();
				end
				
				if ~isempty(obj.pde2ode)
					% pde2ode is already part of obj.output
					if ~any(strcmp(myParser.UsingDefaults, "pde2ode"))
						error("pde2ode was already specified in output")
					end
				else
					obj.output = obj.output.add(myParser.Results.pde2ode.copy());
				end
				
				if any(contains(obj.input.type, "ode2pde"))
					% ode2pde is already part of obj.input, hence assure that it is
					% not defined multiple times. Furthermore, it has the wrong type,
					% so that is corrected.
					if ~any(strcmp(myParser.UsingDefaults, "ode2pde"))
						error("ode2pde was already specified in input")
					end
					ode2pdeIdx = contains(obj.input.type, "ode2pde");
					assert(sum(ode2pdeIdx)==1, "there must be only one ode2pde input");
					if strcmp(obj.input.input(ode2pdeIdx).type, "ode2pde")
						obj.input.input(ode2pdeIdx).setPrefix(obj.prefix);
					else
						assert(strcmp(obj.input.input(ode2pdeIdx).type, obj.prefix + ".ode2pde"), ...
							"ode2pde must have type 'ode2pde' or '[prefix].ode2pde'");
					end
					
				else
					assert(contains(myParser.Results.ode2pde.type, "ode2pde"), ...
						"ode2pde has wrong type property");
					if strcmp(myParser.Results.ode2pde.type, "ode2pde")
						myParser.Results.ode2pde.setPrefix(obj.prefix);
					else
						assert(strcmp(myParser.Results.ode2pde.type, obj.prefix + ".ode2pde"), ...
							"ode2pde must have type 'ode2pde' or '[prefix].ode2pde'");
					end
					obj.input = obj.input.add(myParser.Results.ode2pde.copy());
				end % if any(contains(obj.input.type, "ode2pde"))
				
				%% add prefix to output
				obj = obj.setOutputName();
				
				%% Check parameter
				% Check ode
				assert(misc.iseye(obj.ode.C), "Coupling between ode and wave is considered via" ...
					+ " pde2ode output and ode2pde input, hence the output matrix of " ...
					+ "the ode must be an identity matrix.");
				assert(misc.iseye(obj.ode.B), "Coupling between ode and wave is considered via " ...
					+ "pde2ode output and ode2pde input, hence the output matrix of " ...
					+ "the ode must be an identity matrix.");
				
				% Check pde2ode
				assert(~any(strcmp(obj.output.type, "pde2ode")), ...
					"Property type of pde2pde must be '[obj.prefix].pde2ode'");
				assert(isempty(obj.pde2ode.input), "input property of pde2ode must be empty");
				if (obj.pde2ode.lengthOutput ~= 0) || (obj.pde2ode.lengthState ~= 0)
					assert(obj.pde2ode.lengthOutput == size(obj.ode.B, 2), ...
						"lengthOutput of pde2ode must be equal to size(ode.B, 2)");
					assert(obj.pde2ode.lengthState == size(obj.(obj.nameCharacteristicMatrix), 1), ...
						"lengthState of pde2ode must be equal to number of states of pde");
				end % if ~isempty(obj.pde2ode)
				
				% Check ode2pde
				assert(isempty(obj.ode2pde.D), "D property of ode2pde must be empty");
				assert((obj.ode2pde.length == size(obj.ode.C, 1)) || (obj.ode2pde.length == 0), ...
					"length of ode2pde must be equal to size(ode.C, 1)");
				
				% Check odeInput
				if ~isempty(obj.odeInput)
					assert((numel(obj.odeInput.outputType) == 1) ...
						&& strcmp(obj.odeInput.outputType(1), obj.prefix + ".odeInput"), ...
						"Property outputType of odeInput must be '[obj.prefix].odeInput'");
					assert(size(obj.ode, 2) == obj.odeInput.lengthOutput, ...
						'odeInput must fit to ode-input-size');
				end
				
			end % nargin > 0
		end % WaveOde() Constructor
				
		function simData = simulatePostproduction(obj, simData)
			% simulatePostproduction ensures, that the simulation output struct simData contains a
			% signal that is named as the obj.ode2pde input, i.e. it copies
			% simData.(obj.stateName.ode) to simData.(obj.ode2pde.type).
			simData = simulatePostproduction@model.Wave(obj, simData);
			simData = misc.setfield(simData, obj.ode2pde.type, ...
						misc.getfield(simData, obj.stateName.ode));
		end % simulatePostproduction()
		
		function obj = setApproximation(obj, varargin)
			% setApproximation(obj) sets the property obj.approximation using
			% finite differences or chebfun methods of the wave dynamics. This method
			% relies on the PDE approximation in the parent class model.Wave. It
			% only adds the ODE dynamics.
			% See also model.Wave.setApproximation
			myParser = misc.Parser();
			myParser.addParameter("allowBcChange", false);
			myParser.parse(varargin{:});
			
			% get finite approximation of parent class
			if myParser.Results.allowBcChange ...
					&& all(obj.bc0.isMatrixZeroOrEmpty("Ct0", "Cz0", "Cz", "Ct", "Ct1", "Ct1", "Ctk", "Czk"))
				% apply time derivative to boundary condition at z = 0, since velocity
				% boundary conditions seem to be numerical benefitial
				objBc0_t = obj.bcTimeDerivative("bc0");
				objBc0_t = setApproximation@model.Wave(objBc0_t , varargin{:});
				obj.grid = objBc0_t.grid; 
				obj.domain2odePosition = objBc0_t.domain2odePosition;
				obj.domain2odeVelocity = objBc0_t.domain2odeVelocity;
				pdeSs = objBc0_t.approximation;
			else
				obj = setApproximation@model.Wave(obj , varargin{:});
				pdeSs = obj.approximation;
			end
			obj.approximation = obj.combineSsPdeOde(pdeSs);
		end % setApproximation()
		
		function myStateSpace = combineSsPdeOde(obj, pdeSs)
			% connect the ss model of the open loop pde system and the ode.
			
			% The state space pdeSs already contains the approximation of the output 
			% operator obj.pde2ode. To connect this to the ode, its OutputName is
			% changed.
			pdeSs = stateSpace.changeSignalName(pdeSs, obj.ode2pde.type, obj.stateName.ode);
			% get ODE dynamics (already a state space)
			odeSs = obj.ode;
			odeSs.OutputName = obj.stateName.ode;
			% The input of the ODE needs to be connected to the PDE and additonal
			% inputs defined by obj.odeInput. Hence, a sum-block is created to
			% implement: odeInputInternal = pde2ode + odeInput.
			odeSs = stateSpace.setSignalName(odeSs, "input",  ...
				obj.prefix + ".odeInputInternal", size(odeSs, 2));
			if (numel(obj.odeInput)>0) && (obj.odeInput.numTypes > 0)
				odeSum = sumblk(char(...
					obj.prefix + ".odeInputInternal = " + obj.prefix + ".pde2ode + " ...
					+ string(obj.odeInput.outputType2sumblk())), size(odeSs, 2));
				odeInputGainSs = obj.odeInput.gain2ss();
			else
				odeSum = sumblk(char(...
					obj.prefix + ".odeInputInternal = " + obj.prefix + ".pde2ode"), size(odeSs, 2));
				odeInputGainSs = ss();
			end
			
			% Finally, pdeSs and odeSs are connected. First, InputName and OutputName
			% are concatenated, then they are connected.
			approximationOutput = [pdeSs.OutputName; odeSs.OutputName];
			
			approximationInput = [pdeSs.InputName(...
				~strcmp(stateSpace.removeEnumeration(pdeSs.InputName), obj.stateName.ode)); ...
				odeInputGainSs.InputName];
			approximationInput = setdiff(approximationInput, approximationOutput);
			
			
			myStateSpace = stateSpace.connect(...
				approximationInput, approximationOutput, ...
				pdeSs, odeSs, odeInputGainSs, odeSum);
		end % combineSsPdeOde()
		
		function [texString, nameValuePairs] = printParameter(obj, varargin)
			[~, nameValuePairsWave] = printParameter@model.Wave(obj);
			nameValuePairs = {
				'C', obj.ode2pde.B0, ...
				'F', obj.ode.A, ...
				'B', obj.pde2ode.Cz0, ...
				};
			texString = misc.variables2tex([], nameValuePairsWave{:}, nameValuePairs{:}, varargin{:});
		end % printParameter()
		
		function texString = print(obj, stateNamePde, stateNameOde, doPrintOutput)
			% print Print equation to command window as latex compatible string-array
			
			arguments
				obj;
				stateNamePde = strrep(obj.stateName.pde(1), obj.prefix + ".", "");
				stateNameOde = strrep(obj.stateName.ode, obj.prefix + ".", "");
				doPrintOutput = true;
			end % arguments
			
			% call parent class to get PDE + BCs, but not outputs.
			myStringArray = print@model.Wave(obj, stateNamePde, stateNameOde, false);
			
			% add ODE
			myOdeString = "\dot{" + stateNameOde + "}(t) &= " ...
				+ misc.latexChar(obj.ode.A) + stateNameOde + "(t)";
			pde2odeString = obj.pde2ode.print("none", stateNamePde);
			odeInputString = obj.odeInput.print(obj.odeInput.outputType);
			if ~isempty(pde2odeString) && ~strcmp(pde2odeString, "")
				myOdeString = myOdeString + " + " + pde2odeString;
			end
			if ~isempty(odeInputString) && ~strcmp(odeInputString, "")
				myOdeString = myOdeString + " + " + odeInputString;
			end
			myStringArray = [myStringArray; myOdeString];
			
			% add output
			if doPrintOutput
				myInput = obj.input.copy.remove(obj.ode2pde.type);
				outputWithoutPde2ode = obj.output.copy.remove(obj.pde2ode.type);
				outputWithoutPde2ode = outputWithoutPde2ode.add(myInput.D);
				myStringArray = [myStringArray; outputWithoutPde2ode.print()];
			end
			
			% set output parameter or print
			if nargout > 0
				texString = myStringArray;
			else
				misc.printTex(myStringArray);
			end
		end % print()
		
		function [ic, stateName] = getInitialCondition(obj, varargin)
			% getInitialCondition reads varargin if there are initial conditions
			% defined as name-value-pair with the name according to obj.stateName and
			% returns 2 cell arrays: 
			%	1. ic is a cell array of the values of the states initial condition
			%	2. stateName is a cell array of the names of those states.
			
			icStruct = misc.struct(varargin{:});
			
			% Create initial condition for all states specified by obj.stateName
			% If a value is specified in icStruct, than this value is used.
			% Otherwise, a default value is considered.
			[ic, stateName] = getInitialCondition@model.Wave(obj, icStruct);
			if misc.isfield(icStruct, obj.stateName.ode)
				w0 = misc.getfield(icStruct, obj.stateName.ode);
			else
				w0 = zeros(size(obj.ode.A, 1), 1);
			end
			assert(numel(w0) == size(obj.ode.A, 1), ...
				"vector of ODE initial condition has wrong length");
			
			% Create output parameter
			ic = [ic(:); w0];
			stateName = [stateName(:); obj.stateName.ode];
		end % getInitialCondition()
		
	
		function obj = setOutputName(obj)
			setOutputName@model.Wave(obj);
			if ~isempty(obj.odeInput)
				for it = 1 : numel(obj.odeInput.outputType)
					if ~contains(obj.odeInput.outputType(it), obj.prefix + ".")
						obj.odeInput.strrepOutputType(obj.odeInput.outputType(it), ...
							obj.prefix + "." + obj.odeInput.outputType(it));
					end
				end
			end
		end % setOutputName()
		
		function obj = resetOutputName(obj, oldPrefix, newPrefix)
			resetOutputName@model.Wave(obj, oldPrefix, newPrefix);
			if ~isempty(obj.odeInput)
				obj.odeInput.strrepOutputType(oldPrefix+".", newPrefix+".");
				assert(all(contains(obj.odeInput.outputType, [newPrefix, '.'])), ...
					'outputType of odeInput is faulty');
			end
		end % resetOutputName()
		
		function newWave = bcTimeDerivative(obj, bcName)
			% Apply time-derivative to the boundary condition specified by the string
			% bc and by using the ode-dynamics.			
			arguments
				obj model.WaveOde;
				bcName (1, 1) string;
			end
			
			if strcmp(bcName, "bc0")
				z = "0";
			elseif strcmp(bcName, "bc1")
				z = "1";
			else
				error("Boudary condition " + bcName + " is unknown");
			end
			
			bcOld = obj.(bcName);
			% time derivative of boundary condition (PDE part)
			assert(all(bcOld.isMatrixZeroOrEmpty(...
				'Ct0', 'Cz0', 'Ct1', 'Cz1', 'Ct', 'Cz', 'Czk', 'Ctk')), ...
				"Time derivative not applicable / implemented for this boundary condition");
			
			if bcOld.isMatrixZeroOrEmpty('Ck')
				newBc = model.Output(bcName, 'Ct', bcOld.Ct, 'Ct1', bcOld.C1, 'Ct0', bcOld.C0);
			else
				newBc = model.Output(bcOld, 'zk', bcOld.zk, 'Ctk', bcOld.Ck, ...
					'Ct', bcOld.Ct, 'Ct1', bcOld.C1, 'Ct0', bcOld.C0);
			end
			
			% consider ODE and input
			ode2pdeTemp = copy(obj.ode2pde);
			newInput = obj.input.copy.remove(obj.ode2pde.type);
			
			% Apply time-derivative to all inputs but ode2pde:
			tempNewInput = newInput.copy();
			for it = 1 : tempNewInput.numTypes
				if ~tempNewInput.input(it).isMatrixZeroOrEmpty("B" + z)
					% A new input is added of the time derivative of that input, 
					% indicated by index _t in type.
					newInput.add(...
						model.Input(tempNewInput.type(it) + "_t", ...
							"B" + z, tempNewInput.input(it).("B" + z)));
					newInput.get(tempNewInput.type(it)).("B" + z) = ...
						0 * newInput.get(tempNewInput.type(it)).("B" + z);
				end
			end
			% Furthermore, the time derivative of the ODE state can be replaced with
			% the ODEs state equation times ode2pde.B0
			%	ode2pde.B0 w_t(t) = ode2pde.B0 (ode.A w(t) + pde2ode[v] + odeInput u(t))
			% which is considered in the following:
			% 1) B0 ode.A w(t) | B1 ode.A. w(t)
			oldB01 = ode2pdeTemp.("B"+z);
			ode2pdeTemp.("B"+z) = oldB01 * obj.ode.A;
			% 2) B0 pde2ode[v] | B1 pde2ode[v]
			pde2ode2pde = mtimes(obj.pde2ode, oldB01, bcName);
			newBc = newBc.parallel(pde2ode2pde);
			% 3) B0 odeInput | B0 odeInput
			for it = 1 :  obj.odeInput.numTypes
				newInput.add(model.Input(obj.odeInput.inputType(it), ...
					"B"+z, oldB01 * obj.odeInput.gain(it).value));
			end
			
			% create resulting WaveOde-object.
			newWave = obj.copyAndReplace(...
				bcName, newBc.strrepType(newBc.type, bcName), ...
				'input', newInput+ode2pdeTemp);
			
		end % bcTimeDerivative()

		
		function [x0, w0] = setX01minusCos2PiZQuad(obj)
			x0 = setX01minusCos2PiZQuad@model.Wave(obj);
			w0 = zeros(size(obj.ode.A, 1));
		end % setX01minusCos2PiZQuad()
		
		function [x0, w0] = setIcLinear(obj, w0, x0z1)
			assert(isnumeric(w0) && isequal(size(w0), [size(obj.ode.A, 1), 1]), ...
				'w0 of wrong size or type');
			assert(isnumeric(x0z1) && isequal(size(x0z1), [obj.n, 1]), ...
				'x0z1 of wrong size or type');
			obj.w0 = w0;
			x0z0 = obj.ode2pde.B0 * w0;
			x0 = quantity.Symbolic(x0z0 + (x0z1-x0z0) * obj.Gamma(1).variable, ...
				obj.Gamma(1).domain, "name", "x_{0}");
		end % setIcLinear()
		
		%% get methods
		function pde2ode = get.pde2ode(obj)
			pde2ode = obj.output.output(obj.output.index(obj.prefix + ".pde2ode"));
		end % get.pde2ode()
		
		function ode2pde = get.ode2pde(obj)
			ode2pde = obj.input.input(obj.input.index(obj.prefix + ".ode2pde"));
		end % get.ode2pde()
		
		function myStateName = stateName(obj)
			% stateName getter of stateName struct.
			if isscalar(obj)
				myStateName = stateName@model.Wave(obj);
				myStateName.ode = obj.prefix + ".w";
			else
				myStateNameCell = cell(size(obj));
				for it = 1 : numel(obj)
					myStateNameCell{it} = obj(it).stateName;
				end
				myStateName = cat(1, myStateNameCell{:});
			end
		end % stateName()
		
	end % methods
	
	methods (Access = protected)
		% Override copyElement method:
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@model.Wave(obj);
			cpObj.odeInput = copy(obj.odeInput);
		end
	end % methods (Access = protected)
end % classdef
