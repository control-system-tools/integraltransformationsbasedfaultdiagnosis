classdef Wave < model.Dps & handle & matlab.mixin.Copyable
	% model.Wave represents n coupled linear wave equations.
	% This implements wave systems of the form
	%
	%	v_tt(z,t) = Gamma(z) v_zz(z,t) + D(z) v_t(z,t) + G(z) v_z(z,t) - G0(z) v_z(0,t) ...
	%				 S(z) v(z,t) + int_at^bt Ft(z,zeta) v_t(zeta,t) dzeta ...
	%				 + int_az^bz Fz(z,zeta) v_z(zeta,t) dzeta + int_a^b F(z,zeta) v(zeta,t) dzeta
	%				 + input.B(z) u(t)
	%			0 = bc0(v, v_t) + input.B0 u(t)
	%			0 = bc1(v, v_t) + input.B1 u(t)
	%		 y(t) = output[v(z,t), v_t(z,t)] + input.D u(t)
	%
	% and the initial conditions v(z,0) = v_0(z), v_t(z,0) = v_t,0(z).
	% The distributed states are v(z, t), v_t(z, t) \in R^n, with the spatial variable z in [0, 1] 
	% and the temporal variable t.
	% The boundary operators bc0 and bc1 allow to define general Dirichlet, Neumann, velocity and
	% integral terms, see model.Output. The vector u(t) contains all input signals, for instance 
	% control input,  disturbances or faults. y(t) is a vector of output signals, for instance the
	% control output or measurements. Outputs can define general outputs
	%
	%	output[h, h_t] = sum_{i=1}^{l} [F_i * h(z_i)] + int_0^1 [C(z) h(z) + Cz(z) h_z(z)] dz ...
	%					 + sum_{i=1}^{lt} [Ft_i * h_t(z_i)] + int_0^1 [Ct(z) h(z)] dz
	%
	% see model.Outputs.
	% The bounds of the integral in the PDE are defined by FBounds = {a, b}, FtBounds = {at, bt}, 
	% FzBounds = {az, bz} wherein at a, b, at, bt, az, bz can be spatial values in [0, 1] or a 
	% string "z".
	%
	% See also model.Wave.Wave (constructor), model.Dps, model.WaveOde, model.Transport, 
	% model.TransportOde
	
	properties (SetAccess = protected)
		Gamma (:,:) quantity.Discrete {mustBe.quadraticMatrix};	% coefficient of v_zz
		D (:,:) quantity.Discrete {mustBe.quadraticMatrix};		% coefficient of v_t
		G (:,:) quantity.Discrete {mustBe.quadraticMatrix};		% coefficient of v_z
		G0 (:,:) quantity.Discrete {mustBe.quadraticMatrix};	% coefficient of v_z(0)
		S (:,:) quantity.Discrete {mustBe.quadraticMatrix};		% coefficient of v
		Ft (:,:) quantity.Discrete {mustBe.quadraticMatrix};	% coefficient in integral of v_t
		FtBounds (1, 2) cell = {0, 0};							% integral bounds at, bt in a cell
		Fz (:,:) quantity.Discrete {mustBe.quadraticMatrix};	% coefficient in integral of v_t
		FzBounds (1, 2) cell = {0, 0};							% integral bounds az, bz in a cell
		F (:,:) quantity.Discrete {mustBe.quadraticMatrix};		% coefficient in integral of v_t
		FBounds (1, 2) cell = {0, 0};							% integral bounds a, b in a cell
		
		% left boundary operator at z=0, see model.Output. By default, bc0 implements
		%	v(0, t) = input.B0 * input.B0
		% with bc0.C0 = -I.
		bc0;
		
		% right boundary operator at z=1, see model.Output. By default, bc1 implements 
		%	v_z(1, t) = input.B1 * u
		% with bc1.Cz1 = -I.
		bc1;
		
		% Input gains for control signal, disturbances, faults, etc. are stored in this model.Inputs
		% object. Inputs are defined at the left (B0) and right (B1) boundary and distributed (B).
		% Feedthroughs (D) can also be implemented via the output property. See model.Inputs
		input;
		
		% Output operator for control, measurement, PDE -> ODE coupling, etc... Implements boundary, 
		% distributed, pointwise, dirichlet, neumann, and time derivative outputs.
		output;
		
		n {mustBeInteger, mustBeNonnegative};	% number of waves, i.e. size(obj.Gamma, 1)
		
		% finite approximation of the dynamics, see model.Wave.setApproximation.
		approximation;
		
		% cell of spatial grids for every state which are used for the discrete approximation. The
		% grids might be different for every state.
		grid cell;
		
		% matrix to select discrete-space approximation of v and v_t which are implemented as
		% ODE-states of the lumped approximation, see model.Wave.setApproximation.
		domain2ode (:,:) double;
		% matrix to select discrete-space approximation of v and v_t which are implemented as
		% algebraic equations of the lumped approximation (for instance some boundary conditions),
		% see model.Wave.setApproximation.
		domain2algebraic (:,:) double
		% matrix to select discrete-space approximation of v and v_t which are implemented as
		% ODE-states of the lumped approximation and who represent displacement states v,
		% see model.Wave.setApproximation.
		domain2odePosition (:,:) double;
		% matrix to select discrete-space approximation of v and v_t which are implemented as
		% ODE-states of the lumped approximation and who represent velocity states v_t,
		% see model.Wave.setApproximation.
		domain2odeVelocity (:,:) double;
		
		name;				% name of object
		arglist;			% list of input parameters used in the constructor
		prefix;				% is added to the type of output, the state name and simulation signals
	end % properties (SetAccess = protected)
	
	properties (Dependent = true)
		gridLength (:, 1) double;	% length of the elements of the obj.grid-cell array
		settlingTime;		% struct of the nominal achievable settling time by state feedback
	end % properties (Dependent = true)
	
	properties (Hidden = true)
		% name of the matrix defining the dominant PDE dynamics i.e. "Gamma" for the wave velocity
		% of model.Wave or "Lambda" for the transport velocity of model.Transport
		nameCharacteristicMatrix = "Gamma";
	end % properties (Hidden = true)
	
	methods
		function obj = Wave(Gamma, varargin)
			% model.Wave.Wave constructs a model.Wave object, which describes a system of 
			% n coupled wave equations.
			%
			%	obj = model.Wave(Gamma) returns a wave system with wave velocities defined by 
			%		Gamma, i.e. the PDE-coefficient of the 2nd order time-derivative term. Usually, 
			%		Gamma is diagonal, and has positive diagonal-elements.
			%
			%	model.Wave([...], "D", D) sets the distributed matrix D, which is the 
			%		PDE-coefficient of time-derivative term v_t. D must be a quantity.Discrete with
			%		size and domain fitting to Gamma.
			%
			%	model.Wave([...], "F", F) sets the distributed matrix F, which is the 
			%		coefficient of the non-derivative term v in the integral. F must be a 
			%		quantity.Discrete with size and domain fitting to Gamma and defined on and
			%		domains z and zeta.
			%
			%	model.Wave([...], "FBounds", FBounds) sets bounds of the integral over the 
			%		non-derivative state variable to FBounds. FBounds{1} is the lower bound, 
			%		FBounds{2} is the upper bound. Elements of FBounds may be scalars \in [0, 1] or
			%		a string/char "z".
			%
			%	model.Wave([...], "Ft", Ft) sets the distributed matrix Ft, which is the 
			%		coefficient of the time-derivative term v_t in the integral. Ft must be a 
			%		quantity.Discrete with size and domain fitting to Gamma and defined on and 
			%		domains z and zeta.
			%
			%	model.Wave([...], "FtBounds", FtBounds) sets bounds of the integral over the 
			%		time-derivative state variable to FtBounds. FtBounds{1} is the lower bound, 
			%		FtBounds{2} is the upper bound. Elements of FtBounds may be scalars \in [0, 1]
			%		or a string/char "z".
			%
			%	model.Wave([...], "Fz", Fz) sets the distributed matrix Fz, which is the 
			%		coefficient of the space-derivative term v_z in the integral. Fz must be a 
			%		quantity.Discrete with size and domain fitting to Gamma and defined on and the
			%		domains z and zeta.
			%
			%	model.Wave([...], "FzBounds", FzBounds) sets bounds of the integral over the 
			%		space-derivative state variable to FzBounds. FzBounds{1} is the lower bound, 
			%		FzBounds{2} is the upper bound. Elements of FzBounds may be scalars \in [0, 1]
			%		or a string/char "z".
			%
			%	model.Wave([...], "G", G) sets the distributed matrix G, which is the 
			%		PDE-coefficient of space-derivative term v_z. G must be a quantity.Discrete with
			%		size and domain fitting to Gamma.
			%
			%	model.Wave([...], "G", G) sets the distributed matrix G0, which is the 
			%		PDE-coefficient of local space-derivative term v_z(0). G0 must be a 
			%		quantity.Discrete with size and domain fitting to Gamma.
			%
			%	model.Wave([...], "S", S) sets the distributed matrix S, which is the 
			%		PDE-coefficient of non-derivative term v. S must be a quantity.Discrete with
			%		size and domain fitting to Gamma.
			%
			%	model.Wave([...], "bc0", bc0) sets the boundary operator for z=0 to the 
			%		model.Output object bc0, to implement: 0 = bc0(v, v_t) + input.B0 u(t)
			%		By default, bc0 implements v(0, t) = input.B0 * u with bc0.C0 = -I.
			%		input must be defined separate, see below.
			%
			%	model.Wave([...], "bc1", bc1) sets the boundary operator for z=1 to the
			%		model.Output object bc1, to implement: 0 = bc1(v, v_t) + input.B1 u(t).
			%		By default, bc1 implements  v_z(1, t) = input.B1 * u with bc1.Cz1 = -I.
			%		input must be defined separate, see below. When there is a bc1 input, there must
			%		not be a Q1 input.
			%
			%	model.Wave([...], "input", input) sets obj.input. Input gains for control
			%		signal, disturbances, faults, etc. are stored in this model.Inputs object.
			%		Inputs are  defined at the left (B0) and right (B1) boundary and distributed (B).
			%		Feedthroughs (D) can also be implemented via the output property. 
			%		See model.Inputs
			%
			%	model.Wave([...], "output", output) sets obj.output. Output operator for
			%		control output, measurement, PDE -> ODE coupling, etc... Implements boundary,
			%		distributed, pointwise, dirichlet, neumann, and time-derivative outputs.
			%
			%	model.Wave([...], "name", name) sets obj.name.
			%
			%	model.Wave([...], "prefix", prefix) sets obj.prefix, which is a string, which
			%		is added to the type of output, the state name and simulation signals to be able
			%		to differ between different systems.
			%
			% See also model.Wave (class description and equations), model.Dps, model.WaveOde.
			if nargin > 0
				obj.Gamma = Gamma;
				if MAX(-Gamma) > 0
					warning("Gamma should be positive");
				end
				obj.n = size(obj.Gamma, 1);
				
				%% Input parser
				myParser = misc.Parser();
				myParser.addParameter("name", string(obj(1).n) + " waves", @(v) ischar(v) |isstring(v));
				myParser.addParameter("prefix", "plant", @(v) ischar(v) | isstring(v));
				
				% PDE
				zeroOfZ = quantity.Discrete.zeros([obj.n, obj.n], Gamma(1).domain);
				zeroOfZZeta = quantity.Discrete.zeros([obj.n, obj.n], ...
					[Gamma(1).domain, quantity.Domain("zeta", Gamma(1).domain.grid)]);
				myParser.addParameter("D", zeroOfZ.copy.setName("D"));
				myParser.addParameter("G", zeroOfZ.copy.setName("G"));
				myParser.addParameter("G0", zeroOfZ.copy.setName("G0"));
				myParser.addParameter("S", zeroOfZ.copy.setName("S"));
				myParser.addParameter("F", zeroOfZZeta.copy.setName("F"));
				myParser.addParameter("FBounds", {0, 0});
				myParser.addParameter("Ft", zeroOfZZeta.copy.setName("F_{t}"));
				myParser.addParameter("FtBounds", {0, 0});
				myParser.addParameter("Fz", zeroOfZZeta.copy.setName("F_{z}"));
				myParser.addParameter("FzBounds", {0, 0});
				
				% Input & Output
				myParser.addParameter("input", model.Inputs());
				myParser.addParameter("output", model.Outputs());
				
				% BC at z = 0
				myParser.addParameter("bc0", model.Output("bc0", "C0", -eye(obj.n)));
				
				% BC at z = 1
				myParser.addParameter("bc1", model.Output("bc1", "Cz1", -eye(obj.n)));
				
				%% write from parser to obj
				myParser.parse(varargin{:});
				obj.arglist = fieldnames(myParser.Results);
				for it = 1 : numel(obj.arglist)
					myProp = myParser.Results.(obj.arglist{it});
					if any(strcmp(obj.arglist{it}, ["D", "G", "G0", "S", "F", "Ft", "Fz"]))
						% check grid
						assert(isequal(myProp(1).domain(1).grid([1, end]), [0; 1]), ...
							"spatial grids must be defined from 0 to 1");
						if any(strcmp(obj.arglist{it}, ["F", "Ft", "Fz"]))
							assert(isequal([myProp(1).domain.name], ["z", "zeta"]), ...
								"domain.name of "+obj.arglist{it}+" must be [z, zeta]");
							assert(isequal(myProp(1).domain(2).grid([1, end]), [0; 1]), ...
								"spatial grids "+obj.arglist{it}+" must be defined from 0 to 1");
						end
					elseif strcmp(obj.arglist{it}, "input")
						if isa(myProp, "model.Input")
							% cast Input 2 Inputs
							myProp = model.Inputs(myProp.copy());
						else
							myProp = myProp.copy();
						end
					elseif strcmp(obj.arglist{it}, "output")
						if isa(myProp, "model.Output")
							% cast Output 2 Outputs
							myProp = model.Outputs(myProp.copy());
						else
							myProp = myProp.copy();
						end
					end
					obj.(obj.arglist{it}) = myProp;
				end
				
				%% Check if intergral bounds of F, Ft, Fz are defined correctly
				for myF = ["F", "Ft", "Fz"]
					if any(strcmp(myParser.UsingDefaults, myF))
						assert(any(strcmp(myParser.UsingDefaults, myF+"Bounds")) ...
							&& iscell(obj.(myF+"Bounds")) && (numel(obj.(myF+"Bounds")) == 2) ...
							&& (isnumeric(obj.(myF+"Bounds"){1}) || strcmp(obj.(myF+"Bounds"){1}, "z")) ...
							&& (isnumeric(obj.(myF+"Bounds"){2}) || strcmp(obj.(myF+"Bounds"){2}, "z")), ...
							myF+"Bounds must be defined if " +myF + " is input of constructor. " + ...
							myF+"Bounds must be a cell array with two elements which are either" + ...
							" numeric values or a char 'z'");
					end
				end
				
				%% Input-Check: Output & Input
				% todo
				
				%% add prefix to output
				obj = obj.setOutputName();
				
			end % nargin > 0
		end % Wave() Constructor
		
		function obj = setApproximation(obj, varargin)
			% setApproximation creates a lumped approximation of the dynamics by setting the
			% obj-properties approximation and grid by using finite differences or chebfun methods 
			% to approximate the wave dynamics.
			% The system is approximated using the state x = [v, v_t].' and
			% the ODE
			%	xPde_t = [		0										I						] x
			%			 [	Gamma D2 + G D1 + S + int F (.) dzeta		D + int Ft (.) dzeta	]
			%		   = A x
			%
			% and the algebraic equations for the BCs
			%	xBc(z=0) = f(x, x_t, u),
			%	xBc(z=1) = f(x, x_t, u).
			%
			% The state x results by combining xPde and xBc. D2 and D1 are
			% differentiation matrices for representing spatial
			% derivatives of 2nd and 1st order. f(x, xt, u) is function
			% that represents the boundary condition, but converts all kind
			% of Neumann or Robin BCs to Dirichlet BCs.
			%
			% setApproximation(OBJ, "method", method) with method = 
			% "finiteDifferences" or "cheb" specifies the approximation method used
			% for the spatial coordinate. default: "cheb"
			%
			% setApproximation(OBJ, "zPointSimulation", n) Number n of spatial 
			% discretization points used in simulation, 
			% default: numel(obj.Gamma(1).domain.grid)
			% 
			% See also getClosedLoopSimulationModel and simulate.
			
			myParser = misc.Parser();
			myParser.addParameter("method", "cheb", @(v) ismember(v, ["cheb", "finiteDifferences"]));
			myParser.addParameter("zPointSimulation", obj.Gamma(1).domain.n);
			myParser.addParameter("grid", obj.Gamma(1).domain.grid);
 			myParser.parse(varargin{:});
			method = myParser.Results.method;
			zPointSimulation = myParser.Results.zPointSimulation;
			
			if isfield(myParser.Unmatched, "grid")
				if ~contains(myParser.UsingDefault, "zPointSimulation")
					zPointSimulation = numel(myParser.Results.grid);
				end
			end
			
			% Check if selected approxmation method can be used.
			if strcmp(method, "cheb")
				if exist("chebfun", "class") ~= 8
					warning("To use chebyshev polynomial methods for simulation " ...
						+ "the chebfun package needs to be in the matlab path. " ...
						+ "Since no chebfun class was found, please download the " ...
						+ "package from www.chebfun.org." ...
						+ " Meanwhile, the finite differences method is used.")
					method = "finiteDifferences";
				end
			end
			
			% create grid and differention and integral matrices
			if strcmp(method, "finiteDifferences")
				myGrid = linspace(obj.Gamma(1).domain(1).grid(1), obj.Gamma(1).domain(1).grid(end), ...
							zPointSimulation).';
				D1 = numeric.fd.matrix("5pointCentral", myGrid, 1);
				D2 = numeric.fd.matrix("5pointCentral", myGrid, 2);
				I1 = numeric.massMatrix(quantity.Domain("z", myGrid));
			elseif strcmp(method, "cheb")
				myGrid = chebpts(zPointSimulation, obj.Gamma(1).domain(1).grid([1, end]));
				D1 = diffmat(numel(myGrid), 1, myGrid([1, end]).');
				D2 = diffmat(numel(myGrid), 2, myGrid([1, end]).');
				I1 = cumsummat(numel(myGrid), myGrid([1, end]).');
			else
				error("method = " + method + " is not a valid value");
			end
			gridCell = cell(obj.n, 1);
			[gridCell{:}] = deal(myGrid);
			myGridLength = numel(myGrid) * ones(obj.n, 1);
			obj.grid = gridCell;
			
			%% Boundary conditions
			% the boundary conditions are approximated as algebraic equations, hence
			% they do not appear in the system matrix A, but in the output equation.
			% The approximation is implemented in the model.Output-class by
			% approximating integrals with a mass matrix and derivatives with finite
			% differences.
			% If the boundary gain for the velocity, i.e. Ct0 and Ct1 are invertible,
			% then those are used to implement boundary conditions for the velocity
			% states. Otherwise, the boundary conditions are implemented for the
			% position term as algebraic equation.
			bc.implementation = ["position", "position"];
			if abs(det(obj.bc0.Ct0)) > 1e-3 * abs(det(obj.bc0.C0))
				bc.implementation(1) = "velocity";
			end
			if abs(det(obj.bc1.Ct1)) > 1e-3 * abs(det(obj.bc1.C1))
				bc.implementation(2) = "velocity";
			end
			[bc.operator, bc.weights, bc.input] = useAsBoundaryOperator(...
				obj.bc0 + obj.bc1, bc.implementation, [0, 1], ...
				"grid", myGrid, "withTimeDerivative", true, ...
				"input", obj.input, ...
				"finiteDifferencesStencilLength", 5, ... % only needed for finiteDifferences
				"method", method);
			bc.selector.z0 = 1:2:(2*obj.n-1);
			bc.selector.z1 = bc.selector.z0 + 1;
			
			%% matrices for combining in-domain and boundary states
			% to a full state-vector. The boundary conditions are
			% implemented as algebraic boundary conditions, while the indomain states
			% result from simulating the pde as ode in time.
			% Hence, the matrices algebraic2domain and ode2domain have to be
			% calculated such that
			%	   x = ode2domain * xOde + algebraic2domain xAlgebraic
			% holds. Notice, that also
			%		  xOde = ode2domain.' * x = domain2ode * x and
			%	xAlgebraic = algebraic2domain.' * x = domain2algebraic * x
			% holds. However, the matrices ode2domain and algebraic2domain depend on
			% wheather the boundary condition is a "position" or a "velocity"
			% boundary condition.
			ode2domainPositionCell = cell(obj.n, 1);
			algebraic2domainPositionCell = cell(obj.n, 1);
			ode2domainVelocityCell = cell(obj.n, 1);
			algebraic2domainVelocityCell = cell(obj.n, 1);
			numPositionBcs = sum(strcmp(bc.implementation, "position"));
			numVelocityBcs = sum(strcmp(bc.implementation, "velocity"));
			for it = 1 : obj.n
				% left boundary z = 0
				if strcmp(bc.implementation{1}, "position")
					ode2domainPositionCell{it} = zeros(1, myGridLength(it)-numPositionBcs);
					algebraic2domainPositionCell{it} = eye(1, numPositionBcs);
				elseif strcmp(bc.implementation{1}, "velocity")
					ode2domainVelocityCell{it} = zeros(1, myGridLength(it)-numVelocityBcs);
					algebraic2domainVelocityCell{it} = eye(1, numVelocityBcs);
				end
				% indomain
				ode2domainPositionCell{it} = [ode2domainPositionCell{it}; ...
						eye(myGridLength(it)-numPositionBcs)];
				algebraic2domainPositionCell{it} = [algebraic2domainPositionCell{it}; ...
						zeros(myGridLength(it)-numPositionBcs, numPositionBcs)];
				ode2domainVelocityCell{it} = [ode2domainVelocityCell{it}; ...
						eye(myGridLength(it)-numVelocityBcs)];
				algebraic2domainVelocityCell{it} = [algebraic2domainVelocityCell{it}; ...
						zeros(myGridLength(it)-numVelocityBcs, numVelocityBcs)];
				% right boundary z = 1
				if strcmp(bc.implementation{2}, "position")
					ode2domainPositionCell{it} = [ode2domainPositionCell{it}; ...
							zeros(1, myGridLength(it)-numPositionBcs)];
					algebraic2domainPositionCell{it} = [algebraic2domainPositionCell{it}; ...
							flip(eye(1, numPositionBcs))];
				elseif strcmp(bc.implementation{2}, "velocity")
					ode2domainVelocityCell{it} = [ode2domainVelocityCell{it}; ...
							zeros(1, myGridLength(it)-numVelocityBcs)];
					algebraic2domainVelocityCell{it} = [algebraic2domainVelocityCell{it}; ...
							flip(eye(1, numVelocityBcs))];
				end
			end % for it = 1 : obj.n
			ode2domain = blkdiag(ode2domainPositionCell{:}, ode2domainVelocityCell{:});
			algebraic2domain = blkdiag(algebraic2domainPositionCell{:}, algebraic2domainVelocityCell{:});
			obj.domain2ode = ode2domain.';
			obj.domain2algebraic = algebraic2domain.';
			obj.domain2odePosition = blkdiag(ode2domainPositionCell{:}).';
			obj.domain2odeVelocity = blkdiag(ode2domainVelocityCell{:}).';
			
% 			% check result
% 			assert(isequal(size(algebraic2domain), [2*sum(myGridLength(:)), 2*obj.n]));
% 			assert(isequal(size(ode2domain), 2*sum(myGridLength(:))+[0, -2*obj.n]));
% 			assert(sum(algebraic2domain(:)) == 2 * obj.n);
% 			assert(sum(algebraic2domain(:))+sum(ode2domain(:)) == 2*sum(myGridLength));
% 			xTest = rand(2*sum(myGridLength), 1);
% 			assert(isequal(xTest, ...
% 				ode2domain * obj.domain2ode * xTest + algebraic2domain * obj.domain2algebraic * xTest));
			
			%% PDE dynamics
			% first the PDEs approximation is calculated for the full grid.
			% Later the rows for the boundary are removed, if that boundary
			% values result from the boundary conditions, which are
			% implemented as algebraic equations ("position", not "velocity").
			% The approximation rows for each state are calculated
			% separatetly, since in a later version, the spatial
			% discretication may be implemented individual for every state
			% to improve numerical performance (see wikipedia "cfl"-number).
						
			% a.pde.ll = Gamma D2 + (G+G0) D1 + S + F + Fz D1	(A lower left -> ll)
			a.pde.ll = cell(obj.n, obj.n);
			% a.pde.lr = D + Ft						(A lower right -> lr)
			a.pde.lr = cell(obj.n, obj.n);
			b.pde.cell = cell(obj.n, 1);
			
			integralDomainF = obj.integralDomain(obj.FBounds, obj.F(1).domain);
			integralDomainFt = obj.integralDomain(obj.FtBounds, obj.Ft(1).domain);
			integralDomainFz = obj.integralDomain(obj.FzBounds, obj.Fz(1).domain);
			FIntDomain = obj.F * integralDomainF;
			FtIntDomain = obj.Ft * integralDomainFt;
			FzIntDomain = obj.Fz * integralDomainFz;
			
			% preallocate distributed B (since getter is slow);
			Btemp = obj.input.B;
			for it = 1 : obj.n % iterate over rows of system operator
				for jt = 1 : obj.n % iterate over columns of system operator
					GammaTemp = diag(reshape(obj.Gamma(it, jt).on(gridCell{it}), [myGridLength(it), 1, 1]));
					GTemp = diag(reshape(obj.G(it, jt).on(gridCell{it}), [myGridLength(it), 1, 1]));
					G0Temp = reshape(obj.G0(it, jt).on(gridCell{it}), [myGridLength(it), 1, 1]) ...
						* [1, zeros(1, myGridLength(jt)-1)];
					STemp = diag(reshape(obj.S(it, jt).on(gridCell{it}), [myGridLength(it), 1, 1]));
					DTemp = diag(reshape(obj.D(it, jt).on(gridCell{it}), [myGridLength(it), 1, 1]));
					FTemp = zeros(myGridLength(it), myGridLength(jt));
					FtTemp = zeros(myGridLength(it), myGridLength(jt));
					FzTemp = zeros(myGridLength(it), myGridLength(jt));
					F_ij = FIntDomain(it, jt).on({gridCell{it}, gridCell{jt}}, {'z', 'zeta'});
					Ft_ij = FtIntDomain(it, jt).on({gridCell{it}, gridCell{jt}}, {'z', 'zeta'});
					Fz_ij = FzIntDomain(it, jt).on({gridCell{it}, gridCell{jt}}, {'z', 'zeta'});
					if strcmp(method, "cheb")
						for zIdx = 1 : myGridLength(it)
							FTemp(zIdx, :) = I1(zIdx, :) .* F_ij(zIdx, :);
							FtTemp(zIdx, :) = I1(zIdx, :) .* Ft_ij(zIdx, :);
							FzTemp(zIdx, :) = I1(zIdx, :) .* Fz_ij(zIdx, :);
						end
					else
						for zIdx = 1 : myGridLength(it)
							FTemp(zIdx, :) = I1 * F_ij(zIdx, :).';
							FtTemp(zIdx, :) = I1 * Ft_ij(zIdx, :).';
							FzTemp(zIdx, :) = I1 * Fz_ij(zIdx, :).';
						end
					end
					a.pde.ll{it, jt} = GammaTemp * D2 + (GTemp - G0Temp + FzTemp) * D1 + STemp + FTemp;
					a.pde.lr{it, jt} = DTemp + FtTemp;
				end % for jt = 1 : obj.n
				
				% consider distributed input
				if isempty(obj.input)
					b.pde.cell{it} = zeros(myGridLength(it), 0);
				else
					if ~isempty(Btemp)
						b.pde.cell{it} = reshape(Btemp(it, :).on(gridCell{it}), ...
							[myGridLength(it), sum(obj.input.length(:))]);
					else
						b.pde.cell{it} = zeros(myGridLength(it), sum(obj.input.length(:)));
					end
				end
			end % for it = 1 : obj.n
			
			%% combine indomain and boundary to obtain state-space-matrices
			a.pde.top = [zeros(sum(myGridLength(:))), eye(sum(myGridLength(:)))];
			b.pde.top = zeros(sum(myGridLength(:)), sum(obj.input.length(:)));
			% now combine a.pde.top and b.pde.top with a.pde.l*. b.pde.cell
			a.pde.full = [a.pde.top; cell2mat(a.pde.ll), cell2mat(a.pde.lr)];
			b.pde.full = vertcat(b.pde.top, b.pde.cell{:});
			a.pde.indomain = obj.domain2ode * a.pde.full * ode2domain;
			b.pde.indomain = obj.domain2ode * b.pde.full;
			%a.pde.bc = obj.domain2algebraic * a.pde.full;
			
			%% store result in property
			% finite state model of the kind
			%	dt xinDomain(t) = A * xinDomain(t) + B u(t)
			%	y(t) = x(t) = C * xinDomain(t) + D u(t) 
			% while x includes indomain states (xinDomain) as well as boundary states
			% which result from algebraic equations implemented in the output equation.
			ssA = a.pde.indomain ...
				+ obj.domain2ode * a.pde.full * algebraic2domain * bc.operator * ode2domain;
			ssB = b.pde.indomain ...
				+ obj.domain2ode * a.pde.full * algebraic2domain * bc.input;
			ssC = ode2domain + algebraic2domain * bc.operator * ode2domain;
			ssD = algebraic2domain * bc.input;
			obj.approximation = ss(ssA, ssB, ssC, ssD);
			
			% set input and output names
			if isempty(obj.input)
				assert(size(obj.approximation.B, 2) == 0, "faulty input")
			else
				obj.approximation = stateSpace.setSignalName(obj.approximation, "input", ...
					obj.input.type, obj.input.length);
			end
			obj.approximation = stateSpace.setSignalName(obj.approximation, "output", ...
				obj.stateName.pde, [sum(myGridLength); sum(myGridLength)]);
			
			%% add Outputs to simulation model
			obj.approximation = obj.output.addOutput2stateSpace(...
				obj.approximation, ...
				obj.stateName.pde, gridCell, true, ...
				"finiteDifferencesStencilLength", 5, ...
				"method", method);
			obj.approximation = obj.input.addD2ss(obj.approximation);
				
		end % setApproximation()
				
		function simData = simulatePostproduction(obj, simData)
			x = misc.getfield(simData, obj.stateName.pde(1));
			x_t = misc.getfield(simData, obj.stateName.pde(2));
			z = x(1).domain.find("z");
			if ~isequidistant(z) % assume its a chebgrid
				assert(isequal(z.grid, chebpts(z.n, z.grid([1, end]).')), ...
					"unrecognized grid");
				D1 = diffmat(numel(z.grid), 1, z.grid([1, end]).');
			else
				D1 = numeric.fd.matrix("5pointCentral", z.grid, 1);
			end
			
			% use misc.multArray for differentiation
			x_z = quantity.Discrete(permute(...
				misc.multArray(D1, x.on(), 1, 2), [2, 1, 3]), ...
				x(1).domain, "name", strrep(x_t(1).name, "_t", "_z"));
			x_tt = x_t.diff("t", 1).setName(strrep(x_t(1).name, "_t", "_tt"));
			
			% backup of original simulation data
			simData.(obj.prefix).originalSimulation = simData.(obj.prefix);
			
			% sample data to grid specified in simData.domain
			if isequidistant(z)
				for var = {x, x_t, x_tt, x_z}
					varTemp = var{1};
					simData = misc.setfield(simData, varTemp(1).name, varTemp.changeDomain(simData.domain));
				end
			else % cheb case
				z = simData.domain.find("z");
				t = simData.domain.find("t");
				for var = {x, x_t, x_tt, x_z}
					if isnan(var{1}, true)
						varTemp = var{1}.changeDomain([t, z]);
						varDouble = on(varTemp);
					else
						varTemp = var{1}.changeDomain(t);
						varDouble = zeros([t.n, z.n, size(varTemp)]);
						for it = 1 : numel(varTemp)
							chebObjTemp = chebfun(varTemp(it).on().', z.grid([1, end]).');
							varDouble(:, :, it) = chebObjTemp(z.grid).';
						end
					end
					quanTemp = quantity.Discrete(varDouble, ...
						simData.domain, "name", varTemp(1).name);
					simData = misc.setfield(simData, quanTemp(1).name, quanTemp);
				end
			end
		end % simulatePostproduction()
		
		function spatialGrids = getSpatialGridsOfSimulation(obj, varargin)
			% getSpatialGridsOfSimulation returns a cell array containing 2 
			% cell-arrays. The first contains the names of signals that are 
			% distributed in space, the second one contains the spatial grids of 
			% those signals. The resulting cell-array of spatial grids is later used
			% in stateSpace.simulationOutput2Quantity to split the simulations result
			% into signals.
			myParser = misc.Parser();
			myParser.addParameter("observer", []);
			myParser.parse(varargin{:});
			
			spatialGrids = {obj.stateName.pde(:), {obj.grid; obj.grid}};
			if ~isempty(myParser.Results.observer)
				spatialGrids{1} = [spatialGrids{1}(:); myParser.Results.observer.dynamics.stateName.pde(:)];
				spatialGrids{2} = [spatialGrids{2}; ...
					{myParser.Results.observer.grid; myParser.Results.observer.grid}];
			end
		end % getSpatialGridsOfSimulation
		
		function myStateSpace = getClosedLoopSimulationModel(obj, varargin)
			% getClosedLoopSimulationModel creates a closed-loop simulation model containing the
			% lumped plant approximation, static and dynamic feedback and observer.
			% See also simulate and setApproximation.
			myParser = misc.Parser();
			myParser.addParameter("feedback", []);
			myParser.addParameter("observer", []);
			myParser.addParameter("odeObserver", []);
			myParser.addParameter("signalModel", []);
			myParser.addParameter("Odes", []);
			myParser.addParameter("sumblk", []);
			myParser.parse(varargin{:});
			myFeedback = myParser.Results.feedback;
			myObserver = myParser.Results.observer;
			myOdeObserver = myParser.Results.odeObserver;
			mySignalModel = myParser.Results.signalModel;
			myOdes = myParser.Results.Odes;
			mySumblk = myParser.Results.sumblk;
			
			% combine feedback and model:
			if isempty(myFeedback) && isempty(myObserver)
				% open loop
				
				% establish approximation of system dynamics
				obj = obj.setApproximation(varargin{:});
				myStateSpace = obj.approximation;
				
			elseif ~isempty(myFeedback) && isempty(myObserver) 
				% closed-loop state feedback
				assert(~isempty(obj.input.get(obj.prefix + ".control")) && ...
					misc.iseye(obj.input.get(obj.prefix + ".control").B1), ...
					"the control input matrix must be an identity and defined at the right boundary");
				
				% get closed-loop dynamics by including feedback to boundary 
				% condition bc1. This allows a better implementation of the 
				% closed-loop boundary dynamics in model.Wave.setApproximation.
				% 1. split pde state feedback and other gains in feedback.input
				fb = myFeedback.copyAndReplace("input", misc.Gains());
				otherGain = myFeedback.input.copy();
				% rename ode-feedback into ode2pde-type to consider it correctly when adding
				% otherGain to input
				if isfield(obj.stateName, "ode")
					otherGain.get(obj.stateName.ode).strrepInputType(obj.stateName.ode, obj.ode2pde.type);
				end
				
				% 2. set closed-loop boundary condition
				bc1ClosedLoop = parallel(obj.bc1, fb); 
				bc1ClosedLoop = bc1ClosedLoop.strrepType(bc1ClosedLoop.type, obj.bc1.type);
				
				% 3. add otherGain to input.
				inputClosedLoop = copy(obj.input);
				for it = 1 : otherGain.numTypes
					inputClosedLoop = inputClosedLoop.add(model.Input(...
						otherGain.gain(it).inputType, "B1", otherGain.gain(it).value));
				end
				inputClosedLoop.remove(obj.prefix + ".control");
				
				% 4. add feedback to output to obtain the control input in the
				% simulation output simData
				outputClosedLoop = copy(obj.output).add(...
					myFeedback.copy.strrepType(myFeedback.type, obj.prefix + ".control"));
				
				% 5. get closed-loop system.
				closedLoopSystem = obj.copyAndReplace(...
					"bc1", bc1ClosedLoop, ...
					"input", inputClosedLoop, ...
					"output", outputClosedLoop);
				
				% 6. get closed-loop approximation
				closedLoopSystem = closedLoopSystem.setApproximation(varargin{:});
				myStateSpace = closedLoopSystem.approximation;
				% write selector matrixes into original objects, as they will be used for setting
				% the initial conditions
				obj.domain2ode = closedLoopSystem.domain2ode;
				obj.domain2algebraic = closedLoopSystem.domain2algebraic;
				obj.domain2odePosition = closedLoopSystem.domain2odePosition;
				obj.domain2odeVelocity = closedLoopSystem.domain2odeVelocity;
				obj.grid = closedLoopSystem.grid;
				
				if ~isempty(myOdeObserver) % reference observer
					myOutputName = [myStateSpace.OutputName; myOdeObserver.OutputName];
					myInputName = union(myStateSpace.InputName, myOdeObserver.InputName, "stable");
					myInputName = setdiff(myInputName, myOutputName);
					myStateSpace = stateSpace.connect( ...
						myInputName, myOutputName, ...
						myStateSpace, myOdeObserver.odes);
					for it = 1 : numel(myFeedback.input.gain)
						if ~isempty(mySignalModel) &&...
								(strcmp(myFeedback.input.gain(it).inputType, ...
									mySignalModel.stateNameReference) || ...
								strcmp(myFeedback.input.gain(it).inputType, ...
									mySignalModel.stateName))
							myFeedback.input.gain(it).strrepInputType(...
								mySignalModel.name, "observer");
						end
					end
				end
				
			elseif isempty(myFeedback) && ~isempty(myObserver) 
				% observer parallel to plant

				% establish approximation of system dynamics
				obj = obj.setApproximation(varargin{:});
				
				myObserver = myObserver.setApproximation(varargin{:});
				myObserver.approximation = stateSpace.removeInputOfOutput(myObserver.approximation);
				myOutputName = [obj.approximation.OutputName; myObserver.approximation.OutputName];
				myInputName = union(obj.approximation.InputName, myObserver.approximation.InputName, "stable");
				myInputName = setdiff(myInputName, myOutputName);
				myStateSpace = stateSpace.connect( ...
					myInputName, myOutputName, ...
					obj.approximation, myObserver.approximation);
				
			elseif ~isempty(myFeedback) && ~isempty(myObserver) 
				% closed-loop output feedback
				warning("not tested, check setApproximation args!")
				
				% establish approximation of system dynamics
				obj = obj.setApproximation(varargin{:});

				myObserver = myObserver.setApproximation(varargin{:});
				myObserver.approximation = stateSpace.removeInputOfOutput(myObserver.approximation);
				
				myOutputName = [obj.approximation.OutputName; myObserver.approximation.OutputName];
				myInputName = union(obj.approximation.InputName, myObserver.approximation.InputName, "stable");
				myInputName = setdiff(myInputName, myOutputName);
				myStateSpace = stateSpace.connect( ...
					myInputName, myOutputName, ...
					obj.approximation, myObserver.approximation);
				
				% replace plant and signalModel signals in feedback
				if any(contains(myFeedback.input.inputType, ".network."))
					warning("for cooperative output feedback the prefix must not be changed." ...
						+ " See also model.Transport.getClosedLoopSimulationModel")
				end
				for it = 1 : numel(myFeedback.input.gain)
					myFeedback.input.gain(it).strrepInputType(...
						obj.prefix+".", myObserver.prefix+".", "startOnly", true);
					if ~isempty(mySignalModel) &&...
							strcmp(myFeedback.input.gain(it).inputType, ...
								mySignalModel.stateNameDisturbance)
						myFeedback.input.gain(it).strrepInputType(...
							mySignalModel.name+".", myObserver.prefix+".", "startOnly", true);
					end
					if ~isempty(mySignalModel) &&...
							(strcmp(myFeedback.input.gain(it).inputType, ...
								mySignalModel.stateNameReference) || ...
							strcmp(myFeedback.input.gain(it).inputType, ...
								mySignalModel.stateName))
 						myFeedback.input.gain(it).strrepInputType(...
 							mySignalModel.name+".", myObserver.prefix+".", "startOnly", true);
					end
				end
				
				% connect observer based state feedback with input of plant and observer
				myStateSpace = myFeedback.useAsFeedback(...
					myStateSpace, obj.prefix + ".control", ...
					myObserver.dynamics.stateName.pde, obj.grid, true, ...
					varargin{:});
				myStateSpace = myFeedback.useAsFeedback(...
					myStateSpace, myObserver.prefix + ".control", ...
					myObserver.dynamics.stateName.pde, myObserver.grid, true, ...
					varargin{:});
			end
			
			if ~isempty(mySignalModel)
				myStateSpace = mySignalModel.connect2ss(myStateSpace);
			end % if ~isempty(signalModel)
			
			% additional sumblk (sum blocks)
			if ~isempty(mySumblk)
				if ~iscell(mySumblk)
					mySumblk = {mySumblk};
				end
				for it = 1 : numel(mySumblk)
					myStateSpace = stateSpace.connect(...
						[myStateSpace.InputName; ...
							setdiff(mySumblk{it}.InputName, myStateSpace.OutputName)], ...
						[myStateSpace.OutputName; mySumblk{it}.OutputName], ...
						myStateSpace, mySumblk{it});
				end
			end % if ~isempty(mySumblk)
			
			% additional state-spaces defined in ss
			if ~isempty(myOdes)
				myStateSpace = stateSpace.connect(...
					[setdiff(myStateSpace.InputName, myOdes.OutputName); ...
						setdiff(myOdes.InputName, myStateSpace.OutputName)], ...
					[myStateSpace.OutputName; myOdes.OutputName], ...
					myStateSpace, myOdes.odes);
			end % if ~isempty(mySs)
			
		end % getClosedLoopSimulationModel()
		
		function [ic, stateName] = getInitialCondition(obj, varargin)
			% getInitialCondition reads varargin if there are initial conditions
			% defined as name-value-pair with the name according to obj.stateName and
			% returns 2 cell arrays: 
			%	1. ic is a cell array of the values of the states initial condition
			%	2. stateName is a cell array of the names of those states.
			
			icStruct = misc.struct(varargin{:});
			
			% Create initial condition for all states specified by obj.stateName
			% If a value is specified in icStruct, than this value is used.
			% Otherwise, a default value is considered.
			if misc.isfield(icStruct, obj.stateName.pde(1))
				x0quantity = misc.getfield(icStruct, obj.stateName.pde(1));
			else
				x0quantity = quantity.Discrete.zeros([obj.n, 1], obj(1).Gamma(1).domain);
			end
			assert(numel(x0quantity) == obj.n, ...
				"vector of pdes position initial condition has wrong length");
			if misc.isfield(icStruct, obj.stateName.pde(2))
				x0quantity_t = misc.getfield(icStruct, obj.stateName.pde(2));
			else
				x0quantity_t = quantity.Discrete.zeros([obj.n, 1], obj(1).Gamma(1).domain);
			end
			assert(numel(x0quantity_t) == obj.n, ...
				"vector of pdes velocity initial condition has wrong length");
			
			myIc = [];
			myIc_t = [];
			for it = 1 : obj.n
				myIc = [myIc; x0quantity(it).on(obj.grid{it})];
				myIc_t = [myIc_t; x0quantity_t(it).on(obj.grid{it})];
			end
			
			% create output parameter
			ic = {obj.domain2odePosition * myIc; ...
					obj.domain2odeVelocity * myIc_t};
			stateName = obj.stateName.pde;
		end % getInitialCondition()
		
		function ew = eigenvaluesOfSimulationModel(obj, varargin)
			warning("This method eigenvaluesOfSimulationModel() returns unverified, " ...
				+ "and odd results. Do not trust them.");
			
			myParser = misc.Parser();
			myParser.addParameter("feedback", []);
			myParser.parse(varargin{:});
			myStateSpace = getClosedLoopSimulationModel(obj, ...
				myParser.Results.feedback);
			
			ew = eig(myStateSpace);
		end % eigenValuesOfSimulationModel()
		
		function x0 = setX01minusCos2PiZQuad(obj)
			% setX01minusCos2PiZQuad returns a vector x0 that can be used as initial condition fo
			% the PDE-state. Every element of x0 is 0.5*(1-cos(2*pi*z))^2.
			% See also setX0zero, setX0one.
			x0 = quantity.Symbolic(repmat(0.5*(1-cos(2*pi*sym("z")))^2, [obj.n, 1]), ...
				obj.Gamma(1).domain, "name", "x_{0}");
		end % setX01minusCos2PiZQuad()
		
		function gridLength = get.gridLength(obj)
			% gridLength is the number of discrete points used for the numerical
			% approximation. Since the number of grid points may be individual for
			% every state, gridLength is a vector of the lengths of each state
			gridLength = cellfun(@(v) numel(v), obj.grid);
		end % get.gridLength()
		
		function myStateName = stateName(obj)
			% stateName getter of stateName struct.
			if isscalar(obj)
				myStateName.pde = obj.prefix + [".x"; ".x_t"];
			else
				myStateNameCell = cell(size(obj));
				for it = 1 : numel(obj)
					myStateNameCell{it} = obj(it).stateName;
				end
				myStateName = cat(1, myStateNameCell{:});
			end
		end % stateName()
		
		function settlingTime = get.settlingTime(obj)
			% based on the state feedback control of hyperbolic transport systems,
			% this settling time is the nominal settling time via backstepping (finite) or
			% backstepping-Fredholm (minimum) boundary control.
			Lambda = sqrtm(blkdiag(obj.Gamma, -rot90(rot90(obj.Gamma))));
			phi1 = int(Lambda, "z", 0, 1);
			settlingTime.minimum = phi1(obj.n) - phi1(obj.n+1);
			settlingTime.finite = sum(abs(phi1(1:(obj.n+1))));
		end % get.settlingTime()
		
		function [texString, nameValuePairs] = printParameter(obj)
			% printParameter displays a list of all parameters in the command window.
			%
			% See also misc.variables2tex, misc.variables2tex, misc.latexChar, print.
			nameValuePairs = {
				"\Gamma", obj.Gamma, ...
				"D", obj.D, ...
				"G", obj.G, ...
				"G0", obj.G0, ...
				"S", obj.S, ...
				"bc0", "?", ...
				"bc1", "?", ..., ...
				};
			texString = misc.variables2tex([], nameValuePairs);
		end % printParameter()
		
		function texString = print(obj, stateNamePde, stateNameOde, doPrintOutput)
			% print Print equation to command window as latex compatible string-array.
			%
			% See also misc.variables2tex, misc.variables2tex, misc.latexChar, printParameter.
			
			arguments
				obj;
				stateNamePde = strrep(obj.stateName.pde(1), obj.prefix + ".", "");
				stateNameOde = "";
				doPrintOutput = true;
			end % arguments
			
			% prepare pde
			suffix.D = "\dot{"+stateNamePde+"}(z, t)";
			suffix.G = stateNamePde+"^{\prime}(z, t)";
			suffix.G0 = stateNamePde+"^{\prime}(0, t)";
			suffix.S = stateNamePde+"(z, t)";
			suffix.Ft = "\dot{"+stateNamePde+"}(\zeta, t) \mathrm{d}\zeta";
			suffix.Fz = stateNamePde+"^{\prime}(\zeta, t) \mathrm{d}\zeta";
			suffix.F = stateNamePde+"(\zeta, t) \mathrm{d}\zeta";
			option.rational = true;
			pre.Ft = "\int\limits_{" + misc.latexChar(obj.FtBounds{1}, option) ...
				+ "}^{" + misc.latexChar(obj.FtBounds{2}, option) + "}";
			pre.Fz = "\int\limits_{" + misc.latexChar(obj.FzBounds{1}, option) ...
				+ "}^{" + misc.latexChar(obj.FzBounds{2}, option) + "}";
			pre.F = "\int\limits_{" + misc.latexChar(obj.FBounds{1}, option) ...
				+ "}^{" + misc.latexChar(obj.FBounds{2}, option) + "}";
			mySign.G0 = "-";
			
			% put pde together
			pdeString = "\ddot{"+stateNamePde+"}(z, t) &= " ...
				+ misc.latexChar(obj.Gamma) + " " + stateNamePde+"^{\prime\prime}(z, t)";
			for myPar = ["D", "G", "G0", "S", "Ft", "Fz", "F"]
				if MAX(abs(obj.(myPar))) ~= 0
					if isfield(mySign, myPar)
						pdeString = pdeString + " " + mySign.(myPar);
					else
						pdeString = pdeString + " +";
					end
					if isfield(pre, myPar)
						pdeString = pdeString + " " + pre.(myPar);
					end
					if iseye(obj.(myPar))
						pdeString = pdeString + " ";
					else
						pdeString = pdeString + " " + misc.latexChar(obj.(myPar));
					end
					if isfield(suffix, myPar)
						pdeString = pdeString + " " + suffix.(myPar);
					end
				end
			end
			
			myInput = copy(obj.input);
			if isa(obj, "model.WaveOde")
				myInput.remove(obj.ode2pde.type);
				ode2pdeString = [...
					obj.ode2pde.print("B", stateNameOde + "(t)"); ...
					obj.ode2pde.print("B0", stateNameOde + "(t)"); ...
					obj.ode2pde.print("B1", stateNameOde + "(t)")];
				if ~obj.ode2pde.isMatrixZeroOrEmpty("D")
					warning("D in ode2pde is nonzero but not printed");
				end
			else
				ode2pdeString = [""; ""; ""];
			end
			
			% add inputs and boundary conditions
			myStringArray = [...
					pdeString + myInput.print("B"); ...
					obj.bc0.print(0, stateNamePde) + myInput.print("B0"); ...
					obj.bc1.print(0, stateNamePde) + myInput.print("B1")]...
				+ ode2pdeString;
			
			% add output
			if doPrintOutput
				tempOutput = obj.output.copy.add(myInput.D);
				myStringArray = [myStringArray; tempOutput.print()];
			end
			
			% set output parameter or print
			if nargout > 0
				texString = myStringArray;
			else
				misc.printTex(myStringArray);
			end
		end % print()
		
		function obj = discretizeQuantity(obj)
			% discretizeQuantity converts all properties containg quantity-objects to
			% quantity.Discrete objects. This is often speeds up calculations.
			for prop = ["Gamma", "D", "G", "G0", "S", "Ft", "Fz", "F"]
				obj.(prop) = quantity.Discrete(obj.(prop));
			end
			obj.input = obj.input.discretizeQuantity();
			obj.output = obj.output.discretizeQuantity();
			obj.bc0 = obj.bc0.discretizeQuantity();
			obj.bc1 = obj.bc1.discretizeQuantity();
		end % discretizeQuantity()
		
	end % methods
	
	methods (Access = protected)
		function cpObj = copyElement(obj)
			% Make a shallow copy of all properties
			cpObj = copyElement@matlab.mixin.Copyable(obj);
			cpObj.Gamma = copy(obj.Gamma);
			cpObj.D = copy(obj.D);
			cpObj.G = copy(obj.G);
			cpObj.G0 = copy(obj.G0);
			cpObj.S = copy(obj.S);
			cpObj.Ft = copy(obj.Ft);
			cpObj.Fz = copy(obj.Fz);
			cpObj.F = copy(obj.F);
			cpObj.bc0 = copy(obj.bc0);
			cpObj.bc1 = copy(obj.bc1);
			cpObj.input = copy(obj.input);
			cpObj.output = copy(obj.output);
		end
	end % methods (Access = protected)
end % classdef