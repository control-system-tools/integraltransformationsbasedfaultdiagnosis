function [tests] = phdThesisTest()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function setup(testCase)
init();
end

function beamDetectionTest(testCase)

% perform the fault detection for the cantilever beam
examples.thesis.cantilever.detection.main;

% do the export to a local temporary location:
exportOptions{2} = fullfile("+unittests", "build", "phdThesisTestData");
examples.thesis.cantilever.detection.export;

unitTestPath = fullfile("+unittests", "phdThesisTestData", "data", "beam", "detection");

% compare all created data:
files = dir(fullfile(unitTestPath, "*.dat"));
for file = files'
	
	verifyFile = readmatrix( fullfile( unitTestPath, file.name ) );
	testFile = readmatrix( fullfile( exportOptions{2}, exportOptions{4}, file.name ) );
	
	fprintf("verify the data of file %s: ", file.name);
	testCase.verifyEqual( verifyFile, testFile );
end

end

function beamDiagnosisTest(testCase)

examples.thesis.cantilever.diagnosis.main;

exportOptions{2} = fullfile("+unittests", "build", "phdThesisTestData");
unitTestPath = fullfile("+unittests", "phdThesisTestData", exportOptions{4});


examples.thesis.cantilever.diagnosis.export;

% compare all created data:
files = dir(fullfile(unitTestPath, "*.dat"));
for file = files'
	
	verifyFile = readmatrix( fullfile( unitTestPath, file.name ) );
	testFile = readmatrix( fullfile( exportOptions{2}, exportOptions{4}, file.name ) );
	
	fprintf("verify the data of file %s: ", file.name);
	testCase.verifyEqual( verifyFile, testFile );
end

end


function cableDetectionTest(testCase)

examples.thesis.cable.detection.main;

exportOptions{2} = fullfile("build", "phdThesisTestData");
unitTestPath = fullfile("+unittests", "phdThesisTestData", exportOptions{4});

flags.export = true;
examples.thesis.cable.detection.export;

% compare all created data:
files = dir(fullfile(unitTestPath, "*.dat"));
for file = files'
	
	verifyFile = readmatrix( fullfile( unitTestPath, file.name ) );
	testFile = readmatrix( fullfile( exportOptions{2}, exportOptions{4}, file.name ) );
	
	fprintf("verify the data of file %s: ", file.name);
	testCase.verifyEqual( verifyFile, testFile );
end

end

function cableDiagnosisTest(testCase)

examples.thesis.cable.diagnosis.main;

exportOptions{2} = fullfile("build", "phdThesisTestData");
unitTestPath = fullfile("+unittests", "phdThesisTestData", exportOptions{4});

flags.export = true;
examples.thesis.cable.diagnosis.export;

% compare all created data:
files = dir(fullfile(unitTestPath, "*.dat"));
for file = files'
	
	verifyFile = readmatrix( fullfile( unitTestPath, file.name ) );
	testFile = readmatrix( fullfile( exportOptions{2}, exportOptions{4}, file.name ) );
	
	fprintf("verify the data of file %s: ", file.name);
	testCase.verifyEqual( verifyFile, testFile );
end

end

