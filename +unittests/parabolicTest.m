function [tests] = parabolicTest()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function initTest(testCase)

z = quantity.Domain("z", linspace(0, 1));
a = quantity.Symbolic( sym("z") + 1, z);
b = quantity.Discrete.ones(1, z);
p = faultdiagnosis.parabolic.ScalarParabolic( z, 'alpha', a, 'beta', b, 'gamma', -b, ...
	'q00', 1, 'q01', 1, ...
	'q10', 1, 'q11', 1, ...
	'b1', [a; 0], 'b2', [1; 0], 'b3', [0; 1], 'b4', eye(2), ...
	'g1', a, 'g2', 1, 'g3', 1, 'g4', [1; 0], ...
	'G_bounded', 1, 'G_deterministic', 1, ...
	'e1', b, 'e2', 2, 'e3', 2, 'e4', [0; 1], ...
	'F', diag([-2, -3]), 'l1', [a; a], 'l2', [1;0], 'l3', [0;1], ...
	'h1', [a; b], 'h2', [1;0], 'h3', [0;1], ...
	'c1', [b 0 0 0 0]', 'c2', [0 1 0 0 0]', 'c3', [0 0 1 0 0]', 'c4', [ zeros(3, 2); eye(2)], ...
	'N_s', 15);

% todo sinnvollen Test schreiben.
end

function testODEPDECouplings(testCase)

z = quantity.Domain("z", linspace(0, 1, 100));
t = quantity.Domain("t", 0:1e-3:10);

a = quantity.Symbolic(1, z, 'name', 'alpha');

% PDE:
%          dt v = dz( a dz v )
% q01 dz v(0,t) = b2 u
% q11 dz v(1,t) = 0
%             y = v(1,t)
pde = faultdiagnosis.parabolic.ScalarParabolic(z, 'alpha', a, ...
	'q01', 1, 'b2', 1, ...
	'q11', 1, ...
	'c3', 1);

% dt w = F w + B u
F = -1;
B = 1; 
C = 1;
ode = ss( -1, B, C, [] );

u = quantity.Discrete(ones(t.n, 1), t, "name", "u");

% simulate the ode system:
uPde = quantity.Discrete( lsim(ode, u.on, t.grid), t);
% simulate the pde system:

sim.pde = pde.discreteModel.simulate(t, "u", uPde);

% ODE-PDE system
%          dt v = dz( a dz v )
% q01 dz v(0,t) = -h2 w
% q11 dz v(1,t) = 0
%             y = v(1,t)
odepde = faultdiagnosis.parabolic.ScalarParabolic(z, 'alpha', a, ...
	'q01', 1, 'h2', -1, ...
	'q11', 1, ...
	'F', F, 'b4', B, ...
	'c3', 1);


sim.odePde = odepde.discreteModel.simulate(t, "u", u);

testCase.verifyEqual( sim.pde.v.on(), sim.odePde.v.on(), "AbsTol", 1e-7)

end

function testWithReferenceSolution(testCase)

zDomain = quantity.Domain("z", linspace(0, 1, 500));
z = Discrete(zDomain);
time = quantity.EquidistantDomain("t", 0, 2, "stepNumber", 201);

% TODO: It looks like there could be an error dependent on alpha?
%	for the parameters: alpha = 1; beta = 0; gamma = 0; the numeric error is about 1e-11. For alpha
%	\not= 1 the numeric error increases very fast. Maybe there is a bug, concerning the usage of
%	alpha?
%	The effect occurrs for both: alpha < 1 and alpha > 1
coeffs.alpha = 2 * quantity.Discrete.ones(1, zDomain);
coeffs.beta = -5 * quantity.Discrete.ones(1, zDomain);
coeffs.gamma = -5 * quantity.Discrete.ones(1, zDomain);

% test Neumann boundary conditions
model = faultdiagnosis.parabolic.ScalarParabolic( zDomain, ...
	"alpha", coeffs.alpha, ...
	"beta", coeffs.beta, ...
	"gamma", coeffs.gamma, ...
	"b1", [z^2,  - 2 * coeffs.alpha.diff(zDomain) * z - 2 * coeffs.alpha - 2 * coeffs.beta * z - z^2 * coeffs.gamma], ...
	"b3", [0 2]);

u = quantity.Function({@(t) t.*0 + 1; @(t) t}, time);

sim = model.discreteModel.simulate( time, "u", u);

w = quantity.Function( @(z,t) z.^2 .* t, [zDomain, time]);

% plot( [sim.v; w] )
% w.near(sim.v)

testCase.verifyEqual( w.on(), sim.v.on(), "AbsTol", 6e-3 );

end

function referenceTrajectoryPlanningTest(testCase)

z = quantity.Domain("z", linspace(0, 1, 501)');
a = quantity.Discrete.ones(1, z);
gma = -a;
l1 = quantity.Symbolic( sin(sym("z") * pi), z);

sys = faultdiagnosis.parabolic.ScalarParabolic(z, 'alpha', a, 'gamma', gma * 0, 'b1', [-a 0 0 0], ...
	'q00', 0, 'q01', 1, 'g2', -1, 'q10', 1, 'q11', 0, 'e2', 1, 'b2', [0 1 0 0], 'b3', [0 0 -1 0], ...
	'F', -1, 'l1', l1, 'l2', 0, 'l3', 1, 'b4', [0 0 0 1], ...
	'h1', l1 * 0, 'h2', 3, 'h3', 3,...
	'N_s', 15 );
% max( real( eig( sys.discreteModel.stsp.A ) ) )

I = quantity.Domain("t", linspace(0, 1, 151));
b = signals.GevreyFunction('order', 1.8, 'timeDomain', I, 'diffShift', 1, 'numDiff', sys.N_s);

trj = sys.referenceTrajectory([1; 0; 0; 1] * b );
sim = sys.discreteModel.simulate(I, "u", trj.u);

% figure()
% subplot(311);
% plot(trj.x(2), "currentFigure", true)
% subplot(312);
% plot(sim.v, "currentFigure", true)
% subplot(313);
% plot(sim.v - trj.x(2), "currentFigure", true)
% plot( sim.v.subs("z", 0 ), "hold", true, "currentFigure", true )
% plot( trj.x(2).subs("z", 0 ), "hold", true, "currentFigure", true )

testCase.verifyTrue( near(trj.x(2), sim.v, 2e-1) );
testCase.verifyTrue( near(trj.w, sim.w, 3e-2) );
testCase.verifyTrue( near( trj.x(2).subs("z", 1),  -sys.h3 * trj.w, 5e-12 ) )

%% test the trajectory planning with truncation
trun.sys = faultdiagnosis.parabolic.ScalarParabolic(z, 'alpha', a, 'gamma', gma * 0, 'b1', [-a 0 0 0], ...
	'q00', 0, 'q01', 1, 'g2', -1, 'q10', 1, 'q11', 0, 'e2', 1, 'b2', [0 1 0 0], 'b3', [0 0 -1 0], ...
	'F', -1, 'l1', l1, 'l2', 0, 'l3', 1, 'b4', [0 0 0 1], ...
	'h1', l1 * 0, 'h2', 3, 'h3', 3,...
	'N_s', 15, "truncate", true );
trun.trj = trun.sys.referenceTrajectory([1; 0; 0; 1] * b );

testCase.verifyTrue( trj.u.near(trun.trj.u) );
testCase.verifyTrue( trj.w.near(trun.trj.w) );
testCase.verifyTrue( trj.x.near(trun.trj.x) );

end

% function femMatricesTest(testCase)
% 
% % test case was generated with the old implementation and the following
% % settings:
% % alpha = 1;
% % gamma = -2;
% % c0  = dps.parabolic.faultdiagnosis.OutputOperator('zm', 0, 'C', 1);
% % c1  = dps.parabolic.faultdiagnosis.OutputOperator('zm', 1, 'C', 1);
% % b   = dps.parabolic.faultdiagnosis.Input('b', 15, 'type', 'B');
% % e   = dps.parabolic.faultdiagnosis.Input('b', [1, 3], 'type', 'E');
% % %     
% % ---------- robin --------------
% % bc0 = dps.parabolic.faultdiagnosis.BoundaryCondition('z', 0, 'q1', 1, 'q0', 2, 'b', 2, 'g', 3, 'e', [4 5]);
% % bc1 = dps.parabolic.faultdiagnosis.BoundaryCondition('z', 1, 'q1', 1, 'q0', 3, 'b', 0, 'g', 6, 'e', [7 8]);
% % ---------- neumann ------------
% % bc0 = dps.parabolic.faultdiagnosis.BoundaryCondition('z', 0, 'q1', 1, 'q0', 0, 'b', 2, 'g', 3, 'e', [4 5]);
% % bc1 = dps.parabolic.faultdiagnosis.BoundaryCondition('z', 1, 'q1', 1, 'q0', 0, 'b', 0, 'g', 6, 'e', [7 8]);
% %----------- dirichlet ---------
% % bc0 = dps.parabolic.faultdiagnosis.BoundaryCondition('z', 0, 'q1', 0, 'q0', 1, 'b', 2, 'g', 3, 'e', [4 5]);
% % bc1 = dps.parabolic.faultdiagnosis.BoundaryCondition('z', 1, 'q1', 0, 'q0', 1, 'b', 0, 'g', 6, 'e', [7 8]);
% %
% % sys = dps.parabolic.faultdiagnosis.System('alpha', alpha, 'gamma', gamma, ...
% %     'B', b, 'E', e, 'BoundaryCondition', [bc0, bc1], 'c0', c0, 'c1', c1);
% % sys.approximation = dps.parabolic.faultdiagnosis.FEApproximation(sys, 'N', 11, 'v', 1);
% 
% testMatrices = load('+faultdiagnosis/+unittests/parabolicFEM.mat');
% z = linspace(0, 1, 11)';
% 
% sysNeuman = faultdiagnosis.parabolic.System(z, ...
% 	'alpha', quantity.Symbolic(1, 'variable', {'z'}, 'grid', z), ...
% 	'gamma', quantity.Symbolic(-2, 'variable', {'z'}, 'grid', z), ...
% 	'q01', 1, 'b0', 2, 'g0', 3, 'e0', [4 5], ...
% 	'q11', 1, 'b1', 0, 'g1', 6, 'e1', [7 8], ...
% 	'b', -15, 'e', -[1 3], 'c20', [1; 0], 'c30', [0; 1]);
% stspNeumann = sysNeuman.discretization.stsp;
% compareStateSpaceMatrices(testCase, testMatrices.stsp_neumann, stspNeumann);
% 
% sysDirichlet = faultdiagnosis.parabolic.System(z, ...
% 	'alpha', quantity.Symbolic(1, 'variable', {'z'}, 'grid', z), ...
% 	'gamma', quantity.Symbolic(-2, 'variable', {'z'}, 'grid', z), ...
% 	'q01', 0, 'q00', 1, 'b0', 2, 'g0', 3, 'e0', [4 5], ...
% 	'q11', 0, 'q10', 1, 'b1', 0, 'g1', 6, 'e1', [7 8], ...
% 	'b', -15, 'e', -[1 3], 'c20', [1; 0], 'c30', [0; 1]);
% compareStateSpaceMatrices(testCase, testMatrices.stsp_dirichlet, sysDirichlet.discretization.stsp);
% 
% sysRobin = faultdiagnosis.parabolic.System(z, ...
% 	'alpha', quantity.Symbolic(1, 'variable', {'z'}, 'grid', z), ...
% 	'gamma', quantity.Symbolic(-2, 'variable', {'z'}, 'grid', z), ...
% 	'q01', 1, 'q00', 2, 'b0', 2, 'g0', 3, 'e0', [4 5], ...
% 	'q11', 1, 'q10', 3, 'b1', 0, 'g1', 6, 'e1', [7 8], ...
% 	'b', -15, 'e', -[1 3], 'c20', [1; 0], 'c30', [0; 1]);
% compareStateSpaceMatrices(testCase, testMatrices.stsp_robin, sysRobin.discretization.stsp);
% end

% function compareStateSpaceMatrices(testCase, sysA, sysB)
% 
% testCase.verifyTrue(numeric.near(sysA.A, sysB.A, 1e-3));
% testCase.verifyTrue(numeric.near(sysA.B, sysB.B, 1e-3));
% testCase.verifyTrue(numeric.near(sysA.C, sysB.C, 1e-3));
% 
% end

% 
% function faultDiagnosisTest(testCase)
% % definition of the system parameters:
% z = linspace(0, 1, 51)';
% a = quantity.Symbolic(1 , 'variable', 'z', 'grid', z, 'name', 'alpha');
% c = quantity.Symbolic(1 , 'variable', 'z', 'grid', z, 'name', 'c');
% 
% % system description
% sys = faultdiagnosis.parabolic.System(z, 'alpha', a, ...
% 	'q01', 1, ...
% 	'q11', 1, 'e1', -1, 'b1', -1, ...
% 	'c10', c);
% b0 = signals.GevreyFunction('order', 1.99,...
% 	'N_t', 101, 'T', 2, 'diffShift', 1);
% 
% %% Time
% dt = 1e-3;
% tend = 2;
% t = (0:dt:tend)';
% 
% %% definition of the faults and the disturbances:
% f = signals.faultmodels.TaylorPolynomial('coefficients', [1], ...
% 	'occurrence', 0, ...
% 	'localGrid', b0.grid{1}, ...
% 	'globalGrid', t, ...
% 	'T', b0.T, ...
% 	'index', 1);
% 
% U = signals.RandomStairs('steps', 15, 'seedt', -1, 'seedf', 1, 'dim', 1);
% u = U.smoothSignal(t);
% 
% [y, t] = sys.simulate(t, 'u', u, 'f', f.on(t), 'plot', true );
% 
% %%
% kernl = faultdiagnosis.Kernel(sys, 'faults', f, 'basicVariable', b0, 'N_s', 7);
% %%
% % kernl.computeKernel()
% fir = kernl.computeFir('dt', dt);
% 
% %% fault detection:
% [theta] = faultdiagnosis.Kernel.faultDetection(fir, t, y, u);
% 
% testCase.verifyEqual(theta(end), 1, 'AbsTol', 0.1);
% 
% end



