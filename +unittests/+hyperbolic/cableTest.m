function [tests] = cableTest()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function setup(testCase)
	% define the discretization parameters so that the systems will lead to simulation results that can
	% be compared.
	Nz = 5; % TODO 51;
	dt = 1e-1; %TODO 1e-3;
	%TODO t = quantity.EquidistantDomain("t", 0, 5, "stepSize", dt);
	t = quantity.EquidistantDomain("t", 0, 1, "stepSize", dt);

	[faultyCable, waveCable, ~, ~, sysParas, cableParameters] = ...
		examples.thesis.cable.wave2heteroDirectionalSystem(Nz);
	
	testCase.TestData.Nz = Nz;
	testCase.TestData.dt = dt;
	testCase.TestData.t = t;
	testCase.TestData.faultyCable = faultyCable;
	testCase.TestData.waveCable = waveCable;
	testCase.TestData.dModel = signals.models.Constant(1, "timeDomain", t, "signalName", "d");

end

% function teardown(testCase)
% 	close(testCase.TestData.Figure);
% end

% TODO uncomment
% function implementationTest(testCase)
% 
% %%
% t = testCase.TestData.t;
% 
% u = quantity.Discrete.empty;
% u = [1; 1] * quantity.Symbolic( sin( sym("t") * pi *2 ), t);
% %quantity.Discrete.ones([2,1], t);
% 
% %u(1,1) = signals.UniformDistributedSignal(t, "seedf", 1, "seedt", 2, "steps", 4);
% %u(2,1) = signals.UniformDistributedSignal(t, "seedf", 3, "seedt", 3, "steps", 4);
% Nz = 111;
% 
% %% 1st order
% % [cable1stOrder, parameters1] = examples.thesis.cable.systemDefinitionSys1stOrder(Nz);
% % [simData1, cableModel1] = cable1stOrder.simulate( "t", t.grid, "plant.u", u);
% % y1 = simData1.plant.y;
% % y1.setName("y_1");
% % y1.plot
% 
% %% wave
% cableWave = testCase.TestData.waveCable;
% simData2 = cableWave.simulate( "t", t.grid, "plant.u", u, "zPointSimulation", 151);
% simData2.plant.y.plot
% y2 = simData2.plant.y;
% y2.setName("y_2");
% 
% %% hetero direct
% cableHetero = testCase.TestData.faultyCable;
% simData3 = cableHetero.systemOde.simulate( "t", t.grid, "plant.u", u);
% simData3.plant.y.plot
% y3 = simData3.plant.y;
% y3.setName("y_{3}");
% 
% %% compare the matrices
% % R = [0 1 0 0; 1 0 0 0; 0 0 1 0; 0 0 0 1];
% % 
% % tilde.Lamba = R * cable1stOrder.Lambda * R^(-1);
% % tilde.A = R * cable1stOrder.A * R;
% % 
% % testCase.verifyEqual( tilde.Lamba.on, cableHetero.Lambda.on(), "AbsTol", 1e-12);
% % testCase.verifyEqual( tilde.A.on, cableHetero.A.on(), "AbsTol", 1e-12);
% 
% %% compare the simulation results
% % testCase.verifyEqual( y1.on(), y3.on(), "AbsTol", 1e-12 );
% testCase.verifyEqual( y2.on(), y3.on(), "AbsTol", 1e-5 );
% %% visualization
% % plot( y1, "figureId", 1, "hold", true)
% plot( y2, "figureId", 3, "hold", false)
% plot( y3, "figureId", 3, "hold", true)
% % 
% % plot( y1 - y2, "figureId", 2, "hold", false)
% % plot( y1 - y3, "figureId", 2, "hold", false)
% 
% end

function simulationTest(testCase)

t = testCase.TestData.t;
u = quantity.Discrete.empty;
u(1,1) = signals.UniformDistributedSignal(t, "seedf", 1, "seedt", 2, "filter", "gevrey");
u(2,1) = signals.UniformDistributedSignal(t, "seedf", 3, "seedt", 3, "filter", "gevrey");

heteroDirectionalSim = testCase.TestData.faultyCable.systemOde.simulate( "t", t.grid, "plant.u", u);
waveSim = testCase.TestData.waveCable.simulate( "t", t.grid, "plant.u", u);

% waveSim.plant.y.plot("figureId", 2, "hold", true);
% heteroDirectionalSim.plant.y.plot("figureId", 2, "hold", true);

testCase.verifyEqual( heteroDirectionalSim.plant.y.on(), waveSim.plant.y.on(), "AbsTol", 2e-5);%TODO 1.1e-6

end

function faultDetectionKernelTest(testCase)
	
dModel = testCase.TestData.dModel;
faultyCable = testCase.TestData.faultyCable;

kernel = faultdiagnosis.hyperbolic.heteroDirectional.DetectionKernel( faultyCable, ...
	"disturbanceModel", dModel, "numberOfLines", 5, "tolerance", 1e-1); % 

testCase.verifyEqual( kernel.backsteppingKernel.hash, 'EBB63D5BED4E2E102CE8E0F4DD67E566D3FE3B2D'); % TODO '40423773FB84CB76D908C3509C57B971D8859C4E'
delete( fullfile( kernel.backsteppingKernel(1).savedKernelPath, [kernel.backsteppingKernel(1).hash '.mat']) )
fprintf("done\n")

end


% TODO uncomment
% function inputSignalsTest(testCase)
% 
% Nz = 51;
% [faultyCable, waveCable] = examples.thesis.cable.wave2heteroDirectionalSystem(Nz);
% 
% dt = 5e-3;
% t = quantity.EquidistantDomain("t", 0, 5, "stepSize", dt);
% 
% %% inputs:
% u = misc.unitSelector(2,1)* quantity.Discrete.ones([2,1], t) * 1e4;
% sim1 = faultyCable.discreteModel.simulate(t, "u", u);
% sim2 = waveCable.simulate("t", t.grid, "plant.u", u);
% testCase.verifyTrue( l2norm( sim1.plant.y - sim2.plant.y ) / l2norm( sim1.plant.y) < 0.2);
% 
% plot( sim1.plant.y, "figureId", 1, "hold", false);
% plot( sim2.plant.y, "figureId", 1, "hold", true);
% 
% 
% u = misc.unitSelector(2,2)* quantity.Discrete.ones([2,1], t) * 1e4;
% sim1 = faultyCable.discreteModel.simulate(t, "u", u);
% sim2 = waveCable.simulate("t", t.grid, "plant.u", u);
% testCase.verifyTrue( l2norm( sim1.plant.y - sim2.plant.y ) / l2norm( sim1.plant.y) < 0.3);
% 
% 
% 
% %% d1
% dTilde = quantity.Discrete.ones(1, t) * 20;
% sim2 = waveCable.simulate("t", t.grid, "plant.d.ode", dTilde);
% sim1 = faultyCable.discreteModel.simulate(t, "d_deterministic", dTilde);
% testCase.verifyTrue( l2norm( sim1.plant.y - sim2.plant.y ) / l2norm( sim1.plant.y) < 0.04);
% 
% %% dBar1
% dBar(1,1) = quantity.Discrete.ones(1, t) * 7;
% dBar(2,1) = quantity.Discrete.zeros(1, t);
% dBar(3,1) = quantity.Discrete.zeros(1, t);
% 
% sim1 = faultyCable.discreteModel.simulate(t, "d_bounded", dBar);
% sim2 = waveCable.simulate("t", t.grid, "plant.d.bounded", dBar);
% 
% testCase.verifyTrue( l2norm( sim1.plant.y - sim2.plant.y ) / l2norm( sim1.plant.y) < 0.04);
% 
% %% dBar2
% dBar(1,1) = quantity.Discrete.zeros(1, t);
% dBar(2,1) = quantity.Discrete.ones(1, t);
% dBar(3,1) = quantity.Discrete.zeros(1, t);
% 
% sim1 = faultyCable.discreteModel.simulate(t, "d_bounded", dBar);
% sim2 = waveCable.simulate("t", t.grid, "plant.d.bounded", dBar);
% 
% testCase.verifyTrue( l2norm( sim1.plant.y - sim2.plant.y ) / l2norm( sim1.plant.y) == 0);
% 
% 
% %% dBar3
% dBar(1,1) = quantity.Discrete.zeros(1, t);
% dBar(2,1) = quantity.Discrete.zeros(1, t);
% dBar(3,1) = quantity.Discrete.ones(1, t);
% 
% sim1 = faultyCable.discreteModel.simulate(t, "d_bounded", dBar);
% sim2 = waveCable.simulate("t", t.grid, "plant.d.bounded", dBar);
% 
% testCase.verifyTrue( l2norm( sim1.plant.y - sim2.plant.y ) / l2norm( sim1.plant.y) == 0);
% 
% %% f1
% f = quantity.Discrete.zeros([5,1], t);
% f(1,1) = quantity.Discrete.ones(1,t);
% sim1 = faultyCable.discreteModel.simulate(t, "f", f);
% sim2 = waveCable.simulate("t", t.grid, "plant.f", f); 
% testCase.verifyTrue( l2norm( sim1.plant.y - sim2.plant.y ) / l2norm( sim1.plant.y) < 0.03);
% 
% %% f2
% f = zeros(5,1) * quantity.Discrete.zeros(1, t);
% f(2,1) = quantity.Discrete.ones(1,t);
% sim2 = waveCable.simulate("t", t.grid, "plant.f", f);
% sim1 = faultyCable.discreteModel.simulate(t, "f", f);
% 
% testCase.verifyTrue( l2norm( sim1.plant.y - sim2.plant.y ) / l2norm( sim1.plant.y) < 0.2);
% 
% %% f3
% f = zeros(5,1) * quantity.Discrete.zeros(1, t);
% f(3,1) = quantity.Discrete.ones(1,t);
% sim2 = waveCable.simulate("t", t.grid, "plant.f", f);
% sim1 = faultyCable.discreteModel.simulate(t, "f", f);
% testCase.verifyTrue( l2norm( sim1.plant.y - sim2.plant.y ) / l2norm( sim1.plant.y) < 0.3);
% 
% %% f4
% f = zeros(5,1) * quantity.Discrete.zeros(1, t);
% f(4,1) = quantity.Discrete.ones(1,t);
% sim2 = waveCable.simulate("t", t.grid, "plant.f", f);
% sim1 = faultyCable.discreteModel.simulate(t, "f", f);
% testCase.verifyTrue( l2norm( sim1.plant.y - sim2.plant.y ) / l2norm( sim1.plant.y) == 0);
% 
% 
% %% f5
% f = zeros(5,1) * quantity.Discrete.zeros(1, t);
% f(5,1) = quantity.Discrete.ones(1,t);
% sim2 = waveCable.simulate("t", t.grid, "plant.f", f);
% sim1 = faultyCable.discreteModel.simulate(t, "f", f);
% testCase.verifyTrue( l2norm( sim1.plant.y - sim2.plant.y ) / l2norm( sim1.plant.y) == 0);
% 
% % %%
% % plot( sim1.plant.y, "figureId", 1, "hold", false);
% % plot( sim2.plant.y, "figureId", 1, "hold", true);
% % plot( sim1.plant.y - sim2.plant.y, "figureId", 2, "hold", false);
% 
% end
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
% 
