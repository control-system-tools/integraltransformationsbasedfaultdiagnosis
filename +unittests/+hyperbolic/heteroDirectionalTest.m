function [tests] = heteroDirectionalTest()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
tests = functiontests(localfunctions);
end

function setupOnce(testCase)

% setup an example system
spatialDomain = quantity.Domain('z', linspace(0, 1, 121));
Lambda = quantity.Symbolic(diag([2, -3]), spatialDomain, 'name', 'Lambda');		
faultySys = faultdiagnosis.hyperbolic.heteroDirectional.System(...
	Lambda, -4, ...
	'Hb0', 6, 'Eb0', 0, 'Q_0', 8, 'Q_1', 9, ...
	'E_3', 10);
dt = faultySys.systemOde.suggestStepSize(1e-2);
timeDomain = quantity.EquidistantDomain('t', 0, 15, 'stepSize', dt);

faultModel = signals.models.Constant (2, 'timeDomain', timeDomain, ...
		'occurrence', 0.5, 'SignalName', 'f');
	
testCase.TestData.faultySystem = faultySys;
testCase.TestData.faultModel = faultModel;
testCase.TestData.timeDomain = timeDomain;

end

function testSystemCreation(testCase)
% rebuild the system from the testCase

% The form for the faulty system is
%	x_z = Lambda x_t + A x + int_0^z D(z,zeta) x(zeta)
%			+ H_1(z) w + E_1(z) f(t)
%	w_t = F w + L_2 x_p(0) + E_4 f
%	x_n(0) = K_0 x_p(0) + H_2 w + E_2 f
%	x_p(1) = K_1 x_n(1) + E_3 f + u
%	
% The readformulation into the form of the implementation of
% dps.wave.System leads to
%	
%	x_t = Lambda^-1 x_z - Lambda^-1 A_f x ...
%			- int_0^z Lambda_f^-1(z) D(z,zeta) x(zeta) dz
%			- Lambda_f^-1 H_1 w 
%			- Lambda_f^-1 E_1 f
%	w_t = F w + L_2 J_p x(0) + E_4 f
%

F = testCase.TestData.faultySystem;
S = testCase.TestData.faultySystem.systemOde;
invL = inv( F.Lambda_1 );

testCase.verifyTrue( S.Lambda.near( invL ) );
testCase.verifyTrue( S.A.near( -invL * F.Lambda_0 ) );
testCase.verifyTrue( S.F.near( -invL * F.D) );
testCase.verifyTrue( S.ode2pde.B.near( -invL * F.H_1 ) );
testCase.verifyTrue( S.input.get('plant.f').B.near( -invL * F.E_1 ) );

testCase.verifyEqual( S.ode.A, F.F );
testCase.verifyEqual( S.pde2ode.C0, F.L_2)
% testCase.verifyEqual( S.input
end
