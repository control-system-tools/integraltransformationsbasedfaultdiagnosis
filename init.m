%clearvars('-except', 'testCase', 'S', 'cl')

% all figure should be docked:
set(0,'DefaultFigureWindowStyle','docked');

% provide the conI toolbox
addpath coni
S = quantity.Settings.instance();
S.plot.dock = true;
S.plot.showTitle = false;
S.plot.name2axis = true;
S.plot.focus = false;
S.plot.name2title = false;
S.plot.grabFocus = false;

% provide the hyperbolic simulation and backstepping files
addpath(genpath('./hyperbolic/'))

% provide the other dependencies:
addpath(genpath('external'))
% verify if the external files are loaded:
if isempty( which("three_point_centered_D1") )
	warning("INIT:The MatMol file three_point_centered_D1 is not loaded. You can download it from http://www.matmol.org/images/new_structure/Matmol_1.4.zip, and find it at `Matmol_1.4/PDE_methods/Linear_operators/Finite_Differences/FD_uniform_grids/. This file is required for the simulation of the hyperbolic systems.");
end

if isempty( which("chebfun") )
	warning("INIT: The chebfun toolbox is not loaded. You can get it from https://www.chebfun.org/download/. This toolbox is required for the simulation of hyperbolic systems. Please download it and add it to the matlab path");
end

% create a testCase for simpler tests of unittests
%testCase = matlab.unittest.TestCase.forInteractiveUse();

% set default properties for the current project:
prefs.loadData = false;

set(0,'defaultTextInterpreter','latex');

%  --- initialize the parallel pool ---
pc = parcluster('local');
pc.NumWorkers = 5;
p = gcp; % if there is no pool, it will be created. If there is a pool, nothing happens
clear p
clear pc

% disable some warnings:
warning("off", "symbolic:generate:IndefiniteIntegral")
warning("off", "Control:analysis:LsimStartTime")