# A Matlab toolbox for the fault diagnosis for distributed-parameter systems using integral transformations
This toolbox contains MATLAB code for the fault diagnosis of linear distributed-parameters systems (DPSs) on a one-dimensional spatial domain using integral transforms and trajectory planning methods. It provides a general implementation of the new approach to fault detection and diagnosis described in [1].
To generate simulation examples, the toolbox contains a finite-element method based approximation for general parabolic systems with spatially varying coefficients, a Galerkin approximation for the Euler-Bernoulli beam equation, and an approximation based on the spectral method for wave equations. These simulation environments can also be of interest in their own right. 
The integral transformation-based fault diagnosis method [1] uses results from the flatness-based trajectory planning methods for DPSs. Due to the object-oriented implementation for the computation of the corresponding differential expressions, this repository provides also the computation of trajectories based on flatness methods for parabolic, biharmonic and hyperbolic ODE-PDE systems.

The toolbox provides the scripts for the computation of the simulation examples from the thesis [1]. However, the object-oriented implementation of the proposed fault diagnosis method allows the interested reader to solve other fault diagnosis problems as well.

## Dependencies
### conI
The Matlab code of this repository relies heavily on the [conI](https://doi.org/10.5281/zenodo.4555225) toolbox. It is linked as a [submodule](https://git-scm.com/docs/git-submodule) in this repository. Thus, use the `--recurse-submodules` flag when you clone the repository to initialize the submodule. Make sure that the `conI` folder is provided in the `Matlab path`. 

### MatMol
An other dependency is the finite difference scheme `three_point_centered_D1` from [MatMol](http://www.matmol.org/). The latter can be downloaded from the [MatMol download page](http://www.matmol.org/images/new_structure/Matmol_1.4.zip). Make sure that the file `Matmol_1.4/PDE_methods/Linear_operators/Finite_Differences/FD_uniform_grids/three_point_centered_D1` is included in the `Matlab path`.

### chebfun
The toolbox [chebfun](https://www.chebfun.org/) is used for the simulation of wave equations. Thus, make sure that the path of the `chebfun toolbox Version 5.7.0 02-Jun-2017` is available in the `Matlab path`. The `chebfun` toolbox can be obtained from [Download Chebfun (.zip)](https://github.com/chebfun/chebfun/archive/master.zip).

## Toolbox structure
The toolbox uses the [Matlab packages feature](https://de.mathworks.com/help/matlab/matlab_oop/scoping-classes-with-packages.html) to create namespaces. These packages are explained below:
* `+examples` Simulation examples for the fault detection and fault diagnosis simulation examples in [1].
* `+faultdiagnosis` The classes and some scripts for the fault detection/diagnosis. Its subpackages are structured regarding the type of the considered PDE, which can be `+biharmonic`, `+parabolic` or `+hyperbolic`.
* `+unittests` Some tests.

In the toolbox folder `hyperbolic`, the classes for the simulation of hyperbolic systems and the backstepping transformation of hyperbolic systems are provided. This folder must be added to the `Matlab path`. 

To simplify the initialization of this toolbox, the `init.m` script is provided. It adds the folders `conI` and `hyperbolic` to the `Matlab path`, initializes the `Matlab Parallel Pool` and sets some default settings. It also checks if the required dependencies are present.

### Getting started
For a first orientation on the use of this toolbox I recommend to try the examples in the `+examples` package. 
Before that, however, all external toolboxes must be added to the `Matlab path`. Just download the [MatMol](http://www.matmol.org/) and [chebfun](https://www.chebfun.org/) toolbox from the given sources and add them to the `Matlab path`. 
Then, by executing the `init.m` script, Matlab will be prepared to run the scripts in the `+examples` package. 

### Coding Guidelines
The coding follows the same guidelines as the `conI` toolbox (see [the related wiki-page](https://gitlab.com/control-system-tools/coni/-/wikis/Coding-guidelines)).

## Questions & Contact  
In case of questions or problems with the dependencies, you are welcome to contact [Ferdinand Fischer](https://www.uni-ulm.de/en/in/institute-of-measurement-control-and-microtechnology/institute/staff/wissenschaftliche-mitarbeiter/fischer-ferdinand-m-sc/).

## Contributing
We look forward to your feedback, criticism and contributions. You are welcome to contact [Ferdinand Fischer](https://www.uni-ulm.de/en/in/institute-of-measurement-control-and-microtechnology/institute/staff/wissenschaftliche-mitarbeiter/fischer-ferdinand-m-sc/) and do not hesitate to make suggestions. We will review all contributions to ensure quality.

## Acknowledgement
I want to thank [Jakob Gabriel](https://www.uni-ulm.de/en/in/institute-of-measurement-control-and-microtechnology/institute/staff/wissenschaftliche-mitarbeiter/gabriel-jakob-m-sc/) for sharing his matlab code for the simulation and backstepping transformations for hyperbolic systems.

# License
GNU Lesser General Public License v3.0 (LGPL), see [License-file](https://gitlab.com/control-system-tools/coni/-/blob/master/COPYING.LESSER).

# References
[1] Fischer, Ferdinand (2022): Fault diagnosis for distributed-parameter systems using integral transformations and trajectory planning methods. Open Access Repositorium der Universität Ulm und Technischen Hochschule Ulm. Dissertation. http://dx.doi.org/10.18725/OPARU-42075
