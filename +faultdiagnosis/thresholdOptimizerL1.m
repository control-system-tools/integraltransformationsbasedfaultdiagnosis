function [xStar, JStar, J0] = thresholdOptimizerL1(obj, c, b, delta, optArgs)
% solves the optimization problem:
%	J(x*)  = min_{x* \in Range(Z)} int( | c + b x* | )^T * delta
%
arguments
	obj,
	c,
	b,
	delta (:,1) double,
	optArgs.repeatedOptimization = true;
	optArgs.maxRepetitions = 5;
	optArgs.use0asInitialPoint = false;
	optArgs.additionalObjective;
end

% the objective function:
%	int( | c + b x* | )^T * delta
% with optional computation of the gradient. (However, for fminsearch the gradient cannot be
% used...)
J = @(x) objectiveWithGradient(c, b, delta, x);

% zero initial point for the optimization:
o = zeros( size(b,2), 1 );
% compute the result for the zero initial point:
J0 = J(o);

if isfield(optArgs, "additionalObjective")
	J = @(x) J(x) + optArgs.additionalObjective(x);
end

% as an alternative to the zero-initial point use the solution of the quadratic programm, because it
% can be computed analytically:
% use min|| W*(c+b*gammaStar) ||_2^2 to gues an initial point:
W = diag(delta.^2);
Q = int( b' * W * b);
P = 2 * int( c' * W * b )';
gamma0star = - pinv(Q) * P;

%%
%% try fminsearch:
opts = optimset(@fminsearch);
if obj.verbose
	opts.Display = "iter";
% 	opts.MaxFunEvals = 200*size(b,2)
% 	opts.TolFun = 1e-3;
% 	opts.TolX = 1e2;
else
	opts.Display = "off";
end

obj.logMsg("Compute the (sub) optimal threshold. rb0 = %g \n", J0 );
% decide which initial point shuold be used for the optimization:
if J0 <= J(gamma0star) || optArgs.use0asInitialPoint
	initialPoint = o;
	obj.logMsg("Use zero as initial point\n");
else
	initialPoint = gamma0star;
	obj.logMsg("Use gamma_0* as initial point\n");
end

% do the optimization unless the optimal result has converged:
exitFlag = ~optArgs.repeatedOptimization;
i = 0;
while exitFlag == 0
	[xStar, JStar, exitFlag] = fminsearch( J, initialPoint, opts);
	i = i + 1;
	if exitFlag == 0
		initialPoint = xStar;
		obj.logMsg("Optimization has not been terminated. Last rB* = %g, Restart with the last value.\n", JStar)
		obj.logMsg("Optimization iteration %i \n", i);
	end	
	
	if i >= optArgs.maxRepetitions
		obj.report("Maximal number of repetitions reached");
		%obj.logMsg("Maximal number of repetitions reached");
		exitFlag = 2;
	end
end

%%
obj.logMsg(" rB0 = %g and rB* = %g \n", J0, JStar);
end

function [rB, gradRB] = objectiveWithGradient(c, b, delta, x)

y = c+b*x;

rB = int( delta' * abs( y ) );

if nargout > 1
	gradRB = int( ( delta .* sign(y) ).' * b );
end

end