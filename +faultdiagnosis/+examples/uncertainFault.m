init();
settings.export = false;

% definition of the system parameters:
z = quantity.Domain("z", linspace(0, 1, 511));
Z = quantity.Discrete(z.grid, z);
a = quantity.Symbolic(1, z, 'name', 'alpha');
dt = 0.001;

% system description
heatConductor = faultdiagnosis.parabolic.ScalarParabolic(z, 'alpha', a, ...
	'q00', 0, 'q01', 1, 'c2', 1, ...
	'q10', 1, 'q11', 0, 'b3', 1, 'e3', 1, 'g3', 1, ...
	'G_bounded', 1, ...
	'N_s', 12);
timeDomain = quantity.EquidistantDomain('t', 0, 10, 'stepSize', dt);
inputs.model.f(1) = signals.models.Sinus(5, 'time', timeDomain, ...
	'occurrence', 3, 'initialCondition', [2; 0], 'signalName', 'f1');

%% kernel equation
T = 1;
I = quantity.EquidistantDomain("t", 0, T, 'stepSize', dt);
% 
kernel = faultdiagnosis.parabolic.ScalarParabolic(z, 'alpha', a, ...
	'q01', 1, 'b2', 1, 'q10', 1, 'q11', 0, 'N_s', 11);
S = inputs.model.f.S;
rf = inputs.model.f.P;
modulatingFunction = kernel.basisTrajectoryPlanningODEPDEODECascade( I, rf.', S.', 'P_3', -rf.' * [1 0]);

modulatingFunction.x.plot('fig', 4)
gca; subplot(211); hold on;
plot3( 0*I.grid, I.grid, modulatingFunction.u.on(), 'r', 'LineWidth', 4)

filt.n = signals.fir.firstOrderHold(modulatingFunction.u, I, 'flip', false);
filt.m = signals.fir.firstOrderHold(modulatingFunction.x(1).subs("z", 1), I, 'flip', false);
filt.threshold = int( abs( int( modulatingFunction.x(2), z) ), I);

%% system simulation - identification

% ----- control -----
u_tmp = signals.RandomStairs( 'steps', 15, ...
 	'seedt', 1, 'seedf', 1, 'fmin', -15, 'fmax', 11, 'name', 'u');
inputs.u(1,1) = Discrete( u_tmp, timeDomain, 'smooth', true);
% ----- faults -----
inputs.f = inputs.model.f.simulate();

% ----- disturbance -----
inputs.d.bounds = 1;
% inputs.d.bounded = quantity.Discrete( signals.RandomStairs('steps', 13, 'seedt', 2, 'seedf', 2, ...
% 	'fmin', -inputs.d.bounds, 'fmax', inputs.d.bounds, 'name', 'd'), timeDomain, 'smooth', true );

inputs.d.bounded = quantity.Discrete( inputs.d.bounds * ...
	sign(cos( timeDomain.grid * 0.5 * 5)) , timeDomain);

%% Fault identification
identification = heatConductor.discreteModel.simulate(timeDomain, ...
	"u", inputs.u, "f", inputs.f, 'plot', true);

identification.f_hat = signals.fir.mFilter(filt.n, identification.y.on()) ...		
					- signals.fir.mFilter(filt.m, identification.u.on());
figure(2); clf;
[identification.parameters, ...
		identification.result] = faultdiagnosis.plotFault(1, timeDomain, inputs.model.f, ...
		identification.f_hat, 0, 'T', T );

identification.result.u = inputs.u.on();
identification.result.y = identification.y.on();
identification.result.d = inputs.d.bounded.on();
identification.result.f = inputs.f.on();
	
%% Fault estimation
%inputs.d.bounded = quantity.Discrete( inputs.d.bounds * ...
%	sign(cos( timeDomain.grid * 0.5 * 20)) , timeDomain);
inputs.d.bounded = Discrete( signals.RandomStairs( "steps", 55, "seedt", 2, "seedf", 2, ....
	"fmin", -1, "fmax", 1, "name", "d_{bounded}"), timeDomain );

modF = signals.models.Sinus(5, 'time', timeDomain, ...
	'occurrence', 3, 'initialCondition', [2; 0], 'signalName', 'f1');
inputs.f = modF.simulate();

detection = heatConductor.discreteModel.simulate(timeDomain, ...
	"u", inputs.u, "f", inputs.f + inputs.d.bounded, 'plot', true);

f_detection = signals.fir.mFilter(filt.n, detection.y.on()) ...
	- signals.fir.mFilter(filt.m, detection.u.on());

figure(5); clf; hold on;
[detection.parameters, ...
		detection.result] = faultdiagnosis.plotFault(1, timeDomain, inputs.model.f, ...
		f_detection, filt.threshold * inputs.d.bounds, 'T', T );
detection.result.y = detection.y.on();
plot( inputs.d.bounded + inputs.f, "currentFigure", true, "hold", true, "LineWidth", 2)
	

figure(6); clf; hold on;
subplot(321)
inputs.d.bounded.plot("currentFigure", true)
subplot(322)
inputs.f.plot("currentFigure", true)
subplot(3, 2, [3 4]);
plot( inputs.d.bounded + inputs.f, "currentFigure", true, "hold", true, "LineWidth", 2)

subplot(3, 2, [5 6])
[detection.parameters, ...
		detection.result] = faultdiagnosis.plotFault(1, timeDomain, inputs.model.f, ...
		f_detection, filt.threshold * inputs.d.bounds, 'T', T, "uncertainFault", inputs.f );


%% export
if settings.export
	dataPath = "/home/fischer/text/Oberseminar/talk";
	transform.dt = 0.02;
	transform.t = quantity.EquidistantDomain("t", 0, 5.5, "stepSize", transform.dt);
	transform.I = quantity.EquidistantDomain("t", 0, I.upper, "stepSize", transform.dt);
	transform.filter = fir.trapez(modulatingFunction.u, transform.I, "flip", false);
	transform.y = identification.y.subs("t", transform.t);
	transform.v = signals.mFilter(transform.filter, transform.y.on());

	figure(4); clf; hold on;
	subplot(211); hold on;
	plot(transform.y, "currentFigure", true);
	subplot(212); hold on;
	plot(transform.t.grid, transform.v)

	data.transformation = export.dd("M", [transform.t.grid, transform.y.on(), transform.v], "header", {'t', 'y', 'v'}, ...
		"N", 0, "foldername", "data", "filename", "transformation", "basepath", dataPath);
	data.kernel = export.dd("M", [I.grid, modulatingFunction.u.on(), on( modulatingFunction.x(1).subs("z", 1))], ...
		'header', {'tau', 'n', 'dzm1'}, "foldername", "data", "filename", "kernel", "basepath", dataPath);

	data.identification = export.dd("M", struct2array( identification.result ), ...
		'header', fieldnames(identification.result), ...
		"foldername", "data", "filename", "heatIdentification", "basepath", dataPath);

	data.detection = export.dd("M", struct2array( detection.result ), ...
		'header', fieldnames(detection.result), ....
		"foldername", "data", "filename", "heatDetection", "basepath", dataPath);


	export.Data.exportAll(data)
end	
	
	
	
	
	
	
	
	
	
	