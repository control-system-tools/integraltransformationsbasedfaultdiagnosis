clearvars;
init;

% system:
% dt x = A x + B u + E1 f + G1 d
%    y = C x + E2 f + G2 d

A = [-1 5; 0 -3];
B = [2; 2];
C = [3 1];
D = [];
E1 = [2; 2];
E2 = 0;
G1 = [1 0; 0 0];
G2 = [0 1];
Gb = eye(2);

dims.x = size(A,1);
dims.y = size(C,1);
dims.u = size(B,2);
dims.f = size(E1,2);
dims.d = size(G1,2);
dims.d_b = size(Gb, 2);

sys = ss(A, [B, E1, G1], C, [zeros(dims.y, dims.u), E2, G2]);

% estimation of sampling period for quasi-continuous design
T_A = 1 ./ eig(A);
T_min = min(abs(T_A));
dt = 0.25 * T_min; 

% time definitions
tEnd = 10;
t = quantity.Domain("t", 0:dt:tEnd);

% TODO: add the threshold for the disturbances
%% Fehlermodell
% drifting fault
faultModel = signals.models.Ramp( 0, -.5, "occurrence", 5, "timeDomain", t);

%% simulation
%Input u as a pseudo random stair signal
u = Discrete(signals.RandomStairs('steps', 7, 'seedt', -1, 'seedf', 1, 'fmin', -1, 'fmax', 1), t);
y = lsim(sys, on([u; faultModel.simulate]), t.grid, 'foh');

%% KERNEL EQUATION
% initialize the extended state-space system
extendedSys = ss(...
	                [A E1 * faultModel.P; ...
	                zeros(size(sys.A, 1), faultModel.n) faultModel.S], ...
	                -[B; zeros(faultModel.n, 1)], ...
	                [C  zeros(1, faultModel.n)], D);

% initialize the system for the kernel equation
kernel = ss(extendedSys.A', extendedSys.C', extendedSys.B', extendedSys.D);

%% Solve the transition for the kernel equation.

% initial condition
meT = [zeros(size(A, 1), 1); faultModel.P'];

% sampling period for the solution of the kernel equation ( dtau >> dt ) "quasi continuous"
dtau = 1e-2;
T = 2;
tau = quantity.EquidistantDomain("tau", 0, T, "stepSize", dtau);

% Bm = B^T * m
[n, Bm, m] = misc.ss.planTrajectory(kernel, tau, 'x0', -meT);


%% Calculating the filter coefficients with various approximation methods

fdFilter.tau = quantity.EquidistantDomain("tau", 0, T, "stepSize", dt);
fdFilter.n = signals.fir.firstOrderHold(n, fdFilter.tau, "flip", false);
fdFilter.mb = signals.fir.firstOrderHold(Bm, fdFilter.tau, "flip", false);

%% fault evaluation: f(t) = < n, y(t) > + < l, u(t) >
theta_foh = signals.fir.mFilter(fdFilter.n, y) ...
	+ signals.fir.mFilter(fdFilter.mb, u.on());


%%
% Diagnose
figure(2); clf; 
%plot(t,y)
plot(faultModel.simulate, 'fig', 2, 'LineStyle', ':','LineWidth', 2);
hold on;
plot(t.grid, theta_foh,'LineWidth',3);

hold off;
