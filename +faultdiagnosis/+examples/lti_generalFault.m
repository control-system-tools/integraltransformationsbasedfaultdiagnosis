clearvars;
init;

A = [-1 5; 0 -3];
B = [2; 2];
C = [3 1];
D = [];
E1 = [2; 2];

% P_f = 1;
% S_f = 0;

sys = ss(A, [B, E1], C, D);

% estimation of sampling period for quasi-continuous design
T_A = 1 ./ eig(A);
T_min = min(abs(T_A));
dt = 0.25 * T_min; 

% time definitions
tEnd = 10;
t = quantity.Domain("t", 0:dt:tEnd);

%% Fehlermodell
% drifting fault
faultModel = signals.models.Ramp( 0, -.5, "occurrence", 5, "timeDomain", t);

%% simulation
%Input u as a pseudo random stair signal
u = Discrete(signals.RandomStairs('steps', 7, 'seedt', -1, 'seedf', 1, 'fmin', -1, 'fmax', 1), t);
f = faultModel.simulate();
y = lsim(sys, on([u; f]), t.grid, 'foh');

%% KERNEL EQUATION
% initialize the system for the kernel equation
n = size(sys.A,1);
p = size(E1,2);
m = size(sys.C,1);
extendedKernel = ss([sys.A' zeros(n,p); E1' zeros(p,p)], [sys.C'; zeros(p,m)], [E1' zeros(p,p)], []);
kernel = ss(sys.A', sys.C', E1', []);

%% Solve a transition for the kernel equation.
% initial condition
qT = [zeros(n,1); 1];

% sampling period for the solution of the kernel equation ( dtau >> dt ) "quasi continuous"
dtau = 1e-2;
T = 2;
tau = quantity.EquidistantDomain("tau", 0, T, "stepSize", dtau);

% Bm = B^T * m
[n, ext.p, ext.m] = misc.ss.planTrajectory(extendedKernel, tau, 'x1', qT);
[p,~,m] = lsim(kernel, n.on(), tau.grid);
p = quantity.Discrete(p, tau);
m = quantity.Discrete(m, tau);
W = int(p' * p);
assert( W > 0 )
%% Calculating the filter coefficients with various approximation methods
fdFilter.tau = quantity.EquidistantDomain("tau", 0, T, "stepSize", dt);
fdFilter.n = signals.fir.firstOrderHold(n, fdFilter.tau, "flip", false);
fdFilter.mb = signals.fir.firstOrderHold(B.' * m, fdFilter.tau, "flip", false);

%% fault evaluation: f(t) = < n, y(t) > + < l, u(t) >
r = signals.fir.mFilter(fdFilter.n, y) ...
	+ signals.fir.mFilter(fdFilter.mb, u.on());
r = quantity.Discrete(r, t);

f_hat = p.at(T/2) / W * r;
figure(2);
plot(f, "currentFigure", true, "hold", true)
plot(f_hat, "currentFigure", true, "hold", true)
