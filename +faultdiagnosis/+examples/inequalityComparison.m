N = 1000;
n = 50;

delta = nan(N,1);

for i = 1:N

  x = (rand(n,1) - 0.5);
  y = (rand(n,1) - 0.5);
  
  x(5:10) = 0;

  cauchyDelta = sqrt( sum( x.^2 ) * sum( y.^2 ) ) - sqrt(( x'*y)^2);
  assert( cauchyDelta > 0)
  % 

  absDelta =  abs(x)' * abs(y) - abs( x' * y);
  assert( absDelta >= 0 )
  % 

  delta(i) = cauchyDelta - absDelta;
  
end

plot(delta, 'LineWidth', 2)
if any( delta < 0 )
  warning('Absolute delta smaller than cauchy delta!')
endif

fprintf('Delta for cauchy inequality = %g \n', mean( cauchyDelta ) );
fprintf('Delta for absolute value inequality = %g \n', mean( absDelta ) );