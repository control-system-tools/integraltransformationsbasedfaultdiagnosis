init()

t = quantity.EquidistantDomain("t", 0, 50, "stepSize", 1e-3);
d.model = signals.models.Sinus( pi * 10, "initialCondition", [0; 1.5], "timeDomain", t);
d.ode = d.model.simulate();

tau = quantity.EquidistantDomain("t", 0, .1, "stepSize", 1e-3);
W = quantity.Discrete.eye(1, tau);
w = signals.fir.midpoint( W, tau );
L2filteredDisturbance = signals.fir.mFilter(w, d.ode.^2) / tau.upper;
fprintf(" max( < d, d> ) = %g \n", max(L2filteredDisturbance));

figure(); clf;
subplot(211);
plot( d.ode, "currentFigure", true, "hold", true);

subplot(212);
plot( L2filteredDisturbance, "currentFigure", true, "hold", true);


tt = quantity.EquidistantDomain("t", 0, 50, "stepSize", 1e-3);
w = pi;
t = sym("t", "real");

d = quantity.Symbolic( cos( w * t ) * (1 + sin( w*t)), tt);
d.plot



