init();
settings.export = false;

% definition of the system parameters:
z = quantity.Domain("z", linspace(0, 1, 501));
Z = quantity.Discrete(z.grid, z);
a = quantity.Symbolic(1, z, 'name', 'alpha');
dt = 0.01;

e1Tmp = signals.RandomStairs( 'steps', 3, ...
 	'seedt', 1, 'seedf', 1, 'fmin', 0, 'fmax', 2, 'name', 'u');
e1 = Discrete( e1Tmp, z, 'smooth', true);
 
% 	'q00', 0, 'q01', 1, 'c2', [1; 0], ...
% 	'q10', 1, 'q11', 0, 'h3', -1, 'e3', 1, 'c3', [0; 1], ...

% system description
heatConductor = faultdiagnosis.parabolic.ScalarParabolic(z, 'alpha', a, ...
	'g1', a, ...
	'q00', 1, 'q01', 0, 'h2', -1, 'e2', [1 0], 'c2', [1; 0], ...
	'q10', 0, 'q11', 1, 'h3',  0, 'e3', [0 1], 'c3', [0; 1], ...
	'F', -10, 'b4', 1, ...
	'c4', [0; 0], ...
	'G_deterministic', 1, ...
	'G_bounded', 1, ...
	'N_s', 5);

timeDomain = quantity.EquidistantDomain('t', 0, 50, 'stepSize', dt);
%inputs.model.f(1) = signals.models.Sinus(5, 'time', timeDomain, ...
%	'occurrence', 3, 'initialCondition', [2; 0], 'SignalName', 'f1');
inputs.model.f(1) = signals.models.Polynomial(1, 1, "timeDomain", timeDomain, ...
	"occurrence", 3);
inputs.model.f(2) = signals.models.Polynomial(1, -1, "timeDomain", timeDomain, ...
	"occurrence", 3);
inputs.model.d(1) = signals.models.Sinus(5, 'time', timeDomain, ...
	'occurrence', 3, 'initialCondition', [0; 0], 'SignalName', 'f1');
%% kernel equation
T = 5;
I = quantity.EquidistantDomain("t", 0, T, 'stepSize', dt);
% 
kernel = faultdiagnosis.parabolic.Kernel(heatConductor, "N_s", 17, "disturbanceModel", inputs.model.d); % , "disturbanceModel", inputs.model.d

[detection.filter, detection.modulatingFunctions, detection.kernels] = kernel.computeDetectionFilter(I);

% [identification.filter, identification.modulatingFunctions, identification.kernels] = ...
%  	kernel.computeIdentificationFilter(I);

detection.kernels.M.plot('fig', 1)
gca; subplot(212); hold on;
plot3( 0*I.grid, I.grid, detection.kernels.N.on(), 'r', 'LineWidth', 4)

% identification.kernels.M.plot("fig", 2);
% gca; subplot(212); hold on;
% plot3( 0*I.grid, I.grid, identification.kernels.N.on(), 'r', 'LineWidth', 4)

 
%% system simulation - definition of the inputs (control input, fault and disturbance)
 % ----- control -----
u_tmp = signals.RandomStairs( 'steps', 15, ...
 	'seedt', 1, 'seedf', 1, 'fmin', -15, 'fmax', 11, 'name', 'u');
inputs.u(1,1) = Discrete( u_tmp, timeDomain, 'smooth', true);
% ----- faults -----
inputs.f = inputs.model.f.simulate();
 % ----- disturbance -----
inputs.d.bounds = 1;
% inputs.d.bounded = quantity.Discrete( signals.RandomStairs('steps', 13, 'seedt', 2, 'seedf', 2, ...
% 	'fmin', -inputs.d.bounds, 'fmax', inputs.d.bounds, 'name', 'd'), timeDomain, 'smooth', true );
% bang-bang disturbance:
inputs.d.bounded = quantity.Discrete( inputs.d.bounds * ...
	sign(cos( timeDomain.grid * 0.5 * 5)) , timeDomain);
 
%% Fault detection - without unknown but bounded disturbance
detection.sim = heatConductor.discreteModel.simulate(timeDomain, ...
	"u", inputs.u, "f", inputs.f, 'plot', true);
figure(3); clf; %subplot(313)
[tHat, fHat, detection.result] = ...
	kernel.faultDiagnosis( detection.filter, detection.sim.y, detection.sim.u, "f", inputs.f );
title("Fehlerdetektion ohne Störung")
% 	
% % isolation
% T = 10;
% I_iso = quantity.EquidistantDomain("t", 0, T, 'stepSize', dt);
% [isolation.filters, isolation.modulatingFunctions, isolation.kernels] = kernel.computeIsolationFilter(I_iso);
% 
% detection.sim = heatConductor.discreteModel.simulate(timeDomain, ...
% 	"u", inputs.u, "f", [1 0; 0 0] * inputs.f, 'plot', true);
% 
% figure(4); clf; %subplot(313)
% for k = 1:heatConductor.n.f
% subplot(2,1,k)
% [tHat(:,k), fHat(:,k), detection.result(k)] = ...
% 	kernel.faultDetection( isolation.filters(k), detection.sim.y, detection.sim.u, "f", detection.sim.f );
% end
% 
% detection.sim = heatConductor.discreteModel.simulate(timeDomain, ...
% 	"u", inputs.u, "f", [0 0; 0 1] * inputs.f, 'plot', true);
% figure(5); clf; %subplot(313)
% for k = 1:heatConductor.n.f
% subplot(2,1,k)
% [tHat(:,k), fHat(:,k), detection.result(k)] = ...
% 	kernel.faultDetection( isolation.filters(k), detection.sim.y, detection.sim.u, "f", detection.sim.f );
% end

% %% Fault detection - with unknown but bounded disturbance
% detection.simD = heatConductor.discreteModel.simulate(timeDomain, ...
% 	"u", inputs.u, "f", inputs.f, "d_bounded", inputs.d.bounded, 'plot', true);
% 
% figure(4); clf; %subplot(313)
% kernel.faultDetection( detection.filter, detection.simD.y, detection.simD.u, "f", inputs.f );
% title("Fehlerdetektion mit Störung")
% 
% %% fault identification - without unknown but bounded disturbance
% figure(5); clf;
% kernel.faultDetection( identification.filter, detection.sim.y, detection.sim.u, "f", inputs.f );
% title("Fehleridentifikation ohne Störung")
% 
% %% fault estimation - with unknown but bounded disturbance
% figure(6); clf;
% kernel.faultDetection( identification.filter, detection.simD.y, detection.simD.u, "f", inputs.f );
% title("Fehleridentifikation mit Störung")



































