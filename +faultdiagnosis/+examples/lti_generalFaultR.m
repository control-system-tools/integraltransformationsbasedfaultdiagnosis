init();

%% system
a = -1;
sys = ss(a, [1 1], 1, 0);

dt = 0.01;
t = quantity.Domain("t", 0:dt:5 );

u = Discrete( signals.RandomStairs(), t);
f = Discrete( signals.RandomStairs(), t);%quantity.Discrete( sin( t.grid * pi), t);
y = quantity.Discrete( lsim(sys, [u.on(), f.on()], t.grid), t);

%% kernel
tau = sym("tau", "real");
T = 1;
I = quantity.Domain("tau", 0:dt:T);
m = quantity.Symbolic( sqrt(2) * sin( tau * pi / T ), I);
n = diff(m, I) - a * m;
%m_ = quantity.Discrete( lsim( sys, n.on(), I.grid ), I);

%% filter
filt.n = signals.fir.firstOrderHold(n, I, "flip", false);
filt.m = signals.fir.firstOrderHold(m, I, "flip", false);

r = quantity.Discrete( signals.fir.mFilter(filt.n, y.on()), t) - quantity.Discrete( signals.fir.mFilter(filt.m, u.on()), t);
lhs = quantity.Discrete( signals.fir.mFilter(filt.m, f.on()), t);

assert( max(r-lhs) < 1e-3 ); % verify that lhs and rhs are equal.

W = int( m^2 );
F = m / W * r;

%% compute the rest term:
F2 = quantity.Symbolic( sin( pi * ( sym("t") - sym("tau") ) ), [t, I]);
R = F2 - F;
R.name = "R(t,\tau)";
R.plot("fig", 1);

%% 
figure(2); clf;
subplot(221);
plot(u, "currentFigure", true);
subplot(222);
plot(y, "currentFigure", true);
subplot(2,2,[3 4])
f2 = subs(F, "tau", 1/2);
plot( f2, "currentFigure", true );
plot( f, "currentFigure", true, "hold", true);
plot( quantity.Discrete( sin( ( t.grid - 0.5 ) * pi), t ), "currentFigure", true, "hold", true, "LineStyle", '--' );
legend("$\hat{f}(t-0.5)$", "$f(t)$", "$f(t-0.5)$", "interpreter", "latex");