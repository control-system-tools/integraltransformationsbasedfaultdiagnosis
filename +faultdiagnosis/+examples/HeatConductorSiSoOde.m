init();
settings.export = false;

% definition of the system parameters:
z = quantity.Domain("z", linspace(0, 1, 501));
Z = quantity.Discrete(z.grid, z);
a = quantity.Symbolic(1, z, 'name', 'alpha');
dt = 1e-3;

e1 = signals.UniformDistributedSignal(z, 'steps', 3, ...
 	'seedt', 1, 'seedf', 1, 'fmin', 0, 'fmax', 2, 'name', 'u', "filter", "gevrey");
 
% 	'q00', 0, 'q01', 1, 'c2', [1; 0], ...
% 	'q10', 1, 'q11', 0, 'h3', -1, 'e3', 1, 'c3', [0; 1], ...

% system description
heatConductor = faultdiagnosis.parabolic.ScalarParabolic(z, 'alpha', a, ...
	'g1', a, ...
	'q00', 0, 'q01', 1, 'h2', -1, 'e2', 0, 'c2', 0, ...
	'q10', 0, 'q11', 1, 'h3',  0, 'e3', 1, 'c3', 1, ...
	'F', -1, 'b4', 1, ...
	'c4', 0, ...
	'G_deterministic', 1, ...
	'G_bounded', 1);

%%
timeDomain = quantity.EquidistantDomain('t', 0, 50, 'stepSize', dt);
%inputs.model.f(1) = signals.models.Sinus(5, 'time', timeDomain, ...
%	'occurrence', 3, 'initialCondition', [2; 0], 'SignalName', 'f1');
inputs.model.f(1) = signals.models.Polynomial(1, "timeDomain", timeDomain, ...
	"occurrence", 3);
inputs.model.d(1) = signals.models.Sinus(3, 'time', timeDomain, ...
	'occurrence', 3, 'initialCondition', [1; 0], 'SignalName', 'f1');
inputs.dOde = inputs.model.d.simulate();

% ----- control -----
inputs.u(1,1) = signals.UniformDistributedSignal(timeDomain, 'steps', 15, ...
 	'seedt', 1, 'seedf', 1, 'fmin', -15, 'fmax', 11, 'name', 'u', "filter", "gevrey");
% ----- faults -----
inputs.f = inputs.model.f.simulate();
 % ----- disturbance -----
inputs.d.bounds = 1;
% inputs.d.bounded = quantity.Discrete( signals.RandomStairs('steps', 13, 'seedt', 2, 'seedf', 2, ...
% 	'fmin', -inputs.d.bounds, 'fmax', inputs.d.bounds, 'name', 'd'), timeDomain, 'smooth', true );
% bang-bang disturbance:
inputs.d.bounded = quantity.Discrete( inputs.d.bounds * ...
	sign(cos( timeDomain.grid * 0.5 * 5)) , timeDomain);

%% kernel equation
T = 10;
I = quantity.EquidistantDomain("t", 0, T, 'stepSize', dt);
% 
kernel = faultdiagnosis.parabolic.DetectionKernel(heatConductor, "N_s", 17, ...
	"disturbanceModel", inputs.model.d, "delta", inputs.d.bounds, "truncate", true);

% 1) define the reference trajectory:
% 1.1) define the gevrey function:
theta = signals.GevreyFunction( 'order', 1.999, "diffShift", 1, "numDiff", kernel.N_s, ...
				'timeDomain', I, "g", @signals.gevrey.bump.g, "name", "theta");
[refBasicVar, ~, ~, mBarf] = kernel.referenceTrajectoryPlanningStationaryGain(I, "Mf", ...
		1, "theta", theta, "extendedDOF", 0);

% 2) solve the kernel equations and compute the fir filters
% [ioIntegralKernels, solution, relInc, maxRelInc, iter] = ...
% 	detection.kernel.compute(I, "basicVariable", refBasicVar);
[ioIntegralKernels, relInc, numIter] = kernel.computeIOkernels(refBasicVar);
refTrajectory = kernel.referenceTrajectory( refBasicVar );

integralKernels.M = refTrajectory.x;
integralKernels.M.setName("m");
integralKernels.N = refTrajectory.u;
integralKernels.N.setName("n");
	
% 3) discretization of the integral terms in the fault diagnosis equation:
kernel.report("Discretize the integral terms and generate FIR filters.")
firFilter = kernel.generateFirFilters(I, ioIntegralKernels, "type", "detection", ...
	"discretizationFunction", @signals.fir.firstOrderHold);

integralKernels.M.plot('fig', 1)
gca; subplot(212); hold on;
plot3( 1 + 0*I.grid, I.grid, -integralKernels.N.on(), 'r', 'LineWidth', 4)
 
%% Fault detection - without unknown but bounded disturbance

% uStep = quantity.Discrete(ones( timeDomain.n, 1), timeDomain );

detection.sim = heatConductor.discreteModel.simulate(timeDomain, ...
	"u", inputs.u, "f", inputs.f * 0, "d_deterministic", inputs.dOde, 'plot', true);

occurrences = [inputs.model.f.occurrence];

% fault detection - without disturbance:
[detection.sim.r, detection.sim.result] = ...
	kernel.firFaultDiagnosis( firFilter, ...
		detection.sim.y, ...
		detection.sim.u, ...
		"f", detection.sim.f, ...
		"occurrence", occurrences);
	
kernel.plotResult(detection.sim.r, detection.sim.result, "detection", ...
		I.upper, "occurrence", occurrences);
% % isolation
% T = 10;
% I_iso = quantity.EquidistantDomain("t", 0, T, 'stepSize', dt);
% [isolation.filters, isolation.modulatingFunctions, isolation.kernels] = kernel.computeIsolationFilter(I_iso);
% 
% detection.sim = heatConductor.discreteModel.simulate(timeDomain, ...
% 	"u", inputs.u, "f", [1 0; 0 0] * inputs.f, 'plot', true);
% 
% figure(4); clf; %subplot(313)
% for k = 1:heatConductor.n.f
% subplot(2,1,k)
% [tHat(:,k), fHat(:,k), detection.result(k)] = ...
% 	kernel.faultDetection( isolation.filters(k), detection.sim.y, detection.sim.u, "f", detection.sim.f );
% end
% 
% detection.sim = heatConductor.discreteModel.simulate(timeDomain, ...
% 	"u", inputs.u, "f", [0 0; 0 1] * inputs.f, 'plot', true);
% figure(5); clf; %subplot(313)
% for k = 1:heatConductor.n.f
% subplot(2,1,k)
% [tHat(:,k), fHat(:,k), detection.result(k)] = ...
% 	kernel.faultDetection( isolation.filters(k), detection.sim.y, detection.sim.u, "f", detection.sim.f );
% end

% %% Fault detection - with unknown but bounded disturbance
% detection.simD = heatConductor.discreteModel.simulate(timeDomain, ...
% 	"u", inputs.u, "f", inputs.f, "d_bounded", inputs.d.bounded, 'plot', true);
% 
% figure(4); clf; %subplot(313)
% kernel.faultDetection( detection.filter, detection.simD.y, detection.simD.u, "f", inputs.f );
% title("Fehlerdetektion mit Störung")
% 
% %% fault identification - without unknown but bounded disturbance
% figure(5); clf;
% kernel.faultDetection( identification.filter, detection.sim.y, detection.sim.u, "f", inputs.f );
% title("Fehleridentifikation ohne Störung")
% 
% %% fault estimation - with unknown but bounded disturbance
% figure(6); clf;
% kernel.faultDetection( identification.filter, detection.simD.y, detection.simD.u, "f", inputs.f );
% title("Fehleridentifikation mit Störung")



































