syms s z zeta

K = [ 0 1 0 0; 0 0 0 1];
KTperp = eye(4) - K' * pinv(K');
KTperp = orth(KTperp)' * KTperp;

mA_star = [0 -1; s 0];
Phi = simplify( mA_star * (z - zeta) );
Phi0 = limit(Phi, s, 0);

C0 = KTperp(:, [1 2]);
C1 = KTperp(:, [3 4]);

Q0 = C0 + C1 * subs(Phi0, {z, zeta}, {1, 0});

adjQ0 = misc.adj(Q0);


%%
c = sin(2 * pi * z);
C(z) = [0; c];

Psi = - int( Phi * subs(C(z), z, zeta), zeta, 0, z);
Psi0  = limit(Psi, s, 0);

R0 = C1 * subs(Psi0, {z, zeta}, {1 0});

W0 = simplify( -subs(Phi0, zeta, 0) * adjQ0 * R0 + subs(Psi0, zeta, 0) * det(Q0) );

w0 = quantity.Symbolic(W0, 'variable', 'z', 'grid', linspace(0,1,501)', 'name', 'w0');
w2 = quantity.Symbolic(1, 'variable', 'z', 'grid', w0(1).grid, 'name', 'w2');

v01 = w0(1);
v02 = w2 - int(w2 * v01, 0, 1) / int(v01 * v01, 0, 1) * v01;

assert( int(v01 * v02, 0, 1) < 1e-12);

%% use c(z) = v01 and e(z) = v02, then the fault can not be detected!

% definition of the system parameters:
z = v02.grid{1};
a = quantity.Symbolic(1, 'variable', 'z', 'grid', z, 'name', 'alpha');
b = quantity.Symbolic(sin(3 * pi * sym('z')), 'variable', 'z', 'grid', z, 'name', 'alpha');
e = quantity.Symbolic(sin(5 * pi * sym('z')), 'variable', 'z', 'grid', z, 'name', 'alpha');
c = quantity.Symbolic(c, 'variable', 'z', 'grid', z, 'name', 'alpha');

% system description
sys = faultdiagnosis.parabolic.System(z, ...
	'alpha', quantity.Discrete(a), ...
	'b', b, 'e', e, ...
	'q00', 1, 'q01', 0, ...
	'q10', 1, 'q11', 0, ...
	'c10', c);

b0 = signals.GevreyFunction( 'order', 1.99,...
	'N_t', 501, 'T', 1, 'diffShift', 1);

%% Time
dt = 1e-3;
tend = 1;
t = (0:dt:tend)';

b1 = signals.GevreyFunction( 'order', 1.99,...
	'N_t', 501, 'T', 1, 'diffShift', 0);

[w, u, t] = sys.setPointChange('b', b1, 'N', 11);
[y, t, wsim] = sys.simulate(t, 'u', u.on(), 'plot', true);


% figure(1);
% subplot(211), misc.isurf(wsim')
% subplot(212), misc.isurf(w(sys.valueIdx(1)).on())
% numeric.near(wsim', w(sys.valueIdx(1)).on(), 1e-3)

%% definition of the faults and the disturbances:
f = signals.faultmodels.TaylorPolynomial('coefficients', [1], ...
	'occurrence', 0, ...
	'localGrid', b0.grid{1}, ...
	'globalGrid', t, ...
	'T', b0.T, ...
	'index', 1);

%%
kernl = faultdiagnosis.Kernel(sys, 'faults', f, 'basicVariable', b0, 'N_s', 15);

U = signals.RandomStairs('steps', 15, 'seedt', -1, 'seedf', 1, 'dim', 1);
u = U.smoothSignal(t) * 0;

[y, t, x] = sys.simulate(t, 'u', u, 'f', f.on(t), 'plot', true );

%%
% kernl.computeKernel()
fir = kernl.computeFir('dt', b0.dt);

%% fault detection:
[theta, tt] = faultdiagnosis.Kernel.faultDetection(fir, t, y, u);

% plotten.
figure(2), clf
subplot(3,1,1);
plot(t, u); hold on;
plot(t, f.on(t));
legend('u', 'f_1', 'f_1', 'd');

subplot(3,1,2);
plot(t, y); hold on;
legend('y_1', 'y_2')

subplot(3,1,3), hold on
plot(t, f.on(t), '--')
plot(tt, theta)







