%% Example for the visualization of the transformations.

t = quantity.EquidistantDomain("t", 0, 4, "stepSize", 0.01);
y = signals.UniformDistributedSignal(t, 'steps', 8, 'seedt', 2, 'seedf', 1, 'fmin', 0, 'fmax', 15, ...
	'name', 'y', "filter", "gevrey");

tau = quantity.EquidistantDomain("tau", 0, 1, "stepSize", 0.01);
n = signals.GevreyFunction("timeDomain", tau, "diffShift", 1).fun * quantity.Discrete( sin( 2 * pi * tau.grid), tau);
n.name = "n";

tauT = 3.8;
tau2 = quantity.EquidistantDomain("tau", tauT - tau.upper, tauT, "stepSize", 0.01);
n2 = n.replaceGrid(tau2);

figure(1); clf; hold on;
subplot(211)
y.plot("figureId", -1, "smash", true,"hold", true);
n2.plot("figureId", -1, "smash", true, "hold", true);
xlim([t.lower, t.upper])

o = signals.fir.mFilter( signals.fir.midpoint( n, tau), y);
o.setName("r");
subplot(212);
o.plot("figureId", -1, "smash", true, "hold", true);

%% export
dataPath = "/home/fischer/text/diss";
data.y = export.catQuantities({y, o}, "foldername", "data", "filename", "y", "basepath", dataPath);
data.n = exportData(n2, "foldername", "data", "filename", "n", "basepath", dataPath);
export.Data.exportAll(data)

%% data for the visualization of the approximation of the integral expressions
tStar = quantity.EquidistantDomain("tau", 2.5, 2.5 + tau.upper, "stepSize", tau.stepSize);

yStar = y.subs("t", tStar).flipDomain("tau").replaceGrid(tau);

integrand = yStar * n;
integrand.setName("yn");
data.integrand = integrand.exportData("foldername", "data", "filename", "integrand", "basepath", dataPath);

tauK = quantity.EquidistantDomain("tau", 0.1

data.integrand.export
