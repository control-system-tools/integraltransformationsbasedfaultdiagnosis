syms t
T = 1; 
si = 1;
I1 = quantity.Domain("t", linspace(0,1));
f = exp(-(1.0./T.^2.*(T.*t-t.^2)).^(-si));
g1 = quantity.Symbolic(f, I1);

T = 3;
I2 = quantity.Domain("t", linspace(0,T));
f = exp(-(1.0./T.^2.*(T.*t-t.^2)).^(-si));
g2 = quantity.Symbolic(f, I2);
g2 = g2 / int(g2, I2);

G = int(g1, I1) * T;

figure(1), clf;
plot(g2);
hold on;
plot( I2.grid, g1.on() / G, '--')

for k = 0:5
	figure(k+1); clf
	plot( g2.diff(I2, k), "figureId", k+1, "hold", true)
	hold on
	%plot(I2.grid,  g1.diff(I1, k).on() ./ (T^(k+1)) , "--")
	plot(I2.grid,  g1.diff(I1, k).on() ./ (T^k) / G , "--")
end

%% Abhängigkeit von T:
T = sym("T", "real");
Tupper = 4;
I1 = quantity.Domain("t", linspace(0,Tupper));
domT = quantity.Domain("T", linspace(1,Tupper));

f = @(t,T) exp(-(1.0./T.^2.*(T.*t-t.^2)).^(-si)) .* ( t < T);
g1 = quantity.Discrete(	quantity.Function(f, [I1, domT]) ) / int(g1, I1);
g1.plot;

plot( g1.diff(domT, 1) )
plot( g1.int(I1) )

%% Gevrey-Funktion ohne Skalierung:
I = quantity.Domain("t", linspace(0,5));
g = signals.GevreyFunction("order", 2, "timeDomain", I, "diffShift", 1, "g", @signals.gevrey.bump.g2);
T = I.upper;
si = g.sigma;
t = sym("t");
theta = quantity.Symbolic(exp(-(1.0./T.^2.*(T.*t-t.^2)).^(-si)), I);

for k = 0:3
	figure(k+1); clf;
	plot( g.diff(I, k), "figureId", k+1, "hold", true)
	plot( theta.diff(I, k), "figureId", k+1, "hold", true, "LineStyle", "--")
end

