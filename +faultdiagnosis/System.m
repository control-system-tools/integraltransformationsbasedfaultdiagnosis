classdef System < handle & misc.Reporter
	%% SYSTEM A generic description for distributed parameter systems
	%
	%   Special form for distributed parameter systems as generic description
	%   for fault diagnosis based on modulating functions.
	%
	%   The system is described by the PDE
	%       dzx(z,t) = A[x(t)](z) + H_1(z) w(t) + B_1(z) u(t) +  G_1(z) d(t) + E_1(z) f(t)
	%
	%   the boundary conditions
	%		K_0 x(0) + K_1 x(1) + H_2 w(t) = B_2 u(t) + E_2 f(t) + G_2 d(t)
	%
	%	and the coupled ODE
	%		dt w(t) = F w(t) + int_0^1 L_1 x(z,t) dz + L_2 x(0,t) + L_3 x(1,t) + B_3 u(t) ...
	%					+ E_3 f(t) + G_3 d(t)	
	%   the output operator C
	%      y(t)  = int_0_1 C_1(z) x(z,t) dz + C_2 x(0,t) + C_3 x(1,t) + C_4 w(t) + B_4 u(t) ...
	%				+ E_4 f(t) + G_4 d(t)
	%
	%	The disturbances are assumed to be separated into:
	%		d(t) = G_deterministic d_deterministic(t) + G_bounded d_bounded(t) ...
	%				+ G_stochastic d_stochastic(t)
	%
	%   System definitions:
	%       (z,t) \in \Omega \times \R^+
	%       spatial domain: \Omega = (0, 1)
	%       system variable: x(z,t) \in \R^n_x
	%       measurements:    y(t)   \in \R^n_y
	%		faults:			 f(t)   \in \R^n_f
	%		disturbances:    d(t)   \in \R^n_d
	%				d_deterministic \in \R^n_d_deterministic
	%				      d_bounded \in \R^n_d_bounded
	%			       d_stochastic \in \R^n_d_stochastic
	%
	%   The parameter names are as far as possible consistent with the thesis [ ... ]
	%
	%   example:
	%   ---------------------
	%   TODO
	%   ---------------------
	
	% Public Porperties
	properties (Access = public)
		fnumber = 1;	% figure number to plot some results
		recompute (1,1) logical = false;
		name (1,1) string = ""
	end
	% REMARK: These properties are set to be transient because these lead to errors with the use of
	% parfoor-loops. Using objects in parfor-loops, they must be saved. Thus the getter for this
	% objects is called. If they are not set, this will lead to problems. If they are not required
	% for the use in hyperbolic systems, they are set to be transient. If this makes problems for
	% the computation of parabolic or biharmonic systems, a different solution must be found. A
	% simple workaround would be to initialize the differentialExpressions and transitionMatrices
	% from the beginning, so that they must not be created 
	properties(GetAccess = public, SetAccess = public,  Hidden) 
		%% parameters for the motion planning
		differentialExpressions struct = struct();
		% State transition matrix to the system operator
		transitionMatrices;
	end
	
	properties(GetAccess = public, SetAccess = protected) 
		%% PDE system parameters:
		% System operator matrix 
		%    A = [A0, A1, A2, ... ]
		% with A0, A1, A2, ... for the representation of the operator
		%    A[h] = A0 + A1 dt h + A2 dt^2 h + ...
		A signals.PolynomialOperator;
		% Boundary matrices
		K_0 double;
		K_1 double;
		
		%% ODE parameters
		% system matrix for the ODE
		F (:,:) double;
		% ODE to PDE coupling matrices
		H_1 (:,:) quantity.Discrete;
		H_2 (:,:) double;
		% PDE to ODE coupling matrices
		L_1 (:,:) quantity.Discrete;
		L_2 (:,:) double;
		L_3 (:,:) double;
		%% output
		% Output operator as quantity of dimension (n_y, n_x)
		C_1 (:,:) quantity.Discrete;
		% Output matrix for measurements at the left boundary (n_y, n_x) 
		C_2 (:,:) double;
		% Output matrix for the right boundary measurements, size = (n_y, n_x)
		C_3 (:,:) double;		
		% Output matrix for the ODE
		C_4 (:,:) double;
		
		%% control inputs
		B_1 (:,:) quantity.Discrete;
		B_2 (:,:) double;
		B_3 (:,:) double;
		B_4 (:,:) double;
		
		%% fault inputs
		E_1 (:,:) quantity.Discrete;
		E_2 (:,:) double;
		E_3 (:,:) double;		
		E_4 (:,:) double;
				
		%% Disturbance input 
		G_1 (:,:) quantity.Discrete;
		G_2 (:,:) double;
		G_3 (:,:) double;
		G_4 (:,:) double;
					
		% disturbance matrices:
		G_deterministic double;
		G_bounded double;
		G_stochastic double;
		
		% Spatial domain of the DPS Omega 
		spatialDomain quantity.Domain;
		
		% Symbolic variable for the spatial coordinate
		z sym = sym("z", "real");
		
		% Symbolic variable for the representation of the operator form
		s sym = sym('s');
		
		% dimensions for the system quantities as structure;
		n struct = struct('x', []);
		
		% selection matrices
		J_0 double;
		J_1 double;
		
		%% discretization
		% object for the finit dimensional model of the DPS
		discreteModel
		
		% Number of derivatives for the trajectory planning
		N_s (1,1) double = 5;
	end
	
	methods
		
		%% Constructor
		function obj = System(spatialDomain, A, K_0, K_1, varargin)
			arguments
				spatialDomain (1,1) quantity.Domain;
				A signals.PolynomialOperator;
				K_0 double;
				K_1 double;
			end
			arguments (Repeating)
				varargin
			end

			if nargin == 0 || isempty(A) || isempty(spatialDomain)
				return;
			end
			
			obj.spatialDomain = spatialDomain;
			obj.A = A;
			obj.K_0 = K_0;
			obj.K_1 = K_1;
					
			%% determine the dimension of each different input 
			% so that it is possible to set the right dimensions in the main parser
			obj.n.x = size(obj.A(1).coefficient, 1);		
			
			optArgs = struct(varargin{:});
			if isfield(optArgs, "F")
				obj.n.w = size(optArgs.F,1);
			else
				obj.n.w = 0;
			end
			
			obj.n.u = misc.getDimensions(optArgs, ["B_1", "B_2", "B_3", "B_4"], 2);
			obj.n.f = misc.getDimensions(optArgs, ["E_1", "E_2", "E_3", "E_4"], 2);
			obj.n.d = misc.getDimensions(optArgs, ["G_1", "G_2", "G_3", "G_4"], 2);			
			obj.n.y = misc.getDimensions(optArgs, ["C_1", "C_2", "C_3"], 1);
			obj.n.w = max(misc.getDimensions(optArgs, ["H_1", "H_2", "C_4"], 2), ...
				misc.getDimensions(optArgs, ["L_1", "L_2", "L_3"], 1));
							
			if obj.n.w > 0
				assert( isfield( optArgs, "F" ) )
			end
			
			n = obj.n;
			ipp = misc.Parser();
			
			% check the control inputs
			ipp.addParameter('B_1', quantity.Discrete.zeros( [n.x, n.u], spatialDomain ), ...
				@(a) validateattributes(a, {'quantity.Discrete'}, {'size', [n.x, n.u]}));
			ipp.addParameter('B_2', zeros( n.x, n.u ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.x, n.u]}));
			ipp.addParameter('B_3', zeros( n.w, n.u ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.w, n.u]}));
			ipp.addParameter('B_4', zeros( n.y, n.u ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.y, n.u]}));
			
			% check the faulty inputs
			ipp.addParameter('E_1', quantity.Discrete.zeros( [n.x, n.f], spatialDomain ), ...
				@(a) validateattributes(a, {'quantity.Discrete'}, {'size', [n.x, n.f]}));
			ipp.addParameter('E_2', zeros( n.x, n.f ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.x, n.f]}));
			ipp.addParameter('E_3', zeros( n.w, n.f ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.w, n.f]}));
			ipp.addParameter('E_4', zeros( n.y, n.f ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.y, n.f]}));
			
			% check the disturbance inputs
			ipp.addParameter('G_1', quantity.Discrete.zeros( [n.x, n.d], spatialDomain ), ...
				@(a) validateattributes(a, {'quantity.Discrete'}, {'size', [n.x, n.d]}));
			ipp.addParameter('G_2', zeros( n.x, n.d ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.x, n.d]}));
			ipp.addParameter('G_3', zeros( n.w, n.d ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.w, n.d]}));
			ipp.addParameter('G_4', zeros( n.y, n.d ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.y, n.d]}));		
			ipp.addParameter('G_deterministic', zeros(n.d, 0), ...
				@(A) validateattributes(A, {'numeric'}, {'nrows', n.d} ));
			ipp.addParameter('G_bounded', zeros(n.d, 0), ...
				@(A) validateattributes(A, {'numeric'}, {'nrows', n.d} ));
			ipp.addParameter('G_stochastic', zeros(n.d, 0), ...
				@(A) validateattributes(A, {'numeric'}, {'nrows', n.d} ));
			
			% ODE
			ipp.addParameter('F', []);
			
			% ODE to PDE coupling matrices
			ipp.addParameter('H_1', quantity.Discrete.zeros( [n.x, n.w], spatialDomain ), ...
				@(a) validateattributes(a, {'quantity.Discrete'}, {'size', [n.x, n.w]}));
			ipp.addParameter('H_2', zeros( n.x, n.w ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.x, n.w]}));
			
			% PDE to ODE coupling matrices
			ipp.addParameter('L_1', quantity.Discrete.zeros( [n.w, n.x], spatialDomain ), ...
				@(a) validateattributes(a, {'quantity.Discrete'}, {'size', [n.w, n.x]}));
			ipp.addParameter('L_2', zeros( n.w, n.x ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.w, n.x]}));
			ipp.addParameter('L_3', zeros( n.w, n.x ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.w, n.x]}));
			
			% outputs
			ipp.addParameter('C_1', quantity.Discrete.zeros( [n.y, n.x], spatialDomain ), ...
				@(a) validateattributes(a, {'quantity.Discrete'}, {'size', [n.y, n.x]}));
			ipp.addParameter('C_2', zeros( n.y, n.x ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.y, n.x]}));
			ipp.addParameter('C_3', zeros( n.y, n.x ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.y, n.x]}));
			ipp.addParameter('C_4', zeros( n.y, n.w ), ...
				@(a) validateattributes(a, {'numeric'}, {'size', [n.y, n.w]}));
			
			% selection matrices
			ipp.addParameter('J_0', eye( n.x ) );
			ipp.addParameter('J_1', eye( n.x) );
			
			ipp.addParameter('N_s', 5);
			ipp.addParameter('name', "");
			
			ipp.parse(varargin{:});

			% verify that the selection matrices are unitary
			assert( numeric.near(ipp.Results.J_0.' * ipp.Results.J_0, eye( n.x ) ));
			assert( numeric.near(ipp.Results.J_1.' * ipp.Results.J_1, eye( n.x ) ));
			
			for par = fieldnames(ipp.Results)'
				obj.(par{1}) = ipp.Results.(par{1});
			end
			ipp.unmatchedWarning();
			
			% set the remaining dimensions of the disturbances parts			
			obj.n.d_deterministic = size( obj.G_deterministic, 2);
			obj.n.d_bounded = size( obj.G_bounded, 2);
			obj.n.d_stochastic = size( obj.G_stochastic, 2);
						
			assert( size( obj.G_deterministic, 1) == n.d)
			assert( size( obj.G_bounded, 1) == n.d)
			assert( size( obj.G_stochastic, 1) == n.d)
			
			
			% todo: asserts to check the size of each matrix:
% 			assert(all(size(obj.G_1) == [n.x, n.d]));
% 			assert(all(size(obj.G_2) == [obj.n, obj.q]));
% 			assert(all(size(obj.B_1(1).coefficient) == [obj.n, obj.p]));
% 			assert(all(size(obj.E_1) == [obj.n, obj.r]));
% 			assert(all(size(obj.E_2) == [obj.n, obj.r]));
% 			assert(all(size(obj.E_3) == [obj.m, obj.r]));
		end
		
		function d = get.discreteModel(obj)
			if isempty(obj.discreteModel)
				% the discreteModel will be set to the property in the
				% obj.creatediscreteModel function!
				obj.discreteModel = obj.createDiscreteModel();
			end
			d = obj.discreteModel;
		end		
		
		function tr = computeTransitionMatrices(obj)
			[Phi, F0] = obj.A.stateTransitionMatrix('N', obj.N_s);
			tr.Phi = Phi;
			tr.Psi_B = obj.A.inputTransitionMatrix(obj.B_1, F0, 'N', obj.N_s);
			if obj.n.w > 0
				tr.Psi_H = obj.A.inputTransitionMatrix(obj.H_1, F0, 'N', obj.N_s);
			else
				tr.Psi_H = quantity.Discrete.empty( [obj.n.x, obj.n.w] );
			end
			tr.F0 = F0;
		end
				
		function setTransitionMatrices(obj)
			[Phi, F0] = obj.A.stateTransitionMatrix('N', obj.N_s);
			transMatrices.Phi = Phi;
			transMatrices.Psi_B = obj.A.inputTransitionMatrix(obj.B_1, F0, 'N', obj.N_s);
			if obj.n.w > 0
				transMatrices.Psi_H = obj.A.inputTransitionMatrix(obj.H_1, F0, 'N', obj.N_s);
			else
				transMatrices.Psi_H = quantity.Discrete.empty( [obj.n.x, obj.n.w] );
			end
			transMatrices.F0 = F0;
			
			obj.transitionMatrices = transMatrices;
		end
		
		function setDifferentialExpressions(obj)
			obj.differentialExpressions = obj.computeDifferentialParametrization();
		end
		
		function d = get.differentialExpressions(obj)
			if isempty(obj.differentialExpressions) || isempty( fields( obj.differentialExpressions ) )
				obj.setDifferentialExpressions();
			end
			d = obj.differentialExpressions;
		end
		
	end
	
	methods (Access = public)
		
% 		function trajectory = referenceTrajectoryPlanningODEPDEODECascade(obj, I, v0, S, optArgs)
% 			%% TRANSITION compute the transition from an initial state to an end state
% 			% trajectory = basisTrajectoryPlanningODEPDEODECascade(obj, I, S, optArgs) computes a
% 			% transition for the cascade system of
% 			%
% 			%	dz x = A[x] + H_1 w + B_1 u
% 			%	K0 x(0) + K1 x(1) + H_2 w = B_2 u
% 			%	dt w = F w + < L_1, x > + L_2 x(0) + L_3 x(1) + B_3 u
% 			%
% 			% and
% 			%	
% 			%	dt v = S v + < P_1, x > + P_2 x(0) + P_3 x(1) + P_4 w + B_4 u
% 			%
% 			% The transition is restricted to:
% 			%
% 			%	x(z,0) = 0  --->  x(z,T) = 0
% 			%     w(0) = 0  --->    w(T) = 0
% 			%	  v(0) = v0 --->    v(T) = 0
% 			%
% 			% todo: describe the input parameters
% 			% todo: give the method a speaking name
% 			arguments
% 				obj;
% 				I (1,1) quantity.Domain;
% 				v0 (:,1) double;
% 				S double;
% 				optArgs.P_1 (:,:) quantity.Discrete;
% 				optArgs.P_2 (:,:) double;
% 				optArgs.P_3 (:,:) double;
% 				optArgs.P_4 (:,:) double;
% 				optArgs.B_4 (:,:) double;
% 				optArgs.theta (1,1) signals.GevreyFunction = ...
% 					signals.GevreyFunction( 'order', 1.999, "diffShift", 1, "numDiff", obj.N_s, ...
% 					'timeDomain', I);
% 				optArgs.method string = "gramian";
% 			end
% 			
% 			assert(size(v0,1) == size(S,1));
% 			
% 			%% parse the input parameters:
% 			n_v = size(S,1);
% 			ipp = misc.Parser();
% 			ipp.addParameter('P_1', quantity.Discrete.zeros( [n_v, obj.n.x], obj.spatialDomain ), ...
% 				@(a) validateattributes(a, {'quantity.Discrete'}, {'size', [n_v, obj.n.x]}));
% 			ipp.addParameter('P_2', zeros( n_v, obj.n.x ), ...
% 				@(a) validateattributes(a, {'numeric'}, {'size', [n_v, obj.n.x]}));
% 			ipp.addParameter('P_3', zeros( n_v, obj.n.x ), ...
% 				@(a) validateattributes(a, {'numeric'}, {'size', [n_v, obj.n.x]}));
% 			ipp.addParameter('P_4', zeros( n_v, obj.n.w ), ...
% 				@(a) validateattributes(a, {'numeric'}, {'size', [n_v, obj.n.w]}));
% 			ipp.addParameter('B_4', zeros( n_v, obj.n.u ), ...
% 				@(a) validateattributes(a, {'numeric'}, {'size', [n_v, obj.n.u]}));
% 			args = namedargs2cell(optArgs);
% 			ipp.parse(args{:});
% 			P_1 = ipp.Results.P_1;
% 			P_2 = ipp.Results.P_2;
% 			P_3 = ipp.Results.P_3;
% 			P_4 = ipp.Results.P_4;
% 			B_4 = ipp.Results.B_4;
% 			
% 			%%
% 			obj.report( "=== Plan the transition with the initial condition v(0) = " + misc.vector2string( v0 ) + " ===" );
% 			
% 			%% compute the differential parametrization
% 			de = obj.differentialExpressions;
% 			tm = obj.transitionMatrices;
% 			
% 			%% set up the helper xi-system
% 			%
% 			%	dt xi = A xi + B mu
% 			%		y = C xi
% 			%
% 			% compute the term (sI - S)
% 			sImS = signals.PolynomialOperator({ -S, eye( n_v ) });
% 			% compute the parameters for det(sI-S) = sum a_i s^i
% 			a = shiftdim( double( det( sImS ) ) ).';
% 			
% 			A = kron( [zeros( n_v-1, 1), eye( n_v-1 ); ...
% 				       -a(1:end-1)                   ], ...
% 					   eye(obj.n.u) );
% 			B = kron( misc.unitVector(n_v, n_v), eye(obj.n.u) );
% 			C = kron( misc.unitVector(n_v, 1).', eye(obj.n.u) );
% 			
% 			%% compute the initial conditions for the xi system			
% 			PsiV = [ int( P_1 * tm.Phi ) + P_2 ...
% 						+ P_3 * tm.Phi.subs("z", 1), ...
% 					 int( P_1 * tm.Psi_H ) ...
% 					 + P_3 * tm.Psi_H.subs("z", 1) + P_4] * de.adjPiD ...
% 					 + ( int( P_1 * tm.Psi_B ) + P_3 * tm.Psi_B.subs("z", 1) + B_4 ) * de.varpi;
% 			V = adj(sImS) * PsiV;
% 			
% 			V_ = zeros( n_v, n_v * obj.n.u );
% 			for i = 1:obj.N_s
% 				V_i = V(i).coefficient.on() * C * A^(i-1);
% 				% todo verify that this series converges...
% 				V_ = V_ + V_i;
% 			end
% 			
% 			assert( numeric.near( V_ * pinv( V_ ) * v0, v0, 1e-4 ), ...
% 				"The fault is not identifiable, since v0* = " + misc.vector2string(V_*pinv(V_)*v0) ...
% 				+ " but it should be v0 = " + misc.vector2string(v0) )
% 			
% 			xi0 = pinv(V_) * v0;
% 			
% 			% verify xi0
% 			for j = 0:obj.N_s
% 				dtjmu0 = 0;
% 				for i = 0: n_v
% 					dtjmu0 = dtjmu0 + a(i+1) * C * A^(i + j) * xi0;
% 				end
% 				isNear = numeric.near( dtjmu0, zeros(obj.n.u,1) );
% 				if ~isNear
% 					obj.report("   dtau^" + num2str(j) + " mu(0) = " + misc.vector2string( dtjmu0 ) + ")");
% 				end
% 			end
% 			
% 			%% compute the mu for the end condition:
% 			% two options are availbable:
% 			% ======================================================================================
% 			% 1) use the modified gramian matrix:
% 			if optArgs.method == "gramian"
% 				
% 				%compute the gramian controllability matrix:
% 				Phi_t1_t = expm(sys.A * (t.upper - t.Symbolic ) );
% 				W = int( Phi_t1_t * sys.B * sys.B' * Phi_t1_t' * optArgs.weight, t, t.lower, t.upper );
% 				
% 				
% 				
% 				xiSys = ss( A, B, C, [] );
% 				
% 				
% 				
% 				
% 				% to compute phi and dtau^i phi at first the derivatives of the state transition
% 				% matrix U = Phi^T(0, tau) must be computed:
% 				V_ = expm( - A.' * I.Symbolic );
% 				for i = 1 : obj.N_s
% 					dtau_i_Phi{i} = (-1)^i * V_ * ( A^i ).';
% 				end
% 				V_ = signals.BasicVariable( V_, dtau_i_Phi );
% 				
% 				% then the mu(tau) and its derivatives can be computed using the product rule:
% 				mu = - B.' * V_ * optArgs.theta * inv( W.at(I.upper) ) * xi0;
% 				
% 				for i = 1 : obj.N_s
% 					diffs_phi{i} = C * A^i * trj.xi;
% 					for j = 0 : i-1
% 						diffs_phi{i} = diffs_phi{i} + C * A^(i-1-j) * B * mu.diff(I, j);
% 					end
% 				end
% 				
% 				phi = signals.BasicVariable( trj.phi, diffs_phi );
% 
% 			% verify the result:
% 			mu_ = det( sImS ).applyTo( phi );
% 			
% 			[~,m,~] = numeric.near(mu_.on(), mu.diff(I, 0).on(), 1e-9);
% 			obj.report( misc.Message(sprintf('%g: relative tolerance for the difference of mu computed with the differential expression to the mu which was computed with the parametrized Gevrey function.', m), 'data', m) );
% 			
% 			% compute the differential expressions:
% 			trajectory = obj.referenceTrajectory( mu );
% 			trajectory.v = V.applyTo( phi, 'n', obj.N_s );
% 			trajectory.phi = phi;
% 			trajectory.mu = mu;
% 			trajectory.xi = trj.xi;
% 			
% 			odeInputOperator = int( P_1 * de.V ) + P_2 * de.V.subs("z", 0) ...
% 				+ P_3 * de.V.subs("z", 1) + B_4 * de.varpi;
% 			
% 			trajectory.odeInput = odeInputOperator.applyTo( mu, 'n', obj.N_s);
% 			
% 			% verify the result:
% 			[~,data] = numeric.near( trajectory.v.at(0), v0 );
% 			obj.report( misc.Message(sprintf('%g: difference for initial condition of the v-ode states', data), 'data', data) );
% 			[~,data] = numeric.near( trajectory.v.at(I.upper), zeros(n_v, 1) );
% 			obj.report( misc.Message(sprintf('%g: difference for end condition of the v-ode states', data), 'data', data) );
% 			[~,data] = numeric.near( trajectory.x.subs(I.name, 0 ).on(), zeros(obj.spatialDomain.n, 1));
% 			obj.report( misc.Message( sprintf('%g: difference for the initial condition of the pde states', data), 'data', data) );
% 			[~,data] = numeric.near( trajectory.x.subs(I.name, I.upper ).on(), zeros(obj.spatialDomain.n, 1));
% 			obj.report( misc.Message( sprintf('%g: difference for the end condition of the pde states', data), 'data', data) );
% % 			[~,data] = numeric.near( trajectory.w.atIndex(1), zeros(obj.n.w, 1));
% % 			obj.report( misc.Message( sprintf('%g: difference for the initial condition of the w-ode states', data), 'data', data) );
% % 			[~,data] = numeric.near( trajectory.w.atIndex(end), zeros(obj.n.w, 1));
% % 			obj.report( misc.Message( sprintf('%g: difference for the end condition of the w-ode states', data), 'data', data) );
% 
% 		end
		
		function [differentialExpression, Phi0, Psi_B] = computeDifferentialParametrization(obj)
			% COMPUTEDIFFERENTIALPARAMETRIZATION
			% computes the differential parametrization for the system in
			% the form:
			%	x(z,t) = sum X_k(z) phi_k(t)
			%	u(t)   = sum U_k(z) phi_k(t)
			
			%% compute the state transition matrix
			
			if isempty( obj.transitionMatrices )
				obj.setTransitionMatrices;
			end
			
			tMatrices = obj.transitionMatrices;
			Phi0 = tMatrices.Phi;
			Psi_B = tMatrices.Psi_B;
			Psi_H = tMatrices.Psi_H;
				
			% to obtain the differential parametrizations the system of equations 
			%
			%    /x(0)\
			% Pi |    | = D u
			%    \ w  /
			%
			% must be solved with
			%
			%	Pi = / Pi_x  Pi_xw \
			%		 \ Pi_wx Pi_w  /
			%
			%	D = / D_x \
			%       \ D_w /
			Pi_x  = obj.K_0 + obj.K_1 * Phi0.subs("z", 1);
			Pi_xw = obj.K_1 * Psi_H.subs("z", 1) + obj.H_2;
			Pi_wx = - int( obj.L_1 * Phi0 ) - obj.L_2 - obj.L_3 * Phi0.subs("z", 1);
			sImF = signals.PolynomialOperator({ -obj.F, eye(obj.n.w) });
			if obj.n.w > 0
				Pi_w  =  sImF - int( obj.L_1 * Psi_H ) - obj.L_3 * Psi_H.subs("z", 1);
			else
				Pi_w = zeros(0, 0);
			end
			
			D_x = obj.B_2 - obj.K_1 * Psi_B.subs("z", 1);
			D_w = int(obj.L_1 * Psi_B ) + obj.L_3 * Psi_B.subs("z", 1) + obj.B_3;
			
			Pi = [Pi_x, Pi_xw; ...
				  Pi_wx, Pi_w];
			
			D = [D_x; D_w];
			
			% with this, the differential expression can be computed:
			differentialExpression = struct();
			
			% the system of equations has the solution
			% [x(0); w] = adj(Pi) * D
			adjPiD = adj(Pi) * D;
			differentialExpression.adjPiD = adjPiD;
			% differential parametrization for the input:
			differentialExpression.varpi = det( Pi );
			% the differential parametrization for the ODE state is given by:
			if obj.n.w > 0
				differentialExpression.U = [ zeros( obj.n.w, obj.n.x ) eye( obj.n.w) ] * adjPiD;
			end
			% differential parametrization for the PDE state:
			differentialExpression.V = [Phi0 Psi_H] * adjPiD + Psi_B * differentialExpression.varpi;
			
		end
		
		function [trajectory, relativeDelta, numIter] = referenceTrajectory(obj, basisVariable, optArgs)
			arguments
				obj
				basisVariable signals.BasicVariable;
				optArgs.relativeIncrementTolerance;
			end
			de = obj.differentialExpressions;
			
			if isfield(optArgs, "relativeIncrementTolerance")
				varArgs = {"relativeIncrementTolerance", optArgs.relativeIncrementTolerance};
			else
				varArgs = { 'n', obj.N_s };
			end
			
			[trajectory.x, relativeDelta.x, numIter.x] = de.V.applyTo( basisVariable, varArgs{:}  );
			if obj.n.w > 0
				[trajectory.w, relativeDelta.w, numIter.w] = de.U.applyTo( basisVariable, varArgs{:} );
			end
			[trajectory.u, relativeDelta.u, numIter.u] = de.varpi.applyTo( basisVariable, varArgs{:} );
			
		end
		
		function [w, u, t] = computeSolution(obj, W, U, b, N, diffShift)
			
			if nargin <= 5
				diffShift = 0;
			end
			
			pbar = misc.ProgressBar('name', 'diff.Parametrization', 'steps', N);
			pbar.start();
			I = b(1).fun(1).domain;
			for k = 1:N
				% The derivative of the basic variable must be transposed because it is returned in
				% the form b_diff(i,j,k), where i is the derivative, j is size(b,1) and k is
				% size(b,2).
				wk(:,1,k) = W(k).coefficient * b.diff(I, k-1 + diffShift);
				uk(:,1,k) = U(k).coefficient * b.diff(I, k-1 + diffShift);
				pbar.raise();
			end
			
			pbar.stop();
			
			w = sum(wk, 3);
			u = sum(uk, 3);
			t = I.grid;
		end
				
		function stsp = createDiscreteModel(obj)
			warning('no discretization scheme implemented for this system')
		end
		
		function i = isempty(obj)
			i = isempty(obj.A);
		end
	end
	

end
