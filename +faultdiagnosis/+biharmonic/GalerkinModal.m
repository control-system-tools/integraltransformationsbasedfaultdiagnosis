classdef GalerkinModal < handle
    %GLAERKINMODAL A galerkin based approximation for the euler-bernoulli cantilever
	% 
    
	properties (SetAccess = protected)
		sys;
		z;
		phi;
		stsp;
		pdeStates;
		odeStates;
	end
	
    properties (Hidden)
        N_modes(1,1) double {mustBeInteger, mustBeNonnegative} = 5; 
        M;
        D;
        K;
        Lb;
        Pw;
        Pwdz;
		MM;
	end
	
    methods
        function obj = GalerkinModal(sys, optArgs)
            %MODALAPPROXIMATION Construct an instance of this class
            %   Detailed explanation goes here
            arguments
				sys faultdiagnosis.biharmonic.Cantilever;
				optArgs.N_modes (1,1) double = 5;
			end
			
			obj.z = sym("z", "real");
			obj.sys = sys;
			obj.N_modes = optArgs.N_modes;
						
			obj.phi = obj.computeBasisFunctions();
			[obj.stsp, obj.pdeStates, obj.odeStates] = obj.computeStSpMatrices();
			
        end
        
%         function phi = get.phi(obj)
%             if isempty(obj.phi) || size(obj.phi, 1) ~= obj.N_modes
%                obj.phi = obj.
%             end
%             phi =  obj.phi;
% 		end
			
        function dt = recommendedSampling(obj)
            
            % compute recommende sampling following the code from 
            % 'MATLAB/R2017b/toolbox/shared/controllib/engine/+ltipack/@ssdata/lsim'
            % -> LocalCheckSampling(D,dt)
            
           r = eig(obj.stsp);
           imagR = r(imag(r)>0 & abs(real(r)) < 0.2 * abs(r));
		   
		   if isempty(imagR)
			   % If there are no imaginary poles, chose the sampling time
			   % by factor 10 faster than the fastest eigenfrequency of the
			   % system.
			  dt =  abs(1 / min(r) * 0.1);
		   else
			   mf = max(abs(imagR));
			   [~,ee] = log2(pi/2/mf);
			   dt = pow2(ee-1);
		   end
		end		
    end
    
    methods (Access = public)
        
         function [stsp, pdeStates, odeStates] = computeStSpMatrices(obj)
            %GETSTSPMATRICES Function for the calculation of the matrices
            %for the state space representation.
            
            %% initialization of the progressbar
            counter = 9;
            pbar = misc.ProgressBar(...
                'name', 'Compute Galerkin Approximation: ', ...
                'terminalValue', counter, ...
                'steps', counter, ...
                'initialStart', true);
            
            %% compute matrices for the finite-dimensional approximation
            % M dt^2 w(t) + D dt w(t) + K w(t) = L u(t)
			
            fhi = quantity.Discrete(obj.phi);
            dzfhi = quantity.Discrete(obj.phi.diff(obj.z));
            dzzfhi = quantity.Discrete(obj.phi.diff(obj.z, 2));
            dzzzfhi = quantity.Discrete(obj.phi.diff(obj.z, 3));
			
            M2 = int(fhi * ( fhi * obj.sys.mue ).');                   pbar.raise();
            M1 = - obj.sys.J * ( fhi.atIndex(end) * dzfhi.atIndex(end).' ...
                * obj.sys.lambda.diff().atIndex(end) - dzfhi.atIndex(end) ...
                * dzfhi.atIndex(end).' * obj.sys.lambda.atIndex(end)) ;                pbar.raise();
            M0 = obj.sys.mass * obj.sys.lambda.atIndex(end) ...
                * fhi.atIndex(end) * fhi.atIndex(end).';                              pbar.raise();
            
            obj.M = (M0 + M1 + double(M2));                                 pbar.raise();
            obj.D = double(int(fhi * obj.sys.gamma * fhi.'));                  pbar.raise();
            obj.K = double(int(dzzfhi* obj.sys.lambda * dzzfhi.'));           pbar.raise();
            Lbdomain = double( int(fhi * [obj.sys.bz, obj.sys.gz, obj.sys.ez], ...
				'z', obj.sys.spatialDomain.lower, obj.sys.spatialDomain.upper ) );    
			pbar.raise();
            Lbboundary = (dzfhi.atIndex(end) * obj.sys.lambda.atIndex(end) ...
				- fhi.atIndex(end) * obj.sys.lambda.diff().atIndex(end) ) * ...
				[obj.sys.b1 obj.sys.g1, obj.sys.e1] ...
                - fhi.atIndex(end) * obj.sys.lambda.atIndex(end) * ... 
				[obj.sys.b2 obj.sys.g2 obj.sys.e2];
            
            obj.Lb = Lbdomain + Lbboundary;
            
            obj.Pw = fhi.on();                                          pbar.raise();
            obj.Pwdz = dzfhi.on();                                      pbar.raise();
            
            %% reformulation as system of first order
			% E dt x(t) = A x(t) + B u(t)
			
			% E = [D M; M 0]
			% A = [-K 0; 0 M]
			% B = [L; 0]
			% C = [C, 0]
            O = zeros(obj.N_modes);
            E = [obj.D, obj.M; obj.M, O];
            A = [- obj.K, O; O, obj.M];
            B = [obj.Lb; zeros(size(obj.Lb))];

            c0 = zeros(1, obj.sys.spatialDomain.n);
            c0(end) = 1;

			%% compute the output matrix:
			
			Jx1 = [eye(obj.N_modes), O];
			Jx2 = [O, eye(obj.N_modes)];
			z1 = obj.sys.spatialDomain.upper;
			z0 = obj.sys.spatialDomain.lower;
			
			C_1 = double(...
				int( obj.sys.C_1 * [dzzzfhi'; dzzfhi'; dzfhi'; fhi'] * Jx1 ));
			
			C_2 = obj.sys.C_2 * ...
					[dzzzfhi.at(z0)'; ...
					 dzzfhi.at(z0)'; ...
					 dzfhi.at(z0)'; ...
					 fhi.at(z0)'] * Jx1;
			
			C_3 = obj.sys.C_3 * ...
					[dzzzfhi.at(z1)'; ...
					 dzzfhi.at(z1)'; ...
					 dzfhi.at(z1)'; ...
					 fhi.at(z1)'] * Jx1;
			
			C = C_1 + C_2 + C_3;
				 
            pdeStates = [obj.Pw, zeros(size(obj.Pw))];
			
			if isempty( obj.sys.F )
				odeStates = zeros(0, 2*obj.N_modes);
			else
				odeStates = [dzfhi.atIndex(end).' * Jx2; ...
							 dzfhi.atIndex(end).' * Jx1; ...
							 fhi.atIndex(end).' * Jx2; ...
							 fhi.atIndex(end).' * Jx1];
			end
			
			DD = [zeros(obj.sys.n.y, obj.sys.n.u) obj.sys.G_4 obj.sys.E_4];			
            stsp = dss(A, B, C, DD, E);                                              
			pbar.stop();
                        
         end
        
        function [Phi, residuum] = computeBasisFunctions(obj)

            z = obj.z;
            z0 = obj.sys.spatialDomain.lower;
            assert(z0 == 0);
            z1 = obj.sys.spatialDomain.upper;
            p = sym('p');

            %% initialization of the progressbar
            counter = obj.N_modes;
            pbar = misc.ProgressBar(...
                'name', 'Compute Eigenfunctions: ', ...
                'terminalValue', counter, ...
                'steps', counter, ...
                'initialStart', true);
            
            % the basis functions are the eigenfunctions of the undamped
            % euler-bernoulli-beam-equation
            %   dt^2 w(z,t) + dz^4 w(z,t) = 0
            %   w(0,t) = dz w(0,t) = 0;
            %   dz^2 w(1,t) = dz^3 w(1,t) = 0
            % For this, the eigenfunctions are solutions of
            %   dz^4 phi_k(z) = p_k^4 phi(z)
            % and satisfy the boundary conditions
            %   phi(0) = dz phi(0) = 0
            %   dz^2 phi(1) = dz^3 phi(1) = 0
            % As ansatz for the solution the Rayleigh-functions are used:
            % definition of the rayleigh-functions
            R(1, 1) = cosh( p * z ) + cos( p * z );
            R(1, 2) = sinh( p * z ) + sin( p * z );
            R(1, 3) = cosh( p * z ) - cos( p * z );
            R(1, 4) = sinh( p * z ) - sin( p * z );

            % with this the solution can be parametrized by
            %    Phi_k(z) = a1 * R(1,1) + a2 * R(1,2) + a3 * R(1,3) + a4 * R(1,4)
            % The further calculations can be simplified by introducing the new
            % systems vector: PHI_k = [phi_k, dz phi_k, dz^2 phi_k, dz^3 phi_k]'
            % With this the solution can be parametrized as:
            %   PHI_k(z) = R(z) * a 
            % with: a = [a1 a2 a3 a4]' the derivatives of the Rayleigh-functions
            R(2, :) = diff(R(1, :), 1, z);
            R(3, :) = diff(R(1, :), 2, z);
            R(4, :) = diff(R(1, :), 3, z);

            % The fundamental matrix for the eigenfunctions problem is
            % given by
            %   PSI = R(z) * R0^-1
            % with:
            PSI = R / subs(R, z, z0);
            % This can be seen by PHI(0) = R(0)a -> a = R(0)^-1 PHI(0)
            %   PHI(z) = R(z) * R(0)^-1 * PHI(0)
            % What is identical with the general solution
            %   PHI(z) = PSI(z) * PHI(0)   

            % Now the unknown paramter vector a is determined by considering the
            % boundary conditions. For this, the linear system of equations is set
            % up:
            %   PHI_1(0) = R[1,:](0) * a
            %   PHI_2(0) = R[2,:](0) * a
            %   PHI_3(1) = R[3,:](1) * a
            %   PHI_4(1) = R[4,:](1) * a

            R_ = [subs(PSI(1,:), z, z0); ...
                  subs(PSI(2,:), z, z0); ...
                  subs(PSI(3,:), z, z1); ...
                  subs(PSI(4,:), z, z1)];
            % To allow non trivial solutions the determinant of this problem as to
            % vanish. 
            d = simplify(det(R_));
            D_ = matlabFunction(d);
            % Thus, we have to find the zeros of the determinant of R_:
            n = 1:obj.N_modes; % Iteration for the number of zeros
            % initial guess for the location of the zeros
            % is motivated by the determinant: d = (cos(p)*cosh(p))/2 + 1/2
            % As cosh(p) is always greater zero, it changes only the sign at the
            % zeros of cos(p) shifted by 1/2. For big p, the 1/2 can be neglected
            % and the zero is very close to the zero of the cos(p). For small it
            % can be found, that it will be also close to cos(p) and that there are
            % not more than one zero in one half period of cos(p). Thus
            p = (2*n - 1) * pi / 2 / (z1 - z0); 
            % is a good initial guess.

            % iterate over all initial guesses and find the right location of the
            % zeros
			% The search domain is given by the half of the distance
			% between two guesses. As the zeros for p>>0 are
			% nearly the zeros of the cos(p), this should work most of the
			% time.
            p4 = NaN(1, obj.N_modes);
            for k = 1:obj.N_modes
               p4(k) = fzero(D_, [p(k)-p(1)/2 p(k)+p(1)/2]);
            end

            assert(all(diff(p4)))
            
            % now we can compute the eigenfunctions: from phi(0) = dz phi(0) = 0 we
            % know that a1 = a2 = 0. With this the linear system of equations that
            % has to be solved simplifies to
            %   [ PHI_3(1) ] = 0 = [ R[3,3](1) R[3,4](1)] [a3 a4]'
            %   [ PHI_4(1) ] = 0 = [ R[4,3](2) R[4,4](1)] [a3 a4]'
            % so we can iterate over the zeros of the determinant and find the
            % eigenfunctions:

			
			R = simplify(R);
            for k = 1:obj.N_modes
                pbar.raise();
                % substituing the current zero:
                r33i = subs(R(3,3), 'p', p4(k));
                r34i = subs(R(3,4), 'p', p4(k));

                % As the linear system of equations has no unique solution we have
                % to paramterize the solution by 
                %   a4 = - gamma a3
                % and with this we have
                %   gamma = R[3,3](1) / R[3,4](1) = R[4,3](1) / R[4,4](1)
                gamma = double(subs(r33i / r34i, 'z', z1));        

                % As the eigenfunction is 
                %   phi(z) = PHI[1,:](z)
                % And this is given by phi(z) = R[1,:](z) * a and from this by
                %   phi(z) = a3 (R[1,3](z) - gamma R[1,4](z))
                % we can find the solution:        
                tmpPhi = simplify( subs(R(1, 3) - gamma * R(1, 4), 'p', p4(k)) );
                % So far, we did not definded the a3. This can be used for
                % normalization. So we norm the eigenfunction to
                %   || phi_k(z) ||^2 = int(dz^2 phi(z))^2, z, 0, 1);
                % From this we get
                a_3 = 1 / sqrt( double( int( ( r33i - gamma * r34i )^2 , z0, z1) ) );

                Phi(k, 1) = quantity.Symbolic( a_3 * tmpPhi, obj.sys.spatialDomain, ...
					'name', 'Phi', 'symbolicEvaluation', false);
            end
            pbar.stop();
        end                 
    end
end

