classdef Cantilever < faultdiagnosis.System
	% CANTILEVER describes a cantilever With this class the euler-bernoulli
	% beam, clamped on the left side and a free end on the right side
	%     mu(z) dt^2 w(z,t) + gamma(z) dt w(z,t)
	%           + dz^2 (lambda(z) dz^2 w(z,t)) = b(z) u(t) + g(z) d(t) + e(z) f(t)
	% with spatially-varying coefficients is described. The systemvariable
	% w(z,t)\in\mathbb{R} is considered on the domain
	% (z,t)\in\Omega\times\mathbb{R}^+, with the spatial domain \Omega=(z0,
	% z1). The boundary conditions of the system are given by
	%        w(0,t) = 0 
	%     dz w(0,t) = 0
	%   dz^2 w(1,t) = b_1 u(t) + g_1 d(t) + e_1 f(t)
	%   dz^3 w(1,t) = b_2 u(t) + g_2 d(t) + e_2 f(t)
	%   
	% and the output equation with
	%   y = int_0^1 C_1 w(z,t) dz + C_3 [dz^3w(1,t); dz^2 w(1,t); dz w(1,t); w(1,t)] 
	%		+ G_4 d(t) + E_4 f(t)
	%
	%   A[x(t)](z) = A0 x(z,t) + A1 dt x(z,t) + A2 dt^2 x(z,t)
	%
	%   A0 = [-2 dz lambda(z) / lambda(z), -dz^2 lambda(z) / lambda(z) 0 0 ];
	%        [      1                           0                      0 0 ];
	%        [      0                           1                      0 0 ]; 
	%        [      0                           0                      1 0 ]; 
	%
	%   A1(1,4) = -gamma(z) / lambda(z);   otherwise 0 
	%   A2(1,4) = -mu(z) / lambda(z);      otherwise 0
	%
	%   B(1,:) = b(z) / lambda(z); otherwise 0 E1(1,1) = e1(z) / lambda(z);
	%   otherwise 0 G(1,1) = g(z) / lambda(z); otherwise 0 
	%   K0 = [0 0 1 0];
	%        [0 0 0 1]; 
	%        [0 0 0 0]; 
	%        [0 0 0 0];
	%
	%   K1 = [0 0 0 0;
	%         0 0 0 0; 
	%         0 1 0 0; 
	%         1 0 0 0];
	%
	%   B2 = [0;
	%         0;
	%         b_1;
	%         b_2;];
	%
	%   E2 = [0;
	%         0;
	%         e_1;
	%         e_2;];
	%
	%   G2 = [0;
	%         0;
	%         g_1;
	%         g_2;];
	
	properties (SetAccess = protected)
		lambda;
		mue;
		gamma;
		L;
		galerkinApproximation;
		J = 0;
		mass = 0;
		N_modes = 5;
	end
	
	properties 
		% --- input vector for the control inputs ---
		bz; % input vector for the actuator in the spatial domain
		b1; % input vector for the torque on the right tip
		b2; % input vector for the shearforce on the right tip
		
		% --- input vectors for the disturbances ---
		gz; % input vector for the in-domain disturbance
		g1;
		g2;
		
		% --- input vector for the faults ---
		ez; % input vector for the in-domain fault
		e1;
		e2;
	end
		
	methods
		function obj = Cantilever(spatialDomain, optArgs)
			arguments
				spatialDomain (1,1) quantity.Domain;
				optArgs.mue (1,1) quantity.Symbolic = quantity.Symbolic(0, spatialDomain, 'name', 'mu');
				optArgs.lambda (1,1) quantity.Symbolic = quantity.Symbolic(0, spatialDomain, 'name', 'lambda');
				optArgs.gamma (1,1) quantity.Symbolic = quantity.Symbolic(0, spatialDomain, 'name', 'gamma');
				optArgs.J (1,1) double;
				optArgs.mass (1,1) double;
				% --- control inputs ----
				optArgs.bz (1,:) quantity.Symbolic = quantity.Symbolic(0, spatialDomain, 'name', 'b');
				optArgs.b1 (1,:) double;
				optArgs.b2 (1,:) double;
				% --- fault inputs ---				
				optArgs.ez (1,:) quantity.Symbolic = quantity.Symbolic(0, spatialDomain, 'name', 'e1');
				optArgs.e1 (1,:) double;
				optArgs.e2 (1,:) double;
				optArgs.E_4 double;
				% --- disturbance inputs ---
				optArgs.gz (1,:) quantity.Symbolic = quantity.Symbolic(0, spatialDomain, 'name', 'g');
				optArgs.g1 double;
				optArgs.g2 double;
				optArgs.G_deterministic double;
				optArgs.G_bounded double;
				optArgs.G_4 double;
				% --- outputs ---
				optArgs.C_1 quantity.Symbolic
				optArgs.C_3 double;
				% --- additional parameters ---
				optArgs.L double;
				optArgs.N_s (1,1) double = 7;
				optArgs.N_modes (1,1) double = 5;
			end
			
			%todo e_1; e_2
			
			lmbd = optArgs.lambda;
			A = cat(3, ...
				[-2 * optArgs.lambda.diff("z", 1) / lmbd, - optArgs.lambda.diff("z", 2) / lmbd, 0, 0; ...
				  1 0 0 0; 0 1 0 0; 0 0 1 0], ...
				[0 0 0, - optArgs.gamma / lmbd; 0 0 0 0; 0 0 0 0; 0 0 0 0], ...
				[0 0 0, - optArgs.mue / lmbd;   0 0 0 0; 0 0 0 0; 0 0 0 0]...
				);

			[A.name] = deal('A');
			A = signals.PolynomialOperator(A);
			
			% --- determine the dimensions of inputs and outputs ---
			nu = misc.getDimensions(optArgs, ["bz", "b1", "b2"], 2);
			nf = misc.getDimensions(optArgs, ["ez", "e1", "e2", "E_4"], 2);
			nd = misc.getDimensions(optArgs, ["gz", "g1", "g2", "G_4"], 2);
			ny = misc.getDimensions(optArgs, ["C_1", "C_3"], 1);
			% --- control inputs ----
			B1 = [optArgs.bz / lmbd; ...
				   zeros(3, nu)];
			[B1.name] = deal('B_1');
			if ~isfield(optArgs, "b1")
				optArgs.b1 = zeros(1, nu);
			end
			if ~isfield(optArgs, "b2")
				optArgs.b2 = zeros(1, nu);
			end				
			B2 = zeros(4, nu);
			
			% --- fault inputs ---
			E1 = [optArgs.ez / lmbd; 
					zeros(3, nf)];
			[E1.name] = deal('ez');
			if ~isfield(optArgs, "E_4")
				optArgs.E_4 = zeros(ny, nf);
			end
			if ~isfield(optArgs, "e1")
				optArgs.e1 = zeros(1, nf);
			end
			if ~isfield(optArgs, "e2")
				optArgs.e2 = zeros(1, nf);
			end
			E2 = zeros(4, nf);
			
			% --- disturbance inputs ---
			G1 = [optArgs.gz / lmbd; ...
					zeros(3, nd)];
			[G1.name] = deal('G_1');
			if ~isfield(optArgs, "g1")
				optArgs.g1 = zeros(1, nd);
			end
			if ~isfield(optArgs, "g2")
				optArgs.g2 = zeros(1, nd);
			end
			G2 = zeros(4, nd);
			if isfield(optArgs, "G_deterministic")
				G_det = optArgs.G_deterministic;
			else
				G_det = zeros(nd, 0);
			end
			
			if isfield(optArgs, "G_bounded")
				G_b = optArgs.G_bounded;
			else
				G_b = zeros(nd, 0);
			end
			
			if ~isfield( optArgs, "G_4")
				optArgs.G_4 = zeros(ny, nd);
			end
			
			% --- the boundary ode ---
			if isfield(optArgs, "J") && isfield(optArgs, "mass")
				% cantilever with tip load:
				F = [0 0 0 0; 1 0 0 0; 0 0 0 0; 0 0 1 0];
				L3 = [0 -1/optArgs.J 0 0; 0 0 0 0; 1/optArgs.mass 0 0 0; 0 0 0 0];
				H2 = [0 0 0 0; 0 0 0 0; 0 -1 0 0; 0 0 0 -1];
				B3 = [optArgs.b1 ./ optArgs.J; zeros(1, nu); - optArgs.b2 ./ optArgs.mass; ...
					zeros(1, nu)];
				E3 = [optArgs.e1 ./ optArgs.J; zeros(1, nf); - optArgs.e2 ./ optArgs.mass; ...
					zeros(1, nf)];
				G3 = [optArgs.g1 ./ optArgs.J; zeros(1, nd); - optArgs.g2 ./ optArgs.mass; ...
					zeros(1, nd)];
				% boundary matrices:
				K0 = [0 0 1 0; 0 0 0 1; 0 0 0 0; 0 0 0 0];
				K1 = [0 0 0 0; 0 0 0 0; 0 0 1 0; 0 0 0 1];			
			else
				% cantilever without tip load:
				F = [];
				L3 = zeros(0,4);
				H2 = zeros(4,0);
				B3 = zeros(0,nu);
				E3 = zeros(0,nf);
				G3 = zeros(0,nd);
				K0 = [0 0 1 0; 0 0 0 1; 0 0 0 0; 0 0 0 0];
				K1 = [0 0 0 0; 0 0 0 0; 0 1 0 0; 1 0 0 0];
				
				B2 = [zeros(2, nu); ...
					  optArgs.b1; ...
					  optArgs.b2];
				  
				G2 = [zeros(2, nd); ...
					  optArgs.g1; ...
					  optArgs.g2];
				
				E2 = [zeros(2, nf); ...
					  optArgs.e1; ...
					  optArgs.e2];
			end
			
			% --- outputs ---
			if isfield(optArgs, "C_1")
				C_1 = optArgs.C_1;				
			else
				C_1 = quantity.Symbolic.zeros([ny, 4], spatialDomain );
			end
			if isfield(optArgs, "C_3")
				C_3 = optArgs.C_3;
			else
				C_3 = zeros( ny, size(F,1) );
			end
			
			obj@faultdiagnosis.System(spatialDomain, A, K0, K1, ...
				'F', F, 'H_2', H2, 'L_3', L3, ...
				'C_1', C_1, 'C_3', C_3, ...
				'B_1', B1, 'B_2', B2, 'B_3', B3, ...
				'E_1', E1, 'E_2', E2, 'E_3', E3, 'E_4', optArgs.E_4, ...
				'G_1', G1, 'G_2', G2, 'G_3', G3, 'G_4', optArgs.G_4, ...
				'G_deterministic', G_det, 'G_bounded', G_b, ...
				'N_s', optArgs.N_s);
			
			obj.L = optArgs.L;
			obj.mue = optArgs.mue;
			obj.lambda = optArgs.lambda;
			obj.gamma = optArgs.gamma;
					
			if isfield(optArgs, "J") && isfield(optArgs, "mass")
				obj.J = optArgs.J;
				obj.mass = optArgs.mass;
			end
			
			obj.bz = optArgs.bz;
			obj.b1 = optArgs.b1;
			obj.b2 = optArgs.b2;
			
			obj.gz = optArgs.gz;
			obj.g1 = optArgs.g1;
			obj.g2 = optArgs.g2;
			
			obj.ez = optArgs.ez;
			obj.e1 = optArgs.e1;
			obj.e2 = optArgs.e2;
			
			obj.N_modes = optArgs.N_modes;
			
			obj.discreteModel = obj.createDiscreteModel();
		end
	end
	
	methods ( Access = public )
		function discreteModel = createDiscreteModel(obj)
			arguments
				obj;
			end
			
			obj.galerkinApproximation = faultdiagnosis.biharmonic.GalerkinModal(obj, "N_modes", obj.N_modes);
						
			discreteModel = faultdiagnosis.DiscreteModel( obj, obj.galerkinApproximation.stsp, ...
				obj.spatialDomain, 'pdeStates', obj.galerkinApproximation.pdeStates, ...
				'odeStates', obj.galerkinApproximation.odeStates);
			
		end
	end
end