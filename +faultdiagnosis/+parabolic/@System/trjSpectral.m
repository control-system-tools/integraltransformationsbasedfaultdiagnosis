function [ m, n, z, t, series, ws_sym ] = trjSpectral( obj, varargin )
%TRJSPECTRAL Summary of this function goes here
%   Detailed explanation goes here

%% TODO: 
%       trajectory planning bisher nur für das Stellsignal und
%       noch nicht ausgibig getestet.
%
%   
%
    warning('experimental');
    m=[];
    z=[];
    series = [];
    ws_sym = [];

    %% default values
    Nz = misc.ifemptyelse(obj.approximation.N, 100);
    basicvar = basicvariable.bump; 
    N = 5;

    %% input parsen
    for k = 1:2:numel(varargin)
        switch varargin{k}
            case 'Nz'
                Nz = varargin{k+1};
            case 'b'
                basicvar = varargin{k+1};
            case 'N'
                N = varargin{k+1};
            otherwise
                error(['Not implemented argument' varargin{k}]);
        end
    end            

    %%
    dim.t = basicvar.Nt;                   % Anzahl der Diskretisierungspunkte f�r die Zeit
    dim.z = obj.approximation.N;    % Anzahl der Diskretisierungspunkte f�r den Ort
    dim.p = obj.p.b;
    dim.n = obj.n;
    dim.N = N;            

    clear Nz

    if isempty(obj.approximation)       
        obj.approximation = discretization.SymParabolic(obj, 'N', dim.z);
    end
    [diagSys, T] = canon(obj.approximation.stsp, 'modal');
    egv = eig(diagSys.A);

    assert(all(diff(egv)>0))  % assure that the eigenvalues are sorted

    % the eigenvalues are sorted in ascending order:
    % => the values have to be iterated backward
    characteristicpoly = poly(egv(end-(N-1):end));

    % normieren auf Zeitkonstantenform
    uk = characteristicpoly / characteristicpoly(end);

    xik = zeros(dim.p, dim.t, dim.N);        % (m, t, k)
    nk = zeros(dim.p, dim.t, dim.N);         % (m, t, k)

    for k = 1:(N+1)
        [xik(:, :, k ), t] = basicvar.eval(k-1); 
        nk(:,:,k) = uk( (N+2)-k ) * xik(:,:,k);                
    end

    n = sum(nk, 3);

end

