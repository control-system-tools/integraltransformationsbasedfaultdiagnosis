function [ m, n, z, t, series, ws_sym ] = trjTaylor( obj, Nz, b, N )
%TRJTAYLOR Computation of the trajectory planning based on taylor expansion
%
%       dz v(z,t) = A[v(t)](z)
%       C0 v(0,t) + C1 v(1,t) + D n(t) = 0
%
    assert(obj.b == 0, 'Taylor series based trajecotry planning is only implemented for boundary inputs. No in-domain inputs are allowed!');

    %% diskretisierungen
    dim.t = b.N;                  
    dim.p = obj.p.b;
    dim.n = obj.n;
    dim.z = Nz;
    dim.N = N;

    clear Nz N   % eingangsvariablen löschen, da die nicht mehr gebraucht werden                      

    [W, U] = obj.taylorExpansion(dim.N);
%     
%     W = obj.W;
%     U = obj.U;

    %% Numerische Auswertung
    %   Berechnung der Reihenentwicklung: 
    %       w(z,s) = W(z,s) b(s)
    %                W(z,s) = sum wk(z) s^k b(s)
    %       
    %       w(z,t) = sum wk(z) dt^k b(t)
    %
    %       wk \in (n, p)
    %       bk \in (p)
    %       w  \in (n)
    %       

    z = obj.spatialdiscretization(dim.z);

    % W(z,s) Matrix an den Orten z auswerten
    W.num = W.fun(z);
    % Das Ergebnis muss noch in die Richtige Matrixform gebracht
    % werden:
    wk = reshape( double( [ W.num{ :, :, 1 : dim.N } ] ), ...
               dim.z, dim.n, dim.p, dim.N ); % (z, n, p, k)

    % Alle weiteren Matrizen initialisieren
    uk = zeros(1, dim.N);
    mk = zeros(dim.n, dim.z, dim.t, dim.N);  % (n, z, t, k)
    nk = zeros(dim.p, dim.t, dim.N);         % (p, t, k)
    bk = zeros(dim.p, dim.t, dim.N);         % (p, t, k)

    % Erste Schleife über die Reihenterme
    t = b.maket();
    for kN = 1:dim.N
        bk(:, :, kN ) = b.eval(kN-1, t).';  

        % Weil Multiplikation von n-d Matrizen (bzw. dim > 2)
        % kompliziert werden die Berechnungen mit For-Schleifen
        % realisiert:
        for kn = 1:dim.n
            for kp = 1:dim.p
                wknp = squeeze(wk(:, kn, kp, kN));    % (z, n, p, k)     
                % wk[n,p](z) * bk[p](t)
                mk(kn, :, :, kN) = squeeze(mk(kn, :, :, kN)) + wknp * bk(kp,:,kN); 
            end
        end   

        uk(kN)     = U.poly(kN);
        nk(:,:,kN) = uk(kN) * bk(:,:,kN);
    end

    % summation über die Reihe
    m = sum(mk, 4);
    n = sum(nk, 3)';

    % Reihenterme zum debuggen
    series.W  = W;
    series.wk = wk;
    series.uk = uk;
    series.bk = bk;
    series.mk = mk;
    series.nk = nk;
    series.z  = z;
    series.t  = t;

    ws_sym=misc.ws2struct;
