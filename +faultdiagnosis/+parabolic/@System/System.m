classdef System < faultdiagnosis.System
	% SYSTEM described by parabolic PDEs
	% With this class diffusion-convection-reaction systems
	%      dt v(z, t) = dz (alpha(z) dz v(z, t)) + beta(z) dz v(z, t) + gamma(z) v(z, t) +
	%                    + b(z) u(t) + g(z) d(t) + e(z) f(t)
	% with spatially-varying coefficients are described. The systemvariable
	% v(z,t)\in\mathbb{R}^n is considered on the domain
	% (z,t)\in\Omega\times\mathbb{R}^+, with the spatial domain \Omega=(z0, z1).
	%  The boundary conditions of the system are given by
	%     q01 dz v(0,t) + q00 v(0, t) = b0^T u(t) + g0^T d(t) + e0^T f(t)
	%     q11 dz v(1,t) + q10 v(1, t) = b1^T u(t) + g1^T d(t) + e1^T f(t)
	% and the output equation with
	%     y = int_0^1 c11(z) dz v(z,t) + c10(z) v(z,t) dz + c21 dz v(0,t) +
	%     c20(z) v(0,t) + c31 dz v(1,t) + c30 v(1,t) + e2
	%
	% === usage ===
	%
	% === Spatial system of first order ===
	
	properties (SetAccess = protected)
		%% Parameters for the pde
		alpha;	% diffusion parameter
		beta;	% convection parameter
		gamma;	% reaction parameter
		%
		trajectoryFunction
	end
	
	properties (Dependent)
		b;		% input operator
		g;		% disturbance input operator
		e;		% faulty input operator
		%% Parameters for the boundary conditions
		q01;	%
		q00;
		b0;
		g0;
		e0;
		q11;
		q10;
		b1;
		g1;
		e1;
		%% Output parameters
		c10;
% 		c11;
		c20;
% 		c21;
		c30;
% 		c31;
		e2;
	end
	
	methods
		%% Constructor
		function obj = System(spatialDomain, varargin)
			
			sysparser = misc.Parser();
			sysparser.addOptional('alpha', quantity.Discrete(ones(spatialDomain.n, 1), spatialDomain));
			sysparser.addOptional('beta', quantity.Symbolic(0, spatialDomain));
			sysparser.addOptional('gamma', quantity.Symbolic(0, spatialDomain));
			sysparser.addOptional('b', quantity.Discrete.empty(1, 0));
			sysparser.addOptional('g', quantity.Discrete.empty(1, 0));
			sysparser.addOptional('e', quantity.Discrete.empty(1, 0));
			% 			sysparser.addOptional('c11', quantity.Symbolic.empty(0, 0));
			sysparser.addOptional('c10', quantity.Discrete.empty(0, 0));
			sysparser.addOptional('c20', []);
			% 			sysparser.addOptional('c21', []);
			sysparser.addOptional('c30', []);
			% 			sysparser.addOptional('c31', []);
			sysparser.addOptional('e2', []);
			sysparser.addOptional('q10', 0);
			sysparser.addOptional('q00', 0);
			sysparser.addOptional('q11', 0);
			sysparser.addOptional('q01', 0);
			sysparser.addOptional('b0', []);
			sysparser.addOptional('g0', []);
			sysparser.addOptional('e0', []);
			sysparser.addOptional('b1', []);
			sysparser.addOptional('g1', []);
			sysparser.addOptional('e1', []);
			sysparser.parse(varargin{:});	% all parameters are directly written into the workspace
			
			% matlab trys to call the function beta and does not take the
			% variable in the workspace. Thus, beta is called b and so that
			% b does not feel that alone, also alpha and gamma are
			% explicitly set to local variables.
			alfa = sysparser.Results.alpha;
			bta = sysparser.Results.beta;
			gma = sysparser.Results.gamma;
			
			A0 = [- ( alfa.diff() + bta ) / alfa, -gma / alfa ; ...
				1        ,    0  ];
			A1 = [0, 1 / alfa ; ...
				0,    0  ];
			
			K0 = [sysparser.Results.q01, sysparser.Results.q00];
			K1 = [sysparser.Results.q11, sysparser.Results.q10];
			
			%% Arguments for the parent class
			parentargs.valueIdx = [2, 1];
						
			% determine the number of inputs and outputs:
			dims.p = faultdiagnosis.System.getMaxDimension(sysparser.Results, ...
				{'b', 'b0', 'b1'}, 2);
			dims.q = faultdiagnosis.System.getMaxDimension(sysparser.Results, ...
				{'g', 'g0', 'g1'}, 2);
			dims.r = faultdiagnosis.System.getMaxDimension(sysparser.Results, ...
				{'e', 'e0', 'e1'}, 2);
			dims.m = faultdiagnosis.System.getMaxDimension(sysparser.Results, ...
				{'c10', 'c20', 'c30'}, 1);			
			
			prsr = misc.Parser();
			prsr.addOptional('b0', zeros(1, dims.p));
			prsr.addOptional('g0', zeros(1, dims.q));
			prsr.addOptional('e0', zeros(1, dims.r));
			prsr.addOptional('b1', zeros(1, dims.p));
			prsr.addOptional('g1', zeros(1, dims.q));
			prsr.addOptional('e1', zeros(1, dims.r));
			prsr.addOptional('e2', zeros(dims.m, dims.r));
			prsr.addOptional('b', quantity.Discrete.zeros([1, dims.p], spatialDomain, 'name', 'b'));
			prsr.addOptional('g', quantity.Discrete.zeros([1, dims.q], spatialDomain, 'name', 'g'));
			prsr.addOptional('e', quantity.Discrete.zeros([1, dims.r], spatialDomain, 'name', 'e'));
			prsr.addParameter('c10', quantity.Discrete.zeros([dims.m, 1], spatialDomain, 'name', 'c10'));
			prsr.addParameter('c20', zeros(dims.m, 1));
			prsr.addParameter('c30', zeros(dims.m, 1));
			prsr.parse(varargin{:});			
			
			
			% use 1/alpha * b instead of b/a because the second expression
			% does not work if b is empty.
			parentArgs.B_1 = signals.PolynomialOperator(...
				[ -1 / alfa * prsr.Results.b; ...
				 zeros(1, dims.p)]);
			parentArgs.E_1 = [ -1 / alfa * prsr.Results.e; zeros(1, dims.r)];
			parentArgs.G_1 = [ -1 / alfa * prsr.Results.g; zeros(1, dims.q)];
			
			parentArgs.K_1 = [K0       ; zeros(1, 2)];
			parentArgs.K_2 = [zeros(1, 2); K1];
			parentArgs.B_2 = [prsr.Results.b0; ...
							  prsr.Results.b1];
			parentArgs.E_2 = [prsr.Results.e0; ...
							  prsr.Results.e1];
			parentArgs.G_2 = [prsr.Results.g0; ...
							  prsr.Results.g1];
			
			parentArgs.C_1 = [zeros(dims.m, 1), prsr.Results.c10];
			parentArgs.C_2 = [zeros(dims.m, 1), prsr.Results.c20];
			parentArgs.C_3 = [zeros(dims.m, 1), prsr.Results.c30];
			parentArgs.E_3 = prsr.Results.e2;
			parentArgs = misc.struct2namevaluepair(parentArgs, true);
			unmatched = misc.struct2namevaluepair(sysparser.Unmatched, true);
			
			obj@faultdiagnosis.System(spatialDomain, {A0, A1}, parentArgs{:}, unmatched{:});
			
			obj.alpha = sysparser.Results.alpha;
			obj.beta = sysparser.Results.beta;
			obj.gamma = sysparser.Results.gamma;
			
% 			for par = fieldnames(prsr.Results)'
% 				obj.(par{1}) = prsr.Results.(par{1});
% 			end
			
		end
		
		function set.b0(obj, b0)
			obj.B_2(1,:) = b0;
		end
		function set.b1(obj, b1)
			obj.B_2(2,:) = b1;
		end
		function set.b(obj, b)
			obj.B_1(1).coefficient(1,:) = -1 / obj.alpha * b;
		end
		function set.g0(obj, g0)
			obj.G_2(1,:) = g0;
		end
		function set.g1(obj, g1)
			obj.G_2(2,:) = g1;
		end
		function set.g(obj, g)
			obj.G_1(1,:) = - 1 / obj.alpha * g;
		end
		function set.e0(obj, e0)
			obj.E_2(1,:) = e0;
		end
		function set.e1(obj, e1)
			obj.E_2(2,:) = e1;
		end
		function set.e(obj, e)
			obj.E_1(1,:) = -1 / obj.alpha * e;
		end

		%% GETTERS
		function e2 = get.e2(obj)
			e2 = obj.E_3;
		end
		function c20 = get.c20(obj)
			c20 = obj.C_2(:,2);
		end
		function c30 = get.c30(obj)
			c30 = obj.C_3(:,2);
		end
		function b = get.b(obj)
			b = - obj.alpha * obj.B_1(1).coefficient(1,:);
		end
		function g = get.g(obj)
			g = - obj.alpha * obj.G_1(1,:);
		end
		function e = get.e(obj)
			e = - obj.alpha * obj.E_1(1,:);
		end
		function c10 = get.c10(obj)
			c10 = obj.C_1(:, 2);
		end
		function q00 = get.q00(obj)
			q00 = obj.K_1(1,2);
		end
		function q01 = get.q01(obj)
			q01 = obj.K_1(1,1);
		end
		function q10 = get.q10(obj)
			q10 = obj.K_2(2,2);
		end
		function q11 = get.q11(obj)
			q11 = obj.K_2(2,1);
		end
		function b0 = get.b0(obj)
			b0 = obj.B_2(1,:);
		end
		function b1 = get.b1(obj)
			b1 = obj.B_2(2,:);
		end
		function g0 = get.g0(obj)
			g0 = obj.G_2(1,:);
		end
		function g1 = get.g1(obj)
			g1 = obj.G_2(2,:);
		end
		function e0 = get.e0(obj)
			e0 = obj.E_2(1,:);
		end
		function e1 = get.e1(obj)
			e1 = obj.E_2(2,:);
		end
		%% Methods - trajectory planning
		[ m, n, z, t, series, ws_sym] = trjWoittennek( obj, varargin);
		[ m, n, z, t, series, ws_sym] = trjSpectral( obj, varargin );
		[ m, n, z, t, series, ws_sym] = trjTaylor( obj, varargin );
		[ W, U ] = taylorExpansion( obj, N );
		
		function [ m, n, z, t] = trajectoryPlanning( obj, varargin)
			
			%% Default values:
			Nz = misc.ifemptyelse(obj.approximation.N, 100);
			b  = signals.gevrey.Bump();
			N  = 4;
			
			%% input parsen
			for k = 1:2:numel(varargin)
				switch varargin{k}
					case 'Nz'
						Nz = varargin{k+1};
					case 'b'
						b = varargin{k+1};
					case 'N'
						N = varargin{k+1};
					otherwise
						error(['Not implemented argument' varargin{k}]);
				end
			end
			
			[z.num, zL, zR] = obj.spatialdiscretization(dim.z);
			A = obj.A.K;
			B = obj.B.K;
			zeta = sym('zeta', 'real');
			
			%% computation of the transition matrix
			[Phi, F, z.sym] = misc.peanobakerseries_poly(A, N, zeta);
			
		end
		
		
		function [ m, n, z, t] = trjPlanning( obj, varargin)
			
			%% Default values:
			Nz = misc.ifemptyelse(obj.approximation.N, 100);
			b = signals.gevrey.Bump();
			N = 4;
			
			%% input parsen
			for k = 1:2:numel(varargin)
				switch varargin{k}
					case 'Nz'
						Nz = varargin{k+1};
					case 'b'
						b = varargin{k+1};
					case 'N'
						N = varargin{k+1};
					otherwise
						error(['Not implemented argument' varargin{k}]);
				end
			end
			
			[ m, n, z, t] = obj.trajectoryFunction( Nz, b, N );
			
			if nargout == 0
				figure(gcf);
				subplot(3,2,[1 2]);
				plot(t, n);
				xlabel('t')
				ylabel('n')
				
				subplot(3, 2, 3)
				misc.isurf(z, t, m(1,:,:));
				zlabel('m_1')
				
				subplot(3, 2, 4)
				misc.isurf(z, t, m(2,:,:));
				zlabel('m_2')
				
				[~, t, x] = obj.approximation.simulate(t, 'u', n);
				
				subplot(325)
				misc.isurf(z, t, x.');
				zlabel('x')
				
				subplot(3, 2, 6)
				misc.isurf(z, t, squeeze(m(obj.valueIdx(1), :, :)) - x.');
				zlabel('\Delta')
			end
		end
		
		function createDiscretization(obj)
			obj.discretization = faultdiagnosis.parabolic.FEApproximation(obj);
		end
	end
	methods (Access = public)
		
		function signalmodel = SignalModel(obj)
			signalmodel = SignalModel@faultdiagnosis.System(obj);
		end
	end
	methods (Access=protected)
		function [y, t, x] = solve(obj, t, v, varargin)
			[y, t, x] = lsim(obj.discretization.stsp, v, t, varargin{:});
			x = x.';
		end		
		function A = get_A(obj, A)
		end
		function C = get_C(obj, C)
		end
		function C0 = get_C0(obj, C0)
		end
		function C1 = get_C1(obj, C1)
		end
		
	end
end