  function [ m, n, z, t, series, ws_sym] = trjWoittennek( obj, Nz, b, N)
%%TRJWOITTENNEK - Trajectoryplanning for partial derivative equations
% Calculates a control for the given distributed-parameters System by
% Seriesexpansion. Detailed description for trajectory planning can be
% found in:
%   Trajektorienplanung für die Steuerung gewisser linearer Systeme mit
%   verteilten Parametern, Joachim Rudolph und Frank Woittenek,
%   at-Atuomatisierungstechnik 54 (2006)


% [ m, n, m_k, n_k, seriesTerms] = trjPlanning( A, B, C0, C1, D, b, s, z, t, N, z0, z1 )
%   Obligatory input arguments:
%       A: System-Operator Matrix \in C[\dt]^(nxn); with n is the number of
%           states. This Matrix has to be a polynomial Matrix, with:
%                size(A) = [n, n, n_{A,s}]; while n_{A,s} is the highest derivative
%                wrt time
%       
%       B: Input-Operator Matrix \in C[\dt]^(nxp); with p is the number of
%           inputs. It has to be a polynomial Matrix with:
%               size(B) = [n, n, n_{B,s}; while n_{B,s} is the highest
%               derivative wrt time
%
%       C0: Matrix for left Border \in C^(nxn)
%       C1: Matrix for right Border \in C^(nxn)
%
%       D: Matrix for Boundarycontrol \in C^(pxq)
%           C0, C1, D satisfy the Boundaryconditionequation:
%           C0 m(z0,t) + C1 m(z1,t) + D u(t) = 0
%
%       b: Basic input vector \in G^(px1)_alpha; with G is the Gevrey-Class
%          with Gevrey-Order alpha
%
%   Optional input arguments:
%       [z]: Spatial variable
%       [N]: Number of series terms to be calculated
%       [z0]: value for left boundary [defualt is 0]
%       [z1]: value for right boundary [default is 1]
%
%   Output arguments:
%       m: distributed state for the Trajectory as function with respect to
%           z and t
%       n: control for the calculated trajectory as function with respect
%           to t
%       m_k: series terms for distributed state as cell array as fuction
%           with respect to z and t
%       n_k: series terms for control as cell array as funtino with respect
%           to t
%       seriesTerms: cell-array with the series terms for W, U and b; ;W_k
%       and U_k are function with respect to z and b_k is a function with
%       respect to t

    %% Discretization parameters
    dim.t = b.N;                  
    dim.z = Nz;
    dim.p = obj.p.b;
    dim.n = obj.n;
    dim.N = N;

    clear Nz N

    %% Lokale Variablen bereitlegen
    [z, zL, zR] = obj.spatialdiscretization(dim.z);
    A = obj.A.K;
    B = obj.B.K;
    
    syms zz0 zeta S
  
    counter = dim.N + dim.N;
    pbar = misc.ProgressBar('name', 'Trajectory Planning (Woittennek)', ...
        'terminalValue', counter, ...
        'steps', counter, ...
        'initialStart', true);

    %% Calculation of the Transistionmatrix with Powerseries 
     Phi_z=sym(zeros([dim.n, dim.n, dim.N])); % dimensions: ( states, states, polynomial degree)
     Psi_z=sym(zeros([dim.n, dim.p, dim.N])); % dimensions: ( states, inputs, polynomial degree)

    % =========================================================================
    % Transistionmatrix overline{Phi}:
    if obj.A.IsConstant
        %use if A contains only constant coefficients
        Phi_z(:,:,1) = expm(A(:,:,1)*(zz-zL));
        zz = sym('z');
    else
        %use if A contains z-dependent coefficients
        Phi_z(:,:,1) = misc.peanobakerseries_sym(A(:,:,1), zL, 20); 
        zz = obj.A.z;
    end

    % =========================================================================
    % PSI-Matrix overline{Psi}:
    Phi_int = subs(Phi_z(:,:,1), zz, zz - zeta);
    B = misc.ifemptyelse(B(:,:,1), zeros(obj.n, obj.p.b));
    Psi_z(:,:,1) = int(Phi_int * subs(B, obj.B.z, zeta), zeta, zL, zz);

    % =========================================================================
    % hat{Phi}_k; hat{Psi}_k

    for kN = 2 : dim.N
       pbar.raise();
       
       dA = min(size(A, 3), kN);
       tmp_Phi=sym(zeros(dim.n, dim.n));
       tmp_Psi=sym(zeros(dim.n, dim.p));

       for i=2:dA
          tmp_Phi = tmp_Phi + A(:,:,i) * Phi_z(:,:,kN-i+1);    
          tmp_Psi = tmp_Psi + A(:,:,i) * Psi_z(:,:,kN-i+1);
       end

       if size(B,3)>=kN
          tmp_Psi = tmp_Psi + B(:,:,kN); 
       end

       Phi_z(:,:,kN) = int(Phi_int * subs(tmp_Phi, zz, zeta), zeta, zL, zz);
       Psi_z(:,:,kN) = int(Phi_int * subs(tmp_Psi, zz, zeta), zeta, zL, zz);
    end

    % =========================================================================
    % Convert coefficient matrices into polynomial matrices
    Phi = misc.powerSeries2Sum(Phi_z, S);
    Psi = misc.powerSeries2Sum(Psi_z, S);  

    %% Calculation of Matrix Q(xi) and R(xi)
    [C0, C1, C2, D] = obj.makeTrjBoundaryCondition();

    if size(D,2) == 0
        D = sym(zeros(dim.n, dim.p));
    end
    
    % TODO: TESTEN DER TERME ABHÄNGIG VON C2!!!
    
    Q  = C0 * subs(Phi, zz ,zL) + C1 * subs(Phi, zz, zR) - int( C2 * Phi, zL, zR, zz);
    R  = C0 * subs(Psi, zz, zL) + C1 * subs(Psi, zz, zR) + sym(D) - int( C2 * Psi, zL, zR, zz);
    U_ = det(Q);

    %  adjQ=subs(adj(pol(Q)),S);

    W = misc.polynomial2coefficients(-Phi * misc.adj(Q) * R + U_ * Psi, S, dim.N);
    U = misc.polynomial2coefficients(U_, S, dim.N);

    %% Derivation of  Gevrey-Function
    % Vorzeichenwechsel in bk damit es mit der Implementierung der
% Systembeschreibung übereinstimmt



%% Steuerung und Verlauf der Zustände berechen
    % wk = zeros(B.n, Nz, B.m, N); % (n, z, m, k)
    % bk = zeros(B.m, Nt, N);      % (m, t, k)

    wk = zeros(dim.n, dim.p, dim.N, dim.z); % (n, m, k, z)
    wwk= zeros(dim.n, dim.p, dim.N, dim.z); % (n, m, k, z)
    uk = zeros(1, dim.N);
    mk = zeros(dim.n, dim.z, dim.t, dim.N);  % (n, z, t, k)
    nk = zeros(dim.p, dim.t, dim.N);      % (m, t, k)
    bk = zeros(dim.p, dim.t, dim.N);      % (m, t, k)

    sigmak = zeros(1, dim.N);
    bk_ = zeros(dim.p, dim.t, dim.N);      % (m, t, k)
    uk_ = zeros(1, dim.N);
    nk_ = zeros(dim.p, dim.t, dim.N);      % (m, t, k)

    WW = reshape(W, dim.n * dim.p, dim.N);   
    
    t = b.maket();
    for kN = 1 : dim.N
        pbar.raise();
        
        %Numerische Auswertung:
        z = z(:)';
        wk(:,:,kN,:) = reshape(double(subs(WW(:,kN), zz, z)), dim.n, dim.p, dim.z);
        uk(kN) = double(U(kN));

        bk(:, :, kN ) = b.eval(kN-1, t).';  

        % as the multiplication of n-d matrices is not defined, use for
        % loops for the calculation wk * bk
        for kn = 1:dim.n
            for kp = 1:dim.p
                mk(kn, :, :, kN) = squeeze(mk(kn, :, :, kN)) ...
                    + squeeze(permute(wk(kn, kp, kN, :), [3 2 4 1]))*bk(kp,:,kN); %(n, m, k, z) -> (z, m, n, k)        
            end
        end   

        nk(:,:,kN) = uk(kN)*bk(:,:,kN); % TODO hier wurde das minus entfernt, so dass die Trajektorienplanung stimmt
    end
   
    m = sum(mk, 4);
    n = sum(nk, 3);
    n_ = sum(nk_, 3);

    pbar.stop();

    series.wk=wk;
    series.uk=uk;
    series.bk=bk;
    series.mk = mk;
    series.nk = nk;
    series.nk_ = nk_;
    series.n_ = n_;
    series.z = z;
    series.t = t;

    ws_sym=misc.ws2struct;
  end        