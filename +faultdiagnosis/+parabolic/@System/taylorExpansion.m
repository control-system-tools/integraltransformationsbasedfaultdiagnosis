function [ W, U] = taylorExpansion( obj, N )
%% TAYLOREXPANSION
% Bestimmung der differenziellen parametrierung mittels der
% (Q, R) Schreibweise von Joachim Rudolph:
%
%   u(s)   = U(s) b(s)
%   w(z,s) = W(z,s) b(s)
%
%            W(z,s) = - Phi(z, s) adj( Q(s) ) R(s)
%            U(s)   = det( Q(s) )
%
% Die differentielle Parametrierung erhält man durch die
% Reihenentwicklung (Taylorreihe)
%   
%   U(s)   = sum Uk s^k
%   W(z,s) = sum Wk(z) s^k
%
%   

%% Berechnen der Reihenglieder
    syms s z

    % Systemoperator matrix im Laplacebereich
    A = obj.A.laplaceOperator;       

    % Bestimmung der Fundamentalmatrix:
    Phi = expm(A * (z - obj.BoundaryCondition(1).z));
        % Symbolisches umstellen der Fundamentalmatrix damit diese
        % in der gewünschten Form vorliegt:
        Phi = rewrite(Phi, 'sinh');     % Exponentialfunktionen durch sinh ausdrücken
        Phi = simplify(Phi);            % die dadurch komplizierten Ausdrücke wieder vereinfachen
        Phi = symfun(Phi, z);           % und als Funktion bezgl. z definieren.

    [C0, C1, D] = obj.makeTrjBoundaryCondition();

    Q = C0 * Phi(0) + C1 * Phi(1);

    U.sym    = det(Q);
    U.taylor = misc.pylor(U.sym, N);
    % Die Koeffizienten aus dem Polynom herausholen und
    % symbolischen Vektor in numerischen umwandeln.
    U.poly   = squeeze([double(misc.polynomial2coefficients(U.taylor, s))]);

    %% calculating W
    %   Für W(z,s) kann Matlab die transzendenten Funktionen nicht
    %   in eine Taylorreihe entwickeln. Daher wird auf Python
    %   ausgewichen. 

    % Matrix soweit wie möglich in Matlab berechnen.
    W.sym = - Phi(z) * misc.adj(Q) * D;
    W.taylor = sym(zeros(size(W.sym)));

    % Für jeden Matrix Eintrag die Taylorreihe mit dem Pythonskript
    % taylor.py berechnen
    for k = 1:numel(W.sym)
        W.taylor(k) = misc.pylor(W.sym(k), N);
    end

    % Koeffizientenfunktionen (sind von z abhängig) bestimmen und
    % als Funktionen ablegen.
    W.poly   = misc.polynomial2coefficients(W.taylor, s, N);
    W.fun    = symfun(W.poly, z);  

    clear z s   % wird nicht mehr benötigt und symbolisches z und numerisches könnten verwechselt werden            

end
