classdef DetectionKernel < faultdiagnosis.parabolic.Kernel
	
	methods
		function obj = DetectionKernel(sys, optArgs)
			%DETECTIONKERNEL Construct an instance of this class
			%   Detailed explanation goes here
			arguments
				sys (1,1) faultdiagnosis.System;
				% optional arguments for the kernel definition
				optArgs.faultModel (:,1) signals.SignalModel; %TODO: Wieso kann man hier ein Fehlermodell übergeben???
				optArgs.disturbanceModel (:,1) signals.SignalModel;
				optArgs.delta (:,1) double; %TODO delta = bounds of the disturbance
				optArgs.N_s (1,1) double;
				optArgs.truncate (1,1) double;
			end
			kernelArgsList = misc.struct2namevaluepair(optArgs, "fieldNames", ["faultModel", ...
				"disturbanceModel", "delta", "N_s", "truncate"]);
			obj@faultdiagnosis.parabolic.Kernel(sys, kernelArgsList{:});
			
			refTrjArgsList = misc.struct2namevaluepair(optArgs, "fieldNames", [...
				"theta", "delta", "method", "FF", "normalizeTheta"]);
			
% 			[firFilter, modulatingFunctions, solution] = obj.compute(I, refTrjArgsList{:});
% 			obj.solution = misc.structMerge(solution, modulatingFunctions);
% 			obj.firFilters = firFilter;
		end
	end
	
	methods (Access = public)
		function [] = changeFirFilterStepSize(obj, I)
			obj.firFilters = obj.generateFirFilters(I, obj.solution, "type", "detection");
		end
		
		function [ioIntegralKernels, solution, relInc, maxRelInc, iter] = compute(obj, I, optArgs)
			arguments
				obj,
				I (1,1) quantity.Domain;
				optArgs.basicVariable (:,1) signals.BasicVariable;	
				optArgs.update (1,1) logical = true;
				optArgs.relIncTol (1,1) double = obj.relIncTol;
			end
			
			if ~isfield(optArgs, "basicVariable")
				obj.report("Reference trajectory planning with the stationary gain approach.")
				[optArgs.basicVariable, alpha, theta] = ...
					obj.referenceTrajectoryPlanningStationaryGain ( I );
			end		
			% solve the kernel equations based on the differential parametrization with the given
			% basic variable
			obj.report("Compute the solution for the kernel equations.")
			if isnan( optArgs.relIncTol )
				 [solution, relInc, maxRelInc, iter] = obj.solve( optArgs.basicVariable);
			else
				[solution, relInc, maxRelInc, iter] = obj.solve( optArgs.basicVariable, ...
					 "relativeIncrementTolerance", optArgs.relIncTol);	
			end
			
			% computation of the integral kernels for the fault detection equations
			obj.report("Compute the integral kernels for the fault detection equation.")
			ioIntegralKernels = obj.computeModulatingFunctions(I, solution);

			if optArgs.update
				obj.solution = solution;
				obj.ioIntegralKernels = ioIntegralKernels;
			end
			
		end		

		function [phi, parameters, solution, Theta] = ...
				referenceTrajectoryPlanningL2L2PerformanceIndex(obj, I, optArgs)
			% REFERENCETRAJECTORYPLANNINGL2L2PERFORMANCEINDEX 
			% reference trajectory planning for the basic variable in view of a L2/L2 - performance
			% index.
			%
			%	[basicVariable, alpha, theta] = ...
			%		referenceTrajectoryPlanningL2L2PerformanceIndex(obj, I, optArgs) 
			%
			% Computation of the reference trajectory mu* on the basis of sensitivity measures and
			% a performance index. It uses the weighted L2 sensitivity measures
			%
			%	S_f = || W_f m_f ||^2_2	and		S_d = || W_d m_d ||^2_2.
			%
			% Since S_f measures the sensitivity w.r.t. the faults and S_d the sensitivity w.r.t.
			% the disturbances, it becomes obvious that a compromise must be met. For this, the
			% performance index
			%
			%	J = S_d / S_f
			%
			% is introduced. In order to find a suitable mu*, impose the ansatz
			%
			%	mu* = theta(tau) basis^T(tau) * alpha
			%
			% At this, theta is a Gevrey function of apropriate order, basis(tau) is a vector of
			% basis functions, e.g., fourier basis or polynomial basis and alpha is a vector of
			% coefficients to be determined. This ansatz allows to express the sensitivity measures
			% in quadratic forms of alpha
			%
			%	S_f = alpha^T Xi alpha	and		Sd = alpha^T Upsilon alpha
			%
			% With this, the minimization problem
			%
			%	J^* = min Sd/ Sf
			%
			% can be solved explicitly, since alpha* = argmin S_d/S_f is the eigenvector w* of the
			% generalized eigenvalue problem
			%
			%	Upsilon w = lambda Xi w,	
			%
			% assigned to the minimal eigenvalue lambda*.
			
			arguments
				obj,
				I (1,1) quantity.Domain;
				optArgs.theta (:,1) signals.BasicVariable;
				optArgs.normalizeTheta (1,1) logical = false;
				optArgs.basis (:,1) signals.BasicVariable = ...
					signals.Monomial.taylorPolynomialBasis( I, "numDiff", obj.N_s );
				optArgs.Wf double = eye( obj.sys.n.f );
				optArgs.Wd double {} = eye( obj.sys.n.d_bounded );
				optArgs.SfDesired (1,1) double = 1;
				optArgs.Theta
				optArgs.relIncTol (1,1) double = obj.relIncTol;
			end
			
			mustBe.size( optArgs.Wf, [obj.sys.n.f, obj.sys.n.f]);
			mustBe.size( optArgs.Wd, [obj.sys.n.d_bounded, obj.sys.n.d_bounded]);
						
			if ~isfield(optArgs, "theta")
				theta = signals.GevreyFunction( ...
					'order', 1.999, "diffShift", 1, "numDiff", obj.N_s, 'timeDomain', I);
			else
				theta = optArgs.theta;
			end
			differentialExpressions = obj.differentialExpressions();
			
			if optArgs.normalizeTheta
				% with this the theta function can be normalized, so that || N(\tau) || = 1
				% This can have some time numerical effects
				thetaN = obj.differentialExpressions.varpi.applyTo(theta, "n", obj.N_s);
				theta.setGain(1 / thetaN.l2norm);
			end
			
			% Compute the basis functions for m_f:
			% The components of the reference trajectory are given by
			%
			%	phi_i = theta * basis_i * a_i
			%
			% respectively:
			%
			%	phi = Theta * a
			%
			%		            [ basis_1,	0,			0, ...
			%   Theta = theta * [   0,		basis_2,	0, ...
			%			        [   0,		0,			basis_3, ....
			%
			if ~isfield( optArgs, "Theta")
				Theta = kron( eye( obj.sys.n.y ), optArgs.basis' ) * theta;
			else
				Theta = optArgs.Theta;
			end
			% compute the basis functions for the integral kernel
			%	m_f(tau) = xi(tau) * a
			
			%%
			xi = differentialExpressions.X.applyTo( Theta );
			% compute the symmetric matrix for the sensitivity measure:
			%	S_f = a' Xi a
			Xi = int( xi.' * optArgs.Wf * optArgs.Wf.' * xi );
			% compute the basis functions for the disturbance filter:
			% m_d(tau) = upsilon(tau) * a
			upsilon = differentialExpressions.Y.applyTo( Theta );
			% compute the matrix for the sensitivity measure:
			%	S_d = a' * Upsilon * a
			Upsilon = int( upsilon.' * optArgs.Wd * optArgs.Wd.' * upsilon );
			
			% performance index:
			%	J = S_d / S_f
			%
			% Optimiering: min J so that S_f = 1
			%	L = a' Upsilon a - lmbda( a' Xi a - 1 )
			%  \nablda L = 2 Upsilon a - lmbda Xi a
			%
			%	=>  Upsilon a = lmbda Xi a
			%         a' Xi a = 1
			[V, Lmbda] = eig( Upsilon, Xi );
			% verify that all eigen values are real and not negative
			assert( isreal( Lmbda(:) ) )
			
			% compute the valid index range: 
			eigValues = diag( Lmbda );
			idxLambda = eigValues >= 0 & eigValues < inf & diag( V' * Xi * V ) > eps;
			
			% set all not allowed eigenvalues to inf
			eigValues( ~idxLambda ) = inf;
			
			% get the minimal eigenvalue
			[~,i] = min( eigValues );
			% scale wMin so that ||mf || = 1
			% compute the optimal solution for the coefficients a
			wMin = V(:,i);
			% scale the eigenvector so that S_f = 1
			aStar = wMin / sqrt( wMin.' * Xi * wMin ) * sqrt( optArgs.SfDesired );
			Sf = aStar.' * Xi * aStar;
			Sd = aStar.' * Upsilon * aStar;
			JStar = Sd / Sf;
			% verify that S_f = 1 and JStar is non negative:
			%assert( numeric.near( Sf, 1, 1e-3 ) )
			assert( JStar >= 0 )
			
			% compute the reference trajectory
			phi = Theta * aStar;
			phi.setName("\phi");
			
			parameters.J = JStar;
			parameters.Sf = Sf;
			parameters.Sd = Sd;
			parameters.a = aStar;
			
			%
			[ioIntegralKernels, solution] = obj.compute(I, "basicVariable", phi, ...
				"relIncTol", optArgs.relIncTol);
			
			if obj.verbose
				for k = 1:obj.sys.n.f

					disp( l2norm( ioIntegralKernels.Mf(k)))

				end
			end
			
		end
	
		function [refTrajectory, alpha, theta, mBarf] = referenceTrajectoryPlanningStationaryGain(obj, I, optArgs)
			% referenceTrajectoryPlanningStationaryGain plan a suitable reference trajectory with a specific
			% stationary gain
			% [refTrajectory, alpha, theta] = referenceTrajectoryPlanningStationaryGain(obj, I, optArgs)
			% Computes the reference trajectory mu^d so that the stationary gain
			%	Mf = int( mf, 0, T)
			% is close to the one vectors. This can be interpreted that the stationary gain for
			% each fault should be 1, so that constant faults can be estimated in its absolute
			% value.
			% For this, the ansatz
			%	mu^d = theta * alpha
			% is used, where theta is a Gevrey function of suitable order and alpha is the
			% degree of freedom to be computed.
			% With this,
			%	Mf = X0 * int(theta) * gamma
			% follows, where X0 is the first term of the differential parametrization for mf.
			% Solving this for gamma
			%	gamma = pinv(X0) * Mf / int(theta)
			% can be achieved using the pseudo inverese. If X0 has rang defects, so that the
			% system of equations does not have a solution, gamma will be chosen as close the
			% desired value Mf as possible. However, this does not ensure that Mf will be
			% nonzero for every entry!
			%
			% The temporal domain for the computation is specified by I, and with "optArgs" the
			% following name-value-pair arguments can be set:
			%	"theta": the gevrey function for the basis variable
			%	"normalizeTheta": flag to decide if "theta" should be normalized to the considered
			%	kernels.
			%	"Mf" : the stationary gain that should be achieved.
			%	"forceInitialPointToBeZero": an option to force the initial point of the
			%	optimization for the trade-off design to be zero. Otherwise, a heuristic is used to
			%	decide whether the zero or the solution of a quadratic minimization should be used.
			
			arguments
				obj
				I (1,1) quantity.Domain;
				optArgs.theta (:,1) signals.BasicVariable;
				optArgs.normalizeTheta (1,1) logical = false;
				optArgs.Mf (:,1) double = ones(obj.sys.n.f, 1);
				optArgs.extendedDOF (1,1) double = 4;
				optArgs.forceInitialPointToBeZero (1,1) logical = false;
			end
			
			if ~isfield(optArgs, "theta")
				theta = signals.GevreyFunction( ...
					'order', 1.999, "diffShift", 1, "numDiff", obj.N_s, 'timeDomain', I);
			else
				theta = optArgs.theta;
			end
			de = obj.differentialExpressions();
			
			if optArgs.normalizeTheta
				% with this the theta function can be normalized, so that || N(\tau) || = 1
				%
				thetaN = obj.differentialExpressions.varpi.applyTo(theta, "n", obj.N_s);
				theta.setGain(1 / thetaN.l2norm);
			end
			
			X0 = de.X(1).coefficient.double;
			
			for i = 1:obj.sys.n.f
				assert( sum( abs( X0(i,:) ) ) > 0, "The fault f_i, i=" + num2str(i) + ...
					" is not detectable. The corresponding row in M_f0 is zero.");
			end
			
			%%
			phi = signals.Sinusoidal.fourierBasis( I, "order", optArgs.extendedDOF, ...
					"numDiff", obj.N_s);
			
			basis = phi*theta;
						
			Y = X0 * kron( int( basis.f )', eye( obj.sys.n.y ) );
			
			sY = svd(Y);
			if numel(sY) >= obj.sys.n.y+1
				pinvY = pinv(Y, mean( sY( obj.sys.n.y:obj.sys.n.y+1 ) ) );
			else
				pinvY = pinv(Y);
			end
			
			mBarf = Y * pinvY * optArgs.Mf;
			
			alpha0 = pinvY * mBarf;
			
			if optArgs.extendedDOF > 0
% 				Z = orth( eye( size(Y,2) ) - pinvY * Y );
				Z = null( Y );

				Theta = kron( basis', eye(obj.n.u));

				refTrajectory = Theta * alpha0;
				rB0 = obj.delta' * int( abs( obj.sys.G_bounded' * de.Xd.applyTo( refTrajectory )));

				c = obj.sys.G_bounded' * de.Xd.applyTo( Theta ) * alpha0;
				b = ( obj.sys.G_bounded' * de.Xd.applyTo( Theta) * Z );

				aStar = faultdiagnosis.thresholdOptimizerL1(obj, c, b, obj.delta, ...
										"use0asInitialPoint", optArgs.forceInitialPointToBeZero);

				alpha = (alpha0 + Z * aStar);
				
				refTrajectory =  Theta * alpha;

			else
				alpha = pinv(X0) * optArgs.Mf / int(quantity.Discrete( theta.fun() ), I);
				refTrajectory = alpha * theta;
			end
			
			if any( alpha == 0 )
				warning("The measurement " + misc.vector2string( (1:obj.sys.n.y) * ...
					(alpha == 0) ) + " has no contribution for the detection");
			end
			
			[refTrajectory.name] = deal("\mu");
		end

	end % methods (Access = public)	
end

