classdef ScalarParabolic < faultdiagnosis.System
	% SYSTEM described by parabolic PDEs
	% With this class diffusion-convection-reaction systems
	%      dt v(z, t) = dz (alpha(z) dz v(z, t)) + beta(z) dz v(z, t) + gamma(z) v(z, t) 
	%                   + h1(z) w(t) + b1(z) u(t) + g1(z) d(t) + e1(z) f(t)
	% with spatially-varying coefficients are described. The systemvariable
	% v(z,t)\in\mathbb{R}^n is considered on the domain
	% (z,t)\in\Omega\times\mathbb{R}^+, with the spatial domain \Omega=(z0, z1).
	%  The boundary conditions of the system are given by
	%     q01 dz v(0,t) + q00 v(0, t) + h2 w(t) = b2 u(t) + g2 d(t) + e2 f(t)
	%     q11 dz v(1,t) + q10 v(1, t) + h3 w(t) = b3 u(t) + g3 d(t) + e3 f(t)
	% with a coupled ODE
	%	dt w = F w  + < l1, v > + l2 v(0) + l3 v(1) + g4 d(t) + e4 f(t) + b4 u(t)
	% and the output equation with
	%     y = int_0^1 c1(z) v(z,t) dz + c2 v(0,t) + c3 v(1,t) + c4 w(t) + e5 f(t) + g5 d(t)
	%
	% The scalar parabolic system is mapped to a system of PDE wrt to space, in the form given by
	% the basis class faultdiagnosis.System. The auxiliary state is chosen as
	%
	%            / dz v(z,t) \
	%	x(z,t) = |            |
	%            \   v(z,t)  /
	%
	%
	%
	%
	% === usage ===
	%
	% === Spatial system of first order ===
	
	properties (SetAccess = protected)
		% Parameters for the pde
		alpha (1,1) quantity.Discrete;		% diffusion parameter
		beta (1,1)  quantity.Discrete;		% convection parameter
		gamma (1,1) quantity.Discrete;		% reaction parameter

		% control inputs:
		b1 (1,:) quantity.Discrete;		
		b2 (1,:) double;
		b3 (1,:) double;
		b4 (:,:) double;
		
		% disturbance inputs
		g1 (1,:) quantity.Discrete;		
		g2 (1,:) double;
		g3 (1,:) double;
		g4 (:,:) double;
		g5 (:,:) double;

		% faulty inputs
		e1 (1,:) quantity.Discrete;		
		e2 (1,:) double;
		e3 (1,:) double;
		e4 (:,:) double;
		e5 (:,:) double;
		
		% Parameters for the boundary conditions
		q01 (1,1) double;
		q00 (1,1) double;
		q11 (1,1) double;
		q10 (1,1) double;
		
		% ode parameters
		l1 (:,1) quantity.Discrete;
		l2 (:,1) double;
		l3 (:,1) double; 
		h1 (1,:) quantity.Discrete;
		h2 (1,:) double;
		h3 (1,:) double;
		
		% Output parameters
		c1 (:,1) quantity.Discrete;
		c2 (:,1) double;
		c3 (:,1) double;
		c4 (:,:) double;
	end
	
	methods
		%% Constructor
		function obj = ScalarParabolic(spatialDomain, o)
			arguments
				spatialDomain (1,1) quantity.Domain;
				% system parameters
				o.alpha (1,1) quantity.Discrete = quantity.Discrete.ones(1, spatialDomain);
				o.beta (1,1) quantity.Discrete = quantity.Discrete.zeros(1, spatialDomain);
				o.gamma (1,1) quantity.Discrete = quantity.Discrete.zeros(1, spatialDomain);
				% boundary conditions
				o.q00 (1,1) double = 0;
				o.q10 (1,1) double = 0;
				o.q01 (1,1) double = 1;
				o.q11 (1,1) double = 1;
				% control inputs
				o.b1 (1,:) quantity.Discrete;
				o.b2 (1,:) double;
				o.b3 (1,:) double;
				o.b4 (:,:) double;
				% disturbance inputs
				o.g1 (1,:) quantity.Discrete;
				o.g2 (1,:) double;
				o.g3 (1,:) double;
				o.g4 (:,:) double;
				o.g5 (:,:) double;
				o.G_deterministic double;
				o.G_bounded double;
				o.G_stochastic double;
				% faulty inputs
				o.e1 (1,:) quantity.Discrete;
				o.e2 (1,:) double;
				o.e3 (1,:) double;
				o.e4 (:,:) double;
				o.e5 (:,:) double;
				% ode parameters
				o.F (:,:) double = [];
				o.l1 (:,1) quantity.Discrete;
				o.l2 (:,1) double;
				o.l3 (:,1) double; 
				o.h1 (1,:) quantity.Discrete;
				o.h2 (1,:) double;
				o.h3 (1,:) double;
				% outputs
				o.c1 (:,1) quantity.Discrete;
				o.c2 (:,1) double;
				o.c3 (:,1) double;
				o.c4 (:,:) double;
				% other parameters
				o.N_s (1,1) double = 9;
				o.recompute (1,1) logical = false;
				o.truncate (1,1) logical = false;
			end
			
			% verify if a valid boundary condition is set.
			assert( abs(o.q00) + abs(o.q01) > 0)
			assert( abs(o.q10) + abs(o.q11) > 0)
				
			% create the system parameters for the required system form:			
			A0 = [- ( o.alpha.diff() + o.beta ) / o.alpha, -o.gamma / o.alpha; ...
				                                1        ,    0  ];
			A1 = [0, 1 / o.alpha ; ...
				  0,    0  ];
			
			assert( o.q11 >= 0 )
			assert( o.q01 >= 0 )
			  
			K0 = [o.q01 o.q00; zeros(1, 2)];
			K1 = [zeros(1, 2); o.q11 o.q10];
						
			% determine the number of inputs and outputs:
			dims.u = misc.getDimensions(o, ["b1", "b2", "b3", "b4"], 2);
			dims.d = misc.getDimensions(o, ["g1", "g2", "g3", "g4"], 2);
			dims.f = misc.getDimensions(o, ["e1", "e2", "e3", "e4"], 2);
			dims.y = misc.getDimensions(o, ["c1", "c2", "c3"], 1);
			dims.w = size( o.F, 1 );
					
			prsr = misc.Parser();
			% ode couplings
			prsr.addOptional('l1', quantity.Discrete.zeros( [dims.w, 1], spatialDomain));
			prsr.addOptional('l2', zeros( dims.w, 1 ));
			prsr.addOptional('l3', zeros( dims.w, 1 ));
			prsr.addOptional('h1', quantity.Discrete.zeros( [1, dims.w], spatialDomain));
			prsr.addOptional('h2', zeros( 1, dims.w ));
			prsr.addOptional('h3', zeros( 1, dims.w ));
			% control inputs
			prsr.addOptional('b1', quantity.Discrete.zeros( [1, dims.u], spatialDomain));
			prsr.addOptional('b2', zeros(1, dims.u));
			prsr.addOptional('b3', zeros(1, dims.u));
			prsr.addOptional('b4', zeros(dims.w, dims.u));
			% disturbance inputs
			prsr.addOptional('g1', quantity.Discrete.zeros( [1, dims.d], spatialDomain));
			prsr.addOptional('g2', zeros(1, dims.d));
			prsr.addOptional('g3', zeros(1, dims.d));
			prsr.addOptional('g4', zeros(dims.w, dims.d));
			prsr.addOptional('g5', zeros(dims.y, dims.d));
			prsr.addParameter('G_deterministic', zeros(dims.d, 0), ...
				@(A) validateattributes(A, {'numeric'}, {'nrows', dims.d} ));
			prsr.addParameter('G_bounded', zeros(dims.d, 0), ...
				@(A) validateattributes(A, {'numeric'}, {'nrows', dims.d} ));
			prsr.addParameter('G_stochastic', zeros(dims.d, 0), ...
				@(A) validateattributes(A, {'numeric'}, {'nrows', dims.d} ));
			% faulty inputs
			prsr.addOptional('e1', quantity.Discrete.zeros( [1, dims.f], spatialDomain));
			prsr.addOptional('e2', zeros(1, dims.f));
			prsr.addOptional('e3', zeros(1, dims.f));
			prsr.addOptional('e4', zeros(dims.w, dims.f));
			prsr.addOptional('e5', zeros(dims.y, dims.f));
			% outputs
			prsr.addOptional('c1', quantity.Discrete.zeros( [dims.y, 1], spatialDomain)	, ...
				@(A) validateattributes(A, {'quantity.Discrete'}, {'size', [dims.y, 1]} ));
			prsr.addOptional('c2', zeros(dims.y, 1), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dims.y, 1]} ));
			prsr.addOptional('c3', zeros(dims.y, 1), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dims.y, 1]} ));
			prsr.addOptional('c4', zeros(dims.y, dims.w), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dims.y, dims.w]} ));
			args = namedargs2cell(o);
			prsr.parse(args{:});			

			obj@faultdiagnosis.System(spatialDomain, signals.PolynomialOperator({A0, A1}, ...
				"truncate", o.truncate), K0, K1, ...
				'F', o.F, ...
				'H_1', [ -1 / o.alpha * prsr.Results.h1; zeros(1, dims.w)], ...
				'H_2', [prsr.Results.h2; ...
				        prsr.Results.h3], ...
				'L_1', [ zeros( dims.w, 1 ), prsr.Results.l1], ...
				'L_2', [ zeros( dims.w, 1 ), prsr.Results.l2], ...
				'L_3', [ zeros( dims.w, 1 ), prsr.Results.l3], ...
				'B_1', [ -1 / o.alpha * prsr.Results.b1; zeros(1, dims.u)], ...
				'B_2', [prsr.Results.b2; ...
				        prsr.Results.b3], ...
				'B_3', prsr.Results.b4, ...
				'G_1', [ -1 / o.alpha * prsr.Results.g1; zeros(1, dims.d)], ...
				'G_2', [prsr.Results.g2; ...
				        prsr.Results.g3], ...
				'G_3', prsr.Results.g4, ...
				'G_deterministic', prsr.Results.G_deterministic, ...
				'G_bounded', prsr.Results.G_bounded, ...
				'G_stochastic', prsr.Results.G_stochastic, ...
				'E_1', [ -1 / o.alpha * prsr.Results.e1; zeros(1, dims.f)], ...
				'E_2', [prsr.Results.e2; ...
				        prsr.Results.e3], ...
				'E_3', prsr.Results.e4, ...
				'C_1', [ zeros(dims.y, 1) prsr.Results.c1], ...
				'C_2', [ zeros(dims.y, 1) prsr.Results.c2], ...
				'C_3', [ zeros(dims.y, 1) prsr.Results.c3], ...
				'C_4', prsr.Results.c4, ...
				'N_s', o.N_s);
			
			obj.recompute = o.recompute;
			obj.alpha = o.alpha;
			obj.beta = o.beta;
			obj.gamma = o.gamma;
			obj.q00 = o.q00;
			obj.q10 = o.q10;
			obj.q01 = o.q01;
			obj.q11 = o.q11;
			
			for par = fieldnames(prsr.Results)'
				obj.(par{1}) = prsr.Results.(par{1});
			end
		end
		
		function discreteModel = createDiscreteModel(obj, optArgs)
			arguments
				obj;
				% this is a weighting factor for the consideration of the boundary conditions. It is
				% only important if dirichlet boundary conditions are present.
				optArgs.lv = 1; 
			end
			
			n_elements = obj.spatialDomain.n;
			
			% compute the finite-element matrices:
			[M, K, P, Q, phi, ~, Dz] = numeric.femMatrices(obj.spatialDomain, ...
				'alpha', obj.alpha, ...
				'beta', obj.beta, ...
				'gamma', obj.gamma, ...
				'b', [obj.b1, obj.g1, obj.e1, obj.h1], ...
				'c', [obj.c1; obj.l1]);
			
			dzphi = phi.diff();
			
			%% compute the boundary conditions:
			%   The bounday conditions are taken into account in the first and last row
			%   of the stiffness and input matrix. The entries for the stiffness matrix
			%   correspond to
			%       Kb = [phi alpha dz phi^T + phi q dz phi^T + phi p phi^T]_z0^z1
			
			% z0 = linspace(ldisc(1), ldisc(2), element.N)';
			% phi = phi.subs('z', z0);
			% dzphi = dzphi.subs('z', z0);
			if obj.q01 == 0
				kappa = -1;
			else
				kappa = 1;
			end
			lv = optArgs.lv;
			K_0 = - phi.atIndex(1) * obj.alpha.atIndex(1) * dzphi.atIndex(1)' ...
				+ phi.atIndex(1) * obj.q01 * dzphi.atIndex(1)' * lv * kappa ...
				+ phi.atIndex(1) * obj.q00 * phi.atIndex(1)' * lv * kappa ; %TODO lauft Doku sollte diese Zeile eigenltich ein - haben!
			if obj.q00 ~= 0
				warning("Accordingly to the documentation of the discretization, the term concerning q00 should have a negative sign. Verify if the line here or the documentation has an error")
			end
			
			P_0 = - lv * phi.atIndex(1) * [obj.b2, obj.g2, obj.e2, -obj.h2] * kappa;
			%
			% z1 = linspace(ldisc(end-1), ldisc(end), element.N)';
			% phi = phi.subs('z', z1);
			% dzphi = dzphi.subs('z', z1);
			
			K_1 = + phi.atIndex(end) * obj.alpha.atIndex(end) * dzphi.atIndex(end)' ...
				- phi.atIndex(end) * obj.q11 * dzphi.atIndex(end)' * lv ...
				- phi.atIndex(end) * obj.q10 * phi.atIndex(end)' * lv;
			
			P_1 = lv * phi.atIndex(end) * [obj.b3, obj.g3, obj.e3, -obj.h3];
			
			idx1 = 1:2;
			idxN = n_elements-1:n_elements;
			
			K(idx1, idx1) = K(idx1, idx1) + K_0;
			K(idxN, idxN) = K(idxN, idxN) + K_1;
			
			P(idx1, :) = P(idx1, :) + P_0;
			P(idxN, :) = P(idxN, :) + P_1;
			
			Q(:, 1) = [obj.c2; obj.l2];
			Q(:, end) = [obj.c3; obj.l3];			
			
			%% build the matrices for the state space system:
			% compute the inversion only once to speed up this step
			tmp = M\[K P]; 
			A_pde = tmp(:, 1:n_elements);
			
			% if required: extract the corresponding input matrices:			
			p = obj.n.u;
			q = obj.n.d;
			r = obj.n.f;
			s = obj.n.w;
			
			assert( n_elements + p + q + r + s == size(tmp, 2));
			
			if p > 0
				B_pde = tmp(:, (n_elements + 1):(n_elements + p));
			else
				B_pde = [];
			end
			if q > 0 
				G_pde = tmp(:, (n_elements + p + 1):(n_elements + p + q));
			else
				G_pde = [];
			end
			if r > 0
				E_pde = tmp(:, (n_elements + p + q + 1):(n_elements + p + q + r));
			else
				E_pde = [];
			end
			if s > 0
				H = tmp(:, (n_elements + p + q + r +1):(n_elements + p + q + r +s));
			else
				H = [];
			end
			
			% extract the corresponding output matrices:
			if obj.n.y > 0
				C_pde = Q(1:obj.n.y, :);
			else
				C_pde = [];
			end
			if obj.n.w > 0
				L = Q(obj.n.y+1 : end, :);
			else
				L = [];
			end
			
			%% combine the pde and the ode subsystem
			A = [A_pde, H; L obj.F];
			B = [B_pde, G_pde, E_pde; obj.b4, obj.g4, obj.e4];
			C = [C_pde, obj.c4];
			
			stsp = ss(A, B, C, []);
			
			pdeStates = [eye( n_elements ) zeros( n_elements, obj.n.w )];
			odeStates = [zeros( obj.n.w, n_elements ), eye( obj.n.w )];
			
			%%
			discreteModel = faultdiagnosis.DiscreteModel( obj, stsp, obj.spatialDomain, ...
				'pdeStates', pdeStates, 'odeStates', odeStates, 'Dz', Dz );
			
		end
	end
	methods (Access = public)
		
		function signalmodel = SignalModel(obj)
			signalmodel = SignalModel@faultdiagnosis.System(obj);
		end
	end
end