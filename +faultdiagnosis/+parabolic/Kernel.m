classdef Kernel < faultdiagnosis.System & faultdiagnosis.Kernel
%KERNEL object for the fault detection / identification kernel
% 

properties (SetAccess = protected, Hidden)
	KTpinv;
	CB;
	LB;
end

properties ( Dependent )
	X0;
end

properties( Dependent, Hidden )
	J; % selection matrices
end

methods
	function obj = Kernel(sys, optArgs)
		arguments
			sys (1,1) faultdiagnosis.System;
			optArgs.disturbanceModel (:,1) signals.SignalModel;
			optArgs.delta (:,1) double;
			optArgs.N_s (1,1) double;
			optArgs.truncate (1,1) double = false;
			optArgs.verbose (1,1) logical = true;		
			optArgs.relIncTol (1,1) double {mustBePositive} = 1e-6;
		end
		
			% compute the corresponding dimensions:
			if isfield(optArgs, "disturbanceModel")
				assert( optArgs.disturbanceModel.m == sys.n.d_deterministic, ...
					"Disturbance model for wrong number of disturbances");
				n.v.d = optArgs.disturbanceModel.n;
				RdTGtildeT = optArgs.disturbanceModel.getP' * sys.G_deterministic';
				S = optArgs.disturbanceModel.getS;
			else
				assert( sys.n.d_deterministic == 0, "Disturbance model is missing.")
				n.v.d = 0;
				RdTGtildeT = zeros(0, sys.n.d);
				S = [];
			end

			% compute the boundary conditions:
			K = [sys.K_0, sys.K_1];
			KTpinv = pinv(K');
			V = eye(2*sys.n.x) - K' * KTpinv;
			% remove all zero columns:
			K_T_annihilator = orth(V)' * V;

			assert( all( K_T_annihilator * K' == 0, 'all' ) );

			LB = [ sys.L_2 sys.L_3 ];
			HB_ = [ - sys.H_2'; ...
					- RdTGtildeT * sys.G_2'] * KTpinv;
			CB = [ sys.C_2, sys.C_3 ];

			idx0 = 1:sys.n.x;
			idx1 = (1:sys.n.x) + sys.n.x;
			K0 = - K_T_annihilator(:, idx0);
			K1 =   K_T_annihilator(:, idx1);

			%% TODO: allow to compute the adjoint for quantity.Matrixes		
			obj@faultdiagnosis.System(sys.spatialDomain, - sys.A.', K0, K1, ...
				  "H_1", [ sys.L_1.', zeros(sys.n.x, n.v.d) ], ... % ode coupling
				  "B_1", sys.C_1.', ... % input
				  "H_2", [ K_T_annihilator * LB', zeros(sys.n.x, n.v.d) ], ...
				  "B_2", - K_T_annihilator * CB', ...
				  "F", [ sys.F' - sys.H_2' * KTpinv * LB', zeros(sys.n.w, n.v.d); ...
					   - RdTGtildeT * ( sys.G_2' * KTpinv * LB' + sys.G_3' ), S'], ...
				  "L_1", [ - sys.H_1'; ...
						   RdTGtildeT * sys.G_1'], ...
				  "L_2", -HB_(:,idx0), ...
				  "L_3",  HB_(:,idx1), ...
				  "B_3", [  sys.C_4' - sys.H_2' * KTpinv * CB'; ...
						   -RdTGtildeT * ( sys.G_2' * KTpinv * CB' + sys.G_4')]);
					   
			[obj.A.truncate] = deal(optArgs.truncate);
			
			obj.sys = sys;
			if n.v.d > 0
				obj.disturbanceModel = optArgs.disturbanceModel;
			end

			if isfield(optArgs, "faultModel")
				obj.faultModel = optArgs.faultModel;
			end

			if isfield(optArgs, "delta")
				obj.delta = optArgs.delta;
			end

			obj.n.v.d = n.v.d;

			obj.KTpinv = KTpinv;
			obj.CB = CB;
			obj.LB = LB;

			if isfield(optArgs, "N_s")
				obj.N_s = optArgs.N_s;
			end
			
			obj.verbose = optArgs.verbose;
			obj.relIncTol = optArgs.relIncTol;			
			
% 			obj.report("Compute the transition matrices:")
% 			obj.transitionMatrices = obj.computeTransitionMatrices();
% 			obj.report("Compute differential expressions:")
% 			obj.differentialExpressions = obj.computeDifferentialParametrization();
% 			
	end
	
	%% GETTERS
	
	% TODO rename:
	%	    P -> Qw
	%	Gamma -> P
	
	function X0 = get.X0(obj)
		X0 = obj.differentialExpressions.X(1).coefficient.double;
	end
	
	function J = get.J(obj)
		% J selection matrices for the ODE states
		%    P = J.P * Pe;
		%   Qd = J.Qd * Pe;
		% selection matrices for the boundary values
		% M(0) = J.M0 * MB;
		% M(1) = J.M1 * MB;
		J.P = [eye(obj.sys.n.w) zeros(obj.sys.n.w, obj.n.v.d ) ];
		J.Qd = [ zeros(obj.n.v.d, obj.sys.n.w), eye( obj.n.v.d ) ];
		
		J.M0 = -[eye(obj.sys.n.x) zeros(obj.sys.n.x)];
		J.M1 =  [zeros(obj.sys.n.x) eye(obj.sys.n.x)];
	end
	
	function Mf = getMf(obj, M, Gamma, N, optArgs)
		arguments
			obj;
			M;
			Gamma;
			N;
			optArgs.P;
		end
		Mf = - int( obj.sys.E_1' * M, obj.spatialDomain ) ...
			+ obj.sys.E_2' * Gamma + obj.sys.E_4' * N;
		
		if isfield(optArgs, "P") && ~isempty(optArgs.P)
			Mf = Mf + obj.sys.E_3' * optArgs.P;
		end
	end
	
	function Gamma = getGamma(obj, M, N, optArgs)
		% GETGAMMA compute the auxiliary integral kernel gamma
		% Gamma = getGamma(obj, M, N, optArgs) computes the auxiliary integral kernel gamma, which
		% is given by
		%	gamma(tau) = (K^T)^+ ( m_B + C_B^T n + L_B^T q_w )
		%
		%		m_B = [-m(0,t); m(1,t)] : the boundary values
		%		n : the input
		%		q_w : the ode-states corresponding to the system ode
		%
		%		
		arguments
			obj
			M
			N
			optArgs.P
		end
		
		Gamma = obj.KTpinv * ([- M.subs(obj.spatialDomain.name, 0); ....
				M.subs(obj.spatialDomain.name, 1)] + obj.CB' * N );
			
		if isfield(optArgs, "P") && ~isempty( optArgs.P )
			Gamma = Gamma + obj.KTpinv * obj.LB' * optArgs.P;
		end
	end
	
	function [de, Phi0, Psi_B] = computeDifferentialParametrization(obj)
		[de, Phi0, Psi_B] = computeDifferentialParametrization@faultdiagnosis.System(obj);
		% COMPUTEDIFFERENTIALPARAMETRIZATION computes the differential expressions for the flatness
		% based trajectory planning
		% [de, Phi0, Psi_B] = computeDifferentialParametrization(obj) computes the differential
		% expressions required for the flatness based trajectory planning. 
		%	m(z,t) = sum V_i dt^i my
		%   n(t) = sum varpi_i dt^i my
		%	p(t) = J_p sum U_i dt^i my
		%   qd(t) = J_Qd sum U_i dt^i my
		%	mf(t) = sum X_i dt^i my
		%   md_bounded(t) = sum Y_i dt^i my
		%   mu(t) = sum X_ui dt^i my
		
		if obj.sys.n.w > 0
			Gamma = obj.getGamma( de.V, de.varpi, ...
				"P", obj.J.P * de.U );
			
			Xf = obj.getMf( de.V, Gamma, de.varpi, ...
				"P", obj.J.P * de.U);
		else
			Gamma = obj.getGamma( de.V, de.varpi);
			
			Xf = obj.getMf( de.V, Gamma, de.varpi);
		end
		
		Xd = int( obj.sys.G_1' * de.V, obj.spatialDomain ) ...
				- obj.sys.G_2' * Gamma - obj.sys.G_4' * de.varpi;
			
		Xu = int( obj.sys.B_1' * de.V, obj.spatialDomain ) ...
				- obj.sys.B_2' * Gamma - obj.sys.B_4' * de.varpi;
			
		if obj.sys.n.w > 0
			Xd = Xd - obj.sys.G_3' * obj.J.P *  de.U;
			Xu = Xu - obj.sys.B_3' * obj.J.P *  de.U;
		end
		
		de.X = Xf;
		de.Xd = Xd;
		de.Y = obj.sys.G_bounded' * Xd;
		de.Xu = Xu;
		
		% verify if all faults are detectable:
		% [Theorem 2.3; diss]
		X = double( Xf );
		for i = 1:obj.sys.n.f
			if all( X(i,:,:) == 0)
				warning( "Fault " + num2str(i) + " is not detectable, since e_" + num2str(i) + ...
					"^T X is zero for all j");
			end
		end
		
	end
	
	function [ioKernels, relInc, numIter] = computeIOkernels(obj, basicVariable, optArgs)
		% copmutes the io integral kernels
		% [ioKernels, relInc, maxRelInc, numIter] = computeIOkernels(obj, basicVariable, optargs)
		% returns the reference trajectories for the io integral kernels:
		%   N(t) = sum varpi_i dt^i mu
		%	Mu(t) = sum X_u,i dt^i mu
		%   MdBar(t) = sum X_dBar,i dt^i mu
		%	Mf(t) = sum X_i dt^i mu
		arguments
			obj
			basicVariable (:,:) signals.BasicVariable;
			optArgs.relativeIncrementTolerance;
		end
		
		de = obj.differentialExpressions;
		
		if isfield(optArgs, "relativeIncrementTolerance")
			varArgs = {"relativeIncrementTolerance", optArgs.relativeIncrementTolerance};
		else
			varArgs = { 'n', obj.N_s };
		end

		[ioKernels.N, relInc.n, numIter.n] = de.varpi.applyTo( basicVariable, varArgs{:}  );
		[ioKernels.Mu, relInc.Mu, numIter.Mu] = de.Xu.applyTo( basicVariable, varArgs{:}  );
		[ioKernels.Mf, relInc.Mf, numIter.Mf] = de.X.applyTo( basicVariable, varArgs{:}  );
		
		
		XdBounded = obj.sys.G_bounded' * de.Xd;
		
		[ioKernels.Md_bounded, relInc.Md_bounded, numIter.Md_bounded] ...
			= XdBounded.applyTo( basicVariable, varArgs{:}  );

		ioKernels.N.setName("n");
		ioKernels.Mu.setName("m_u");
		ioKernels.Mf.setName("m_f");
		ioKernels.Md_bounded.setName("m_dBar");
		
	end
	
end

methods ( Access = protected )
		
	function [solution, relInc, maxRelInc, numIter] = solve(obj, basicVariable, optArgs)
		% SOLVE sovles the kernel equations 
		% solution = solve(obj, basisVariable) the reference trajectory for the basis variable is
		% applied to the differential parametrizations. This yields for mu = basicVariable:
		%	M(z,t) = sum V_i dt^i mu
		%   N(t) = sum varpi_i dt^i mu
		%	P(t) = J_p sum U_i dt^i mu
		%   Qd(t) = J_Qd sum U_i dt^i mu
		%	Gamma(t) = getGamma( M, N, P)
		% 
		arguments
			obj
			basicVariable (:,1) signals.BasicVariable;
			optArgs.relativeIncrementTolerance;
		end
		namedArgs = namedargs2cell(optArgs);
		[refTraj, relIncSys, numIter] = obj.referenceTrajectory(basicVariable, namedArgs{:});
		
		solution.M = refTraj.x;
		solution.N = refTraj.u;
		
		relInc.m = relIncSys.x;
		relInc.n = relIncSys.u;
		
		if isfield( refTraj, "w")
			solution.P = obj.J.P * refTraj.w;
			solution.Qd = obj.J.Qd * refTraj.w;
			solution.Gamma = obj.getGamma( solution.M, solution.N, "P", solution.P );
			
			relInc.p = obj.J.P * relIncSys.w;
			relInc.qd = obj.J.Qd * relIncSys.w;
			
		else			
			solution.Gamma = obj.getGamma( solution.M, solution.N);
		end
		
		%% verify the kernels
		I = basicVariable(1).domain;
		[lB.m,lL1.m,lowerBound.m] = near( solution.M.subs(I.name, I.lower));
		[uB.m,uL1.m,upperBound.m] = near( solution.M.subs(I.name, I.upper));

		if isfield( solution, "Qd")
			[uB.qd,uL1.qd,upperBound.qd] = numeric.near( solution.Qd.at(I.upper));
			[lB.qd,lL1.qd,lowerBound.qd] = numeric.near( solution.Qd.at(I.lower));
		end
		if isfield( refTraj, "w")
			[lB.p,lL1.p,lowerBound.p] = numeric.near(solution.P.at(I.lower));	
			[uB.p,uL1.p,upperBound.p] = numeric.near( solution.P.at(I.upper));
		end
		
		if obj.verbose
			disp("Lower bounds:")
			disp( lowerBound );
			disp("Upper bounds:")
			disp( upperBound );
		end
		
		maxRelInc = 0;
		for kernelName = fieldnames(lB)'
			if ~lB.(kernelName{1})
				warning("The lower bound of kernel " + kernelName{1} + " has a L1-norm error: " + lL1.(kernelName{1}))
			end
			if ~uB.(kernelName{1})
				warning("The upper bound of kernel " + kernelName{1} + " has a L1-norm error: " + uL1.(kernelName{1}))
			end
			if any( relInc.(kernelName{1}) > obj.relIncTol )
				warning("The relative increment tolerance is " + misc.vector2string( relInc.(kernelName{1}) ) + " for kernel " + kernelName{1});
			end
			
			maxRelInc_i = max( relInc.(kernelName{1}), [], 'all');
			if maxRelInc_i > maxRelInc
				maxRelInc = maxRelInc_i;
			end
			
		end
				
	end % function solve
	
end % methods ( Access = protected )

end




































