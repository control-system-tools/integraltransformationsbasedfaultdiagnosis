classdef FEApproximation < faultdiagnosis.LPS
	%FEAPPROXIMATION Discretization of a parabolic system using FEM
	%
	%
	
	properties
		M;                                       % Mass matrix
		K;                                       % Stifness matrix
		v = 1;                                   % Gewichtung für dirichlet randbedingungen. 1e4 hat sich bisher bewährt.
	end
	
	methods
		function obj = FEApproximation(sys, varargin)
			obj@faultdiagnosis.LPS(sys, varargin);
			
			for arg=1:2:length(varargin)
				obj.(varargin{arg}) = varargin{arg + 1};
			end
			
			obj.stsp = obj.GetStSpMatrices();
			
		end
		
		function M = get.M(obj)
			if isempty(obj.M)
				obj.stsp;
			end
			M = obj.M;
		end
		function K = get.K(obj)
			if isempty(obj.K)
				obj.stsp;
			end
			K = obj.K;
		end
	end
	
	methods (Access = public)
		function [A, B, G, E, C, M, K] = FEMmatrices(obj, N)
			% FEMMATRICES Computes the matrices of a fem approximation
			%
			% Discretization for the parabolic PDE
			%       \dt v(z,t) = \dz (\alpha \dz v(z,t)) + \beta(z) \dz v(z,t) ...
			%                   + \gamma(z) v(z,t) + b^T(z) u(t) + g^T(z) d(t)
			% BCs: q_i \dz v(z_i, t) = a_i v(z_i,t) + b_i u(t)
			% Output: y(t) = int c_int(z) v(z,t) dz + c0(z_i)v(z_i,t) + c1(z_i)\dz v(z_i,t)
			%
			% The function will calculate the discretization matrices:
			% M \dt \nu(t) = K \nu(t) + L u(t)
			%         y(t) = C \nu(t) + D f(t)
			%
			
			%% lokale Variablen
			lz = obj.z;
			ldisc = obj.disc;
			ldelta = obj.delta;
			lN = N;
			lv = obj.v;
			
			%% initalisierung der benoetigten matrizen
			
			p = obj.sys.n.u;
			q = obj.sys.n.d;
			r = obj.sys.n.f;
			
			b = obj.sys.b1.';
			g = obj.sys.g1.';
			e = obj.sys.e1.';
			
			[M, K, L, C, phi] = numeric.femMatrices(obj.sys.spatialDomain, ...
				'alpha', obj.sys.alpha, ...
				'beta', obj.sys.beta, ...
				'gamma', obj.sys.gamma, ...
				'b', [b, g, e], ...
				'c', obj.sys.c1);
			
			dzphi = phi.diff();
			
			%% Berücksichtigung der Randbedinungen
			%   The bounday conditions are taken into account in the first and last row
			%   of the stiffness and input matrix. The entries for the stiffness matrix
			%   correspond to
			%       Kb = [phi alpha dz phi^T + phi q dz phi^T + phi p phi^T]_z0^z1
			
			% z0 = linspace(ldisc(1), ldisc(2), element.N)';
			% phi = phi.subs('z', z0);
			% dzphi = dzphi.subs('z', z0);
			if obj.sys.q01 == 0
				kappa = -1;
			else
				kappa = 1;
			end
			
			K_0 = - phi.atIndex(1) * obj.sys.alpha.atIndex(1) * dzphi.atIndex(1)' ...
				+ phi.atIndex(1) * obj.sys.q01 * dzphi.atIndex(1)' * lv * kappa ...
				+ phi.atIndex(1) * obj.sys.q00 * phi.atIndex(1)' * lv * kappa ;
			
			L_0 = - lv * phi.atIndex(1) * [obj.sys.b2, obj.sys.g2, obj.sys.e2] * kappa;
			%
			% z1 = linspace(ldisc(end-1), ldisc(end), element.N)';
			% phi = phi.subs('z', z1);
			% dzphi = dzphi.subs('z', z1);
			
			K_1 = + phi.atIndex(end) * obj.sys.alpha.atIndex(end) * dzphi.atIndex(end)' ...
				- phi.atIndex(end) * obj.sys.q11 * dzphi.atIndex(end)' * lv ...
				- phi.atIndex(end) * obj.sys.q10 * phi.atIndex(end)' * lv;
			L_1 = lv * phi.atIndex(end) * [obj.sys.b3, obj.sys.g3, obj.sys.e3];
			
			idx1 = 1:2;
			idxN = N-1:N;
			
			K(idx1, idx1) = K(idx1, idx1) + K_0;
			K(idxN, idxN) = K(idxN, idxN) + K_1;
			
			L(idx1, :) = L(idx1, :) + L_0;
			L(idxN, :) = L(idxN, :) + L_1;
			
			C(:, 1) = obj.sys.c2;
			C(:, end) = obj.sys.c3;
			
			%% Matrizen für Zustandssystem
			
			tmp = M\[K L];
			A = tmp(:, 1:N);
			
			if p > 0
				B = tmp(:, (N + 1):(N + p));
			else
				B = [];
			end
			if q > 0 %Falls vorhanden, stoerinput hinzufuegen.
				G = tmp(:, (N + p + 1):(N + p + q));
			else
				G = [];
			end
			if r > 0 %Falls vorhanden, fehler eingang hinzufuegen
				E = tmp(:, (N + p + q + 1):end);
			else
				E = [];
			end
			
			obj.M = M;
			obj.K = K;
			
			
		end
		
	end
	
	methods (Access = protected)
		
		[ Kb, Lb, Pb, Eb ] = BoundaryDiscretization( obj );
		
		function [stsp] = GetStSpMatrices(obj)
			%TODO rewrite this as DAE system!
			[A, B, G, E, C, obj.M, obj.K] = FEMmatrices(obj, length(obj.disc));
			Bss = [B, G, E];
			stsp = ss(A, Bss, C, []);
		end
		
	end
	
	methods ( Static)
		p = phi(z, k, disc, deltak, zm);
		p = dzphi(delta, disc, k, N, zm);
	end
end

