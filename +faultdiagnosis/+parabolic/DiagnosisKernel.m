classdef DiagnosisKernel < faultdiagnosis.parabolic.Kernel
	%DIAGNOSISKERNEL Summary of this class goes here
	%   Detailed explanation goes here
	
	properties
		faultModel (:,1) signals.SignalModel;
	end
	
	methods
		function obj = DiagnosisKernel(sys, faultModel, optArgs)
			arguments
				sys (1,1) faultdiagnosis.System;
				faultModel (:,1) signals.SignalModel;
				optArgs.disturbanceModel (:,1) signals.SignalModel;
				optArgs.delta (:,1) double;
				optArgs.N_s (1,1) double;
				optArgs.truncate (1,1) double = false;
				optArgs.verbose (1,1) logical = true;		
				optArgs.relIncTol (1,1) double {mustBePositive} = 1e-6;				
			end
			
			args = namedargs2cell(optArgs);
			
			obj@faultdiagnosis.parabolic.Kernel(sys, args{:});
			
			obj.faultModel = faultModel;
			assert( faultModel.m == sys.n.f );
			obj.n.v.f = faultModel.n;
			
		end
	end
	
	methods ( Access = public)
		function [ioIntegralKernels, kernels, measures] = ...
				computeIdentificationFilter(obj, I, optArgs)
			arguments
				obj
				I (1,1) quantity.Domain;
				optArgs.theta (:,1) signals.BasicVariable = ...
					signals.GevreyFunction( 'order', 1.999, "diffShift", 1, "numDiff", obj.N_s, ...
					'timeDomain', I);
				optArgs.bounds (:,1) double = obj.delta;
				optArgs.useDegreeOfFreedom (1,1) logical = true;
				optArgs.relIncTol (1,1) double {mustBePositive} = 1e-6;
				optArgs.useAlsoXiT (1,1) logical = false;
			end
			
			Rf = obj.faultModel.getP;
			de = obj.differentialExpressions();
			n_v = obj.n.v.f;
			
			%% set up the helper xi-system
			obj.report( "=== Setup the auxilliary xi-system ===" );
			%
			%	dt xi = A xi + B mu
			%		y = C xi
			%
			% compute the parameters for det(sI-S) = sum a_i s^i
			%
			% TODO: the xi-ode are in general three parallel systems with same coefficients. It has
			% been shown that is numerically better to copmute the results for the direct for each
			% subsystem and not for the overall system ... this should be implemente din the
			% future...
			a = shiftdim( double( de.a ) ).';
			assert( a(end) == 1);
			sysXi.A = kron( [zeros( n_v-1, 1), eye( n_v-1 ); ...
				-a(1:end-1)                   ], ...
				eye(obj.n.u) );
			sysXi.B = kron( misc.unitVector(n_v, n_v), eye(obj.n.u) );
			sysXi.C = kron( misc.unitVector(n_v, 1).', eye(obj.n.u) );
			sysXi = ss( sysXi.A, sysXi.B, sysXi.C, []);
			
			%% compute the Upsilon matrix
			Upsilon = zeros(  n_v, n_v * obj.n.u );
			for i = 1:length( de.W )
				Upsiloni = de.W(i).coefficient.on() * sysXi.C * sysXi.A^(i-1);
				Upsilon = Upsilon + Upsiloni;
			end
			
			% verify that the relative increment is below a required tolerance
			if max( abs( Upsiloni(:) ) ) / max( abs( Upsilon(:) ) ) > 1e-9
				warning( "The series to compute Upsilon seems not be converged!");
			end
			
% 			%pinvUpsilon = pinv( Upsilon );
% 			pinvUpsilon = double( pinv( sym( Upsilon ) ) );
% 			if rank(pinvUpsilon) ~= rank(Upsilon)
% 				warning("Rank of Upsilon and its pseudo inverse are not the same");
% 			end

			%% compute the weighted controllability Gramian
			obj.report( "=== Compute the controllability gramian ===" );
			Phi_t_t0 = expm(sysXi.A * ( I.Discrete - I.lower )); % Phi(tau,0)
			Phi_T_t = expm(sysXi.A * (I.upper - I.Discrete ) ); % Phi(T, tau)
			Phi_t_t1 = expm(sysXi.A * (I.Discrete - I.upper ) ); % Phi(tau, T)
			
			% compute the derivatives of the state transition matrix
			for i = 1:obj.N_s
				Phi_T_t_transposed_diffs{i} = (-1)^i * (sysXi.A.')^i * Phi_T_t.';
			end
			Phi_T_t_transposed_diffs = signals.BasicVariable( Phi_T_t.', Phi_T_t_transposed_diffs);
			
			% Note: hier könnte man noch die gramsche Matrix mit function handles berechnen ...
			theta = function_handle( optArgs.theta.f );
			wIntegrand = @(tau) ( expm( sysXi.A * ( I.upper - tau ) ) * sysXi.B  );
			
			WthetaT = integral( @(tau) wIntegrand(tau)*wIntegrand(tau)' * theta(tau), 0, ...
				I.upper, 'ArrayValued', true, "RelTol", 1e-12, "AbsTol", 1e-12);
			
			% compute the weighted gramian controllability matrix
			Wtheta = int( Phi_T_t * sysXi.B * sysXi.B' * Phi_T_t' * optArgs.theta.f, I, ...
				I.lower, I.name );
			% assert that it has full rank
			% assert( rank( Wtheta.at(I.upper) ) == size( sysXi.A, 1) )
			if ~all( eig ( WthetaT ) > 0 )
				warning("W_\theta is not positive definite!");
			end
			obj.report( "=== Compute the basis functions ===" );
			
			if optArgs.useAlsoXiT
				% pre compute the term: W_theta^-1 * [Phi(T,0), -I]
				WthetaTx = mldivide( WthetaT, [Phi_t_t0.at(I.upper), -eye(size(sysXi.A,1))] );
				XmueTheta = ( -sysXi.B' * Phi_T_t_transposed_diffs) * optArgs.theta * WthetaTx;
								
				Upsilon = blkdiag(Upsilon, Upsilon);
			else
				% pre compute the term: W_theta^-1 * [Phi(T,0), -I]
				WthetaTx = mldivide( WthetaT, Phi_t_t0.at(I.upper) );
				XmueTheta = ( -sysXi.B' * Phi_T_t_transposed_diffs) * optArgs.theta * WthetaTx;

			end
			
			Psi_t_t0 = Phi_t_t1 * Wtheta * WthetaTx;
			
			if optArgs.useDegreeOfFreedom
				% pre computation for the use of the degree of freedom
				%Z = double( null( sym( Upsilon ) ) );
				Z = null( Upsilon );
				
				if max( abs( Upsilon * Z ) / max( Upsilon(:) ), [], 'all') > 1e-6
					warning("Kernel of Upsilon is not correct");
				end
				
				% establish the matrices for the quadratic program to guess the initial point for
				% the numerical optimization:
				%
				%	xi_i^00 = - Upsilon^+ * Rf_i^T
				%
				%	m_dBar = theta_0 * xi_i^00 + theta_1 xi_i^*
				%
				%	S_d* = min || WdBar ( theta_0 * xi_i^000 + theta_1 xi_i^* ) ||_2^2
				%
				%		 = min xi_i^*^T Q xi_i^* + b^T xi_i^* + c
				%
				%	Q = int( theta_1^T * WdBar^T * WdBar * theta_1 )
				%
				%
				%
				theta_dBar = applyTo( obj.sys.G_bounded' * de.Xd, XmueTheta);
				% theta_0 = theta_dBar * xi_i^00
				% theta_1 = theta_dBar * Z
				
				WdBar = diag( optArgs.bounds );
				WWW = int( theta_dBar' * (WdBar.' * WdBar) * theta_dBar );
				
				% Verify that WWW is symmetric:
				if max( abs(WWW - WWW'), [], 'all') / max( abs(WWW), [], 'all') > 1e-12
					warning("The matrix WWW is not symmetric");
				end
				
				Q = Z' * WWW * Z;
								
				if any( eig(Q) < 0 )
					warning("The matrix Q is not positive semidefinit")
				end
				
			end
			
			for i = 1:obj.sys.n.f
				
				qf0 = -Rf(i,:)';
				
				if optArgs.useAlsoXiT
					qf0 = [qf0; zeros(size(qf0))];
				end
				
				xi00 = lsqminnorm(Upsilon, qf0);
				
				obj.report( "=== Plan the transition with the initial condition qf(0) = " + ...
					misc.vector2string( qf0 ) + " ===" );
				assert( numeric.near( Upsilon * xi00, qf0, 1e-4 ), ...
					"The fault is not identifiable, since qf^0* = " + ...
					misc.vector2string(Upsilon*pinv(Upsilon)*qf0) ...
					+ " but it should be qf^0 = " + misc.vector2string(qf0) )
				% compute the initial condition for the xi-system related to the i-th initial
				% condition related to the (Sf', Rf') system
												
				if size(Z,2) > 0 && optArgs.useDegreeOfFreedom
					f = (xi00' * WWW * Z)';
					
					%c = xi0' * WWW * xi0;
					
					% Quadratic programm:
					%	JJ = xi0*^T Q xi0* + 2 f^T xi0* + c
					% is equivalent to
					%	J = 1/2 xi0*^T Q xi0* + f^T xi0*
					% which has the gradient:
					%	grad J = Q xi0* + f 
					% Thus, the minimal solution must satisfy:
					%	xi0* = -Q^+ f
					% which has the solution
					%	xi0* = - Q^+ f
					xi0Star = - lsqminnorm( Q, f); % pinv(Q) * f;
					
					xi0 = xi00 + Z * xi0Star;
					
					% solve the minimization of the threshold:
					c = theta_dBar * xi00;
					b = ( theta_dBar * Z );
					
					Jstar = @(x) optArgs.bounds' * int( abs( c + b * x ) );
					
					opts = optimset(@fminsearch);
					if obj.verbose
% 						opts.Display = "iter";
					end
					opts.UseParallel = true;
					
					o = zeros(size(xi0Star));
					
					obj.logMsg("Compute the (sub) optimal threshold. fB%i\n", i);
					if Jstar(o) < Jstar(xi0Star)
						initialPoint = o;
						obj.logMsg("Use zero as initial point\n");
					else
						initialPoint = xi0Star;
						obj.logMsg("Use xi_0^* as initial point\n");
					end
					exitFlag = 0;
					xiL1Star = initialPoint;
					fBi = [];
					while exitFlag == 0						
						[xiL1Star, fBi, exitFlag] = fminsearch( Jstar, xiL1Star, opts);
						obj.logMsg("Repeat optimization")
					end
					
					xi0 = xi00 + Z * xiL1Star;
					
					if max( abs( Upsilon * xi0 - qf0 ) ) > 1e-12
						warning("Upsilon * xi0 does not satisfy qf0!")
					end
					
					obj.logMsg( sprintf("Threshold f_B%i for L2-optimization = %g \n Threshold optimization = %g \n", ...
						i, Jstar(xi0Star), fBi) );
				else
					xi0 = xi00;
				end
				
				% compute the input "mu" and solution for the xi-system
				% Here the Phi_T_t as signals.BasicVariable object is used, so that the required
				% derivatives of mu are already computed.
				mu = XmueTheta * xi0;
				
				
				% xi = Phi(tau,0) * xi0 + Phi(tau,0) * int_0^tau Phi(0,zeta) * B * mu(zeta) d zeta
				%[phi0, ~, xi] = stateSpace.lsim( sysXi, mu.f, I, xi0(1:size(sysXi.A,1)) );
				xi = Phi_t_t0 * xi0(1:size(sysXi.A,1)) - Psi_t_t0 * xi0;
				
				Psi_t_t0 = Phi_t_t1 * Wtheta * WthetaTx;
				
				% verify that xi(T) = 0 holds. For this, the maximal relative error of xi(T) is
				% used, i.e., the maximal value of
				%	| e_i^T xi(T)| / || e_i^T xi(tau) ||_I,infty
				nXi = size(sysXi.A,1);
				
				if optArgs.useAlsoXiT
					xiT = xi0(nXi+1:end);
				else
					xiT = zeros(nXi,1);
				end
				
				if max( abs( xi.at(I.upper) - xiT ) ./ max( abs(xi) ) ) > 1e-6
					warning("The xi-system does not satisfy the final values at t = T")
				end
				if max( abs( xi.at(I.lower) - xi0(1:nXi) ) ./ max( abs(xi) ) ) > 1e-6
					warning("The xi-system does not satisfy the final values at t = 0.")
				end
				
				% compute the phi with all its derivatives.
				phi = {};
				for k = 1 : obj.N_s
					phi{k} = sysXi.C * sysXi.A^k * xi;
					for j = 0 : k-1
						phi{k} = phi{k} + sysXi.C * sysXi.A^(k-1-j) * sysXi.B * mu.diff(I, j);
					end
				end
				phi = signals.BasicVariable( sysXi.C * xi, phi );
				
				% compute the solution for the integral kernels
				nvf = size(Rf,2);
				[sol_i, relInc(i), maxRelInc(i), numIter(i)] = obj.solve(mu, phi, qf0(1:nvf));
				
				% compute the trajectories
				sol_i.phi = phi;
				sol_i.mue = mu;
				sol_i.thresholds = obj.threshold(mu);
				
				solution(i) = sol_i;
				
				obj.logMsg( sprintf("Threshold f_B%i for kernel %i = %g \n", i, i, ...
					solution(i).thresholds) );
			end % i = 1:obj.sys.n.f
			
 			kernels.Phi = [solution.phi]; 
			kernels.Mue = [solution.mue];
			kernels.M = [solution.M];
			kernels.N = [solution.N];
			kernels.Qf = [solution.Qf];

			if isfield( solution, "Qd")
				kernels.Qd = [solution.Qd];
			end
			if isfield( solution, "P")
				kernels.P = [solution.P];
			end
			kernels.Gamma = [solution.Gamma];
			
			ioIntegralKernels = obj.computeModulatingFunctions(I, kernels);
			ioIntegralKernels.thresholds = [solution.thresholds];
			
			relativeIncrement.M = [relInc.m];
			relativeIncrement.N = [relInc.n];
			if isfield( solution, "Qd")
				relativeIncrement.qd = [relInc.qd];
			end
			if isfield( solution, "P")
				relativeIncrement.P = [relInc.p];
			end
			
			relativeIncrement.qf = [relInc.qf];
			
			maxRelativeIncrement = max( maxRelInc );
			
			%% verify the kernels:
			if max( abs( kernels.Qf.at(I.lower) + Rf' ), [], 'all') > 1e-9
				warning(" Initial value of Qf system is not consitent")
			end
			
			measures.relativeIncrement = relativeIncrement;
			measures.maxRelativeIncrement = maxRelativeIncrement;
			measures.Upsilon = Upsilon;
			measures.numIter = numIter;
			
		end
		
		function [de, Phi0, Psi_B] = computeDifferentialParametrization(obj)
			[de, Phi0, Psi_B] = computeDifferentialParametrization@faultdiagnosis.parabolic.Kernel(obj);
						
			% \dot qf = Sf^T qf + Rf^T mf
			Sf = obj.faultModel.getS;
			Rf = obj.faultModel.getP;
			
			n_v = obj.n.v.f;
			
			% compute the expression (sI -Sf^T)
			sImSf = signals.PolynomialOperator({ -Sf.', eye( n_v ) });
			
			adjF = adj(sImSf);
			detF = det(sImSf);
			
% 			commonRoot = nan;
% 			
% 			while ~isempty(commonRoot)
% 			
% 				rootsAdj = adjF.findCommonZeros();
% 				rootsDet = detF.findCommonZeros();
% 
% 				commonRoot = numeric.intersect( rootsAdj, rootsDet );
% 
% 				if ~isempty( commonRoot )
% 					warning("root %g can be removed! \n", commonRoot)
% 					adjF = signals.PolynomialOperator( expand( adjF.sym ./ (obj.s - commonRoot) ) );
% 					detF = signals.PolynomialOperator( expand( detF.sym ./ (obj.s - commonRoot) ) );
% 				end
% 			end
			
			
			% copmute the power series related to "sum Wi s^i"
			tmp = Rf.' * de.X;
			[tmp.truncate] = deal(false);
			de.W = adjF * tmp;
			% compute the differential expression related to det(sI - Sf)
			de.a = detF;
			

		end
		
		function fB = threshold(obj, mu, optArgs)
			arguments
				obj
				mu signals.BasicVariable;
				optArgs.bounds (:,1) double = obj.delta;
			end
			de = obj.differentialExpressions();
			fB = int( abs( obj.sys.G_bounded.' * de.Xd.applyTo(mu) ) ).' * optArgs.bounds;
		end
	end
	
	methods (Access = protected)
		function [solution, relInc, maxRelInc, numIter] = solve(obj, mu, phi, qf0, optArgs)
			arguments
				obj,
				mu,
				phi,
				qf0
				optArgs.relativeIncrementTolerance;
			end
			namedArgs = namedargs2cell(optArgs);
			[solution, relInc, maxRelInc, numIter] = solve@faultdiagnosis.parabolic.Kernel(obj, mu, namedArgs{:});
			de = obj.differentialExpressions();
			
			if isfield(optArgs, "relativeIncrementTolerance")
				varArgs = {"relativeIncrementTolerance", optArgs.relativeIncrementTolerance};
			else
				varArgs = { 'n', obj.N_s };
			end
			
			[solution.Qf, relInc.qf, numIter.qf] = de.W.applyTo(phi, varArgs{:});
			
			I = mu(1).domain;
			
			[lB.qf,lL1.qf,lowerBound.qf] = numeric.near( qf0, solution.Qf.at(I.lower), 1e-9, true, "relativeTo", solution.Qf.abs.MAX);
			[uB.qf,uL1.qf,upperBound.qf] = numeric.near( 0, solution.Qf.at(I.upper), 1e-9, true, "relativeTo", solution.Qf.abs.MAX);
			
			if obj.verbose
				disp("Lower bounds:")
				disp( lowerBound );
				disp("Upper bounds:")
				disp( upperBound );
			end
			
			if ~lB.qf
				warning("The lower bound of kernel qf has a L1-norm error: " + lL1.qf)
			end
			if ~uB.qf
				warning("The upper bound of kernel qf has a L1-norm error: " + uL1.qf)
			end
			if any( relInc.qf > obj.relIncTol )
				warning("The relative increment tolerance is " + misc.vector2string( relInc.qf ) + " for kernel qf");
			end
			
			maxRelInc = max( maxRelInc, max( relInc.qf ));
		end
	end
	
	
end

