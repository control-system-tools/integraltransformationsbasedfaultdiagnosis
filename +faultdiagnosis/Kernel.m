classdef (Abstract) Kernel < handle
	
	properties (SetAccess = protected)
		% The corresponding system to this fault diagnosis kernel
		sys faultdiagnosis.System;
		
		% signal model for the disturbances
		disturbanceModel (:,1) signals.SignalModel;

		% solution for the kernel equations
		solution struct;
	
		% input-output expression integral kernels
		ioIntegralKernels struct;		
		
		% discretization of the integral terms in form of fir filters
		firFilters struct;
	end
	
	properties
		% upper bounds for the unknown but arbitrary disturbance
		delta (:,1) double;
		relIncTol (1,1) double {mustBePositive} = 1e-6;
		verbose (1,1) logical = true;
	end

	methods (Access = public)
		
		function [thresholdList, range] = parameterStudyT(obj, range, optArgs)
			% PARAMETERSTUDYT computes the kernels for different parameters
			% [thresholdList, Tlist] = parameterStudyT(obj, range, optArgs) will compute the kernels
			% for a given range of detection window lengths T. The parameter range can be either a
			% vector with explicit values for T or a struct with the fields "start", "stop", and "n".
			% The following arguments can be set by name-value pairs:
			%	bounds (:,1) [double]: 
			%		upper absolute value of the unknown but bounded disturbance (\delta)
			%	case [string]: 
			%		can be "detection" or "identification". sets the function to compute the kernels
			%	plot (1,1) [logical]:  
			%		defines if the result should be plotted.

			arguments
				obj
				range;
				optArgs.bounds (:,1) double = ones( obj.sys.n.d_bounded, 1);
				optArgs.case = "detection";
				optArgs.plot (1,1) logical = true;
			end

			if isstruct(range)
				assert(isfield(range, "start"))
				assert(isfield(range, "stop"))
				if isfield(range, "n")
					range.n = 5;
				end

				range = logspace( log10( range.start ), log10( range.stop ), range.n );
			elseif isvector(range) && isnumeric(range)
				%range = range;
			else
				error("The type of parameter range. It must be either a struct or a numeric vector")
			end		

			thresholdList = zeros(length(range), obj.sys.n.f);

			for k = 1:length(range)

				I = quantity.Domain("tau", linspace(0,range(k)));

				theta = ...
				signals.GevreyFunction( 'order', 1.999, "diffShift", 1, "numDiff", obj.N_s, ...
					'timeDomain', I, "g", @signals.gevrey.bump.g2);

				if optArgs.case == "detection"
					kern = obj.computeDetectionFilter(I, "theta", theta, "bounds", optArgs.bounds);
				elseif optArgs.case == "identification"
					kern = obj.computeIdentificationFilter(I, "theta", theta,"bounds", optArgs.bounds);
				else
					error("Unknown case " + optArgs.case)
				end
				thresholdList(k,:) = kern.threshold;
			end
			
			if optArgs.plot || nargout == 0
				plot( range, thresholdList )
			end
		end	% function parameterStudyT
		
	end % methods (Access = public)
	
	methods ( Access = public )
		
		function logMsg(obj, txt, varargin)
			if obj.verbose
				obj.report( sprintf(txt, varargin{:}) );
			end
		end
		
		function [modulatingFunctions] = computeModulatingFunctions(obj, I, solution)
			%
			%
			% solution must be a structure with the fields "M", "Gamma", "N", and optional field "P"
			%
			arguments
				obj;
				I (1,1) quantity.EquidistantDomain;
				solution = obj.solution;
			end
			
			if isfield( solution, "Mf" )
				modulatingFunctions.Mf = solution.Mf;
			else
				% --- if Mf is not pre computed: ---
				if obj.n.w > 0
					modulatingFunctions.Mf = obj.getMf( solution.M, solution.Gamma, ...
						solution.N, "P", solution.P);
				else
					modulatingFunctions.Mf = obj.getMf( solution.M, solution.Gamma, ...
						solution.N);
				end
			end
			
			modulatingFunctions.Mu = int( obj.sys.B_1' * solution.M, obj.spatialDomain ) ...
				- obj.sys.B_2' * solution.Gamma - obj.sys.B_4' * solution.N;
						
			modulatingFunctions.Md = int( obj.sys.G_1' * solution.M, obj.spatialDomain ) ...
				- obj.sys.G_2' * solution.Gamma - obj.sys.G_4' * solution.N;
						
			if isfield(solution, "P") && ~isempty(solution.P)
				modulatingFunctions.Mu = modulatingFunctions.Mu - obj.sys.B_3' * solution.P;
				modulatingFunctions.Md = modulatingFunctions.Md - obj.sys.G_3' * solution.P;
			end
			
			modulatingFunctions.Md_bounded = obj.sys.G_bounded' * modulatingFunctions.Md;
			modulatingFunctions.Md_deterministic = obj.sys.G_deterministic' * modulatingFunctions.Md;
			modulatingFunctions.N = solution.N;
			
			% set the names for the integral kernels
			modulatingFunctions.Md.setName("m_d");
			modulatingFunctions.Mu.setName("m_u");
			modulatingFunctions.Md_bounded.setName("m_{\bar{d}}");
			modulatingFunctions.Md_deterministic.setName("m_{\tilde{d}}");
			modulatingFunctions.N.setName("n");
			modulatingFunctions.Mf.setName("m_f");
			
		end % function computeModulatingFunctions
		
		function filters = generateFirFilters(obj, I, modulatingFunctions, optArgs)
			% generateFirFilters computes the fir filters for given io-filters
			%	filters = generateFirFilters(obj, I, modulatingFunctions, optArgs) computes the fir
			%	filters required for the fault detection on the moving horizon I. With
			%	modulationgFunctions, the io-integral kernels are specified. It must be a structure
			%	with the fields "N", "Mu", and optional "Mf", "Md_bounded".
			%
			%	Further options can be specified by name-value-pairs:
			%		bounds: the bounds of the disturbance as a vector
			%		
			%		disturbanceType: Define if the disturbance bounds correspond to a absolutely
			%		bounded disturbance or a L2-bounded disturbance. For the absolutely bounded
			%		disturbance choose "Linfty" and for the L2-bounded disturbance "L2"
			%
			%		type: the type of the residual generator, e.g., "detection", "isolation" or
			%		"diagnosis". This type is specified as a string and used in the plot method for
			%		the fault detection/isolation/diagnosis results.
			%		
			%		discretizationFunction: a function handle to the approximation method of the
			%		integral. The available functions are signals.fir.{midpoint, trapez, ...
			%		zeroOrderHold, firstOrderHold}.
			arguments
				obj
				I (1,1) quantity.EquidistantDomain;
				modulatingFunctions
				optArgs.bounds (:,1) double = obj.delta;
				optArgs.type (1,1) string = "";
				optArgs.discretizationFunction = @signals.fir.midpoint;
				optArgs.disturbanceType (1,1) string = "Linfty";
			end
			
			filters.N = optArgs.discretizationFunction(modulatingFunctions.N.', I, 'flip', false);
			filters.Mu = optArgs.discretizationFunction(modulatingFunctions.Mu.', I, 'flip', false);
			if isfield( modulatingFunctions, "Mf")
				filters.Mf = optArgs.discretizationFunction(modulatingFunctions.Mf.', I, ...
					'flip', false);
			end
			if isfield( modulatingFunctions, "Md_bounded")
				filters.Md_bounded = optArgs.discretizationFunction( ...
					modulatingFunctions.Md_bounded.', I, 'flip', false);
				filters.threshold(:,1) = obj.computeThreshold(I, ...
					modulatingFunctions.Md_bounded, optArgs.disturbanceType, ...
					"bounds", optArgs.bounds);
			end
			filters.dt = I.stepSize;
			filters.T = I.upper;
			filters.type = optArgs.type;
			filters.numel = I.n;
		end % function firFilters
		
		function threshold = computeThreshold(obj, I, MdBounded, disturbanceType, optArgs)
			% computeThreshold compute the threshold for the residual signal
			% threshold = computeThreshold(obj, I, MdBounded, disturbanceType, optArgs) computes the
			% threshold values for the residual signal for integral kernels MdBounded defined on the
			% time domain I. The type of the disturbance is specified "disturbanceType" and can be
			% "L2" for L2-bounded or "Linfty" for absolutely bounded disturbances.
			%
			% Further options as name-value pairs:
			%	"bounds": the corresponding bounds of the disturbance. If not set, the defined
			%	bounds of the kernel-object are used.
			arguments
				obj,
				I (1,1) quantity.EquidistantDomain;
				MdBounded quantity.Discrete
				disturbanceType (1,1) string;
				optArgs.bounds (:,1) double = obj.delta;
			end
			
			switch disturbanceType
				
				case "L2"
					% naiv implementation:
% 					rB = zeros( size(MdBounded, 2), 1 );
% 					for i = 1:size(MdBounded, 2)
% 						for j = 1:size(MdBounded, 1)
% 							rB(i) = rB(i) + MdBounded(j,i).l2norm(I) * optArgs.bounds(j);
% 						end
% 					end
					
					% nice implementation based on quantity
 					threshold = MdBounded.l2norm(I, "elementWise", true)' * optArgs.bounds;
					
				case "Linfty"
					threshold = int( abs( MdBounded.' ), I) * optArgs.bounds;
					
				otherwise
					error( ...
						"The threshold can only be computed for disturbances of the type L2" + ...
						"or Linfty but you requested a threshold for a " + disturbanceType + ...
						" disturbance, which is not implemented.")
			
			end % switch disturbanceType
			
		end
		
		function filters = generateFirFiltersTrapz(obj, I, modulatingFunctions, optArgs)
			% DEPRICATED
			arguments
				obj
				I (1,1) quantity.EquidistantDomain;
				modulatingFunctions
				optArgs.bounds (:,1) double = obj.delta;
				optArgs.type (1,1) string = "";
			end
			
			warning("!!!DEPRICATE!!!");
			
			filters.N = signals.fir.trapez(modulatingFunctions.N.', I, 'flip', false);
			filters.Mu = signals.fir.trapez(modulatingFunctions.Mu.', I, 'flip', false);
			filters.Mf = signals.fir.trapez(modulatingFunctions.Mf.', I, 'flip', false);
			filters.Md_bounded = signals.fir.trapez(modulatingFunctions.Md_bounded.', I, 'flip', false);
			filters.threshold(:,1) = int( abs( modulatingFunctions.Md_bounded.' ), I) * optArgs.bounds;
			filters.dt = I.stepSize;
			filters.T = I.upper;
			filters.type = optArgs.type;
			filters.numel = I.n;
		end % function firFilters
		
		function figureHandle = plotResult(obj, r, results, type, T, optArgs)
			% plotResult plots the fault detection/isolation/diagnosis results
			% figureHandle = plotResult(obj, r, results, type, T, optArgs) visualization of the
			% fault detection/isolation/diagnosis result. With "r", the residual signal is specified.
			% With "results", a structure that provides results from the fault detection. It is the
			% result from "firFaultDiagnosis". The "type" specifies whether the result corresponds
			% to a fault "detection", a "isolation", or "diagnosis".
			% With "T", the length of the moving horizon is specified. 
			%
			% Further name-value-pair arguments:
			%	"figureID": the number of the figure handle that should be used to plot the results
			%	"plotThresholds": logical variable to specify if the thresholds should be plotted.
			%	"f": plot the fault and if possible the filtered fault to verify the results
			%	"d_bounded": plot the filtered disturbance to verify the result
			%	"occurrence": if specififed the presence times of the faults are highlighted
			%	"disappears": see above.
			arguments
				obj
				r (:,1) quantity.Discrete;
				results
				type (1,1) string;
				T (1,1) double;
				optArgs.figureID (1,1) double = -1;
				optArgs.plotThresholds (1,1) logical = true;
				optArgs.f (:,1) quantity.Discrete;
				optArgs.d_bounded (:,1) quantity.Discrete;
				optArgs.occurrence (:,1) double;
				optArgs.disappears (:,1) double;
			end
			
			if ishandle( optArgs.figureID )
				set(groot, 'CurrentFigure', optArgs.figureID );
				figureHandle = gcf();
			else
				figureHandle = figure();
				optArgs.figureID = figureHandle.Number;
			end
			clf; h = subplot(size(r,1), 1, 1);
			
			defaultPlotOpts = {"figureId", optArgs.figureID, "hold", true};
			
			labels = {};
			
			if size(r,1) == 1
				defaultPlotOpts = [defaultPlotOpts(:)', {"smash"}, {true}];
			end
			
			if isfield( optArgs, "f")
				plot( optArgs.f, defaultPlotOpts{:}, "LineStyle", "-", "Color", "k");
				labels = {"$f(t)$"};
			end
			
			plot( r, defaultPlotOpts{:}, "Color", "red", "LineWidth", 2 );
			labels = {labels{:}, "$r(t)$"};
			
			if optArgs.plotThresholds
				% plot the threshold
				plot( results.boundUpper, defaultPlotOpts{:}, "Color", "green", ...
					"LineWidth", 1, "LineStyle", ":" );
				plot( results.boundLower, defaultPlotOpts{:}, "Color", "green", ...
					"LineWidth", 1, "LineStyle", ":" );
				labels = {labels{:}, "$f_B^+$", "$f_B^-$"};
				
				plot( results.fHatUpper,  defaultPlotOpts{:}, "Color", "green", ...
					"LineWidth", 1, "LineStyle", "--" );
				plot( results.fHatLower,  defaultPlotOpts{:}, "Color", "green", ...
					"LineWidth", 1, "LineStyle", "--" );
			end
			
			% plot initial detection window
			for i = 1:size(r,1)
				h = subplot(size(r,1), 1, i); 
				ylim = h.YLim;
				a = area([0, T], ...
					ylim(2) * ones(2,1), ylim(1));
				a.FaceColor = [0 0.4470 0.7410];
				a.FaceAlpha = 0.2;
				a.LineStyle = 'none';
			end
			
			% if available draw detection windows:
			if isfield(optArgs, "occurrence")
				for i = 1:length(optArgs.occurrence)
					for j = 1:size(r,1)
						h = subplot(size(r,1), 1, j);
						ylim = h.YLim;
						a = area([optArgs.occurrence(i), optArgs.occurrence(i)+T], ...
							ylim(2) * ones(2,1), ylim(1));
						a.FaceColor = [0 0.4470 0.7410];
						a.FaceAlpha = 0.2;
						a.LineStyle = 'none';
					end
				end
			end
			
			% if available draw present fault times
			if isfield(optArgs, "occurrence") && isfield(optArgs, "disappears")
				for i = 1:length(optArgs.occurrence)
					for j = 1:size(r,1)
						h = subplot(size(r,1), 1, j);
						ylim = h.YLim;
						a = area([optArgs.occurrence(i)+T, optArgs.disappears(i)], ...
							ylim(2) * ones(2,1), ylim(1));
						a.FaceColor = '#EDB120';
						a.FaceAlpha = 0.2;
						a.LineStyle = 'none';
						
						b = area([optArgs.disappears(i), optArgs.disappears(i)+T], ...
							ylim(2) * ones(2,1), ylim(1));
						b.FaceColor = [0 0.4470 0.7410];
						b.FaceAlpha = 0.2;
						b.LineStyle = 'none';
					end
				end
			end			
			
			if type == "detection"
				for i = 1:length(results.detection) * ~isempty(fieldnames(results.detection))
					subplot(size(r,1),1,results.detection(i).No)
					plot( results.detection(i).start, r(results.detection(i).No).on( results.detection(i).start ), ...
						'x', 'MarkerSize', 10, 'LineWidth', 2);
					
% 					ylim = h.YLim;
% 					a = area([results.detection(i).start, results.detection(i).windowEnd], ylim(2) * ones(2,1), ylim(1));
% 					a.FaceColor = [0.8500 0.3250 0.0980];
% 					a.FaceAlpha = 0.2;
% 					a.LineStyle = 'none';
					
				end
			end
			
			if type == "diagnosis"
				nF = obj.sys.n.f;
				% mark the detected fault in the plot
				for j = 1:length(results.detection) * ~isempty(fieldnames(results.detection))
					subplot(nF, 1, j), hold on
					tX = results.detection(j).start;
					rX = r(results.detection(j).No).on(tX);
					plot( tX, rX, 'x', 'MarkerSize', 15, 'LineWidth', 4, 'color', 'red')
					
					if j <= length( results.isolation )
						tIso = results.isolation(j).start;
						rIso = r(results.detection(j).No).on(tIso);

						plot( tIso, rIso, '*', 'MarkerSize', 15, 'LineWidth', 4)
					end
					% draw detection window:
					h = subplot(nF, 1, j);
					ylim = h.YLim;
					a = area([tX, results.detection(j).windowEnd], ylim(2) * ones(2,1), ylim(1));
					a.FaceColor = [0.8500 0.3250 0.0980];
					a.FaceAlpha = 0.2;
					a.LineStyle = 'none';
				end
			end
			
			
			% plot the reference solution for the fault estimation:
			if isfield( results, "rf" )
				plot( results.rf, defaultPlotOpts{:}, "LineStyle", "--", "Color", "b");
				% compute the maximal estimation error:
				% 				maxEstimationError = max(abs( ff - fHat  ));
				% 				l2EstimationError = int( (ff - fHat).^2 );
				% 				msgMax = misc.Message( "Maximal estimation error is " + num2str(maxEstimationError) + ".", "data", maxEstimationError );
				% 				msgL2 = misc.Message( "L2 estimation error is " + num2str(l2EstimationError) + ".", "data", l2EstimationError );
				% 				obj.report( msgMax );
				% 				obj.report( msgL2 );
				%
				labels = {labels{:}, "$\tilde{f}(t)$" };
				
			end
			
			if isfield( results, "rd")
				plot( results.rd, defaultPlotOpts{:}, "LineStyle", "--", "Color", "c", "LineWidth", 2);
			end
			
			legend(labels, "Interpreter", "Latex")
		end % function plotResults
		
	end % methods ( Access = public )
	
	
	methods ( Static )
		function [r, data, figureHandle] = firFaultDiagnosis(fir, y, u, optArgs)
			% FAULTDIAGNOSIS Perform the fault diagnosis
			%	[tHat, fHat, data] = faultDiagnosis(obj, filters, y, u, optArgs) will apply the
			%	fault diagnosis filter "filters" to the measurement y and input u. The filters must
			%	be defined in a struct with at least the fields: "N", "Mu" and "dt". Additional
			%	options can be specified with "optArgs". These are:
			%	"f": the fault that should be detected. If the fault is also passed to the function,
			%		 the filter r_f(t) = < Mf, f(t) > is also evaluated. This is helpful for
			%		 debugging.
			%	"d": the disturbance used for the simulation. TODO This property is not used so far.
			%		 This is also required to verify the term r_f(t) = < Mf, f(t) > + < Md, d(t) >,
			%		 which can be useful for debugging pruposes.
			%	"plot": defines if the result should directly be plotted
			%	"t": the time domain as quantity.EquidistantDomain with the same discretization as
			%	used for the generation of the fir filters.
			arguments
				fir
				y (:,1) quantity.Discrete;
				u (:,1) quantity.Discrete;
				optArgs.t (:,1) quantity.EquidistantDomain;
				optArgs.f (:,1) quantity.Discrete;
				optArgs.d_bounded (:,1) quantity.Discrete;
				optArgs.occurrence (:,1) double;				
			end
			
			%% input parsing:
			if isfield(optArgs, "t")
				t = optArgs.t;
			else
				t = y.domain;
				
				if ~isa(t, "quantity.EquidistantDomain")
					t = quantity.EquidistantDomain( t );
				end
				
				if fir.dt ~= t.stepSize
					t = quantity.EquidistantDomain(t.name, t.lower, t.upper, "stepSize", t.stepSize);
				end
			end

			
			% assert that the output and input filter is available
			assert( isfield(fir, "N") && isfield(fir, "Mu") && isfield(fir, "dt"));
			
			% assert that time discretization of t and the created filters is equal.
			assert( fir.dt == t.stepSize );
			
			%% evaluation of the fault diagnosis filters:
			r = signals.fir.mFilter(fir.N, y.on(t)) ...
				+ signals.fir.mFilter(fir.Mu, u.on(t));
			r = quantity.Discrete(r, t, "name", "fHat");
			
			if isfield( optArgs, "f")
				% compute ff = < M_f, f(t) >
				rf = signals.fir.mFilter( fir.Mf, optArgs.f.on(t) );
				rf = quantity.Discrete(rf, t);
			end
			
			if isfield( optArgs, "d_bounded")
				rd = -signals.fir.mFilter( fir.Md_bounded, optArgs.d_bounded.on(t) );
				rd = quantity.Discrete(rd, t);
			end
			
			fBoundUpper = fir.threshold(:) * quantity.Discrete.ones(1, t);
			fBoundUpper.setName("fBoundUpper");
			fBoundLower = - fBoundUpper;
			fBoundLower.setName("fBoundLower");
			
			% plot the estimation bounds:
			if isfield( optArgs, "f") && fir.type == "detection"
				fHatUpper = rf + fBoundUpper;
				fHatLower = rf - fBoundUpper;
			elseif isfield( optArgs, "f") && fir.type == "diagnosis"
				fHatUpper = optArgs.f + fBoundUpper;
				fHatLower = optArgs.f - fBoundUpper;
			else
				fHatUpper = r + fBoundUpper;
				fHatLower = r - fBoundUpper;
			end
			
			fHatUpper.setName("fHatUpper");
			fHatLower.setName("fHatLower");
			
			%% fault detection
			if fir.type == "detection"
				j = 0;
				detection = struct([]);
				for k = 1:size(r, 1)
					rData = r(k).on();
					faulty = zeros(size(rData));
					Tidx = size(fir.N,1);
					for i = Tidx+1:length(rData)
						rt = rData(i,:);
						if abs(rt) > fir.threshold(k)
							faulty(i) = 1;

							% if for a detection length no fault was detected, the exceed of the
							% threshold indicates the occurrence of a fault:
							if all( faulty(i-Tidx:i-1) == 0)
								j = j + 1;
								detection(j).idx = i;
								detection(j).start = t.grid(i);
								detection(j).No = k;
								detection(j).windowEnd = inf;
							end	
						else
							if all( faulty(i-Tidx:i-1) == 0 ) && j > 0 && detection(j).windowEnd == inf
								detection(j).windowEnd = t.grid(i);
							end
						end					
					end

					if j > 0 && detection(j).windowEnd == inf
						detection(j).windowEnd = t.grid(i);
					end

					numberOfDetectedFaults = j;
				end % for k
			end % fir.type == "detection"
			
			%% fault detection, isolation and identification
			if fir.type == "diagnosis" 
				fHat = r.on();
				faulty = logical(zeros(size(fHat)));
				Tidx = size(fir.N,1);
				nF = size(fir.N,2);
				faultNumbers = 1:nF;
				detection =  struct([]);; % struct("idx", [], "start", [], "windowEnd", []);
				isolation = struct();
				isolationProcess = false;
				isolatedFaults = logical(zeros(1, nF));
				k = 0;
				for i = Tidx+1:length(fHat)

					faulty(i,:) = abs( fHat(i,:)' ) > fir.threshold;
					
					% verify the NEW present faults, i.e., a fault is present AND NOT isolated					
					presentFaults = and(faulty(i,:), ~isolatedFaults);
					
					% ---FAULT DETECTION------------------------------------------------------------
					% A fault shall only be detected if the threshold is exceeded the first time!
					% Hence, it must be verified that in the detection window before no fault
					% detection has occured:
					% ----------------------
					% Logic for the occurrence of a fault: 
					% [a]: a threshold must be exceeded: presentFaults == 1
					% [b]: the threshold must not be exceeded for the length of a detection window
					% before:	faulty(i-Tidx:i-1,:) == 0
					% [c]: [a] and [b] must be true for the length of a detection window: 
					%		all( [a] & [b] )
					% [d]: additionaly, no other fault is in the isolation process: [c] &
					% ~isolationProcess
					% For the detection it is sufficient if any fault has occurred: any([c])		
					% ------------------------------------------------------------------------------
					if any( all( (faulty(i-Tidx:i-1,:) == 0 ) & presentFaults ) ) &&  ~isolationProcess
						k = k + 1;
						detection(k).idx = i;
						detection(k).No = faultNumbers( presentFaults );
						detection(k).start = t.grid( i );
						
						if i + Tidx > t.n
							detection(k).windowEnd = t.n;
						else
							detection(k).windowEnd = t.grid( i + Tidx);
						end
						%isolation(k).start = 0;
						isolationProcess = true;
					end

					% ----FAULT ISOLATION-----------------------------------------------------------
					% After the detection window length T of the occurrence of a fault (that is not
					% isolated), the next detection value above the threshold is considered as
					% fault!
					if isolationProcess && i >= (detection(end).idx + Tidx) && any( presentFaults )
						isolation(k).idx= i;
						isolation(k).start = t.grid( i );
						detection(k).stop = t.grid( i );
						isolation(k).No = faultNumbers( presentFaults );
						isolationProcess = false;
						isolatedFaults = isolatedFaults | faulty(i,:);
					end
				end
				numberOfDetectedFaults = k;
			end
			
			if nargout >= 2
				
				parameters = struct();

				for i = 1:size(r,1)
					paramters.("fb" + num2str(i)) = fir.threshold(i);
				end
				
				data.detection = detection;
				if (fir.type == "detection" || fir.type == "isolation" ...
						|| fir.type == "diagnosis") && numberOfDetectedFaults > 0
					for j = 1:numel(detection)
						parameters.("detection" + num2str(j)) = detection(j).start;
						parameters.("detectionT" + num2str(j)) = detection(j).windowEnd;
						
						if isfield(detection(j), "No")
							parameters.("detectionNo" + num2str(j)) = detection(j).No;
							parameters.("faultDetection" + num2str(j)) = ...
								r(detection(j).No).at(detection(j).start);	
						end
					end
				end
				
				if fir.type == "diagnosis" && ~isempty( fieldnames( isolation ) )
					data.isolation = isolation;
					for i = 1:numel(isolation)
						parameters.("isolation" + num2str(i)) = isolation(i).start;
						parameters.("isolationT" + num2str(i)) = isolation(i).start + fir.T;
						parameters.("isolationNo" + num2str(i)) = isolation(i).No;
						parameters.("faultIsolation" + num2str(i)) = ...
							r(isolation(i).No).at(isolation(i).start);
						
						if isfield(optArgs, "occurrence")
							parameters.("occurrence" + num2str(i)) = optArgs.occurrence(i);
							parameters.("occurrenceT" + num2str(i)) = optArgs.occurrence(i) + fir.T;
							parameters.("detectionDelay" + num2str(i)) = detection(i).start - ...
								optArgs.occurrence(i);
							
						end
					end
				end

				data.parameters = parameters;
				
				if isfield( optArgs, "f")
					data.rf = rf;
				end
				
				if isfield( optArgs, "d_bounded")
					data.rd = rd;
				end
				
				data.boundUpper = fBoundUpper;
				data.boundLower = fBoundLower;
				data.fHatUpper = fHatUpper;
				data.fHatLower = fHatLower;

			end
		end % function faultDiagnosis
		
	end % methods (Static)
	
end
