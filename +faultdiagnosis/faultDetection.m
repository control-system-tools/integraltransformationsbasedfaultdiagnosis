function [tEvent] = faultDetection(f, t, T, thresholds)
% FAULTDETECTION automatic detection of faults
% [tEvent] = faultDetection(f, t, T, thresholds) first try to implement an automatic fault detection
% based on the fault estimation results and the thresholds.

assert( max(diff(diff(t))) < 1e15, 'the grid must be equidistant');
dt = mean(diff(t));

window = 0:dt:T;

assert( window(end) - T < 1e-15, 'the grid must be equidistantly on 0:dt:T');

tEvent = 0;
idxEvent = [];
idxDetection = [];

fDetected = zeros(size(f,2),1);

for k = 1:length(t)
	
	if (tEvent(end) + T) < t(k) && any( abs(f(k,:)').*~fDetected > thresholds)
		% check that the last event is past longer than T
		% and
		% a threshold is exceeded
		tEvent(end+1) = t(k);
		idxEvent(end+1, :) = abs(f(k,:)') > thresholds;
		
	elseif tEvent(end) + T == t(k) && any( abs(f(k,:)').*~fDetected > thresholds )
		% first time step after the detection of an event 
		%	and
		% there is still an event:
		%
		% fault identification:
		% find out which fault has occurred:
		fDetected_ = abs(f(k,:)') > thresholds;
		idxDetection(end+1, :) = fDetected_;
		fDetected = fDetected | fDetected_;
	end
	
end

end