function x = Psi(z, tau, h, tau_plus_int_Lambda, V_p, A_0_zeta)
	% Psi-operator
	% x = Psi(obj, h) compute the operator
	%	x = Psi[h(.)](z,t)
	
	nX = size( tau_plus_int_Lambda, 1);
	
	x = quantity.Discrete.zeros( [nX, size(h, 2) ], [z, tau]);
	
	for i = 1 : nX	
		Hi = h.compose( tau_plus_int_Lambda(i) );		
		x(i,:) = V_p(i,:) * subs(Hi, "zeta", 0) + int( A_0_zeta(i,:) * Hi, "zeta", 0, "z" );
	end
end %Psi