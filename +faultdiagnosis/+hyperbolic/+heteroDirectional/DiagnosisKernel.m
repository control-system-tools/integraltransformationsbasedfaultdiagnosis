classdef DiagnosisKernel < faultdiagnosis.hyperbolic.heteroDirectional.DetectionKernel
	% System for the fault diagnosis kernel
	%   dz m(z,t) = Lambda_1(z) dt m(z,t) + A_0(z) m_p(0,t)
	%   m_\n(0,t) = Q_0 m_\p(0,t)
	%   m_\p(1,t) = n(t)
	%   dt eta(t) = F eta(t) + int_0^1 L_1(z) m(z,t) dz + L_2(z) m_p(0,t)
	%              + L_3 m_n(1,t)
	%
	% This is the representation of the fault-diagnosis kernel with
	% inverted spatial coordinate.
	
	properties
		% the combine final state for ode-model and fault-model
		%	W_0 = [ 0; R_f; 0]
		q0 double;
		
		% ode model for the fault:
		faultModel (:,1) signals.SignalModel;
		
		% Selection matrix for the fault-model-ODE
		%	dtau q(tau) = S^T q(tau) + Q_T^T * ( int_0^1 E_1^T(z) m(z,tau)
		%	dz + E_2^T m_n(0,tau) + E_3^T m_p(1,tau) + E_4^T p(tau) )		
		J_ode_q_f double;
	end
	
	methods
		function obj = DiagnosisKernel(sys, faultModel, optArgs)
			% System for the fault diagnosis
			%  dz m(z,t) = Lambda_1(z) dt m(z,t) + A_0(z) m_p(z,t)
			%  m_\n(0,t) = Q_0 m_\p(0,t)
			%  m_\p(1,t) = n(t)
			%	 dt w(t) = S w(t) + int_0^1 L_1(z) m(z,t) dz ...
			%              + L_2 x_p(0,t) + L_3 x_n(1,t)
			arguments
				sys faultdiagnosis.hyperbolic.heteroDirectional.System;
				faultModel signals.SignalModel
				optArgs.disturbanceModel signals.SignalModel;
				optArgs.tolerance (1,1) double = 1e-4;
				optArgs.referenceGrid (:,1) double = sys.Lambda_1(1).domain.grid;
				optArgs.numberOfLines (1,1) double = 11; %TODO check this value!
				optArgs.backsteppingKernel;
			end
			
			assert( size( faultModel.ss.C, 1) == sys.n.f, ...
				"Number of faults does not fit to the fault model");
			
			eta = faultdiagnosis.hyperbolic.heteroDirectional.DetectionKernel.commonOde( sys, ...
					"disturbanceModelOde", optArgs.disturbanceModel, "faultModelOde", faultModel);
			
			parentArgs = namedargs2cell( optArgs );
				
			obj@faultdiagnosis.hyperbolic.heteroDirectional.DetectionKernel( sys, "eta", eta, ...
				parentArgs{:});
			
			
			obj.q0 = eta.O;
			obj.faultModel = faultModel;
			obj.J_ode_q_f = eta.J_q_f;
		end
		
		function outputArg = method1(obj,inputArg)
			%METHOD1 Summary of this method goes here
			%   Detailed explanation goes here
			outputArg = obj.Property1 + inputArg;
		end
	end
	
	methods (Access = public)
		
		function [ioKernels, measures, kernel] = computeSolution(obj, tau, optArg)
			arguments
				obj,
				tau,
				optArg.dynamicalExtension = 0;
				optArg.usePolyomial = false;
				optArg.silent = false;
				optArg.etaSim = true
				optArg.delta = ones( obj.sys.n.d_bounded, 1);
			end
			
			% turn off the warning for the simulation at non-zero time.
			h=ctrlMsgUtils.SuspendWarnings("Control:analysis:LsimStartTime");
			
			% 			myProgressBar = misc.ProgressBar('name', "Fault diagnosis kernel solution", ...
			% 			'terminalValue',
			%
			% check that T is seperable into dt
% 			if mod(T, dt) ~= 0
% 				warning('The moving horizon length T should be a multiple of dt. Thus it is changed to the next higher value');
% % 				T = ceil( T / dt ) * dt;
% 			end
			
			%% Define the time domains:
			% the time-domain for the transition is separated into three
			% parts. These parts are obtained from the initial and end
			% value for the kernel m(z,tau). Thus, the parts are defined as
			%	I_1 = [-t_Delta_minus, t_Delta_plus]
			%	I_2 = (t_Delta_plus, T - t_Delta_minus)
			%	I_3 = [T - t_Delta_minus, t_Delta_plus]
			
			T = tau.upper;
			dt = tau.stepSize;
			
			I = quantity.Domain('tau', obj.theta_min:dt:T+obj.theta_max);
			I = I.split( [ obj.theta_max, T + obj.theta_min ], "addPoints", true );
			
			alpha = obj.differentialExpressions.alpha;
			S = obj.differentialExpressions.S;
			

% 			a = flip( charpoly( sym( obj.F ) ) );
% 			A = [ sym( diag(ones(obj.n.w-1,1), 1) )];
% 			A(end,:) = -a(1:end-1);

			% due to heteroDirectional.Kernel, n_w is n_eta from the notes
			%        /  0     1     0             \
			%        |  0     0     1             |
			% A_xi = |                            |
			%        |  0     0     0        1    |
			%        \-a_0  -a_1  -a_2 ... -a_n-1 /
			A_xi = [zeros(obj.n.w-1, 1), eye(obj.n.w-1); ...
				-alpha(1:end-1)];
			B_xi = [zeros(obj.n.w - 1, 1); ...
				1];
			
			n_star = optArg.dynamicalExtension;
			if n_star > 0
				A_star = [zeros(n_star-1, 1), eye(n_star - 1); ...
					zeros(1, n_star)];
				B_star = misc.unitVector(n_star, n_star);
				C_star = misc.unitVector(n_star, 1).';
				xi_model = ss([A_star, zeros(n_star, obj.n.w); ...
					B_xi * C_star, A_xi], ...
					[B_star; zeros(obj.n.w, 1)], ...
					misc.unitVector(n_star + obj.n.w, 1).', []);
			else
				xi_model = ss(A_xi, B_xi, zeros(1, obj.n.w), 1);
			end
			
			% Compute the state transition matrix: Phi(t,tau) = expm(A*(t-tau) )
			Phi_I2 = expm( xi_model.A * (I(2).Discrete - I(2).lower) );
			invPhi_t1 = expm(xi_model.A * (I(2).upper - I(2).Discrete));
			
			%int_t0^t1 expm(A*(tau-t0)) * b * b^T * expm(A^T(tau-t0)) dtau
			gramian = int( Phi_I2 * xi_model.b * xi_model.b' * ...
				expm(xi_model.A' * (I(2).Discrete - I(2).lower)), I(2), I(2).lower, I(2).name);
			gramian_t1  = gramian.at(I(2).upper);
			

			% Phi(tau, -tau^-) = expm(A * (tau + tau^-) )
			% Error? -> Phi_xi = expm( A_xi * quantity.Discrete(I(1).grid, I(1)));				
			Phi_xi_I1 = expm( A_xi * ( I(1).Discrete - I(1).lower ) );
			
			t2 = tic;
			Upsilon = zeros(obj.n.w, obj.n.w * obj.n.p);
			
			% compute all W_0 matrices:
			for i = 1:obj.n.w
				Psi_Phi_i = quantity.Discrete.zeros( [obj.n.x, obj.n.p*obj.n.w], obj.spatialDomain );
				for j = 1:obj.sys.n.y
					idx = ((j-1)*obj.n.w + 1):j*obj.n.w;
					Phi_xi_i = misc.unitVector(obj.n.w, i)' * Phi_xi_I1;
					Psi_Phi_i(:, idx) = obj.Psi(Phi_xi_i, 0, 'idx_j', j);
				end
				
				Upsilon = Upsilon ...
					+ int( S(:,:,i) * obj.L_1  * Psi_Phi_i, "z", 0, 1) ...
					+ S(:,:,i) * obj.L_2 * Psi_Phi_i.at(0) ...
					+ S(:,:,i) * obj.L_3 * Psi_Phi_i.at(1);
				
			end
			
			pinvUpsilon = pinv( Upsilon );
			Z = null( Upsilon );

			[isSolvable, condition] = ...
				numeric.near( Upsilon * pinvUpsilon * obj.q0, obj.q0, 4e-9);
			
			obj.logMsg("Condition of the detectability conditionis %g\n", condition);
			assert( isSolvable, ...
				'Fault is not detectable. The compatibility condition is %d\n', condition);
			
			obj.logMsg("Computation of W_0: %g seconds\n", toc(t2));
					
			%% computation of the io-kernels in terms of backstepping coordinates
			[bsCoordinates.G1, bsCoordinates.G2, bsCoordinates.G3, bsCoordinates.G4] ...
				= obj.ioCoefficientsInBacksteppingCoordinates("G");
			
			ThetaMu_I1 = quantity.Discrete.zeros( [1, obj.n.w], I(1));
			ThetaMu_III = quantity.Discrete.zeros( [1, obj.n.w], I(3));
			ThetaMu_I2 = - xi_model.b.' * invPhi_t1.' * mldivide( gramian_t1, Phi_I2.at(I(2).upper) ) * Phi_xi_I1.atIndex(end);
			ThetaMu_j = quantity.Piecewise({ThetaMu_I1, ThetaMu_I2, ThetaMu_III}, "addPoint", true);
			
			PsiThetaMu = [];
			for j = 1:obj.sys.n.y
				parfor k = 1:obj.n.w
					PsiThetaMu = [PsiThetaMu, obj.Psi( ThetaMu_j(:,k), tau, "idx_j", j )];
				end
			end			
			
			ThetaMu = subs(kron( eye(obj.sys.n.y), ThetaMu_j), "tau", tau);
			
			% d_tau q(tau) = F q(tau) + ...
			% q(tau) = Phi_q(tau,0) * q(0) + Phi_q(tau,0) * INT( ... )
			% Phi_q(tau, 0) = e^(A * tau)
			PhiQ = expm( obj.F * ( tau.Discrete ) ); 
			% 
			ThetaQ = PhiQ * int( expm( obj.F * ( - tau.Discrete ) ) ...
				* ( int( obj.L_1 * PsiThetaMu, "z", 0, 1) ... 
				+ obj.L_2 * obj.Jn.' * ThetaMu ...
				+ obj.L_3 * PsiThetaMu.subs("z", 1) ), "tau", 0, "tau");
			
			thetaD = int( bsCoordinates.G1 * PsiThetaMu, "z", 0, 1) ... 
				+ bsCoordinates.G2 * ThetaMu + bsCoordinates.G3 * PsiThetaMu.subs("z", 1) ...
				+ bsCoordinates.G4 * ThetaQ;
			
			theta0 = obj.sys.G_bounded.' * (thetaD * pinvUpsilon + bsCoordinates.G4 * PhiQ);
			theta1 = obj.sys.G_bounded.' * thetaD * Z;
			
			
			% Compute the coefficient matrices for the io-kernels in backstepping coordinates:
			% for the m_f(\tau) io-kernel:
			[E1, E2, E3, E4] = obj.ioCoefficientsInBacksteppingCoordinates("E", -1);
			% m_f = ThetaF * eta
			ThetaF.xi = int( E1 * PsiThetaMu, "z", 0, 1) + E2 * ThetaMu + ...
				E3 * PsiThetaMu.subs("z", 1) + E4 * ThetaQ;
			ThetaF.q = E4 * PhiQ;
			
			% for the m_{\bar{d}}(\tau) io-kernel:
			[G1, G2, G3, G4] = obj.ioCoefficientsInBacksteppingCoordinates("G", obj.sys.G_bounded.');
			%
			ThetaGBar.xi = int( G1 * PsiThetaMu, "z", 0, 1) + G2 * ThetaMu + ...
				G3 * PsiThetaMu.subs("z", 1) + G4 * ThetaQ;
			ThetaGBar.q = G4 * PhiQ;
			
			% for the m_u io-kernel:
			[B1, B2, B3, B4] = obj.ioCoefficientsInBacksteppingCoordinates("B");
			ThetaU.xi = int( B1 * PsiThetaMu, "z", 0, 1) + B2 * ThetaMu + ...
				B3 * PsiThetaMu.subs("z", 1) + B4 * ThetaQ;
			ThetaU.q = B4 * PhiQ;
						
			% for the n io-kernel
			[C1, C2, C3, C4] = obj.ioCoefficientsInBacksteppingCoordinates("C");
			ThetaN.xi = int( C1 * PsiThetaMu, "z", 0, 1) + C2 * ThetaMu + ...
				C3 * PsiThetaMu.subs("z", 1) + C4 * ThetaQ;
			ThetaN.q = C4 * PhiQ;
			
			% #todo: Parallelisierung
			for i = 1:size(obj.q0, 2)
				
				% turn off the warnings:
				warning("off", "Control:analysis:LsimStartTime")
				
				obj.logMsg("Start computation of kernel %i\n", i);
				tic_i = tic;
				
				qT = at( PhiQ + ThetaQ * pinvUpsilon, tau.upper ) * obj.q0(:,i);
				qZT = at( ThetaQ, tau.upper) * Z;
				
				% as an additional objective for the determination of the degrees of freedom, 
				% J0 = |q(T)| 
				% is introduced. This should be zero. But because of numerical errors, the resulting
				% trajectory does not always reach zero. However, to ensure that it will be close to
				% zero this additional objective is introduced.
				Jadd = @(x) sum( abs( qT + qZT*x) ) * 1;
				
 				[xiStar, fBi(:,i), fBi0(:,i)] = faultdiagnosis.thresholdOptimizerL1(obj, theta0 * obj.q0(:,i), theta1, ...
					optArg.delta, "use0asInitialPoint", true, "additionalObjective", Jadd );
				
				% compute the initial condition for the interval I1:
				% xi_i_0 = xi_i( -tau^- )
				xi_i_0 = pinvUpsilon * obj.q0(:,i) + Z * xiStar;
				
				Mf(:,i) = ThetaF.xi * xi_i_0 + ThetaF.q * obj.q0(:,i);
				Md_bounded(:,i) = ThetaGBar.xi * xi_i_0 + ThetaGBar.q * obj.q0(:,i);
				Mu(:,i) = ThetaU.xi * xi_i_0 + ThetaU.q * obj.q0(:,i);
				N(:,i) = ThetaN.xi * xi_i_0 + ThetaN.q * obj.q0(:,i);
				Xi_0(:,i) = xi_i_0;
				XiStar(:,i) = xiStar;
				
			end

			% set the names:
			N.setName("N");
			Mf.setName("Mf");
			Md_bounded.setName("M_{\bar{d}}");
			Mu.setName("Mu");
			
			% for the integral kernel results local variables must be used, because the parfor does
			% not allow to use the structure directly. Thus, the local variables can only be
			% summarized to the structure after the parfor loop.
			ioKernels.Mf = Mf;
			ioKernels.Md_bounded = Md_bounded;
			ioKernels.Mu = Mu;
			ioKernels.N = N;
			ioKernels.My = ThetaMu * Xi_0;
			
			measures.Upsilon = Upsilon;
			measures.thresholds = fBi;
			measures.fBi0 = fBi0;
			
			if nargout >= 3
				for i = 1:size(obj.q0, 2)

					Mtilde(:,i) = PsiThetaMu * Xi_0(:,i);
					% 					Q = PhiQ * obj.q0(:,i) + ThetaQ * xi_i_0;
					Q(:,i) = ( PhiQ + ThetaQ * pinvUpsilon ) * obj.q0(:,i) + ThetaQ * Z * XiStar(:,i);

				end
				Mtilde.setName("M");
				Q.setName("Q");
				
				kernel.Mtilde = Mtilde;
				kernel.Q = Q;
			end
			
		end % compute solution
		
		
		
		function [filters, modulatingFunctions, kernels] = computeIdentificationFilter(obj, I, optArgs)
			arguments
				obj
				I (1,1) quantity.EquidistantDomain
				optArgs.bounds (:,1) double = ones(obj.sys.n.d_bounded,1)
				optArgs.dynamicalExtension (1,1) double = 0
				optArgs.firApproximation (1,1) string = "firstOrderHold"
				optArgs.kernels 
			end			
			
			% copmute identification kernels:
			if isfield(optArgs, "kernels")
				kernels = optArgs.kernels;
			else
				[kernels, tau, mdBar] = obj.computeSolution( I.stepSize, I.upper, "delta", optArgs.bounds);
			end
				
			% compute the modulating functions
			modulatingFunctions.Mf = - obj.computeModulatingFunction("E", kernels.M, kernels.P, kernels.N);
			modulatingFunctions.Mu = obj.computeModulatingFunction("B", kernels.M, kernels.P, kernels.N);
			modulatingFunctions.Md = obj.computeModulatingFunction("G", kernels.M, kernels.P, kernels.N);
			modulatingFunctions.Md_bounded = ...
				obj.sys.G_bounded.' * modulatingFunctions.Md;
			modulatingFunctions.Md_deterministic = ...
				obj.sys.G_deterministic.' * modulatingFunctions.Md;
			modulatingFunctions.Md_stochastic = ...
				obj.sys.G_stochastic.' * modulatingFunctions.Md;
			modulatingFunctions.N = kernels.N;
			
			% compute the time-discrete FIR filters
			if optArgs.firApproximation == "firstOrderHold"
				filters.N = signals.fir.firstOrderHold(kernels.N.', I, 'flip', false);
				filters.Mu = signals.fir.firstOrderHold(modulatingFunctions.Mu.', I, 'flip', false);
				filters.Mf = signals.fir.firstOrderHold(modulatingFunctions.Mf.', I, 'flip', false);
			elseif optArgs.firApproximation == "trapz"
				filters.N = signals.fir.trapez(kernels.N.', I, 'flip', false);
				filters.Mu = signals.fir.trapez(modulatingFunctions.Mu.', I, 'flip', false);
				filters.Mf = signals.fir.trapez(modulatingFunctions.Mf.', I, 'flip', false);				
			else
				error("FIR approximation method " + optArgs.firApproximation + " is not known.")
			end
			filters.threshold = obj.thresholdVectOfAbsValues( modulatingFunctions.Md_bounded, optArgs.bounds);
			filters.dt = I.stepSize;
			filters.T = I.upper;
			filters.type = "diagnosis";
		end
		
		function fS = thresholdStochastic(obj, M_G_stochastic, vectorOfVariances, dt, P)
			
			S0 = dt * diag(vectorOfVariances);
			variance_f = diag( double( int( M_G_stochastic.' * S0 * M_G_stochastic ) ) );
			F_stochastic = signals.WhiteGaussianNoise( sqrt( variance_f ) );
			
			fS = F_stochastic.calculateThresholdForProbability(ones(obj.n.f,1) * P);
			
		end
		
		function fB = thresholdVectOfAbsValues(obj, M_G_bounded, d_bound)
			fB = int( abs(M_G_bounded) ).' * d_bound;
		end
		
		function fB = thresholdSchwarzIneq(obj, M_G_bounded, d_bound)
			T = M_G_bounded(1).domain.upper;
			fB = sqrt(T) * sqrt(int( M_G_bounded.^2 )).' * d_bound;
			
		end
	end
	
	
end

