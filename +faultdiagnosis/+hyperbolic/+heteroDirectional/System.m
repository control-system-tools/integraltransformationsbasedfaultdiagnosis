classdef System < faultdiagnosis.System
	% System for the fault diagnosis
    %  dz x(z,t) = Lambda_1(z) dt x(z,t) + Lambda_0(z) x(z,t) + A_0(z) x_p(0,t)
    %              + int_0^z D(z,zeta) x(zeta, t) dz + H_1(z) w(t)
	%              + E_1(z) f(t) + G_1(z) d(t) + B_1(z) u(t)
	%  x_p(0,t) = Q_0 x_n(0,t) + Hb0 w(t) + Eb0 f(t) + Gb0 d(t)
	%  x_n(1,t) = Q_1 x_p(1,t) + Hb1 w(t) + Eb1 f(t) + u(t) + Gb1 d(t)
	%    dt w(t) = F w(t) + int_0^1 L_1(z) x(z,t) dz + L_2 x(0,t) 
	%              + L_3 x(1,t) + E_3 f(t) + G_3 d(t) + B_3 u(t)
	%       y(t) = C_0 x_n(0,t) + E_4 f(t) + G_4 d(t) + H_4 w(t)
	%
	%
	%	The signal model for the fault and the distrubance will be
	%	specified in the generation of the fault diagnosis kernel:
	% \dt v_f(t) = S_f v_f(t)
	%       f(t) = R_f v_f(t)
	% \dt v_d(t) = S_d v_d(t)
	%       d(t) = R_d v_d(t)
	% The disturbance is separated into an deterministic and arbitrary but
	% bounded component:
	%	d(t) = G_deterministic d_deterministic(t) + G_bounded d_bounded(t)
	%
	%
	% The structure for the dimensions of the system has the fields:
	% .x	number of the pde states
	% .w	number of the ode states
	% .u	number of the inputs
	% .y	number of the outputs
	% .f	number of the faults
	% .d	number of the disturbances
	% .d_deterministic number of the disturbances described by an ode
	% .d_bounded number of the unknown but bounded disturbances
	% .d_stochastic NOT YET IMPLEMENTED
	% .p	number of states with a transport in positive spatial direction
	% .n	number of states with a transport in negative spatial direction
	properties
		%% pde system parameter matrices:

		% transportation speed matrix
		Lambda_1 quantity.Discrete
		% reaction term matrix
		Lambda_0 quantity.Discrete
		% Boundary coupling into the pde
		A_0 quantity.Discrete
		% integral term matrix		
		D quantity.Discrete
		% boundary conditions
		Q_0 double
		Q_1 double
		
		%% control input
		Bb0 double
		Bb1 double
		
		%% fault input matrices:	
		% indomain fault input
		Eb0 double
		Eb1 double
		
		%% distrubance inputs
		Gb0 double
		Gb1 double
		
%		G_1 quantity.Discrete
% 		Gb0 double -> Gb0
% 		Gb1 double -> Gb1
% 		G_3 double -> Gb1
% 		G_4 double -> G_3
%		G_5 -> G_4
% 		G_deterministic double
% 		G_bounded double
% 		G_stochastic double
		
% TODO L_1; L_2; L_3
		% The usage of L_2 in the text and the code is different:
		% L_2 = LL2 * Jn, where LL2 is the matrix from the text.
		% => LL2 = L_2 * Jn' if L_2 * Jp' = 0

		%% ODE to PDE coupling
		Hb0 double
		Hb1 double
		H_4 double
		
		% output
		C_0 double
		
		% 		
		systemOde model.TransportOde %todo make it  hidden and/or protected
	
		Jn;		% selector matrix for x_n = Jn * x
		Jp;		% selector matrix for x_p = Jp * x
		
		theta_min double;
		theta_max double;
		
		stepSize (1,1) double;
		
	end
	
	properties (Access = protected)
		Int_zeta_z_Lambda_1_dz;
	end
	
	
 	methods
		function obj = System(Lambda_1, F, optArg)
			% System for the fault diagnosis
			%  dz x(z,t) = Lambda_1(z) dt x(z,t) + Lambda_0(z) x(z,t) 
			%				+ A_0(z) x_\p(0,t)
			%				+ int_0^z D(z,zeta)	x(zeta, t) dz + H_1(z) w(t)
			%				+ E_1(z) f(t) + G_1(z) d(t)
			%  x_\n(0,t) = Q_0 x_\p(0,t) + Hb0 w(t) + Eb0 f(t) + Gb0 d(t)
			%  x_\p(1,t) = Q_1 x_\n(1,t) + Eb1 f(t) + u(t) + Gb1 d(t)
			%	 dt w(t) = F w(t) + L_2 x(0,t) + E_3 f(t) + G_3 d(t)
			%		y(t) = x_\p(0,t) + E_4 f(t) + G_4 d(t)
			%
			% obj = System(Lambda_1, F, optArg) will initialize a system of
			% the described form above. Lambda_1 and F are mandatory
			% arguments. The further system parameters can be specified by
			% name-value-pair arguments.
			arguments
				% system parameters
				Lambda_1 quantity.Discrete;	
				F double
				optArg.Lambda_0 quantity.Discrete
				optArg.A_0 quantity.Discrete
				optArg.D quantity.Discrete				
				optArg.Q_0 double
				optArg.Q_1 double
				% ode to pde coupling
				optArg.H_1 quantity.Discrete
				optArg.Hb0 double
				optArg.Hb1 double
				optArg.H_4 double
				% control inputs
				optArg.B_1 quantity.Discrete
				optArg.Bb0 double
				optArg.Bb1 double
				optArg.B_3 double
				% fault inputs
				optArg.E_1 quantity.Discrete
				optArg.Eb0 double
				optArg.Eb1 double
				optArg.E_3 double
				optArg.E_4 double
				% disturbance inputs
				optArg.G_1 quantity.Discrete
				optArg.Gb0 double
				optArg.Gb1 double
				optArg.G_3 double
				optArg.G_4 double
				optArg.G_deterministic double
				optArg.G_bounded double
				optArg.G_stochastic double
				% pde to ode couplings
				optArg.L_1 quantity.Discrete
				optArg.L_2 double
 				optArg.L_3 double	
				% output
				optArg.C_0 double
% 				% 				
				optArg.stepSize (1,1) double = 0.1;
 				optArg.name string = "";
			end
						
			%% compute the required dimensions of this system
			dim.n = size( Lambda_1, 1);
			[dim.Jn, dim.Jp, dim.n_n, dim.n_p] = ...
				faultdiagnosis.hyperbolic.heteroDirectional.System.stateSelectionMatrices( Lambda_1 );
			% TODO Assert descending sorting of velocities 1/lambda_i in
			% Lambda_1.
			invL_vect = inv(Lambda_1) * ones(dim.n,1);
			
			deltaL = invL_vect(1:end-1) - invL_vect(2:end);

			assert( all( deltaL.on() > 0, 'all' ), 'Transport velocities are not sorted descending' )
			
			dim.n_w = size( F, 1 );
			dim.n_u = misc.getDimensions(optArg, ["B_1", "Bb0", "Bb1", "B_3"], 2);
			dim.n_f = misc.getDimensions(optArg, ["E_1", "Eb0", "Eb1", "E_3", "E_4"], 2);
			dim.n_d = misc.getDimensions(optArg, ["G_1", "Gb0", "Gb1", "G_3", "G_4"], 2);
			dim.n_y = dim.n_n;
						
			% do some assertions on the dimensions
			assert(dim.n == dim.n_n + dim.n_p);
			
			spatialDomain = Lambda_1(1).domain;
			
			%% set the system values to the properties.
			% This is outsourced from constructor in order to use the
			% arguments block with the default values and size validation.
			myParser = misc.Parser();
			myParser.addParameter('Lambda_0', ...
				quantity.Discrete.zeros( [dim.n, dim.n], spatialDomain ), ...
				@(A) validateattributes(A, {'quantity.Discrete'}, {'size', [dim.n, dim.n]}));
			myParser.addParameter('A_0', ...
				quantity.Discrete.zeros( [dim.n, dim.n_p], spatialDomain ), ...
				@(A) validateattributes(A, {'quantity.Discrete'}, {'size', [dim.n, dim.n_p]}));
			myParser.addParameter('D', ...
				quantity.Discrete.zeros( [dim.n, dim.n], [spatialDomain, spatialDomain.rename('zeta')] ), ...
				@(A) validateattributes(A, {'quantity.Discrete'}, {'size', [dim.n, dim.n]}));
			myParser.addParameter('Q_0', zeros(dim.n_n, dim.n_p), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_p, dim.n_n]} ));
			myParser.addParameter('Q_1', zeros(dim.n_n, dim.n_p), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_n, dim.n_p]} ));
			
			% ode to pde couplings
			myParser.addParameter('H_1', ...
				quantity.Discrete.zeros( [dim.n, dim.n_w], spatialDomain ), ...
				@(A) validateattributes(A, {'quantity.Discrete'}, {'size', [dim.n, dim.n_w]}));
			myParser.addParameter('Hb0', zeros(dim.n_p, dim.n_w), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_p, dim.n_w]} ));
			myParser.addParameter('Hb1', zeros(dim.n_n, dim.n_w), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_n, dim.n_w]} ));
			myParser.addParameter('H_4', zeros(dim.n_n, dim.n_w), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_n, dim.n_w]} ));
		
			% control inputs
			myParser.addParameter('B_1', ...
				quantity.Discrete.zeros( [dim.n, dim.n_u], spatialDomain ), ...
				@(A) validateattributes(A, {'quantity.Discrete'}, {'size', [dim.n, dim.n_u]}));
			myParser.addParameter('Bb0', zeros(dim.n_p, dim.n_u), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_p, dim.n_u]} ));
			myParser.addParameter('Bb1', zeros(dim.n_n, dim.n_u), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_n, dim.n_u]} ));
			myParser.addParameter('B_3', zeros(dim.n_w, dim.n_u), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_w, dim.n_u]} ));
			myParser.addParameter('B_4', zeros(dim.n_y, dim.n_u), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_y, dim.n_u]} ));
			% fault inputs
			myParser.addParameter('E_1', ...
				quantity.Discrete.zeros( [dim.n, dim.n_f], spatialDomain ), ...
				@(A) validateattributes(A, {'quantity.Discrete'}, {'size', [dim.n, dim.n_f]}));
			myParser.addParameter('Eb0', zeros(dim.n_p, dim.n_f), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_p, dim.n_f]} ));
			myParser.addParameter('Eb1', zeros(dim.n_n, dim.n_f), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_n, dim.n_f]} ));
			myParser.addParameter('E_3', zeros(dim.n_w, dim.n_f), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_w, dim.n_f]} ));
			myParser.addParameter('E_4', zeros(dim.n_y, dim.n_f), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_y, dim.n_f]} ));
			
			
			% disturbance inputs
			myParser.addParameter('G_1', ...
				quantity.Discrete.zeros( [dim.n, dim.n_d], spatialDomain ), ...
				@(A) validateattributes(A, {'quantity.Discrete'}, {'size', [dim.n, dim.n_d]}));
			myParser.addParameter('Gb0', zeros(dim.n_p, dim.n_d), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_p, dim.n_d]} ));
			myParser.addParameter('Gb1', zeros(dim.n_n, dim.n_d), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_n, dim.n_d]} ));
			myParser.addParameter('G_3', zeros(dim.n_w, dim.n_d), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_w, dim.n_d]} ));
			myParser.addParameter('G_4', zeros(dim.n_y, dim.n_d), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_y, dim.n_d]} ));
			myParser.addParameter('G_deterministic', zeros(dim.n_d, 0), ...
				@(A) validateattributes(A, {'numeric'}, {'nrows', dim.n_d} ));
			myParser.addParameter('G_bounded', zeros(dim.n_d, 0), ...
				@(A) validateattributes(A, {'numeric'}, {'nrows', dim.n_d} ));
			myParser.addParameter('G_stochastic', zeros(dim.n_d, 0), ...
				@(A) validateattributes(A, {'numeric'}, {'nrows', dim.n_d} ));
			myParser.addParameter('L_2', zeros(dim.n_w, dim.n), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_w, dim.n]}));
			myParser.addParameter('L_3', zeros(dim.n_w, dim.n), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_w, dim.n]}));
			% pde to ode couplings
			myParser.addParameter('L_1', ...
				quantity.Discrete.zeros( [dim.n_w, dim.n], spatialDomain ), ...
				@(A) validateattributes(A, {'quantity.Discrete'}, {'size', [dim.n_w, dim.n]}));
			
			% measurement
			myParser.addParameter('C_0', eye(dim.n_n), ...
				@(A) validateattributes(A, {'numeric'}, {'size', [dim.n_n, dim.n_n]}));
			
			varargin = misc.struct2namevaluepair(optArg);
			myParser.parse(varargin{:});

			% define the boundary matrices:
			K_0 = [dim.Jn - myParser.Results.Q_0 * dim.Jp; ...
				   zeros(dim.n_p, dim.n)];
			K_1 = [zeros(dim.n_n, dim.n); ...
				   dim.Jp - myParser.Results.Q_1 * dim.Jp];
			
			A = signals.PolynomialOperator({myParser.Results.Lambda_0, Lambda_1});
			   
			res = myParser.Results;
			assert( all(res.L_2 * dim.Jp.' == 0, 'all'), "Matrix L2 must satisfy L2 Jp^T == 0" ) 
			
			obj@faultdiagnosis.System( spatialDomain, A, K_0, K_1, "F", F, "H_1", res.H_1, ...
				"H_2", [-res.Hb0; -res.Hb1], ...
				"L_1", res.L_1, "L_2", res.L_2, "L_3", res.L_3, ...
				"C_2", res.C_0 * dim.Jn, ...
				"B_1", res.B_1, "B_2", [res.Bb0; res.Bb1], "B_3", res.B_3, "B_4", res.B_4, ...
				"G_1", res.G_1, "G_2", [res.Gb0; res.Gb1], "G_3", res.G_3, "G_4", res.G_4, ...
				"E_1", res.E_1, "E_2", [res.Eb0; res.Eb1], "E_3", res.E_3, "E_4", res.E_4, ...
				"G_deterministic", res.G_deterministic, ...
				"G_bounded", res.G_bounded, ...
				"G_stochastic", res.G_stochastic, ...
				"name", optArg.name)
				
			
			obj.Bb0 = res.Bb0;
			obj.Bb1 = res.Bb1;
			
			obj.Gb0 = res.Gb0;
			obj.Gb1 = res.Gb1;
			
			obj.Eb0 = res.Eb0;
			obj.Eb1 = res.Eb1;
			
			obj.Jn = dim.Jn;
			obj.Jp = dim.Jp;
			obj.n.p = dim.n_n;
			obj.n.n = dim.n_p;
			
			obj.Lambda_1 = Lambda_1;
			obj.Lambda_0 = res.Lambda_0;
			obj.A_0	= res.A_0;
			obj.D = res.D;
			obj.Q_0 = res.Q_0;
			obj.Q_1 = res.Q_1;
			obj.Hb0 = res.Hb0;
			obj.Hb1 = res.Hb1;
			obj.H_4 = res.H_4;
			
			obj.C_0 = res.C_0;
			
			obj.stepSize = optArg.stepSize;
							
			%% initialize the model.TransportOde:
			% The dps.wave.SystemOde represents systems of the form
			%
			% 	x_t(z,t) = Lambda_1(z) x_z(z,t) + Lambda_0(z) x(z,t) ...
			%			  + int_a^b F(z,zeta) x(zeta,t) dzeta ...
			%			  + input('f').B f(t) + ode2pde.B(z) w(t)
			% 	  w_t(t) = ode.A w(t) + eye(.) pde2ode.C0 x(0) ...
			% 	           + odeInput('f') f(t)
			% 	x_2(0,t) = Q0 x_1(0,t) + ode2pde.B0 w(t) ...
			%             + input('f').B0 f(t) 
			%   x_1(1,t) = Q1 x_2(1,t) + input('u').B1 u(t) ...
			%             + input('f').B1 f(t)
			%       y(t) = output.get('y').C0 x(0,t)
			%
			% In order to convert the system representation as pdes wrt. dz
			% into the form wrt. dt, the pde must be reordered:
			%
			%  dt x(z,t) = Lambda_1^-1(z)dz x(z,t) - Lambda_1^-1(z)Lambda_0(z)x(z,t)
			%			  - Lambda_1^-1(z) A_0(z) x_\p(0,t)
			%			  - int_0^z Lambda_1^-1(z)D(z,zeta)x(zeta,t)dz ...
			%             - Lambda_1^-1(z)H_1(z) w(t)
			%			  - Lambda_1^-1(z)E_1(z) f(t)
			%			  - Lambda_1^-1(z)G_1(z) d(t)
			%  x_\n(0,t) = Q_0 x_\p(0,t) + Hb0 w(t) + Eb0 f(t) + Gb0 d(t)
			%  x_\p(1,t) = Q_1 x_\n(1,t) + Eb1 f(t) + u(t) + Gb0 d(t)
			%	 dt w(t) = F w(t) + L_2 x(0,t) + E_3 f(t) + G_3 d(t)
			%		y(t) = J_w x(0,t) + E_4 f(t) + G_4 d(t)

			invLambda_1 = inv(Lambda_1);

			% +++++ inputs +++++
			inputs = model.Inputs();

			% ----- control inputs -----
			input_u = obj.generateInputs("plant.u", "B", obj, invLambda_1);
			inputs.add(input_u.pde);

			% ----- fault inputs -----
			input_f = obj.generateInputs("plant.f", "E", obj, invLambda_1);
			inputs.add(input_f.pde);

			% ----- deterministic disturbance inputs -----
			input_d_deterministic = obj.generateInputs("plant.d_deterministic", ...
				"G", obj, invLambda_1, obj.G_deterministic);
			inputs.add(input_d_deterministic.pde);

			% ----- bounded disturbance inputs -----				
			input_d_bounded = obj.generateInputs("plant.d_bounded", ...
				"G", obj, invLambda_1, obj.G_bounded);
			inputs.add(input_d_bounded.pde);		

			% ----- stochastic disturbance inputs -----			
			input_d_stochastic = obj.generateInputs("plant.d_stochastic", ...
				"G", obj, invLambda_1, obj.G_stochastic);
			inputs.add(input_d_stochastic.pde);

			% ----- mapping from pde to ode -----
			pde2ode = model.Output('pde2ode', 'C', obj.L_1, ...
				'C0', obj.L_2, 'C1', obj.L_3); % TODO add H4 output

			ode2pde = model.Input('ode2pde', 'B', -invLambda_1 * obj.H_1, ...
				'B0', obj.Hb0, 'B1', obj.Hb1);

			odeInput = misc.Gains();
			odeInput.add(input_u.ode);
			odeInput.add(input_f.ode);
			odeInput.add(input_d_bounded.ode);
			if ~isempty( obj.G_stochastic )
				odeInput.add(input_d_stochastic.ode);
			end
			odeInput.add(input_d_deterministic.ode);

			odeOutput = misc.Gain("plant.w", obj.H_4, 'outputType', 'plant.y');
			
			% ----- outputs of the system -----
			output = model.Output('plant.y', 'C0', obj.C_2, 'input', ...
				odeOutput + ...
				input_u.output + ...
				input_f.output + ...
				input_d_bounded.output + ...
				input_d_deterministic.output + ...
				input_d_stochastic.output);

			% ----- initialisation of the system -----
			obj.systemOde = model.TransportOde( invLambda_1, ...
				'A', - invLambda_1 * obj.Lambda_0, ...
				'A0', - invLambda_1 * obj.A_0, ...
				'F', - invLambda_1 * obj.D, ...
				'FBounds', {0, 'z'}, ...
				'input', inputs, ...
				'odeInput', odeInput, ...
				'ode2pde', ode2pde, ...
				'pde2ode', pde2ode, ...
				'ode', ss( obj.F, eye(obj.n.w), eye(obj.n.w), []), ...
				'Q0', obj.Q_0, ...
				'Q1', obj.Q_1, ...
				'output', output);

			obj.discreteModel = faultdiagnosis.hyperbolic.heteroDirectional.DiscreteModel(...
				obj, obj.systemOde, obj.stepSize);
				
		end
		function sys = model.TransportOde(obj)
 			% test
 			sys = obj.systemOde;
		end	
		function L = get.Int_zeta_z_Lambda_1_dz(obj)
			if isempty(obj.Int_zeta_z_Lambda_1_dz)
				obj.Int_zeta_z_Lambda_1_dz = int( obj.Lambda_1 * ones(obj.n.x, 1), "z", "zeta", "z" );
			end
			L = obj.Int_zeta_z_Lambda_1_dz;
		end
		
		function theta_min = get.theta_min(obj)
			theta_min = min( obj.Int_zeta_z_Lambda_1_dz.at( [1, 0]) );
		end
		
		function theta_max = get.theta_max(obj)
			theta_max = max( obj.Int_zeta_z_Lambda_1_dz.at( [1, 0]) );
		end		
		
	end
 	methods (Access = public)	
		
		function [T0, tau_plus, tau_minus, transportationTimes] = transportationTimes(obj)
			
			transportationTimes = zeros(obj.n.x, 1);
			for k = 1:obj.n.x
				transportationTimes(k) = int( obj.Lambda_1(k,k) );
			end
			
			transportationTimes_ = minmax( transportationTimes.' );
			
			tau_plus = transportationTimes_(2);
			tau_minus = transportationTimes_(1);
			
			T0 = tau_plus - tau_minus;
			
		end
		
		function [arg, var, fun, prefix, suffix] = generatePrintNames(obj)
			
			z = obj.spatialDomain.name;
			t = "t";
			O = "0";
			zeta = "\zeta";
			
			arg.z = "(" + z + ")";
			arg.t = "(" + t + ")";
			arg.zt = "(" + z + ", " + t + ")";
			arg.ot = "(" + 0 + ", " + t + ")";
			arg.lt = "(" + 1 + ", " + t + ")";
			arg.zzeta = "(" + z + ", " + zeta + ")";
			arg.zetat = "(" + zeta + ", " + t + ")";
			arg.zetaz = "(" + zeta + ", " + z + ")";
				
			
			% PDE parameters
			var.name.Lambda = "\Lambda";
			var.name.A = "A";
			var.name.A0 = "A_0";
			var.name.D = "D";
			var.name.H_1 = "H_1"; 
			var.name.E_1 = "E_1";
			var.name.G_1 = "G_1";
			var.name.B_1 = "B_1";
			
			% BC0 
			
			
			var.arg.Lambda = arg.z;
			var.arg.A = arg.z;
			var.arg.A0 = arg.z;
			var.arg.D = arg.zt;
			var.arg.H_1 = arg.z;
			var.arg.E_1 = arg.z;
			var.arg.G_1 = arg.z;
			var.arg.B_1 = arg.z;
						
			% function names
			var.name.x = "x";
			var.name.x0 = "x_{-}";
			var.name.w = "w";
			var.name.f = "f";
			var.name.d = "d";
			var.name.u = "u";
			
			var.name.xn = var.name.x + "^{+}";
			var.name.xp = var.name.x + "^{-}";
			
			var.arg.x = arg.zt;
			var.arg.x0 = arg.ot;
			var.arg.w = arg.t;
			var.arg.f = arg.t;
			var.arg.d = arg.t;
			var.arg.u = arg.t;
			
			% function mapping
			fun.name.A = var.name.x;
			fun.name.A_0 = var.name.x0;
			fun.name.D = var.name.x;
			fun.name.H_1 = var.name.w;
			fun.name.E_1 = var.name.f;
			fun.name.G_1 = var.name.d;
			fun.name.B_1 = var.name.u;
			
			
			fun.arg.A = arg.zt;
			fun.arg.A_0 = arg.ot;
			fun.arg.D = arg.zetat;
			fun.arg.H_1 = arg.t;
			fun.arg.E_1 = arg.t;
			fun.arg.G_1 = arg.t;
			fun.arg.B_1 = arg.t;
			
			prefix.D = "\int_0^z";
			suffix.D = "\mathrm{d}" + zeta;
						
			%% bc 0
			fun.name.K_0 = var.name.xp;
			fun.name.Hb0 = var.name.w;
			fun.name.Eb0 = var.name.f;
			fun.name.Gb0 = var.name.d;
			fun.name.Bb0 = var.name.u;
			
			fun.arg.K_0 = arg.ot;
			fun.arg.Hb0 = arg.t;
			fun.arg.Eb0 = arg.t;
			fun.arg.Gb0 = arg.t;
			fun.arg.Bb0 = arg.t;
			
			%% bc 1
			fun.name.K_1 = var.name.xn;
			fun.name.Hb1 = var.name.w;
			fun.name.Eb1 = var.name.f;
			fun.name.Gb1 = var.name.d;
			fun.name.Bb1 = var.name.u;
			
			fun.arg.K_1 = arg.lt;
			fun.arg.Hb1 = arg.t;
			fun.arg.Eb1 = arg.t;
			fun.arg.Gb1 = arg.t;
			fun.arg.Bb1 = arg.t;
			
			%% ode
			fun.name.F = var.name.w;
			fun.arg.F = arg.t;
			fun.name.L_1 = var.name.x;
			fun.arg.L_1 = arg.zt;
			fun.name.L_2 = var.name.xp;
			fun.arg.L_2 = arg.ot;
			fun.name.L_3 = var.name.xn;
			fun.arg.L_3 = arg.lt;
			
			fun.name.E_3 = var.name.f;
			fun.name.G_3 = var.name.d;
			fun.name.B_3 = var.name.u;
			
			fun.arg.E_3 = arg.t;
			fun.arg.G_3 = arg.t;
			fun.arg.B_3 = arg.t;
						
			prefix.L_1 = "\int_0^1";
			suffix.L_1 = "\mathrm{d}" + arg.z;
			
			
			%% output
			var.name.y = "y";
			fun.name.E_4 = var.name.f;
			fun.name.G_4 = var.name.d;
			
			fun.arg.E_4 = arg.t;
			fun.arg.G_4 = arg.t;
			
			
		end
		
		function [] = print(obj, arg, var, fun)
			
			
			if nargin == 1
				[arg, var, fun, prefix, suffix] = obj.generatePrintNames();
			end
			
			% print PDE
			pdeString = var.name.x + "^{\prime}" + arg.zt ...
				+ " &= " + misc.latexChar(obj.Lambda_1) ...
				+ " \dot{" + var.name.x + "}" + arg.zt + " + "...
				+ obj.printRhs( ["A", "A_0", "D", "H_1", "E_1", "G_1", "B_1"], ...
				fun, prefix, suffix);
			
			% boundary condition:
			bc0 = var.name.xn + arg.ot + "&= " ...
				+ obj.printRhs( ["K_0", "Hb0", "Bb0", "Eb0", "Gb0"], ...
				fun, prefix, suffix);
			
			bc1 = var.name.xp + arg.lt + " &= " ...
				+ obj.printRhs( ["K_1", "Hb1", "Bb1", "Eb1", "Gb1"], ...
				fun, prefix, suffix);
			
			% print the ode
			ode = "\dot{" + var.name.w + "}" + arg.t + " &= "...
				+ obj.printRhs( ["F", "L_1", "L_2", "L_3", "E_3", "G_3", "B_3"], ...
				fun, prefix, suffix);
			
			% print output
			output = var.name.y + arg.t + "&=" + var.name.xp + arg.ot + "+" ...;
				+ obj.printRhs(["E_4", "G_4"], fun, prefix, suffix, 'lineBreak', false);
			
			mySystem = [pdeString; ...
				        bc0; ...
						bc1; ...
						ode; ...
						output];
			tmp = mySystem;
            %% Postprocessing
            mySystem = strrep(mySystem, '\mathrm{heaviside}', '\theta');
            mySystem = strrep(mySystem, '.0', '');
                    
			if nargout > 0
				texString = mySystem;
			else
				misc.printTex(mySystem);
            end
            
		end
		
		function rhs = printRhs(obj, parameterNames, fun, prefix, suffix, argOpt)
            arguments
               obj
               parameterNames
               fun
               prefix
               suffix
               argOpt.lineBreak = true;
            end
			
			rhs = "";
			
			for myPar = parameterNames
				
				if isa( obj.(myPar), 'double')
					if max( abs( obj.(myPar) ), [], 'all') == 0
						continue
					end
				elseif isa( obj.(myPar), 'quantity.Discrete')
					if MAX(abs(obj.(myPar))) == 0
						continue
                    end				
				else
					error("Unknown type of the parameter" + myPar);
				end
				
				if isfield(prefix, myPar)
					rhs = rhs + prefix.(myPar);
				end
				
				rhs = rhs + misc.latexChar(obj.(myPar)) ...
					+ fun.name.(myPar) + fun.arg.(myPar);
				
				if isfield(suffix, myPar)
					rhs = rhs + suffix.(myPar);
				end
				
				rhs = sprintf("%s\n%s", rhs, " + ");
            end
            
			rhs = sprintf("%s\b\b\b", rhs);
            if ~argOpt.lineBreak
                rhs = sprintf("%s\b", rhs);
            end
		end
			
		function [faultDiagnosisKernel, backsteppingKernel] = ...
				FaultDiagnosisKernel(obj, faultModel, optArgs)
			% The kernel is given in the form
			%
			%	m_z = -Lambda_1^T m_t - A^T m 
			%			- \int_z^1 D^T(zeta, z) m_i(zeta) d_zeta
			%	m_p(0) = -Q_0^T m_n(0) - L_2^T p + n
			%	m_n(1) = -Q_1^T m_p(1) 
			%	   p_t = F^T p Hb0^T m_n(0) + int_0^1 H_1^T m dz
			%
			% Fault model ODE:
			%	   q_t = S^T q + Q_T^T int_0^1 E_1^T m dt 
			%			+ Q_T^T Eb0^T m_2(0)
			%	        - Q_T^T Eb1^T m_1(1) + Q_T^T E_3^T p
			%
			% so the transportation direction is inverted. Thus, an
			% inversion of the spatial coordinate
			%	z_ = 1 - z
			% must be applied. Then, the kernel has the form
			%
			%    m_z = Lambda_1^T m_t - A^T m
			%			- \int_z^1 D^T(zeta, z) m_i(zeta) d_zeta
			% m_n(0) = -Q_1^T m_p(0)
			% m_p(1) = -Q_0^T m_n(1) - L_2^T p + n
			%    p_t = F^T p + Hb0^T m_n(1) + int_0^1 H_1^T m dz
			%
			%    q_t = S^T q + Q_T^T ( int_0^1 E_1^T m dt 
			%	      + Eb0^T m_n(1)
			%	      - Eb1^T m_p(0) + E_3^T p )
			%
			%	this must be reformulated into a system wrt to dt
			%	...
			%
			
			arguments
				obj;
				faultModel signals.SignalModel;
				optArgs.disturbanceModel signals.SignalModel;
				optArgs.tolerance (1,1) double = 1e-3;
				optArgs.referenceGrid (:,1) double = obj.Lambda_1(1).domain.grid;
				optArgs.numberOfLines (1,1) double;
				optArgs.backsteppingKernel;
			end
			
			%% assert that the system is in the correct form
			% todo 
			% assert(A_0 == 0)
			% assert(L_1 == 0)
			% assert(L_2 == 0)
			% assert( Hb1 == 0 )

			%% backstepping transformation
			% compute the backstepping transformation kernels, in order
			% to remove the integral and reaction term in the PDE
			if isfield(optArgs, "backsteppingKernel")
				backsteppingKernel = obj.getBacksteppingKernel( "tolerance", optArgs.tolerance, ...
					"referenceGrid", optArgs.referenceGrid, "numberOfLines", optArgs.numberOfLines, ...
					"backsteppingKernel", optArgs.backsteppingKernel);
			else
				backsteppingKernel = obj.getBacksteppingKernel( "tolerance", optArgs.tolerance, ...
					"referenceGrid", optArgs.referenceGrid, "numberOfLines", optArgs.numberOfLines);
			end
			
			assert( size( faultModel.ss.C, 1 ) == obj.n.f, ...
				'Number of faults does not fit to the signal model')
			assert( size( optArgs.disturbanceModel.ss.C, 1) == obj.n.d_deterministic, ...
				'Number of disturbances does not fit to the disturbance model');
			
			% pre-computation of parameters for the fault diagnosis kernel
			% in backstepping coordinates
			Lambda_1_ = obj.Lambda_1.flipDomain("z");
			
			%% common ODE system
			[eta] = faultDiagnosisKernelOde(obj, faultModel, ...
												optArgs.disturbanceModel);

				  
			%% ODE-system in backstepping coordinates
			B_eta1_ = eta.B_1.flipDomain('z');
			A_0_ = obj.A_0.flipDomain('z');
			K_I = backsteppingKernel.getValueInverse();
			
			backsteppingCoordinates.A_eta = ...
				eta.A + eta.B_3 * obj.Jn * obj.L_2.' * eta.J_w;
			backsteppingCoordinates.B_eta1 = ...
				 B_eta1_ + eta.B_3 * A_0_.' + ( eta.Bb0 * obj.Jp ...
				 + eta.B_3 * ( obj.Jn + obj.Q_0.' * obj.Jp) ) * subs(subs(K_I, "z", 1), "zeta", "z") ...
				 + int( ( B_eta1_.subs("z", "zeta") + eta.B_3 * subs( A_0_.', "z", "zeta")) * ...
					 subs(K_I, ["z", "zeta"], ["zeta", "z"] ), "zeta", "z", 1);			
				 
			backsteppingCoordinates.B_eta2 = eta.Bb1 * obj.Jn;
			backsteppingCoordinates.B_eta3 = eta.Bb0 * obj.Jp + eta.B_3 * (obj.Jn + obj.Q_0.' * obj.Jp);
			
			faultDiagnosisKernel = ...
				faultdiagnosis.hyperbolic.heteroDirectional.Kernel(...
				Lambda_1_, ...
				backsteppingCoordinates.A_eta, ...
				backsteppingKernel, ...
				'A_0', - Lambda_1_ * backsteppingKernel.getA0Target, ...
				'Q_0', - obj.Q_1.', ...
				'L_1', backsteppingCoordinates.B_eta1, ...
				'L_2', backsteppingCoordinates.B_eta2, ...
				'L_3', backsteppingCoordinates.B_eta3, ...
				'eta_0', eta.O, ...
				'sys', obj, ...
				'J_ode_p', eta.J_w, ... % TODO rename: J_ode_p -> J_w
				'J_ode_q_f', eta.J_q_f, ...
				'J_ode_q_d', eta.J_q_d, ....
				'faultModel', faultModel, ...
				'disturbanceModel', optArgs.disturbanceModel);
		end
		
		function [ode] = faultDiagnosisKernelOde(obj, faultModelOde, disturbanceModelOde)
			arguments
				obj
				faultModelOde ss
				disturbanceModelOde ss
			end
			
			n_v_f = size( faultModelOde.A, 1);
			n_v_d = size( disturbanceModelOde.A, 1);
			R_f = faultModelOde.C;			
			R_d = disturbanceModelOde.C;
	
				
			 A = [ obj.F.', zeros(obj.n.w, n_v_f), zeros(obj.n.w, n_v_d); ...
					   R_f.' * obj.E_3.', faultModelOde.A.', zeros(n_v_f, n_v_d); ...
					   R_d.' * obj.G_deterministic.' * obj.G_3.', zeros(n_v_d, n_v_f), ...
					   disturbanceModelOde.A.'];
			B_1 = [obj.H_1.'; ...
					  R_f.' * obj.E_1.'; ...
					  R_d.' * obj.G_deterministic.' * obj.G_1.'];
				  
			Bb0 = [ obj.Hb0.'; ...
						R_f.' * obj.Eb0.'; ...
						R_d.' * obj.G_deterministic.' * obj.Gb0.'];
			Bb1 = [ - obj.Hb1.'; ...
					  - R_f.' * obj.Eb1.'; ...
					  - R_d.' * obj.G_deterministic.' * obj.Gb1.'];
			B_3 = [ zeros(obj.n.w, obj.n.y); ...
					  - R_f.' * obj.E_4.'; ...
					  - R_d.' * obj.G_deterministic.' * obj.G_4.'];
			
			J_w = [	eye( obj.n.w ) ...
					zeros(obj.n.w, n_v_f) ...
					zeros(obj.n.w, n_v_d)];
				
			J_q_f = [	zeros( n_v_f, obj.n.w ), ...
						eye( n_v_f), ...
						zeros( n_v_f, n_v_d) ];
					
			J_q_d = [	zeros( n_v_d, obj.n.w ), ...
						zeros( n_v_d, n_v_f), ...
						eye( n_v_d) ];
				
			O = [	zeros( obj.n.w, obj.n.f); ...	      
					R_f.'; ...
					zeros( n_v_d, obj.n.f)];
			ode = misc.ws2struct();
		end		
		
		function faultDiagnosisKernel = ...
				faultDiagnosisKernelOriginalCoordinates(obj, faultModel, optArgs)
			arguments
				obj
				faultModel
				optArgs.disturbanceModel ss = ss();
			end
			
			eta = faultDiagnosisKernelOde(obj, faultModel, ...
							optArgs.disturbanceModel);				
			
			faultDiagnosisKernel = ...
				faultdiagnosis.hyperbolic.heteroDirectional.System(...
				obj.Lambda_1.flipDomain('z'), ...
				eta.A, ...
				'A', obj.Lambda_0.flipDomain('z').', ...
				'D', subs(obj.D.flipDomain(["z", "zeta"]), ["z", "zeta"], ["zeta", "z"]).', ...
				'Q_0', - obj.Q_1.', ...
				'Q_1', - obj.Q_0.', ...
				'Hb1', - obj.L_2.' * eta.J_w, ...
				'L_1', eta.B_1, ...
				'L_2', eta.Bb0, ...
				'L_3', eta.Bb1, ...
				'B_ode', eta.B_3);
					
		end
			
		function [delta_x_l2norm, delta_w_l2norm, sim] = verifySolution(obj, X, W, U, optArgs)
			
			arguments
				obj, 
				X, 
				W,
				U,
				optArgs.AbsTol = 1e-12;
				optArgs.w0 = zeros(size(W, 1));
			end
			
			delta_x_l2norm = zeros(1, size(X, 2) );
			delta_w_l2norm = zeros(1, size(X, 2) );
			
			fprintf("Compare the planned solution and the simulated solution:\n");
			timeDomain = U(1).domain;
			for i = 1:size(X, 2)
				sim(i) = obj.simulate(timeDomain, 'u', U(:, i), 'w0', optArgs.w0);
				
				delta_x = ( sim(i).plant.x - X(:,i));
				delta_x_l2norm(i) = delta_x.l2norm;
				
				delta_w = sim(i).plant.w - W(:,i);
				delta_w_l2norm(i) = delta_w.l2norm;
				
				misc.warning(delta_x_l2norm(i) < optArgs.AbsTol, ...
					sprintf("The L_2 norm of delta #%i for x is %d\n", i, delta_x_l2norm(i)));
				
				misc.warning(delta_w_l2norm(i) < optArgs.AbsTol, ...
					sprintf("The L_2 norm of delta #%i for w is %d\n", i, delta_w_l2norm(i)));
				
			end
			
		end
		
		function verifyInitialEndCondition(obj, simData, T, x0, xT, w0, wT, optArgs)
			arguments
				obj
				simData
				T
				x0
				xT
				w0
				wT
				optArgs.AbsTol = 1e-12
			end
			
			
			fprintf("Compare initial and end conditions:\n");

			for i = 1:numel(simData)
				
				% verify x0
				[isNear, delta.x0] = near( subs( simData(i).plant.x, "t", 0), x0(:,i), optArgs.AbsTol );
				if ~isNear
					warning("initial condition of x has a deviation of %f.\n", delta.x0);
				end
				
				% verify xT
				[isNear, delta.xT] = near( subs( simData(i).plant.x, "t", T), xT(:,i), optArgs.AbsTol );
				if ~isNear
					warning("end condition of x has a deviation of %f.\n", delta.xT);
				end
				
				% verify w0
				[isNear, delta.w0] = numeric.near( subs( simData.plant.w, "t", 0), w0(:,i), optArgs.AbsTol );
				if ~isNear
					warning("initial condition of w has a deviation of %f.\n", delta.w0);
				end
				
				% verify wT
				[isNear, delta.wT] = numeric.near( subs( simData.plant.w, "t", T), wT(:,i), optArgs.AbsTol );
				if ~isNear
					warning("end condition of w has a deviation of %f.\n", delta.wT);
				end
			end
		end
		
		function backsteppingKernel = getBacksteppingKernel(obj, optArgs)
			arguments 
				obj
				optArgs.tolerance (1,1) double = 1e-4;
				optArgs.referenceGrid (:,1) double = obj.Lambda_1(1).domain.grid;
				optArgs.numberOfLines (1,1) double = 11;
				optArgs.backsteppingKernel;
			end
			
			args = namedargs2cell( optArgs );
			[hash, kernelArgIn] = obj.getBacksteppingKernelHash( args{:} );
			
			if isfield( optArgs, "backsteppingKernel") && ...
					all(hash == optArgs.backsteppingKernel.hash)
				backsteppingKernel = optArgs.backsteppingKernel;
			else
				backsteppingKernel = kernel.Feedback.tryToLoadKernel(kernelArgIn{:});	
			end
		end
		
		function [hash, kernelArgIn] = getBacksteppingKernelHash(obj, optArgs)
			arguments 
				obj
				optArgs.tolerance (1,1) double = 1e-4;
				optArgs.referenceGrid (:,1) double = obj.Lambda_1(1).domain.grid;
				optArgs.numberOfLines (1,1) double = 11;
				optArgs.backsteppingKernel;
			end
			
			% Lambda_1( 1- z )^-1^T
			invL_m1T = inv( obj.Lambda_1.flipDomain("z").' );

			kernelArgIn = { invL_m1T, ...
				'A', - invL_m1T * obj.Lambda_0.flipDomain("z").', ...
				'F', - invL_m1T * subs(obj.D.flipDomain(["z", "zeta"]).', ...
						["z", "zeta"], ["zeta", "z"]), ...
				'Q0', - obj.Q_1.', ...
				'name', 'K', ...
				'iterationMargin', [3 30], ...
				"tolerance", optArgs.tolerance, ...
				"referenceGrid", optArgs.referenceGrid, ...
				"numberOfLines", optArgs.numberOfLines };			
			
			if isfield( optArgs, "backsteppingKernel")
				hash = optArgs.backsteppingKernel.hash;
			else			
				% compare the specified kernel with the parameters:
				dummyKernel = kernel.Feedback(kernelArgIn{:}, 'dummy', true);
				hash = dummyKernel.hash();
			end
		end
		
	end
	
	methods ( Static, Access = protected )
		function inputs = generateInputs(inputName, matrixName, obj, invLambda_1, S)
			
			if nargin == 4
				n_input = size( obj.(matrixName + "_1"), 2);
				S = eye(n_input);
			end
			
			inputs.pde = model.Input(inputName, ...
				'B', - invLambda_1 * obj.(matrixName + "_1") * S, ....
				'B0', obj.(matrixName + "b0") * S, ...
				'B1', obj.(matrixName + "b1") * S) ;
			inputs.ode = misc.Gain(inputName, ...
				obj.(matrixName + "_3") * S, ...
				'outputType', 'plant.odeInput');
			inputs.output =  misc.Gain(inputName, obj.(matrixName + "_4") * S, ...
				'outputType', 'plant.y');
		end
	end
	
	
	methods ( Static )
		
		function [Jn, Jp, n_n, n_p] = stateSelectionMatrices( Lambda_1 )
			% #todo
			n_n = sum( Lambda_1.at(0) < 0, 'all' );
			n_p = sum( Lambda_1.at(0) > 0, 'all' );
			
			Jn = [eye(n_p), zeros(n_p, n_n)];
			Jp = [zeros(n_n, n_p), eye(n_n) ];
		end % stateSelectionMatrices
		
	end % Static
	
	
	
end

