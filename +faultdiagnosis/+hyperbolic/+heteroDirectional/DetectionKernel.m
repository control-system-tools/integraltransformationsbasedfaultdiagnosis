classdef DetectionKernel < faultdiagnosis.hyperbolic.heteroDirectional.System & faultdiagnosis.Kernel
	% System for the fault diagnosis kernel
	%   dz m(z,t) = Lambda_1(z) dt m(z,t) + A_0(z) m_p(0,t)
	%   m_\n(0,t) = Q_0 m_\p(0,t)
	%   m_\p(1,t) = n(t)
	%   dt eta(t) = F eta(t) + int_0^1 L_1(z) m(z,t) dz + L_2(z) m_p(0,t)
	%              + L_3 m_n(1,t)
	%
	% This is the representation of the fault-diagnosis kernel with
	% inverted spatial coordinate.
	properties
		
		backsteppingKernel kernel.Feedback;
		
		% Selection matrix for the system-ODE
		%	dtau p(tau) = F^T p(tau) + int_0^1 H_1^T(z) m(z,tau) dz + H_2^T m_n(0,tau)
		%
		%	p(tau) = J_ode_p * eta(tau)
		J_ode_p double;
		
		% Selection matrix for the disturbance-model-ODE
		J_ode_q_d double;
	end
	
	methods
		function obj = DetectionKernel(sys, optArgs)
			% System for the fault diagnosis
			%  dz m(z,t) = Lambda_1(z) dt m(z,t) + A_0(z) m_p(z,t)
			%  m_\n(0,t) = Q_0 m_\p(0,t)
			%  m_\p(1,t) = n(t)
			%	 dt w(t) = S w(t) + int_0^1 L_1(z) m(z,t) dz ...
			%              + L_2 x_p(0,t) + L_3 x_n(1,t)
			
			arguments
				sys faultdiagnosis.hyperbolic.heteroDirectional.System;
				optArgs.disturbanceModel signals.SignalModel;
				optArgs.tolerance (1,1) double = 1e-4;
				optArgs.referenceGrid (:,1) double = sys.Lambda_1(1).domain.grid;
				optArgs.numberOfLines (1,1) double = 11; %TODO check this value!
				optArgs.backsteppingKernel;
				optArgs.eta
			end
			
			%% backstepping transformation
			% compute the backstepping transformation kernels, in order
			% to remove the integral and reaction term in the PDE
			if isfield(optArgs, "backsteppingKernel")
				backsteppingKernel = sys.getBacksteppingKernel( "tolerance", optArgs.tolerance, ...
					"referenceGrid", optArgs.referenceGrid, "numberOfLines", optArgs.numberOfLines, ...
					"backsteppingKernel", optArgs.backsteppingKernel);
			else
				backsteppingKernel = sys.getBacksteppingKernel( "tolerance", optArgs.tolerance, ...
					"referenceGrid", optArgs.referenceGrid, "numberOfLines", optArgs.numberOfLines);
			end
			
			if isfield(optArgs, "disturbanceModel")
				assert( size( optArgs.disturbanceModel.ss.C, 1) == sys.n.d_deterministic, ...
					'Number of disturbances does not fit to the disturbance model');
			else
				error("Not yet implemented: The fault detection case without a disturbance " + ...
					"that is described by a signal model is not yet implemented. Please feel " + ...
					"free to extend the implementation. If there are question please contact " + ...
					"me, e.g., by mail: ferdinand.fischer@uni-ulm.de.")
			end
			
			% pre-computation of parameters for the fault diagnosis kernel
			% in backstepping coordinates
			Lambda_1_ = sys.Lambda_1.flipDomain("z");
			
			%% common ODE system
			if isfield(optArgs, "eta")
				eta = optArgs.eta;
			elseif isfield(optArgs, "disturbanceModel")
				eta = faultdiagnosis.hyperbolic.heteroDirectional.DetectionKernel.commonOde( sys, ...
					"disturbanceModelOde", optArgs.disturbanceModel);
			else
				eta = faultdiagnosis.hyperbolic.heteroDirectional.DetectionKernel.commonOde( sys );
			end
			%% ODE-system in backstepping coordinates
			B_eta1_ = eta.B_1.flipDomain('z');
			A_0_ = sys.A_0.flipDomain('z');
			K_I = backsteppingKernel.getValueInverse();
			% K_I(zeta,z):
			KIzetaZ = backsteppingKernel.getValueInverse().subs(["z", "zeta"], ["zeta", "z"]);
			iC0T = inv( sys.C_0.' );
			
			% \bar{B}_3 -> Bb0
			% \bar{B}_2 -> Bb1
			% \bar{B}_4 -> eta.B_3
			
			backsteppingCoordinates.A_eta = ...
				eta.A + eta.B_3 / sys.C_0.' * sys.Jn * sys.L_2.' * eta.J_w;
		
			backsteppingCoordinates.B_eta1 = B_eta1_ ...
				+ int( B_eta1_ * KIzetaZ, "zeta", "z", 1) ...
				+ eta.Bb0 * sys.Jp * KIzetaZ.subs("zeta", 1) ...
				+ eta.B_3 / sys.C_0.' * ( ...
					( A_0_.' + sys.Jn + sys.Q_0.' * sys.Jp ) * KIzetaZ.subs("zeta", 1) ...
					+ int( A_0_.subs("z", "zeta").' * KIzetaZ ) );

			backsteppingCoordinates.B_eta2 = eta.Bb1 * sys.Jn;
			backsteppingCoordinates.B_eta3 = eta.Bb0 * sys.Jp + eta.B_3 / sys.C_0.' ...
				* (sys.Jn + sys.Q_0.' * sys.Jp);
			
			%%
			obj@faultdiagnosis.hyperbolic.heteroDirectional.System( ...
				Lambda_1_, backsteppingCoordinates.A_eta, ...
				'A_0', - Lambda_1_ * backsteppingKernel.getA0Target, ...
				'Q_0', - sys.Q_1.', ...
				'Bb1', eye( sys.n.p ), ...
				'L_1', backsteppingCoordinates.B_eta1, ...
				'L_2', backsteppingCoordinates.B_eta2, ...
				'L_3', backsteppingCoordinates.B_eta3, ...
				'name', "detectionKernel")
			
			obj.backsteppingKernel = backsteppingKernel;
			obj.sys = sys;
			obj.n.f = sys.n.f;
			obj.J_ode_p = eta.J_w;
			obj.J_ode_q_d = eta.J_q_d;
			obj.disturbanceModel = optArgs.disturbanceModel;
		end
	end % methods
	
	methods ( Access = public )
		
		function [differentialExpression] = computeDifferentialParametrization(obj)
			% premilinary initializations:
			sImF = signals.PolynomialOperator( {-obj.F, eye(obj.n.w)} );
			adjF = adj(sImF);
			detF = det(sImF);
			
			commonRoot = nan;
			
			while ~isempty(commonRoot)
			
				rootsAdj = adjF.findCommonZeros();
				rootsDet = detF.findCommonZeros();

				commonRoot = numeric.intersect( rootsAdj, rootsDet );

				if ~isempty( commonRoot )
					warning("Remove root %g \n", commonRoot)
					adjF = signals.PolynomialOperator( expand( adjF.sym ./ (obj.s - commonRoot) ) );
					detF = signals.PolynomialOperator( expand( detF.sym ./ (obj.s - commonRoot) ) );
				end
			end
			
			alpha = shiftdim( double( detF ) ).';
			S = double( adjF );
			
			
			[Xf1, Xf2, Xf3] = obj.ioKernelDifferentialExpression("E", alpha, S, -1);
			[XdB1, XdB2, XdB3] = obj.ioKernelDifferentialExpression("G", alpha, S, obj.sys.G_bounded.');
			
			differentialExpression.Xf1 = Xf1;
			differentialExpression.Xf2 = Xf2;
			differentialExpression.Xf3 = Xf3;
			
			differentialExpression.XdB1 = XdB1;
			differentialExpression.XdB2 = XdB2;
			differentialExpression.XdB3 = XdB3;
			
			differentialExpression.S = S;
			differentialExpression.alpha = alpha;
		end
		
		function [Xmag, isDetectable] = verifyDetectability(obj, optArgs)
			arguments 
				obj
				optArgs.plot (1,1) logical = false;
			end
			%%
			V = (obj.sys.Jn.' - obj.sys.Jp.' * obj.sys.Q_1.');
			P0 = subs(obj.A_0, "z", "zeta");
			omega = quantity.Domain("omega", [logspace(-2, log10(0.9)), logspace(0, 2, 5e2)]); %logspace(-12, 12, 2e2)
			s = quantity.Discrete( 1i * omega.grid, omega );
			
			for i = 1:obj.sys.n.x
				tauBar(i) = int( obj.sys.Lambda_1(i,i).flipDomain('z'), "z", "zeta", "z");
				Psi_s(i,:) = V(i,:) * exp( s * tauBar(i).subs("zeta", 0) ) ...
					+ int( P0(i,:) * exp( s * tauBar(i)), "zeta", 0, "z");
			end
			
			de = obj.differentialExpressions;
			X = zeros( obj.sys.n.f, obj.sys.n.y);
			for i = 1:obj.n.w+1
				X = X + ( int( de.Xf1(:,:,i) * Psi_s, "z", 0, 1) + de.Xf2(:,:,i) + de.Xf3(:,:,i) * Psi_s.subs("z", 1) ) * s^(i-1);
			end
			
			for i = 1:obj.sys.n.f
				isDetectable(i) = any( max( abs( X(i,:) ) ) > eps );
			end
			
			for i = 1:obj.sys.n.f
				for j = 1:obj.sys.n.y
					Xmag(i,j) = quantity.Discrete( mag2db( abs( X(i,j).on() ) ), omega);			
				end
			end
			Xmag.setName("|X(j\omega)|_{dB}")
			
			if nargout == 0 || optArgs.plot
				
				Xmag.plot("log", "x", "grid", true);

				idx = 1:obj.n.f;
				
				for i = idx( ~isDetectable )
					warning("the %ith fault is not detectable!", i)
				end
				
			end
			
		end
		
		function [ioKernel, measures, kernel, PsiThetaMuZ] = computeSolution3(obj, tau, optArg)
			% COMPUTESOLUTION3
			arguments
				obj,
				tau (1,1) quantity.EquidistantDomain;
				optArg.desiredGains (:,1) = ones(obj.n.f,1);
				optArg.delta = ones( obj.sys.n.d_bounded, 1);
				optArg.repeatedOptimization = true;
				optArg.SfDesired (1,1) double = 1;
				optArg.f;
				optArg.performanceIndex = "II";
				optArg.dof (1,1) double {mustBeInteger} = int32(1);
				optArg.ansatz = "taylorPolynomial";
				optArg.normedFaultFunction = 1;
				optArg.C0regularity = 0;
			end
			% --- define the time domains --- %
			% the time-domain for the transition is separated into three
			% parts. These parts are obtained from the initial and end
			% value for the kernel m(z,tau). Thus, the parts are defined as
			%	I_1 = [-t_Delta_minus, t_Delta_plus]
			%	I_2 = (t_Delta_plus, T - t_Delta_minus)
			%	I_3 = [T - t_Delta_minus, t_Delta_plus]
			% The trajectory planning is preformed on the extended domain:
			%	I = (tau^- : T + tau^+);
			% This domain is generated from the original tau domain, so that the step size is kept
			% close to the original one, i.e., the step-size of tau and I should be near. 
			% Compute the length of the extended time-domain: 
			Textended = tau.upper + obj.theta_max - obj.theta_min;
			% Compute the ratio of both lengths:
			ratioT = Textended / tau.upper;
			% define the new domain, with the step number raised by the ration of both domains. By
			% this, the step-size should change only minimal, but the upper and lower bound of the
			% domain hold:
			I = quantity.Domain("tau", linspace( obj.theta_min, tau.upper + obj.theta_max, tau.n*ratioT));
			% then, split the domain into the three parts:
			I = I.split( [ obj.theta_max, tau.upper + obj.theta_min ], "addPoints", true );

			% --- setup the ansatz functions --- %
			% The system of ansatz functions must be linear independent. The most simple ones are
			% taylor polynomials, however these have sometimes bad numerical behaviour. Thus,
			% sinusoidal ansatz functions are proposed. Alternatively, orthogonal polynomials as,
			% e.g., chebycheff polynomials, should also be fine.
			% With the ansatz, the reference trajectory \phi is parametrized, i.e.,:
			%	phi = ansatz * eta
			% where eta are the unknown coefficients to be determined.
			if optArg.ansatz == "taylorPolynomial"
				ansatz = signals.Monomial.taylorPolynomialBasis( I(2), ...
					"order", 2 * obj.n.w + optArg.dof - 1 + 2 * optArg.C0regularity, ...
					"numDiff", obj.n.w + optArg.C0regularity);
			elseif optArg.ansatz == "sinusoidal"
				ansatz = signals.Sinusoidal.fourierBasis( I(2), ...
					"order", 2 * obj.n.w + optArg.dof - 1 + 2 * optArg.C0regularity, ...
					"numDiff", obj.n.w + optArg.C0regularity );
			elseif optArg.ansatz == "polybump"
				bump = signals.PolyBump(I(2), "a", obj.n.w + optArg.C0regularity, ...
					"b", obj.n.w + optArg.C0regularity, "norm", true, ...
					"numDiff", obj.n.w + optArg.C0regularity);
				ansatz = signals.Sinusoidal.fourierBasis( I(2), ...
					"order", optArg.dof-1 + optArg.C0regularity, ...
					"numDiff", obj.n.w + optArg.C0regularity);
				% compute the ansatz function regularized by the bump-function
				ansatz = bump * ansatz;
			else
				error( "The ansatz function " + optArg.ansatz + " is not known");
			end
			
			% the differential expressions for the parametrization of system variabels in term of
			% the basis variable are precomputed and accessed via the object property:
			de = obj.differentialExpressions;
			dimAnsatz = size(ansatz.');
			
			% establish the linear system of equations:
			% M1_j = [ dtau^i ansatz(tau^+) ], i = 0, ..., n_w
			M1j = zeros( obj.n.w, dimAnsatz(2) ); % for tau = tau^+
			M2j = zeros( obj.n.w, dimAnsatz(2) ); % for tau = T-tau^-
			for k = 1:obj.n.w + optArg.C0regularity% iteration until d_tau^i, i=0,...,n_q-1
				M1j(k,:) = ansatz.diff( I(2), k-1 ).atIndex(1).';
				M2j(k,:) = ansatz.diff( I(2), k-1 ).atIndex(end).';
			end
			Mj = [M1j; M2j];
			
			
			% 0 = M*eta -> eta = pinv(M) * 0 + null(M)*etaStar
			M = kron( eye( obj.sys.n.y ), Mj );
			
			% mBarF = Mf * eta 
			% -> mBarF = Mf * null(M) * etaStar
			% -> etaStar = pinv(Mf * null(M)) * mBarF + null( Mf*null(M) ) * etaStar
			Zj =  null( Mj ) ;
			
			if optArg.ansatz == "polybump"
				Z = eye( 2 * (optArg.dof) );
			else
				Z = kron( eye( obj.sys.n.y ), Zj );	% null(M)
			end
			
			% The reference trajectory for \tilde{m}^-(0,\tau) is piecewise defined, thus it is also
			% piecewise computed:
			% ThetaMu_I2 is the mapping from the coefficients of the ansatz function to the
			% reference trajectory \tilde{m}^-(0,\tau) = \mu(\tau), \tau\in\Time_2, i.e.:
			%	mu = Theta_Mu_I2 * eta
			% where eta are the coefficients of the ansatz functions.
			ThetaMu_I2 = zeros( dimAnsatz );
			for k = 1:(obj.n.w+1)
				ThetaMu_I2 = ThetaMu_I2 + de.alpha(k) * ansatz.diff(I(2), k-1).';
			end
			
			ThetaMu_I1 = quantity.Discrete.zeros( dimAnsatz, I(1));
			ThetaMu_I3 = quantity.Discrete.zeros( dimAnsatz, I(3));
			ThetaMu_j = quantity.Piecewise({ThetaMu_I1, ThetaMu_I2, ThetaMu_I3}, ...
				"addPoint", true, "upperBoundaryIncluded", [true, false]);
			
			ThetaMu = kron( eye( obj.sys.n.y ), ThetaMu_j ).subs( "tau", tau);
			
			ThetaMuZ = ThetaMu*Z;
			
			PsiThetaMuZ = obj.Psi( ThetaMuZ, tau );
						
			%% compute TheteQ by a simulation
			% it has been shown that the computation of the ode states q is more robust to numerical
			% erros if it is computed by a simulation of the corresponding state-space system. To
			% this end, the input of this system is computed
			qOdeInputZ = int( obj.L_1 * PsiThetaMuZ, "z", 0, 1) ... 
 				+ obj.L_2 * obj.Jn.' * ThetaMuZ + obj.L_3 * PsiThetaMuZ.subs("z", 1);
			% and the state-space system is initialized
			qOde = ss( obj.F, eye(obj.n.w), eye(obj.n.w), []);
			ThetaQZ = quantity.Discrete.zeros([obj.n.w, size(Z,2)], tau);
			% and simulated using lsim
			for k = 1:size(Z,2)
				ThetaQZ(:,k) = stateSpace.lsim( qOde, qOdeInputZ(:,k), tau);
			end
			
			% Compute the coefficient matrices for the io-kernels in backstepping coordinates:
			% for the m_f(\tau) io-kernel:
			[E1, E2, E3, E4] = obj.ioCoefficientsInBacksteppingCoordinates("E", -1);
			% m_f = ThetaF * eta
			ThetaFZ = int( E1 * PsiThetaMuZ, "z", 0, 1) + E2 * ThetaMuZ + ...
				E3 * PsiThetaMuZ.subs("z", 1) + E4 * ThetaQZ;
			
			% for the m_{\bar{d}}(\tau) io-kernel:
			[G1, G2, G3, G4] = obj.ioCoefficientsInBacksteppingCoordinates("G", obj.sys.G_bounded.');
			%
			ThetaGBarZ = int( G1 * PsiThetaMuZ, "z", 0, 1) + G2 * ThetaMuZ + ...
				G3 * PsiThetaMuZ.subs("z", 1) + G4 * ThetaQZ;
			
			% for the m_u io-kernel:
			[B1, B2, B3, B4] = obj.ioCoefficientsInBacksteppingCoordinates("B");
			%
			ThetaU = int( B1 * PsiThetaMuZ, "z", 0, 1) + B2 * ThetaMuZ + ...
				B3 * PsiThetaMuZ.subs("z", 1) + B4 * ThetaQZ;
						
			% for the n io-kernel
			[C1, C2, C3, C4] = obj.ioCoefficientsInBacksteppingCoordinates("C");
			ThetaN = int( C1 * PsiThetaMuZ, "z", 0, 1) + C2 * ThetaMuZ + ...
				C3 * PsiThetaMuZ.subs("z", 1) + C4 * ThetaQZ;

			%%
			MfZ = int( optArg.normedFaultFunction.subs("tau", tau) * ThetaFZ ) ; % Mf * null(M)
		
% 			% alternative computation for MfZ:
% 			V = obj.Jn.' + obj.Jp.' * obj.Q_0;
% 			MBarf = int( de.Xf1(:,:,1) * ( V + int( obj.A_0, "z", 0, "z" )), "z", 0, 1) + de.Xf2(:,:,2) ...
% 						+ de.Xf3(:,:,1) * ( V + int( obj.A_0, "z", 0, 1));
% 
% 			Theta = quantity.Discrete( kron( eye(obj.sys.n.y), ansatz.f.') );
% 			rBar = ones(5,1);
% 			MfZ = MBarf * int( Theta * Z );
						
			sMf = svd(MfZ);
			if numel(sMf) >= obj.sys.n.y+1
				svdTol = mean( sMf( obj.sys.n.y:obj.sys.n.y+1 ) );
				pMfZ = pinv(MfZ,  svdTol);
				
% 				use the approach with the precomputed pseudo invervese, because the singular values
% 				of MfZ indicate that there are more dependent rows than there should be. Since there
% 				are only two inputs, the linear independent rows in MfZ should be not be greater
% 				than two. This is considered in the computation of the pseudo-inverse and with this
% 				form also in the annihilator...
				Z_MfZ = misc.nullTol(MfZ, svdTol);
% 				Z_MfZ = orth( eye(size(MfZ,2))-pMfZ*MfZ );
%  				Z_MfZ = null(MfZ);
			else
				pMfZ = pinv(MfZ);
				Z_MfZ = null(MfZ);
			end
			mBarF = MfZ * pMfZ * optArg.desiredGains;
			
			% --- establish the optimization problem for the remaining degrees of freedom --- %
			if isempty( Z_MfZ )
% 				warning("No degree of freedom available")
				etaStar = pMfZ * mBarF;
			else
				%	rB = min int( | theta_d + Z * ( Mf^+ * mBar + Z_f mBarStar) 
				mBarStar = faultdiagnosis.thresholdOptimizerL1( obj, ... 
					ThetaGBarZ * pMfZ * mBarF, ThetaGBarZ * Z_MfZ, optArg.delta);

				etaStar = pMfZ * mBarF + Z_MfZ * mBarStar;
			 end
						
			mBarFStar = MfZ * etaStar;
			
			if any( abs( mBarFStar - mBarF ) > 1e-6)
				warning("Difference between desired gain and resulting gain is: " + misc.vector2string( mBarF - mBarFStar, "formatSpec", "%.2e"));
			end
			
			% --- compute the io-kernels --- %
			ioKernel.Mf = ThetaFZ * etaStar;
			ioKernel.Md_bounded = ThetaGBarZ * etaStar;
			ioKernel.Mu = ThetaU * etaStar;
			ioKernel.N = ThetaN * etaStar;
			% set the correct names of the io kernels
			ioKernel.Mf.setName("m_f");
			ioKernel.Mu.setName("m_u");
			ioKernel.Md_bounded.setName("m_{\bar{d}}");
			ioKernel.N.setName("n");
			% write some information into the log:
			obj.logMsg("Mf(T) = " + misc.vector2string( ioKernel.Mf.atIndex(end), "formatSpec", "%.2e" ))
% 			obj.logMsg("N(T) = " + misc.vector2string( ioKernel.N.atIndex(end), "formatSpec", "%.2e" ))
			obj.logMsg("MdBar(T) = " + misc.vector2string( ioKernel.Md_bounded.atIndex(end), "formatSpec", "%.2e" ))
			obj.logMsg("Mu(T) = " + misc.vector2string( ioKernel.Mu.atIndex(end), "formatSpec", "%.2e" ))
			
			obj.logMsg("Relative Mf(T) = " + misc.vector2string( ioKernel.Mf.atIndex(end) ./ max(abs(ioKernel.Mf)), "formatSpec", "%.2e" ))
% 			obj.logMsg("Relative N(T) = " + misc.vector2string( ioKernel.N.atIndex(end) ./ max(abs(ioKernel.N)), "formatSpec", "%.2e" ))
			obj.logMsg("Relative MdBar(T) = " + misc.vector2string( ioKernel.Md_bounded.atIndex(end) ./ max(abs(ioKernel.Md_bounded)), "formatSpec", "%.2e" ))
			obj.logMsg("Relative Mu(T) = " + misc.vector2string( ioKernel.Mu.atIndex(end) ./ max(abs(ioKernel.Mu)), "formatSpec", "%.2e" ))			
			
			if nargout >= 2
				% if required compute some additional information about the obtained solution
				Wf = diag( mBarF );
				Wd = diag( optArg.delta );

				Sd = l2norm( Wd * ioKernel.Md_bounded )^2;
				Sf = l2norm( Wf * ioKernel.Mf )^2;
				
				measures.mBarFStar = mBarFStar;
				measures.availableDOF = rank(Z);
% 				measures.desiredDOF = size(M1,2) - size(M1,1);
				measures.Sd = Sd;
				measures.Sf = Sf;
				measures.JStar = Sd/Sf;
				measures.mfGain = int( ioKernel.Mf );
				measures.threshold = obj.thresholdVectOfAbsValues( ioKernel.Md_bounded, optArg.delta);
				measures.mfL2 = int( ioKernel.Mf.^2 );
				measures.mfMax = max( abs( ioKernel.Mf ) );
				measures.name = "r_B";
				measures.n_phi = 2*dimAnsatz(2);

			end
			
			if nargout >= 3
				% if required compute also the not necessarily required integral kernels
				p = ThetaQZ * etaStar;
				if any(p.atIndex(end) ./ max(abs(p)) > 1e-6)
					warning("ode states do not reach zero: p(T) = " + misc.vector2string(p.atIndex(end)))
				end
				
				mTilde = PsiThetaMuZ * etaStar;

				m_ = obj.backsteppingKernel.inverseBacksteppingTransformation(mTilde);
				m = m_.flipDomain("z");
				n = obj.inverseTransformOfInputSignal(m_, p);
				pw = obj.J_ode_p * p;

				kernel.mTilde = mTilde;		
				kernel.m = m;
				kernel.N = n;
				kernel.pw = pw;
				kernel.pd = obj.J_ode_q_d * p;		

				% set the correct names:
				kernel.N.setName("n");
				kernel.m.setName("m");
				kernel.pw.setName("p_w");
				kernel.pd.setName("p_d");
				
				kernel.Theta = kron( eye(obj.sys.n.y), ansatz.f.');
				kernel.eta = Z * etaStar;
				kernel.phi = kernel.Theta * kernel.eta;
			end
		end
	
		
		function [kernels, measures, PsiTheta] = computeSolution(obj, PsiTheta, Theta0, optArg)
			arguments
				obj,
				PsiTheta,
				Theta0 = 0,
				optArg.fExpected = ones(obj.n.f,1);
				optArg.delta = ones( obj.sys.n.d_bounded, 1);
				optArg.repeatedOptimization = true;
				optArg.SfDesired (1,1) double = 1;
				optArg.f;
				optArg.performanceIndex = "rB";
			end
			de = obj.differentialExpressions;
						
			mfx = obj.applyTo( PsiTheta, de.Xf1, de.Xf2, de.Xf3);
			mdBx = obj.applyTo( PsiTheta, de.XdB1, de.XdB2, de.XdB3);
			
			Z = null( Theta0 );
			X = mfx * Z; % Theta_f
			Y = mdBx * Z;	
			
			if optArg.performanceIndex == "rB"
				[gammaStar, mf, mdB, measures] = referenceTrajectoryThreshold(obj, X, Y, optArg.fExpected, optArg.delta);
				gamma = Z*gammaStar;
			elseif optArg.performanceIndex == "h2h2"
				[gammaStar, mf, mdB, measures] = referenceTrajectoryH2H2(obj, X, Y, optArg.fExpected, optArg.delta);
				gamma = Z*gammaStar;
			elseif optArg.performanceIndex == "rB2"
				[gamma, mf, mdB, measures] = referenceTrajectoryThreshold2(obj, Theta0, mfx, mdBx, optArg.fExpected, optArg.delta);
			elseif optArg.performanceIndex == "II"
				[gammaStar, mf, mdB, measures] = referenceTrajectoryII(obj, X, Y, optArg.fExpected, optArg.delta);
				gamma = Z*gammaStar;
			else
				error("The performance index " + optArg.performanceIndex + " is not known.")
			end
			
			if false
				plot(mf); suptitle( "$\bar{m}_f = " + misc.vector2string(optArg.fExpected, ", ") + "$")
				plot(mdB); suptitle( "$\bar{m}_f = " + misc.vector2string(optArg.fExpected, ", ") + "$")

				dt = optArg.f(1).domain.stepSize();
				sim.I = quantity.EquidistantDomain("tau", tau.lower, tau.upper, "stepSize", dt);
				sim.fir = signals.fir.midpoint( mf.', sim.I);
				r = signals.fir.mFilter( sim.fir, optArg.f );

				figure(); clf
				r.plot("figureId", 0);
				th = diag([measures.threshold, -measures.threshold] )* quantity.Discrete.ones([2;1], optArg.f(1).domain);
				th.plot("figureId", 0, "hold", true, "smash", true)
			end
						
			Psi_dtau_k_phi = {1:obj.n.w + 1};
			for k = 1:obj.n.w + 1
				Psi_dtau_k_phi{k} = PsiTheta{k} * gamma;
			end
			mTilde = zeros( obj.sys.n.x, 1);
			for k = 1:obj.n.w + 1
 				mTilde = mTilde + de.alpha(k) * Psi_dtau_k_phi{k};
			end
						
			p = zeros( obj.n.w, 1);
			for k = 1:obj.n.w
				p = p + de.S(:,:,k) * ( int( obj.L_1 * Psi_dtau_k_phi{k}, "z", 0, 1) ...
					+ obj.L_2 * Psi_dtau_k_phi{k}.subs("z", 0) ...
					+ obj.L_3 * Psi_dtau_k_phi{k}.subs("z", 1) );
			end
			
			m_ = obj.backsteppingKernel.inverseBacksteppingTransformation(mTilde);
			m = m_.flipDomain("z");
			n = obj.inverseTransformOfInputSignal(m_, p);
			pw = obj.J_ode_p * p;
			
			kernels.Mf = mf;
			kernels.Mu = obj.computeModulatingFunction("B", m, pw, n);
			kernels.Md = obj.computeModulatingFunction("G", m, pw, n);
			kernels.Md_bounded = mdB;
			kernels.Md_deterministic = ...
				obj.sys.G_deterministic.' * kernels.Md;
% 			kernel.M_G_stochastic = ...
% 				obj.sys.G_stochastic.' * kernel.Md;
			kernels.m = m;
			kernels.N = n;
			kernels.pw = pw;
			kernels.pd = obj.J_ode_q_d * p;
			
			% set the correct names:
			kernels.Mf.setName("m_f");
			kernels.Mu.setName("m_u");
			kernels.Md.setName("m_d");
			kernels.Md_bounded.setName("m_{\bar{d}}");
			kernels.Md_deterministic.setName("m_{\tilde{d}}");
			kernels.N.setName("n");
			kernels.m.setName("m");
			kernels.pw.setName("p_w");
			kernels.pd.setName("p_d");
		end % computeSolution
		
		function [PsiTheta, Theta0, I] = computePsiTheta(obj, tau, optArg)
			arguments
				obj,
				tau (1,1) quantity.EquidistantDomain;
				optArg.dof (1,1) double {mustBeInteger} = int32(2);
				optArg.ansatz = "sinusoidal";
			end
			
			% check that T is seperable into dt
			assert( mod(tau.upper, tau.stepSize) == 0, ...
				'The transfer time T must be a multiple of dt. Thus it is changed to the next higher value');
			
			%% Define the time domains:
			% the time-domain for the transition is separated into three
			% parts. These parts are obtained from the initial and end
			% value for the kernel m(z,tau). Thus, the parts are defined as
			%	I_1 = [-t_Delta_minus, t_Delta_plus]
			%	I_2 = (t_Delta_plus, T - t_Delta_minus)
			%	I_3 = [T - t_Delta_minus, t_Delta_plus]
			
			I = quantity.Domain('tau', obj.theta_min : tau.stepSize : tau.upper+obj.theta_max);
			I = I.split( [ obj.theta_max, tau.upper + obj.theta_min ], "addPoints", true );
			
			if optArg.ansatz == "taylorPolynomial"
				ansatz = signals.Monomial.taylorPolynomialBasis( I(2), ...
					"order", 2*obj.n.w + optArg.dof, "numDiff", obj.n.w);
			elseif optArg.ansatz == "sinusoidal"
				ansatz = signals.Sinusoidal.fourierBasis( I(2), ...
					"order", 2*obj.n.w + optArg.dof, "numDiff", obj.n.w);
			else
				error( "The ansatz function " + optArg.ansatz + " is not known");
			end
			
			Ansatz = kron( ansatz.', eye( obj.sys.n.y) );
			
			Theta0 = zeros( 0, size( Ansatz, 2) );
			for k = 1:obj.n.w
				Theta0 = [Theta0; Ansatz.diff(I(2), k-1).at( I(2).lower )];
				Theta0 = [Theta0; Ansatz.diff(I(2), k-1).at( I(2).upper )];
			end
			assert( size(Theta0, 1) == 2*obj.n.w*obj.sys.n.y);
			
			
			theta_I1 = quantity.Discrete.zeros( size(Ansatz), I(1));
			theta_I3 = quantity.Discrete.zeros( size(Ansatz), I(3));
			
			% in the parfor-loop it is much faster if the external function is used with
			% pre-computed matrices. Maybe this reduces the communication overhead.
			V_p = obj.sys.Jn.' - obj.sys.Jp.' * obj.sys.Q_1.';
			
			tau_plus_int_Lambda = ...
				ones(obj.n.x, 1) * quantity.Discrete(tau.grid, tau) ...
				+ obj.Int_zeta_z_Lambda_1_dz;
			
			A_0_zeta = subs(obj.A_0, "z", "zeta");
			
			z = obj.spatialDomain;
			
			PsiTheta = {};
			t0 = tic;
			
			if optArg.ansatz == "taylorPolynomial"
				obj.logMsg("Start computation of Psi[theta_" + 1 + "]\n");
				h = quantity.Piecewise({ theta_I1, Ansatz.f, theta_I3 }, "addPoint", true);
				
				t1 = tic;
				%in the following, it is much more memory efficient without any preallocation of
				%matrices!
				parfor l = 1:size(h,2)
% 					obj.logMsg("start l = %i \n", l);
					disp("start iteration l="+l);
					PsiTheta0(:,l) = faultdiagnosis.hyperbolic.heteroDirectional.Psi( z, tau, ...
						h(:,l), tau_plus_int_Lambda, V_p, A_0_zeta);
% 					obj.logMsg("finish l = %i at %g \n", l, toc(t2));
				end
				PsiTheta{1} = PsiTheta0;
				obj.logMsg("finish k = %i at %g \n", 1, toc(t1));
				
				nPhi = size(ansatz,1);
				ny = obj.sys.n.y;
				for k = 1:obj.n.w
					
					% dt^0 Psi = [ Psi1, Psi2, Psi3, Psi4, ... ]
					% dt^1 Psi = [ 0, Psi1, Psi2, Psi3, Psi4, ... ]
					% dt^2 Psi = [ 0, 0, Psi1, Psi2, Psi3, Psi4, ... ]
					%
					% dt^k Psi = [ 0, 0, ..., Psi1, Psi2, ... ]
					%
					idx0 = 1 : k*ny;
					idx1 = (k*ny+1) : nPhi*ny;
					PsiTheta{k+1}(:,idx0) = PsiTheta{1}(:,idx0) * 0;
					PsiTheta{k+1}(:,idx1) = PsiTheta{1}(:,1:(nPhi-k)*ny);
				end
				obj.logMsg("Computation of Psi[Theta](tau) required %g", toc(t0));
			else
				for k = 1:(obj.n.w+1)
					splicedTheta{k} = Ansatz.diff(I(2), k-1);
				end
				
				for k = 1:(obj.n.w+1)
					t1 = tic;
					obj.logMsg("Start computation of Psi[theta_" + k + "]");
					h = quantity.Piecewise({ theta_I1, splicedTheta{k}, theta_I3 }, "addPoint", true);
					
					parfor l = 1:size(h,2)
						disp("start iteration l="+l);
						tmp(:,l,k) = faultdiagnosis.hyperbolic.heteroDirectional.Psi( z, tau, ...
							h(:,l), tau_plus_int_Lambda, V_p, A_0_zeta);
					end
					PsiTheta{k} = tmp(:,:,k);
					obj.logMsg("   Elapsed time: %g\n", toc(t1));
				end
			end
			
		end
		
		function [PsiTheta, I] = computePsiThetaRegularized(obj, tau, optArg)
			arguments
				obj,
				tau (1,1) quantity.EquidistantDomain;
				optArg.dof (1,1) double {mustBeInteger} = int32(2);
				optArg.ansatz = "sinusoidal";
			end
			
			
			% check that T is seperable into dt
			assert( mod(tau.upper, tau.stepSize) == 0, ...
				'The transfer time T must be a multiple of dt. Thus it is changed to the next higher value');
			
			%% Define the time domains:
			% the time-domain for the transition is separated into three
			% parts. These parts are obtained from the initial and end
			% value for the kernel m(z,tau). Thus, the parts are defined as
			%	I_1 = [-t_Delta_minus, t_Delta_plus]
			%	I_2 = (t_Delta_plus, T - t_Delta_minus)
			%	I_3 = [T - t_Delta_minus, t_Delta_plus]
			
			I = quantity.Domain('tau', obj.theta_min : tau.stepSize : tau.upper+obj.theta_max);
			I = I.split( [ obj.theta_max, tau.upper + obj.theta_min ], "addPoints", true );
			
			%%
			if optArg.ansatz == "taylorPolynomial"
				ansatz = signals.Monomial.taylorPolynomialBasis( I(2), "order", ...
					optArg.dof , "numDiff", obj.n.w);
			elseif optArg.ansatz == "sinusoidal"
				ansatz = signals.Sinusoidal.fourierBasis( I(2), "order", optArg.dof, ...
					"numDiff", obj.n.w);
			else
				error( "The ansatz function " + optArg.ansatz + "is not known");
			end
			
			smoother = signals.PolyBump(I(2), "a", obj.n.w, "b", obj.n.w, "norm", true, ...
				"numDiff", obj.n.w); %verify: is numDiff obj.n.w or obj.n.w+1???
			smoothAnsatz = ansatz*smoother;
			Ansatz = kron( smoothAnsatz.', eye( obj.sys.n.y) );
			
			theta_I1 = quantity.Discrete.zeros( size(Ansatz), I(1));
			theta_I3 = quantity.Discrete.zeros( size(Ansatz), I(3));
			
			% in the parfor-loop it is much faster if the external function is used with
			% pre-computed matrices. Maybe this reduces the communication overhead.
			V_p = obj.sys.Jn.' - obj.sys.Jp.' * obj.sys.Q_1.';
			
			tau_plus_int_Lambda = ...
				ones(obj.n.x, 1) * quantity.Discrete(tau.grid, tau) ...
				+ obj.Int_zeta_z_Lambda_1_dz;
			
			A_0_zeta = subs(obj.A_0, "z", "zeta");
			
			z = obj.spatialDomain;
			
			PsiTheta = {};
			
			for k = 1:(obj.n.w+1)
				splicedTheta{k} = Ansatz.diff(I(2), k-1);
			end
			
			for k = 1:(obj.n.w+1)
				t1 = tic;
				obj.logMsg("Start computation of Psi[theta_" + k + "]");
				h = quantity.Piecewise({ theta_I1, splicedTheta{k}, theta_I3 }, "addPoint", true);
				
				parfor l = 1:size(h,2)
					disp("compute " + l )
					tmp(:,l,k) = faultdiagnosis.hyperbolic.heteroDirectional.Psi( z, tau, ...
						h(:,l), tau_plus_int_Lambda, V_p, A_0_zeta);
				end
				PsiTheta{k} = tmp(:,:,k);
				obj.logMsg("   Elapsed time: %g\n", toc(t1));
			end
			
		end
		
		
		function [kernels, measures] = computeSolution2(obj, tau, optArg)
			arguments
				obj,
				tau (1,1) quantity.EquidistantDomain;
				optArg.dynamicalExtension = 0;
				optArg.usePolyomial = false;
				optArg.silent = false;
				optArg.fExpected (:,1) double = ones(obj.n.f,1);
				optArg.dof (1,1) double {mustBeInteger} = int32(1);
				optArg.delta = ones( obj.sys.n.d_bounded, 1);
				optArg.repeatedOptimization = true;
				optArg.SfDesired (1,1) double = 1;
				optArg.f;
				optArg.performanceIndex = "rB"
				optArg.ansatz = "sinusoidal"
			end
			
			% 			myProgressBar = misc.ProgressBar('name', "Fault diagnosis kernel solution", ...
			% 				'terminalValue',
			%
			t1 = tic;
			
			% check that T is seperable into dt
			assert( mod(tau.upper, tau.stepSize) == 0, ...
				'The transfer time T must be a multiple of dt. Thus it is changed to the next higher value');
			
			%% Define the time domains:
			% the time-domain for the transition is separated into three
			% parts. These parts are obtained from the initial and end
			% value for the kernel m(z,tau). Thus, the parts are defined as
			%	I_1 = [-t_Delta_minus, t_Delta_plus]
			%	I_2 = (t_Delta_plus, T - t_Delta_minus)
			%	I_3 = [T - t_Delta_minus, t_Delta_plus]

			I = quantity.Domain('tau', obj.theta_min : tau.stepSize : tau.upper+obj.theta_max);
			I = I.split( [ obj.theta_max, tau.upper + obj.theta_min ], "addPoints", true );
					
			% premilinary initializations:
			sImF = signals.PolynomialOperator( {-obj.F, eye(obj.n.w)} );
			adjF = adj(sImF);
			detF = det(sImF);
			
			commonRoot = nan;
			
			while ~isempty(commonRoot)
			
				rootsAdj = adjF.findCommonZeros();
				rootsDet = detF.findCommonZeros();

				commonRoot = numeric.intersect( rootsAdj, rootsDet );

				if ~isempty( commonRoot )
					warning("Remove root %g \n", commonRoot)
					adjF = signals.PolynomialOperator( expand( adjF.sym ./ (obj.s - commonRoot) ) );
					detF = signals.PolynomialOperator( expand( detF.sym ./ (obj.s - commonRoot) ) );
				end
			end
			
			alpha = shiftdim( double( detF ) ).';
			S = double( adjF );
			Psi_I = obj.Psi(eye( obj.n.n ) * quantity.Discrete.ones(1, I(2)) , I(2));
			
			[Xf1, Xf2, Xf3] = obj.ioKernelDifferentialExpression("E", alpha, S);
			[Xd1, Xd2, Xd3] = obj.ioKernelDifferentialExpression("G", alpha, S);
			
			% apply the selection matrix for the bounded disturbances and consider the minus sign!
			for k = 1:length(alpha)
				XdB1(:,:,k) = - Xd1(:,:,k) * obj.sys.G_bounded;
				XdB2(:,:,k) = - obj.sys.G_bounded.' * Xd2(:,:,k);
				XdB3(:,:,k) = - obj.sys.G_bounded.' * Xd3(:,:,k);
			end
			
			
			%%
			if optArg.ansatz == "taylorPolynomial"
				ansatz = signals.Monomial.taylorPolynomialBasis( I(2), "order", ...
					optArg.dof , "numDiff", length(alpha)-1);
			elseif optArg.ansatz == "sinusoidal"
				ansatz = signals.Sinusoidal.fourierBasis( I(2), "order", optArg.dof, ...
					"numDiff", length(alpha)-1);
			else 
				error( "The ansatz function " + optArg.ansatz + "is not known");
			end
			
			theta = signals.PolyBump(I(2), "a", obj.n.w, "b", obj.n.w, "norm", true, "numDiff", length(alpha));
			basis = ansatz*theta;
			THETA = kron( basis.', eye( obj.sys.n.y) );
			
			theta_I1 = quantity.Discrete.zeros( size(THETA), I(1));
			theta_I3 = quantity.Discrete.zeros( size(THETA), I(3));
			
			for k = 1:length(alpha)
				splicedTheta{k} = THETA.diff(I(2), k-1);
			end
			
			%%
			% in the parfor-loop it is much faster if the external function is used with
			% pre-computed matrices. Maybe this reduces the communication overhead.
			V_p = obj.sys.Jn.' - obj.sys.Jp.' * obj.sys.Q_1.';
			
			tau_plus_int_Lambda = ...
				ones(obj.n.x, 1) * quantity.Discrete(tau.grid, tau) ...
				+ obj.Int_zeta_z_Lambda_1_dz;
			
			A_0_zeta = subs(obj.A_0, "z", "zeta");
			
			z = obj.spatialDomain;
			
			PsiTheta = {};
			for k = 1:length(alpha)
				t1 = tic;
				obj.logMsg("Start computation of Psi[theta_" + k + "]");
				h = quantity.Piecewise({ theta_I1, splicedTheta{k}, theta_I3 }, "addPoint", true);
				
				parfor l = 1:size(h,2)
					tmp(:,l,k) = faultdiagnosis.hyperbolic.heteroDirectional.Psi( z, tau, ...
						h(:,l), tau_plus_int_Lambda, V_p, A_0_zeta);
				end
				PsiTheta{k} = tmp(:,:,k);
				obj.logMsg("   Elapsed time: %g\n", toc(t1));
			end
			
			mfX = obj.applyTo( PsiTheta, Xf1, Xf2, Xf3);
			mdBx = obj.applyTo( PsiTheta, XdB1,XdB2, XdB3);				
			
			%
			if optArg.performanceIndex == "rB"
				[gammaStar, mf, mdB, measures] = referenceTrajectoryThreshold(obj, ...
					mfX, mdBx, optArg.fExpected, optArg.delta);
			elseif optArg.performanceIndex == "h2h2"
				[gammaStar, mf, mdB, measures] = referenceTrajectoryH2H2(obj, ...
					X, Y, optArg.fExpected, optArg.delta);
			else
				error("The performance index " + optArg.performanceIndex + " is not known.")
			end
			
			%
			Psi_dtau_k_phi = {1:obj.n.w + 1};
			for k = 1:obj.n.w + 1
				Psi_dtau_k_phi{k} = PsiTheta{k} * gammaStar;
			end
			mTilde = zeros( obj.sys.n.x, 1);
			for k = 1:obj.n.w + 1
 				mTilde = mTilde + alpha(k) * Psi_dtau_k_phi{k};
			end
						
			p = zeros( obj.n.w, 1);
			for k = 1:obj.n.w
				p = p + S(:,:,k) * ( int( obj.L_1 * Psi_dtau_k_phi{k}, "z", 0, 1) ...
					+ obj.L_2 * Psi_dtau_k_phi{k}.subs("z", 0) ...
					+ obj.L_3 * Psi_dtau_k_phi{k}.subs("z", 1) );
			end
			
			m_ = obj.backsteppingKernel.inverseBacksteppingTransformation(mTilde);
			m = m_.flipDomain("z");
			n = obj.inverseTransformOfInputSignal(m_, p);
			pw = obj.J_ode_p * p;
			
			kernels.Mf = mf;
			kernels.Mu = obj.computeModulatingFunction("B", m, pw, n);
			kernels.Md = obj.computeModulatingFunction("G", m, pw, n);
			kernels.Md_bounded = mdB;
			kernels.Md_deterministic = ...
				obj.sys.G_deterministic.' * kernels.Md;
% 			kernel.M_G_stochastic = ...
% 				obj.sys.G_stochastic.' * kernel.Md;
			kernels.m = m;
			kernels.N = n;
			kernels.pw = pw;
			kernels.pd = obj.J_ode_q_d * p;
			
			% set the correct names:
			kernels.Mf.setName("m_f");
			kernels.Mu.setName("m_u");
			kernels.Md.setName("m_d");
			kernels.Md_bounded.setName("m_{\bar{d}}");
			kernels.Md_deterministic.setName("m_{\tilde{d}}");
			kernels.N.setName("n");
			kernels.m.setName("m");
			kernels.pw.setName("p_w");
			kernels.pd.setName("p_");
		end % computeSolution		
			
		function mX = applyTo( obj, PsiTheta, X1, X2, X3)
			mX = zeros(size(X2,1), size( PsiTheta{1}, 2) );
			for k = 1:obj.n.w + 1
				mX = mX + obj.Chi( PsiTheta{k}, X1(:,:,k), X2(:,:,k), X3(:,:,k));
			end				
		end
		
		function [H1, H2, H3, H4] = ioCoefficientsInBacksteppingCoordinates(obj, name, factor)
			arguments
				obj
				name
				factor = 1;
			end
			
			KIzetaZ = obj.backsteppingKernel.getValueInverse().subs(["z", "zeta"], ["zeta", "z"]);
			
			if name == "C"
				H1 = inv( obj.sys.C_0.' ) * ( ...
					( obj.sys.Jn + obj.sys.Q_0.' * obj.sys.Jp) * KIzetaZ.subs("zeta", 1) ...
					+ obj.sys.A_0.flipDomain("z").' ...
					+ int( obj.sys.A_0.flipDomain("z").subs("z", "zeta").' * KIzetaZ, "zeta", "z", 1) );
				H2 = zeros( obj.sys.n.y );
				H3 = obj.sys.C_0.' \ ( obj.sys.Jn + obj.sys.Q_0.' * obj.sys.Jp);
				H4 = obj.sys.C_0.' \ (obj.sys.L_2 * obj.sys.Jn').' * obj.J_ode_p;
			else				
				H1tmp = obj.sys.(name + "_1").flipDomain('z').' ...
					- obj.sys.(name + "_4").' /  obj.sys.C_0.' * obj.sys.A_0.flipDomain("z").';
				
				H3 = obj.sys.(name + "b0").' * obj.sys.Jp ...
					- obj.sys.(name + "_4").' / obj.sys.C_0.' * ( obj.sys.Jn + obj.sys.Q_0.' * obj.sys.Jp);

				H1 = H1tmp + int( H1tmp.subs("z", "zeta") * KIzetaZ, ...
					"zeta", "z", 1) + H3 * KIzetaZ.subs("zeta", 1);

				H2 = -obj.sys.(name + "b1").';
				H4 = (obj.sys.(name + "_3").' - obj.sys.(name + "_4").' / obj.sys.C_0.' ...
					* (obj.sys.L_2 * obj.sys.Jn').') * obj.J_ode_p;
			end
			
			H1 = factor * H1;
			H2 = factor * H2;
			H3 = factor * H3;
			H4 = factor * H4;
			
		end
		
		
		function [X1, X2, X3] = ioKernelDifferentialExpression(obj, name, alpha, S, factor)
			% compute the differential parametrizations for mf and mdBar
			arguments
				obj
				name
				alpha
				S
				factor = 1;
			end
			[XTilde1, XTilde2, XTilde3, XTilde4] = ...
				obj.ioCoefficientsInBacksteppingCoordinates(name, factor);
			
			S(:,:,end + 1) = 0;
			
			for k = 1:length(alpha)
				X1(:,:,k) = alpha(k) * XTilde1 + XTilde4 * S(:,:,k) * obj.L_1;
				X2(:,:,k) = alpha(k) * XTilde2 + XTilde4 * S(:,:,k) * obj.L_2 * obj.sys.Jn';
				X3(:,:,k) = alpha(k) * XTilde3 + XTilde4 * S(:,:,k) * obj.L_3;
			end
		end
		
		function y = Chi( obj, PsiPhi, X1, X2, X3)
			% y = < X1, Psi[phi]> + X2 Psi[phi](0) + X3 Psi[phi](1)
			y = int( X1.' * PsiPhi, "z", 0, 1) + X2 * PsiPhi.subs("z", 0) + X3 * PsiPhi.subs("z", 1);
		end
				
		function [filters, modulatingFunctions, kernels] = computeDetectionFilter(obj, I, optArgs)
			arguments
				obj
				I (1,1) quantity.EquidistantDomain
				optArgs.bounds (:,1) double = ones(obj.sys.n.d_bounded,1)
				optArgs.dynamicalExtension (1,1) double = 0
				optArgs.WithInitialCondition (1,1) logical = false
				optArgs.firApproximation (1,1) string = "trapz"
				optArgs.kernels 
			end			
			
			% copmute identification kernels:
			if isfield(optArgs, "kernels")
				kernels = optArgs.kernels;
			else
				kernels = obj.computeSolution( I.stepSize, I.upper, ...
					"dynamicalExtension", optArgs.dynamicalExtension, ...
					"WithInitialCondition", optArgs.WithInitialCondition);
			end
				
			% compute the modulating functions
			modulatingFunctions.Mf = - obj.computeModulatingFunction("E", kernels.M, kernels.P, kernels.N);
			modulatingFunctions.Mu = obj.computeModulatingFunction("B", kernels.M, kernels.P, kernels.N);
			modulatingFunctions.Md = obj.computeModulatingFunction("G", kernels.M, kernels.P, kernels.N);
			modulatingFunctions.Md_bounded = ...
				obj.sys.G_bounded.' * modulatingFunctions.Md;
			modulatingFunctions.Md_deterministic = ...
				obj.sys.G_deterministic.' * modulatingFunctions.Md;
			modulatingFunctions.Md_stochastic = ...
				obj.sys.G_stochastic.' * modulatingFunctions.Md;			
			
			% compute the time-discrete FIR filters
			if optArgs.firApproximation == "firstOrderHold"
				filters.N = signals.fir.firstOrderHold(kernels.N.', I, 'flip', false);
				filters.Mu = signals.fir.firstOrderHold(modulatingFunctions.Mu.', I, 'flip', false);
				filters.Mf = signals.fir.firstOrderHold(modulatingFunctions.Mf.', I, 'flip', false);
			elseif optArgs.firApproximation == "trapz"
				filters.N = signals.fir.trapez(kernels.N.', I, 'flip', false);
				filters.Mu = signals.fir.trapez(modulatingFunctions.Mu.', I, 'flip', false);
				filters.Mf = signals.fir.trapez(modulatingFunctions.Mf.', I, 'flip', false);				
			else
				error("FIR approximation method " + optArgs.firApproximation + " is not known.")
			end
			filters.threshold = obj.thresholdVectOfAbsValues( modulatingFunctions.Md_bounded, optArgs.bounds);
			filters.dt = I.stepSize;
			filters.T = I.upper;
			filters.type = "diagnosis";
		end
				
		function fB = thresholdVectOfAbsValues(obj, M_G_bounded, d_bound)
			fB = int( abs(M_G_bounded) ).' * d_bound;
		end
				
		function M_ = computeModulatingFunction(obj, name, M, P, N)
			
			M_ = int( obj.sys.(name + "_1").' * M, "z", 0, 1) ...
				+ obj.sys.(name + "b0").' * subs( obj.sys.Jp * M, "z", 0 ) ...
				- obj.sys.(name + "b1").' * subs( obj.sys.Jn * M, "z", 1 ) ...
				+ obj.sys.(name + "_3").' * P ...
				- obj.sys.(name + "_4").' * N;
			
		end
		
		function n = inverseTransformOfInputSignal(obj, m_, p)
			%INVERSETRANSFORMOFINPUTSIGNAL
			% n = inverseTransformOfInputSignal(obj, m_, p)
			% Computes the input signal n(tau) in dependence of the integral kernels m_ and p. 
			
			n = obj.sys.C_0.' \ (int( obj.sys.A_0.flipDomain("z").' * m_, "z", 0, 1) + ...
				( obj.sys.Jn + obj.sys.Q_0.' * obj.sys.Jp ) * subs(m_, "z", 1) ...
				+ obj.sys.Jn * obj.sys.L_2.' * obj.J_ode_p * p);
		end
		
		function verifyKernelSolution(obj, originalSystem, faultModel, Mu, N, I, sol_tilde, optArgs)
			arguments
				obj
				originalSystem
				faultModel
				Mu
				N
				I
				sol_tilde
				optArgs.AbsTol = 1e-14;
				optArgs.backsteppingKernel;
			end
			
			O = quantity.Discrete.zeros([originalSystem.n_u, ...
				originalSystem.n_f], obj.spatialDomain);
			T = I.upper;
			
			fprintf("# Verify the boundary conditions of the modulating functions\n");
			misc.warning( numeric.near( subs(Mu, "tau", 0), 0), ...
				"initial condition for M_u not satisfied" );
			misc.warning( numeric.near( subs(Mu, "tau", T), 0), ...
				"end condition for M_u not satisfied" );
			
			fprintf("# Verify the boundary conditions of the solution in backstepping coordinats\n");
			misc.warning( near( subs(sol_tilde.m, "tau", 0), O, optArgs.AbsTol), "initial condition of modulating function not satisfied" );
			misc.warning( near( subs(sol_tilde.m, "tau", T), O, optArgs.AbsTol), "end condition of modulating function not satisfied" );
			
			fprintf("# Verify the solution in the backstepping coordinates\n");
			w = [sol_tilde.p; sol_tilde.q];
			[~, ~, sim_backstepping] = obj.verifySolution(...
				subs(sol_tilde.m, 'tau', 't'), ...
				subs(w, 'tau', 't'), ...
				subs(sol_tilde.n, 'tau', 't'), ...
				'w0', obj.eta_0);
			
			obj.verifyInitialEndCondition(sim_backstepping, ...
				T, O, O, obj.eta_0, zeros(obj.n.w, obj.n.f))
			
			fprintf("# Verify the solution in the original coordinates\n")
			fKernelOrgCo = ...
				originalSystem.faultDiagnosisKernelOriginalCoordinates(	faultModel.sys );
			
			% transform the planned solution into the original coordinates:
			if isfield(optArgs, 'backsteppingKernel')
				m = optArgs.backsteppingKernel.inverseBacksteppingTransformation(...
					sol_tilde.m);
				[~,~, sim_original] = fKernelOrgCo.verifySolution(...
					subs(m, 'tau', 't'), ...
					subs(w, 'tau', 't'), ...
					subs(N, 'tau', 't'), ...
					'w0', obj.eta_0);
			else
				sim_original = fKernelOrgCo.simulate(I, 'u', N, 'w0', obj.eta_0);
			end
			
			obj.verifyInitialEndCondition(sim_original, T, O, O, obj.eta_0, zeros(obj.n.w, obj.n.f))
			
			fprintf("--------------------------------------------\n # done\n\n")
		end % verifyKernelSolution
		
	end % methods (Access = public)
	
	methods ( Access = protected)
		function x = Psi(obj, h, tau, optArg)
			% Psi-operator
			% x = Psi(obj, h) compute the operator
			%	x = Psi[h(.)](z,t)
			arguments
				obj,
				h,
				tau
				optArg.idx_j = 1:obj.n.p;
				optArg.parallel = true;
			end
			
			if isnumeric(tau)
				tau = quantity.Domain("tau", tau);
			end
			
			V_p = obj.Jn.' + obj.Jp.' * obj.Q_0;
			
			x = quantity.Discrete.zeros( [obj.n.x, size(h, 2) ], [obj.spatialDomain, tau]);
			
			tau_plus_int_Lambda = ...
				ones(obj.n.x, 1) * quantity.Discrete(tau.grid, tau) ...
				+ obj.Int_zeta_z_Lambda_1_dz;
			
			A_0_zeta = subs(obj.A_0, "z", "zeta");
			idxj = optArg.idx_j;
			if optArg.parallel
				
				spatialDomain = obj.spatialDomain;
				o = quantity.Discrete.zeros( [1, size(h, 2) ], [spatialDomain, tau]);
				parfor i=1:obj.n.x
					xi = o;
					A_0_zeta_i = A_0_zeta(i,:);
					Vp_i = V_p(i,:);
					for k = 1:size(h,2)
						for j = idxj
							if numel(idxj) == 1
								h_idx = 1;
							else
								h_idx = j;
							end
							
							h_jk = h(h_idx,k).compose( tau_plus_int_Lambda(i) );
							
							A0_ij_h_j = A_0_zeta_i(j) * h_jk;
							
							xi(k) = xi(k) + Vp_i(j) * subs(h_jk, "zeta", 0) ...
								+ int( A0_ij_h_j, "zeta", 0, "z" );
						end
					end
					x(i, :) = xi;
				end
				
			else
				for i = 1:obj.n.x
					for j = optArg.idx_j
						for k = size(h,2)
							if numel(optArg.idx_j) == 1
								h_idx = 1;
							else
								h_idx = j;
							end

							h_j = h(h_idx,k).compose( tau_plus_int_Lambda(i) );

							A0_ij_h_j = A_0_zeta(i,j) * h_j;

							x(i,k) = x(i,k) + V_p(i,j) * subs(h_j, "zeta", 0) ...
								+ int( A0_ij_h_j, "zeta", 0, "z" );
						end
					end
				end
			end
			
			if tau.n == 1
				x = subs(x, tau.name, tau.grid);
			end
		end %Psi
		
	end % methods (Access = protected)
	
	methods (Static)
		
		%TODO make this static and protected
		out = solveKernel_i(varargin)
		
		% #TODO: this should not be required...
		function [targetSystem, K] = ...
				decouplingTransformation(originalSystem, varargin)
			% compute the backstepping transformation for the decoupling of
			% the pde from the ode
			
			% solve kernel equations
			K = kernel.Feedback(...
				originalSystem.systemOde.Lambda, ...
				'A', originalSystem.systemOde.A, ...
				'F', originalSystem.systemOde.F, ...
				'A0', originalSystem.systemOde.A0, ...
				'Q0', originalSystem.systemOde.Q0, ...
				'name', 'K', ...
				varargin{:});
			
			targetSystem = K.getTargetSystem(originalSystem.systemOde);
			
			targetSystem = ...
				faultdiagnosis.hyperbolic.heteroDirectional.System(targetSystem);
		end
	
		function ode = commonOde(sys, optArgs)
			arguments
				sys
				optArgs.faultModelOde ss
				optArgs.disturbanceModelOde ss
			end
			
			if isfield( optArgs, "faultModelOde")
				faultModelOde = optArgs.faultModelOde;
			else
				
				faultModelOde.A = [];
				faultModelOde.C = zeros([sys.n.f, 0]);
				
			end
			
			if isfield( optArgs, "disturbanceModelOde")
				disturbanceModelOde = optArgs.disturbanceModelOde;
			else
				error("TODO")
			end
			
			%%
			
			n_v_f = size( faultModelOde.A, 1);
			n_v_d = size( disturbanceModelOde.A, 1);
			R_f = faultModelOde.C;			
			R_d = disturbanceModelOde.C;
	
				
			ode.A = [ sys.F.', zeros(sys.n.w, n_v_f), zeros(sys.n.w, n_v_d); ...
					   R_f.' * sys.E_3.', faultModelOde.A.', zeros(n_v_f, n_v_d); ...
					   R_d.' * sys.G_deterministic.' * sys.G_3.', zeros(n_v_d, n_v_f), ...
					   disturbanceModelOde.A.'];
			ode.B_1 = [sys.H_1.'; ...
					  R_f.' * sys.E_1.'; ...
					  R_d.' * sys.G_deterministic.' * sys.G_1.'];
				  
			ode.Bb0 = [ sys.Hb0.'; ...
						R_f.' * sys.Eb0.'; ...
						R_d.' * sys.G_deterministic.' * sys.Gb0.'];
			ode.Bb1 = [ - sys.Hb1.'; ...
					  - R_f.' * sys.Eb1.'; ...
					  - R_d.' * sys.G_deterministic.' * sys.Gb1.'];
			ode.B_3 = [ - sys.H_4.'; ...
					  - R_f.' * sys.E_4.'; ...
					  - R_d.' * sys.G_deterministic.' * sys.G_4.'];
			
			ode.J_w = [	eye( sys.n.w ) ...
					zeros(sys.n.w, n_v_f) ...
					zeros(sys.n.w, n_v_d)];
				
			ode.J_q_f = [	zeros( n_v_f, sys.n.w ), ...
						eye( n_v_f), ...
						zeros( n_v_f, n_v_d) ];
					
			ode.J_q_d = [	zeros( n_v_d, sys.n.w ), ...
						zeros( n_v_d, n_v_f), ...
						eye( n_v_d) ];
				
			ode.O = [	zeros( sys.n.w, sys.n.f); ...	      
					R_f.'; ...
					zeros( n_v_d, sys.n.f)];
			
		end	
		
	end
	
	
end

