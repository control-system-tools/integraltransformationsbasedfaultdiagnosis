classdef Kernel < faultdiagnosis.hyperbolic.heteroDirectional.System & faultdiagnosis.Kernel
	% System for the fault diagnosis kernel
	%   dz m(z,t) = Lambda_1(z) dt m(z,t) + A_0(z) m_p(0,t)
	%   m_\n(0,t) = Q_0 m_\p(0,t)
	%   m_\p(1,t) = n(t)
	%   dt eta(t) = F eta(t) + int_0^1 L_1(z) m(z,t) dz + L_2(z) m_p(0,t)
	%              + L_3 m_n(1,t)
	%
	% This is the representation of the fault-diagnosis kernel with
	% inverted spatial coordinate.
	properties
		% the combine final state for ode-model and fault-model
		%	W_0 = [ 0; R_f; 0]
		eta_0 double;
		backsteppingKernel kernel.Feedback;
		
		% Selection matrix for the system-ODE
		%	dtau p(tau) = F^T p(tau) + int_0^1 H_1^T(z) m(z,tau) dz + H_2^T m_n(0,tau)
		%
		%	p(tau) = J_ode_p * eta(tau)
		J_ode_p double;
		
		% Selection matrix for the fault-model-ODE
		%	dtau q(tau) = S^T q(tau) + Q_T^T * ( int_0^1 E_1^T(z) m(z,tau)
		%	dz + E_2^T m_n(0,tau) + E_3^T m_p(1,tau) + E_4^T p(tau) )
		J_ode_q_f double;
		
		J_ode_q_d double;
		
		faultModel (:,1) signals.SignalModel;
	end
	
	methods
		function obj = Kernel(Lambda_1, S, backsteppingKernel, optArgs)
			% System for the fault diagnosis
			%  dz m(z,t) = Lambda_1(z) dt m(z,t) + A_0(z) m_p(z,t)
			%  m_\n(0,t) = Q_0 m_\p(0,t)
			%  m_\p(1,t) = n(t)
			%	 dt w(t) = S w(t) + int_0^1 L_1(z) m(z,t) dz ...
			%              + L_2 x_p(0,t) + L_3 x_n(1,t)
			
			arguments
				Lambda_1 quantity.Discrete;
				S double;
				backsteppingKernel kernel.Feedback;
				optArgs.A_0 quantity.Discrete;
				optArgs.Q_0 double;
				optArgs.L_1 quantity.Discrete;
				optArgs.L_2 double;
				optArgs.L_3 double;
				optArgs.eta_0 double;
				optArgs.J_ode_p double;
				optArgs.J_ode_q_f double;
				optArgs.J_ode_q_d double;
				optArgs.sys faultdiagnosis.hyperbolic.heteroDirectional.System
				optArgs.faultModel signals.SignalModel;
				optArgs.disturbanceModel signals.SignalModel;
			end
			
			obj@faultdiagnosis.hyperbolic.heteroDirectional.System( ...
				Lambda_1, S, ...
				'A_0', optArgs.A_0, ...
				'Q_0', optArgs.Q_0, ...
				'Bb1', eye( optArgs.sys.n.p ), ...
				'L_1', optArgs.L_1, ...
				'L_2', optArgs.L_2, ...
				'L_3', optArgs.L_3, ...
				'name', "kernel")
			
			obj.eta_0 = optArgs.eta_0;
			obj.backsteppingKernel = backsteppingKernel;
			
			if isfield(optArgs, 'sys')
				obj.sys = optArgs.sys;
			else 
				error("sys must be set")% TODO move kernel computation into the constructor
			end
			
			obj.n.f = size(obj.eta_0, 2);
			obj.J_ode_p = optArgs.J_ode_p;
			obj.J_ode_q_f = optArgs.J_ode_q_f;
			obj.J_ode_q_d = optArgs.J_ode_q_d;
			obj.faultModel = optArgs.faultModel;
			obj.disturbanceModel = optArgs.disturbanceModel;
		end
		
	end % methods
	
	methods ( Access = public )
		
		function [kernel, timeDomain] = computeSolutionSetPointChange(obj, dt, T, optArg)
			arguments
				obj,
				dt,
				T,
				optArg.dynamicalExtension = 0;
				optArg.usePolyomial = false;
				optArg.silent = false;
			end
			
			% 			myProgressBar = misc.ProgressBar('name', "Fault diagnosis kernel solution", ...
			% 				'terminalValue',
			%
			t1 = tic;
			
			% check that T is seperable into dt
			if mod(T, dt) ~= 0
				warning('The transfer time T must be a multiple of dt. Thus it is changed to the next higher value');
				T = ceil( T / dt ) * dt;
			end
			
			%% Define the time domains:
			% the time-domain for the transition is separated into three
			% parts. These parts are obtained from the initial and end
			% value for the kernel m(z,tau). Thus, the parts are defined as
			%	I_1 = [-t_Delta_minus, t_Delta_plus]
			%	I_2 = (t_Delta_plus, T - t_Delta_minus)
			%	I_3 = [T - t_Delta_minus, t_Delta_plus]
			timeDomain = quantity.Domain('tau', 0:dt:T);
			
			I = quantity.Domain('tau', obj.theta_min:dt:T+obj.theta_max);
			I = I.split( [ obj.theta_max, T + obj.theta_min ] );
			
			if ~optArg.silent
				fprintf("initialization: %g seconds\n", toc(t1));
				t2 = tic;
			end
			
			% premilinary initializations:
			sImF = signals.PolynomialOperator( {-obj.F, eye(obj.n.w)} );
			adjF = adj(sImF);
			alpha = shiftdim( double( det(sImF) ) ).';
			S = double( adj(sImF) );
			Psi_I = obj.Psi(eye( obj.n.p ) * quantity.Discrete.ones(1, I(1)) , 0);
			
			W0 = int( S(:,:,1) * obj.L_1 * Psi_I, "z", 0, 1) + S(:,:,1) * obj.L_2 * Psi_I.at(0) ...
				+ S(:,:,1) * obj.L_3 * Psi_I.at(1);
			pinv_W0 = pinv(W0);


			[isInvertible, condition] = ...
				numeric.near( W0 * pinv_W0 * obj.eta_0, obj.eta_0, 4e-9);

			if ~optArg.silent
				fprintf("Condition of the detectability conditionis %g\n", condition);
			end
			assert( isInvertible, ...
				'Fault is not detectable. The compatibility condition is %d\n', condition);

			if ~optArg.silent
				fprintf("Computation of W_0: %g seconds\n", toc(t2));
			end	
			
			phi_I0 = quantity.Discrete.zeros( [ obj.n.p, 1], I(1));
			phi_I3 = quantity.Discrete.zeros( [ obj.n.p, 1], I(3));
			
			for i = 1:size(obj.eta_0, 2)
				
				% Check if the initial condition is a set point:
				% assert( all( obj.F * obj.eta_0(:,i) == 0 ), 'The initial condition for the kernel equations is not a set point' )
				
				% compute the initial value for the flat output
				phi_0 = pinv_W0 * obj.eta_0(:,i);
				
				phi_I1 = phi_0 * quantity.Discrete.ones(1, I(1));
				phi_I2 = quantity.Symbolic( stateSpace.setPointChange( phi_0, 0, I(2).lower, ...
					I(2).upper, obj.n.w - 1, 'var', I(2).name), I(2) );
				
				
				phi{1} = quantity.Piecewise({ phi_I1, phi_I2, phi_I3 }, ...
					'upperBoundaryIncluded', [true, false]);
				
				for k = 2:obj.n.w+1	
					phi{k} = quantity.Piecewise({ phi_I0, phi_I2.diff('tau', k-1), phi_I3 }, ...
						'upperBoundaryIncluded', [true, false]);
				end
				
				eta = quantity.Discrete.zeros( obj.n.w, timeDomain);
				m_tilde = quantity.Discrete.zeros( obj.n.n + obj.n.p, timeDomain);
				
				for k = 1:obj.n.w+1
					
					Psi_dtau_k_phi = obj.Psi( phi{k}, timeDomain );
					
					if k <= obj.n.w
						eta = eta + S(:,:,k) * ( ...
							int( obj.L_1  * Psi_dtau_k_phi, "z", 0, 1) ...
							+ obj.L_2 * subs(Psi_dtau_k_phi, "z", 0) ...
							+ obj.L_3 * subs(Psi_dtau_k_phi, "z", 1) );
					end
					P(:,i) = obj.J_ode_p * eta;
					Q_f(:,i) = obj.J_ode_q_f * eta;
									
					m_tilde = m_tilde + alpha(k) * Psi_dtau_k_phi;				
				end
				
				m_ = obj.backsteppingKernel.inverseBacksteppingTransformation(m_tilde);
				M(:, i) = m_.flipDomain("z");
				N(:,i) = obj.inverseTransformOfInputSignal(m_, P(:,i));
				
			end
			
			
			kernel.M_E = obj.computeModulatingFunction("E", M, P, N);
			kernel.M_B = obj.computeModulatingFunction("B", M, P, N);
			kernel.M_G = obj.computeModulatingFunction("G", M, P, N);
			kernel.M_G_bounded = ...
				obj.sys.G_bounded.' * kernel.M_G;
			kernel.M_G_deterministic = ...
				obj.sys.G_deterministic.' * kernel.M_G;
			kernel.M_G_stochastic = ...
				obj.sys.G_stochastic.' * kernel.M_G;
			kernel.M = M;
			kernel.N = N;
			kernel.P = P;
			kernel.Q_f = Q_f;
		end % compute solution_2
				
		function [kernel, timeDomain] = computeSolution(obj, dt, T, optArg)
			arguments
				obj,
				dt,
				T,
				optArg.dynamicalExtension = 0;
				optArg.usePolyomial = false;
				optArg.silent = false;
				optArg.WithInitialCondition = false;
			end
			
			% 			myProgressBar = misc.ProgressBar('name', "Fault diagnosis kernel solution", ...
			% 			'terminalValue',
			%
			t1 = tic;

			% check that T is seperable into dt
			if mod(T, dt) ~= 0
				warning('The transfer time T must be a multiple of dt. Thus it is changed to the next higher value');
				T = ceil( T / dt ) * dt;
			end
			
			%% Define the time domains:
			% the time-domain for the transition is separated into three
			% parts. These parts are obtained from the initial and end
			% value for the kernel m(z,tau). Thus, the parts are defined as
			%	I_1 = [-t_Delta_minus, t_Delta_plus]
			%	I_2 = (t_Delta_plus, T - t_Delta_minus)
			%	I_3 = [T - t_Delta_minus, t_Delta_plus]
			timeDomain = quantity.Domain('tau', 0:dt:T);
			
			I = quantity.Domain('tau', obj.theta_min:dt:T+obj.theta_max);
			I = I.split( [ obj.theta_max, T + obj.theta_min ] );
			
			%% initialize the xi-system
			I_np = eye(obj.n.p);
			
			% polynomial operator for adj(sI - F^T) and det(sI - F^T)
			sImF = signals.PolynomialOperator( {-obj.F, eye(obj.n.w)} );
			
			alpha = shiftdim( double( det(sImF) ) ).';
			S = double( adj(sImF) );
			
			% due to heteroDirectional.Kernel, n_w is n_eta from the notes
			
			%        /  0     1     0             \
			%        |  0     0     1             |
			% A_xi = |                            |
			%        |  0     0     0        1    |
			%        \-a_0  -a_1  -a_2 ... -a_n-1 /
			A_xi = [zeros(obj.n.w-1, 1), eye(obj.n.w-1); ...
				-alpha(1:end-1)];
			B_xi = [zeros(obj.n.w - 1, 1); ...
				1];
			
			n_star = optArg.dynamicalExtension;
			if n_star > 0
				A_star = [zeros(n_star-1, 1), eye(n_star - 1); ...
					zeros(1, n_star)];
				B_star = misc.unitVector(n_star, n_star);
				C_star = misc.unitVector(n_star, 1).';
				xi_model = ss([A_star, zeros(n_star, obj.n.w); ...
					B_xi * C_star, A_xi], ...
					[B_star; zeros(obj.n.w, 1)], ...
					misc.unitVector(n_star + obj.n.w, 1).', []);
			else
				xi_model = ss(A_xi, B_xi, zeros(1, obj.n.w), 1);
			end
			
			if optArg.WithInitialCondition
				% Phi(tau, T-tau^-) = expm(A * (tau - T + tau^-) )
				Phi_xi = expm( A_xi * quantity.Discrete(I(3).grid, I(3)));
				t_star = T;
			else
				% Phi(tau, -tau^-) = expm(A * (tau + tau^-) )
				Phi_xi = expm( A_xi * quantity.Discrete(I(1).grid, I(1)));
				t_star = 0;
			end
			
			W = zeros(obj.n.w, obj.n.w * obj.n.p);
			
			if ~optArg.silent
				fprintf("initialization: %g seconds\n", toc(t1));
				t2 = tic;
			end
			
			% compute all W_0 matrices:
			for i = 1:obj.n.w
				Psi_Phi_i = quantity.Discrete.zeros( [obj.n.x, obj.n.p*obj.n.w], obj.spatialDomain );
				for j = 1:obj.n.p
					idx = ((j-1)*obj.n.w + 1):j*obj.n.w;
					Phi_xi_i = misc.unitVector(obj.n.w, i)' * Phi_xi;			
					Psi_Phi_i(:, idx) = obj.Psi(Phi_xi_i, t_star, 'idx_j', j);
				end
				
				W = W ...
					+ int( S(:,:,i) * obj.L_1  * Psi_Phi_i, "z", 0, 1) ...
					+ S(:,:,i) * obj.L_2 * Psi_Phi_i.at(0) ...
					+ S(:,:,i) * obj.L_3 * Psi_Phi_i.at(1);
				
			end
			pinv_W = pinv(W);
			
			[isInvertible, condition] = ...
				numeric.near( W * pinv_W * obj.eta_0, obj.eta_0, 4e-9);
			
			if ~optArg.silent			
				fprintf("Condition of the detectability conditionis %g\n", condition);
			end
			assert( isInvertible, ...
				'Fault is not detectable. The compatibility condition is %d\n', condition);
			
			if ~optArg.silent
				fprintf("Computation of W_0: %g seconds\n", toc(t2));
			end
			
			% initialization for the following computations:
			mp0_I1 = quantity.Discrete.zeros( [obj.n.p, 1], I(1));
			mp0_I3 = quantity.Discrete.zeros( [obj.n.p, 1], I(3));
			J_whi = eye(obj.n.w * obj.n.p);
			
			tic_i = zeros(size(obj.eta_0, 2), 1, 'uint64');
			
			% #todo: Parallelisierung
			parfor i = 1:size(obj.eta_0, 2)
				
				if ~optArg.silent
					fprintf("Start computation of kernel %i\n", i);
					tic_i(i) = tic;
				end
				% compute the initial condition for the interval I1:
				if optArg.WithInitialCondition
					xi_t = - pinv_W * expm(obj.F * t_star) * obj.eta_0(:,i);
				else
					xi_t = pinv_W * obj.eta_0(:,i);
				end
				
				% initialization for the following computations:
				xi_I1_i = quantity.Discrete.zeros( [obj.n.p * obj.n.w, 1], I(1) );
				xi_I2_i = quantity.Discrete.zeros( [obj.n.p * obj.n.w, 1], I(2) );
				xi_I3_i = quantity.Discrete.zeros( [obj.n.p * obj.n.w, 1], I(3) );
				
				mp0_I2_i =quantity.Discrete.zeros( [obj.n.p, 1], I(2) );				
				
				
				% In contrary to the paper, the state space system for xi is considered in its sub
				% system. Since the overall system will lead to a system with a lot of states
				% n_w*n_p, the solutions are computed for each xi-subsystem separately to avoid
				% numerical problems with big state space systems.
				for j = 1:obj.n.p
					
					% Copmute the selection matrix for the corresponding subsystems:
					% xi_j = J_xi_j * xi
					J_xi_j = kron( misc.unitVector( obj.n.p, j )', eye(obj.n.w, obj.n.w));
					idx_j = obj.n.w*(j-1) + (1:obj.n.w);
					
					% Select the initial condition for the corresponding
					% subsystem:
					xi_t_j = J_xi_j * xi_t;
					% Compute the solution for the autonomous auxiliary
					% system on I1 or I3
					xi_j_I_star = Phi_xi * xi_t_j;
					
					% safe the solution for the j-th auxiliary system in
					% the solution vector for the entire auxiliary system.
					if optArg.WithInitialCondition
						xi_I3_i(idx_j, 1) = xi_j_I_star;
					else
						xi_I1_i(idx_j, 1) = xi_j_I_star;
					end
					% copmute the solution on the interval I2:
					% dt xi_i = A_xi xi_i(t) + B_xi m_p_i(0,t)
					
					% For I2, the solution xi_j and the
					% "input" m_p_j(0,t) is
					% obtained by the solution of the boundary value problem
					%	xi_j(t1) = xi_j_t1  ---> xi_j(t2) = xi_j_t2
					% for the j-th xi-ODE. This is solved using the gramian
					% controllability matrix to compute the m_p_j(0,t) with
					% minimal energy.
					
					% Define the initial and end value
					if optArg.WithInitialCondition
						xi_j_t1 = zeros(obj.n.w, 1);
						xi_j_t2 = xi_j_I_star.atIndex(1);
					else
						xi_j_t1 = xi_j_I_star.atIndex(end);
						xi_j_t2 = zeros(obj.n.w, 1);
					end
					
					% consider the dynamical extension:
					xi_j_t1_star = [zeros(n_star, 1); xi_j_t1];
					xi_j_t2_star = [zeros(n_star, 1); xi_j_t2];
					
					if optArg.usePolyomial
						[~, mp0_j_I2, xi_j_I2_star] = stateSpace.planPolynomialTrajectory( ...
							xi_model, I(2), ...
							'x0', xi_j_t1_star, 'x1', xi_j_t2_star);
					else
						[~, mp0_j_I2, xi_j_I2_star] = stateSpace.planTrajectory( ...
							xi_model, I(2), ...
							'x0', xi_j_t1_star, 'x1', xi_j_t2_star, "method", "Bernstein");
					end
					
					xi_j_I2 = [zeros(obj.n.w, n_star), eye(obj.n.w)] * xi_j_I2_star;
					
					mp0_I2_i(j) = mp0_j_I2;
					xi_I2_i(idx_j, 1) = xi_j_I2;
				end
				
				% Assembling of the solution for the three parts results in
				% the required solution. For the assembling it is important
				% to consider, that the boundary values are considered
				% correctly. Thus, the definition of the time domains I1,
				% I2, and I3 must be considered correctly:
				%	I1 = -
				xi = quantity.Piecewise({xi_I1_i, xi_I2_i, xi_I3_i}, ...
					'upperBoundaryIncluded', [true, false]);
				m_p0 = quantity.Piecewise({ mp0_I1, mp0_I2_i, mp0_I3}, ...
					'upperBoundaryIncluded', [true, false]);
				M_desired(:,i) = m_p0;
				Xi(:,i) = xi;
				
		
				% The kernel solution m_tilde(z,tau) is derived by
				%	m_tilde(z,tau) = Psi[ m_p(0,tau) ]
				% m_tilde is the solution in backstepping coordinates
				m_tilde = obj.Psi( m_p0, timeDomain );
				% From this, the input in backstepping coordinates n_tilde
				% is derived by
				%	n_tilde(tau) = m_tilde_plus(1, tau)
				% n_tilde is the boundary value aka "input" in the
				% backstepping coordinates
				n_tilde = obj.Jn * subs(m_tilde, "z", 1);
				
				% The solution of the ODE-part is computed by
				%	eta(tau) = sum_k=0^n_eta-1 S_k * (int_0^1 L_1(z)
				%		Psi[ dtau^k phi](z,tau) dz + L_2 J_w Psi[ dtau^k
				%		phi] (0,tau) + L_3 J_n Psi[ dtau^k phi] (1,tau))
				eta = quantity.Discrete.zeros( obj.n.w, timeDomain);
				
				for k = 1:obj.n.w
					
					J_k = J_whi(k + obj.n.w*(0:obj.n.p-1), :);
					
					Psi_dtau_k_phi = obj.Psi( J_k * xi, timeDomain );
					
					eta = eta + S(:,:,k) * ( ...
						int( obj.L_1  * Psi_dtau_k_phi, "z", 0, 1) ...
						+ obj.L_2 * subs(Psi_dtau_k_phi, "z", 0) ...
						+ obj.L_3 * subs(Psi_dtau_k_phi, "z", 1) );
					
				end
				% select the ode states corresponding to the system ode part of the kernel equations.
				P(:,i) = obj.J_ode_p * eta;
				Q_f(:,i) = obj.J_ode_q_f * eta;
				
				% compute the kernel solutions in the original coordinates:
				% for this
				%	1) apply inverse backstepping transformation:
				m_ = obj.backsteppingKernel.inverseBacksteppingTransformation(m_tilde);
				%	2) apply inversion of the spatial coordinate:
				M(:, i) = m_.flipDomain("z");
				
				% The input n for the system in original coordinates is
				% obtained by
				%	n(tau) = n_tilde + Q_0.' * J_n * ( m_tilde(1, tau) +
				%		+ int_0^1 K_I(1,zeta) * m_tilde(zeta, tau) d zeta
				N(:,i) = obj.inverseTransformOfInputSignal(m_, P(:,i));
				
				if ~optArg.silent
					fprintf("Computation of kernel %i: %g seconds\n", i, toc(tic_i(i)));
					tic
				end
			end
			
			% set the names:
			M_desired.setName("M_desired");
			Xi.setName("Xi");
			M.setName("M");
			N.setName("N");
			P.setName("P");
			Q_f.setName("Q_f");		
			
			kernel.M_desired = M_desired;
			kernel.Xi = Xi;
			kernel.M = M;
			kernel.N = N;
			kernel.P = P;
			kernel.Q_f = Q_f;
		end % compute solution
		
		function [filters, modulatingFunctions, kernels] = computeIdentificationFilter(obj, I, optArgs)
			arguments
				obj
				I (1,1) quantity.EquidistantDomain
				optArgs.bounds (:,1) double = ones(obj.sys.n.d_bounded,1)
				optArgs.dynamicalExtension (1,1) double = 0
				optArgs.WithInitialCondition (1,1) logical = false
				optArgs.firApproximation (1,1) string = "trapz"
				optArgs.kernels 
			end			
			
			% copmute identification kernels:
			if isfield(optArgs, "kernels")
				kernels = optArgs.kernels;
			else
				kernels = obj.computeSolution( I.stepSize, I.upper, ...
					"dynamicalExtension", optArgs.dynamicalExtension, ...
					"WithInitialCondition", optArgs.WithInitialCondition);
			end
				
			% compute the modulating functions
			modulatingFunctions.Mf = - obj.computeModulatingFunction("E", kernels.M, kernels.P, kernels.N);
			modulatingFunctions.Mu = obj.computeModulatingFunction("B", kernels.M, kernels.P, kernels.N);
			modulatingFunctions.Md = obj.computeModulatingFunction("G", kernels.M, kernels.P, kernels.N);
			modulatingFunctions.Md_bounded = ...
				obj.sys.G_bounded.' * modulatingFunctions.Md;
			modulatingFunctions.Md_deterministic = ...
				obj.sys.G_deterministic.' * modulatingFunctions.Md;
			modulatingFunctions.Md_stochastic = ...
				obj.sys.G_stochastic.' * modulatingFunctions.Md;			
			
			% compute the time-discrete FIR filters
			if optArgs.firApproximation == "firstOrderHold"
				filters.N = signals.fir.firstOrderHold(kernels.N.', I, 'flip', false);
				filters.Mu = signals.fir.firstOrderHold(modulatingFunctions.Mu.', I, 'flip', false);
				filters.Mf = signals.fir.firstOrderHold(modulatingFunctions.Mf.', I, 'flip', false);
				filters.Md_bounded = ...
					signals.fir.firstOrderHold(modulatingFunctions.Md_bounded.', I, 'flip', false);
			elseif optArgs.firApproximation == "trapz"
				filters.N = signals.fir.trapez(kernels.N.', I, 'flip', false);
				filters.Mu = signals.fir.trapez(modulatingFunctions.Mu.', I, 'flip', false);
				filters.Mf = signals.fir.trapez(modulatingFunctions.Mf.', I, 'flip', false);				
				filters.Md_bounded = ...
					signals.fir.trapez(modulatingFunctions.Md_bounded.', I, 'flip', false);
			else
				error("FIR approximation method " + optArgs.firApproximation + " is not known.")
			end
			filters.threshold = obj.thresholdVectOfAbsValues( modulatingFunctions.Md_bounded, optArgs.bounds);
			filters.dt = I.stepSize;
			filters.T = I.upper;
			filters.type = "diagnosis";
		end
		
		function fS = thresholdStochastic(obj, M_G_stochastic, vectorOfVariances, dt, P)
			
			S0 = dt * diag(vectorOfVariances);
			variance_f = diag( double( int( M_G_stochastic.' * S0 * M_G_stochastic ) ) );
			F_stochastic = signals.WhiteGaussianNoise( sqrt( variance_f ) );
			
			fS = F_stochastic.calculateThresholdForProbability(ones(obj.n.f,1) * P);
			
		end
		
		function fB = thresholdVectOfAbsValues(obj, M_G_bounded, d_bound)
			fB = int( abs(M_G_bounded) ).' * d_bound;
		end
		
		function fB = thresholdSchwarzIneq(obj, M_G_bounded, d_bound)
			T = M_G_bounded(1).domain.upper;
			fB = sqrt(T) * sqrt(int( M_G_bounded.^2 )).' * d_bound;
			
		end
		
		
		function M_ = computeModulatingFunction(obj, name, M, P, N)
			
			M_ = int( obj.sys.(name + "_1").' * M, "z", 0, 1) ...
				+ obj.sys.(name + "b0").' * subs( obj.Jp * M, "z", 0 ) ...
				- obj.sys.(name + "b1").' * subs( obj.Jn * M, "z", 1 ) ...
				+ obj.sys.(name + "_3").' * P ...
				- obj.sys.(name + "_4").' * N;
			
		end
		
		function n = inverseTransformOfInputSignal(obj, m_, p)
			%INVERSETRANSFORMOFINPUTSIGNAL
			% n = inverseTransformOfInputSignal(obj, n_tilde, m_tilde)
			% computes the transformation of the input signal and state in
			% backstepping coordinates to the input signal in the original
			% coordinates.
			% The input n for the system in original coordinates is
			% obtained by
			%	n(tau) = n_tilde + Q_0.' * J_n * ( m_tilde(1, tau) +
			%		+ int_0^1 K_I(1,zeta) * m_tilde(zeta, tau) d zeta
			
			n = int( obj.sys.A_0.flipDomain("z").' * m_, "z", 0, 1) + ...
				( obj.Jn + obj.sys.Q_0.' * obj.Jp ) ...
				* subs(m_, "z", 1) ...
				+ obj.Jn * obj.sys.L_2.' * p;
		end
		
		function verifyKernelSolution(obj, originalSystem, faultModel, Mu, N, I, sol_tilde, optArgs)
			arguments
				obj
				originalSystem
				faultModel
				Mu
				N
				I
				sol_tilde
				optArgs.AbsTol = 1e-14;
				optArgs.backsteppingKernel;
			end
			
			O = quantity.Discrete.zeros([originalSystem.n_u, ...
				originalSystem.n_f], obj.spatialDomain);
			T = I.upper;
			
			fprintf("# Verify the boundary conditions of the modulating functions\n");
			misc.warning( numeric.near( subs(Mu, "tau", 0), 0), ...
				"initial condition for M_u not satisfied" );
			misc.warning( numeric.near( subs(Mu, "tau", T), 0), ...
				"end condition for M_u not satisfied" );
			
			fprintf("# Verify the boundary conditions of the solution in backstepping coordinats\n");
			misc.warning( near( subs(sol_tilde.m, "tau", 0), O, optArgs.AbsTol), "initial condition of modulating function not satisfied" );
			misc.warning( near( subs(sol_tilde.m, "tau", T), O, optArgs.AbsTol), "end condition of modulating function not satisfied" );
			
			fprintf("# Verify the solution in the backstepping coordinates\n");
			w = [sol_tilde.p; sol_tilde.q];
			[~, ~, sim_backstepping] = obj.verifySolution(...
				subs(sol_tilde.m, 'tau', 't'), ...
				subs(w, 'tau', 't'), ...
				subs(sol_tilde.n, 'tau', 't'), ...
				'w0', obj.eta_0);
			
			obj.verifyInitialEndCondition(sim_backstepping, ...
				T, O, O, obj.eta_0, zeros(obj.n.w, obj.n.f))
			
			fprintf("# Verify the solution in the original coordinates\n")
			fKernelOrgCo = ...
				originalSystem.faultDiagnosisKernelOriginalCoordinates(	faultModel.sys );
			
			% transform the planned solution into the original coordinates:
			if isfield(optArgs, 'backsteppingKernel')
				m = optArgs.backsteppingKernel.inverseBacksteppingTransformation(...
					sol_tilde.m);
				[~,~, sim_original] = fKernelOrgCo.verifySolution(...
					subs(m, 'tau', 't'), ...
					subs(w, 'tau', 't'), ...
					subs(N, 'tau', 't'), ...
					'w0', obj.eta_0);
			else
				sim_original = fKernelOrgCo.simulate(I, 'u', N, 'w0', obj.eta_0);
			end
			
			obj.verifyInitialEndCondition(sim_original, T, O, O, obj.eta_0, zeros(obj.n.w, obj.n.f))
			
			fprintf("--------------------------------------------\n # done\n\n")
		end % verifyKernelSolution
		
	end % methods (Access = public)
	
	methods ( Access = protected)
		function x = Psi(obj, h, tau, optArg)
			% Psi-operator
			% x = Psi(obj, h) compute the operator
			%	x = Psi[h(.)](z,t)
			arguments
				obj,
				h,
				tau
				optArg.idx_j = 1:obj.n.p;
			end
			
			if isnumeric(tau)
				tau = quantity.Domain("tau", tau);
			end
			
			V_p = obj.Jn.' + obj.Jp.' * obj.Q_0;
			
			x = quantity.Discrete.zeros( [obj.n.x, size(h, 2) ], [obj.spatialDomain, tau]);
			
			tau_plus_int_Lambda = ...
				ones(obj.n.x, 1) * quantity.Discrete(tau.grid, tau) ...
				+ obj.Int_zeta_z_Lambda_1_dz;
			
			A_0_zeta = subs(obj.A_0, "z", "zeta");
			
			for i = 1:obj.n.x
				for j = optArg.idx_j
					
					if numel(optArg.idx_j) == 1
						h_idx = 1;
					else
						h_idx = j;
					end
					
					h_j = h(h_idx,:).compose( tau_plus_int_Lambda(i) );
					
					A0_ij_h_j = A_0_zeta(i,j) * h_j;
					
					x(i,:) = x(i,:) + V_p(i,j) * subs(h_j, "zeta", 0) ...
						+ int( A0_ij_h_j, "zeta", 0, "z" );
				end
			end
			
			if tau.n == 1
				x = subs(x, tau.name, tau.grid);
			end
		end %Psi
	end % methods (Access = protected)
	
	methods (Static)
		
		%TODO make this static and protected
		out = solveKernel_i(varargin)
		
		% #TODO: this should not be required...
		function [targetSystem, K] = ...
				decouplingTransformation(originalSystem, varargin)
			% compute the backstepping transformation for the decoupling of
			% the pde from the ode
			
			% solve kernel equations
			K = kernel.Feedback(...
				originalSystem.systemOde.Lambda, ...
				'A', originalSystem.systemOde.A, ...
				'F', originalSystem.systemOde.F, ...
				'A0', originalSystem.systemOde.A0, ...
				'Q0', originalSystem.systemOde.Q0, ...
				'name', 'K', ...
				varargin{:});
			
			targetSystem = K.getTargetSystem(originalSystem.systemOde);
			
			targetSystem = ...
				faultdiagnosis.hyperbolic.heteroDirectional.System(targetSystem);
		end
		
	end
	
end

