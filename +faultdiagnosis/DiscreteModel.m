classdef DiscreteModel
    %DISCRETEMODEL Lumped parameter system as an approximation of a
    % distributed-parameter systems
    %
    %   Basisclass for the discretization of a distributed-parameter
    %   systems. The DPS is spatially discretized and then displayed as
    %   state space system
	
    properties
        % System that is discretized
		sys faultdiagnosis.System = faultdiagnosis.System.empty();
		
		% Statespace object for discretized dps
        stsp ss;
		
		% spatial domain with the discretization of the approximation
		discreteDomain (1,1) quantity.Domain;
		
		% output matrix to extract the PDE states:
		%     v(z_i,t) = C_pde * x(t)
		% with v(z_i,t) as pde system variable at discretization points z_i and x as state of the
		% discrete model.
		pdeStates double;
		
		% output matrix to extract the ODE states:
		%	w(t) = C_ode * x(t)
		% with w(t) as state of the original ode and x(t) as state of the discrete model. 
		odeStates double;
		
		% spatial derivative matrix:
		Dz double;
		
		parameters struct;
		
		% TODO: names = struct("pde", "distributed", "ode", ...)
	end
	
	properties (Dependent)
		% structure for the number of system states:
		n struct;
	end
	
	methods
		function obj = DiscreteModel(sys, stsp, discreteDomain, optArgs)
			%DISCRETEMODEL A finite-dimensional model for a DPS
			arguments
				sys faultdiagnosis.System;
				stsp ss;
				discreteDomain (1,1) quantity.Domain;
				optArgs.pdeStates double;
				optArgs.odeStates double;
				optArgs.Dz double;
				optArgs.parameters struct;
			end
			obj.sys = sys;
			obj.stsp = stsp;
			
			obj.discreteDomain = discreteDomain;
			
			if isfield(optArgs, "pdeStates")
				obj.pdeStates = optArgs.pdeStates;
			else
				obj.pdeStates = eye( size(obj.stsp.A) );
			end
			
			if isfield(optArgs, "odeStates")
				obj.odeStates = optArgs.odeStates;
			else
				obj.odeStates = [];
			end
			
			if isfield(optArgs, "Dz")
				obj.Dz = optArgs.Dz;
			else
				obj.Dz = [];
			end
			
			if isfield(optArgs, "parameters")
				obj.parameters = optArgs.parameters;
			end
			
		end
		function n = get.n(obj)
			
			n.all = size( obj.stsp.A, 1 );
			n.ode = size( obj.odeStates, 1);
			n.pde = n.all - n.ode;
			
		end
	end
	
	methods ( Access = public )
		
		function results = simulate(obj, t, optArgs)
			%SIMULATE Runs a simulation for the system
			%   [results] = simulate(obj, t, optArgs) simulates the system on the time-domain t and
			%   inputs that can be specified by the name-value-pairs: "u", "f", "d_deterministic",
			%   "d_bounded", "d_stoachastic". The initial condition can be set by name-value-pair
			%   "x0". With the name-value-pair "plot" it can be defined if the result should be
			%   plotted directly into the figure given by "fig".
			%
			arguments
				obj
				t (1,1) quantity.Domain
				optArgs.u (:,1) quantity.Discrete
				optArgs.f (:,1) quantity.Discrete
				optArgs.d_deterministic (:,1) quantity.Discrete
				optArgs.d_bounded (:,1) quantity.Discrete
				optArgs.d_stochastic (:,1) quantity.Discrete;
				optArgs.x0 = zeros( obj.n.all, 1);
				optArgs.fig;
				optArgs.plot = false;
			end
						
			ip = misc.Parser();
			ip.addParameter("u", quantity.Discrete.zeros( [obj.sys.n.u, 1], t ), ...
				@(a) validateattributes(a, {'quantity.Discrete'}, {'size', [obj.sys.n.u, 1]}));
			ip.addParameter("f", quantity.Discrete.zeros( [obj.sys.n.f, 1], t ), ...
				@(a) validateattributes(a, {'quantity.Discrete'}, {'size', [obj.sys.n.f, 1]}));
			ip.addParameter("d_deterministic", quantity.Discrete.zeros( [obj.sys.n.d_deterministic, 1], t ), ...
				@(a) validateattributes(a, {'quantity.Discrete'}, {'size', [obj.sys.n.d_deterministic, 1]}));
			ip.addParameter("d_bounded", quantity.Discrete.zeros( [obj.sys.n.d_bounded, 1], t ), ...
				@(a) validateattributes(a, {'quantity.Discrete'}, {'size', [obj.sys.n.d_bounded, 1]}));
			ip.addParameter("d_stochastic", quantity.Discrete.zeros( [obj.sys.n.d_stochastic, 1], t ), ...
				@(a) validateattributes(a, {'quantity.Discrete'}, {'size', [obj.sys.n.d_stochastic, 1]}));
			varargin = namedargs2cell( optArgs );
			ip.parse(varargin{:});

			% combine the disturbances to a common disturbance vector:
			d.total = quantity.Discrete.zeros( [obj.sys.n.d, 1], t);
			
			if obj.sys.n.d_deterministic > 0
				d.deterministic = ip.Results.d_deterministic;
				d.total = d.total + obj.sys.G_deterministic * d.deterministic;
			end
			
			if obj.sys.n.d_bounded > 0
				d.bounded = ip.Results.d_bounded;
				d.total = d.total + obj.sys.G_bounded * d.bounded;
			end
			
			if obj.sys.n.d_stochastic > 0
				d.stochastic = ip.Results.d_stochastic;
				d.total = d.total + obj.sys.G_stochastic * d.stochastic;
			end
			
			% call the solve function
			results = obj.solve(t, [ip.Results.u; d.total; ip.Results.f], optArgs.x0);
			
			results.u = results.inputs(1:obj.sys.n.u);
			results.d = d;
			results.f = results.inputs(obj.sys.n.u+obj.sys.n.d+1:end);
			
			% set the signal names
			results.u.setName("u");
			if isfield( results.d, "deterministic")
				results.d.deterministic.setName("d_deterministic");
			end
			if isfield( results.d, "bounded")
				results.d.bounded.setName("d_bounded");
			end
			results.d.total.setName("d");
			results.f.setName("f");
			results.y.setName("y");
			results.v.setName("v");
			if obj.sys.n.w > 0
				results.w.setName("w");
			end
			
			% plot the results if required
			if nargout == 0 || optArgs.plot
				
				if ~isfield( optArgs, "fig")
					optArgs.fig = figure().Number;
				end
				
				obj.plotResults(results, 'fig', optArgs.fig, 'parent', optArgs);
			end
		end		
		
		function step(obj, t)
			arguments
				obj
				t (1,1) quantity.Domain = quantity.Domain("t", linspace(0, 1));
			end			
			obj.simulate(t, 'u', quantity.Discrete.ones( [obj.sys.n.u, 1], t));
		end		
		function plotResults(obj, results, optArgs)
			arguments
				obj,
				results,
				optArgs.fig (1,1) double = figure().Number,
				optArgs.parent,
			end
			
			% use "set(groot, 'CurrentFigure', optArgs.fig)" instead of figure(.), because, the
			% latter steals the focus.
			set(groot, 'CurrentFigure', optArgs.fig); clf; 
					
			plotOps = {'figureId', optArgs.fig, 'smash', true, 'hold', true, 'dock', false};
			subplot(4,1,2);
			results.y.plot(plotOps{:});
			xlabel('t')
			ylabel('y')
			
			if obj.sys.n.w > 0
				subplot(4,1,3);
				results.v.plot(plotOps{:});
				zlabel('v');
				xlabel('z');
				ylabel('t');
				
				subplot(4,1,4);
				results.w.plot(plotOps{:});
				xlabel('t');
				ylabel("w");
				
			else
				subplot(4,1,[3, 4]);
				results.v.plot(plotOps{:});
				zlabel('x');
				xlabel('z');
				ylabel('t');
			end
			
			subplot(4,1,1);hold all
			labels = {};
						
			if isfield( optArgs.parent, "u" )
				optArgs.parent.u.plot(plotOps{:});
				labels = misc.appendLegendLabels(labels, optArgs.parent.u, 'u');
			end
			
			if isfield(optArgs.parent, "d_deterministic")
				optArgs.parent.d_deterministic.plot(plotOps{:});
				labels = misc.appendLegendLabels(labels, optArgs.parent.d_deterministic, 'd_deterministic');
			end
			
			if isfield( optArgs.parent, "d_bounded")
				optArgs.parent.d_bounded.plot(plotOps{:});
				labels = misc.appendLegendLabels(labels, optArgs.parent.d_bounded, 'd_bounded');
			end
			
			if isfield( optArgs.parent, "d_stochastic")
				optArgs.parent.d_stochastic.plot(plotOps{:});
				labels = misc.appendLegendLabels(labels, optArgs.parent.d_stochastic, 'd_stochastic');
			end
			
			if isfield( optArgs.parent, "f")
				optArgs.parent.f.plot(plotOps{:});
				labels = misc.appendLegendLabels(labels, optArgs.parent.f, 'f');
			end
						
			xlabel('t')
			legend(labels);
			
		end
		
	end
	
	methods (Access = protected)
		
		function results = solve(obj, t, u, x0)			
			arguments
				obj
				t (1,1) quantity.Domain;
				u (:,1) quantity.Discrete;
				x0 (:,1) double;
			end
			[y, ~, xi] = lsim(obj.stsp, u.on(t), t.grid, x0);
			results.y = quantity.Discrete( y, t);
			results.v = quantity.Discrete( obj.pdeStates * xi.', [obj.discreteDomain, t]);
			% use the numerical differntiation of quantity.Discrete because it leads to better
			% results than the FEM matrix version.
			results.dzx = results.v.diff( obj.discreteDomain, 1 );
			%results.dz = quantity.Discrete( obj.Dz * results.x.on(), [obj.discreteDomain, t]);

			if obj.sys.n.w > 0
				results.w = quantity.Discrete( ( obj.odeStates * xi.' ).', t);
			end
			results.t = t;
			results.inputs = u;			
		end

	end	
	
end

