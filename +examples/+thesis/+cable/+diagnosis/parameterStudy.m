%% parameter study
% investiagte the influence of the moving horizon length on the threshold value and the detection
% delay
init
% define if the results should be plotted, if additional information should be printed or the
% results should be saved
flags.plot = false;
flags.debug = false;
flags.save = true;
% define properties for the parameter study
studySetup.tic = tic;					% for time measurement
studySetup.Nz = 301;					% number of spatial discretization points
studySetup.backsteppingTol = 1e-6;		% tolerance to stop the fixpoint iteration of the 
										% computation of the backstepping kernels
% define the cable system
[faultyCable, waveCable, ~, ~, sysParas, cableParameters] = ...
	examples.thesis.cable.wave2heteroDirectionalSystem(studySetup.Nz, ...
	"szenario", "diagnosis");
% initialize the input signals 
dt = 1e-3;
examples.thesis.cable.diagnosis.inputs

% compute the integral kernels for the fault diagnosis
kernel = faultdiagnosis.hyperbolic.heteroDirectional.DiagnosisKernel( faultyCable, in.model.f, ...
	"disturbanceModel", in.model.d, "numberOfLines", studySetup.Nz, ...
	"tolerance", studySetup.backsteppingTol);

kernel.verbose = false;

% define the parameter study values
studySetup.T.numel = 100;
studySetup.T.start = 6;
studySetup.T.stop = 12;
studySetup.T.values = logspace( log10( studySetup.T.start), log10( studySetup.T.stop ), studySetup.T.numel );

studySetup.dBounds = in.d.bounds;
studySetup.Ntau = 701;
identifier = "701";			% an identifier for the filename of the study results

kernel.report( sprintf( ...
	"Start of parameter study for the cable with T from T in {%g ... %g} with %g values", ...
	studySetup.T.start, studySetup.T.stop, studySetup.T.numel) );

%% normalized faults
normalizedFaults = in.model.f;
[normalizedFaults.occurrence] = deal(0);

normalizedFaults(1).initialCondition = [5; 2];
normalizedFaults(2).initialCondition = [5; 2];
normalizedFaults(3).initialCondition = [1; 1];

studySetup.normalizedFaults = normalizedFaults;

%% do the parameter study
clear study
study(1:studySetup.T.numel) = struct();
exs = [];
for k = 1:studySetup.T.numel
	% define the time domain for the integral kernels
	tau = quantity.EquidistantDomain("tau", 0, studySetup.T.values(k), ...
		"stepNumber", studySetup.Ntau);
	% initialize some local variables
	local = struct();
	tLocal = tic;
	local.I = tau;
	
	kernel.report( sprintf( ...
		"--- k = %i --- : Compute the kernels for T = %g ", ...
		k, studySetup.T.values(k)));
	try		% with the try-catch-block the parameter study should be robust to errors
		
		ioKernels = kernel.computeSolution(local.I, "delta", studySetup.dBounds);
				
		local.f = studySetup.normalizedFaults.simulate("time", local.I);
		
		local.fir = kernel.generateFirFilters(local.I, ioKernels, "bounds", in.d.bounds, ...
			"type", "diagnosis");

		local.rAll = signals.fir.mFilter( local.fir.Mf, local.f );
		
		% compute the residual signal for each normalized fault and the resulting detection delay
		for m = 1:kernel.sys.n.f
			
			local.r(:,m) = signals.fir.mFilter( local.fir.Mf, ...
												misc.unitSelector(kernel.sys.n.f, m) * local.f );
			
			for n = 1:kernel.sys.n.f
				% find the detection delay for separate excitation
				resi = on( abs( local.r(n,m) ) );
				idx = find( resi >= local.fir.threshold(n),1, "first" );
				if isempty( idx )
					local.detectionDelays(m,n) = nan;
				else
					local.detectionDelays(m,n) = local.I.grid(idx);
				end
			end
			local.detectionDelay(m) = min( local.detectionDelays(m,:) );
			
			% find the detection delay for excitation with all faults
			resiAll = on( abs( local.rAll(m) ) );
			idxAll = find( resiAll >= local.fir.threshold(m),1, "first" );
			if isempty( idxAll )
				local.detectionDelaysAll(m) = nan;
			else
				local.detectionDelaysAll(m) = local.I.grid(idxAll);
			end
		end %m = 1:kernel.sys.n.f
		
		% put the parameter study results in the result structure
		study(k).T = studySetup.T.values(k);
		study(k).ioKernels = ioKernels;
		study(k).I = local.I;
		study(k).k = k;
		study(k).threshold = local.fir.threshold(:);
		study(k).detectionDelay = local.detectionDelay(:);
		study(k).r = local.r;
		
	catch ex
		% if an error has occurred, then print the error but continue the parameter study
		disp(ex);
		ex = ex.addCause(MException("STUDY:err", "Error in iteration k=%i, l=%i, with T = %g", ...
			k, studySetup.T.values(k)));
		disp(ex.cause{1});
		exs = [exs, ex];
	end %try
	
	% save the study result for the parameter set corresponding to "k" 
	studyK = study(k);
	saveName = "build/cable/cableDiagnosisStudy_" + identifier + "_" + num2str(k) + "_" + ...
					datestr(now, 'yyyymmddTHHMMSS');
	save(saveName, "studyK", "studySetup");
	fprintf("Saved file %s \n", saveName)
end % k = 1:studySetup.T.numel

studySetup.toc = toc(studySetup.tic);
fprintf("Elapsed time %g min \n", studySetup.toc / 60);

% save the overall parameter study result 
if ~isfolder("build/cable")
	mkdir("build/cable")
end

saveName = "build/cable/cableDiagnosisStudy_" + identifier + "_" + datestr(now, 'yyyymmddTHHMMSS');
save(saveName, "study", "studySetup");
fprintf("Saved file %s \n", saveName)

%% visualization of the results
if flags.plot
	nf = 3;
	figure();clear ax1
	pltIdxThreshold = [1, 3, 5];
	pltIdxDelay = [2, 4, 6];

	% plot the resulting threshold and detection delay
	for i = 1:nf
		subplot(3,2,pltIdxThreshold(i));
		ax1(:,i) = plot( studySetup.T.values, misc.unitVector(3,i)' * [study.threshold], "LineWidth", 2);
		xlabel("$T$", "Interpreter", "latex")
		ylabel("$f_{B," + i +"}$", "Interpreter", "latex");
		grid on;
		
		subplot(3,2,pltIdxDelay(i));
		ax1(:,i) = plot( studySetup.T.values, misc.unitVector(3,i)' * [study.detectionDelay], "LineWidth", 2);
		xlabel("$T$", "Interpreter", "latex")
		ylabel("$\Delta_{f," + i +"}$", "Interpreter", "latex");
		grid on;
	end

	% plot the resulting residual signal for the normalized faults 
	for k = 1:studySetup.T.numel
		thresholdsSignals = study(k).threshold * quantity.Discrete.ones( [1,3], study(k).I);
		study(k).r.plot;
		thresholdsSignals.plot("hold", true, "figureId", 0);
		plot(-thresholdsSignals, "hold", true, "figureId", 0);
	end	
end % flags.plot

%% export the results
if flags.doTheExport
	
	studyData.study = export.dd("M", [studySetup.T.values', [study.threshold]', [study.detectionDelay]'], ...
		"header", ["T", "fB1", "fB2", "fB3", "delta1", "delta2", "delta3"], "filename", "diagnosisStudy", ...
		exportOptions{:});
	
	studyParameters.f101 = studySetup.normalizedFaults(1).initialCondition(1);
	studyParameters.f102 = studySetup.normalizedFaults(1).initialCondition(2);
	studyParameters.f201 = studySetup.normalizedFaults(2).initialCondition(1);
	studyParameters.f202 = studySetup.normalizedFaults(2).initialCondition(2);
	studyParameters.f301 = studySetup.normalizedFaults(3).initialCondition(1);
	studyParameters.f302 = studySetup.normalizedFaults(3).initialCondition(2);

	studyData.parameters = export.namevalues.struct2namevalues( studyParameters, ...
		'filename', 'diagnosisStudyParameters', exportOptions{:});
	
	export.Data.exportAll( studyData )
end % flags.doTheExport
