t = quantity.EquidistantDomain("t", 0, 120, "stepSize", dt); % 200, 5e-3;
% at first call the inputs for the detection and then overwrite the faults:
examples.thesis.cable.inputs

% faults
in.on = [20, 50, 80];
in.off = [120, 120, 120];
in.model.f(1) = signals.models.Sinus(2, 'time', t, ...
	'occurrence', in.on(1), "offTime", in.off(1), 'initialCondition', [0.5; 0], 'SignalName', 'f1');

in.model.f(2) = signals.models.Sinus(0.5, 'time', t, ...
	'occurrence', in.on(2), "offTime", in.off(2), 'initialCondition', [0.5; 0], 'SignalName', 'f2');
% in.model.f(2) = signals.models.Constant(5e3, 'time', t, ...
% 	'occurrence', in.on(2), "offTime", in.off(2), "SignalName", 'f2');

in.model.f(3) = signals.models.Sinus(1, "time", t, ...
	'occurrence', in.on(3), "offTime", in.off(3), 'initialCondition', [0.15; 0], 'SignalName', 'f3');

% in.model.f(3) = signals.models.Constant(10, 'time', t, ...
% 	'occurrence', in.on(3), "offTime", in.off(3), "SignalName", 'f2');

in.f = in.model.f.simulate();
