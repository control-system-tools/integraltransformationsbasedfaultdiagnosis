init
flags.plot = true;
flags.debug = true;
flags.export = true;
exportOptions = {'basepath', '/home/fischer/text/diss/', 'foldername', 'data/cable/diagnosis'};

Nz = 301;
[faultyCable, waveCable, ~, ~, sysParas, cableParameters] = ...
	examples.thesis.cable.wave2heteroDirectionalSystem(Nz, ...
	"szenario", "diagnosis");
%%
dt = 1e-3;
examples.thesis.cable.diagnosis.inputs;

nCheb = 51;
%% undisturbed
fprintf("# Simulation of the system\n")
noiseFree = waveCable.simulate("t", t.grid, "plant.u", in.u, "plant.f", in.f, ...
	"plant.d.ode", in.d.ode, "zPointSimulation", nCheb);
if flags.plot
	examples.thesis.cable.plotResults(noiseFree, "figureId", 3);
end

noiseFree.plant.u.setName("u");
noiseFree.plant.f.setName("f");
noiseFree.plant.d.ode.setName("dOde");

%% 
% this is the same kernel as used for the fault detection:
backsteppingTol = 1e-6;
kernel = faultdiagnosis.hyperbolic.heteroDirectional.DiagnosisKernel( faultyCable, in.model.f, ...
	"disturbanceModel", in.model.d, "numberOfLines", Nz, "tolerance", backsteppingTol);

%%
computation.T = 8.14;
Ntau = 701; % parameters 401 [x]
% Ntau = 701;
fprintf("Computation for Ntau = %i\n", Ntau);
computation.I = quantity.EquidistantDomain("tau", 0, computation.T, "stepNumber", Ntau);
% computation.I = quantity.EquidistantDomain("tau", 0, computation.T, "stepSize", dt);

[ioKernel, measures] = kernel.computeSolution(computation.I, "delta", in.d.bounds);
ioKernel.My.setName("My")
%%
detection.dt = 1e-2;
detection.I = quantity.EquidistantDomain("tau", 0, computation.T, "stepSize", detection.dt);
detection.t = quantity.EquidistantDomain("t", 0, t.upper, "stepSize", detection.dt);

firFilters = kernel.generateFirFilters(detection.I, ioKernel, "bounds", in.d.bounds, ...
					"type", "diagnosis", "discretizationFunction", @signals.fir.firstOrderHold);
%
fprintf("# Fault identification\n")
[identification.r, identification.result] = ...
	kernel.firFaultDiagnosis( firFilters, ...
	noiseFree.plant.y.subs("t", detection.t), ...
	noiseFree.plant.u.subs("t", detection.t), ...
	"f", noiseFree.plant.f.subs("t", detection.t), ...
	"occurrence", [in.model.f.occurrence]);

kernel.plotResult( identification.r, identification.result, "diagnosis", ...
	computation.T, "f", noiseFree.plant.f, "occurrence", [in.model.f.occurrence]);

%% disturbed

%% worst case bounded disturbance
dftau = diag( in.d.bounds ) * sign( ioKernel.Md_bounded ) * 0.9;

% build the worst case disturbances
tDf = quantity.EquidistantDomain("t", 0, detection.I.upper, "stepSize", t.stepSize);
dft = dftau.subs("tau", tDf).flipDomain("t");

d_bounded_value = [];
nSlices = ceil( t.upper / detection.I.upper / 3 );
rng(5), rSign = sign( randn(nSlices*3,1) );

for k = 1:nSlices
	d_bounded_value = [d_bounded_value; ...
						rSign(1*k) * dft(:,1).on(); ...
						rSign(2*k) * dft(:,2).on(); ...
						rSign(3*k) * dft(:,3).on()];
end

in.d.bounded = quantity.Discrete( d_bounded_value(1:t.n, :), t, "name", "d_bounded");

df2 = signals.fir.mFilter( firFilters.Md_bounded, in.d.bounded.subs("t", detection.t ) );
% plot(df2, "figureId", 5, "smash", true); hold on;
% plot(detection.t.grid, ones(detection.t.n,1) * firFilters.threshold')
% plot(detection.t.grid, -ones(detection.t.n,1) * firFilters.threshold')


%%
fprintf("# Simulation of the system\n")
noisy = waveCable.simulate("t", t.grid, "plant.u", in.u, "plant.f", in.f, ...
	"plant.d.ode", in.d.ode, "plant.d.bounded", in.d.bounded, "zPointSimulation", nCheb);
if flags.plot
	examples.thesis.cable.plotResults(noisy, "figureId", 4);
end
noisy.plant.d.bounded.setName("dBounded");

%%
fprintf("# Fault identification\n")
[estimation.r, estimation.result] = ...
	kernel.firFaultDiagnosis( firFilters, ...
	noisy.plant.y.subs("t", detection.t), ...
	noisy.plant.u.subs("t", detection.t), ...
	"f", noisy.plant.f.subs("t", detection.t), ...
	"occurrence", [in.model.f.occurrence]);

kernel.plotResult( estimation.r, estimation.result, "diagnosis", ...
	computation.T, "f", noisy.plant.f, "occurrence", [in.model.f.occurrence]);
