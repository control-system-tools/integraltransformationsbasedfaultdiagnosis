tExport = quantity.EquidistantDomain("t", 0, computation.T * 3, "stepSize", 1e-2);

dP.Nz = Nz;
dP.nCheb = nCheb;
dP.nSsOrder = size(waveCable.approximation.A, 1);
dP.T = computation.T;
dP.dtKernel = computation.I.stepSize;
dP.omega = [in.model.f.omega];
dP.fB1 = firFilters.threshold(1);
dP.fB2 = firFilters.threshold(2);
dP.fB3 = firFilters.threshold(3);
dP.dtSim = t.stepSize;
dP.Ntau = Ntau;
dP.dtau = computation.I.stepSize;
dP.dtDetection = detection.I.stepSize;
dP.nKernel = detection.I.n;
dP.dtSim = dt;
dP.f101 = in.model.f(1).initialCondition(1);
dP.f102 = in.model.f(1).initialCondition(2);
dP.f201 = in.model.f(2).initialCondition(1);
dP.f202 = in.model.f(2).initialCondition(2);
dP.f301 = in.model.f(3).initialCondition(1);
dP.f302 = in.model.f(3).initialCondition(2);
dP.tEnd = t.upper;
dP.tdBoundedUpper = tExport.upper;
dP = misc.structMerge( dP, estimation.result.parameters );
dP.fB0_1 = measures.fBi0(1);
dP.fB0_2 = measures.fBi0(2);
dP.fB0_3 = measures.fBi0(3);
data.detectionParameters = export.namevalues.struct2namevalues( dP, ...
		"filename", "cableDiagnosisParameters", exportOptions{:});

% export integral kernels:
data.ioKernels = export.catQuantities( ...
	{ioKernel.N, ioKernel.Mu, ioKernel.My}, "filename", "integralKernels", exportOptions{:}, "N", -1);

data.sim = export.catQuantities( {noiseFree.plant.u, noiseFree.plant.f, noiseFree.plant.d.ode}, ...
	"filename", "simulation", exportOptions{:}, "N", 501);


data.dBounded = exportData( noisy.plant.d.bounded.subs("t", tExport), ...
	"filename", "dBounded", exportOptions{:}, "N", -1);

data.residualIdentification = export.catQuantities( ...
	{identification.r, identification.result.fHatUpper, identification.result.fHatLower}, ...
	"filename", "residualIdentification", "N", 1001, exportOptions{:});

data.residualEstimation = export.catQuantities( ...
	{estimation.r, estimation.result.boundUpper, estimation.result.boundLower, ...
	estimation.result.fHatUpper, estimation.result.fHatLower}, ...
	"filename", "residualEstimation", "N", 1001, exportOptions{:});

export.Data.exportAll(data)
	
pathParts = struct(exportOptions{:});
pathParts.fileName = "svdUpsilon";
fullExportPath = fullfile(pathParts.basepath, pathParts.foldername, pathParts.fileName);

export.mat2latex( svd(measures.Upsilon)', fullExportPath, "matrixEnvironment", "", "separator", ", ");
