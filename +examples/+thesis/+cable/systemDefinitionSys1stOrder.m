function [cable, parameters] = systemDefinitionSys1stOrder(Nz)
warning("BAUSTELLE")
syms z;

% system Parameters:
L = 1210; % m
A_a = 0.47e-3; % m^2
E = 7.03e10; % N/m^2
m_c = 8.95; % kg/m
M_L = 8000; % kg % payload
V_p = 5; % m^3 % payload volume
g = 9.8; % m/s
C_d = 1;
V_s = 2; % m/s stream velocity
rho_w = 1024; %kg/m^3
c_u = 0.5;
c_v = 0.3;
c_h = 0.5;
c_w = 0.3;

rho = m_c - rho_w * A_a; % kg / m
M = M_L - rho_w * V_p;
F_0 = ( rho_w / 2 ) * C_d * V_s^2; % water stream caused drag force

spatialDomain = quantity.Domain("z", linspace(0, 1, Nz));

epsilon = quantity.Symbolic( 1 / E / A_a * sqrt( ( rho * g * z * L + M * g)^2 + F_0^2) , spatialDomain, ...
	"name", "epsilon");
theta = atan( F_0 / sqrt( rho * g * L + M * g ) );

phi = quantity.Symbolic( atan( F_0 / ( rho * g * z * L + M * g ) ) - theta, spatialDomain, "name", "phi");
T = quantity.Symbolic( rho * g * z * L + M * g, spatialDomain, "name", "T");

%%
d1 = ( 3/2 * E * A_a * phi^2 + T ) / m_c / L^2;
d2 = (E * A_a * epsilon.diff( spatialDomain, 1) / L + rho * g) / m_c / L;
d3 = - E * A_a / m_c / L^2 * phi.diff( spatialDomain, 1);
d4 = - quantity.Symbolic( c_v / m_c, spatialDomain ); % FIX
d5 = 0;

d6 = quantity.Symbolic( E * A_a / m_c, spatialDomain) / L^2;
d7 = - E * A_a * phi.diff( spatialDomain, 1) / m_c / L^2;
d8 = 0;
d9 = 0;
d10 = - quantity.Symbolic( c_u / m_c, spatialDomain ); % FIX

d11 = -c_w / M_L;
d12 = E * A_a / 2 / M_L * phi.at(0)^2 / L;  % FIX sign swap
d13 = 0;
d14 = - E * A_a / M_L * phi.at(0) / L; % FIX: sign swap + wrong sign in the paper
d15 = - c_h / M_L;
d16 = E * A_a / M_L / L; % FIX. sign swap
d17 = 0;
d18 = - E * A_a / 2 / M_L * phi.at(0) / L; % FIX: sign swap

d19 = 1 / E / A_a * L;
d20 = 1 / ( E * A_a * epsilon.at(L) + E * A_a / 2 * phi.at(L)^2 + T.at(L)) * L;

O = quantity.Symbolic.zeros( [2,2], spatialDomain, "name", "Gamma");
Q = O;
Q(1,1) = sqrt( d6 );
Q(2,2) = sqrt( d1 );

s1 = ( d8 - d6.diff(spatialDomain, 1)/2) / 2 / sqrt(d6);
s2 = ( d2 - d1.diff(spatialDomain, 1)/2) / 2 / sqrt(d1);

Ta = [s1+d10/2, d7/2/sqrt(d1)+d9/2; d3/2/sqrt(d6)+d5/2, s2+d4/2];

Tb = [d10/2-s1, d9/2-d7/2/sqrt(d1); d5/2-d3/2/sqrt(d6), d4/2-s2];

Lambda = [Q O; O -Q];
A = [Ta, Tb; Ta, Tb];

% boundary conditions
bc0 = model.Output("bc0", "C0", [-eye(2), -eye(2)]);
bc1 = model.Output("bc1", "C1", [-eye(2), eye(2)]);

% input
R = 2 * diag([ sqrt(d6.at(1)) * d19, sqrt(d1.at(1)) * d20]);
control = model.Input("plant.u", "B1", R);

% pde-ode couplings:

% * (-1)

A1 = [0 1; 0, d11];
A2 = [0 1; 0, d15];
B1 = [0; 1];
C2 = [0, 1];

Abar = [A1, d13 * B1 * C2; d17*B1*C2, A2];
Bbar = 0.5 * [0, 0; ...
	d14/sqrt(d6.at(0)), d12/sqrt(d1.at(0)); ...
	0, 0; ...
	d16/sqrt(d6.at(0)), d18/sqrt(d1.at(0))];
C3 = 2 * [ 0 0 C2; C2 0 0];

F = Abar - Bbar * C3;

ode = ss(F, eye(4), eye(4), []);
pde2ode = model.Output("plant.pde2ode", "C0", 2 * Bbar * [eye(2) zeros(2)]);

ode2pde = model.Input("plant.ode2pde", "B0", C3);


output = model.Output("plant.y", "C0", [eye(2), zeros(2)]);

cable = model.TransportOde( Lambda, "A", A, "bc0", bc0, "bc1", bc1, "ode", ode, ...
	"pde2ode", pde2ode, "ode2pde", ode2pde, "input", control, "output", output);

parameters = misc.ws2struct();
















