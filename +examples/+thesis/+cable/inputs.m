%% inputs

% reset the input:

in.uMax = 1e4;
in.u(1,1) = signals.PseudoRandomPulses(t, "seedf", 1, "seedt", 2, "seedPulse", 3, "steps", 50, ...
	"fmin", -in.uMax, "fmax", in.uMax, "deltaMin", .7, "deltaMax", 1.5, "name", "u");
in.u(2,1) = signals.PseudoRandomPulses(t, "seedf", 3, "seedt", 4, "seedPulse", 5, "steps", 50, ...
	"fmin", -in.uMax, "fmax", in.uMax, "deltaMin", .5, "deltaMax", 1, "name", "u");

%%
% ode-disturbances
in.model.d(1) = signals.models.Sinus(5, 'time', t, ...
	'occurrence', 0, 'initialCondition', [5; 0], 'SignalName', 'd1');
in.d.ode = in.model.d.simulate();

% bounded disturbances
in.d.bounds = [.03; .01; 0.02];
in.d.bounded(1,1) = signals.UniformDistributedSignal(t, "seedf", 6, "seedt", 7, ...
	"fmax", in.d.bounds(1), "fmin", -in.d.bounds(1), "filter", "gevrey", "stepsRatio", 1);
in.d.bounded(2,1) = signals.UniformDistributedSignal(t, "seedf", 8, "seedt", 9, ...
	"fmax", in.d.bounds(2), "fmin", -in.d.bounds(2), "filter", "gevrey", "stepsRatio", 2);
in.d.bounded(3,1) = signals.UniformDistributedSignal(t, "seedf", 11, "seedt", 12, ...
	"fmax", in.d.bounds(3), "fmin", -in.d.bounds(3), "filter", "gevrey", "stepsRatio", 1);

in.d.bounded.plot


%% initial conditions
% x1 = v_z(z,t)
% x2 = v_y(z,t)
% Anfangsbedingungen:
% dz x1(z) = epsilon(z) -> x1(z) = int( epsilon(z), 0, z ) + x1(0); x1(0) = 0
% dz x2(z) = -phi(z) -> x2(z) = int( -phi(z), 0, z ) + x2(0); 


x0 = quantity.Discrete.zeros([2,1], sysParas.spatialDomain );
x00(1) = int( -sysParas.phi, "z", 0, "z" );
x00(2) = int( sysParas.epsilon, "z", 0, "z" );

x0(1) = x00(1) - x00(1).at(1);
x0(2) = x00(2);

x0zGrid = quantity.Domain("z", x0(2).on + sysParas.spatialDomain.grid );
p = x0(1).replaceGrid(x0zGrid);



dtx0 = quantity.Discrete.zeros([2,1], sysParas.spatialDomain );



