function plotResults(results, optArgs)
arguments
	results
	optArgs.figureId (1,1) double {mustBeInteger};
end

if isfield(optArgs, "figureId")
	figure(optArgs.figureId), clf,
else
	figure();
end
plotOps = {'figureId', 0, "smash", true, 'hold', true};

subplot(4,2,[1 2]);
results.plant.u.plot(plotOps{:});
results.plant.d.bounded.plot(plotOps{:});
results.plant.d.ode.plot(plotOps{:});
ylabel("inputs");

legend(strrep( [results.plant.u.getIndexedName; ...
		results.plant.d.bounded.getIndexedName; ...
		results.plant.d.ode.getIndexedName], "plant.", ""), "Location", 'eastoutside')

subplot(4,2,4);
plot(results.plant.f, plotOps{:});
legend(strrep( results.plant.f.getIndexedName, "plant.", ""), "Location", 'eastoutside');
	
subplot(4,2,3);
plot(results.plant.x(1), plotOps{:});
zlabel("$v_z$", "Interpreter", "latex")

subplot(4,2,5);
plot(results.plant.x(2), plotOps{:});
zlabel("$v_y$", "Interpreter", "latex")

subplot(4,2,6);
plot(results.plant.w, plotOps{:});

subplot(4, 2, [7,8]);
plot(results.plant.y, plotOps{:});

drawnow()