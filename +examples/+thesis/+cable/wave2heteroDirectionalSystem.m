function [cable, waveCable, cableHopfCole, cableRiemannCoordinates, parameters, exportParameters] ...
	= wave2heteroDirectionalSystem(Nz, optArgs)
arguments
	Nz (1,1) double {mustBeInteger, mustBePositive} = int32(11);
	optArgs.szenario = "detection"
end
syms z;

%% Constant parameters
L = 1210; % m
A_a = 0.47e-3; % m^2
E = 7.03e10; % N/m^2
m_c = 8.95; % kg/m
M_L = 8000;% kg % payload
V_p = 5; % m^3 % payload volume
g = 9.8; % m/s
C_d = 1;
V_s = 2; % m/s stream velocity
rho_w = 1024; %kg/m^3
c_u = 0.5;
c_v = 0.3;
c_h = .5;% 1e3; %0
c_w = 0.3; % 1e3; %

rho = m_c - rho_w * A_a; % kg / m
M = M_L - rho_w * V_p;
F_0 = ( rho_w / 2 ) * C_d * V_s^2; % water stream caused drag force

%% spatially varying parameters
spatialDomain = quantity.Domain("z", linspace(0, 1, Nz));

epsilon = quantity.Symbolic( 1 / E / A_a * sqrt( ( rho * g * z * L + M * g)^2 + F_0^2) , spatialDomain, ...
	"name", "epsilon");
theta = atan( F_0 / sqrt( rho * g * L + M * g ) );

phi = quantity.Symbolic( atan( F_0 / ( rho * g * z * L + M * g ) ) - theta, spatialDomain, "name", "phi");
T = quantity.Symbolic( rho * g * z * L + M * g, spatialDomain, "name", "T");

%%
k1 = ( 3/2 * E * A_a * phi^2 + T ) / m_c / L^2;
k2 = (E * A_a * epsilon.diff( spatialDomain, 1) / L + rho * g) / m_c / L;
k3 = - E * A_a * phi.diff( spatialDomain, 1) / m_c / L^2;
k4 = - quantity.Symbolic( c_v / m_c, spatialDomain ); % FIX minus swap due to simulation section in [Wang2020]

k6 = quantity.Symbolic( E * A_a / m_c, spatialDomain) / L^2;
k7 = - E * A_a * phi.diff( spatialDomain, 1) / m_c / L^2 ;
k8 = 0;
k10 = - quantity.Symbolic( c_u / m_c, spatialDomain ); % FIX minus swap due to simulation section in [Wang2020]

% according to the literature this should be a minus, but than the system would be not stable!
k11 = -c_w / M_L;
k12 = E * A_a * phi.at(0)^2 / 2 / M_L / L; % FIX sign swap
k14 = - E * A_a * phi.at(0) / M_L / L; %  FIX: sign swap + wrong sign in the paper

k15 = - c_h / M_L;
k16 = E * A_a / M_L / L; % FIX. sign swap
k18 = - E * A_a * phi.at(0) / 2 / M_L / L; % FIX: sign swap

b1 = L / ( E * A_a * epsilon.at(1) + E * A_a / 2 * phi.at(1)^2 + T.at(1)); % -> U2 -> dz v_z
% b1: Aktuierung in y-Richtung -> im [Wang]: d20 zu U2 zugeordnet

b2 = L / E / A_a; % -> U1 -> w_x | dz x(2) -> dz v_y -> u2 im Paper
% b2: Aktuierung in z-Richtung; im [Wang]: d19 zu U1 zugeordnet

s1 = ( k2 - k1.diff(spatialDomain, 1) / 2 ) / 2 / sqrt(k1);
s2 = ( k8 - k6.diff(spatialDomain, 1) / 2 ) / 2 / sqrt(k6);


%%
Lambda = quantity.Symbolic.zeros( [4, 4], spatialDomain, "name", "Lambda");
Lambda(1,1) = sqrt(k1);
Lambda(2,2) = sqrt(k6);
Lambda(3,3) = -sqrt(k6);
Lambda(4,4) = -sqrt(k1);

Atilde = quantity.Symbolic.zeros( [4, 4], spatialDomain, "name", "\tilde{A}");
Atilde(1,:) = [ s1 + k4 / 2, k3 / 2 / sqrt(k6), - k3 / 2 / sqrt(k6), k4 / 2 - s1];
Atilde(2,:) = [ k7 / 2 / sqrt(k1), k10/2 + s2, k10/2 - s2, -k7/2/sqrt(k1)];
Atilde(3,:) = Atilde(2,:);
Atilde(4,:) = Atilde(1,:);

% F = [	0,			1,					0,		0;					...
% 		0, k11 - k12/sqrt(k1.at(0)),	0,	-k14/sqrt(k6.at(0));	...
% 		0,			0,					0,		1;					...
% 		0,	- k18/sqrt(k1.at(0)),		0,	k15 - k16 / sqrt(k6.at(0)) ];
% 
% L2 = [	0,					0;					...
% 		k12/sqrt(k1.at(0)),	k14/sqrt(k6.at(0));	...
% 		0,					0;					...
% 		k18/sqrt(k1.at(0)),	k16/sqrt(k6.at(0))];
% 	
% tilde.Q0 = [0 -1; -1 0];
% tilde.H2 = [0 0 0 2; 0 2 0 0];
% tilde.Q1 = [0 1; 1 0];
% tilde.B3 = [2*sqrt(k1.at(1)) * b5; ...
% 			2*sqrt(k6.at(1)) * b6];
% ode = ss(F, eye(4), eye(4), []);

F = [ k11 - k12/sqrt(k1.at(0)), -k14/sqrt(k6.at(0)); ...
	  - k18/sqrt(k1.at(0)),		k15 - k16 / sqrt(k6.at(0))];

L2 = [ k12/sqrt(k1.at(0)),	k14/sqrt(k6.at(0));	...
	   k18/sqrt(k1.at(0)),	k16/sqrt(k6.at(0))];
  
tilde.Q0 = [0 -1; -1 0];
tilde.H2 = [0 2; 2 0];
tilde.Q1 = [0 1; 1 0];
tilde.B3 = diag( [2*sqrt(k1.at(1)) * b1, 2*sqrt(k6.at(1)) * b2] ) ;
ode = ss(F, eye(2), eye(2), []);
%%
pde2ode = model.Output("plant.pde2ode", "C0", L2 * [eye(2) zeros(2)]);
ode2pde = model.Input("plant.ode2pde", "B0", tilde.H2);

%%
C = diag([1/sqrt(k1.at(0)), 1/sqrt(k6.at(0))]);
C1 = -C;
odeOutput = misc.Gain("plant.w", C1, 'outputType', 'plant.y');
output = model.Output("plant.y", "C0", [C zeros(2)], "input", odeOutput);

%%
bc0 = model.Output("bc0", "C0", [tilde.Q0, -eye(2)]);
bc1 = model.Output("bc1", "C1", [-eye(2), tilde.Q1]);
control = model.Input("plant.u", "B1", tilde.B3);

cableRiemannCoordinates = model.TransportOde( Lambda, "A", Atilde, "bc0", bc0, "bc1", bc1, "ode", ode, ...
	"pde2ode", pde2ode, "ode2pde", ode2pde, "input", control, "output", output);

%% hopf-cole transformation
[cableHopfCole, Phi, invPhi] = feedback.riemann2antidiagonalA(cableRiemannCoordinates);

Jminus = cableHopfCole.e1.';
Jplus  = cableHopfCole.e2.';

PhiMinus = Jminus * Phi * Jminus';
invPhiMinus = Jminus * invPhi * Jminus';

PhiPlus = Jplus * Phi * Jplus';
invPhiPlus = Jplus * invPhi * Jplus';

%%

if optArgs.szenario == "detection"
	% 5 faults:
	% f1: indomain
	% f2,3: actuator
	% f4,5: sensor
	e1 = 1;
	e1_z = quantity.Symbolic.ones(1, spatialDomain) * [e1 0 0 0 0];

	e2 = 1e4;
	E2 = [0 b1*e2 0 0 0]; 
	
	e3 = b2 * 1e4;
	E3 = [0 0 e3 0 0];
	
	e4 = 1;
	E4 = [0 0 0 e4 0];
	
	e5 = 1;
	E5 = [0 0 0 0 e5];%1e-1

elseif optArgs.szenario == "diagnosis"
	e1 = 1;
	e1_z = quantity.Symbolic.ones(1, spatialDomain) * [e1 0 0];
	
	e2 = 0;
	E2 = [0 0 0];
	
	e3 = b2 * 1e4;
	E3 = [0 e3 0];
	
	e4 = 0;
	E4 = [0 0 e4];
	
	e5 = 1;
	E5 = [0 0 e5];
end

E1 = [1; 0; 0; 1] * e1_z;
tilde.E3 = [2*sqrt(k1.at(1)) * E2; ...
			2*sqrt(k6.at(1)) * E3];
		

g1Tilde = 1;
g1 = quantity.Symbolic.ones(1, spatialDomain) * g1Tilde;
g1Bounded = quantity.Symbolic.ones(1, spatialDomain) * [0 1 0 0];
Gtilde = [1 0 0 0]';

G4 = [0 0 1 0; 0 0 0 1];
Gbounded = [0 1 0 0; 0 0 1 0; 0 0 0 1]';

G1 = g1 * [0; 1; 1; 0] * [1 0 0 0] + kron( g1Bounded, ones(4,1) );

g2 = [1 0] * G4;
g3 = [0 1] * G4;

Gamma = inv( Lambda );
cable = faultdiagnosis.hyperbolic.heteroDirectional.System( Gamma, F, ...
	"Lambda_0", - Gamma * cableHopfCole.A, ...
	"L_2", L2 * Jminus * invPhi.at(0) , ...
	"Q_0", PhiPlus.at(0) * tilde.Q0 * invPhiMinus.at(0), ...
	"Hb0", PhiPlus.at(0) * tilde.H2, ...
	"Q_1", PhiMinus.at(1) * tilde.Q1 * invPhiPlus.at(1), ...
	"Bb1", PhiMinus.at(1) * tilde.B3, ...
	"G_1", - Gamma * Phi * G1, ...
	"G_4", G4, ...
	"G_deterministic", Gtilde, ...
	"G_bounded", Gbounded, ...
	'E_1', - Gamma * Phi * E1, ...
	'Eb1', PhiMinus.at(1) * tilde.E3, ...
	'E_4', [E4; E5], ...
	'C_0', C * invPhiMinus.at(0), ...
	'H_4', C1);

parameters = misc.ws2struct();

waveCable = examples.thesis.cable.systemDefinitionWaveEquation( ...
	spatialDomain, parameters, "b1", b1, "b2", b2, "g1Bounded", g1Bounded * Gbounded, ...
	"g1", g1, "g2", g2 * Gbounded, "g3", g3 * Gbounded, "e1", e1_z, "e2", E2, "e3", E3, "e4", E4, "e5", E5);

exportParameters.L = L;
exportParameters.A =A_a;
exportParameters.E = E;
exportParameters.mc = m_c;
exportParameters.ML = M_L;
exportParameters.Vp = V_p;
exportParameters.g = g;
exportParameters.Cd = C_d;
exportParameters.Vs = V_s;
exportParameters.rhoW = rho_w;
exportParameters.cu = c_u;
exportParameters.cv = c_v;
exportParameters.ch = c_h;
exportParameters.cw = c_w;
exportParameters.thetac = theta;
exportParameters.Mc = M;
exportParameters.rhoc = rho;
exportParameters.g1Tilde = g1Tilde;
exportParameters.e1 = e1;
exportParameters.e2 = e2;
exportParameters.e3 = e3;
exportParameters.e4 = e4;
exportParameters.e5 = e5;
exportParameters.minLambda1 = min( Lambda(1,1) );
exportParameters.Lambda2 = min( Lambda(2,2) );
end
























