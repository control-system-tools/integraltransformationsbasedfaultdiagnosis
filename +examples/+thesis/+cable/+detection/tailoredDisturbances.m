%% worst case bounded disturbance
% generate the worst case sequences:
dftau = diag( in.d.bounds ) * sign( ioKernel.Md_bounded) * .9;
df1 = int( ioKernel.Md_bounded' * dftau );
% build the worst case disturbances
tDf = quantity.EquidistantDomain("t", 0, detection.I.upper, "stepSize", t.stepSize);
dft = dftau.subs("tau", tDf);

d_bounded_value = [];
nSlices = ceil( t.upper / detection.I.upper );
rng(5), rSign = sign( randn(nSlices,1) );

for k = 1:nSlices
	d_bounded_value = [d_bounded_value; ...
						rSign(k) * dft.on()];
end

in.d.bounded = quantity.Discrete( d_bounded_value(1:t.n, :), t, "name", "d_bounded");

df2 = int( ioKernel.Md_bounded' * in.d.bounded.subs("t", detection.I) );