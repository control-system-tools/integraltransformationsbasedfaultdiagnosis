% assert that the kernel exists
studySetup.tic = tic;

%= [2:0.1:3, 3.2:0.2:4, 4.5:1:8]; % 
%
studySetup.T.numel = 51;
studySetup.T.start = 4;
studySetup.T.stop = 12;
studySetup.T.values = logspace( log10( studySetup.T.start), log10( studySetup.T.stop ), studySetup.T.numel );

studySetup.dof.values = [2,6,12];%
studySetup.dof.numel = length(studySetup.dof.values);
studySetup.dof.start = studySetup.dof.values(1);
studySetup.dof.stop = studySetup.dof.values(end);
studySetup.dBounds = in.d.bounds;
studySetup.desiredGains = [3.886912e-01,  1.040648e+00,  -3.713900e-01,  2.573706e+00,  -1.540102e+00].';
studySetup.Ntau = 301; %10; %warning("change Ntau back to 401")

% Ntau = 401; Nz = 301: für ein k
% ca. 20 Minuten 
% ca. 28 GiB
% => Für 11 T: 11*25 / 60 = 4.6h
% => 40 GiB
% => 8 Kerne

detection.kernel.report( sprintf( ...
	"Start of parameter study for the cable with T from T in {%g ... %g} with %g values and dof = {%g ... %g} with %g values", ...
	studySetup.T.start, studySetup.T.stop, studySetup.T.numel, studySetup.dof.start, ...
	studySetup.dof.stop, studySetup.dof.numel) );

clear study
study(1:studySetup.T.numel, 1:studySetup.dof.numel) = struct();
exs = [];
for k = 1:studySetup.T.numel
	
	tau = quantity.EquidistantDomain("tau", 0, studySetup.T.values(k), ...
		"stepNumber", studySetup.Ntau);
	
	parfor l = 1:studySetup.dof.numel
		local = struct();
		tLocal = tic;
		local.I = tau;
		
		detection.kernel.report( sprintf( ...
			"--- k = % i --- l = % i --- : Compute the kernels for T = %g and DOF = %i", ...
			k, l, studySetup.T.values(k), studySetup.dof.values(l)));
		try
		
			local.normedFaultFunction = quantity.Discrete( 0.5 * (local.I.upper - local.I.grid).^2, local.I );

			[local.ioKernel, local.measures] = detection.kernel.computeSolution3(local.I, ...
				"dof", studySetup.dof.values(l), "delta", studySetup.dBounds, ....
				"desiredGains", studySetup.desiredGains, "normedFaultFunction", local.normedFaultFunction);
						
			local.f = diag(1./studySetup.desiredGains) * quantity.Discrete.ones([5,1], local.I);		
			local.fir = detection.kernel.generateFirFilters(local.I, local.ioKernel, ...
				"bounds", studySetup.dBounds, "type", "detection");
			
			local.r = signals.fir.mFilter( local.fir.Mf, local.f );
					
			resi = abs( local.r.on() );
			idx = find( resi >= local.fir.threshold, 1, "first" );
			if isempty( idx )
				study(k,l).detectionDelay = nan;
			else
				study(k,l).detectionDelay = local.I.grid(idx);
			end
			
			fieldNames = fieldnames( local.measures )';
			for iii = 1:length(fieldNames)%;
				study(k,l).(fieldNames{iii}) = local.measures.(fieldNames{iii});
			end
						
			study(k,l).normedFaultFunction = local.normedFaultFunction;
			study(k,l).T = studySetup.T.values(k);
			study(k,l).dof = studySetup.dof.values(l);
			study(k,l).ioKernels = local.ioKernel;
			study(k,l).I = local.I;			
			study(k,l).k = k;
			study(k,l).l = l;
			
			if flags.debug
				study(k,l).kernels = local.kernel;
				study(k,l).r = local.r;
			end
			
		catch ex
			disp(ex);
			ex = ex.addCause(MException("STUDY:err", "Error in iteration k=%i, l=%i, with T = %g and DOF = %i", k,l, studySetup.T.values(k), studySetup.dof.values(l)));
			disp(ex.cause{1});
			exs = [exs, ex];
		end
		
		if ~isfolder("build/cable")
			mkdir("build/cable")
		end
		
% 		studykl = study(k,l);
% 		
% 		saveName = "build/cable/cableDetectionStudy_" + identifier + "study_kl_" + datestr(now, 'yyyymmddTHHMMSS');
% 		save(saveName, "studykl", "studySetup");
% 		fprintf("Saved file %s \n", saveName)

		local.toc = toc(tLocal);
	end
	
	study_k = study(k,:);
	saveName = "build/cable/cableDetectionStudy_" + identifier + "study_k_" + datestr(now, 'yyyymmddTHHMMSS');
	save(saveName, "study_k", "studySetup");
	fprintf("Saved file %s \n", saveName)
	
end

studySetup.toc = toc(studySetup.tic);
fprintf("Elapsed time %g min \n", studySetup.toc / 60);
if ~isfolder("build/cable")
	mkdir("build/cable")
end

saveName = "build/cable/cableDetectionStudy_" + identifier + datestr(now, 'yyyymmddTHHMMSS');
save(saveName, "study", "studySetup");
fprintf("Saved file %s \n", saveName)

%%
if flags.plot
	for k = 1:studySetup.T.numel
		for l = 1:1:studySetup.dof.numel
			if isempty( study(k,l).threshold )
				warning("error in study(k,l), k=%i, l=%i", k, l)
				continue
			end
			% 			rBSignal = study(k,l).threshold * quantity.Discrete.ones(1, study(k,l).r.domain);
			%
			% 			figure();
			% 			study(k,l).r.plot("figureId", 0);
			% 			plot( rBSignal, "figureId", 0, "hold", true);
			% 			plot( -rBSignal, "figureId", 0, "hold", true);
			% 			title(sprintf("Normalized response for T = %g  and dof = %g, k=%i, l=%i \n", studySetup.T.values(k), studySetup.dof.values(l), k, l));
			%
			%%
			study(k,l).kernels.Mf.plot;
			
			dt = in.f(1).domain.stepSize;
			sim.I = quantity.EquidistantDomain("tau", study(k,l).I.lower, study(k,l).I.upper, "stepSize", dt);
			sim.fir = signals.fir.midpoint( study(k,l).kernels.Mf.', sim.I);
			r = signals.fir.mFilter( sim.fir, in.f );
			
			figure(); clf
			r.plot("figureId", 0);
			th = diag([study(k,l).threshold, -study(k,l).threshold] ) ...
				* quantity.Discrete.ones([2;1], in.f(1).domain);
			th.plot("figureId", 0, "hold", true, "smash", true);
			title(sprintf("Normalized response for T = %g  and dof = %g, k=%i, l=%i \n", studySetup.T.values(k), studySetup.dof.values(l), k, l));
			
			
			
		end
	end
	%%
	figure(); clf; hold on;
	
	plotValue = ["threshold", "detectionDelay", "Sf", "Sd"];
	yLabelName = ["$r_B$", "$\Delta_t$", "$S_f$", "$S_d$"];
	tiledlayout(4,1)
	for k = 1:4
		ax(k) = nexttile; hold on;
		for l = 1:studySetup.dof.numel
			plot( studySetup.T.values, [study(:,l).(plotValue(k))], "LineWidth", 2 )
		end
		ylabel(yLabelName(k))
		xlabel("T")
	end
	linkaxes(ax,'x')
	legend(compose("dof: %i", [study(1,:).dof]), "Orientation", "horizontal", "Position", [0.5, 0.01, 0.1, 0.03])
	
	%%
	figure(); clf; hold on;
	for m = 1:5
		subplot(5,1,m); hold on;
		for l = 1:studySetup.dof.numel
			plot( studySetup.T.values, abs( misc.unitVector(5, m)' * [study(:,l).mfGain] ), "LineWidth", 2);
		end
		ylabel("$\bar{m}_{f," + m + "}$")
		xlabel("T")
		legend(compose("dof: %i", [study(1,:).dof]), "Orientation", "horizontal")		
	end
	
	%%
	figure(); clf; hold on;
	for m = 1:5
		subplot(5,1,m); hold on;
		for l = 1:studySetup.dof.numel
			plot( studySetup.T.values, abs( misc.unitVector(5, m)' * [study(:,l).mfL2] ), "LineWidth", 2);
		end
		ylabel("$||m_{f," + m + "}||_2$")
		xlabel("T")
		legend(compose("dof: %i", [study(1,:).dof]), "Orientation", "horizontal")
	end
	
	%%
	figure(); clf; hold on;
	for m = 1:5
		subplot(5,1,m); hold on;
		for l = 1:studySetup.dof.numel
			plot( studySetup.T.values, abs( misc.unitVector(5, m)' * [study(:,l).mfMax] ), "LineWidth", 2);
		end
		ylabel("$||m_{f," + m + "}||_\infty$")
		xlabel("T")
		legend(compose("dof: %i", [study(1,:).dof]), "Orientation", "horizontal")		
	end
	
	%% desired gains
	figure(); clf; hold on;
	for m = 1:5
		subplot(5,1,m); hold on;
		for l = 1:studySetup.dof.numel
			plot( studySetup.T.values, misc.unitVector(5, m)' * ([study(:,l).mBarFStar]) , "LineWidth", 2);
		end
		ylabel("$\bar{m}_{f," + m + "}$")
		xlabel("T")	
	end	

	for m = 1:5
		subplot(5,1,m); hold on;
		plot( studySetup.T.values, studySetup.desiredGains(m) * ones( studySetup.T.numel, 1), "--"); 
		legend( [compose("dof: %i", [study(1,:).dof]), "*"], "Orientation", "horizontal")	
	end
	
end





	
