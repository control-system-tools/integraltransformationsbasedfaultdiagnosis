init
flags.plot = true;
flags.debug = true;
flags.export = true;
exportOptions = {'basepath', '/home/fischer/text/diss/', 'foldername', 'data/cable/detection'};
%% system initialization
% the simulation of the cable requires a temporal discretization round about 
%	dt = 1e-3 
% so that the simulation results between the description as wave-equation and hetero-directional
% system are something we can call close.
Nz = 301;
[faultyCable, waveCable, ~, ~, sysParas, cableParameters] = ...
	examples.thesis.cable.wave2heteroDirectionalSystem(Nz);

dt = 1e-3;
examples.thesis.cable.detection.inputs
NzSim = 51;

%% noise-free simulation
noiseFree = waveCable.simulate("t", t.grid, "plant.u", in.u, "plant.f", ...
	misc.unitSelector(5,[1,2,3,5]) * in.f, ... % see remark 1
	"plant.x", x0*0, "plant.x_t", dtx0*0, ...
	"plant.d.bounded", in.d.bounded * 0, ...
	"plant.d.ode", in.d.ode, "zPointSimulation", NzSim);
% --- Remark 1 --- %
% Fault 4 is a multiplicative sensor fault. Thus, it must be added after the simulation in the
% output signal:
y1Orig = noiseFree.plant.y(1);
noiseFree.plant.f(4) = y1Orig * in.f(4) * sysParas.e4;
noiseFree.plant.y(1) = y1Orig + noiseFree.plant.f(4);
if flags.plot
	examples.thesis.cable.plotResults(noiseFree, "figureId", 3);
end
% set the correct signal names:
noiseFree.plant.f.setName("f");
noiseFree.plant.d.ode.setName("d_deterministic");
noiseFree.plant.d.bounded.setName("d_bounded");
noiseFree.plant.u.setName("u");
noiseFree.plant.y.setName("y");

%% compute the fault detection kernel:
backsteppingTol = 1e-6;
detection.kernel = faultdiagnosis.hyperbolic.heteroDirectional.DetectionKernel( faultyCable, ...
	"disturbanceModel", in.model.d, "numberOfLines", Nz, "tolerance", backsteppingTol); % 
% default: 5 steps
% "numberOfLines", Nz, "tolerance", 1e-9: 9 steps 
% Nz = 301, "numberOfLines", 301, "tolerance", 1e-6 : 7 steps
% Nz = 51, "numberOfLines", 51, "tolerance", 1e-6 : 
diary BACKSTEPPINGKERNEL.log

%% compute the integral kernels
fprintf("# compute the integral kernels\n")
% --- define the parameters for the computation of the integral kernels
Tmin = abs( detection.kernel.theta_max ) + abs( detection.kernel.theta_min );
dofs = 2;
T = 6;
Ntau = 301;
I = quantity.EquidistantDomain("tau", 0, T, "stepNumber", Ntau);
% the desired gains chosen so that they can be achieved
desiredGains = [3.886912e-01,  1.040648e+00,  -3.713900e-01,  2.573706e+00,  -1.540102e+00].';
normedFaultFunction = quantity.Discrete.zeros([5,5], I);
normedFaultFunction(1,1) = quantity.Discrete( 0.5 * (I.upper - I.grid).^2, I );
normedFaultFunction(2,2) = quantity.Discrete( 0.5 * (I.upper - I.grid).^2, I );
normedFaultFunction(3,3) = quantity.Discrete( 0.5 * (I.upper - I.grid).^2, I );
normedFaultFunction(4,4) = quantity.Discrete( 0.5 * (I.upper - I.grid).^2, I );%quantity.Discrete( (I.upper - I.grid), I );
normedFaultFunction(4,4) = quantity.Discrete( 0.5 * (I.upper - I.grid).^2, I );

[ioKernel, measures, kernels, PsiThetaMu] = detection.kernel.computeSolution3(I, "dof", dofs, ...
		 "delta", in.d.bounds, "desiredGains", desiredGains, ...
		 "normedFaultFunction", normedFaultFunction, "ansatz", "taylorPolynomial", ...
		 "C0regularity", 0);
fprintf("Achieved gains: " + misc.vector2string(measures.mBarFStar, "formatSpec", "%e", "separator", ",  ") + "\n");
fprintf("Difference to desired gains: " + misc.vector2string(measures.mBarFStar-desiredGains, "formatSpec", "%e", "separator", ",  ") + "\n");

%%
% kernels(k).Mf.plot
dtDetection = I.stepSize; % t.stepSize
detection.I = quantity.EquidistantDomain("tau", 0, T, "stepSize", dtDetection);
detection.t = quantity.EquidistantDomain("t", 0, t.upper, "stepSize", dtDetection);

firFilter = detection.kernel.generateFirFilters(detection.I, ioKernel, "bounds", in.d.bounds, ...
	"type", "detection", "discretizationFunction", @signals.fir.midpoint);
fprintf("Threshold value: %g\n", firFilter.threshold);
%
[detectionNoiseFree.r, detectionNoiseFree.result] = ...
	detection.kernel.firFaultDiagnosis( firFilter, ...
		noiseFree.plant.y.subs("t",	detection.t), ...
		noiseFree.plant.u.subs("t", detection.t), ...
		"f", noiseFree.plant.f.subs("t", detection.t), ...
		"occurrence", in.on);
detectionNoiseFree.r.setName("rNoiseFree");

detection.kernel.plotResult(detectionNoiseFree.r, detectionNoiseFree.result, ...
	"detection", detection.I.upper, "occurrence", in.on, "figureID", 5);
title( sprintf( ...
	"$$T = %g, dt_{Kernel} = %g, N_{\\tau} = %i, dt_{sim} = %g, dt_{detec} = %g, r_B = %g$$, \n $$dof = %i, \\bar{m}_f = %s, Sf = %g$$", ...
	T, I.stepSize, Ntau, t.stepSize, detection.t.stepSize, measures.threshold, dofs, ...
	misc.vector2string(measures.mBarFStar, "formatSpec", "%.1e", "separator", ",  ", "format", "latex"), ...
	measures.Sf) )

%% noisy simulation
examples.thesis.cable.detection.tailoredDisturbances;

%%
[noisy, stsp] = waveCable.simulate("t", t.grid, "plant.u", in.u, "plant.f", ...
	misc.unitSelector(5,[1,2,3,5]) * in.f, ... % see remark 2
	"plant.x", x0*0, "plant.x_t", dtx0*0, ...
	"plant.d.bounded", in.d.bounded, ...
	"plant.d.ode", in.d.ode, "zPointSimulation", NzSim);
% --- Remark 2 --- %
% Fault 4 is a multiplicative sensor fault. Thus, it must be added after the simulation in the
% output signal:
y1Orig = noisy.plant.y(1);
noisy.plant.f(4) = y1Orig * in.f(4) * sysParas.e4;
noisy.plant.y(1) = y1Orig + noisy.plant.f(4);
if flags.plot
	examples.thesis.cable.plotResults(noisy, "figureId", 5);
end

% set the correct signal names:
noisy.plant.f.setName("f");
noisy.plant.d.ode.setName("d_deterministic");
noisy.plant.d.bounded.setName("d_bounded");
noisy.plant.u.setName("u");
noisy.plant.y.setName("y");

[detectionNoisy.r, detectionNoisy.result] = ...
	detection.kernel.firFaultDiagnosis( firFilter, ...
		noisy.plant.y.subs("t",	detection.t), ...
		noisy.plant.u.subs("t", detection.t), ...
		"f", noisy.plant.f.subs("t", detection.t), ...
		"d_bounded", noisy.plant.d.bounded.subs("t", detection.t), ...
		"occurrence", in.on);
detectionNoisy.r.setName("rNoisy");

detection.kernel.plotResult(detectionNoisy.r, detectionNoisy.result, ...
	"detection", detection.I.upper, "occurrence", in.on, "figureID", 6);

title( sprintf( ...
	"$$T = %g, dt_{Kernel} = %g, N_{\\tau} = %i, dt_{sim} = %g, dt_{detec} = %g, r_B = %g$$, \n $$dof = %i, \\bar{m}_f = %s, Sf = %g$$", ...
	T, I.stepSize, Ntau, t.stepSize, detection.t.stepSize, measures.threshold, dofs, ...
	misc.vector2string(measures.mBarFStar, "formatSpec", "%.1e", "separator", ",  ", "format", "latex"), ...
	measures.Sf) )



























