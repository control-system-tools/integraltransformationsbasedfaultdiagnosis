% define the temporal domain
t = quantity.EquidistantDomain("t", 0, 195, "stepSize", dt);
% for the control and disturbance inputs call:
examples.thesis.cable.inputs

% --- faults ---
in.offDelay = 20;
in.fDeltaOn = 40;
in.on = 15 + (0:4) * in.fDeltaOn;
in.off = in.on + in.offDelay;

in.f1Max = .7;
in.f3Max = 2;

in.f = quantity.Discrete.zeros([5, 1], t, "name", "f");
in.f(1) = signals.UniformDistributedSignal(t, "t0", in.on(1), "tEnd", in.off(1), ...
	"seedf", 11, "seedt", 12, "fmin", -in.f1Max, "fmax", in.f1Max, "filter", "gevrey");

in.f(2) = quantity.Discrete( (t.grid >= in.on(2) & t.grid <= in.off(2)), t) * in.u(2,1) * 2e-4;

in.f(3) = signals.PseudoRandomPulses(t, "t0", in.on(3), "tEnd", in.off(3)-1, ...
	"seedf",6, "seedt", 13, "seedPulse", 15, "steps", 235, ...
	"deltaMin", 1, "deltaMax", 2, ...
	"fmin", -in.f3Max, "fmax", in.f3Max, "name", "f3");

in.f(4) = quantity.Discrete( (t.grid >= in.on(4) & t.grid <= in.off(4))*.5, t);
in.f(5) = quantity.Discrete( (t.grid >= in.on(5) & t.grid <= in.off(5)), t) * 6e-1; % .* (t.grid - in.on(5))

% --- plot the excitation signals ---
if flags.plot
	
	figure(1); clf;
	subplot(4,1,1);
	plot( in.u, "figureId", 0, "smash", true);
	
	subplot(4,1,2);
	plot( in.f, "figureId", 0, "smash", true);
	legend(strrep( in.f.getIndexedName, "plant.", ""))
	
	subplot(4,1,3);
	plot( in.d.ode, "figureId", 0, "smash", true);

	subplot(4,1,4);
	plot( in.d.bounded, "figureId", 0, "smash", true);

	in.f.plot
end