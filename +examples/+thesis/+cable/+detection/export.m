if flags.export
	
	% todo
	% g1Tilde; 
	
	% cable parameters
	cableParameters.omegad = in.model.d.omega; 
	cableParameters.vd0 = in.model.d.initialCondition; %to verify
	cableParameters.delta1 = in.d.bounds(1);
	cableParameters.delta2 = in.d.bounds(2);
	cableParameters.delta3 = in.d.bounds(3);
	cableParameters.Nz = detection.kernel.spatialDomain.n;
	cableParameters.backsteppingTolerance = detection.kernel.backsteppingKernel(1).tolerance;
	cableParameters.backsteppingSteps = 5;
	cableParameters.Nz = Nz;
	cableParameters.backsteppingTolerance = backsteppingTol;
	cableParameters.successiveApproximationIterations = 7;
	cableParameters.chebMatDim = NzSim;
	cableParameters.nStateSpace = size(stsp.A,2);
	cableParameters.uMax = in.uMax;
	cableParameters.trasnportationTime = detection.kernel.theta_max;
	cableParameters.T0 = detection.kernel.theta_max * 2;
	
	data.parameters = export.namevalues.struct2namevalues(cableParameters, 'filename', 'cableParameters', exportOptions{:});
		
	% detection parameters:
	detectionParameters = measures;
	detectionParameters.nPhi = measures.n_phi/faultyCable.n.y;
	detectionParameters.T = T;
	detectionParameters.offDelay = in.offDelay;
	detectionParameters.fOn = in.on;
	detectionParameters.fDeltaOn = in.fDeltaOn;
	detectionParameters.f1Bounds = in.f1Max;
	detectionParameters.f3Bounds = in.f3Max;
	detectionParameters.dtDetection = dtDetection;
	detectionParameters.dtSim = dt;
	detectionParameters.firOrder = firFilter.numel;
	detectionParameters.tEnd = t.upper;
	
	for i = 1:faultyCable.n.f
		detectionParameters.("t" + i) = in.on(i);
		detectionParameters.("e" + i) = in.off(i);
		detectionParameters.("detection" + i ) = detectionNoisy.result.detection(i).start;		
		detectionParameters.("detectionDelay" + i ) = detectionNoisy.result.detection(i).start - in.on(i);
% 		detection.result.parameters.("detectionDelay" + i) = detectionNoiseFree.result.parameters.("detection"+i) - occurrences(i);
	end
	
	data.detectionParameters = export.namevalues.struct2namevalues( detectionParameters, ...
		"filename", "cableDetectionParameters", exportOptions{:});

	% integral kernels:
	data.ioKernels = export.catQuantities(...
		{ioKernel.N, ioKernel.Mu, ioKernel.Mf, ioKernel.Md_bounded}, ...
		"filename", "integralKernels", exportOptions{:}, "N", -1);
	
	jumps{1} = struct('idx_t', 1, 'idx_j', 5+1, 'tj', in.on(5));
	data.faults = exportData( noiseFree.plant.f, "jumps", jumps, "N", 1001, ...
		"filename", "faults", exportOptions{:});
	faultsDomain = linspace(0, t.upper, data.faults.N);
	for k = 1:numel(in.on)
		idxk = find( faultsDomain > in.on(k) & faultsDomain < in.off(k) );
		idxk = ceil( linspace(idxk(1), idxk(end), 7));
		misc.vector2string( idxk(2:end-1), "separator", ", ")
	end
	
	% simulation data
	% TODO: simulation -> [u_i ... d_deterministic]
	data.sim = export.catQuantities( {noiseFree.plant.u}, ...
		"filename", "simulation", "N", 401, exportOptions{:});
	% the disturbances:
	distDomain = quantity.EquidistantDomain("t", 0, 10, "stepSize", t.stepSize);
	data.disturbances = export.catQuantities( {noiseFree.plant.d.ode.subs("t", distDomain); ...
		noisy.plant.d.bounded.subs("t", distDomain)}, ...
		"filename", "disturbance", "N", 501, exportOptions{:});
	% residual data
	data.residual = export.catQuantities(...
		{detectionNoiseFree.r, detectionNoiseFree.result.boundUpper, ...
		 detectionNoiseFree.result.boundLower, detectionNoisy.r}, ...
		"filename", "residual", exportOptions{:}, "N", 1000);
	
	export.Data.exportAll(data)
	
% 	% magnitudes for the fault detectability condition:
	if false
		Xmag = detection.kernel.verifyDetectability();
	%	set the components 2,2 and 3,1 to zero because they are zero, which cannot be represented in dB.
	%	Thus, for the pgfplots to work, it is set to zero.
		Xmag(2,2)=Xmag(2,1)*0 ;
		Xmag(3,1)=Xmag(2,1)*0 ;
		Xmag.setName("Xmag");
		XmagExport = Xmag.exportData( "filename", "Xmagnitudes", "N", -1, exportOptions{:});
		XmagExport.export
 	end
% 	% compute the deflection of the cable:
% 	z = quantity.Domain("z", linspace(0.1,1));
% 	p = quantity.Discrete( (z.grid - 1.5 ).^4, z);
% 	p = p - p.at(1);
% 	p = - p / p.at(0);
% 	p.setName("p");
% 	p.plot
% 	
% 	p.exportData("filename", "cablePosition0", exportOptions{:}).export;
	
	% export the desired gains:
	pathParts = struct(exportOptions{:});
	pathParts.fileName = "desiredGains";
	
	fullExportPath = fullfile(pathParts.basepath, pathParts.foldername, pathParts.fileName);
	
	export.mat2latex(desiredGains', fullExportPath);
	
	%% export the parameter study results:
	paraStudy = load('/home/fischer/algebraicfaultdiagnosis/build/cable/cableDetectionStudy_1_20210909T043920.mat');
	rbData = reshape( [paraStudy.study.threshold], paraStudy.studySetup.T.numel, paraStudy.studySetup.dof.numel);
	rbData = [ paraStudy.studySetup.T.values.', rbData ];
	rbData = [ rbData, reshape( [paraStudy.study.detectionDelay], paraStudy.studySetup.T.numel, paraStudy.studySetup.dof.numel)];
	
	rBHeaders = "T";
	for i = 1:paraStudy.studySetup.dof.numel
		rBHeaders(end+1) = "rB_" + i;
	end
	for i = 1:paraStudy.studySetup.dof.numel
		rBHeaders(end+1) = "d_" + i;
	end
	
	export.dd("M", rbData, "header", rBHeaders, "filename", "rb", "N", -1, exportOptions{:}).export;
	
	rbParameters.nPhi = 2 * ( 2 * detection.kernel.n.w + paraStudy.studySetup.dof.values );
	rbParameters.nDof = paraStudy.studySetup.dof.numel;
	
	rBParaExport = export.namevalues.struct2namevalues( rbParameters, "filename", "cableDetectionStudyParameters", ...
		exportOptions{:});
	rBParaExport.export();
	
end