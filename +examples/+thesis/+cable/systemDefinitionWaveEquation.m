function [cable, parameters] = systemDefinitionWaveEquation(spatialDomain, cableParameters, optArgs)
arguments
	spatialDomain (1,1) quantity.Domain;
	cableParameters struct;
	optArgs.g1Bounded quantity.Symbolic = quantity.Symbolic.zeros([4,1], spatialDomain);
	optArgs.g1 quantity.Symbolic = quantity.Symbolic.zeros(1, spatialDomain);
	optArgs.g2 (1,:) = zeros(2,1);
	optArgs.g3 (1,:) = zeros(2,1);
	optArgs.e1 (1,:) quantity.Symbolic = quantity.Symbolic.zeros(1, spatialDomain);
	optArgs.e2 (1,:) double = 0;
	optArgs.e3 (1,:) double = 0;
	optArgs.e4 (1,:) double = 0;
	optArgs.e5 (1,:) double = 0;
	optArgs.b1 (1,1) double = 0;
	optArgs.b2 (1,1) double = 0;
end

%% pde dt² x = lambda dz² x + G dz x + D dt x
% x1 = w -> v_y Auslenkung in y-Richtung
% x2 = u -> v_z Auslenkung in z-Richtung

% dz² Term
Gamma = quantity.Symbolic.zeros( [2,2], spatialDomain, "name", "Gamma");
Gamma(1,1) = cableParameters.k1;
Gamma(2,2) = cableParameters.k6;

% dz term
% [G11, G12] [dz v_y]  
% [G21, G22] [dz v_z]
G = quantity.Symbolic.zeros( [2,2], spatialDomain, "name", "Gamma");
G(1,1) = cableParameters.k2; 
G(1,2) = cableParameters.k3;
G(2,1) = cableParameters.k7;

% dt term
% [D11, D12] [dt v_y]
% [D21, D22] [dt v_z]
D = quantity.Symbolic.zeros( [2,2], spatialDomain, "name", "Gamma");
D(1,1) = cableParameters.k4; 
D(2,2) = cableParameters.k10;

% ode - subssystem dt xi = F xi + L2 [x1(0); x2(0)]
% xi1 = dt v_y(0)
% xi2 = dt v_z(0)
F = diag([cableParameters.k11, cableParameters.k15]);
ode = ss(F, eye(2), eye(2), []);

% boundary condition 0:
% [ dt v_y(0) ] = [ w_1(0) ]
% [ dt v_z(0) ] = [ w_2(0) ]
% 0 = bc0(v, v_t) + ode2pde.B0 w(t) 
% 0 = Ct0 * dtv(0) + B0 * w
bc0 = model.Output("bc0", "Ct0", -eye(2));
% ode2pde:
ode2pdeB = model.Input("plant.ode2pde", "B0", eye(2));
% pde2ode: dt w = F w + Bode * dz v(0)
Bode =   [cableParameters.k12, cableParameters.k14; ...
          cableParameters.k18, cableParameters.k16];
pde2ode = model.Output("plant.pde2ode", "Cz0", Bode);

%%
% boundary conditions z=1:
bc1 = model.Output("bc1", "Cz1", -eye(2));

% boundary input
B1 = diag( [cableParameters.b1, cableParameters.b2] );

input.u = model.Input("plant.u", "B1", B1);
input.d.ode = model.Input("plant.d.ode", "B", [zeros(size( optArgs.g1 ) ); optArgs.g1]);
input.d.bounded = model.Input("plant.d.bounded", "B", kron( optArgs.g1Bounded, ones(2,1) ) );
input.y.d.bounded = misc.Gain("plant.d.bounded", [optArgs.g2; optArgs.g3], 'outputType', 'plant.y');

input.e = model.Input("plant.f", "B", [optArgs.e1; zeros( size( optArgs.e1 ) )], ...
	"B1", [optArgs.e2; optArgs.e3]);

inputs = model.Inputs();
inputs.add(input.u);
inputs.add(input.d.ode);
inputs.add(input.d.bounded );
inputs.add(input.e);

%%
% output
% y1 = w' + dBar1
% y2 = u' + dBar2
input.eOutput = misc.Gain("plant.f", [optArgs.e4; optArgs.e5], "outputType", "plant.y");
output = model.Output("plant.y", "Cz0", eye(2), "outputType", input.y.d.bounded, "input", ...
	input.y.d.bounded + input.eOutput);

cable = model.WaveOde( Gamma, "G", G, "D", D, "ode", ode, "bc0", bc0, "bc1", bc1, "ode2pde", ode2pdeB, ...
	"pde2ode", pde2ode, "input", inputs, "output", output);


%%
parameters = misc.ws2struct();











