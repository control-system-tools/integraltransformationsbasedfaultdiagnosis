
studyFiles = dir("build/cable/study/*.mat");

studyi = load( fullfile( studyFiles(1).folder, studyFiles(1).name) );

% init the structure:
study = studyi.studykl;
for i = 1:numel(studyFiles)
	studyFileName = fullfile( studyFiles(i).folder, studyFiles(i).name);
	fprintf("Load file %s \n", studyFileName);
	studyi = load( studyFileName );
	studySetup = studyi.studySetup;
	k = find( studySetup.T.values == studyi.studykl.T );
	l = find( studySetup.dof.values == studyi.studykl.dof );
	study(k,l) = studyi.studykl;
end

studySetup.T.values = 3:0.5:5;

%% compute kernels:
l=1;

for k = 1:3:studySetup.T.numel
detection.kernel.report( sprintf( ...
			"--- k = % i --- l = % i --- : Compute the kernels for T = %g and DOF = %i", ...
			k, l, studySetup.T.values(k), studySetup.dof.values(l)));
I = study(k,l).mf(1).domain;
		
kernels.Mf = study(k,l).mf;
kernels.N = study(k,l).n;
kernels.Mu = study(k,l).mu;	 
	 
% do the fault detection
dt = t.stepSize;
detection.I = quantity.EquidistantDomain("tau", 0, I.upper, "stepSize", dt);
detection.t = quantity.EquidistantDomain("tau", 0, t.upper, "stepSize", dt);

firFilter = detection.kernel.generateFirFilters(detection.I, kernels, "bounds", in.d.bounds, ...
	"type", "detection");
firFilter.threshold = study(k,l).threshold;

[detectionNoiseFree(k).r, detectionNoiseFree(k).result] = ...
	detection.kernel.firFaultDiagnosis( firFilter, ...
		noiseFree.plant.y.subs("t",	detection.t), ...
		noiseFree.plant.u.subs("t", detection.t), ...
		"f", noiseFree.plant.f.subs("t", detection.t), ...
		"occurrence", in.on);
detectionNoiseFree(k).r.setName("rNoiseFree");

detection.kernel.plotResult(detectionNoiseFree(k).r, detectionNoiseFree(k).result, ...
		"detection", detection.I.upper, "occurrence", in.on);
title( sprintf( ...
	"$$T = %g, dt_{Kernel} = %g, dt_{sim} = %g, dt_{detec} = %g, dof = %i, S_f = %g, n_{\\tau} = %i$$", ...
	detection.I.upper, misc.stepSize(I), t.stepSize, detection.t.stepSize, studySetup.dof.values(l), ...
	study(k,l).Sf, I.n) )
end