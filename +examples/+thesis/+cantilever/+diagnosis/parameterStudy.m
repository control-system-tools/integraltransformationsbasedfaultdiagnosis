
if ~exist( "kernel", "var")
	examples.thesis.cantilever.diagnosis.createKernel;
end

%% Parameterstudy for the detection window length
studySetup.tic = tic;
studySetup.n = 61;		% number of detection window lengths
studySetup.o = 4;		% number of gevrey orders
studySetup.start = 1.2;	% start value for the detection window length
studySetup.stop = 4;		% end value for the detection window length
studySetup.T = logspace( log10( studySetup.start ), log10( studySetup.stop ), studySetup.n );
studySetup.order = linspace( 1.6, 1.999, studySetup.o);

% over all time 8 min
% one iteration ca. 6 min -> take 12 min
% 

% copy the faults used for the simulation:
% TODO Add the normalizedFaults to the study setup
% TODO Add Ntau to the study setup
normalizedFaults = inputs.models.f;
[normalizedFaults.occurrence] = deal(0);

normalizedFaults(1).initialCondition = 100;
normalizedFaults(2).initialCondition = [100; 0];
normalizedFaults(3).initialCondition = [3; 1];
normalizedFaults(4).initialCondition = [50; 0];

%logspace( log10( 1.5), log10( 1.999 ), studySetup.o);

% Matlab requires round about 16GB and ca 45 seconds for computation of one kernel:
% estiamte the required time:
% kernel.report( sprintf(" The parameter study will require round about %g minutes for the computation", ...
% 	studySetup.n * 220 / 60));

clear study;
study(1:studySetup.n, 1:studySetup.o) = struct();

for k = 1:studySetup.n
	parfor l = 1:studySetup.o
		kernel.report( sprintf("--- k = % i --- l = % i --------------------------------------------------\n", k, l) );
		kernel.report( sprintf("Compute the kernels for T = %g  and alpha = %g \n", studySetup.T(k), studySetup.order(l)) );
		tic;
		study_kl = struct();
		
		study_kl.I = quantity.EquidistantDomain("tau", 0, studySetup.T(k), "stepNumber", Ntau);
		
		% Compute the fault detection kernel on the detection time window Ik:
		study_kl.theta = signals.GevreyFunction( 'order', studySetup.order(l), ...
			"diffShift", 1, "numDiff", kernel.N_s, ...
			'timeDomain', study_kl.I, "name", "theta");
		
		study_kl.ioKernels = kernel.computeIdentificationFilter(study_kl.I, ...
			"theta", study_kl.theta);
		study_kl.threshold = study_kl.ioKernels.thresholds;
		
		% compute the normalized inputs:
		study_kl.f = normalizedFaults.simulate("time", study_kl.I);
		study_kl.f.setName("f")
				
		study_kl.fir = ...
			kernel.generateFirFilters(study_kl.I, study_kl.ioKernels, "type", "diagnosis");
		
		thresholdsSignals = study_kl.threshold.' * quantity.Discrete.ones( 1, study_kl.I);
		
		study_kl.rAll = signals.fir.mFilter( study_kl.fir.Mf, study_kl.f );
		
		for m = 1:kernel.sys.n.f	
			Jm = zeros( kernel.sys.n.f );
			Jm(m,m) = 1;
			
			study_kl.r(:,m) = signals.fir.mFilter( study_kl.fir.Mf, Jm * study_kl.f );
						
			for n = 1:kernel.sys.n.f
				% find the detection delay for separate excitation
				resi = on( abs( study_kl.r(n,m) ) );
				idx = find( resi >= study_kl.threshold(n),1, "first" );
				if isempty( idx )
					study_kl.detectionDelays(m,n) = nan;
				else
					study_kl.detectionDelays(m,n) = study_kl.I.grid(idx);
				end
			end
			study_kl.detectionDelay(m) = min( study_kl.detectionDelays(m,:) );
			
			% find the detection delay for excitation with all faults
			resiAll = on( abs( study_kl.rAll(m) ) );
			idxAll = find( resiAll >= study_kl.threshold(m),1, "first" );
			if isempty( idxAll )
				study_kl.detectionDelaysAll(m) = nan;
			else
				study_kl.detectionDelaysAll(m) = study_kl.I.grid(idxAll);
			end
		end
		
		study(k,l).detectionDelayAll = min( study_kl.detectionDelaysAll );
		study(k,l).detectionDelay = study_kl.detectionDelay;
		study(k,l).threshold = study_kl.threshold;
		study(k,l).r = study_kl.r;
		study(k,l).r.setName("\hat{f}");
		study(k,l).rAll = study_kl.rAll;
		study(k,l).rAll.setName("\hat{f}");
		study(k,l).k = k;
		study(k,l).l = l;
		study(k,l).fir = study_kl.fir;
		
		kernel.report( sprintf("--- finished --- k = % i --- l = % i --- toc = %g seconds ------------\n", k, l, toc) );
		
		%% debug section
		if flags.debug
			study(k,l).ioKernels = study_kl.ioKernels;
		end	
	end
	studyk = study(k,:);
	fName = "build/beam/beamDiagnosisStudy_study_k_" + datestr(now, 'yyyymmddTHHMMSS');
	save(fName, "studyk", "studySetup");
	fprintf("Saved file %s \n", fName);
end

fName = "build/beam/beamDiagnosisStudy_study_" + datestr(now, 'yyyymmddTHHMMSS');
save(fName, "study", "studySetup");
fprintf("Saved file %s \n", fName)

studySetup.toc = toc( studySetup.tic );
studySetup.tocOverAll = toc( flags.tic );
kernel.report( sprintf(" Study took %g min \n", studySetup.toc / 60));
kernel.report( sprintf(" Overall time %g min \n", studySetup.tocOverAll / 60));
%%
if flags.plot
	for k = 1:10 %studySetup.n
		for l = 2 %1:studySetup.o
			
			study_kl = study(k,l);
			
			thresholdsSignals = study_kl.threshold.' * quantity.Discrete.ones( 1, study_kl.r(1).domain);

			idx = 1 : numel(study_kl.r);
			idx = reshape(idx, size(study_kl.r, 2), size(study_kl.r, 1))';
			idxTh = repmat( (1:size(study_kl.r, 1))', 1, size(study_kl.r, 2));

			figure();
			for m = 1:numel(study_kl.r)

				subplot(4,4,idx(m));

				study_kl.r(m).plot("figureId", -1, "smash", 1, "hold", true);

				ylabel( study_kl.r.getIndexedName("idx", m, "format", "latex"), "Interpreter", "latex" )

				plot(thresholdsSignals(idxTh(m)), "hold", true, "figureId", -1, "smash", true);
				plot(-thresholdsSignals(idxTh(m)), "hold", true, "figureId", -1, "smash", true);
			end
			suptitle(sprintf("Normalized response for T = %g  and alpha = %g \n", studySetup.T(k), studySetup.order(l)));
			
% 			%%
% 			figure()
% 			for m = 1:numel(study_kl.rAll)
% 				subplot(4,1,m);
% 				study_kl.rAll(m).plot("figureId", -1, "smash", 1, "hold", true);
% 				
% 				plot(thresholdsSignals(m), "hold", true, "figureId", -1, "smash", true);
% 				plot(-thresholdsSignals(m), "hold", true, "figureId", -1, "smash", true);
% 			end
% 			suptitle(sprintf("Normalized response for T = %g  and alpha = %g with simultaneous excitation \n", studySetup.T(k), studySetup.order(l)));
% 			
		end
	end
end
%%
% data preparation
[grid.O, grid.T] = meshgrid( studySetup.order, studySetup.T );

nf = 4;

fB = reshape( [study.threshold], nf, studySetup.n, studySetup.o );
delay = reshape( [study.detectionDelay], nf, studySetup.n, studySetup.o );

if flags.plot
	
	% --- plot the thresholds ---
	figure(1); clf; clear ax1
	for i = 1:nf
		figure(1)
		subplot(3,2,i);
		ax1(:,i) = plot3( grid.T, grid.O, squeeze(fB(i,:,:)) , "LineWidth", 2);
		xlabel("$T$", "Interpreter", "latex")
		ylabel("$\alpha$", "Interpreter", "latex")
		zlabel("$f_{B," + i +"}$", "Interpreter", "latex");
		zlim([0 20])
		grid on;
		view(0,0)
	end
	hLegend = subplot(3,2,5.5);
	posLegend = get(hLegend, "Position");
	leg = legend( hLegend, ax1(:,1), num2cell( arrayfun( @(a) "$\alpha = " + a + "$", studySetup.order)), ...
		"Orientation", "horizontal", "Interpreter", "latex");
	axis(hLegend,'off');
	set(leg,'Position',posLegend);
	
	% --- plot the delays ---
	figure(2); clf; clear ax2
	for i = 1:nf
		subplot(3,2,i);
		ax2(:,i) = plot3( grid.T, grid.O, squeeze(delay(i,:,:)) , "LineWidth", 2);
		xlabel("$T$", "Interpreter", "latex")
		ylabel("$\alpha$", "Interpreter", "latex")
		zlabel("$\Delta_{f," + i +"}$", "Interpreter", "latex");
		grid on;
		view(0,0)
	end
	hLegend = subplot(3,2,5.5);
	posLegend = get(hLegend, "Position");
	leg = legend( hLegend, ax2(:,1), num2cell( arrayfun( @(a) "$\alpha = " + a + "$", studySetup.order)), ...
		"Orientation", "horizontal", "Interpreter", "latex");
	axis(hLegend,'off');
	set(leg,'Position',posLegend);
	% 	plot3( grid.T, grid.O, delay , "LineWidth", 2);
	% 	grid on;
	% 	view(0,0);
	
	figure(3); clf; clear ax3
	subplot(4,1,[1 3])
	ax3 = plot3( grid.T, grid.O, reshape( [study.detectionDelayAll], studySetup.n, studySetup.o));
	xlabel("$T$", "Interpreter", "latex")
	ylabel("$\alpha$", "Interpreter", "latex")
	zlabel("$\Delta_{f}$", "Interpreter", "latex");
	grid on;
	view(0,0)
	hLegend = subplot(4,1,4);
	posLegend = get(hLegend, "Position");
	leg = legend( hLegend, ax3(:,1), num2cell( arrayfun( @(a) "$\alpha = " + a + "$", studySetup.order)), ...
		"Orientation", "horizontal", "Interpreter", "latex");
	axis(hLegend,'off');
	set(leg,'Position',posLegend);
	
end

% --- export ---
if flags.doTheExport
	for i = 1:nf
		studyData.("fB" + i) = exportData( quantity.Discrete( squeeze( fB(i,:,:) ), ...
			quantity.Domain("T", studySetup.T), "name", "rB"), "filename", "fB" + i, exportOptions{:});
		studyData.("delay" + i) = exportData( quantity.Discrete( squeeze( delay(i,:,:) ), ...
			quantity.Domain("T", studySetup.T), "name", "delay"), "filename", "delay"+i , exportOptions{:});
	end

	studyData.delayAll = exportData( quantity.Discrete( ...
		reshape( [study.detectionDelayAll], studySetup.n, studySetup.o), ...
		quantity.Domain("T", studySetup.T), "name", "delay"), "filename", "delayAll" , exportOptions{:});
	
	studyParameters.alphaMin = studySetup.order(1);
	studyParameters.alphaMax = 2;
	studyParameters.alphaN = studySetup.o;
	studyParameters.legend = sprintf("$\\alpha = %.4g$, ", studySetup.order);
	studyParameters.f10 = normalizedFaults(1).initialCondition;
	studyParameters.f201 = normalizedFaults(2).initialCondition(1);
	studyParameters.f202 = normalizedFaults(2).initialCondition(2);
	studyParameters.f301 = normalizedFaults(3).initialCondition(1);
	studyParameters.f302 = normalizedFaults(3).initialCondition(2);
	studyParameters.f401 = normalizedFaults(4).initialCondition(1);
	studyParameters.f402 = normalizedFaults(4).initialCondition(2);

	for i = 1:studySetup.o
		studyParameters.("alpha" + i) = studySetup.order(i);
	end

	studyData.parameters = export.namevalues.struct2namevalues(studyParameters, ...
		'filename', 'studyParameters', exportOptions{:});

	
	for l = 1:studySetup.o
		thetaExport(l) = signals.GevreyFunction( 'order', studySetup.order(l), ...
		"diffShift", 1, "numDiff", 0, ...
		'timeDomain', quantity.EquidistantDomain("tau", 0, 1.75, "stepNumber", Ntau), "name", "theta").f;
	end
	
	studyData.theta = exportData( thetaExport, "filename", "theta" , exportOptions{:});
	
	export.Data.exportAll( studyData )
end










