init;

flags.tic = tic;
flags.doTheExport = true;
flags.samplingRateStudy = false;
flags.detection = false;
flags.plot = true;
flags.save = true;
exportOptions = {'basepath', '/home/fischer/text/diss/', 'foldername', 'data/beam/diagnosis'};

% discretization parameters
N_z = 801;
N_s = 21; 
N_modes = 28;
dt = 5e-3;
tEnd = 34;
relIncTol = 1e-6;

examples.thesis.cantilever.diagnosis.cantilever;
examples.thesis.cantilever.detection.inputSignals;
examples.thesis.cantilever.diagnosis.faults;
examples.thesis.cantilever.diagnosis.createKernel;

%% solve the fault diagnosis kernel equation
T = 1.75;
I = quantity.EquidistantDomain("tau", 0, T, 'stepSize', dt);
theta = signals.GevreyFunction( 'order', 1.9999, "diffShift", 1, "numDiff", kernel.N_s, ...
				'timeDomain', I, "name", "theta");

[ioIntegralKernels, kernels, measures] = ...
	kernel.computeIdentificationFilter(I, "bounds", inputs.d.bounds, "theta", theta);

[~, relInc, numIter] = kernel.computeIOkernels(kernels.Mue, ...
	"relativeIncrementTolerance", relIncTol);

% relativeIncrement, maxRelativeIncrement, numIter

assert( measures.maxRelativeIncrement < kernel.relIncTol );

%% do the fault IDENTIFICATION
identification = beam.discreteModel.simulate(t, "u", inputs.u, "d_deterministic", inputs.d.det, ...
	"f", inputs.f, "plot", flags.plot, "fig", 3);

identification.I = quantity.EquidistantDomain("tau", 0, T, 'stepSize', t.stepSize);
firFilter = kernel.generateFirFilters(identification.I, ioIntegralKernels, "type", "diagnosis", ...
	"discretizationFunction", @signals.fir.midpoint);

[identification.r, identification.result] = ...
	kernel.firFaultDiagnosis( firFilter, ...
	identification.y, ...
	identification.u, ...
	"f", identification.f, ...
	"d_bounded", identification.d.bounded, ...
	"occurrence", occurrences);

if flags.plot
	kernel.plotResult( identification.r, identification.result, "diagnosis", identification.I.upper, ...
		"f", identification.f, "occurrence", occurrences);
end

%% do the fault DIAGNOSIS
examples.thesis.cantilever.diagnosis.faults;

estimation = beam.discreteModel.simulate(t, "u", inputs.u, "d_deterministic", inputs.d.det, ...
	"d_bounded", inputs.d.bounded, "f", inputs.f, "plot", flags.plot, "fig", 5);

[estimation.r, estimation.result] = ...
	kernel.firFaultDiagnosis( firFilter, ...
		estimation.y, ...
		estimation.u, ...
		"f", estimation.f, ...
		"d_bounded", estimation.d.bounded, ...
		"occurrence", occurrences);

if flags.plot
	kernel.plotResult( estimation.r, estimation.result, "diagnosis", identification.I.upper, ...
		"f", estimation.f, "occurrence", occurrences);
end

% compute the signal to noise ratio:
nominal = beam.discreteModel.simulate(t, "u", inputs.u, "d_deterministic", inputs.d.det, ...
	"f", inputs.f, "plot", false);
noiseSignal = nominal.y - estimation.y;
SNR = 10 * log10( int( nominal.y.' * nominal.y ) / int( noiseSignal.' * noiseSignal) );

%% do the pure fault detection for this example
if flags.detection

	detection.kernel = faultdiagnosis.parabolic.DetectionKernel( beam, ...
		"disturbanceModel", inputs.models.d, ...
		"delta", inputs.d.bounds, ...
		"N_s", N_s, "truncate", true);

	detection.T = 2;
	detection.I = quantity.EquidistantDomain("tau", 0, detection.T, 'stepSize', dt);

	detection.theta = signals.GevreyFunction( 'order', 1.999, "diffShift", 1, "numDiff", N_s, ...
					'timeDomain', detection.I, "name", "theta");

	expectedMagnitudes = max(abs( estimation.f));

	detection.refTrj = detection.kernel.referenceTrajectoryPlanningStationaryGain(detection.I, "Mf", ...
			1./expectedMagnitudes, "theta", detection.theta); 

	[detection.ioIntegralKernels, ~, ~, detection.maxRelInc] = ...
		detection.kernel.compute(detection.I, "basicVariable", detection.refTrj );	
	detection.firFilter = detection.kernel.generateFirFilters(detection.I, ...
		detection.ioIntegralKernels, "type", "detection");

	[detection.r, detection.result] = ...
		kernel.firFaultDiagnosis( detection.firFilter, ...
			estimation.y, ...
			estimation.u, ...
			"f", estimation.f, ...
			"d_bounded", estimation.d.bounded, ...
			"plotThresholds", true, "occurrence", occurrences, "plotFault", true, "fig", 7, ...
			"plot", flags.plot);

end

if flags.doTheExport
	examples.thesis.cantilever.diagnosis.export;
end

if flags.save
	if ~isfolder("build/beam")
		mkdir("build/beam")
	end
	flags.toc = toc(flags.tic);
	fName = "build/beam/faultDiagnosisResult"+datestr(now, 'yyyymmddTHHMMSS');
	save(fName);
	fprintf("Saved workspace to %s \n", fName);
end
