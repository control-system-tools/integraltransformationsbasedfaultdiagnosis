clear data

% identification data:
data.simIdentification = export.catQuantities( ...
	{identification.u; identification.d.deterministic; identification.d.bounded; ...
	identification.f; identification.y}, ...
	"filename", "simIdentification",  "N", 401, exportOptions{:});

data.residualIdentification = export.catQuantities( ...
	{identification.r, identification.result.fHatUpper, identification.result.fHatLower}, ...
	"filename", "residualIdentification", "N", 1001, exportOptions{:});

% estimation data
data.simEstimation = export.catQuantities( ...
	{estimation.r, estimation.d.total, estimation.f, estimation.y}, ...
	"filename", "simEstimation",  "N", 601, exportOptions{:});

data.residualEstimation = export.catQuantities( ...
	{estimation.r, estimation.result.boundUpper, estimation.result.boundLower, ...
	estimation.result.fHatUpper, estimation.result.fHatLower}, ...
	"filename", "residualEstimation", "N", 1001, exportOptions{:});

data.ioKernels = export.catQuantities( ...
	{ioIntegralKernels.N, ioIntegralKernels.Mu}, ...
	"filename", "integralKernels", "N", 201, exportOptions{:});

% parameters
parameters = struct();
parameters.fB1 = firFilter.threshold(1);
parameters.fB2 = firFilter.threshold(2);
parameters.fB3 = firFilter.threshold(3);
parameters.fB4 = firFilter.threshold(4);
parameters.t0 = t.lower;
parameters.tEnd = t.upper;
parameters.dt = t.stepSize;
parameters.T = I.upper;
parameters.gevreyOrder = theta.order;
parameters.N_modes = N_modes;
parameters.Nz = N_z;
parameters.Ns = max( struct2array( numIter ) );
parameters.dOmega = inputs.models.d.omega;
parameters.SNR = SNR;
parameters.relIncTol = kernel.relIncTol;
parameters.Nfilter = identification.I.n;

% add data of the faults:
parameters.f10 = inputs.models.f(1).initialCondition;
parameters.f201 = inputs.models.f(2).initialCondition(1);
parameters.f202 = inputs.models.f(2).initialCondition(2);
parameters.f301 = inputs.models.f(3).initialCondition(1);
parameters.f302 = inputs.models.f(3).initialCondition(2);
parameters.f401 = inputs.models.f(4).initialCondition(1);
parameters.f402 = inputs.models.f(4).initialCondition(2);
parameters.fOmega2 = inputs.models.f(2).omega;
parameters.fOmega4 = inputs.models.f(4).omega;

% add parameters of the input signals:
assert( all( diff( abs([inputs.u.fmax, inputs.u.fmin]) ) == 0 ) )
parameters.uBounds = inputs.u(1).fmax;
parameters.dTilde01 = inputs.models.d.initialCondition(1);
parameters.dTilde02 = inputs.models.d.initialCondition(2);

% add the estimation parameters
parameters = misc.structMerge( parameters, estimation.result.parameters );

data.parameters = export.namevalues.struct2namevalues(parameters, ...
	'filename', 'parameters', exportOptions{:});

export.Data.exportAll(data)

pathParts = struct(exportOptions{:});
export.mat2latex( svd( measures.Upsilon ).'*1e-12, ...
	fullfile(pathParts.basepath, pathParts.foldername, "svdUpsilonE12.tex"), ...
	"matrixEnvironment", "matrix", "separator", ", " )


