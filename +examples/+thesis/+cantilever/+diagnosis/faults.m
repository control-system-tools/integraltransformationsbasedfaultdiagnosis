



inputs.models.f(1) = signals.models.Constant(10, "timeDomain", t, "occurrence", 6);
inputs.models.f(2) = signals.models.Sinus(8, "timeDomain", t, "occurrence", 12, ...
	"initialCondition", [14; 0]);
inputs.models.f(3) = signals.models.Ramp(0, -.5, "timeDomain", t, "occurrence", 18);
inputs.models.f(4) = signals.models.Sinus(1, "timeDomain", t, "occurrence", 24, ...
	"initialCondition", [1.1; 0.4]);
inputs.f = inputs.models.f.simulate();
inputs.f.setName("f")
if flags.plot
	inputs.f.plot("smash", true)
end
occurrences = [inputs.models.f.occurrence];