init;

flags.tic = tic;
flags.doTheExport = true;
flags.detectionKernel = "stationaryGain";
flags.parameterStudy = false;
flags.samplingRateStudy = false;
flags.detection = false;
flags.plot = false;
flags.save = true;
exportOptions = {'basepath', '/home/fischer/text/diss/', 'foldername', 'data/beam/diagnosis'};

% discretization parameters
N_z = 801;
N_s = 21;
N_modes = 28;