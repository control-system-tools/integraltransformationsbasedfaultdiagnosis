%% Parameterstudy for the detection window length

clear study

studySetup.tic = tic;
studySetup.n = 61;
studySetup.o = 4;
studySetup.start = 0.3;
studySetup.stop = 4;
studySetup.T = logspace( log10( studySetup.start ), log10( studySetup.stop ), studySetup.n );
studySetup.order = linspace( 1.6, 1.999, studySetup.o);
studySetup.Ntau = 1e3;
studySetup.N_s = N_s;
studySetup.N_z = N_z;
studySetup.N_modes = N_modes;
studySetup.expectedMagnitudes = expectedMagnitudes;

% Matlab requires round about 16GB and ca 45 seconds for computation of one kernel:
% estiamte the required time:
detection.kernel.report( sprintf("Estimated computation time %g minutes.", ...
	studySetup.n * 80 / 60));

study(1:studySetup.n, 1:studySetup.o) = struct();
for k = 1:studySetup.n
	parfor l = 1:studySetup.o
		tic
		fprintf("--- k = % i --- l = % i --------------------------------------------------\n", k, l);
		fprintf("Compute the kernels for T = %g  and alpha = %g \n", studySetup.T(k), studySetup.order(l));
		
		study_kl = struct();
		study_kl.I = quantity.EquidistantDomain("tau", 0, studySetup.T(k), "stepNumber", studySetup.Ntau);
		
		% Compute the fault detection kernel on the detection time window Ik:
		study_kl.theta = signals.GevreyFunction( 'order', studySetup.order(l), "diffShift", 1, "numDiff", N_s, ...
			'timeDomain', study_kl.I, "g", @signals.gevrey.bump.g, "name", "theta");
		
		% 1) define the reference trajectory:
		study_kl.ref = detection.kernel.referenceTrajectoryPlanningStationaryGain(study_kl.I, ...
			"Mf", 1./expectedMagnitudes, ...
			"theta", study_kl.theta);
		
		% 2) solve the kernel equations and compute the fir filters
		[study_kl.ioKernels, study_kl.solutions] = detection.kernel.compute(study_kl.I, ...
			"basicVariable", study_kl.ref);
		% 3) discretization of the integral terms in the fault diagnosis equation:
		detection.kernel.report("Discretize the integral terms and generate FIR filters.")
		study_kl.fir = detection.kernel.generateFirFilters(study_kl.I, study_kl.ioKernels, ...
			"type", "detection");
		
		study_kl.threshold = study_kl.fir.threshold;
		study_kl.Sf = l2norm( study_kl.ioKernels.Mf )^2;
		study_kl.Sd = l2norm( study_kl.ioKernels.Md_bounded )^2;
		study_kl.J = study_kl.Sd / study_kl.Sf;
		
		study_kl.minWeightedStationaryResponse = abs( expectedMagnitudes .* int( study_kl.ioKernels.Mf , "tau", 0, studySetup.T(k)));
		% compute the normalized detection delay:
		%w = [inputs.models.f.initialCondition];
		
		study_kl.Mf = int( expectedMagnitudes' * study_kl.ioKernels.Mf , "tau", 0, "tau");
		idx = find( abs( study_kl.Mf.on() ) >= study_kl.threshold, 1, "first");
		
		if isempty(idx)
			study_kl.delayTime = inf;
		else
			study_kl.delayTime = study_kl.I.grid(idx);
		end
		
		% 			if flags.plot
		% 				figure(); clf;
		% 				subplot(311);
		% 				plot( study_kl.ref.f, "smash", true, "figureId", 0)
		%
		% 				subplot(312);
		% 				plot( study_kl.ioKernels.Mf, "smash", true, "figureId", 0, "hold", true);
		%
		% 				subplot(313);
		% 				plot( study_kl.Mf, "smash", true, "figureId", 0, "hold", true);
		% 				hold on;
		% 				plot( study_kl.I.grid, study_kl.threshold * ones( study_kl.I.n, 1) );
		% 				plot( study_kl.delayTime, study_kl.Mf.at( study_kl.delayTime ), "x", "LineWidth", 5);
		%
		% 				suptitle("kernels for $T = " + range.T(k) + "$ and $\alpha = " + range.order(l) +"$");
		% 			end
		
		study(k,l).threshold = study_kl.threshold;
		study(k,l).J = study_kl.J;
		study(k,l).delayTime = study_kl.delayTime;
		
		fprintf("--- finished --- k = % i --- l = % i --- toc = %g seconds ------------\n", k, l, toc);
	end
end

if flags.save
	if ~isfolder("build/beam")
		mkdir("build/beam")
	end
	fName = "build/beam/beamDetectionStudy_" + datestr(now, 'yyyymmddTHHMMSS');
	save(fName, "study", "studySetup");
	fprintf("Saved file %s \n", fName);
end

if flags.plot
	figure(); clf;
	subplot(311);
	
	[grid.O, grid.T] = meshgrid( studySetup.order, studySetup.T );
	surf( grid.T, grid.O, reshape( [study.J], studySetup.n, length( studySetup.order ) ));
	xlabel("$T$", "Interpreter", "latex")
	ylabel("$\alpha$", "Interpreter", "latex")
	zlabel("$J = S_d / S_f$", "Interpreter", "latex")
	hold on
	
	subplot(312); hold off;
	%
	surf( grid.T, grid.O, reshape( [study.threshold], studySetup.n, length( studySetup.order ) ), "FaceAlpha", 0.1, "EdgeColor", "none");
	hold on
	plot3( grid.T, grid.O, reshape( [study.threshold], studySetup.n, length( studySetup.order ) ), "LineWidth", 2);
	view(0,0)
	
	hold on;
	xlabel("$T$", "Interpreter", "latex")
	ylabel("$\alpha$", "Interpreter", "latex")
	zlabel("$r_{th}$", "Interpreter", "latex");
	
	subplot(313);
	surf( grid.T, grid.O, reshape( [study.delayTime], studySetup.n, length( studySetup.order ) ));
	
	xlabel("$T$", "Interpreter", "latex")
	ylabel("$\alpha$", "Interpreter", "latex")
	zlabel("$\Delta_t$", "Interpreter", "latex");
	
	suptitle("Performance index and threshold in dependence on T");
end

%% export
if flags.doTheExport
	rbData = quantity.Discrete( reshape( [study.threshold], studySetup.n, length( studySetup.order ) ), ...
		quantity.Domain("T", studySetup.T), "name", "rB");
	detectionDelay = quantity.Discrete( reshape( [study.delayTime], studySetup.n, length( studySetup.order ) ), ...
		rbData(1).domain, "name", "d");
	
	
	studyParameters.alphaMin = studySetup.order(1);
	studyParameters.alphaMax = 2;
	studyParameters.alphaN = studySetup.o;
	
	for i=1:studySetup.o
		studyParameters.("alpha" + i) = studySetup.order(i);
	end
	
	studyData.rbData = rbData.exportData("filename", "rb", exportOptions{:});
	studyData.detectionDelay = detectionDelay.exportData("filename", "detectionDelay", exportOptions{:});
	
	% 	studyParameters.expectedMagnitudes = sprintf("%d, ", expectedMagnitudes);
	studyParameters.expectedMagnitudesMin = min( expectedMagnitudes );
	
	studyData.parameters = export.namevalues.struct2namevalues(studyParameters, ...
		'filename', 'studyParametersDetection', exportOptions{:});
	
	export.Data.exportAll( studyData )
end

studySetup.toc = toc(studySetup.tic);
