% ---- faults ----
inputs.models.f(1) = signals.models.Constant(-1, "timeDomain", t, "occurrence", 6, "offTime", 12);
inputs.models.f(2) = signals.models.Constant(30, "timeDomain", t, "occurrence", 18, "offTime", 24);
inputs.models.f(3) = signals.models.Constant(30, "timeDomain", t, "occurrence", 30, "offTime", 36);
inputs.models.f(4) = signals.models.Constant(.5, "timeDomain", t, "occurrence", 42, "offTime", 48);
inputs.models.f(5) = signals.models.Constant(20, "timeDomain", t, "occurrence", 54, "offTime", 60);
inputs.models.f(6) = signals.models.Constant( 1.5, "timeDomain", t, "occurrence", 66, "offTime", 72);
inputs.f = inputs.models.f.simulate();
% --- set the multiplicative faults ---
inputs.f(1) = inputs.f(1) * inputs.u(1);
%
inputs.f(5) = inputs.f(5) * signals.UniformDistributedSignal(t, "steps", 50, "seedt", 15, "seedf", 19, ...
	"fmin", 0, "fmax", +1);
inputs.f(2) = inputs.f(2) * signals.UniformDistributedSignal(t, "steps", 57, "seedt", 16, "seedf", 7, ...
	"fmin", 0, "fmax", +1);
inputs.f.setName("f")
if flags.plot
	plot(inputs.f , "smash", true)
end