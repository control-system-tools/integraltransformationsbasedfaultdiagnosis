init;

arrayfun(@(i) figure(i), 1:6);

flags.tic = tic;
flags.doTheExport = false;
flags.detectionKernel = "stationaryGain";
flags.samplingRateStudy = false;
flags.lowOrderDetection = false;
flags.plot = true;
flags.save = false;
exportOptions = {'basepath', '/home/fischer/text/diss/', 'foldername', 'data/beam/detection'};%
 % '/pfs/work7/workspace/scratch/ul_tgz90-faultDetection-0/'
% discretization parameters
N_z = 801;
N_s = 19;
N_modes = 28;
dt = 5e-3;
tEnd = 72;
relIncTol = 1e-6;

examples.thesis.cantilever.detection.cantilever; % -> beam
examples.thesis.cantilever.detection.inputSignals;
examples.thesis.cantilever.detection.faults;

%% SIMULATION
% fault detection with bounded disturbance
inputs.f(4) = inputs.f(4) * 0; % !!! set the multiiplicative output fault to zero !!!
sim = beam.discreteModel.simulate(t, "u", inputs.u, "d_deterministic", inputs.d.det, ...
	"d_bounded", inputs.d.bounded, "f", inputs.f, "plot", flags.plot, "fig", 1);
% add the multiplicative output fault:
% y(1) = ( 1 + Delta_f4 ) * y_r(4) -> f4 = Delta_f4 * y_r(4)
deltaF4 = inputs.models.f(4).simulate() * sim.y(1);
sim.y(1) = sim.y(1) + deltaF4;
sim.f(4) = deltaF4;

sim.y.setName("y");

disappears = [inputs.models.f.offTime];
occurrences = [inputs.models.f.occurrence];
expectedMagnitudes = max(abs( sim.f ));

% simulation without noise:
inputs.f(4) = inputs.f(4) * 0; % !!! set the multiiplicative output fault to zero !!!
noiseFree = beam.discreteModel.simulate(t, "u", inputs.u, "d_deterministic", inputs.d.det, ...
		"f", inputs.f, "plot", false);
% add the multiplicative output fault:
% y(1) = ( 1 + Delta_f4 ) * y_r(4) -> f4 = Delta_f4 * y_r(4)
deltaF4 = inputs.models.f(4).simulate() * noiseFree.y(1);
noiseFree.y(1) = noiseFree.y(1) + deltaF4;
noiseFree.f(4) = deltaF4;
noiseFree.y.setName("y");

signal = sim.y;
noise = sim.y - noiseFree.y;

SNR = 10 * log10( int( signal.' * signal ) / int( noise.' * noise ) );

if flags.plot
	plot( sim.y - noiseFree.y );
	plot(sim.y, "figureId", 4);
	plot(noiseFree.y, "figureId", 4, "hold", true)
end
%% fault detection kernel
examples.thesis.cantilever.detection.createKernel;

%% fault detection kernel
T = 1.4;
I = quantity.EquidistantDomain("tau", 0, T, 'stepSize', dt);

theta = signals.GevreyFunction( 'order', 1.999, "diffShift", 1, "numDiff", N_s, ...
				'timeDomain', I, "g", @signals.gevrey.bump.g, "name", "theta");
		
%% kernel with adjustable stationary gain
if flags.detectionKernel == "stationaryGain"
	% compute a solution for the fault detection kernel:
	% 1) define the reference trajectory:
	[refStationaryGain, ~, ~, mBarf] = ...
		detection.kernel.referenceTrajectoryPlanningStationaryGain(I, ...
		"Mf", 1./expectedMagnitudes, "theta", theta, "forceInitialPointToBeZero", true);
	refBasicVar = refStationaryGain;
end
%% L2/L2 - optimal kernel
if flags.detectionKernel == "l2l2"
	basis = signals.Sinusoidal.fourierBasis( I, "numDiff", N_s, "order", 0 );
	refL2L2 = detection.kernel.referenceTrajectoryPlanningL2L2PerformanceIndex(I, ...
		"normalizeTheta", true, "basis", basis, "theta", theta, "Wf", diag(max(sim.f)));
	refBasicVar = refL2L2;
end

%%
% 2) solve the kernel equations and compute the fir filters
% [ioIntegralKernels, solution, relInc, maxRelInc, iter] = ...
% 	detection.kernel.compute(I, "basicVariable", refBasicVar);
[ioIntegralKernels, relInc, numIter] = detection.kernel.computeIOkernels(refBasicVar, ...
	"relativeIncrementTolerance", relIncTol);

% 3) discretization of the integral terms in the fault diagnosis equation:
detection.kernel.report("Discretize the integral terms and generate FIR filters.")
firFilter = detection.kernel.generateFirFilters(I, ioIntegralKernels, "type", "detection", ...
	"discretizationFunction", @signals.fir.midpoint);

ioIntegralKernels.intMf = int( expectedMagnitudes' * ioIntegralKernels.Mf , "tau", 0, "tau");
idx = find( abs( ioIntegralKernels.intMf.on() ) >= firFilter.threshold, 1, "first");
ioIntegralKernels.delayTime = I.grid(idx);

detection.I = quantity.EquidistantDomain("tau", 0, T, 'stepSize', t.stepSize);
firFilter = detection.kernel.generateFirFilters(detection.I, ioIntegralKernels, "type", "detection");
detection.t = quantity.EquidistantDomain("t", 0, t.upper, "stepSize", detection.I.stepSize);

%% DETECTION UNDISTURBED
[detectionNoiseFree.r, detectionNoiseFree.result] = ...
	detection.kernel.firFaultDiagnosis( firFilter, ...
		noiseFree.y.subs("t",	detection.t), ...
		noiseFree.u.subs("t", detection.t), ...
		"f", noiseFree.f.subs("t", detection.t), ...
		"occurrence", occurrences);
detectionNoiseFree.r.setName("rNoiseFree");
if flags.plot
	detection.kernel.plotResult(detectionNoiseFree.r, detectionNoiseFree.result, "detection", ...
		I.upper, "occurrence", occurrences, "disappears", disappears, "figureID", 3);
end	

%% DETECTION - DISTURBED
[r, detection.result] = ...
	detection.kernel.firFaultDiagnosis( firFilter, ...
		sim.y.subs("t",	detection.t), ...
		sim.u.subs("t", detection.t), ...
		"f", sim.f.subs("t", detection.t), ...
		"d_bounded", sim.d.bounded.subs("t", detection.t), ...
		"occurrence", occurrences);

if flags.plot
	detection.kernel.plotResult(r, detection.result, "detection", I.upper, ...
		"occurrence", occurrences,  "disappears", disappears, "figureID", 3);
% 	truncatedTimeDomain = quantity.EquidistantDomain( "t", T, t.upper, "stepSize", detection.I.stepSize);
% 	title("N_z = " + num2str( N_z ) + " N_modes = " + num2str(N_modes) + " N_s = " + num2str(N_s) + ...
% 		" dt = " + num2str( dt ) + " ||r||2 = " + num2str( l2norm(r.subs("t", truncatedTimeDomain))) )
end	

%% measures for the fault detection
fprintf(" --- Performance index ---\n")
Sf = l2norm( ioIntegralKernels.Mf )^2;
Sd = l2norm( ioIntegralKernels.Md_bounded )^2;

fprintf("Sf = " + Sf + ...+
	"   Sd = " + Sd + ...
	"   J = Sd/Sf = " + Sd/Sf + "\n" )

for k = 1:beam.n.f
	fprintf( "Sf(" + k + ") = " + l2norm( ioIntegralKernels.Mf(k) )^2 + "   ")
end
fprintf("\n")

%% moving horizion signal energy
fprintf(" --- Moving horizon signal energy --- \n")
av.t = quantity.EquidistantDomain("tau", 0, I.upper, "stepSize", t.stepSize);
av.W = quantity.Discrete.eye(1, av.t); % 1/av.t.upper * 
av.fir = signals.fir.midpoint( av.W, av.t );
flat.avR = signals.fir.mFilter( av.fir, r.^2 );

if flags.plot
	figure(); clf;
	semilogy( t.grid, flat.avR.on()); hold on;
	legend(["||r||_{2,I}^2"]);
	ylim([1e-2, 1e3])
	%xlim([0, t.upper])
	
	h = gca;
	for i = 1:numel( occurrences )
		a = area([occurrences(i), disappears(i) + T], ...
			h.YLim(2) * ones(2,1), h.YLim(1));
		a.FaceColor = [0 0.4470 0.7410];
		a.FaceAlpha = 0.2;
		a.LineStyle = 'none';
	end
	% title(sprintf( "Sliding energy level of the residuals \n T = %i, $\\nu$ = %i and $J^* = \\frac{||m_d^T W_d ||_2^2}{||m_f^T W_f ||_2^2} = %g $" , ...
	% 	T, flat.args.nu+1, flat.args.J))
end
%% parameterstudy for the sampling rate%%
if flags.samplingRateStudy
	
	[samplingRateStudy, truncated] = examples.thesis.cantilever.detection.samplingRateStudy(...
		beam, inputs, detection.kernel, ioIntegralKernels);
	
	if flags.doTheExport
		residualNormFirOrder = export.dd( "M", [samplingRateStudy.samplingPoints', samplingRateStudy.rL2Norm'], ...
				"header", {"N", "norm"}, "filename", "residualNormFirOrder", exportOptions{:});
		residualNormFirOrder.export();
	end
	
	if flags.plot
		figure(14);
		subplot(211)
		plot( samplingRateStudy.samplingRate, samplingRateStudy.rL2Norm)
		subplot(212)
		plot( samplingRateStudy.samplingPoints, samplingRateStudy.rL2Norm)
		title("Truncated detection with FIR filters of order " + lowOrderDetection.firOrder)
	end
	
end

%% low Order detection
if flags.lowOrderDetection
	lowOrderDetection.firOrder = 10;
	lowOrderDetection.I = quantity.EquidistantDomain("tau", 0, T, 'stepNumber', lowOrderDetection.firOrder);
	lowOrderDetection.firFilter = detection.kernel.generateFirFilters(lowOrderDetection.I, ioIntegralKernels, "type", "detection");
	lowOrderDetection.t = quantity.EquidistantDomain("t", 0, t.upper, "stepSize", lowOrderDetection.I.stepSize);

	[lowOrderDetection.r, lowOrderDetection.result] = ...
		detection.kernel.firFaultDiagnosis( lowOrderDetection.firFilter, ...
			sim.y.subs("t",	lowOrderDetection.t), ...
			sim.u.subs("t", lowOrderDetection.t), ...
			"f", sim.f.subs("t", lowOrderDetection.t), ...
			"d_bounded", sim.d.bounded.subs("t", lowOrderDetection.t), ...
			"occurrence", occurrences);
	
	if flags.plot
		detection.kernel.plotResult(lowOrderDetection.r, lowOrderDetection.result, ...
			"detection", I.upper, "occurrence", occurrences,  "disappears", disappears);
	end

	% add additional fields to the detection parameters:
	lowOrderDetection.result.parameters.threshold = lowOrderDetection.firFilter.threshold;
	lowOrderDetection.result.parameters.Nfilter = lowOrderDetection.firFilter.numel;
	lowOrderDetection.result.parameters.stepSize = lowOrderDetection.I.stepSize;
	
	for i = 1:beam.n.f
		lowOrderDetection.result.parameters.("t" + i) = occurrences(i);
		lowOrderDetection.result.parameters.("e" + i) = disappears(i);
		lowOrderDetection.result.parameters.("detectionDelay" + i) = ...
			lowOrderDetection.result.parameters.("detection"+i) - occurrences(i);
	end
	
	if flags.doTheExport
		
		lowOrderDetection.result.parameters.Nfilter = lowOrderDetection.I.n;
		lowOrderDetection.result.parameters.stepSize = lowOrderDetection.I.stepSize;
		lowOrderDetection.result.parameters.threshold = lowOrderDetection.firFilter.threshold;
		
		
		parametersDetectionFirOrder = export.namevalues.struct2namevalues(...
			lowOrderDetection.result.parameters, ...
			'filename', 'parametersDetectionLowOrder', exportOptions{:});

		residualLowOrder = exportData([lowOrderDetection.r, lowOrderDetection.result.boundUpper, lowOrderDetection.result.boundLower],...
			"filename", "residualLowOrder", exportOptions{:}, "header", {"t", "r", "thU", "thL"}, "N", 401);

		parametersDetectionFirOrder.export();
		residualLowOrder.export();
	end
	
end

if flags.save
	if ~isfolder("build/beam")
		mkdir("build/beam")
	end
	flags.toc = toc(flags.tic);
	save("build/beam/faultDetectionResult"+datestr(now, 'yyyymmddTHHMMSS'));
end
