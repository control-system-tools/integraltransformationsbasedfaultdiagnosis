function [samplingRateStudy, truncated] = samplingRateStudy(beam, inputs, kernel, ioIntegralKernels)

tSim = inputs.u(1).domain;
tEnd = tSim.upper;
sim = beam.discreteModel.simulate(tSim, "u", inputs.u, "d_deterministic", inputs.d.det);

T = ioIntegralKernels.Mf(1).domain.upper;

samplingPoints = [5:20, round( linspace(21, ioIntegralKernels.Mf(1).domain.n, 10) )];
%samplingRate = linspace( 5e-3, 0.5, n);
n = numel( samplingPoints );

for k = 1:n
	tic
	fprintf("--- k = % i -----------------------------------------------------\n", k);
	II = quantity.EquidistantDomain("tau", 0, T, "stepNumber", samplingPoints(k));
	samplingRate(k) = II.stepSize;
	
	firFilter = kernel.generateFirFilters(II, ioIntegralKernels, "type", "detection");
	
	t = quantity.EquidistantDomain("t", 0, tEnd, "stepSize", samplingRate(k));
	truncated_t(k) = quantity.EquidistantDomain("t", T, tEnd, "stepSize", samplingRate(k));
	
	r = kernel.firFaultDiagnosis( firFilter, ...
		sim.y.subsDomain("t", t), ...
		sim.u.subsDomain("t", t));
	
	truncated_r(k) = r.subsDomain("t", truncated_t(k));
	
	rL2Norm(k) = l2norm( truncated_r(k) );
	fprintf("--- finished --- n = % i ------ toc = %g seconds ------------\n", n, toc);
end

truncated = struct();
truncated.t = truncated_t;
truncated.r = truncated_r;

samplingRateStudy = struct();
samplingRateStudy.samplingPoints = samplingPoints;
samplingRateStudy.samplingRate = samplingRate;
samplingRateStudy.rL2Norm = rL2Norm;

end

