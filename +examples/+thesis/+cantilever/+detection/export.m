%% export
data.theta = theta.fun.exportData("filename", "theta", exportOptions{:});
data.h1 = beamParameters.H1.exportData('filename', 'h1', "useHeader", false, exportOptions{:});
data.h2 = beamParameters.H2.exportData('filename', 'h2', "useHeader", false, exportOptions{:});

% disturbance signals
distJumps = {};
for k = 1:beam.n.d_bounded
	for i = 2:(inputs.d.bounded(k).steps)
		distJumps{end+1} = struct('idx_t', 1, 'idx_j', k+1, 'tj', inputs.d.bounded(k,1).tk(i));
	end
end
exportDisturbances = quantity.Discrete( inputs.d.bounded ) .* [1/20, 1, 1, 1, 1]';
exportDisturbances.setName("d_bounded");
data.boundedDisturbance = exportData( exportDisturbances, ...
	"jumps", distJumps, "filename", "boundedDisturbance", exportOptions{:});

% simulation signals:
jumps = {};
for k = 1:beam.n.f
	jumps{end+1} = struct('idx_t', 1, 'idx_j', k+1, 'tj', occurrences(k));
	jumps{end+1} = struct('idx_t', 1, 'idx_j', k+1, 'tj', disappears(k));
end

data.faults = exportData( sim.f, "jumps", jumps, "filename", "faults", exportOptions{:});

dBounded1Scaled = sim.d.bounded(1) / 20;
noiseFree.y.setName("yNoiseFree");

data.simulation = export.catQuantities( ...
	{sim.u; sim.d.deterministic; sim.d.bounded; dBounded1Scaled; sim.f; sim.y; noiseFree.y}, ...
	"filename", "simulation",  "N", 401, exportOptions{:});

% integral kernels:
data.ioKernels = export.catQuantities(...
	{ioIntegralKernels.N, ioIntegralKernels.Mu, ioIntegralKernels.Mf, ...
	ioIntegralKernels.Md_bounded}, ...
	"filename", "integralKernels", exportOptions{:});

% residual data
data.residual = export.catQuantities(...
	{r, detection.result.boundUpper, detection.result.boundLower, detectionNoiseFree.r}, ...
	"filename", "residual", exportOptions{:}, "N", 1000);

% parameters
parameters = struct();
parameters = export.Data.numericStruct(beamParameters, "parameters", parameters);
parameters = export.Data.numericStruct(struct("T", T, "gevreyOrder", theta.order), ...
	"parameters", parameters);
parameters.sigma = theta.sigma;
parameters.N_modes = N_modes;
parameters.Nz = N_z;
parameters.Node = size( beam.discreteModel.stsp.A, 1 );
parameters.dt = dt;

parameters.dOmega = inputs.models.d.omega;

parameters.delta1 = inputs.d.bounds(1);
assert( all( diff( inputs.d.bounds(2:5) ) == 0 ) )
parameters.delta2345 = inputs.d.bounds(2);

% add parameters of the input signals:
assert( all( diff( abs([inputs.u.fmax, inputs.u.fmin]) ) == 0 ) )
parameters.uBounds = inputs.u(1).fmax;
parameters.dTilde01 = inputs.models.d.initialCondition(1);
parameters.dTilde02 = inputs.models.d.initialCondition(2);

parameters.f2Bound = inputs.models.f(2).initialCondition;
parameters.f5Bound = inputs.models.f(5).initialCondition;

data.parameters = export.namevalues.struct2namevalues(parameters, ...
	'filename', 'parameters', exportOptions{:});

% add additional fields to the detection parameters:
detection.result.parameters.threshold = firFilter.threshold;
detection.result.parameters.delayTime = ioIntegralKernels.delayTime;

% compute the relative increments error
% relInc, numIter
% 	relativeIncrementAll = cell2mat( struct2cell(relativeIncrement) );
detection.result.parameters.relativeIncrementTolerance = relIncTol;
detection.result.parameters.N_s = max( struct2array( numIter ) );
detection.result.parameters.Nfilter = detection.I.n;
detection.result.parameters.threshold = firFilter.threshold;
detection.result.parameters.delayTime = ioIntegralKernels.delayTime;

for i = 1:beam.n.f
	detection.result.parameters.("t" + i) = occurrences(i);
	detection.result.parameters.("e" + i) = disappears(i);
	detection.result.parameters.("detectionDelay" + i) = detection.result.parameters.("detection"+i) - occurrences(i);
	detection.result.parameters.("fEx" + i) = expectedMagnitudes(i);
end

detection.result.parameters.SNR = SNR;

assert( all( diff( disappears - occurrences ) == 0 ) )
detection.result.parameters.offDelay = disappears(1) - occurrences(1);
detection.result.parameters.tEnd = t.upper;
detection.result.parameters.T = T;

data.parametersDetection = export.namevalues.struct2namevalues(detection.result.parameters, ...
	'filename', 'parametersDetection', exportOptions{:});

export.Data.exportAll(data)

pathParts = struct(exportOptions{:});

export.mat2latex( detection.kernel.X0, ...
	fullfile(pathParts.basepath, pathParts.foldername, "x0.tex"));
export.mat2latex( mBarf.', fullfile(pathParts.basepath, pathParts.foldername, "mBarf.tex"));
