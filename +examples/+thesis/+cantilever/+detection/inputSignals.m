%% define the input signals (control inputs, disturbances, fautls)
clear inputs

t = quantity.EquidistantDomain("t", 0, tEnd, "stepSize", dt);

% ---- inputs ----
inputs.u(1,1) = signals.UniformDistributedSignal(t, "steps", 9, "seedt", 11, "seedf", 3, ...
	"fmin", -100, "fmax", 100, "filter", "gevrey", "f0", 25);
inputs.u(2,1) = signals.UniformDistributedSignal(t, "steps", 7, "seedt", 12, "seedf", 2, ...
	"fmin", -100, "fmax", 100, "filter", "gevrey");
% ---- faults ----
% inputs.models.f(1) = signals.models.Sinus(5, 'timeDomain', t, 'occurrence', 0, ...
%  	'initialCondition', [20; 0], "signalName", "d");

% ---- disturbances ----
inputs.models.d = signals.models.Sinus(3, 'timeDomain', t, 'occurrence', 0, ...
	'initialCondition', [20; 0], "signalName", "d");

inputs.d.det = inputs.models.d.simulate();
inputs.d.bounds = [2; .1; .1; .1; .1];
for k = 1:beam.n.d_bounded
	inputs.d.bounded(k,1) = signals.UniformDistributedSignal(t, 'seedt', 2*k, 'seedf', 3*k, ...
		"steps", 1e2, ...
		'fmin', -inputs.d.bounds(k), 'fmax', inputs.d.bounds(k), 'name', 'd', ...
		"normalize", true);	
end