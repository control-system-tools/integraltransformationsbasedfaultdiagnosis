function [parameters] = cantileverWithPointMass(optArgs)
%% creates an euler-bernoulli-beam 
%    the parameters are taken from [1], [2], except the spatial charackteristic for the
%    piezo patches, what is a self defined funtion to obtain the flatness properties.
%
%   [1] Henikl, J., Schröck, J., Meurer, T., et al. (2012). Infinit-dimensionaler
%   Reglerentwurf für Euler-Bernoulli Balken mit Macro-Fibre Composite Aktoren. at -
%   Automatisierungstechnik Methoden und Anwendungen der Steuerungs-, Regelungs- und
%   Informationstechnik, 60(1) [2}

arguments
	optArgs.N_z (1,1) double = 151;
	optArgs.no (1,1) double = 0.406;
end

no = optArgs.no;

% Beam equation:
%  mu(z) dt^2 w + gamma(z) dt w + dz^2 ( lmbda(z) dz^2 w ) = b(z) (u + f) + g1(z) delta
%  + g2(z) eta
%      w(0) = 0
%   dz w(0) = 0
% dz^2 w(1) = -JE dt^2 dz w(1) dz^3 w(1) = mE dt^2 w(1)

% beam parameters
L_c = 0.406;        %m              length
b_c = 45e-3;        %m              width
h_c = 0.75e-3;      %m              height
A_c = b_c * h_c;    %m^2            cross sectional area
rho_c = 1540.6;     %kg / m^3       density
gma_c = 3.1;        %kg / m^2 / s   viscos damping
Y_c = 29.6e9;       %Pa             tensile modulus            
I_c = b_c * h_c^3 / 12; %           axiale Flächenträgheitsmoment           

% patch parameters
h_p = 0.3e-3;       %m              height
e_p = 0.5e-3;       %m              distance between two electrodes
b_p = 28e-3;        %m              width
A_p = b_p * h_p;    %m²             cross sectional area
a11_beta11 = 11.34; %As / m^2       parameter for the piezoelectrical material
gma_p = 10;         %kg / m^2 / s   viscos damping
rho_p = 5250.1;     %kg / m^3       density
cp1111 = 31.97e9;   %???            parameter for the pieoelectrical material (from [2])
Gma_p = A_p * a11_beta11 * (h_c + h_p) / 2 / e_p; % 

% spatial characteristic
z = sym('z', 'real');
Z = quantity.EquidistantDomain("z", 0, L_c / no, "stepNumber", optArgs.N_z).';

L_p = 170e-3 / no; %m     length
xp1 = 31e-3 / no; %m     position
d = 80e-3 / no;  %m     length of smooth transition area

xp2 = xp1 + L_p;

% transition for smooth spatial charactersitic
[sig, a] = stateSpace.setPointChange(0, 1, 0, d, 2, "var", z);

% piecewise defined spatial characterisitic
h = (heaviside( z ) - heaviside( z - d ) ) * sig + ...
	(heaviside( z - d ) - heaviside( z - L_p + d ) ) + ...
	(heaviside( z  - L_p + d ) - heaviside( z - L_p)) * (1 - subs(sig, z, z - L_p + d)) ;

% spatial characteristic:
%h = quantity.Symbolic( (heaviside( z ) - heaviside( z - d ) ) * sig, Z, "name", "h") 
H1 = quantity.Symbolic( subs(h, z, (z - xp1) ), Z, "name", "h1");
H2 = quantity.Symbolic( subs(h, z, (z - xp2) ), Z, "name", "h2");

H = H1 + H2;

parameters.H1_middle = L_p/2 + xp1;
parameters.H2_middle = L_p/2 + xp2;

% input vector
beta = 2 * Gma_p / no^2;
B = beta * H1.diff("z", 2);

% point mass parameters TODO: Check dimensions for 'point mass'
m_E = 12.6e-3 * no^3;  %kg  point mass
L_E = 10e-3;   %m   length of 'point mass'
b_E = 2e-3;    %m   width of the 'point mass'
J_E = 1 / 12 * m_E * (L_E^2 + b_E^2) / no^2; 
	% moment of inertia for a cuboid

% area density:
muc = A_c * rho_c;
mup = 2 * A_p * rho_p;
mue =  muc + mup * H;

% viscos damping:
gmac = gma_c * b_c;
gmap = 2 * gma_p * b_p;
gma = gmac +  gmap * H;

% stiffness
lmbdac = Y_c * I_c / no^4;
lmbdap = 2 * cp1111 * b_p * ( ( h_c / 2 + h_p )^3 - h_c^3 / 8) / 3 / no^4;
lmbda = lmbdac + lmbdap * H ;

gama_ = 5.2191e-05 / 0.0465 * quantity.Symbolic.ones(1, Z); 

% outputs
cz = 1 / int( H2 ) / no^2 * 100;

parameters.c1 = 1e3;
parameters.c2 = 1e3;
parameters.c0 = cz;

parameters.b2 = 2e-4;

% disturbance inputs
parameters.g1 = gama_.at(1);
parameters.g2 = 1e-3;

% ---- faults ----
ez0 = gama_(1).at(0) / 2;
h2z = quantity.Symbolic( sym("z")^2, Z);
ez1 = ez0 / int(h2z);

parameters.ez = ez0 + ez1 * h2z;
parameters.ez0 = ez0;
parameters.ez1 = ez1;

% define beam parameters:
parameters.muc = muc;
parameters.mup = mup;
parameters.mue = mue;
parameters.gma = gma;
parameters.gmac = gmac;
parameters.gmap = gmap;
parameters.lmbda = lmbda;
parameters.lmbdac = lmbdac;
parameters.lmbdap = lmbdap;
parameters.b0 = beta;
parameters.g = gama_;
parameters.Lp = L_p ;
parameters.J_E = J_E;
parameters.m_E = m_E;
parameters.L_c = L_c;
parameters.xp1 = xp1;
parameters.xp2 = xp2;
parameters.d = d;
parameters.Z = Z;
parameters.B = B;
parameters.no = no;
parameters.H1 = H1;
parameters.H2 = H2;
parameters.hDegree = length(a)-1;

parameters.C_3 = [0 0 0 parameters.c1; 0 0 parameters.c2 0; 0 0 0 0];
parameters.C_1 = [zeros(2,4); 0 parameters.H2 * cz 0 0];

parameters.Gz = [gama_, 0 0 0 0]; 
parameters.G2 = [0, parameters.g2 0 0 0];
parameters.G4 = [ zeros(3,2), eye(3)];
parameters.G_poly = [1; zeros(4,1)];
parameters.G_bounded = eye(5);
