% explicit implementation of the trajectory planning for an ode-pde system.
% With this implementation the explicit implementation of the evaluation of the differential
% expressions for the trajectory planning is presneted. This can help to find bugs in the trajectory
% planning since all intermediate results are easily available.

clearvars
% definition of the system parameters:
z = quantity.Domain("z", linspace(0, 1, 111));
Z = quantity.Discrete(z.grid, z);
a = quantity.Symbolic(1, z, 'name', 'alpha');

Nt = 201;
T = 2;
t = quantity.EquidistantDomain("t", 0, T, 'stepNumber', Nt);

% system description
pde = faultdiagnosis.parabolic.ScalarParabolic(z, 'alpha', a, ...
	'q00', 0, 'q01', 1, 'b2', 1, ...
	'q10', 0, 'q11', 1, ...
	'c3', 1);

fModel = signals.SignalModel.sinusoidal("time", t, "initialCondition", [2; 0], 'omega', pi);
%fModel = signals.SignalModel.constant("time", t, "initialCondition", 2);

S = fModel.sys.A;
R = fModel.sys.C;
nv = size(S,1);
ode = ss(S', R', eye(size(S)), []);

%% differential parametrization
sImST = signals.PolynomialOperator( {-S', eye(size(S))} );
A_i = double( adj(sImST) );
a_i = shiftdim( double( det(sImST) ) ).';

W = [];
K = nv;
for k = 1:size(S,1)
	W = [W A_i(:,:,k) * R'];
end
W = [W, zeros(size(S, 1), K+1) ];

for k = 1:(K+1)
	W(end+1, :) = [zeros(1, k-1), a_i, zeros(1, K+1-k) ];
end

R_ = [R'; zeros( K+1, 1)];

% verträglichkeitsbedingung:9
assert( numeric.near( W * pinv(W) * R_, R_, 1e-9) )

p = pinv(W) * R_;

% verify mu:
for k = 1:K+1
	assert( numeric.near( a_i * p(k:nv+k), 0, 1e-9))
end

%% generation of the gevrey function
numDiff = 12;
numDiff_all = numDiff + nv + 1; 
% The plus 1 is required because the computation of the input n(tau) requires the derivative order
% k+1

theta = signals.GevreyFunction( 'order', 1.999, "offset", 1, "gain", -1, "numDiff", numDiff_all, 'timeDomain', t);

% compute the helper function for the expression: 
%	delta_ki = d_tau^k t^i / i!
% To be particular: delta(k+1, i+1) = d_tau^k t^i / i!
delta_ki = quantity.Discrete.zeros( [nv+K + 1, numDiff_all + 1], t);
for i = 0:nv+K
	for k = 0:numDiff_all
		
		if k <= i
			delta_ki(i+1,k+1) = quantity.Function(@(t) t.^(i-k) / factorial( i - k), t);
		end
	end
end

% compute the phi and its derivatives
factorial_i = factorial(1:nv+K+1);
phi = cell(numDiff_all+1,1);
for n = 0:numDiff_all
	phi{n+1} = quantity.Discrete.zeros(1, t);
	for i = 0:nv+K
		for k = 0:n
			phi{n+1} = phi{n+1} + p(i+1) * nchoosek(n, k) * delta_ki(i+1, k+1) * theta.diff(t, n -k );
		end
	end		
end
phi = signals.BasicVariable(phi{1}, phi(2:end));

% compute the mu0 and its derivatives:
mu0 = cell(numDiff+2,1);
for k = 0:numDiff + 1
	mu0{k+1} = quantity.Discrete.zeros(1,t);
	for i = 0:nv
		mu0{k+1} = mu0{k+1} + a_i(i+1) * phi.diff(t, i + k);
	end
end
mu0 = signals.BasicVariable(mu0{1}, mu0(2:end));

% compute the ODE solution q(tau)
q = quantity.Discrete.zeros( [nv, 1], t);
for k = 0:nv-1
	q = q + A_i(:,:,k+1) * R' * phi.diff(t, k);
end

% compute the PDE solution m(z,tau)
m = quantity.Discrete.zeros(1, [z, t]);
n = quantity.Discrete.zeros(1, t);
for k = 0:numDiff
	m = m + (1 - Z)^(2 * k) / factorial( 2* k) * mu0.diff(t, k);
	n = n - 1 / factorial(2*k + 1) * mu0.diff(t, k+1);
end

% simulation
sim = pde.discreteModel.simulate(t, 'u', n, 'plot', true);
lsim(ode, sim.y.on, sim.t.grid, R);

plot(sim.v - m)

