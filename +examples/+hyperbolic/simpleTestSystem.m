%function [norm, figureID] = simpleTestSystem(Nz, dt)
clearvars('-except', 'testCase', 'S', 'cl')
init;
Nz = 51;
dt = 0.01;

%%
fprintf("# Initialization\n")
spatialDomain = quantity.Domain("z", linspace(0, 1, Nz));
z = sym('z', 'real');
Lambda = quantity.Symbolic(diag([ 1, -2]), spatialDomain, 'name', 'Lambda');

faultySys = faultdiagnosis.hyperbolic.heteroDirectional.System(...
	Lambda, diag(-1), ...
	'Q_0', 0.5, ...
	'Q_1', -0.5, ...
	'L_2', [1 0], ...
	'Hb1', -1, ...
	'Bb0', 1, ...
	'E_1', eye(2) * quantity.Symbolic( heaviside( z - .3) - heaviside(z - .7), spatialDomain, 'name', 'E_1'), ...
	'Eb0', [1 2], ...
	'Eb1', [1 0], ...
	'E_3', [1 0], ...
	'E_4', [0 1], ...
	'G_1', quantity.Symbolic([sin(z * pi) 0; 0 sin(z*2*pi)], spatialDomain, 'name', 'G_1'), ...	
	'Gb0', [1 1], ...
	'Gb1', [1 1], ...
	'G_3', [1 1], ...
	'G_4', [1 1], ...
	'G_bounded', [1; 0], ...
	'G_deterministic', [0; 1], ...
	'stepSize', dt, ...
	'name', "System");

%% input signals
timeDomain = quantity.EquidistantDomain('t', 0, 80, 'stepSize', dt);
faultModel(1) = signals.models.Sinus(1, 'timeDomain', timeDomain, ...
					'initialCondition', [20; 0], "occurrence", 20);
faultModel(2) = signals.models.Constant(10, 'timeDomain', timeDomain, "occurrence", 50);
disturbanceModel(1) = signals.models.Sinus(3, 'timeDomain', timeDomain, ...
	'initialCondition', [5; 1]);
d_bounds = 1;
boundedDisturbance = signals.UniformDistributedSignal(timeDomain, 'steps', 25, ...
 	'seedt', 1, 'seedf', 1, 'fmin', -d_bounds, 'fmax', d_bounds);

u = signals.UniformDistributedSignal(timeDomain, 'steps', 5, ...
	'seedt', 1, 'seedf', 1, 'fmin', -30, 'fmax', 30);
f = faultModel.simulate();
d_det = disturbanceModel.simulate();

%%
fprintf("# Simulation of the system\n")
faultySimData = faultySys.discreteModel.simulate(timeDomain, 'u', u, 'f', f, ...
	'd_deterministic', d_det, 'd_bounded', boundedDisturbance);

%%
figure(1), clf,
plotOps = {'figureId', 0, 'hold', true, 'dock', false, 'smash', true};

subplot(4,2,[1 3])
plot(faultySimData.plant.u, plotOps{:});
plot(faultySimData.plant.f, plotOps{:});
plot(faultySimData.plant.d_bounded, plotOps{:});
plot(faultySimData.plant.d_deterministic, plotOps{:});

subplot(4,2, 5)
plot(faultySimData.plant.x(1), plotOps{:});

subplot(4,2, 7)
plot(faultySimData.plant.x(2), plotOps{:});

subplot(4,2,[2,4])
plot(faultySimData.plant.w, plotOps{:});

subplot(4, 2, [6, 8])
plot(faultySimData.plant.y, plotOps{:});

drawnow;

%%
fprintf("# Backstepping transformation\n")
faultDiagnosisKernel = faultySys.FaultDiagnosisKernel(faultModel, ...
	'disturbanceModel', disturbanceModel, ...
	'tolerance', 1e-4, ...
	'numberOfLines', spatialDomain.n);
%%
fprintf("# Kernel computation\n")
T = 10;
dE = 0;
I = quantity.EquidistantDomain("tau", 0, T, "stepSize", dt);
[filters, modulatingFunctions, kernels] = faultDiagnosisKernel.computeIdentificationFilter(I, ...
	"bounds", d_bounds);

%% fault diagnosis
[r, res] = faultDiagnosisKernel.firFaultDiagnosis(filters, faultySimData.plant.y, ...
	faultySimData.plant.u, "f", faultySimData.plant.f, ...
	"d_bounded", boundedDisturbance, ...
	"occurrence", [20, 50]);

faultDiagnosisKernel.plotResult(r, res, "diagnosis", T, "f", f, "d_bounded", boundedDisturbance, ...
	"occurrence", [faultModel.occurrence]);














